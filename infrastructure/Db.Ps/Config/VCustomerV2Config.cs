﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VCustomerV2Config : IEntityTypeConfiguration<VCustomerV2Entity>
    {
        public void Configure(EntityTypeBuilder<VCustomerV2Entity> builder)
        {
            builder.HasKey(e => e.RowNo);
            builder.ToTable("VCustomerV2");
        }
    }
}

﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main.ManifestCarrier_Report
{
    public interface IManifestCarrier_Report_List_UseCase : IApp<ManifestCarrierEntity>
    {
        public ReturnFormat Read(UiUserProfileEntity uiUserProfileEntity, object req);
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Master.MonitoringShipment.Commands
{
    public partial class MonitoringShipmentDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? monitoringShipmentId { get; set; }
    }

    public class MonitoringShipmentDeleteCommandHandler : IRequestHandler<MonitoringShipmentDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public MonitoringShipmentDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(MonitoringShipmentDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                var res = this.MonitoringShipmentDelete(req.monitoringShipmentId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_MonitoringShipmentDeleteEntity> MonitoringShipmentDelete(Int32? monitoringShipmentId)
        {
            String query = $@"EXEC SP_MonitoringShipmentDelete @monitoringShipmentId = '{monitoringShipmentId}'";
            var res = this._context.SP_MonitoringShipmentDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using Core.Entity.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Base
{
    public interface IPrivilegeActionRepo : IRepo<PrivilegeActionEntity>
    {
    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class MasterProvinceConfig : IEntityTypeConfiguration<MasterProvinceEntity>
    {
        public void Configure(EntityTypeBuilder<MasterProvinceEntity> builder)
        {
            builder.HasKey(e => e.MasterProvinceId);
            builder.ToTable("MasterProvince");
        }
    }
}
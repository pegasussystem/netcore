﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_PrivilegeListConfig : IEntityTypeConfiguration<V_PrivilegeListEntity>
    {
        public void Configure(EntityTypeBuilder<V_PrivilegeListEntity> builder)
        {
            builder.HasKey(e => e.PrivilegeId);
            builder.ToTable("V_PrivilegeList");
        }
    }
}

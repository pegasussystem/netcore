﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Main;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ui.api.main.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {

        private readonly IAppWrapperPs _appPs;
        public LocationController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpGet("city")]
        public async Task<IActionResult> GetCity()
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // get data
            _returnFormat = _appPs.CompanyApp.ReadVCompanyCity();

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("subdistrict")]
        public async Task<IActionResult> GetSubDistrict(String cityId)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // get data
            _returnFormat = _appPs.MasterSubDistrictApp.getSubDistrict(cityId);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("urban")]
        public async Task<IActionResult> GetUrban(String subDistrict)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // get data
            _returnFormat = _appPs.MasterUrbanApp.getUrban(subDistrict);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}
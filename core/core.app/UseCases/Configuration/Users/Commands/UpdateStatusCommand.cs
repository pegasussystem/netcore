﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Users.Commands
{
    public partial class UpdateStatusCommand : IRequest<ReturnFormat>
    {
        public Int32? userId { get; set; }
        public Int32? isActive { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class UpdateStatusCommandHandler : IRequestHandler<UpdateStatusCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdateStatusCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdateStatusCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.UpdateStatus(req.userId, req.isActive, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_UpdateStatusEntity> UpdateStatus(Int32? userId, Int32? isActive, Int32? loginId)
        {
            String query = $@"EXEC SP_UpdateStatus @userId={userId}, @isActive={isActive}, @loginId={loginId} ";
            var res = this._context.SP_UpdateStatusEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Text;

namespace Core.Entity.Main
{
    public class CoaAccuredExpensesManagementEntity
    {
        public Int64 AccuredExpensesId { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public Int64? coaId { get; set; }
        public Int32? IsActive { get; set; }

    }
}

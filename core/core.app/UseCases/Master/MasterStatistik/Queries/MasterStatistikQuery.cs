﻿using System;
using System.Collections.Generic;
using System.Text;
using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Master.MasterStatistik.Queries
{

    public partial class MasterStatistikQuery : IRequest<ReturnFormat>
    {
        public String? cityId { get; set; }
        public String? dateStart { get; set; }
        public String? dateTo { get; set; }
        //public UiUserProfileEntity uiUserProfileEntity { get; set; } 
    }
    public class MasterStatistikQueryHandler : IRequestHandler<MasterStatistikQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MasterStatistikQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MasterStatistikQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;
            IEnumerable<SP_Statistic_OmsetEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataStatistic(req.cityId, req.dateStart, req.dateTo);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                         s.row_number
                         , s.month_code
                         , s.years
                         , s.month_year
                         , s.count_spb
                         , s.sum_caw
                         , s.sum_price
                     }).OrderBy(o => o.row_number);
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_Statistic_OmsetEntity> DataStatistic(String? cityId, String? dateStart, String? dateTo)
        {
            String query = $@"EXEC SP_Statistic_Omset @startDate='{dateStart}', @endDate='{dateTo}', @city='{cityId}' ";
            var res = this._context.SP_Statistic_OmsetEntitys.FromSqlRaw(query).ToList();
            return res;
        }

        // public IEnumerable<SP_Statistic_OmsetEntity> DataStatistic(String? cityId, String? dateStart, String? dateTo)
        //{
        //    try {
        //        String query = $@"EXEC SP_Statistic_Omset @startDate='{dateStart}', @endDate='{dateTo}', @city='{cityId}' ";
        //        var res = this._context.SP_Statistic_OmsetEntitys.FromSqlRaw(query).ToList();
        //    } catch (Exception e) {
        //        var err = e.Message.ToString();
        //    }
        //    return null;
        //}


    }

}
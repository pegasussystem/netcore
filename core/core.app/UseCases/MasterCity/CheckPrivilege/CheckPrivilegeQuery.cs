﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Ui;


namespace core.app.UseCases.MasterCity.Queries

{
    public partial class CheckPrivilegeQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class CheckPrivilegeQueryHandler : IRequestHandler<CheckPrivilegeQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public CheckPrivilegeQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(CheckPrivilegeQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.PrivilegeUserEntitys
                        join b in _context.PrivilegeActionEntitys on a.PrivilegeId equals b.PrivilegeId
                        join c in _context.MenuActionEntitys on b.MenuActionId equals c.MenuActionId
                        join d in _context.MenuDataEntitys on c.MenuId equals d.MenuId
                        where d.MenuKey.Equals("master-city")
                        && c.MenuAction.Equals("list")
                        && a.UserId.Equals(req.uiUserProfileEntity.userId)


                        select new
                        {
                            a.UserId,
                            a.PrivilegeId
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.PrivilegeId
                        ,
                        s.UserId

                    });
            }

            // return 
            return rtn;
        }


    }
}

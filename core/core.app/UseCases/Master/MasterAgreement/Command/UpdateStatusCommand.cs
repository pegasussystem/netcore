﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Master.MasterAgreement.Command
{
    public partial class UpdateStatusCommand : IRequest<ReturnFormat>
    {
        public Int32? isAgree { get; set; }
        public Int32? subBranchId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class UpdateStatusCommandHandler : IRequestHandler<UpdateStatusCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdateStatusCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdateStatusCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                var entity = await _context.SubBranchEntitys.FindAsync(req.subBranchId);

                if (entity == null)
                {
                    return rtn;
                }

                //
                // logic
                entity.IsAgree      = req.isAgree;
                entity.UpdatedBy    = req.uiUserProfile.userId.ToString();
                entity.UpdatedAt    = DateTime.UtcNow;

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }
    }
}

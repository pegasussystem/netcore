﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Core.Entity.Main.Store_Procedure;

namespace core.app.UseCases.Configuration.Users.Commands
{
    public partial class CreatePrivilegeRoleCommand : IRequest<ReturnFormat>
    {
        public Int32? userId { get; set; }
        public Int32? privilegeId { get; set; }
        public Int32? rolesId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreatePrivilegeRoleCommandHandler : IRequestHandler<CreatePrivilegeRoleCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreatePrivilegeRoleCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreatePrivilegeRoleCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.ManageData(req.userId, req.privilegeId, req.rolesId, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_PrivilegeRoleManageDataEntity> ManageData(Int32? userId, Int32? privilegeId, Int32? rolesId, Int32? loginId)
        {
            String query = $@"EXEC SP_PrivilegeRoleManageData @userId={userId}, @privilegeId={privilegeId}, @rolesId={rolesId}, @loginId={loginId} ";
            var res = this._context.SP_PrivilegeRoleManageDataEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

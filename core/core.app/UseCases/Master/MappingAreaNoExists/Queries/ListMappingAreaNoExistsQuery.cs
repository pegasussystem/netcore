﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Master.MappingAreaNoExists.Queries
{
   public partial class ListMappingAreaNoExistsQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListMappingAreaNoExistsQueryHandler : IRequestHandler<ListMappingAreaNoExistsQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListMappingAreaNoExistsQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListMappingAreaNoExistsQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.MappingAreaNoExistsEntitys
                        join b in _context.VCompanyCityEntities on a.AreaId equals b.CityId
                        join c in _context.VCompanyCityEntities on a.CityId equals c.CityId
                        join d in _context.VCompanyAreaEntitys on a.SubDistrictId equals d.MasterSubDistrictId
                        select new
                        {
                            a.MappingAreaNoExistsId
                            , areaName = b.CompanyCityNameCustom
                            , cityName = c.CompanyCityNameCustom
                            , subDistrictName = d.CompanyAreaNameCustom
    };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.MappingAreaNoExistsId
                        , s.areaName
                        , s.cityName
                        , s.subDistrictName
                    }).OrderBy(o => o.MappingAreaNoExistsId);
            }

            // return
            return rtn;
        }
    }
}
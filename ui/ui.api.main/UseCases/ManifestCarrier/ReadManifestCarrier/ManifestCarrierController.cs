﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ui.api.main.UseCases.ManifestCarrier.ReadManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ManifestCarrierController : DefaultController
    {
        private readonly IAppWrapperPs _appPs;
        public ManifestCarrierController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpGet("read")]
        public async Task<IActionResult> Execute()
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();

            // logic
            // ///// ///// /////
            _returnFormat = _appPs.ReadManifestCarrierUseCase.Read(GetUserClaim());

            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}
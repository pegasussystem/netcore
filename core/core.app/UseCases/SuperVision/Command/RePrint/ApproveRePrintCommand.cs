﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;


namespace core.app.UseCases.SuperVision.Command.RePrint
{
    public partial class ApproveRePrintCommand : IRequest<ReturnFormat>
    {
        public Int32? superVisionId { get; set; }
        public Int64? docId { get; set; }
        public Int32? userCreateId { get; set; }
        public String? docNo { get; set; }
        public String? typePrint { get; set; }
        public String? reason { get; set; }
        public String? reasonReject { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ApproveRePrintCommandHandler : IRequestHandler<ApproveRePrintCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ApproveRePrintCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ApproveRePrintCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                var entity = _context.SuperVisionEntitys.Where(w => w.SuperVisionId.Equals(req.superVisionId)).FirstOrDefault();

                if (entity == null)
                {
                    return rtn;
                }

                entity.ReasonReject = req.reasonReject;
                entity.ApproveBy = req.uiUserProfile.userId;
                entity.ApproveDate = DateTime.UtcNow;
                entity.Status = 1;
                entity.UpdatedBy = req.uiUserProfile.userId.ToString();
                entity.UpdatedAt = DateTime.UtcNow;

                await _context.SaveChangesAsync(cancellationToken);

                var r = this.SetRePrint(req.docId, req.typePrint, req.uiUserProfile.userId, req.userCreateId).ToList();
                await _context.SaveChangesAsync(cancellationToken);
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = r;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_SuperVisionRePrintEntity> SetRePrint(Int64? docId, String? typePrint, Int32? userId, Int32? userCreateId)
        {
            String query = $@"EXEC SP_SuperVision_RePrint @docId={docId}, @typePrint='{typePrint}', @userId='{userId}', @userCreateId='{userCreateId}' ";
            var res = this._context.SP_SuperVisionRePrintEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

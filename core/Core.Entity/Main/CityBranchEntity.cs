using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Text;

namespace Core.Entity.Main
{
    public class CityBranchEntity
    {
        
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public Int64 CityBranchId { get; set; }
        public String? CityId { get; set; }
        public String? BranchCode { get; set; }
        public String? BranchName { get; set; }
        public bool? IsCorporate { get; set; } // nullable bit
        public bool? IsActive { get; set; } // nullable bit
        

    }
}

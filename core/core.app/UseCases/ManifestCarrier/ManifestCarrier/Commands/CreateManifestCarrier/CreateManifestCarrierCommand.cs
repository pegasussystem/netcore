﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace core.app.UseCases.ManifestCarrier.Commands.CreateManifestCarrier
{
    public partial class CreateManifestCarrierCommand : IRequest<ReturnFormat>
    {
        public String? manifestCarrierNo { get; set; }

        public Int32? startDateUtcYear { get; set; }
        public Int32? startDateUtcDay { get; set; }
        public Int32? startDateUtcHour { get; set; }
        public Int32? startDateUtcMonth { get; set; }
        public Int32? startDateUtcMinute { get; set; }
        public Int32? startDateUtcSecond { get; set; }
        public Int32? startDateUtcMillisecond { get; set; }
        public Int32? endDateUtcYear { get; set; }
        public Int32? endDateUtcDay { get; set; }
        public Int32? endDateUtcHour { get; set; }
        public Int32? endDateUtcMonth { get; set; }
        public Int32? endDateUtcMinute { get; set; }
        public Int32? endDateUtcSecond { get; set; }
        public Int32? endDateUtcMillisecond { get; set; }

        public Int32? carrierId { get; set; }

        public Int32? firstCarrierCodeId { get; set; }
        public Int32? firstFlightId { get; set; }
        public Int32? firstVendorTransitId { get; set; }
        public Int32? firstStationTransitId { get; set; }

        public Int32? secondCarrierCodeId { get; set; }
        public Int32? secondFlightId { get; set; }
        public Int32? secondVendorTransitId { get; set; }
        public Int32? secondStationTransitId { get; set; }

        public Int32? thirdCarrierCodeId { get; set; }
        public Int32? thirdFlightId { get; set; }
        public Int32? thirdVendorTransitId { get; set; }
        public Int32? thirdStationTransitId { get; set; }

        public Int32? fourthCarrierCodeId { get; set; }
        public Int32? fourthFlightId { get; set; }

        public Int32? originStationId { get; set; }
        public Int32? destinationStationId { get; set; }

        public Int32? modaId { get; set; }
        public String? noSmu { get; set; }
        public Int32? costSMU { get; set; }

        public Decimal? manifestTotalWeight { get; set; }
        public Int32? ppnId { get; set; }

        public Int32? vendorCarrierId { get; set; }
        public Int32? vendorOriginId { get; set; }

        public DateTime? flightDate { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }

        public Int32? manifestCarrierId { get; set; }
        public Int32? counterCostCarrier { get; set; }
        public Int32? counterOrigin { get; set; }
        public Int32? counterDestination { get; set; }
        public Int32? counterManifest { get; set; }
        public Int32? counterApproval { get; set; }
        public Int32? counterPrivilegeApproval { get; set; }

        public String? reasonKoli { get; set; }
        public String? reasonManifest { get; set; }
        public String? reasonSMU { get; set; }
        
        public Int32? approvedUserId { get; set; }
        public String? approvedUser { get; set; }
        public String? approvedPassword { get; set; }
        public Int32? revise { get; set; }
        public String? statRevise { get; set; }

        public DateTime StartDateUtc()
        {
            string dateString = $"{startDateUtcYear}-{(startDateUtcMonth < 10 ? "0" : "")}{startDateUtcMonth}-{(startDateUtcDay < 10 ? "0" : "")}{startDateUtcDay} {(startDateUtcHour < 10 ? "0" : "")}{startDateUtcHour}:{(startDateUtcMinute < 10 ? "0" : "")}{startDateUtcMinute}:{(startDateUtcSecond < 10 ? "0" : "")}{startDateUtcSecond}.000";
            string format = "yyyy-MM-dd HH:mm:ss.fff";
            DateTime dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
            dateTime = dateTime.AddMilliseconds((double)startDateUtcMillisecond);
            return dateTime;
        }

        public DateTime EndDateUtc()
        {
            string dateString = $"{endDateUtcYear}-{(endDateUtcMonth < 10 ? "0" : "")}{endDateUtcMonth}-{(endDateUtcDay < 10 ? "0" : "")}{endDateUtcDay} {(endDateUtcHour < 10 ? "0" : "")}{endDateUtcHour}:{(endDateUtcMinute < 10 ? "0" : "")}{endDateUtcMinute}:{(endDateUtcSecond < 10 ? "0" : "")}{endDateUtcSecond}.000";
            string format = "yyyy-MM-dd HH:mm:ss.fff";
            DateTime dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
            dateTime = dateTime.AddMilliseconds((double)endDateUtcMillisecond);
            return dateTime;
        }
        public List<Manifests>? Manifests { get; set; }
        public List<ManifestKoli>? ManifestKoli { get; set; }
    }
    public class Manifests
    {
        public Int64? manifestId { get; set; }
        public Int32? koli { get; set; }
        public Decimal? aw { get; set; }
        public Decimal? caw { get; set; }
        public Int32? finalKoli { get; set; }
        public Decimal? weightManifest { get; set; }
        public Decimal? weightSmu { get; set; }
        public String? destinationCityId { get; set; }
        public Int32? vendorDestinationId { get; set; }
    }

    public class ManifestKoli
    {
        public Int64? manifestId { get; set; }
        public Int64? spbId { get; set; }
        public Int64? spbGoodsId { get; set; }
        public Int32? koli { get; set; }
    }

    public class CreateManifestCarrierCommandHandler : IRequestHandler<CreateManifestCarrierCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateManifestCarrierCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateManifestCarrierCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;

            List<ManifestCarrierItemEntity> saveCarrierDetail = new List<ManifestCarrierItemEntity>();

            //----------------------------get last flight----------------------------------------

            Int32? last_flight = 0;

            if (req.fourthFlightId != null)
            {
                last_flight = req.fourthFlightId;
            }
            else if (req.thirdFlightId != null)
            {
                last_flight = req.thirdFlightId;
            }
            else if (req.secondFlightId != null)
            {
                last_flight = req.secondFlightId;
            }
            else
            {
                last_flight = req.firstFlightId;
            }

            //cek exists di cost carrier
            var q1 = from cc in _context.CostCarrierEntitys
                     join ccd in _context.CostCarrierDetailEntitys on cc.CostCarrierId equals ccd.CostCarrierId
                     join vd in _context.VendorCarrierEntitys on cc.VendorCarrierId equals vd.VendorCarrierId
                     where cc.CarrierId.Equals(req.carrierId)
                        && cc.FirstFlightId.Equals(req.firstFlightId)
                        && cc.LastFlightId.Equals(last_flight)
                        && vd.VendorId.Equals(req.vendorCarrierId)
                     select cc;

            req.counterCostCarrier = q1.Count();
           

            if (q1.Count() == 0)
            {
                rtn.Data    = req;
                rtn.Status  = StatusCodes.Status200OK;
                return rtn;
            }

            //cek exists di cost origin
            var q2 = from co in _context.CostOriginEntitys
                     join cd in _context.CostOriginDetailEntitys on co.CostOriginId equals cd.CostOriginId
                     join vo in _context.VendorOriginEntitys on co.VendorOriginId equals vo.VendorOriginId
                     where co.StationOriginId.Equals(req.originStationId)
                        && co.CarrierId.Equals(req.carrierId)
                        && vo.VendorId.Equals(req.vendorOriginId)
                     select co;

            req.counterOrigin = q2.Count();

            if (q2.Count() == 0)
            {
                rtn.Data    = req;
                rtn.Status  = StatusCodes.Status200OK;
                return rtn;
            }


            req.counterManifest = req.Manifests.Count();

            if (req.Manifests.Count() == 0)
            {
                rtn.Data = req;
                rtn.Status = StatusCodes.Status200OK;
                return rtn;
            }


            //cek exists di cost destination
            foreach (var item in req.Manifests)
            {
                var q3 = from cd in _context.CostDestinationEntitys
                         join cdd in _context.CostDestinationDetailEntitys on cd.CostDestinationId equals cdd.CostDestinationId
                         join vd in _context.VendorDestinationEntitys on cd.VendorDestinationId equals vd.VendorDestinationId
                         where cd.CityDestinationId.Equals(item.destinationCityId)
                            && cd.CarrierId.Equals(req.carrierId)
                            && vd.VendorId.Equals(item.vendorDestinationId)
                         select cd;


                req.counterDestination = q3.Count();

                if (q3.Count() == 0)
                {
                    rtn.Data = req;
                    rtn.Status = StatusCodes.Status200OK;
                    return rtn;
                }
            }

            //cek user approval
            if (req.approvedUser != null && req.approvedUser != "") {
                var ca = from us in _context.UserEntitys
                         where us.Username.Equals(req.approvedUser) && us.Password.Equals(req.approvedPassword)
                         select us;

                req.counterApproval = ca.Count();
                if (ca.Count() == 0)
                {
                    rtn.Data = req;
                    rtn.Status = StatusCodes.Status200OK;
                    return rtn;
                }
                else {
                    req.approvedUserId = ca.FirstOrDefault().UserId;

                    var ap = from pu in _context.V_ManifestCarrierApprovalEntitys
                             where pu.UserId.Equals(req.approvedUserId)
                             select pu;

                    req.counterPrivilegeApproval = ap.Count();
                    if (ap.Count() == 0)
                    {
                        rtn.Data = req;
                        rtn.Status = StatusCodes.Status200OK;
                        return rtn;
                    }
                }
            }
            

            // use transaction
            // --- --- ---
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                // save and create data
                // --- --- ---
                ManifestCarrierEntity saveCarrier = this.Create_ManifestCarrier(req);
                if (saveCarrier.ManifestCarrierId == null) { return rtn; }

                req.manifestCarrierId = saveCarrier.ManifestCarrierId;

                // create all manifest to table ManifestCarrierItem
                // --- --- ---
                saveCarrierDetail = this.Create_ManifestCarrierItem(req);

                // create all manifest to table ManifestCarrierItemDetail
                // --- --- ---
                this.Create_ManifestCarrierItemDetail(req);

                // create cost carrier to table ManifestCarrierCost
                // --- --- ---
                var result = this.Create_CostCarrier_ManifestCarrierCost(req, saveCarrierDetail);

                this.SetHistoryAction(req.uiUserProfile.userId, saveCarrier.ManifestCarrierId, "Create Manifest Carrier", "Manifest Carrier", "");

                transaction.Complete();

                //return
                if (req.manifestCarrierId != null)
                {
                    rtn.Data = req;
                    rtn.Status = StatusCodes.Status200OK;
                }
            }


            return rtn;
        }

        private ManifestCarrierEntity Create_ManifestCarrier(CreateManifestCarrierCommand req)
        {
            // set new object
            // --- --- ---
            var entity = new ManifestCarrierEntity();

            entity.CreatedAt                    = DateTime.UtcNow;
            entity.CreatedBy                    = req.uiUserProfile.userId;

            entity.OriginStationId              = req.originStationId;
            entity.DestinationStationId         = req.destinationStationId;
            entity.NoSmu                        = req.noSmu;

            entity.FlightDate                   = req.flightDate;
            //entity.FromSendDate                 = req.StartDateUtc();
            //entity.ToSendDate                   = req.EndDateUtc();

            entity.ModaId                       = req.modaId;
            entity.CarrierId                    = req.carrierId;
            entity.VendorCarrierId              = req.vendorCarrierId;
            entity.VendorOriginId               = req.vendorOriginId;

            entity.FirstFlightCodeId            = req.firstCarrierCodeId;
            entity.FirstFlightNoId              = req.firstFlightId;
            entity.FirstStationTransitId        = req.firstStationTransitId;
            entity.FirstVendorTransitId         = req.firstVendorTransitId;

            entity.SecondFlightCodeId           = req.secondCarrierCodeId;
            entity.SecondFlightNoId             = req.secondFlightId;
            entity.SecondStationTransitId       = req.secondStationTransitId;
            entity.SecondVendorTransitId        = req.secondVendorTransitId;

            entity.ThirdFlightCodeId            = req.thirdCarrierCodeId;
            entity.ThirdFlightNoId              = req.thirdFlightId;
            entity.ThirdStationTransitId        = req.thirdStationTransitId;
            entity.ThirdVendorTransitId         = req.thirdVendorTransitId;

            entity.FourthFlightCodeId           = req.fourthCarrierCodeId;
            entity.FourthFlightNoId             = req.fourthFlightId;

            entity.TotalFinalWeightSMU          = req.manifestTotalWeight;

            entity.ManifestCarrierNo            = this.GetManifestCarrierNumber(req.uiUserProfile.userId, req.revise, req.manifestCarrierNo).FirstOrDefault().ManifestCarrierNo;

            entity.ReasonKoli                   = req.reasonKoli;
            entity.ReasonManifest               = req.reasonManifest;
            entity.ReasonSMU                    = req.reasonSMU;
            entity.ApprovedBy                   = req.approvedUserId;

            if (req.approvedUserId != null && req.approvedUserId != 0) {
                entity.ApprovedDate             = DateTime.Now;
            }

            entity.Revise                       = req.revise;
            entity.StatRevise                   = req.statRevise;

            try
            {
                _context.ManifestCarrierEntitys.Add(entity);
                _context.SaveChanges();

                req.manifestCarrierId = entity.ManifestCarrierId;
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }



            // return
            return entity;
        }

        public IEnumerable<SPManifestCarrierNumberGeneratorEntity> GetManifestCarrierNumber(Int32? USER_ID, Int32? Revise, String? ManifestCarrierNo)
        {
            String query = $@"EXEC ManifestCarrierNumberGenerator @USER_ID = '{ USER_ID }', @Revise = '{ Revise }', @ManifestCarrierNo = '{ ManifestCarrierNo }' ";
            var res = this._context.SPManifestCarrierNumberGeneratorEntitys.FromSqlRaw(query).ToList();
            return res;
        }


        private List<ManifestCarrierItemEntity> Create_ManifestCarrierItem(CreateManifestCarrierCommand req)
        {
            List<ManifestCarrierItemEntity> entity = new List<ManifestCarrierItemEntity>();
            // foreach every manifest from user request
            // --- --- ---
            foreach (var item in req.Manifests)
            {
                var itemEntity = new ManifestCarrierItemEntity();

                itemEntity.CreatedAt            = DateTime.UtcNow;
                itemEntity.CreatedBy            = req.uiUserProfile.userId.ToString();
                itemEntity.ManifestCarrierId    = req.manifestCarrierId;
                itemEntity.ManifestId           = item.manifestId;
                itemEntity.Aw                   = item.aw;
                itemEntity.Caw                  = item.caw;
                itemEntity.Koli                 = item.koli;
                itemEntity.CawFinal             = item.weightManifest;
                itemEntity.KoliFinal            = item.finalKoli;
                itemEntity.FinalWeightSMU       = item.weightSmu;
                itemEntity.DestinationCityId    = item.destinationCityId;
                itemEntity.VendorDestinationId  = item.vendorDestinationId;

                entity.Add(itemEntity);

                //save
                _context.ManifestCarrierItemEntitys.Add(itemEntity);
            }

            //save
            _context.SaveChanges();
            //
            // return
            return entity;
        }

        private List<ManifestCarrierItemDetailEntity> Create_ManifestCarrierItemDetail(CreateManifestCarrierCommand req)
        {
            List<ManifestCarrierItemDetailEntity> entity = new List<ManifestCarrierItemDetailEntity>();
            // foreach every manifest from user request
            // --- --- ---
            foreach (var item in req.ManifestKoli)
            {
                var itemEntity = new ManifestCarrierItemDetailEntity();

                itemEntity.CreatedAt                = DateTime.UtcNow;
                itemEntity.CreatedBy                = req.uiUserProfile.userId.ToString();

                itemEntity.ManifestId               = item.manifestId;
                itemEntity.SpbId                    = item.spbId;
                itemEntity.SpbGoodsId               = item.spbGoodsId;
                itemEntity.Koli                     = item.koli;

                entity.Add(itemEntity);

                //save
                _context.ManifestCarrierItemDetailEntitys.Add(itemEntity);
            }

            //save
            _context.SaveChanges();
            //
            // return
            return entity;
        }

        private List<ManifestCarrierCostEntity> Create_CostCarrier_ManifestCarrierCost(CreateManifestCarrierCommand req, List<ManifestCarrierItemEntity> saveCarrierDetail)
        {
            List<ManifestCarrierCostEntity> entity          = new List<ManifestCarrierCostEntity>();
            List<ManifestCarrierCostSMUEntity> entitySMU    = new List<ManifestCarrierCostSMUEntity>();

            Int32? LastFlightId = 0;

            if (req.fourthFlightId != null)
            {
                LastFlightId = req.fourthFlightId;
            }
            else if (req.thirdFlightId != null)
            {
                LastFlightId = req.thirdFlightId;
            }
            else if (req.secondFlightId != null)
            {
                LastFlightId = req.secondFlightId;
            }
            else
            {
                LastFlightId = req.firstFlightId;
            }

            IEnumerable<V_CostCarrier_Entity> CostCarrierData               = this.V_CostCarrierData(req.originStationId, req.carrierId, req.firstFlightId, LastFlightId, req.vendorCarrierId, req.destinationStationId);
            IEnumerable<V_CostCarrier_Entity> CostCarrierDataPPN1Percent    = this.V_CostCarrierDataPPN1Percent();
            IEnumerable<V_CostCarrier_Entity> CostCarrierDataPPN10Percent   = this.V_CostCarrierDataPPN10Percent();
            IEnumerable<V_CostCarrier_Entity> CostCarrierDataSMU            = this.V_CostCarrierDataSMU();

            IEnumerable<V_CostOrigin_Entity>  CostOriginData                = this.V_CostOriginData(req.originStationId, req.carrierId, req.vendorOriginId);

            IEnumerable<V_CostTransit_Entity> CostFirstTransitData          = this.V_CostTransitData(req.firstStationTransitId, req.carrierId, req.firstVendorTransitId);
            IEnumerable<V_CostTransit_Entity> CostSecondTransitData         = this.V_CostTransitData(req.secondStationTransitId, req.carrierId, req.secondVendorTransitId);
            IEnumerable<V_CostTransit_Entity> CostThirdTransitData          = this.V_CostTransitData(req.thirdStationTransitId, req.carrierId, req.thirdVendorTransitId);

            IEnumerable<V_Cost_InlandOrigin_Entity> CostInlandOriginData    = this.V_CostInlandOriginData();


            //save cost per smu
            this.SaveCostCarrierSMU(req, CostCarrierData, CostCarrierDataPPN1Percent, CostCarrierDataPPN10Percent, CostCarrierDataSMU, entitySMU);
            this.SaveCostTransit(req, CostFirstTransitData, CostSecondTransitData, CostThirdTransitData, entitySMU);
            this.SaveCostOrigin(req, CostOriginData, entitySMU);
            this.SaveCostInlandOrigin(req, CostInlandOriginData, entitySMU);

            //save cost per manifest
            foreach (var item in saveCarrierDetail)
            {
                IEnumerable<V_CostDestination_Entity> CostDestinationData = this.V_CostDestinationData(item.DestinationCityId, req.carrierId, item.VendorDestinationId, req.vendorOriginId);
                IEnumerable<V_CostPenerus_Entity> CostPenerusData = this.V_CostPenerusData(req.destinationStationId, item.DestinationCityId, item.VendorDestinationId);
                IEnumerable<V_CostGerai_Entity> CostGeraiData = this.V_CostGeraiData(item.ManifestId);


                // filter cost carrier
                // --- --- ---
                this.SaveCostDestination(req, item, CostDestinationData, entity);
                this.SaveCostPenerus(req, item, CostPenerusData, entity);
                this.SaveCostGerai(req, item, CostGeraiData, entity);

            }



            //save
            _context.SaveChanges();
            //
            // return
            return entity;
        }

        public void SaveCostInlandOrigin(CreateManifestCarrierCommand req, IEnumerable<V_Cost_InlandOrigin_Entity> CostInlandOriginData, List<ManifestCarrierCostSMUEntity> entity)
        {
            foreach (var cost_inland in CostInlandOriginData)
            {
                ManifestCarrierCostSMUEntity ItemEntity = new ManifestCarrierCostSMUEntity();
                ItemEntity.CreatedAt            = DateTime.UtcNow;
                ItemEntity.CreatedBy            = req.uiUserProfile.userId.ToString();
                ItemEntity.ManifestCarrierId    = req.manifestCarrierId;
                ItemEntity.CostType             = cost_inland.CostType;
                ItemEntity.BaseCost             = cost_inland.BaseCost;
                ItemEntity.Unit                 = cost_inland.Unit;
                ItemEntity.CostGroup            = cost_inland.CostGroup;
                ItemEntity.UseWeight            = cost_inland.UseWeight;
                ItemEntity.FinalWeight          = 1;
                ItemEntity.TotalCost            = ItemEntity.BaseCost * ItemEntity.FinalWeight;

                entity.Add(ItemEntity);
                _context.ManifestCarrierCostSMUEntitys.Add(ItemEntity);
            }
        }

        public IEnumerable<V_CostCarrier_Entity> V_CostCarrierData(Int32? originStationId, Int32? carrierId, Int32? firstFlightId, Int32? LastFlightId, Int32? vendorCarrierId, Int32? destinationStationId)
        {
            // create query
            // --- --- ---
            IQueryable<V_CostCarrier_Entity> result =
                from a in _context.CostCarrierEntitys
                join b in _context.CostCarrierDetailEntitys on a.CostCarrierId equals b.CostCarrierId
                join c in _context.VendorCarrierEntitys on a.VendorCarrierId equals c.VendorCarrierId
                where a.OriginStationId.Equals(originStationId) && a.DestinationStationId.Equals(destinationStationId) &&
                a.CarrierId.Equals(carrierId) && a.FirstFlightId.Equals(firstFlightId) && a.LastFlightId.Equals(LastFlightId) && c.VendorId.Equals(vendorCarrierId)

                select new V_CostCarrier_Entity
                {
                    CostCarrierId = a.CostCarrierId
                    , CarrierId = a.CarrierId
                    , FirstFlightId = a.FirstFlightId
                    , LastFlightId = a.LastFlightId
                    , VendorCarrierId = a.VendorCarrierId
                    , CostCarrierDetailId = b.CostCarrierDetailId
                    , CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }
        public IEnumerable<V_CostOrigin_Entity> V_CostOriginData(Int32? originStationId, Int32? carrierId, Int32? vendorOriginId)
        {
            // create query
            // --- --- ---
            IQueryable<V_CostOrigin_Entity> result =
                from a in _context.CostOriginEntitys
                join b in _context.CostOriginDetailEntitys on a.CostOriginId equals b.CostOriginId
                join c in _context.VendorOriginEntitys on a.VendorOriginId equals c.VendorOriginId
                where a.StationOriginId.Equals(originStationId) && a.CarrierId.Equals(carrierId) && c.VendorId.Equals(vendorOriginId)

                select new V_CostOrigin_Entity
                {
                    CostOriginId = a.CostOriginId
                    , StationOriginId = a.StationOriginId
                    , CarrierId = a.CarrierId
                    , VendorOriginId = a.VendorOriginId
                    , CostOriginDetailId = b.CostOriginDetailId
                    , CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }
        public IEnumerable<V_CostTransit_Entity> V_CostTransitData(Int32? StationTransitId, Int32? carrierId, Int32? VendorTransitId)
        {
            // create query
            // --- --- ---
            IQueryable<V_CostTransit_Entity> result =
                from a in _context.CostTransitEntitys
                join b in _context.CostTransitDetailEntitys on a.CostTransitId equals b.CostTransitId
                join c in _context.VendorTransitEntitys on a.VendorTransitId equals c.VendorTransitId
                where a.StationTransitId.Equals(StationTransitId) && a.CarrierId.Equals(carrierId) && c.VendorId.Equals(VendorTransitId)

                select new V_CostTransit_Entity
                {
                    CostTransitId = a.CostTransitId
                    , StationTransitId = a.StationTransitId
                    , CarrierId = a.CarrierId
                    , VendorTransitId = a.VendorTransitId
                    , CostTransitDetailId = b.CostTransitDetailId
                    , CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }
        public IEnumerable<V_CostDestination_Entity> V_CostDestinationData(String? cityDestinationId, Int32? carrierId, Int32? vendorDestinationId, Int32? vendorOriginId)
        {
            // create query
            // --- --- ---
            IQueryable<V_CostDestination_Entity> result =
                from a in _context.CostDestinationEntitys
                join b in _context.CostDestinationDetailEntitys on a.CostDestinationId equals b.CostDestinationId
                join c in _context.VendorOriginEntitys on a.VendorOriginId equals c.VendorOriginId
                join d in _context.VendorDestinationEntitys on a.VendorDestinationId equals d.VendorDestinationId
                where a.CityDestinationId.Equals(cityDestinationId) && a.CarrierId.Equals(carrierId) && d.VendorId.Equals(vendorDestinationId) && c.VendorId.Equals(vendorOriginId)

                select new V_CostDestination_Entity
                {
                    CostDestinationId = a.CostDestinationId
                    , CityDestinationId = a.CityDestinationId
                    , CarrierId = a.CarrierId
                    , VendorDestinationId = a.VendorDestinationId
                    , CostDestinationDetailId = b.CostDestinationDetailId
                    , CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }
        public IEnumerable<V_CostGerai_Entity> V_CostGeraiData(Int64? manifestId)
        {
            var CityId = GetManifestCityId(manifestId);
            // create query
            // --- --- ---
            IQueryable<V_CostGerai_Entity> result =
                from a in _context.CostGeraiEntitys
                join b in _context.CostGeraiDetailEntitys on a.CostGeraiId equals b.CostGeraiId

                where a.ManifestOriginCityId.Equals(CityId)

                select new V_CostGerai_Entity
                {
                    CostGeraiId = a.CostGeraiId
                    , ManifestOriginCityId = a.ManifestOriginCityId
                    , CostGeraiDetailId = b.CostGeraiDetailId
                    , CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                    , Desc = CityId
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }
        public IEnumerable<V_CostPenerus_Entity> V_CostPenerusData(Int32? DestinationStationId, String? cityDestinationId, Int32? vendorDestinationId)
        {
            // create query
            // --- --- ---
            IQueryable<V_CostPenerus_Entity> result =
                from a in _context.CostPenerusEntitys
                join b in _context.CostPenerusDetailEntitys on a.CostPenerusId equals b.CostPenerusId
                join c in _context.VendorDestinationEntitys on a.VendorDestinationId equals c.VendorDestinationId
                where a.StationDestinationId.Equals(DestinationStationId) && a.CityDestinationId.Equals(cityDestinationId) && c.VendorId.Equals(vendorDestinationId)

                select new V_CostPenerus_Entity
                {
                    CostPenerusId = a.CostPenerusId
                    , StationDestinationId = a.StationDestinationId
                    , CityDestinationId = a.CityDestinationId
                    , VendorDestinationId = a.VendorDestinationId
                    , CostPenerusDetailId = b.CostPenerusDetailId
                    , CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }

        public IEnumerable<V_CostCarrier_Entity> V_CostCarrierDataPPN1Percent()
        {
            // create query
            // --- --- ---
            IQueryable<V_CostCarrier_Entity> result =
                from b in _context.CostCarrierDetailPpnEntitys
                where b.CostType.Equals("cost_carrier_ppn_1%")
                select new V_CostCarrier_Entity
                {
                    CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }
        public IEnumerable<V_CostCarrier_Entity> V_CostCarrierDataPPN10Percent()
        {
            // create query
            // --- --- ---
            IQueryable<V_CostCarrier_Entity> result =
                from b in _context.CostCarrierDetailPpnEntitys
                where b.CostType.Equals("cost_carrier_ppn_10%")
                select new V_CostCarrier_Entity
                {
                    CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }
        public IEnumerable<V_CostCarrier_Entity> V_CostCarrierDataSMU()
        {
            // create query
            // --- --- ---
            IQueryable<V_CostCarrier_Entity> result =
                from b in _context.CostCarrierDetailSmuEntitys
                select new V_CostCarrier_Entity
                {
                    CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }

        public IEnumerable<V_Cost_InlandOrigin_Entity> V_CostInlandOriginData()
        {
            // create query
            // --- --- ---
            IQueryable<V_Cost_InlandOrigin_Entity> result =
                from a in _context.CostInlandOriginEntitys
                join b in _context.CostInlandOriginDetailEntitys on a.CostInlandOriginId equals b.CostInlandOriginId
                select new V_Cost_InlandOrigin_Entity
                {
                    CostType = b.CostType
                    , Unit = b.Unit
                    , BaseCost = b.BaseCost
                    , CostGroup = b.CostGroup
                    , UseWeight = b.UseWeight
                };

            // return result as list
            // --- --- ---
            return result.ToList();
        }

        public void SaveCostCarrierSMU(CreateManifestCarrierCommand req, IEnumerable<V_CostCarrier_Entity> CostCarrierData, IEnumerable<V_CostCarrier_Entity> CostCarrierDataPPN1Percent, IEnumerable<V_CostCarrier_Entity> CostCarrierDataPPN10Percent, IEnumerable<V_CostCarrier_Entity> CostCarrierDataSMU, List<ManifestCarrierCostSMUEntity> entity)
        {
            var ppnKey = GetPpnKey(req.ppnId);

            // foreach data from table CostCarrier
            // --- --- ---
            foreach (var cost_item in CostCarrierData)
            {
                ManifestCarrierCostSMUEntity ItemEntity = new ManifestCarrierCostSMUEntity();
                ItemEntity.CreatedAt = DateTime.UtcNow;
                ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                ItemEntity.ManifestCarrierId = req.manifestCarrierId;
                ItemEntity.CostType = cost_item.CostType;
                ItemEntity.BaseCost = cost_item.BaseCost;
                ItemEntity.Unit = cost_item.Unit;
                ItemEntity.CostGroup = cost_item.CostGroup;
                ItemEntity.UseWeight = cost_item.UseWeight;
                ItemEntity.FinalWeight = req.manifestTotalWeight;
                ItemEntity.TotalCost = ItemEntity.BaseCost * req.manifestTotalWeight;

                if (req.costSMU != null || req.costSMU != 0)
                {
                    this.CaclSMU(req, CostCarrierDataSMU, entity);
                }

                // get cost value for get ppn value
                if (ppnKey == "cost_carrier_ppn_1%")
                {
                    this.CaclCarrier1Percent(req, CostCarrierDataPPN1Percent, entity, ItemEntity);
                }
                if (ppnKey == "cost_carrier_ppn_10%")
                {
                    this.CaclCarrier10Percent(req, CostCarrierDataPPN10Percent, entity, ItemEntity);
                }


                entity.Add(ItemEntity);
                _context.ManifestCarrierCostSMUEntitys.Add(ItemEntity);
            }
            //// save data per manifest
        }

        public void SaveCostOrigin(CreateManifestCarrierCommand req, IEnumerable<V_CostOrigin_Entity> CostOriginData, List<ManifestCarrierCostSMUEntity> entity)
        {

            // foreach data from table CostCarrier
            // --- --- ---
            foreach (var cost_item in CostOriginData)
            {
                ManifestCarrierCostSMUEntity ItemEntity = new ManifestCarrierCostSMUEntity();
                ItemEntity.CreatedAt = DateTime.UtcNow;
                ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                ItemEntity.ManifestCarrierId = req.manifestCarrierId;
                ItemEntity.CostType = cost_item.CostType;
                ItemEntity.BaseCost = cost_item.BaseCost;
                ItemEntity.Unit = cost_item.Unit;
                ItemEntity.CostGroup = cost_item.CostGroup;
                ItemEntity.UseWeight = cost_item.UseWeight;
                ItemEntity.FinalWeight = req.manifestTotalWeight;
                ItemEntity.TotalCost = ItemEntity.BaseCost * req.manifestTotalWeight;

                entity.Add(ItemEntity);
                _context.ManifestCarrierCostSMUEntitys.Add(ItemEntity);
            }
            //// save data per manifest
        }

        public void SaveCostTransit(CreateManifestCarrierCommand req, IEnumerable<V_CostTransit_Entity> CostFirstTransitData, IEnumerable<V_CostTransit_Entity> CostSecondTransitData, IEnumerable<V_CostTransit_Entity> CostThirdTransitData, List<ManifestCarrierCostSMUEntity> entity)
        {
            if (req.firstStationTransitId != null || req.firstStationTransitId != 0)
            {

                foreach (var first_item in CostFirstTransitData)
                {
                    ManifestCarrierCostSMUEntity ItemEntity = new ManifestCarrierCostSMUEntity();
                    ItemEntity.CreatedAt = DateTime.UtcNow;
                    ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                    ItemEntity.ManifestCarrierId = req.manifestCarrierId;
                    ItemEntity.CostType = first_item.CostType;
                    ItemEntity.BaseCost = first_item.BaseCost;
                    ItemEntity.Unit = first_item.Unit;
                    ItemEntity.CostGroup = first_item.CostGroup;
                    ItemEntity.UseWeight = first_item.UseWeight;
                    ItemEntity.FinalWeight = req.manifestTotalWeight;
                    ItemEntity.TotalCost = ItemEntity.BaseCost * req.manifestTotalWeight;
                    ItemEntity.Key = req.firstStationTransitId;

                    entity.Add(ItemEntity);
                    _context.ManifestCarrierCostSMUEntitys.Add(ItemEntity);
                }

            }

            if (req.secondStationTransitId != null || req.secondStationTransitId != 0)
            {

                foreach (var second_item in CostSecondTransitData)
                {
                    ManifestCarrierCostSMUEntity ItemEntity = new ManifestCarrierCostSMUEntity();
                    ItemEntity.CreatedAt = DateTime.UtcNow;
                    ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                    ItemEntity.ManifestCarrierId = req.manifestCarrierId;
                    ItemEntity.CostType = second_item.CostType;
                    ItemEntity.BaseCost = second_item.BaseCost;
                    ItemEntity.Unit = second_item.Unit;
                    ItemEntity.CostGroup = second_item.CostGroup;
                    ItemEntity.UseWeight = second_item.UseWeight;
                    ItemEntity.FinalWeight = req.manifestTotalWeight;
                    ItemEntity.TotalCost = ItemEntity.BaseCost * req.manifestTotalWeight;
                    ItemEntity.Key = req.secondStationTransitId;

                    entity.Add(ItemEntity);
                    _context.ManifestCarrierCostSMUEntitys.Add(ItemEntity);
                }

            }

            if (req.thirdStationTransitId != null || req.thirdStationTransitId != 0)
            {

                foreach (var third_item in CostThirdTransitData)
                {
                    ManifestCarrierCostSMUEntity ItemEntity = new ManifestCarrierCostSMUEntity();
                    ItemEntity.CreatedAt = DateTime.UtcNow;
                    ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                    ItemEntity.ManifestCarrierId = req.manifestCarrierId;
                    ItemEntity.CostType = third_item.CostType;
                    ItemEntity.BaseCost = third_item.BaseCost;
                    ItemEntity.Unit = third_item.Unit;
                    ItemEntity.CostGroup = third_item.CostGroup;
                    ItemEntity.UseWeight = third_item.UseWeight;
                    ItemEntity.FinalWeight = req.manifestTotalWeight;
                    ItemEntity.TotalCost = ItemEntity.BaseCost * req.manifestTotalWeight;
                    ItemEntity.Key = req.thirdStationTransitId;

                    entity.Add(ItemEntity);
                    _context.ManifestCarrierCostSMUEntitys.Add(ItemEntity);
                }

            }

        }

        public void SaveCostDestination(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, IEnumerable<V_CostDestination_Entity> CostDestinationData, List<ManifestCarrierCostEntity> entity)
        {

            // foreach data from table CostCarrier
            // --- --- ---
            foreach (var cost_item in CostDestinationData)
            {
                ManifestCarrierCostEntity ItemEntity = new ManifestCarrierCostEntity();
                ItemEntity.CreatedAt = DateTime.UtcNow;
                ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                ItemEntity.ManifestCarrierId = item.ManifestCarrierId;
                ItemEntity.ManifestCarrierItemId = item.ManifestCarrierItemId;
                ItemEntity.CostType = cost_item.CostType;
                ItemEntity.BaseCost = cost_item.BaseCost;
                ItemEntity.Unit = cost_item.Unit;
                ItemEntity.CostGroup = cost_item.CostGroup;
                ItemEntity.UseWeight = cost_item.UseWeight;
                ItemEntity.FinalWeight = item.CawFinal;
                ItemEntity.TotalCost = ItemEntity.BaseCost * item.CawFinal;

                entity.Add(ItemEntity);
                _context.ManifestCarrierCostEntitys.Add(ItemEntity);
            }
            //// save data per manifest
        }

        public void SaveCostGerai(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, IEnumerable<V_CostGerai_Entity> CostGeraiData, List<ManifestCarrierCostEntity> entity)
        {

            // --- --- ---
            foreach (var cost_item in CostGeraiData)
            {
                ManifestCarrierCostEntity ItemEntity = new ManifestCarrierCostEntity();
                ItemEntity.CreatedAt = DateTime.UtcNow;
                ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                ItemEntity.ManifestCarrierId = item.ManifestCarrierId;
                ItemEntity.ManifestCarrierItemId = item.ManifestCarrierItemId;
                ItemEntity.CostType = cost_item.CostType;
                ItemEntity.BaseCost = cost_item.BaseCost;
                ItemEntity.Unit = cost_item.Unit;
                ItemEntity.CostGroup = cost_item.CostGroup;
                ItemEntity.UseWeight = cost_item.UseWeight;
                ItemEntity.FinalWeight = item.CawFinal;
                ItemEntity.TotalCost = ItemEntity.BaseCost * item.CawFinal;
                ItemEntity.Desc = cost_item.Desc;

                entity.Add(ItemEntity);
                _context.ManifestCarrierCostEntitys.Add(ItemEntity);
            }
            //// save data per manifest
        }

        public void SaveCostPenerus(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, IEnumerable<V_CostPenerus_Entity> CostPenerusData, List<ManifestCarrierCostEntity> entity)
        {

            // foreach data from table CostCarrier
            // --- --- ---
            foreach (var cost_item in CostPenerusData)
            {
                ManifestCarrierCostEntity ItemEntity = new ManifestCarrierCostEntity();
                ItemEntity.CreatedAt = DateTime.UtcNow;
                ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                ItemEntity.ManifestCarrierId = item.ManifestCarrierId;
                ItemEntity.ManifestCarrierItemId = item.ManifestCarrierItemId;
                ItemEntity.CostType = cost_item.CostType;
                ItemEntity.BaseCost = cost_item.BaseCost;
                ItemEntity.Unit = cost_item.Unit;
                ItemEntity.CostGroup = cost_item.CostGroup;
                ItemEntity.UseWeight = cost_item.UseWeight;
                ItemEntity.FinalWeight = item.CawFinal;
                ItemEntity.TotalCost = ItemEntity.BaseCost * item.CawFinal;

                entity.Add(ItemEntity);
                _context.ManifestCarrierCostEntitys.Add(ItemEntity);
            }
            //// save data per manifest
        }

        public void CaclSMU(CreateManifestCarrierCommand req, IEnumerable<V_CostCarrier_Entity> V_CostCarrierDataSMU, List<ManifestCarrierCostSMUEntity> entity)
        {
            foreach (var cost_smu in V_CostCarrierDataSMU)
            {
                ManifestCarrierCostSMUEntity ItemEntity = new ManifestCarrierCostSMUEntity();
                ItemEntity.CreatedAt = DateTime.UtcNow;
                ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                ItemEntity.ManifestCarrierId = req.manifestCarrierId;
                ItemEntity.CostType = cost_smu.CostType;
                ItemEntity.BaseCost = req.costSMU;
                ItemEntity.Unit = cost_smu.Unit;
                ItemEntity.CostGroup = cost_smu.CostGroup;
                ItemEntity.UseWeight = cost_smu.UseWeight;
                ItemEntity.FinalWeight = 1;
                ItemEntity.TotalCost = ItemEntity.BaseCost * ItemEntity.FinalWeight;

                entity.Add(ItemEntity);
                _context.ManifestCarrierCostSMUEntitys.Add(ItemEntity);
            }
        }
        public void CaclCarrier1Percent(CreateManifestCarrierCommand req, IEnumerable<V_CostCarrier_Entity> V_CostCarrierDataPPN1Percent, List<ManifestCarrierCostSMUEntity> entity, ManifestCarrierCostSMUEntity IE)
        {
            foreach (var cost_1percent in V_CostCarrierDataPPN1Percent)
            {
                ManifestCarrierCostSMUEntity ItemEntity = new ManifestCarrierCostSMUEntity();
                ItemEntity.CreatedAt = DateTime.UtcNow;
                ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                ItemEntity.ManifestCarrierId = req.manifestCarrierId;
                ItemEntity.CostType = cost_1percent.CostType;
                ItemEntity.BaseCost = 0;
                ItemEntity.Unit = cost_1percent.Unit;
                ItemEntity.CostGroup = cost_1percent.CostGroup;
                ItemEntity.UseWeight = cost_1percent.UseWeight;
                ItemEntity.FinalWeight = 0;
                ItemEntity.TotalCost = ((req.costSMU + (IE.TotalCost)) * 1) / 100;

                entity.Add(ItemEntity);
                _context.ManifestCarrierCostSMUEntitys.Add(ItemEntity);
            }
        }
        public void CaclCarrier10Percent(CreateManifestCarrierCommand req, IEnumerable<V_CostCarrier_Entity> V_CostCarrierDataPPN10Percent, List<ManifestCarrierCostSMUEntity> entity, ManifestCarrierCostSMUEntity IE)
        {
            foreach (var cost_10percent in V_CostCarrierDataPPN10Percent)
            {
                ManifestCarrierCostSMUEntity ItemEntity = new ManifestCarrierCostSMUEntity();
                ItemEntity.CreatedAt = DateTime.UtcNow;
                ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
                ItemEntity.ManifestCarrierId = req.manifestCarrierId;
                ItemEntity.CostType = cost_10percent.CostType;
                ItemEntity.BaseCost = 0;
                ItemEntity.Unit = cost_10percent.Unit;
                ItemEntity.CostGroup = cost_10percent.CostGroup;
                ItemEntity.UseWeight = cost_10percent.UseWeight;
                ItemEntity.FinalWeight = 0;
                ItemEntity.TotalCost = ((req.costSMU + (IE.TotalCost)) * 10) / 100;

                entity.Add(ItemEntity);
                _context.ManifestCarrierCostSMUEntitys.Add(ItemEntity);
            }
        }


        public String? GetPpnKey(Int32? ppnId)
        {
            var query =
                from a in _context.LookupEntitys
                where a.LookupId.Equals(ppnId)

                select new
                {
                    a.LookupKey
                };

            var result = "";
            foreach (var qr in query)
            {
                result = qr.LookupKey.ToString();
            }
            //
            // return
            return result;
        }
        public String? GetManifestCityId(Int64? manifestId)
        {
            var query =
                from a in _context.V_ManifestListEntitys
                join b in _context.VCompanyCityEntities on a.CityIdCode equals b.CompanyCityId
                where a.ManifestId.Equals(manifestId)

                select new
                {
                    a.CityIdCode
                    ,
                    b.CityId
                };

            var result = "";
            foreach (var qr in query)
            {
                result = qr.CityId.ToString();
            }
            //
            // return
            return result;
        }
        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu = Menu;
            dataItem.Desc = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.CarrierManagement.Create;
using core.helper;
using Core.Entity.Ui;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ui.api.main.UseCases.ManifestCarrier.Create
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ManifestCarrierController : DefaultController
    {
        private readonly IAppWrapperPs _appPs;
        public ManifestCarrierController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpPost("create2")]
        public async Task<IActionResult> Execute([FromBody] CreateManifestCarrierRequest request)
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();

            // logic
            // ///// ///// /////
            _returnFormat = _appPs.CreateManifestCarrierUseCase.Create(GetUserClaim(), request);

            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }



        
    }
}
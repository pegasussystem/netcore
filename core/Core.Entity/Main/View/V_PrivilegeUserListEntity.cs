﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_PrivilegeUserListEntity
    {
        public Int32? UserId { get; set; }
        public String? Username { get; set; }
        public String? FullName { get; set; }
        public Int32? PrivilegeId { get; set; }
        public Int32? RolesId { get; set; }
        public String? Email { get; set; }
        public String? RolesName { get; set; }
        public String? Desc { get; set; }
    }
}

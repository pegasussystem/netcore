﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface ISPSpbGoods_CalculatePriceRepo : IRepo<SPSpbGoods_CalculatePriceEntity>
    {
    }
}

﻿using core.app.UseCases.ManifestCarrier.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ListFourthCarrierCodeController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public ListFourthCarrierCodeController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("read-fourth-carrier-code")]
        public async Task<IActionResult> ListCode([FromQuery] ListFourthCarrierCodeReadQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}
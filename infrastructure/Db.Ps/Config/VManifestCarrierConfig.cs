﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VManifestCarrierConfig : IEntityTypeConfiguration<VManifestCarrierEntity>
    {
        public void Configure(EntityTypeBuilder<VManifestCarrierEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierId);
            builder.ToTable("VManifestCarrier");
        }
    }
}

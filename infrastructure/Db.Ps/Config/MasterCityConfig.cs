﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class MasterCityConfig : IEntityTypeConfiguration<MasterCityEntity>
    {
        public void Configure(EntityTypeBuilder<MasterCityEntity> builder)
        {
            builder.HasKey(e => e.MasterCityId);
            builder.ToTable("MasterCity");

        }
    }
}

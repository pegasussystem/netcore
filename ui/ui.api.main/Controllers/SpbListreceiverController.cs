﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Base;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ui.api.main.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class SpbListReceiverController : ControllerBase
    {

        private readonly IAppWrapperPs _appPs;
        public SpbListReceiverController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        //[HttpGet("list")]
        //public async Task<IActionResult> List()
        //{

        //    //
        //    // variable
        //    ReturnFormat _returnFormat = new ReturnFormat();
        //    UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();


        //    //
        //    // get data
        //    uiUserProfileEntity = GetUserClaim();
        //    _returnFormat = _appPs.SpbListReceiverApp.Read(uiUserProfileEntity);


        //    //
        //    // return
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}

        [HttpPost("bulkprint")]
        public async Task<IActionResult> BulkPrint([FromForm] SpbEntity spbEntity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();


            //
            // get data
            uiUserProfileEntity = GetUserClaim();
            _returnFormat = _appPs.SpbListReceiverApp.BulkPrint(uiUserProfileEntity, spbEntity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpPost("read-spb-print")]
        public async Task<IActionResult> ReadSpbPrint([FromForm]VSpbEntity entity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbListReceiverApp.ReadSpbPrint(GetUserClaim(), entity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        UiUserProfileEntity GetUserClaim() {
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            // get claim user
            var claims = User.Claims.Where(w => w.Type.Equals("userProfile")).First().Value;
            uiUserProfileEntity = JsonConvert.DeserializeObject<UiUserProfileEntity>(claims);

            return uiUserProfileEntity;
        }
    }
}
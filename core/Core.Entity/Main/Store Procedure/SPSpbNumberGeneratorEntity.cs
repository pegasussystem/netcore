﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SPSpbNumberGeneratorEntity
    {
        public String? SpbNo { get; set; }
        public Int64? SpbId { get; set; }
    }
}

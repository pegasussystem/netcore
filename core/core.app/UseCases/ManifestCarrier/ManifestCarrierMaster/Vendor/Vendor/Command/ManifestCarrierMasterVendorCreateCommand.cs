﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.Vendor.Command
{
    public partial class ManifestCarrierMasterVendorCreateCommand : IRequest<ReturnFormat>
    {
        public String? vendorName { get; set; }
        public String? desc { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterVendorCreateCommandHandler : IRequestHandler<ManifestCarrierMasterVendorCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterVendorCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterVendorCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.VendorCreate(req.vendorName, req.desc);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_VendorCreateEntity> VendorCreate(String? vendorName, String? desc)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_VendorCreate @vendorName='{vendorName}', @desc='{desc}'";
            var res = this._context.SP_ManifestCarrierMaster_VendorCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
﻿using System;
using Interface.Other;
using Serilog;

namespace Log.Serilog
{
    public class SerilogManager : ILog
    {
        // TODO: konfigrasi yang beridiri sendiri
        ILogger _logDebug;
        ILogger _logError;
        ILogger _logInfo;
        ILogger _logWarn;

        long _fileSizeLimitBytes = 20_971_520; // 20_971_520 = 20MB

        public SerilogManager(String path, string fileSize)
        {
            _fileSizeLimitBytes = long.Parse(fileSize);
            if (_logDebug == null)
            {
                _logDebug = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                   .WriteTo.Console()
                   .WriteTo.File(path + "/debug/debug-.txt",
                   rollingInterval: RollingInterval.Day,
                   rollOnFileSizeLimit: true,
                   fileSizeLimitBytes: _fileSizeLimitBytes,
                   retainedFileCountLimit: null
                   )
                   .CreateLogger();
            }

            if (_logError == null)
            {
                _logError = new LoggerConfiguration()
                   .WriteTo.Console()
                   .WriteTo.File(path + "/error/error-.txt",
                   rollingInterval: RollingInterval.Day,
                   rollOnFileSizeLimit: true,
                   fileSizeLimitBytes: _fileSizeLimitBytes,
                   retainedFileCountLimit: null
                   )
                   .CreateLogger();
            }

            if (_logInfo == null)
            {
                _logInfo = new LoggerConfiguration()
                   .WriteTo.Console()
                   .WriteTo.File(path + "/info/info-.txt",
                   rollingInterval: RollingInterval.Day,
                   rollOnFileSizeLimit: true,
                   fileSizeLimitBytes: _fileSizeLimitBytes,
                   retainedFileCountLimit: null
                   )
                   .CreateLogger();
            }

            if (_logWarn == null)
            {
                _logWarn = new LoggerConfiguration()
                   .WriteTo.Console()
                   .WriteTo.File(path + "/warning/warning-.txt",
                   rollingInterval: RollingInterval.Day,
                   rollOnFileSizeLimit: true,
                   fileSizeLimitBytes: _fileSizeLimitBytes,
                   retainedFileCountLimit: null
                   )
                   .CreateLogger();
            }

        }

        public void LogDebug(string idLog, string message)
        {
            _logDebug.Debug("{@idLog} | {@message}", idLog, message);
        }

        public void LogError(string idLog, string message)
        {
            _logError.Error("{@idLog} | {@message}", idLog, message);
        }

        public void LogInfo(string idLog, string message)
        {
            throw new NotImplementedException();
        }

        public void LogWarn(string idLog, string message)
        {
            throw new NotImplementedException();
        }
    }
}

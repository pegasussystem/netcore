﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Monitoring.MonitoringBarang.Query
{
    public partial class ListMonitoringBarangQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public String? Type { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class ListMonitoringBarangQueryHandler : IRequestHandler<ListMonitoringBarangQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListMonitoringBarangQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListMonitoringBarangQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_MonitoringBarangEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataMonitoringBarangList(req.uiUserProfile.userId, req.Type, req.CreatedAt.Value.Day, req.CreatedAt.Value.Month, req.CreatedAt.Value.Year, req.uiUserProfile.WorkTimeZoneHour, req.uiUserProfile.WorkTimeZoneMinute);

            if (entities != null)
            {
               rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        s.CompanyId
                        , s.BranchId
                        , s.SubBranchId
                        , s.Carrier
                        , s.ManifestId
                        , s.OriginCityId
                        , s.DestinationCityId
                        , s.Origin
                        , s.BranchOrigin
                        , s.Destination
                        , s.DestinationId
                        , s.BranchDestination
                        , s.Alphabet
                        , s.Koli_1200
                        , s.Aw_1200
                        , s.Caw_1200
                        , s.Koli_1400
                        , s.Aw_1400
                        , s.Caw_1400
                        , s.Koli_1530
                        , s.Aw_1530
                        , s.Caw_1530
                        , s.Koli_1600
                        , s.Aw_1600
                        , s.Caw_1600
                        , s.Koli_Final
                        , s.Aw_Final
                        , s.Caw_Final
                    });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_MonitoringBarangEntity> DataMonitoringBarangList(Int32? UserId, String? Type, Int32? Day, Int32? Month, Int32? Year, Int32? Hour, Int32? Minute)
        {
            String query = $@"EXEC SP_MonitoringBarang @userId = '{UserId}', @type = '{Type}', @day='{Day}', @month = '{Month}', @year = '{Year}', @timeZoneHour = '{Hour}', @timeZoneMinute = '{Minute}' ";
            var res = this._context.SP_MonitoringBarangEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
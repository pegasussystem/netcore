﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class ManifestConfig : IEntityTypeConfiguration<ManifestEntity>
    {
        public void Configure(EntityTypeBuilder<ManifestEntity> builder)
        {
            builder.HasKey(e => e.ManifestId);
            builder.ToTable("Manifest");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Text;

namespace Core.Entity.Main
{
    public class CoaCityRevenueLiabilityEntity
    {
        public Int32 Id { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CityId { get; set; }
        public Int32? CoaIdRevenue { get; set; }
        public Int32? CoaIdLiability { get; set; }
        public Int32? IsCorporate { get; set; }
        public Int32? IsKk1 { get; set; }
        public String? CarrierType { get; set; }
        public String? desc { get; set; }
        public Int64? citybranchid { get; set; }
        public Int64? CoaIdAR { get; set; }
    }
}
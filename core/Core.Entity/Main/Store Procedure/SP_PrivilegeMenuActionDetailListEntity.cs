﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_PrivilegeMenuActionDetailListEntity
    {
        public Int32? MenuActionId { get; set; }
        public Int32? MenuId { get; set; }
        public String? MenuActionKey { get; set; }
        public String? MenuActionName { get; set; }
        public String? Desc { get; set; }
        public Int32? IsActive { get; set; }

    }
}

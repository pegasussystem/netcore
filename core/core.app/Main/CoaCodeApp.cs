using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Base;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace core.app.Main
{
    public class CoaCodeApp : ICoaCodeApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public CoaCodeApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(CoaCodeEntity entity)
        {
            throw new NotImplementedException();
        }

        public CoaCodeEntity Delete(CoaCodeEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }
        
        public ReturnFormat Read(CoaCodeEntity entity)
        {
            throw new NotImplementedException();
        }

        public CoaCodeEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public CoaCodeEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(CoaCodeEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

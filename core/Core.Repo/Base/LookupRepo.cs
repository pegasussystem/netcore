﻿using Core.Entity.Base;
using Db.Ps;
using Interface.Repo.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Base
{
    public class LookupRepo : Repo<LookupEntity>, ILookupRepo
    {
        public LookupRepo(DbContextPs ctx) : base(ctx)
        {
        }

    }
}

﻿using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.app.Common.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Core.Entity.Main.Store_Procedure;
using core.app.UseCases.MasterCity.Commands;
using Core.Entity.Main;

namespace core.app.UseCases.MasterCity.Commands
{
    public partial class MasterCityCommand : IRequest<ReturnFormat>
    {
        public Int32? masterCityId { get; set; }
        public String? masterProvinceId { get; set; }
        public String? masterCityType { get; set; }
        public String? masterCityName { get; set; }
        public String? masterCityCapital { get; set; }
        public String? masterCityCode { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MasterCityCommandHandler : IRequestHandler<MasterCityCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public MasterCityCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(MasterCityCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.MasterCity(req.masterCityId, req.masterProvinceId, req.masterCityType, req.masterCityName, req.masterCityCapital, req.masterCityCode, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_MasterCityCreateEntity> MasterCity(Int32? masterCityId, String? masterProvinceId, String? masterCityType, String? masterCityName, String? masterCityCapital, String? masterCityCode, Int32? userid)
        {
            String query = $@"EXEC SP_MasterCityCreate @privilegeId={masterCityId}, @action='{masterProvinceId}', @id={masterCityType}, @location='{masterCityName}', @location='{masterCityCapital}', @location='{masterCityCode}', @userId={userid} ";
            var res = this._context.SP_MasterCityCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }

}

﻿using core.helper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Base.Query
{
    public partial class DistribusiPrintListQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class DistribusiPrintListQueryHandler : IRequestHandler<DistribusiPrintListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public DistribusiPrintListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(DistribusiPrintListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_PrivilegeDistribusiPrintEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DistribusiPrintList(req.uiUserProfileEntity.userId);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                        s.PrintValue
                    });
            }

            // return
            return rtn;
        }

        public IEnumerable<SP_PrivilegeDistribusiPrintEntity> DistribusiPrintList(Int32? UserId)
        {
            String query = $@"EXEC SP_PrivilegeDistribusiPrint @UserId={UserId}";
            var res = this._context.SP_PrivilegeDistribusiPrintEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }
}

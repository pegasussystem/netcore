﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class MDataCustomerReceiverConfig : IEntityTypeConfiguration<MDataCustomerReceiverEntity>
    {
        public void Configure(EntityTypeBuilder<MDataCustomerReceiverEntity> builder)
        {
            builder.HasKey(e => e.Id);
            builder.ToTable("VMasterDataCustomerReceiver");
        }
    }
}

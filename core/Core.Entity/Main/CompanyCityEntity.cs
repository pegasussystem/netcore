﻿using System;
namespace Core.Entity.Main
{
    public class CompanyCityEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }


        public Int32? CompanyCityId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? CityType { get; set; }
        public String? CityId { get; set; }
        public String? CompanyCityCode { get; set; }
        public String? CompanyCityName { get; set; }


    }
}

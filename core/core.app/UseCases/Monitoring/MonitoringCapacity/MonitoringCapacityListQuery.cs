﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;


namespace core.app.UseCases.Monitoring.MonitoringCapacity
{
    public partial class MonitoringCapacityListQuery : IRequest<ReturnFormat>
    {
        public Int32? manifestYear { get; set; }
        public Int32? manifestMonth { get; set; }
        public Int32? manifestDay { get; set; }
        public String? carrierCode { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class MonitoringCapacityListQueryHandler : IRequestHandler<MonitoringCapacityListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MonitoringCapacityListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MonitoringCapacityListQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_MonitoringCapacityEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            var manifestDate = req.manifestYear + "-" + req.manifestMonth + "-" + req.manifestDay;
            entities = this.DataMonitoringCapacityList(manifestDate, req.carrierCode);

            if (entities != null)
            {
               rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        s.CreatedAtLocal
                        , s.Moda
                        , s.OriginCityCode
                        , s.OriginCityName
                        , s.Tujuan
                        , s.Aw
                        , s.Caw
                    });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_MonitoringCapacityEntity> DataMonitoringCapacityList(String? manifestDate, String? carrierCode)
        {
            String query = $@"EXEC SP_MonitoringCapacity @MonitoringDate = '{manifestDate}',@Type = '{carrierCode}' ";
            var res = this._context.SP_MonitoringCapacityEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
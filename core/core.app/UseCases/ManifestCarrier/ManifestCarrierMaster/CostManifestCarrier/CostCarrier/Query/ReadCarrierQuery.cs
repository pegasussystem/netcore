﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using MediatR;
using core.app.Common.Interfaces;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostCarrier.Query
{
     public partial class ReadCarrierQuery : IRequest<ReturnFormat>
    {
        public Int32? originStationId { get; set; }
        public Int32? destinationStationId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadCarrierQueryHandler : IRequestHandler<ReadCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadCarrierQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                rtn.Status = StatusCodes.Status200OK;
                
                var qr = from a in _context.CarrierEntitys
                            join b in _context.FlightRouteEntitys on a.CarrierId equals b.CarrierId
                            where b.OriginStationId.Equals(req.originStationId) && b.DestinationStationId.Equals(req.destinationStationId)
                        select new
                        {
                            a.CarrierId
                            , a.CarrierCode
                            , CarrierName = a.CarrierCode + " - " + a.CarrierName
                        };

                if (qr != null)
                {
                    var query = qr.Distinct();
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.CarrierId
                            , s.CarrierCode
                            , s.CarrierName
                        }).OrderBy(o => o.CarrierId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_PrivilegeListEntity
    {
        public Int32? PrivilegeId { get; set; }
        public String? PrivilegeName { get; set; }
        public Int32? CountUser { get; set; }
        public Int32? OrderCode { get; set; }
    }
}

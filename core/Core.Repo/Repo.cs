﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Db.Ps;
using Interface.Repo;

namespace Core.Repo
{
    public abstract class Repo<T> : IRepo<T> where T : class
    {

        protected DbContextPs _contextPs { get; set; }

        public Repo(DbContextPs repositoryContext)
        {
            _contextPs = repositoryContext;
        }


        public T Create(T entity)
        {
            return this._contextPs.Set<T>().Add(entity).Entity;
        }

        public T Delete(T entity)
        {
            //this._contextPs.Set<T>().Attach(entity);
            //this._contextPs.Set<T>().RemoveRange(entity);
            return this._contextPs.Set<T>().Remove(entity).Entity;
        }

        public T Destroy(T entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Raw(string query)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> RawExt<EXT>(string query)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Read()
        {
            return _contextPs.Set<T>();
        }

        public IQueryable<T> Read(Expression<Func<T, bool>> expression)
        {
            var read = this._contextPs.Set<T>().Where(expression);
            return read;
        }

        public void Save()
        {
            this._contextPs.SaveChanges();
        }

        public void Update(T entity)
        {
            this._contextPs.Set<T>().Update(entity);
        }
    }
}

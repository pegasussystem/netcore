﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Station.StationCity.Command
{
    public partial class ManifestCarrierMasterStationCityDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? stationCityId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterStationCityDeleteCommandHandler : IRequestHandler<ManifestCarrierMasterStationCityDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterStationCityDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterStationCityDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.StationCityDelete(req.stationCityId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_StationCityDeleteEntity> StationCityDelete(Int32? stationCityId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_StationCityDelete @stationCityId='{stationCityId}' ";
            var res = this._context.SP_ManifestCarrierMaster_StationCityDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

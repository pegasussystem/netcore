﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_Cost_InlandOrigin_Entity
    {
        // CostDestination
        public Int32? InlandOriginId { get; set; }

        // CostDestinationDetail
        public Int32? InlandOriginDetailId { get; set; }
        public String? CostType { get; set; }
        public String? Unit { get; set; }
        public Decimal? BaseCost { get; set; }
        public String? CostGroup { get; set; }
        public String? UseWeight { get; set; }
    }
}

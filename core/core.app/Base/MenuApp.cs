﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Base;
using Core.Validator.Base;
using FluentValidation;
using FluentValidation.Results;
using Interface.App.Base;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace core.app.Base
{
    public class MenuApp : IMenuApp
    {
        // TODO: standarisai API
        // TODO: gunakan try catch
        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public MenuApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(MenuEntity entity)
        {
            //
            // varuable
            ReturnFormat rtn = new ReturnFormat();
            MenuValidator _validation = new MenuValidator();
            ValidationResult _result;

            try
            {
                //
                // validation 
                _result = _validation.Validate(entity);
                if (!_result.IsValid)
                {
                    rtn.Status422UnprocessableEntity();
                    rtn.Data = _result.ToString("\n");
                    return rtn;
                }

                //-- unique menuCode
                var duplicateData = _repoPs.MenuRepo.Read(w => w.MenuCode.Equals(entity.MenuCode));
                if (duplicateData.Count() > 0)
                {
                    rtn.Data = "Kode Menu sudah terdafta";
                    rtn.Status422UnprocessableEntity();
                    return rtn;
                }



                //
                // create
                rtn.Status200OK();
                rtn.Data = _repoPs.MenuRepo.Create(entity);
                _repoPs.MenuRepo.Save();


                //
                // return
                return rtn;
            }
            catch (DbUpdateException e)
            {
                rtn.Data = e.InnerException.Message;
                if (e.InnerException.Message.Contains("menu_menucode_uindex")) { rtn.Data = "Kode Menu sudah terdaftar"; } // TODO: translate
                rtn.Status422UnprocessableEntity();
                return rtn;
            }
            catch (Exception e)
            {
                rtn.Data = e.InnerException.Message;
                rtn.Status500InternalServerError();
                return rtn;
            }


        }

        public MenuEntity Delete(MenuEntity entity)
        {
            //
            // set
            var rtn = _repoPs.MenuRepo.Delete(entity);
            _repoPs.MenuRepo.Save();

            //
            // return
            return rtn;
        }

        //public List<MenuEntity> Read()
        //{
        //    //_log.LogError("asdasdasd", "EEEEEEEEEEEEEEE");
        //    //
        //    // get data
        //    List<MenuEntity> data = _repoPs.MenuRepo.Read().ToList();

        //    //
        //    // return
        //    return data;
        //}

        public ReturnFormat Read()
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();


            //
            // get data
            rtn.Data = _repoPs.MenuRepo.Read().ToList();
            rtn.Status = StatusCodes.Status200OK;

            //
            // menu tree
            rtn.Data = (rtn.Data as List<MenuEntity>).OrderBy(o => o.MenuIdParent).ToList();
            rtn.Data = MenuOrder(rtn.Data as List<MenuEntity>);


            //
            // return
            return rtn;
        }

        public ReturnFormat ReadTree()
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();


            //
            // get data
            rtn.Data = _repoPs.MenuRepo.Read().ToList();
            rtn.Status = StatusCodes.Status200OK;

            //
            // menu tree
            rtn.Data = (rtn.Data as List<MenuEntity>);
            rtn.Data = MenuTree(rtn.Data as List<MenuEntity>).OrderBy(o => o.MenuId).ToList();

            //
            // return
            return rtn;
        }

        public ReturnFormat Read(MenuEntity entity)
        {
            throw new NotImplementedException();
        }

        public MenuEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public MenuEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(MenuEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            MenuValidator _validation = new MenuValidator();
            ValidationResult _result;

            //
            // vaidation
            _result = _validation.Validate(entity);
            if (!_result.IsValid)
            {
                rtn.Status = StatusCodes.Status422UnprocessableEntity;
                rtn.Data = _result.ToString("\n");
                return rtn;
            }

            //
            // set var
            rtn.Data = _repoPs.MenuRepo.Update(entity);
            rtn.Status = StatusCodes.Status200OK;


            //
            // return OK
            return rtn;
        }

        #region private function ---------------------------------------------------------------
        private List<MenuEntity> MenuOrder(List<MenuEntity> menuEntitys, int parentId = 0, int tab = 0)
        {
            List<MenuEntity> branch = new List<MenuEntity>();
            foreach (MenuEntity row in menuEntitys)
            {
                if (row.MenuIdParent == parentId)
                {
                    row.MenuName = string.Concat(Enumerable.Repeat("-", tab)) + " " + row.MenuName;
                    branch.Add(row);
                    var children = MenuOrder(menuEntitys, row.MenuId, tab + 1);
                    if (children.Count > 0) { branch.AddRange(children); }
                }
            }
            return branch;
        }

        private List<MenuEntity> MenuTree(List<MenuEntity> menuEntitys, int parentId = 0, int tab = 0)
        {
            List<MenuEntity> branch = new List<MenuEntity>();
            //dynamic rtn = new List<Dynamitey>();
            //dynamic branch = new System.Dynamic.ExpandoObject();
            //dynamic branch = Dynamitey.Builder.New();
            foreach (MenuEntity row in menuEntitys)
            {
                if (row.MenuIdParent == parentId)
                {
                    row.MenuName = string.Concat(Enumerable.Repeat("-", tab)) + " " + row.MenuName;
                    //var person = branch.Person( 
                    //                FirstName: "George",
                    //                LastName: "Washington"
                    //            );
                    var children = MenuTree(menuEntitys, row.MenuId, tab + 1);
                    //person.child = children;

                    //string output = JsonConvert.SerializeObject(person, Formatting.Indented);
                    //branch.AddRange(children);
                    if (children.Count > 0) { row.Children = children; }
                    branch.Add(row);
                }
            }
            return branch;
        }
        #endregion private function ---------------------------------------------------------------

    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Privilege
{
    public partial class MenuDetailListQuery : IRequest<ReturnFormat>
    {
        public Int32? privilegeId { get; set; }
        public Int32? menuId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class MenuDetailListQueryHandler : IRequestHandler<MenuDetailListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MenuDetailListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MenuDetailListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_PrivilegeMenuActionDetailListEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.PrivilegeMenuActionDetailListData(req.privilegeId, req.menuId);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                        id = s.MenuActionId
                        , key = s.MenuActionKey
                        , name = s.MenuActionName
                        , value = (s.IsActive > 0 ? true : false)
                    });
            }

            // return
            return rtn;
        }

        public IEnumerable<SP_PrivilegeMenuActionDetailListEntity> PrivilegeMenuActionDetailListData(Int32? privilegeId, Int32? menuId)
        {
            String query = $@"EXEC SP_PrivilegeMenuActionDetailList @privilegeId={privilegeId}, @menuId={menuId}";
            var res = this._context.SP_PrivilegeMenuActionDetailListEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }
}

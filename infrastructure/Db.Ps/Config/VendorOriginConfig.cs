﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class VendorOriginConfig : IEntityTypeConfiguration<VendorOriginEntity>
    {
        public void Configure(EntityTypeBuilder<VendorOriginEntity> builder)
        {
            builder.HasKey(e => e.VendorOriginId);
            builder.ToTable("VendorOrigin");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
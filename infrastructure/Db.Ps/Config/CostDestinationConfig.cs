﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostDestinationConfig : IEntityTypeConfiguration<CostDestinationEntity>
    {
        public void Configure(EntityTypeBuilder<CostDestinationEntity> builder)
        {
            builder.HasKey(e => e.CostDestinationId);
            builder.ToTable("CostDestination");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

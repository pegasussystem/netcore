﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_CostGerai_Entity
    {
        // CostDestination
        public Int32? CostGeraiId { get; set; }
        public String? ManifestOriginCityId { get; set; }

        // CostDestinationDetail
        public Int32? CostGeraiDetailId { get; set; }
        public String? CostType { get; set; }
        public String? Unit { get; set; }
        public Decimal? BaseCost { get; set; }
        public String? CostGroup { get; set; }
        public String? UseWeight { get; set; }
        public String? Desc { get; set; }
    }
}

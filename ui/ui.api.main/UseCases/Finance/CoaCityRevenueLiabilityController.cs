using System.Threading.Tasks;
using core.app.UseCases.BukuKas.Commands;
using core.app.UseCases.BukuKas.Queries;
using core.app.UseCases.Master.CoaCode.Commands;
using core.app.UseCases.Master.CoaCode.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Finance
{
    [Authorize]
    [Route("coa-city-revenue-liability")]
    [ApiController]

    public class CoaCityRevenueLiabilityController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public CoaCityRevenueLiabilityController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list")]
        public async Task<IActionResult> List([FromQuery] ListCoaCityRevenueLiabilityQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("coa-city-revenue-liability-create")]
        public async Task<IActionResult> CcrlCreate([FromBody] CoaCityRevenueLiabilityCreateCommands q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

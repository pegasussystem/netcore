﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_ManifestCarrierCostEntity
    {
        // CostCarrier
        public Int32? ManifestCarrierId { get; set; }

        public String? ManifestNo { get; set; }
        public String? CostType { get; set; }
        public Decimal? BaseCost { get; set; }
        public String? Unit { get; set; }
        public String? CityCode { get; set; }
        public String? CostName { get; set; }
        public String? UnitValue { get; set; }
        public Int32? Order { get; set; }
        public String? CityCodeDestination { get; set; }
        public Decimal? AwFinal { get; set; }
        public Decimal? FinalWeight { get; set; }
        public Decimal? TotalCost { get; set; }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListVendorFirstStationTransitReadQuery : IRequest<ReturnFormat>
    {
        public Int32? originStationId { get; set; }
        public Int32? destinationStationId { get; set; }

        public Int32? firstCarrierCodeId { get; set; }
        public Int32? firstFlightId { get; set; }
        public Int32? lastCarrierCodeId { get; set; }
        public Int32? lastFlightId { get; set; }
    }

    public class ListVendorFirstStationTransitReadQueryHandler : IRequestHandler<ListVendorFirstStationTransitReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListVendorFirstStationTransitReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListVendorFirstStationTransitReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from ft in _context.FlightTransitRouteEntitys
                        join st in _context.StationEntitys on ft.StationTransitId equals st.StationId
                        join vt in _context.VendorTransitEntitys on ft.StationTransitId equals vt.StationId
                        join ve in _context.VendorEntitys on vt.VendorId equals ve.VendorId
                        where ft.OriginStationId.Equals(req.originStationId)
                            && ft.FirstFlightCodeId.Equals(req.firstCarrierCodeId)
                            && ft.FirstFlightId.Equals(req.firstFlightId)
                            && ft.LastFlightCodeId.Equals(req.lastCarrierCodeId)
                            && ft.LastFlightId.Equals(req.lastFlightId)
                            && ft.FinalDestinationStationId.Equals(req.destinationStationId)
                            && ft.SequenceTransit.Equals(1)
                        select new
                        {
                            ve.VendorId
                            , ve.VendorName
                            , st.StationId
                            , st.StationName
                        };

            var query2 = query.Distinct();
            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(s => new { s.VendorId, s.VendorName, s.StationId, s.StationName });
            }

            //
            // return
            return rtn;
        }
    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostTransitDetailConfig : IEntityTypeConfiguration<CostTransitDetailEntity>
    {
        public void Configure(EntityTypeBuilder<CostTransitDetailEntity> builder)
        {
            builder.HasKey(e => e.CostTransitId);
            builder.ToTable("CostTransitDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

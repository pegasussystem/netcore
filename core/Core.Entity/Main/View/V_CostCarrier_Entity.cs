﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_CostCarrier_Entity
    {
        // CostCarrier
        public Int32? CostCarrierId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? FirstFlightId { get; set; }
        public Int32? LastFlightId { get; set; }
        public Int32? VendorCarrierId { get; set; }

        // CostCarrierDetail
        public Int32? CostCarrierDetailId { get; set; }
        public String? CostType { get; set; }
        public String? Unit { get; set; }
        public Decimal? BaseCost { get; set; }
        public String? CostGroup { get; set; }
        public String? UseWeight { get; set; }
    }
}

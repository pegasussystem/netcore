﻿    using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Entity.Base
{
    public class MenuEntity
    {
        public Int32 MenuId { get; set; }
        public Int32? MenuIdParent { get; set; }
        public String MenuName { get; set; }
        public String MenuCode { get; set; }
        public String Route { get; set; }
        public Boolean? IsPage { get; set; }
        
        public IEnumerable<MenuEntity> Children { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.ManifestCarrier.ManifestCarrierCostList;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierCostList
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ManifestCarrierController : DefaultController
    {
        private readonly IAppWrapperPs _appPs;
        public ManifestCarrierController(IAppWrapperPs app)
        {
            _appPs = app;

        }


        [HttpGet("cost-read")]
        public async Task<IActionResult> Execute([FromQuery] ManifestCarrierCostListInput input)
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();

            // logic
            // ///// ///// /////
            _returnFormat = _appPs.ManifestCarrierCostListUseCase.List(GetUserClaim(), input);

            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

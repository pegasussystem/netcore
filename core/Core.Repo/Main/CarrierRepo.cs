﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Base;

namespace Core.Repo.Main
{
    public class CarrierRepo : Repo<CarrierEntity>, ICarrierRepo
    {
        public CarrierRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public IEnumerable<VCarrierEntity> VCarrier()
        {

            var q =
            from c in _contextPs.Set<CarrierEntity>()
            //join mc in _contextPs.Set<MasterCarrierEntity>() on c.MasterCarrierId equals mc.MasterCarrierId
            select new VCarrierEntity{
                //MasterCarrierId = mc.MasterCarrierId,
                //CompanyId = c.CompanyId,
                //CarrierName = c.CarrierName ?? mc.MasterCarrierName,
                CarrierId = c.CarrierId
            };



            return q.ToList();
        }

        /// <summary>
        /// Menu : Manifest Carrier
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VCarrierEntity> ManifestCarrier_Sb_Carrier()
        {
            // TODO get city
            // --- --- ---

            // TODO get carrier
            // --- --- ---
            var q =
            from c in _contextPs.Set<CarrierEntity>()
            //join mc in _contextPs.Set<MasterCarrierEntity>() on c.MasterCarrierId equals mc.MasterCarrierId
            select new VCarrierEntity
            {
                //MasterCarrierId = mc.MasterCarrierId,
                //CompanyId = c.CompanyId,
                //CarrierName = c.CarrierName ?? mc.MasterCarrierName,
                CarrierId = c.CarrierId
            };



            return q.ToList();
        }
    }
}

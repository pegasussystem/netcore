﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VCustomerEntity
    {

        public Int32? CustomerId { get; set; }
        public String? CityCode { get; set; }
        public String? Phone { get; set; }
        public Int32? CompanyId { get; set; }
        public String? PhoneCustom { get; set; }

    }


}

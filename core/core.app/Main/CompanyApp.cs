﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace core.app.Main
{
    public class CompanyApp : ICompanyApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public CompanyApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(CompanyEntity entity)
        {
            throw new NotImplementedException();
        }

        public CompanyEntity Delete(CompanyEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(CompanyEntity entity)
        {
            throw new NotImplementedException();
        }

        public CompanyEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public CompanyEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// get city from VCompanyArea
        /// </summary>
        /// <param name="CityId">city ID</param>
        /// <param name="cityType"> c = city , p = province </param>
        /// <returns></returns>
        public ReturnFormat ReadVCompanyArea(String CityId, UiUserProfileEntity uiUserProfileEntity)
        {
            // TODO: filter by city id and citytype

            // variable
            ReturnFormat rtn = new ReturnFormat();

            // set variable
            rtn.Status = StatusCodes.Status200OK;


            // get data
            rtn.Data = _repoPs.VCompanyAreaRepo
                .Read(w => w.MasterCityId.Equals(CityId) && w.CompanyId.Equals(uiUserProfileEntity.companyId)).ToList()
          .OrderBy(o => o.CompanyAreaCode);


            // return
            return rtn;
        }

        /// <summary>
        /// get city from VCompanyCity
        /// </summary>
        /// <returns></returns>
        public ReturnFormat ReadVCompanyCity()
        {
            // variable
            ReturnFormat rtn = new ReturnFormat();

            // set variable
            rtn.Status = StatusCodes.Status200OK;

            // get data
            rtn.Data = _repoPs.VCompanyCityRepo
                .Read(w => w.CompanyId.Equals(1)).ToList().OrderBy(o => o.CompanyCityNameCustom);

            // return
            return rtn;

        }

        public ReturnFormat Update(CompanyEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostCarrierConfig : IEntityTypeConfiguration<CostCarrierEntity>
    {
        public void Configure(EntityTypeBuilder<CostCarrierEntity> builder)
        {
            builder.HasKey(e => e.CostCarrierId);
            builder.ToTable("CostCarrier");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
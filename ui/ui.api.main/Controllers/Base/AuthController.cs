﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Base;
using Core.Entity.Main;
using Interface.App;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace ui.api.main.Controllers.Base
{
    [Authorize]
    [Route("[controller]")]
    public class AuthController : Controller
    {

        private readonly IAppWrapperPs _appPs;
        public AuthController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        #region LOGIN
        /** 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
        *  LOGIN
        * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
        */

        /** #########################
	     * ACTION
	     * ##########################
	     */

        [AllowAnonymous]
        [HttpPost("login")]
        /// <summary>
        /// recive username and password
        /// then create JWT if the username and password is true
        /// TODO : https://github.com/mrsimi/jwt-refreshtoken-webapi/tree/master/web-api-jwt
        ///  TODO  : https://medium.com/@adegokesimi/implementing-jwt-and-refresh-token-in-net-core-2-2-web-api-b21ef6de2a19
        /// </summary>
        //public IActionResult Login( String Username="")
        public IActionResult Login([FromForm] UserEntity userEntity)
        {
            //
            // -- variable
            ReturnFormat _returnFormat = new ReturnFormat();
            string refreshToken = "";

            //
            // TODO : validation

            //
            // -- get data
            _returnFormat = _appPs.UserApp.Login(userEntity, out refreshToken);


            //
            // -- return
            if (_returnFormat.Status == StatusCodes.Status200OK) {
                return StatusCode(_returnFormat.Status, new { AccessToken = CreateToken(_returnFormat.Data), RefreshToken = refreshToken, UserProfile = _returnFormat.Data });
            }


            return StatusCode(_returnFormat.Status, new { AccessToken = _returnFormat.Data, UserProfile = _returnFormat.Data });


        }

        [AllowAnonymous]
        [HttpPost("loginapprovecust")]
        /// <summary>
        /// recive username and password
        /// then create JWT if the username and password is true
        /// TODO : https://github.com/mrsimi/jwt-refreshtoken-webapi/tree/master/web-api-jwt
        ///  TODO  : https://medium.com/@adegokesimi/implementing-jwt-and-refresh-token-in-net-core-2-2-web-api-b21ef6de2a19
        /// </summary>
        //public IActionResult Login( String Username="")
        public IActionResult LoginApproveCust([FromForm] UserEntity userEntity)
        {
            //
            // -- variable
            ReturnFormat _returnFormat = new ReturnFormat();
            string refreshToken = "";

            //
            // TODO : validation

            //
            // -- get data
            _returnFormat = _appPs.UserApp.Login(userEntity, out refreshToken);


            //
            // -- return
            if (_returnFormat.Status == StatusCodes.Status200OK)
            {
                return StatusCode(_returnFormat.Status, new { AccessToken = CreateToken(_returnFormat.Data), RefreshToken = refreshToken, UserProfile = _returnFormat.Data });
            }


            return StatusCode(_returnFormat.Status, new { AccessToken = _returnFormat.Data, UserProfile = _returnFormat.Data });


        }

        [AllowAnonymous]
        [HttpPost("refresh-token/{refreshToken}")]
        /// <summary>
        /// recive username and password
        /// then create JWT if the username and password is true
        /// </summary>
        //public IActionResult Login( String Username="")
        public IActionResult RefreshToken([FromRoute]string refreshToken)
        {

            // 
            // -- varaiable
            ReturnFormat _returnFormat = new ReturnFormat();
            string refreshTokenOut = "";

            // 
            // -- set variable


            // find user by refresh token
            _returnFormat = _appPs.UserApp.RefreshToken(refreshToken, out refreshTokenOut);

            // if found then create new token


            //
            // -- return
            if (_returnFormat.Status == StatusCodes.Status200OK)
            {
                return StatusCode(_returnFormat.Status, new { AccessToken = CreateToken(_returnFormat.Data), RefreshToken = refreshTokenOut, UserProfile = _returnFormat.Data });
            }


            return StatusCode(_returnFormat.Status, new { AccessToken = _returnFormat.Data, UserProfile = _returnFormat.Data });




        }


        /** #########################
	     * FUCTION
	     * ##########################
	     */

        ///
        /// create token JWT
        /// 
        private String CreateToken(Object data) {

            

            //
            // -- variable
            Claim[] claims;
            byte[] secretBytes;
            SymmetricSecurityKey key;
            String algorithm;
            SigningCredentials signingCredentials;
            JwtSecurityToken token;
            String tokenJson;
            String dataJson = "";

            //
            // -- set variable
            // TODO : configurable
            dataJson = JsonConvert.SerializeObject(data);
            claims = new[]
            {
                new Claim("userProfile", dataJson)
            };

            // TODO : configurable
            secretBytes = Encoding.UTF8.GetBytes("not_too_short_secret_otherwise_it_might_error");
            key = new SymmetricSecurityKey(secretBytes);
            algorithm = SecurityAlgorithms.HmacSha256;

            signingCredentials = new SigningCredentials(key, algorithm);

            // TODO : configurable
            token = new JwtSecurityToken(
                "https://localhost:44382/",
                "https://localhost:44382/",
                claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddDays(1),
                signingCredentials);

            tokenJson = new JwtSecurityTokenHandler().WriteToken(token);

            return tokenJson;
        }

        #endregion LOGIN


    }
}
﻿using core.helper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Configuration.Users.Query
{
    public partial class DataUnitQuery : IRequest<ReturnFormat>
    {
        public String? type { get; set; }
        public Int32? unitId { get; set; }
    }

    public class DataUnitQueryHandler : IRequestHandler<DataUnitQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public DataUnitQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(DataUnitQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;



            if (req.type == "Company")
            {
                var query = from a in _context.V_DataOptionSettingEntitys
                            where a.Type.Equals(req.type)
                            select new
                            {
                                a.Id
                                , a.Code
                                , a.Name
                                , a.Type
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.Id
                            , s.Code
                            , s.Name
                            , s.Type
                        }).OrderBy(o => o.Id);
                }
            }
            else {
                var query = from a in _context.V_DataOptionSettingEntitys
                            where a.Type.Equals(req.type) && a.Key.Equals(req.unitId)
                            select new
                            {
                                a.Id
                                , a.Code
                                , a.Name
                                , a.Type
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.Id
                            , s.Code
                            , s.Name
                            , s.Type
                        }).OrderBy(o => o.Id);
                }
            }
            
            // return
            return rtn;
        }

    }
}

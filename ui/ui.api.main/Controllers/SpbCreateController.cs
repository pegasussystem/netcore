﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.Spb.Commands.CreateSpb;
using core.helper;
using Core.Entity.Base;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using ui.api.main.Helper;
using Microsoft.Extensions.DependencyInjection;
using Interface.Repo;
using System.Text.RegularExpressions;

namespace ui.api.main.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class SpbCreateController : ControllerBase
    {
        private IMediator _mediator;

        private readonly IRepoWarpperPs _repoPs; // 15feb22
        protected IMediator Med => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        private readonly IAppWrapperPs _appPs;
        public SpbCreateController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpPost("screen-capture")]
        public async Task<IActionResult> ScreenCapture([FromForm]VSpbEntity entity, [FromForm] String image)
        {




            var bytes = Convert.FromBase64String(image.Split("base64,")[1]);// a.base64image 
            //or full path to file in temp location
            //var filePath = Path.GetTempFileName();

            // full path to file in current project location
            string filedir = Path.Combine(Directory.GetCurrentDirectory(), "img/spbcreate/" + DateTime.UtcNow.ToString("yyyyMM") + "/" + DateTime.UtcNow.ToString("dd"));
           
            if (!Directory.Exists(filedir))
            { //check if the folder exists;
                Directory.CreateDirectory(filedir);
            }
            string file = Path.Combine(filedir, entity.SpbNo + " - " + DateTime.UtcNow.ToString("yyyy-MM-dd_HH-mm-ss") + ".jpg");


            if (bytes.Length > 0)
            {
                using (var stream = new FileStream(file, FileMode.Create))
                {
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Flush();
                }
            }
            return Ok();
        

        }

        [HttpPost("cek-phone")]    //Pati 15feb22
        public async Task<IActionResult> CekPhone([FromForm] VSpbEntity entity)
        {
            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data via
            _returnFormat = _appPs.SpbCreateApp.ReadCekPhone(entity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);


        }

        [HttpPost("cek-telp")]    //Pati 15feb22
        public async Task<IActionResult> CekTelp([FromForm] VSpbEntity entity)
        {
            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data via
            _returnFormat = _appPs.SpbCreateApp.ReadCekTelp(entity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);


        }

        //[HttpGet("read-spb-print")]
        [HttpPost("read-spb-print")]
        public async Task<IActionResult> ReadSpbPrint([FromForm]VSpbEntity entity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.ReadSpbPrint(GetUserClaim(), entity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpPost("get-koli-number")]
        public async Task<IActionResult> GetKoliNo([FromForm] SpbGoodsEntity spbGoodsEntity, 
            [FromForm] String SpbNo, [FromForm] Int64 SpbId, [FromForm] String SpbNoManual,
            [FromForm] int destCity, [FromForm] int destArea, [FromForm] int totalKoli,
            [FromForm] String carrier,
            [FromForm] UiSpbCreateEntity uiSpbCreateEntity
            )
        {

            // --- --- --- --- ---
            // variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            // --- --- --- --- ---
            // logic
            // --- --- --- --- ---
            uiUserProfileEntity = GetUserClaim();

            _returnFormat = await Med.Send(new GetKoliNoCommand
            {
                totalKoli = totalKoli,
                carrier = carrier,
                destArea = destArea,
                destCity = destCity,
                entity = spbGoodsEntity,
                SpbId = SpbId,
                SpbNo = SpbNo,
                SpbNoManual = SpbNoManual,
                uiUserProfileEntity = uiUserProfileEntity,
                UiSpbCreateEntity = uiSpbCreateEntity,
                Rates = _appPs.SpbCreateApp.GetRates(GetUserClaim(), uiSpbCreateEntity).Data as core.app.Main.RatesObj
            }).ConfigureAwait(false);

            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        //[HttpGet("read-spb-goods-print")]
        [HttpPost("read-spb-goods-print")]
        public async Task<IActionResult> ReadSpbGoodPrint([FromForm]VSpbGoodsEntity entity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.ReadSpbGoodPrint(entity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("read-sender-list")]
        public async Task<IActionResult> ReadSenderList([FromQuery] UiSpbCreateEntity uiSpbCreateEntity)
        {


          

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();
            Int32 totalRow = 0;
            StringValues workCityId = "";
            StringValues senderPhone = ""; 
            StringValues tipeCust = "";


            Request.Headers.TryGetValue("workCityId", out workCityId);
            Request.Headers.TryGetValue("senderPhone", out senderPhone);
            Request.Headers.TryGetValue("tipeCust", out tipeCust);  // baru nyampe sini
            uiSpbCreateEntity.WorkCityId = Convert.ToInt32(workCityId.FirstOrDefault());
            uiSpbCreateEntity.Sender = senderPhone;
            uiSpbCreateEntity.TipeCust = tipeCust;  //Pati 22 Nov

            //
            // logic

            // get data Vspb
            //_returnFormat = _appPs.SpbCreateApp.ReadSenderList(GetUserClaim(), uiSpbCreateEntity, out totalRow);
            _returnFormat = _appPs.SpbCreateApp.ReadSenderList2(GetUserClaim(), uiSpbCreateEntity, out totalRow); //Pati 25 Agt
            var _returnFormatCustom = new
            {
                data = _returnFormat.Data,
                totalCount = totalRow
            };


            //
            // return
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                return StatusCode(_returnFormat.Status, _returnFormatCustom);

            }
            else { 
            return StatusCode(_returnFormat.Status, _returnFormat);

            }
        }

        [HttpGet("read-sender-telp-list")]
        public async Task<IActionResult> ReadSenderTelpList([FromQuery] UiSpbCreateEntity uiSpbCreateEntity)
        {




            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();
            Int32 totalRow = 0;
            StringValues workCityId = "";
            StringValues senderTelp = "";
            StringValues tipeCust = "";


            Request.Headers.TryGetValue("workCityId", out workCityId);
            Request.Headers.TryGetValue("senderTelp", out senderTelp);
            Request.Headers.TryGetValue("tipeCust", out tipeCust);  // baru nyampe sini
            uiSpbCreateEntity.WorkCityId = Convert.ToInt32(workCityId.FirstOrDefault());
            uiSpbCreateEntity.SenderTelp = senderTelp;
            uiSpbCreateEntity.TipeCust = tipeCust;  //Pati 22 Nov

            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.ReadSenderTelpList(GetUserClaim(), uiSpbCreateEntity, out totalRow); //Pati 25 Agt
            var _returnFormatCustom = new
            {
                data = _returnFormat.Data,
                totalCount = totalRow
            };


            //
            // return
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                return StatusCode(_returnFormat.Status, _returnFormatCustom);

            }
            else
            {
                return StatusCode(_returnFormat.Status, _returnFormat);

            }
        }

        [HttpGet("read-sender-name-list")]
        public async Task<IActionResult> ReadSenderNameList([FromQuery] UiSpbCreateEntity uiSpbCreateEntity)
        {




            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();
            Int32 totalRow = 0;
            StringValues workCityId = "";
            StringValues senderName = "";
            StringValues tipeCust = "";


            Request.Headers.TryGetValue("workCityId", out workCityId);
            Request.Headers.TryGetValue("senderName", out senderName);
            Request.Headers.TryGetValue("tipeCust", out tipeCust);  // baru nyampe sini
            uiSpbCreateEntity.WorkCityId = Convert.ToInt32(workCityId.FirstOrDefault());
            uiSpbCreateEntity.SenderName = senderName;
            uiSpbCreateEntity.TipeCust = tipeCust;  //Pati 22 Nov

            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.ReadSenderNameList(GetUserClaim(), uiSpbCreateEntity, out totalRow); //Pati 09Jun22
            var _returnFormatCustom = new
            {
                data = _returnFormat.Data,
                totalCount = totalRow
            };


            //
            // return
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                return StatusCode(_returnFormat.Status, _returnFormatCustom);

            }
            else
            {
                return StatusCode(_returnFormat.Status, _returnFormat);

            }
        }

        [HttpGet("read-receiver-name-list")] // 10jun22
        public async Task<IActionResult> ReadReceiverNameList([FromQuery] UiSpbCreateEntity uiSpbCreateEntity)
        {
            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();
            Int32 totalRow = 0;
            StringValues workCityId = "";
            StringValues receiverName = "";
            StringValues tipeCust = "";


            Request.Headers.TryGetValue("workCityId", out workCityId);
            Request.Headers.TryGetValue("receiverName", out receiverName);
            Request.Headers.TryGetValue("tipeCust", out tipeCust);  // baru nyampe sini
            uiSpbCreateEntity.WorkCityId = Convert.ToInt32(workCityId.FirstOrDefault());
            uiSpbCreateEntity.ReceiverName = receiverName;
            uiSpbCreateEntity.TipeCust = tipeCust;  

            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.ReadReceiverNameList(GetUserClaim(), uiSpbCreateEntity, out totalRow); //Pati 09Jun22
            var _returnFormatCustom = new
            {
                data = _returnFormat.Data,
                totalCount = totalRow
            };


            //
            // return
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                return StatusCode(_returnFormat.Status, _returnFormatCustom);

            }
            else
            {
                return StatusCode(_returnFormat.Status, _returnFormat);

            }
        }

        [HttpGet("read-sender-idpel")]   // Pati 18Mei22
        public async Task<IActionResult> ReadSenderIdpel([FromQuery] UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();
            Int32 totalRow = 0;
            StringValues workCityId = "";
            StringValues senderPhone = "";
            StringValues tipeCust = "";
            StringValues senderIdPel = "";
            String senderId3lc = string.Empty;

            Request.Headers.TryGetValue("workCityId", out workCityId);
            Request.Headers.TryGetValue("senderPhone", out senderPhone);
            Request.Headers.TryGetValue("tipeCust", out tipeCust);
            Request.Headers.TryGetValue("senderIdPel", out senderIdPel);
            uiSpbCreateEntity.WorkCityId = Convert.ToInt32(workCityId.FirstOrDefault());
            uiSpbCreateEntity.Sender = senderPhone;
            uiSpbCreateEntity.TipeCust = tipeCust;

            if (Regex.IsMatch(senderIdPel.ToString().Substring(0, 2), @"^[a-zA-Z]+$"))
            {
                senderId3lc = senderIdPel.ToString().Substring(0, 2);
                senderIdPel = senderIdPel.ToString().Substring(3, (senderIdPel.ToString().Length - 3));
            }
            uiSpbCreateEntity.CustomerCode = senderIdPel;

            //
            // logic

            // get data Vspb
            //_returnFormat = _appPs.SpbCreateApp.ReadSenderList(GetUserClaim(), uiSpbCreateEntity, out totalRow);
            _returnFormat = _appPs.SpbCreateApp.ReadSenderIdPel(GetUserClaim(), uiSpbCreateEntity, out totalRow); //Pati 25 Agt
            var _returnFormatCustom = new
            {
                data = _returnFormat.Data,
                totalCount = totalRow
            };


            //
            // return
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                return StatusCode(_returnFormat.Status, _returnFormatCustom);

            }
            else
            {
                return StatusCode(_returnFormat.Status, _returnFormat);

            }
        }

        [HttpGet("read-receiver-idpel")]   // Pati 18Mei22
        public async Task<IActionResult> ReadReceiverIdpel([FromQuery] UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();
            Int32 totalRow = 0;
            StringValues workCityId = "";
            StringValues receiverPhone = "";
            StringValues tipeCust = "";
            StringValues receiverIdPel = "";

            String receiverId3lc = string.Empty;

            Request.Headers.TryGetValue("workCityId", out workCityId);
            Request.Headers.TryGetValue("receiverPhone", out receiverPhone);
            Request.Headers.TryGetValue("tipeCust", out tipeCust);
            Request.Headers.TryGetValue("receiverIdPel", out receiverIdPel);
            uiSpbCreateEntity.WorkCityId = Convert.ToInt32(workCityId.FirstOrDefault());
            uiSpbCreateEntity.Receiver = receiverPhone;
            uiSpbCreateEntity.TipeCust = tipeCust;

            if (Regex.IsMatch(receiverIdPel.ToString().Substring(0, 2), @"^[a-zA-Z]+$"))
            {
                receiverId3lc = receiverIdPel.ToString().Substring(0, 2);
                receiverIdPel = receiverIdPel.ToString().Substring(3, (receiverIdPel.ToString().Length - 3));
            }
            uiSpbCreateEntity.CustomerCode = receiverIdPel;

            //
            // logic

            // get data Vspb
            //_returnFormat = _appPs.SpbCreateApp.ReadSenderList(GetUserClaim(), uiSpbCreateEntity, out totalRow);
            _returnFormat = _appPs.SpbCreateApp.ReadReceiverIdPel(GetUserClaim(), uiSpbCreateEntity, out totalRow); //Pati 25 Agt
            var _returnFormatCustom = new
            {
                data = _returnFormat.Data,
                totalCount = totalRow
            };


            //
            // return
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                return StatusCode(_returnFormat.Status, _returnFormatCustom);

            }
            else
            {
                return StatusCode(_returnFormat.Status, _returnFormat);

            }
        }

        [HttpPost("get-rates")]
        public async Task<IActionResult> GetRates([FromForm] UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.GetRates(GetUserClaim(), uiSpbCreateEntity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        ////Tambahan Pati 5 Okt 21
        ////Begin

        //[HttpPost("sender-urban")]
        //public async Task<IActionResult> GetSenderUrban([FromForm] UiSpbCreateEntity uiSpbCreateEntity)
        //{

        //    //
        //    // variable
        //    ReturnFormat _returnFormat = new ReturnFormat();


        //    //
        //    // logic

        //    // get data Vspb
        //    _returnFormat = _appPs.SpbCreateApp.GetSenderUrban(GetUserClaim(), uiSpbCreateEntity);


        //    //
        //    // return
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}
        ////End

        [HttpPost("receiver-urban")]
        public async Task<IActionResult> GetReceiverUrban([FromForm] UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.GetReceiverUrban(GetUserClaim(), uiSpbCreateEntity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("receiver-postalcode")]
        public async Task<IActionResult> GetReceiverPostalCode([FromForm] UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.GetReceiverPostalCode(GetUserClaim(), uiSpbCreateEntity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("receiver-subdistrict")]
        public async Task<IActionResult> GetReceiverSubDistrict([FromForm] UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.GetReceiverSubDistrict(GetUserClaim(), uiSpbCreateEntity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("receiver-city")]
        public async Task<IActionResult> GetReceiverCity([FromForm] UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic
             
            // get data Vspb
            _returnFormat = _appPs.SpbCreateApp.GetReceiverCity(GetUserClaim(), uiSpbCreateEntity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("read-via")]
        public async Task<IActionResult> ReadVia([FromQuery] UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data via
            _returnFormat = _appPs.SpbCreateApp.ReadVia(GetUserClaim(), uiSpbCreateEntity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpPost("do-submit")]
        public async Task<IActionResult> DoSubmit([FromForm] UiSpbCreateEntity uiSpbCreateEntity, [FromForm] SpbEntity spbEntity)
        {
            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            //post data
            _returnFormat = _appPs.SpbCreateApp.DoSubmitForUpdateSpb(GetUserClaim(), uiSpbCreateEntity, spbEntity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        UiUserProfileEntity GetUserClaim() {
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            // get claim user
            var claims = User.Claims.Where(w => w.Type.Equals("userProfile")).First().Value;
            uiUserProfileEntity = JsonConvert.DeserializeObject<UiUserProfileEntity>(claims);

            return uiUserProfileEntity;
        }
    }
}
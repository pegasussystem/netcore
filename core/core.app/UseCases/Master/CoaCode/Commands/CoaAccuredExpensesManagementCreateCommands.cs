using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;
using Dynamitey.DynamicObjects;

namespace core.app.UseCases.Master.CoaCode.Commands
{
    public partial class CoaAccuredExpensesManagementCreateCommands : IRequest<ReturnFormat>
    {
        public Int32 coaId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CoaAccuredExpensesManagementCreateCommandsHandler : IRequestHandler<CoaAccuredExpensesManagementCreateCommands, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CoaAccuredExpensesManagementCreateCommandsHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CoaAccuredExpensesManagementCreateCommands req, CancellationToken cancellationToken)
        {
            ReturnFormat rtn = new ReturnFormat();

            try
            {

                // Membuat objek CoaCityRevenueLiability baru
                var newCoaAccuredExpensesManagement = new CoaAccuredExpensesManagementEntity
                {
                    coaId = req.coaId,
                    IsActive = 1,
                    createdAt = DateTime.Now
                };

                // Menambahkan entitas baru ke konteks
                _context.CoaAccuredExpensesManagementEntities.Add(newCoaAccuredExpensesManagement);

                // Menyimpan perubahan ke database
                await _context.SaveChangesAsync(cancellationToken);

                // Set status dan pesan berhasil
                rtn.Status = StatusCodes.Status201Created;

                rtn.Data = newCoaAccuredExpensesManagement;
            }
            catch (Exception ex)
            {
                // Menangani jika terjadi error
                rtn.Status = StatusCodes.Status500InternalServerError;
                rtn.Data = null;
            }

            return rtn;

        }


    }
}
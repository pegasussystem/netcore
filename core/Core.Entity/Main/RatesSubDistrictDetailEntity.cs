﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Core.Entity.Main
{
    public class RatesSubDistrictDetailEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? RatesSubDistrictDetailId { get; set; }
        public Int32? RatesSubDistrictId { get; set; }
        public Int32? Min { get; set; }
        public Int32? Max { get; set; }
        public Decimal? Rates { get; set; }
        public String? Type { get; set; }


    }
}

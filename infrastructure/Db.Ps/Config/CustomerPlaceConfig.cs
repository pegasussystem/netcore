﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class CustomerPlaceConfig : IEntityTypeConfiguration<CustomerPlaceEntity>
    {
        public void Configure(EntityTypeBuilder<CustomerPlaceEntity> builder)
        {
            builder.HasKey(e => e.CustomerPlaceId);
            builder.ToTable("CustomerPlace", "dbo");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

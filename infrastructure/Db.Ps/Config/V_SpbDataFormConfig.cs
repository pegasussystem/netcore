﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class V_SpbDataFormConfig : IEntityTypeConfiguration<V_SpbDataFormEntity>
    {
        public void Configure(EntityTypeBuilder<V_SpbDataFormEntity> builder)
        {
            builder.HasKey(e => e.SpbId);
            builder.ToView("V_SpbDataForm");
        }
    }
}

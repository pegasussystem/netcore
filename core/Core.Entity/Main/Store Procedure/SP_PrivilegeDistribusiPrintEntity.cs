﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_PrivilegeDistribusiPrintEntity
    {
        public Int32? UserId { get; set; }
        public String? Username { get; set; }
        public Int32? PrivilegeId { get; set; }
        public Int32? MenuActionId { get; set; }
        public String? PrintValue { get; set; }
    }
}

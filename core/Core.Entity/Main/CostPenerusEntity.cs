﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class CostPenerusEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? CostPenerusId { get; set; }
        public Int32? StationDestinationId { get; set; }
        public String? CityDestinationId { get; set; }
        public Int32? VendorDestinationId { get; set; }

    }
}

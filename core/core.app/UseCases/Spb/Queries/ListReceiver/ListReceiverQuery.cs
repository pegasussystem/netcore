﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Spb.Queries.ListReceiver
{
    public partial class ListReceiverQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListReceiverQueryHandler : IRequestHandler<ListReceiverQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListReceiverQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListReceiverQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VSpbEntity> vSpbEntities = null;
            BranchEntity branch = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;
            //ListSpbReceiverInput input = obj as ListSpbReceiverInput;
            var MonthCreate = req.CreatedAt.Value.Month;
            var YearCreate  = req.CreatedAt.Value.Year;

            //
            // logic



            // from branch
            if (req.uiUserProfileEntity.companyId != null && req.uiUserProfileEntity.BranchId != null && req.uiUserProfileEntity.SubBranchId == null)
            {
                // get branch handled 
                branch = _repoPs.BranchRepo.Read(w => w.BranchId.Equals(req.uiUserProfileEntity.BranchId)).FirstOrDefault();
                var branchList = (branch.CityHandled != null) ? branch.CityHandled.Replace("|<", "").Replace(">|", "").Split("><") : null;

                vSpbEntities = _repoPs.VSpbRepo
                    .Read(w => branchList.Contains(w.DestinationCityId)
                                && w.CreatedAt.Value.Month.Equals(MonthCreate)
                                && w.CreatedAt.Value.Year.Equals(YearCreate)
                                )
                    .ToList();
            }

            // from company
            if (req.uiUserProfileEntity.companyId != null && req.uiUserProfileEntity.BranchId == null && req.uiUserProfileEntity.SubBranchId == null)
            {
                vSpbEntities = _repoPs.VSpbRepo
                    .Read(w => w.CompanyId.Equals(req.uiUserProfileEntity.companyId)
                                && w.CreatedAt.Value.Month.Equals(MonthCreate)
                                && w.CreatedAt.Value.Year.Equals(YearCreate)
                                )
                    .ToList();
            }



            if (vSpbEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vSpbEntities
                    .Select(s => new {
                        s.SpbId,
                        s.SpbNo,
                        s.Username,
                        s.OriginCityNameCustom,
                        s.DestinationCityNameCustom,
                        s.Rates,
                        s.Packing,
                        s.Quarantine,
                        s.Etc,
                        s.Ppn,
                        s.Discount,
                        s.TotalPrice,
                        s.SenderName,
                        s.SenderPhone,
                        s.SenderPlace,
                        s.SenderStore,
                        s.SenderAddress,
                        s.ReceiverName,
                        s.ReceiverPhone,
                        s.ReceiverPlace,
                        s.ReceiverStore,
                        s.ReceiverAddress,
                        s.CarrierName,
                        s.PaymentMethod,
                        s.QualityOfServiceName,
                        s.TypeOfServiceName,
                        CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)req.uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfileEntity.WorkTimeZoneMinute)
                    });
            }

            //
            // return
            return rtn;
        }
    }
}

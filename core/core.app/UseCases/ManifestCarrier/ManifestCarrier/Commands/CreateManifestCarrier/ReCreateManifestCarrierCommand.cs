﻿using core.app.Common.Interfaces;
using core.app.Helpers;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using static core.app.UseCases.ManifestCarrier.Commands.CreateManifestCarrierCommandHandler;

namespace core.app.UseCases.ManifestCarrier.Commands
{
    public partial class CreateManifestCarrierCommand : IRequest<ReturnFormat>
    {
        public Int32 startDateUtcYear { get; set; }
        public Int32 startDateUtcDay { get; set; }
        public Int32 startDateUtcHour { get; set; }
        public Int32 startDateUtcMonth { get; set; }
        public Int32 startDateUtcMinute { get; set; }
        public Int32 startDateUtcSecond { get; set; }
        public Int32 startDateUtcMillisecond { get; set; }
        public Int32 endDateUtcYear { get; set; }
        public Int32 endDateUtcDay { get; set; }
        public Int32 endDateUtcHour { get; set; }
        public Int32 endDateUtcMonth { get; set; }
        public Int32 endDateUtcMinute { get; set; }
        public Int32 endDateUtcSecond { get; set; }
        public Int32 endDateUtcMillisecond { get; set; }

        public Int32? carrierId { get; set; }

        public Int32? firstCarrierCodeId { get; set; }
        public Int32? firstCityTransitId { get; set; }
        public Int32? firstFlightId { get; set; }
        public Int32? firstVendorTransitId { get; set; }

        public Int32? secondCarrierCodeId { get; set; }
        public String? secondCityTransitId { get; set; }
        public Int32? secondFlightId { get; set; }
        public Int32? secondVendorTransitId { get; set; }

        public Int32? thirdCarrierCodeId { get; set; }
        public String? thirdCityTransitId { get; set; }
        public Int32? thirdFlightId { get; set; }
        public Int32? thirdVendorTransitId { get; set; }

        public Int32? fourthCarrierCodeId { get; set; }
        public Int32? fourthFlightId { get; set; }

        public DateTime flightDate { get; set; }

        public String? originCityId { get; set; }
        public Int32? originStationId { get; set; }

        public String? cityDestinationId { get; set; }
        public Int32? destinationStationId { get; set; }
        
        public Int32? modaId { get; set; }
        public String? noSmu { get; set; }
        public Int32? costSMU { get; set; }
        
        
        public Int32? manifestTotalWeight { get; set; }
        public Int32? ppnId { get; set; }
        public Int32? vendorCarrierId { get; set; }


        public UiUserProfileEntity uiUserProfile { get; set; }

        public DateTime StartDateUtc()
        {
            string dateString = $"{startDateUtcYear}-{(startDateUtcMonth < 10 ? "0" : "")}{startDateUtcMonth}-{(startDateUtcDay < 10 ? "0" : "")}{startDateUtcDay} {(startDateUtcHour < 10 ? "0" : "")}{startDateUtcHour}:{(startDateUtcMinute < 10 ? "0" : "")}{startDateUtcMinute}:{(startDateUtcSecond < 10 ? "0" : "")}{startDateUtcSecond}.000";
            string format = "yyyy-MM-dd HH:mm:ss.fff";
            DateTime dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
            dateTime = dateTime.AddMilliseconds(startDateUtcMillisecond);
            return dateTime;
        }

        public DateTime EndDateUtc()
        {
            string dateString = $"{endDateUtcYear}-{(endDateUtcMonth < 10 ? "0" : "")}{endDateUtcMonth}-{(endDateUtcDay < 10 ? "0" : "")}{endDateUtcDay} {(endDateUtcHour < 10 ? "0" : "")}{endDateUtcHour}:{(endDateUtcMinute < 10 ? "0" : "")}{endDateUtcMinute}:{(endDateUtcSecond < 10 ? "0" : "")}{endDateUtcSecond}.000";
            string format = "yyyy-MM-dd HH:mm:ss.fff";
            DateTime dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
            dateTime = dateTime.AddMilliseconds(endDateUtcMillisecond);
            return dateTime;
        }
        public List<Manifests>? Manifests { get; set; }
    }

    public class Manifests
    {
    public Int64? manifestId { get; set; }
    public Int32? aw { get; set; }
    public Int32? koli { get; set; }
    public Int32? caw { get; set; }
    public Int32? finalKoli { get; set; }
    public Int32? weightManifest { get; set; }
    public Int32? weightSmu { get; set; }
    }


    public class CreateManifestCarrierCommandHandler : IRequestHandler<CreateManifestCarrierCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateManifestCarrierCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateManifestCarrierCommand req, CancellationToken cancellationToken)
        {
            // --- --- ---
            // variable
            // --- --- ---
            PrivilegeData privilegeData;
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            List<ManifestCarrierItemEntity> saveCarrierDetail = new List<ManifestCarrierItemEntity>();
        
            // --- --- ---
            // Privilege
            // --- --- ---
            privilegeData = new PrivilegeData
            {
                SubBranch_Create = false,
                Branch_Create = true,
                Ho_Create = true
            };
            if (H_Privilege.Create(req.uiUserProfile, privilegeData) == false) { return rtn; }
            
            Int32? last_flight = 0;

            if (req.fourthFlightId != null || req.fourthFlightId != 0)
            {
                last_flight = req.fourthFlightId;
            }
            else if (req.thirdFlightId != null || req.thirdFlightId != 0)
            {
                last_flight = req.thirdFlightId;
            }
            else if (req.secondFlightId != null || req.secondFlightId != 0)
            {
                last_flight = req.secondFlightId;
            }
            else 
            {
                last_flight = req.firstFlightId;
            }

            //cek exists di cost carrier
            var q1 = from cc in _context.CostCarrierEntitys 
                     join ccd in _context.CostCarrierDetailEntitys on cc.CostCarrierId equals ccd.CostCarrierId
                     where cc.CarrierId.Equals(req.carrierId)
                        && cc.FirstFlightId.Equals(req.firstFlightId)
                        && cc.LastFlightId.Equals(last_flight)
                        && cc.VendorCarrierId.Equals(req.vendorCarrierId)
                     select cc;

            if (q1.Count() == 0)
            {
                rtn.Data = req;
                rtn.Status = StatusCodes.Status200OK;
                return rtn;
            }

            ////cek exists di cost origin
            //var q1 = from co in _context.CostOriginEntitys
            //         where co.StationOriginId.Equals(req.originStationId)
            //            && co.CarrierId.Equals()
            //         select co;

            //if (q1.Count() == 0)
            //{
            //    rtn.Data = req;
            //    rtn.Status = StatusCodes.Status200OK;
            //    return rtn;
            //}

            //cek exists di entity
            //foreach (var item in req.Manifests)
            //{
            //    //cek exists di Cost Origin
            //    var q1 = from co in _context.CostOriginEntitys
            //             where co.StationOriginId.Equals(item.originStationId)
            //             select co;

            //    if (q1.Count() == 0)
            //    {
            //        rtn.Data = req;
            //        rtn.Status = StatusCodes.Status200OK;
            //        return rtn;
            //    }

            //    //cek exists di Cost Destination
            //    var q2 = from cd in _context.CostDestinationEntitys
            //             //where cd.DestinationStationId.Equals(req.destinationStationId) && cd.LastCarrierCodeId.Equals(req.lastCarrierCodeId) && cd.LastVendorId.Equals(req.lastVendorId)
            //             select cd;

            //    if (q2.Count() == 0)
            //    {
            //        rtn.Data = req;
            //        rtn.Status = StatusCodes.Status200OK;
            //        return rtn;
            //    }
            //}


            //// use transaction
            //// --- --- ---
            //using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            //{
            //    // create SMU to table ManifestCarrier
            //    // --- --- ---
            //    ManifestCarrierEntity saveCarrier = this.Create_ManifestCarrier(req);
            //    if (saveCarrier.ManifestCarrierId == null) { return rtn; }

            //    req.manifestCarrierId = saveCarrier.ManifestCarrierId;

            //    // create all manifest to table ManifestCarrierItem
            //    // --- --- ---
            //    saveCarrierDetail = this.Create_ManifestCarrierItem(req);

            //    // create cost carrier to table ManifestCarrierCost
            //    // --- --- ---
            //    var result = this.Create_CostCarrier_ManifestCarrierCost(req, saveCarrierDetail);

            //    this.SetHistoryAction(req.uiUserProfile.userId, saveCarrier.ManifestCarrierId, "Create Manifest Carrier", "Manifest Carrier", "");

            //    transaction.Complete();

            //    if (req.manifestCarrierId != null)
            //    {
            //        rtn.Data = req;
            //        rtn.Status = StatusCodes.Status200OK;
            //    }
            //}
            return rtn;
        }

        //public IEnumerable<V_CostCarrier_Entity> V_CostCarrierFirst(Int32? destinationStationId, Int32? firstCarrierId, Int32? firstCarrierCodeId, Int32? firstVendorId)
        //{
        //    // create query
        //    // --- --- ---
        //    IQueryable<V_CostCarrier_Entity> result =
        //        from cc     in _context.CostCarrierEntitys
        //        join ccd    in _context.CostCarrierDetailEntitys on cc.CostCarrierId equals ccd.CostCarrierId
        //        join fc     in _context.MatrixFirstCarrierEntitys on cc.CostCarrierId equals fc.CostCarrierId
        //        join cr     in _context.CarrierEntitys on fc.CarrierId equals cr.CarrierId

        //        where cc.DestinationStationId.Equals(destinationStationId) && cc.VendorId.Equals(firstVendorId) && fc.CarrierId.Equals(firstCarrierId) //&& cr.CarrierCodeId.Equals(firstCarrierCodeId)

        //        select new V_CostCarrier_Entity
        //        {
        //            CostCarrierId       = cc.CostCarrierId,
        //            VendorId            = cc.VendorId,
        //            DestinationStation  = cc.DestinationStationId,
        //            FirstCarrierId      = fc.CarrierId,
        //            FirstCarrierCodeId  = 0,//cr.CarrierCodeId,
        //            CostCarrierDetailId = ccd.CostCarrierDetailId,
        //            CostType            = ccd.CostType,
        //            Unit                = ccd.Unit,
        //            BaseCost            = ccd.BaseCost,
        //            CostGroup           = ccd.CostGroup,
        //            UseWeight           = ccd.UseWeight
        //        };

        //    // return result as list
        //    // --- --- ---
        //    return result.ToList();
        //}

        //public IEnumerable<V_CostCarrier_Entity> V_CostCarrierLast(Int32? destinationStationId, Int32? lastCarrierId, Int32? lastCarrierCodeId, Int32? lastVendorId)
        //{
        //    // create query
        //    // --- --- ---
        //    IQueryable<V_CostCarrier_Entity> result =
        //        from cc in _context.CostCarrierEntitys
        //        join ccd in _context.CostCarrierDetailEntitys on cc.CostCarrierId equals ccd.CostCarrierId
        //        join fc in _context.MatrixLastCarrierEntitys on cc.CostCarrierId equals fc.CostCarrierId
        //        join cr in _context.CarrierEntitys on fc.CarrierId equals cr.CarrierId

        //        where cc.DestinationStationId.Equals(destinationStationId) && cc.VendorId.Equals(lastVendorId) && fc.CarrierId.Equals(lastCarrierId) //&& cr.CarrierCodeId.Equals(lastCarrierCodeId)

        //        select new V_CostCarrier_Entity
        //        {
        //            CostCarrierId       = cc.CostCarrierId,
        //            VendorId            = cc.VendorId,
        //            DestinationStation  = cc.DestinationStationId,
        //            LastCarrierId       = fc.CarrierId,
        //            LastCarrierCodeId   = 0,//cr.CarrierCodeId,
        //            CostCarrierDetailId = ccd.CostCarrierDetailId,
        //            CostType            = ccd.CostType,
        //            Unit                = ccd.Unit,
        //            BaseCost            = ccd.BaseCost,
        //            CostGroup           = ccd.CostGroup,
        //            UseWeight           = ccd.UseWeight
        //        };

        //    // return result as list
        //    // --- --- ---
        //    return result.ToList();
        //}

        ////public IEnumerable<V_CostCarrier_Entity> V_CostCarrier(Int32? firstCarrier, Int32? lastCarrier)
        ////{
        ////    // create query
        ////    // --- --- ---
        ////    IQueryable<V_CostCarrier_Entity> result =
        ////        from cc in _context.CostCarrierEntitys
        ////        join ccd in _context.CostCarrierDetailEntitys on cc.CostCarrierId equals ccd.CostCarrierId
        ////        join fc in _context.MatrixFirstCarrierEntitys on cc.CostCarrierId equals fc.CostCarrierId
        ////        join lc in _context.MatrixLastCarrierEntitys on cc.CostCarrierId equals lc.CostCarrierId

        ////        where fc.CarrierId.Equals(firstCarrier) && lc.CarrierId.Equals(lastCarrier)

        ////        select new V_CostCarrier_Entity
        ////        {
        ////            CostCarrierId       = cc.CostCarrierId,
        ////            VendorId            = cc.VendorId,
        ////            DestinationStation  = cc.DestinationStationId,
        ////            FirstCarrier        = fc.CarrierId,
        ////            LastCarrier         = lc.CarrierId,
        ////            CostCarrierDetailId = ccd.CostCarrierDetailId,
        ////            CostType            = ccd.CostType,
        ////            Unit                = ccd.Unit,
        ////            BaseCost            = ccd.BaseCost,
        ////            CostGroup           = ccd.CostGroup,
        ////            UseWeight           = ccd.UseWeight
        ////        };

        ////    // return result as list
        ////    // --- --- ---
        ////    return result.ToList();
        ////}


        ////public IEnumerable<V_CostOps_Entity> V_CostOps(Int32? originStationId)
        ////{
        ////    // create query
        ////    // --- --- ---
        ////    IQueryable<V_CostOps_Entity> result =
        ////        from co in _context.CostOpsEntitys
        ////        join cod in _context.CostOpsDetailEntitys on co.CostOpsId equals cod.CostOpsId
        ////        where co.OriginStationId.Equals(originStationId)

        ////        select new V_CostOps_Entity
        ////        {
        ////            CostOpsId           = co.CostOpsId,
        ////            OriginStationId     = co.OriginStationId,
        ////            CostOpsDetailId     = cod.CostOpsDetailId,
        ////            CostType            = cod.CostType,
        ////            Unit                = cod.Unit,
        ////            BaseCost            = cod.BaseCost,
        ////            CostGroup           = cod.CostGroup,
        ////            UseWeight           = cod.UseWeight
        ////        };

        ////    // return result as list
        ////    // --- --- ---
        ////    return result.ToList();
        ////}


        ////public IEnumerable<V_CostOrigin_Entity> V_CostOrigin(int? originStationId, int? firstCarrierCodeId, int? firstVendorId)
        ////{
        ////    // create query
        ////    // --- --- ---
        ////    IQueryable<V_CostOrigin_Entity> result =
        ////           from co in _context.CostOriginEntitys
        ////           join cod in _context.CostOriginDetailEntitys on co.CostOriginId equals cod.CostOriginId
        ////           where co.OriginStationId.Equals(originStationId) && co.FirstCarrierCodeId.Equals(firstCarrierCodeId) && co.FirstVendorId.Equals(firstVendorId)

        ////           select new V_CostOrigin_Entity
        ////           {
        ////               CostOriginId         = co.CostOriginId,
        ////               OriginStationId      = co.OriginStationId,
        ////               CostOriginDetailId   = cod.CostOriginDetailId,
        ////               CostType             = cod.CostType,
        ////               Unit                 = cod.Unit,
        ////               BaseCost             = cod.BaseCost,
        ////               CostGroup            = cod.CostGroup,
        ////               UseWeight            = cod.UseWeight
        ////           };
        ////    // return result as list
        ////    // --- --- ---
        ////    return result.ToList();
        ////}


        ////public IEnumerable<V_CostDestination_Entity> V_CostDestination(int? destinationStationId, int? lastCarrierCodeId, int? lastVendorId)
        ////{
        ////    // create query
        ////    // --- --- ---
        ////    IQueryable<V_CostDestination_Entity> result =
        ////      from cd in _context.CostDestinationEntitys
        ////      join cdd in _context.CostDestinationDetailEntitys on cd.CostDestinationId equals cdd.CostDestinationId
        ////      where cd.DestinationStationId.Equals(destinationStationId) && cd.LastCarrierCodeId.Equals(lastCarrierCodeId) && cd.LastVendorId.Equals(lastVendorId)

        ////      select new V_CostDestination_Entity
        ////      {
        ////          CostDestinationId         = cd.CostDestinationId,
        ////          DestinationStationId      = cd.DestinationStationId,
        ////          LastCarrierCodeId         = cd.LastCarrierCodeId,
        ////          LastVendorId              = cd.LastVendorId,
        ////          CostDestinationDetailId   = cdd.CostDestinationDetailId,
        ////          CostType                  = cdd.CostType,
        ////          Unit                      = cdd.Unit,
        ////          BaseCost                  = cdd.BaseCost,
        ////          CostGroup                 = cdd.CostGroup,
        ////          UseWeight                 = cdd.UseWeight
        ////      };
        ////    // return result as list
        ////    // --- --- ---
        ////    return result.ToList();
        ////}

        ////public IEnumerable<V_CostStation_Entity> V_CostStation(int? stationId)
        ////{
        ////    // create query
        ////    // --- --- ---
        ////    IQueryable<V_CostStation_Entity> result =
        ////      from cd in _context.CostStationEntitys
        ////      join cdd in _context.CostStationDetailEntitys on cd.CostStationId equals cdd.CostStationId
        ////      where cd.StationId.Equals(stationId)

        ////      select new V_CostStation_Entity
        ////      {
        ////          CostStationId         = cd.CostStationId,
        ////          CostStationDetailId   = cdd.CostStationDetailId,
        ////          StationId             = cd.StationId,
        ////          CostType              = cdd.CostType,
        ////          Unit                  = cdd.Unit,
        ////          BaseCost              = cdd.BaseCost,
        ////          CostGroup             = cdd.CostGroup,
        ////          UseWeight             = cdd.UseWeight
        ////      };
        ////    // return result as list
        ////    // --- --- ---
        ////    return result.ToList();
        ////}

        //////public IEnumerable<V_CostCommodity_Entity> V_CostCommodity(int? vendorId, String? lookupKey)
        //////{
        //////    // create query
        //////    // --- --- ---
        //////    IQueryable<V_CostCommodity_Entity> result =
        //////      from cd in _context.CostCommodityEntitys
        //////      join cdd in _context.CostCommodityDetailEntitys on cd.CostCommodityId equals cdd.CostCommodityId
        //////      where cd.LookUpKey.Equals(lookupKey)

        //////      select new V_CostCommodity_Entity
        //////      {
        //////          CostCommodityId = cd.CostCommodityId,
        //////          CostCommodityDetailId = cdd.CostCommodityDetailId,
        //////          LookUpKey = cd.LookUpKey,
        //////          CostType = cdd.CostType,
        //////          Unit = cdd.Unit,
        //////          BaseCost = cdd.BaseCost,
        //////          CostGroup = cdd.CostGroup,
        //////          UseWeight = cdd.UseWeight
        //////      };
        //////    // return result as list
        //////    // --- --- ---
        //////    return result.ToList();
        //////}

        ////private ManifestCarrierEntity Create_ManifestCarrier(CreateManifestCarrierCommand req)
        ////{
        ////    // set new object
        ////    // --- --- ---
        ////    var entity = new ManifestCarrierEntity();

        ////    entity.CreatedBy                = req.uiUserProfile.userId.ToString();
        ////    entity.FromSendDate             = req.StartDateUtc();
        ////    entity.ToSendDate               = req.EndDateUtc();
        ////    entity.OriginStationId          = req.originStationId;
        ////    entity.FirstVendorId            = req.firstVendorId;
        ////    entity.DestinationStationId     = req.destinationStationId;
        ////    entity.LastVendorId             = req.lastVendorId;
        ////    entity.NoSmu                    = req.noSmu;
        ////    entity.FlightDate               = req.flightDate;
        ////    entity.FinalWeightSmu           = req.finalWeight;
        ////    entity.FirstCarrierId           = req.firstCarrierId;
        ////    entity.LastCarrierId            = req.lastCarrierId;

        ////    //save
        ////    _context.ManifestCarrierEntitys.Add(entity);
        ////    _context.SaveChanges();
        ////    //
        ////    // return
        ////    return entity;
        ////}

        ////private List<ManifestCarrierItemEntity> Create_ManifestCarrierItem(CreateManifestCarrierCommand req)
        ////{
        ////    List<ManifestCarrierItemEntity> entity = new List<ManifestCarrierItemEntity>();
        ////    // foreach every manifest from user request
        ////    // --- --- ---
        ////    foreach (var item in req.Manifests)
        ////    {
        ////        var itemEntity = new ManifestCarrierItemEntity();

        ////        itemEntity.CreatedBy            = req.uiUserProfile.userId.ToString();
        ////        itemEntity.ManifestCarrierId    = req.manifestCarrierId;
        ////        itemEntity.ManifestId           = item.manifestId;
        ////        itemEntity.Caw                  = item.caw;
        ////        itemEntity.CawFinal             = item.weightSmu;
        ////        entity.Add(itemEntity);

        ////        //save
        ////        _context.ManifestCarrierItemEntitys.Add(itemEntity);
        ////    }

        ////    //save
        ////    _context.SaveChanges();
        ////    //
        ////    // return
        ////    return entity;
        ////}

        ////public Int32? GetDestinationBranch(Int32? DestinationCode, String? Carrier)
        ////{
        ////    var query = from cc in _context.CompanyCityEntitys
        ////                join bc in _context.BranchCityEntitys on cc.CityId equals bc.CityId
        ////                where cc.CompanyCityId.Equals(DestinationCode) && bc.Carrier.Equals(Carrier)

        ////                select bc.BranchId;
        ////    var res = query.FirstOrDefault();

        ////    return res;
        ////}

        ////private List<ManifestCarrierCostEntity> Create_CostCarrier_ManifestCarrierCost(CreateManifestCarrierCommand req, List<ManifestCarrierItemEntity> saveCarrierDetail)
        ////{
        ////    IEnumerable<V_CostStation_Entity> V_CostStation_first   = null;
        ////    IEnumerable<V_CostStation_Entity> V_CostStation_last    = null;
        ////    IEnumerable<V_CostCarrier_Entity> V_CostCarrier_first   = null;
        ////    IEnumerable<V_CostCarrier_Entity> V_CostCarrier_last    = null;
        ////    IEnumerable<V_CostDestination_Entity> v_CostDestination = null;
        ////    IEnumerable<V_CostOrigin_Entity> v_CostOrigin           = null;
        ////    IEnumerable<V_CostOps_Entity> v_CostOps                 = null;

        ////    foreach (var item in req.Manifests)
        ////    {
        ////        // get cost origin from V_CostOrigin
        ////        // --- --- ---
        ////        v_CostOrigin = this.V_CostOrigin(req.originStationId, req.firstCarrierCodeId, req.firstVendorId);

        ////        // get cost ops from V_CostOps
        ////        // --- --- ---
        ////        v_CostOps = this.V_CostOps(req.originStationId);

        ////        // get cost destination from V_CostDestination
        ////        // --- --- ---
        ////        v_CostDestination = this.V_CostDestination(req.destinationStationId, req.lastCarrierCodeId, req.lastVendorId);

        ////        // get cost station from Station
        ////        // --- --- ---
        ////        V_CostStation_first = this.V_CostStation(req.firstTransitStationId);
        ////        V_CostStation_last  = this.V_CostStation(req.lastTransitStationId);
        ////    }

        ////    // get cost station from Station
        ////    // --- --- ---
        ////    V_CostCarrier_first = this.V_CostCarrierFirst(req.destinationStationId, req.firstCarrierId, req.firstCarrierCodeId, req.firstVendorId);
        ////    V_CostCarrier_last  = this.V_CostCarrierLast(req.destinationStationId, req.lastCarrierId, req.lastCarrierCodeId, req.lastVendorId);

        ////    List<ManifestCarrierCostEntity> entity = new List<ManifestCarrierCostEntity>();
        ////    // foreach data from table ManifestCarrierItem
        ////    // --- --- ---
        ////    foreach (var item in saveCarrierDetail)
        ////    {
        ////        // filter cost carrier
        ////        // --- --- ---
        ////        //var vCostCarrierFilter              = v_CostCarrier.Where(w => w.VendorId.Equals(req.vendorId) && w.FirstCarrier.Equals(req.firstCarrier) && w.LastCarrier.Equals(req.lastCarrier) && w.DestinationStation.Equals(req.DestinationStation)).ToList();
        ////        var vCostCarrierFirstFilter         = V_CostCarrier_first.ToList();
        ////        var vCostCarrierLastFilter          = V_CostCarrier_last.ToList();
        ////        var vCostCarrierOpsFilter           = v_CostOps.ToList();
        ////        var vCostCarrierOriginFilter        = v_CostOrigin.ToList();
        ////        var vCostCarrierDestinationFilter   = v_CostDestination.ToList();
        ////        var vCostStationFirstFilter         = V_CostStation_first.ToList();
        ////        var vCostStationLastFilter          = V_CostStation_last.ToList();
        ////        //var vCostCommodityFilter = V_CostCommodity.ToList();

        ////        this.SaveCostOps(req, item, vCostCarrierOpsFilter, entity);
        ////        this.SaveCostCarrierFirst(req, item, vCostCarrierFirstFilter, entity);
        ////        this.SaveCostCarrierLast(req, item, vCostCarrierLastFilter, entity);
        ////        this.SaveCostOrigin(req, item, vCostCarrierOriginFilter, entity);
        ////        this.SaveCostStation(req, item, vCostStationFirstFilter, entity);
        ////        this.SaveCostStation(req, item, vCostStationLastFilter, entity);
        ////        this.SaveCostDestination(req, item, vCostCarrierDestinationFilter, entity);

        ////        //this.SaveCostCommodity(req, item, vCostCommodityFilter, entity);
        ////    }

        ////    //save
        ////    _context.SaveChanges();
        ////    //

        ////    // return
        ////    return entity;
        ////}

        ////public void CaclCost(ManifestCarrierCostEntity item, decimal? baseCost, decimal? weight, string unit)
        ////{
        ////    if (unit == "unit_kg")
        ////    {
        ////        item.Cost = baseCost * weight;
        ////        item.Weight = weight;
        ////    }
        ////    else if (unit == "unit_smu")
        ////    {
        ////        item.Cost = baseCost;
        ////        item.Weight = weight;
        ////    }
        ////    else if (unit == "unit_percent")
        ////    {
        ////        item.Cost = baseCost;
        ////        item.Weight = weight;
        ////    }
        ////    else
        ////    {
        ////        item.Cost = baseCost;
        ////        item.Weight = 1;
        ////    }
        ////}

        ////public void CaclCarrier1Percent(ManifestCarrierCostEntity item, Decimal? baseCost, Decimal? carrierCost)
        ////{
        ////    item.Cost = (baseCost * carrierCost) / 100;
        ////}


        ////public void SaveCostCarrierFirst(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, List<V_CostCarrier_Entity> vCostCarrierFilter, List<ManifestCarrierCostEntity> entity)
        ////{
        ////    decimal? costCarrier = 0;

        ////    // foreach data from table CostCarrier
        ////    // --- --- ---
        ////    foreach (var vCostCarrierFilterItem in vCostCarrierFilter)
        ////    {
        ////        ManifestCarrierCostEntity ItemEntity    = new ManifestCarrierCostEntity();
        ////        ItemEntity.CreatedBy                    = req.uiUserProfile.userId.ToString();
        ////        ItemEntity.ManifestCarrierId            = item.ManifestCarrierId;
        ////        ItemEntity.ManifestCarrierItemId        = item.ManifestCarrierItemId;
        ////        ItemEntity.CostType                     = vCostCarrierFilterItem.CostType;
        ////        ItemEntity.BaseCost                     = vCostCarrierFilterItem.BaseCost;
        ////        ItemEntity.Unit                         = vCostCarrierFilterItem.Unit;
        ////        ItemEntity.CostGroup                    = vCostCarrierFilterItem.CostGroup;
        ////        ItemEntity.UseWeight                    = vCostCarrierFilterItem.UseWeight;

        ////        if (vCostCarrierFilterItem.UseWeight == "smu") { this.CaclCost(ItemEntity, vCostCarrierFilterItem.BaseCost, item.CawFinal, vCostCarrierFilterItem.Unit); }
        ////        if (vCostCarrierFilterItem.UseWeight == "caw") { this.CaclCost(ItemEntity, vCostCarrierFilterItem.BaseCost, item.Caw, vCostCarrierFilterItem.Unit); }

        ////        if (vCostCarrierFilterItem.CostType == "cost_carrier_carrier")
        ////        {
        ////            costCarrier = ItemEntity.Cost;
        ////        } // get cost value for get ppn value
        ////        if (vCostCarrierFilterItem.CostType == "cost_carrier_ppn_1%")
        ////        {
        ////            this.CaclCarrier1Percent(ItemEntity, vCostCarrierFilterItem.BaseCost, costCarrier);
        ////        }
        ////        if (vCostCarrierFilterItem.CostType == "cost_carrier_ppn_10%")
        ////        {
        ////            this.CaclCarrier1Percent(ItemEntity, vCostCarrierFilterItem.BaseCost, costCarrier);
        ////        }


        ////        entity.Add(ItemEntity);
        ////        _context.ManifestCarrierCostEntitys.Add(ItemEntity);
        ////    }
        ////    //// save data per manifest
        ////}

        ////public void SaveCostCarrierLast(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, List<V_CostCarrier_Entity> vCostCarrierFilter, List<ManifestCarrierCostEntity> entity)
        ////{
        ////    decimal? costCarrier = 0;

        ////    // foreach data from table CostCarrier
        ////    // --- --- ---
        ////    foreach (var vCostCarrierFilterItem in vCostCarrierFilter)
        ////    {
        ////        ManifestCarrierCostEntity ItemEntity = new ManifestCarrierCostEntity();
        ////        ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
        ////        ItemEntity.ManifestCarrierId = item.ManifestCarrierId;
        ////        ItemEntity.ManifestCarrierItemId = item.ManifestCarrierItemId;
        ////        ItemEntity.CostType = vCostCarrierFilterItem.CostType;
        ////        ItemEntity.BaseCost = vCostCarrierFilterItem.BaseCost;
        ////        ItemEntity.Unit = vCostCarrierFilterItem.Unit;
        ////        ItemEntity.CostGroup = vCostCarrierFilterItem.CostGroup;
        ////        ItemEntity.UseWeight = vCostCarrierFilterItem.UseWeight;

        ////        if (vCostCarrierFilterItem.UseWeight == "smu") { this.CaclCost(ItemEntity, vCostCarrierFilterItem.BaseCost, item.CawFinal, vCostCarrierFilterItem.Unit); }
        ////        if (vCostCarrierFilterItem.UseWeight == "caw") { this.CaclCost(ItemEntity, vCostCarrierFilterItem.BaseCost, item.Caw, vCostCarrierFilterItem.Unit); }

        ////        if (vCostCarrierFilterItem.CostType == "cost_carrier_last")
        ////        {
        ////            costCarrier = ItemEntity.Cost;
        ////        } // get cost value for get ppn value
        ////        if (vCostCarrierFilterItem.CostType == "cost_carrier_last_ppn_1%")
        ////        {
        ////            this.CaclCarrier1Percent(ItemEntity, vCostCarrierFilterItem.BaseCost, costCarrier);
        ////        }
        ////        if (vCostCarrierFilterItem.CostType == "cost_carrier_last_ppn_10%")
        ////        {
        ////            this.CaclCarrier1Percent(ItemEntity, vCostCarrierFilterItem.BaseCost, costCarrier);
        ////        }


        ////        entity.Add(ItemEntity);
        ////        _context.ManifestCarrierCostEntitys.Add(ItemEntity);
        ////    }
        ////    //// save data per manifest
        ////}

        ////public void SaveCostOrigin(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, List<V_CostOrigin_Entity> vCostCarrierOriginFilter, List<ManifestCarrierCostEntity> entity)
        ////{
        ////    // foreach data from table CostCarrier
        ////    // --- --- ---
        ////    foreach (var vCostCarrierOriginFilterItem in vCostCarrierOriginFilter)
        ////    {
        ////        ManifestCarrierCostEntity ItemEntity = new ManifestCarrierCostEntity();

        ////        ItemEntity.CreatedBy                = req.uiUserProfile.userId.ToString();
        ////        ItemEntity.ManifestCarrierId        = item.ManifestCarrierId;
        ////        ItemEntity.ManifestCarrierItemId    = item.ManifestCarrierItemId;
        ////        ItemEntity.CostType                 = vCostCarrierOriginFilterItem.CostType;
        ////        ItemEntity.BaseCost                 = vCostCarrierOriginFilterItem.BaseCost;
        ////        ItemEntity.Unit                     = vCostCarrierOriginFilterItem.Unit;
        ////        ItemEntity.CostGroup                = vCostCarrierOriginFilterItem.CostGroup;
        ////        ItemEntity.UseWeight                = vCostCarrierOriginFilterItem.UseWeight;

        ////        if (vCostCarrierOriginFilterItem.UseWeight == "smu") { this.CaclCost(ItemEntity, vCostCarrierOriginFilterItem.BaseCost, item.CawFinal, vCostCarrierOriginFilterItem.Unit); }
        ////        if (vCostCarrierOriginFilterItem.UseWeight == "caw") { this.CaclCost(ItemEntity, vCostCarrierOriginFilterItem.BaseCost, item.Caw, vCostCarrierOriginFilterItem.Unit); }

        ////        entity.Add(ItemEntity);
        ////        _context.ManifestCarrierCostEntitys.Add(ItemEntity);
        ////    }
        ////    //// save data per manifest
        ////}


        ////public void SaveCostOps(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, List<V_CostOps_Entity> vCostCarrierOpsFilter, List<ManifestCarrierCostEntity> entity)
        ////{
        ////    // foreach data from table CostCarrier
        ////    // --- --- ---
        ////    foreach (var vCostCarrierOpsFilterItem in vCostCarrierOpsFilter)
        ////    {
        ////        ManifestCarrierCostEntity ItemEntity = new ManifestCarrierCostEntity();

        ////        ItemEntity.CreatedBy                = req.uiUserProfile.userId.ToString();
        ////        ItemEntity.ManifestCarrierId        = item.ManifestCarrierId;
        ////        ItemEntity.ManifestCarrierItemId    = item.ManifestCarrierItemId;
        ////        ItemEntity.CostType                 = vCostCarrierOpsFilterItem.CostType;
        ////        ItemEntity.BaseCost                 = vCostCarrierOpsFilterItem.BaseCost;
        ////        ItemEntity.Unit                     = vCostCarrierOpsFilterItem.Unit;
        ////        ItemEntity.CostGroup                = vCostCarrierOpsFilterItem.CostGroup;
        ////        ItemEntity.UseWeight                = vCostCarrierOpsFilterItem.UseWeight;

        ////        if (vCostCarrierOpsFilterItem.UseWeight == "smu") { this.CaclCost(ItemEntity, vCostCarrierOpsFilterItem.BaseCost, item.CawFinal, vCostCarrierOpsFilterItem.Unit); }
        ////        if (vCostCarrierOpsFilterItem.UseWeight == "caw") { this.CaclCost(ItemEntity, vCostCarrierOpsFilterItem.BaseCost, item.Caw, vCostCarrierOpsFilterItem.Unit); }

        ////        entity.Add(ItemEntity);
        ////        _context.ManifestCarrierCostEntitys.Add(ItemEntity);
        ////    }
        ////    //// save data per manifest
        ////}

        ////public void SaveCostDestination(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, List<V_CostDestination_Entity> vCostCarrierDestinationFilter, List<ManifestCarrierCostEntity> entity)
        ////{
        ////    // foreach data from table CostCarrier
        ////    // --- --- ---
        ////    foreach (var vCostCarrierDestinationFilterItem in vCostCarrierDestinationFilter)
        ////    {
        ////        ManifestCarrierCostEntity ItemEntity = new ManifestCarrierCostEntity();
        ////        ItemEntity.CreatedBy                = req.uiUserProfile.userId.ToString();
        ////        ItemEntity.ManifestCarrierId        = item.ManifestCarrierId;
        ////        ItemEntity.ManifestCarrierItemId    = item.ManifestCarrierItemId;
        ////        ItemEntity.CostType                 = vCostCarrierDestinationFilterItem.CostType;
        ////        ItemEntity.BaseCost                 = vCostCarrierDestinationFilterItem.BaseCost;
        ////        ItemEntity.Unit                     = vCostCarrierDestinationFilterItem.Unit;
        ////        ItemEntity.CostGroup                = vCostCarrierDestinationFilterItem.CostGroup;
        ////        ItemEntity.UseWeight                = vCostCarrierDestinationFilterItem.UseWeight;

        ////        if (vCostCarrierDestinationFilterItem.UseWeight == "smu") { this.CaclCost(ItemEntity, vCostCarrierDestinationFilterItem.BaseCost, item.CawFinal, vCostCarrierDestinationFilterItem.Unit); }
        ////        if (vCostCarrierDestinationFilterItem.UseWeight == "caw") { this.CaclCost(ItemEntity, vCostCarrierDestinationFilterItem.BaseCost, item.Caw, vCostCarrierDestinationFilterItem.Unit); }

        ////        entity.Add(ItemEntity);
        ////        _context.ManifestCarrierCostEntitys.Add(ItemEntity);
        ////    }
        ////    //// save data per manifest
        ////}

        ////public void SaveCostStation(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, List<V_CostStation_Entity> vCostStationFilter, List<ManifestCarrierCostEntity> entity)
        ////{
        ////    // foreach data from table CostCarrier
        ////    // --- --- ---
        ////    foreach (var vCostCarrierStationFilterItem in vCostStationFilter)
        ////    {
        ////        ManifestCarrierCostEntity ItemEntity = new ManifestCarrierCostEntity();

        ////        ItemEntity.CreatedBy                = req.uiUserProfile.userId.ToString();
        ////        ItemEntity.ManifestCarrierId        = item.ManifestCarrierId;
        ////        ItemEntity.ManifestCarrierItemId    = item.ManifestCarrierItemId;
        ////        ItemEntity.CostType                 = vCostCarrierStationFilterItem.CostType;
        ////        ItemEntity.BaseCost                 = vCostCarrierStationFilterItem.BaseCost;
        ////        ItemEntity.Unit                     = vCostCarrierStationFilterItem.Unit;
        ////        ItemEntity.CostGroup                = vCostCarrierStationFilterItem.CostGroup;
        ////        ItemEntity.UseWeight                = vCostCarrierStationFilterItem.UseWeight;

        ////        if (vCostCarrierStationFilterItem.UseWeight == "smu") { this.CaclCost(ItemEntity, vCostCarrierStationFilterItem.BaseCost, item.CawFinal, vCostCarrierStationFilterItem.Unit); }
        ////        if (vCostCarrierStationFilterItem.UseWeight == "caw") { this.CaclCost(ItemEntity, vCostCarrierStationFilterItem.BaseCost, item.Caw, vCostCarrierStationFilterItem.Unit); }

        ////        entity.Add(ItemEntity);
        ////        _context.ManifestCarrierCostEntitys.Add(ItemEntity);
        ////    }
        ////    //// save data per manifest
        ////}

        //////public void SaveCostCommodity(CreateManifestCarrierCommand req, ManifestCarrierItemEntity item, List<V_CostCommodity_Entity> vCostCommodityFilter, List<ManifestCarrierCostEntity> entity)
        //////{
        //////    decimal? costCarrier = 0;

        //////    // foreach data from table CostCarrier
        //////    // --- --- ---
        //////    foreach (var vCostCarrierCommodityFilterItem in vCostCommodityFilter)
        //////    {
        //////        ManifestCarrierCostEntity ItemEntity = new ManifestCarrierCostEntity();
        //////        ItemEntity.CreatedBy = req.uiUserProfile.userId.ToString();
        //////        ItemEntity.ManifestCarrierId = item.ManifestCarrierId;
        //////        ItemEntity.ManifestCarrierItemId = item.ManifestCarrierItemId;
        //////        ItemEntity.CostType = vCostCarrierCommodityFilterItem.CostType;
        //////        ItemEntity.BaseCost = vCostCarrierCommodityFilterItem.BaseCost;
        //////        ItemEntity.Unit = vCostCarrierCommodityFilterItem.Unit;
        //////        ItemEntity.CostGroup = vCostCarrierCommodityFilterItem.CostGroup;
        //////        ItemEntity.UseWeight = vCostCarrierCommodityFilterItem.UseWeight;

        //////        costCarrier = ItemEntity.Cost;

        //////        if (vCostCarrierCommodityFilterItem.CostType == "cost_commodity")
        //////        {
        //////            this.CaclCarrier1Percent(ItemEntity, vCostCarrierCommodityFilterItem.BaseCost, costCarrier);
        //////        }

        //////        if (vCostCarrierCommodityFilterItem.UseWeight == "smu") { this.CaclCost(ItemEntity, vCostCarrierCommodityFilterItem.BaseCost, item.CawFinal, vCostCarrierCommodityFilterItem.Unit); }
        //////        if (vCostCarrierCommodityFilterItem.UseWeight == "caw") { this.CaclCost(ItemEntity, vCostCarrierCommodityFilterItem.BaseCost, item.Caw, vCostCarrierCommodityFilterItem.Unit); }

        //////        entity.Add(ItemEntity);
        //////        _context.ManifestCarrierCostEntitys.Add(ItemEntity);
        //////    }
        //////    //// save data per manifest
        //////}

        ////public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        ////{
        ////    // Mark as Changed
        ////    HistoryActionEntity dataItem = new HistoryActionEntity();

        ////    dataItem.UserId = UserId;
        ////    dataItem.ActionDate = DateTime.UtcNow;
        ////    dataItem.ActionId = Convert.ToInt32(ActionId);
        ////    dataItem.ActionDesc = ActionDesc;
        ////    dataItem.Menu = Menu;
        ////    dataItem.Desc = Desc;

        ////    _context.HistoryActionEntitys.Add(dataItem);
        ////    _context.SaveChanges();
        ////}

    }
}
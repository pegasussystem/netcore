﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;


namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Station.Station.Command
{
    public partial class ManifestCarrierMasterStationCreateCommand : IRequest<ReturnFormat>
    {
        public String? stationName { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterStationCreateCommandHandler : IRequestHandler<ManifestCarrierMasterStationCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterStationCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterStationCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.StationCreate(req.stationName);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_StationCreateEntity> StationCreate(String? stationName )
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_StationCreate @stationName='{stationName}' ";
            var res = this._context.SP_ManifestCarrierMaster_StationCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_CostListEntity
    {
        public Int64 costId { get; set; }
        public DateTime createdAt { get; set; }
        public String? coaCode { get; set; }
        public String? coaName { get; set; }
        public Decimal? total { get; set; }
        public String? digitPertama { get; set; }
        public String? digitKedua { get; set; }
        public String? digitKetiga { get; set; }
        public String? digitKeempat { get; set; }
        public String? digitKelima { get; set; }
        public String? digitKeenam { get; set; }

    }
}

using Core.Entity.Main.View;
using Core.Entity.Main;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interface.Repo.Main
{
    public interface ICoaparentRepo : IRepo<CoaParentEntity>
    {
        Task<IEnumerable<CoaParentEntity>> GetAllAsync();

    }
}

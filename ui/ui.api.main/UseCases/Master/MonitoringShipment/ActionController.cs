﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.Master.MonitoringShipment.Commands;
using core.app.UseCases.Master.MonitoringShipment.Query;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Master.MonitoringShipment
{
    [Authorize]
    [Route("monitoring-shipment")]
    [ApiController]

    public class ActionController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public ActionController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] MonitoringShipmentCreateCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] MonitoringShipmentUpdateCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete([FromBody] MonitoringShipmentDeleteCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

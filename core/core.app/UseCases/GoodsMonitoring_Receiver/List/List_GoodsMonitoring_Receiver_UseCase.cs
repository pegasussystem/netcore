﻿using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Interface.App.Main.GoodsMonitoring_Receiver;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace core.app.UseCases.GoodsMonitoring_Receiver.List
{
    class ListGoodsMonitoringReceiverUseCase : IListGoodsMonitoringReceiverUseCase
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public ListGoodsMonitoringReceiverUseCase(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }


        ////---------------------------- Sudah tidak dipake di pindahkan ke Core.app -> UseCase -> GoodsMonitoring -> ListReceiver -> ListReceiverQuery -------------------------------//
        //public ReturnFormat List(UiUserProfileEntity profile, Object obj)
        //{

        //    // --- --- ---
        //    // variable
        //    // --- --- ---
        //    IEnumerable<ListGoodsMonitoringReceiverEntity> entities;
        //    ListGoodsMonitoringReceiverInput input = obj as ListGoodsMonitoringReceiverInput;
        //    ReturnFormat rtn = new ReturnFormat
        //    {
        //        Status = StatusCodes.Status204NoContent
        //    };


        //    // --- --- ---
        //    // logic
        //    // --- --- ---
        //    entities = _repoPs.ManifestRepo.List_MonitoringGoods_Receiver(input.Year(), input.Month(), input.Day(), input.DataValue , profile.companyId);


        //    // --- --- ---
        //    // return
        //    // --- --- ---
        //    if (entities != null)
        //    {
        //        rtn.Status = StatusCodes.Status200OK;
        //        rtn.Data = entities;
        //    }
        //    return rtn;
        //}




        public ReturnFormat Create(ListGoodsMonitoringReceiverEntity entity)
        {
            throw new NotImplementedException();
        }

        public ListGoodsMonitoringReceiverEntity Delete(ListGoodsMonitoringReceiverEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(ListGoodsMonitoringReceiverEntity entity)
        {
            throw new NotImplementedException();
        }

        public ListGoodsMonitoringReceiverEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public ListGoodsMonitoringReceiverEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(ListGoodsMonitoringReceiverEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

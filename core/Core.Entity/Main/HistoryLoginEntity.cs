﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class HistoryLoginEntity
    {
        public Int32? HistoryLoginId { get; set; }
        public Int32? UserLogin { get; set; }
        public DateTime? LoginDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public String? Menu { get; set; }

    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
     class MappingAreaNoExistsConfig : IEntityTypeConfiguration<MappingAreaNoExistsEntity>
    {
        public void Configure(EntityTypeBuilder<MappingAreaNoExistsEntity> builder)
        {
            builder.HasKey(e => e.MappingAreaNoExistsId);
            builder.ToTable("MappingAreaNoExists");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
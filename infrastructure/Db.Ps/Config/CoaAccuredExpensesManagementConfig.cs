using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CoaAccuredExpensesManagementConfig : IEntityTypeConfiguration<CoaAccuredExpensesManagementEntity>
    {
        public void Configure(EntityTypeBuilder<CoaAccuredExpensesManagementEntity> builder)
        {
            builder.HasKey(e => e.AccuredExpensesId);
            builder.ToTable("CoaAccuredExpensesManagement");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_DividedListEntity
    {
        public Int32 TransportId { get; set; }
        public String ModaId { get; set; }
        public String Moda { get; set; }
        public Int32 DividedValue { get; set; }
        public Int32 Order { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_BukuKasCoaHierarchyCurrentMonthEntity
    {
        public Int64? bukuKasId { get; set; }
        public String? tanggal { get; set; }
        public String? coaCode { get; set; }
        public String? coaParentName { get; set; }
        public String? coaName { get; set; }
        public String? keterangan { get; set; }
        public Decimal? total { get; set; }
        public String? karakterPertama { get; set; }
        public String? karakterKedua { get; set; }
        public String? karakterKetiga { get; set; }
        public String? karakterKeempat { get; set; }
        public String? karakterKelima { get; set; }
        public String? karakterKeenam { get; set; }
        public String? karakterKetujuh { get; set; }
        public String? karakterKedelapan { get; set; }

    }
}

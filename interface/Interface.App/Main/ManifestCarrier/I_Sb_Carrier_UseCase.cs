﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main
{

    public interface I_Sb_Carrier_UseCase : IApp<ManifestCarrierEntity>
    {
        public ReturnFormat Read(UiUserProfileEntity uiUserProfileEntity);
    }
}

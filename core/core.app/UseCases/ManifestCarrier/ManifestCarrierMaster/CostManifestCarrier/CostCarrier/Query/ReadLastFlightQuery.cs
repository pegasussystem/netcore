﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using MediatR;
using core.app.Common.Interfaces;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostCarrier.Query
{
    public partial class ReadLastFlightQuery : IRequest<ReturnFormat>
    {
        public Int32? carrierId { get; set; }
        public Int32? firstFlightId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadLastFlightQueryHandler : IRequestHandler<ReadLastFlightQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadLastFlightQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadLastFlightQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                rtn.Status = StatusCodes.Status200OK;

                var qr = from a in _context.FlightRouteEntitys
                         let lastFlightId = a.FourthFlightId ?? a.ThirdFlightId ?? a.SecondFlightId ?? a.FirstFlightId
                         join c in _context.FlightEntitys on lastFlightId equals c.FlightId
                         where a.CarrierId == req.carrierId && a.FirstFlightId == req.firstFlightId
                         select new
                         {
                             c.FlightId,
                             c.FlightNo
                         };

                if (qr != null)
                {
                    var query = qr.Distinct();
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.FlightId
                            ,
                            s.FlightNo
                        }).OrderBy(o => o.FlightId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
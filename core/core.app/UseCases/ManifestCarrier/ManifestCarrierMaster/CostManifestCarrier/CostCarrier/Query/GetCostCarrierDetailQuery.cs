﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostCarrier.Query
{
    public partial class GetCostCarrierDetailQuery : IRequest<ReturnFormat>
    {

        public Int32? originStationId { get; set; }
        public Int32? firstFlightId { get; set; }
        public Int32? lastFlightId { get; set; }
        public Int32? vendorId { get; set; }
        public Int32? carrierId { get; set; }
        public Int32? destinationStationId { get; set; }
    }

    public class GetCostCarrierDetailQueryHandler : IRequestHandler<GetCostCarrierDetailQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetCostCarrierDetailQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetCostCarrierDetailQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var vendorCarrierId = _context.VendorCarrierEntitys
       .Where(vc => vc.CarrierId == req.carrierId && vc.VendorId == req.vendorId)
       .Select(vc => vc.VendorCarrierId)
       .FirstOrDefault();


                var query = from a in _context.CostCarrierEntitys
                            join b in _context.CostCarrierDetailEntitys on a.CostCarrierId equals b.CostCarrierId
                            join vc in _context.VendorCarrierEntitys on a.VendorCarrierId equals vc.VendorCarrierId
                            where a.OriginStationId == req.originStationId
                                  && a.FirstFlightId == req.firstFlightId
                                  && a.LastFlightId == req.lastFlightId
                                  && a.CarrierId == req.carrierId
                                  && a.DestinationStationId == req.destinationStationId
                                   && a.VendorCarrierId == vendorCarrierId
                            select new
                            {
                                CarrierIdA = a.CarrierId,  // Rename this property
                                BaseCost = b.BaseCost,
                                CarrierIdVC = vc.CarrierId // Rename this property
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {

                            s.BaseCost
                            ,
                            s.CarrierIdA
                            ,
                            s.CarrierIdVC
                        }).OrderByDescending(o => o.CarrierIdA);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;

namespace core.app.UseCases.MasterCity.Queries

{

    public partial class ListProvinceQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; } 
    }
    public class ListProvinceQueryHandler : IRequestHandler<ListProvinceQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListProvinceQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListProvinceQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;



            var query = from a in _context.MasterProvinceEntitys

                        select new
                        {
                            a.MasterProvinceId,
                            a.MasterProvinceCode,
                            a.MasterProvinceName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.MasterProvinceId,
                        s.MasterProvinceCode,
                        s.MasterProvinceName

                    }).OrderByDescending(o => o.MasterProvinceId);
            }

            // return 
            return rtn;
        }


    }

}

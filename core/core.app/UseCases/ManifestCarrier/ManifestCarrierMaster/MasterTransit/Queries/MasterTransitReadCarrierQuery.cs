﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit.Queries
{
    public partial class MasterTransitReadCarrierQuery : IRequest<ReturnFormat>
    {
        public Int32? vendorCarrierId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MasterTransitReadCarrierQueryHandler : IRequestHandler<MasterTransitReadCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MasterTransitReadCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MasterTransitReadCarrierQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // --- --- --- --- ---
            var qr = from s in _context.CarrierEntitys
                        join v in _context.VendorCarrierEntitys on s.CarrierId equals v.CarrierId
                        where v.VendorId.Equals(req.vendorCarrierId)
                        select new
                        {
                            s.CarrierId
                            , CarrierName = s.CarrierCode + " - " + s.CarrierName
                        };

            if (qr != null)
            {
                var query = qr.Distinct();
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CarrierId
                        , s.CarrierName
                    }).OrderBy(o => o.CarrierId);
            }

            //
            // return
            return rtn;
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Privilege.Commands
{
    public partial class SavePrivilegeCommand : IRequest<ReturnFormat>
    {
        public String? privilegeName { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class SavePrivilegeCommandHandler : IRequestHandler<SavePrivilegeCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public SavePrivilegeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(SavePrivilegeCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.CreatePrivilege(req.privilegeName, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_SaveDataPrivilegeEntity> CreatePrivilege(String? privilegeName, Int32? loginId)
        {
            String query = $@"EXEC SP_SaveDataPrivilege @privilegeName='{privilegeName}', @loginId='{loginId}' ";
            var res = this._context.SP_SaveDataPrivilegeEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

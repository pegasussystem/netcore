﻿using core.app.Common.Interfaces;
using core.helper;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main;
using Core.Entity.Ui;

namespace core.app.UseCases.Base
{
    public partial class CreateHistoryLoginCommand : IRequest<ReturnFormat>
    {
        public DateTime? loginDate { get; set; }
        public String? menu { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateHistoryLoginCommandHandler : IRequestHandler<CreateHistoryLoginCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateHistoryLoginCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateHistoryLoginCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                HistoryLoginEntity dataItem = new HistoryLoginEntity();

                dataItem.UserLogin = req.uiUserProfile.userId;
                dataItem.LoginDate = DateTime.UtcNow;
                dataItem.StartTime = DateTime.UtcNow;
                dataItem.Menu = req.menu;

                _context.HistoryLoginEntitys.Add(dataItem);
                _context.SaveChanges();

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = dataItem.HistoryLoginId;

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }
    }

}
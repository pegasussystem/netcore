﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class SuperVisionEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? SuperVisionId { get; set; }
        public Int64? SpbId { get; set; }
        public String? Type { get; set; }
        public String? Reason { get; set; }
        public Int32? PicCreatedBy { get; set; }
        public DateTime? PicCreatedDate { get; set; }
        public Int32? ApproveBy { get; set; }
        public DateTime? ApproveDate { get; set; }
        public String? ReasonReject { get; set; }
        public Int32? Status { get; set; }
        public String? Param1 { get; set; }
        public String? Param2 { get; set; }
        public String? Param3 { get; set; }
        public String? Param4 { get; set; }
        public String? Param5 { get; set; }
        public String? Param6 { get; set; }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MonitoringShipment.Query
{
    public partial class MonitoringShipmentReadCorporateQuery : IRequest<ReturnFormat>
    {
        public String? cityId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MonitoringShipmentReadCorporateQueryHandler : IRequestHandler<MonitoringShipmentReadCorporateQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MonitoringShipmentReadCorporateQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MonitoringShipmentReadCorporateQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.SubBranchEntitys
                        join b in _context.CompanyCityEntitys on a.CityIdCode equals b.CompanyCityId
                        where b.CityId.Equals(req.cityId) && a.IsCorporate.Equals(true) && a.IsActive != true

                        select new
                        {

                            CorporateId = a.SubBranchId
                            , active = a.IsActive
                            , CorporateName = "[ " + a.SubBranchCode + " ] - " + a.SubBranchName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CorporateId,
                        s.CorporateName

                    }).OrderBy(o => o.CorporateId);
            }

            // return
            return rtn;
        }
    }
}



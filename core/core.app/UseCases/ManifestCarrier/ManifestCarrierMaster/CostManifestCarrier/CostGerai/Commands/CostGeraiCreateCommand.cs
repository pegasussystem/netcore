﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostGerai.Commands
{
    public partial class CostGeraiCreateCommand : IRequest<ReturnFormat>
    {
        public String? manifestOriginCityId { get; set; }
        public String? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostGeraiCreateCommandHandler : IRequestHandler<CostGeraiCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostGeraiCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostGeraiCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostGeraiCreate(req.manifestOriginCityId, req.baseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostGeraiCreateEntity> CostGeraiCreate(String? manifestOriginCityId, String? baseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostGeraiCreate @manifestOriginCityId='{manifestOriginCityId}', @baseCost='{baseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostGeraiCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VBukuKasEntity
    {
        // CostDestination
        
        public String? tanggal { get; set; }
         public DateTime createdAt { get; set; }
        public Int64? bukuKasId { get; set; }
        public Int64? coaId { get; set; }
        public String? keterangan { get; set; }
        public Decimal? debit { get; set; }
        public Decimal? credit { get; set; }
        public Decimal? saldo { get; set; }
        public Decimal? cumulativeBalance { get; set; }
        public String? coaCode { get; set; }
        public String? coaName { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using MediatR;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Core.Entity.Main.Store_Procedure;

namespace core.app.UseCases.Configuration.Users.Commands
{
    public partial class ManageDataCommand : IRequest<ReturnFormat>
    {
        public Int32? userId { get; set; }
        public Int32? unitId { get; set; }
        public Int32? unitPlaceId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManageDataCommandHandler : IRequestHandler<ManageDataCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManageDataCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManageDataCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.ManageData(req.userId, req.unitId, req.unitPlaceId, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_UserManageDataEntity> ManageData(Int32? userId, Int32? unitId, Int32? unitPlaceId, Int32? loginId)
        {
            String query = $@"EXEC SP_UserManageData @userId={userId}, @unitId='{unitId}', @unitPlaceId={unitPlaceId}, @loginId='{loginId}' ";
            var res = this._context.SP_UserManageDataEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostPenerus.Commands
{
    public partial class CostPenerusCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? stationId { get; set; }
        public String? cityId { get; set; }
        public Int32? vendorId { get; set; }
        public Int32? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostPenerusCreateCommandHandler : IRequestHandler<CostPenerusCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostPenerusCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostPenerusCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostPenerusCreate(req.stationId, req.cityId, req.vendorId, req.baseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostPenerusCreateEntity> CostPenerusCreate(Int32? stationId, String? cityId, Int32? vendorId, Int32? baseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostPenerusCreate @stationDestinationId='{stationId}', @cityDestinationId='{cityId}', @vendorDestinationId='{vendorId}', @baseCost='{baseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostPenerusCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostCarrier.Query
{
    public partial class ListCostCarrierQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListCostCarrierQueryHandler : IRequestHandler<ListCostCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCostCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCostCarrierQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var query = from a in _context.CostCarrierEntitys
                            join b in _context.CostCarrierDetailEntitys on a.CostCarrierId equals b.CostCarrierId
                            join c in _context.CarrierEntitys on a.CarrierId equals c.CarrierId
                            join d in _context.StationEntitys on a.OriginStationId equals d.StationId
                            join e in _context.FlightEntitys on a.FirstFlightId equals e.FlightId
                            join f1 in _context.CarrierEntitys on e.CarrierId equals f1.CarrierId
                            join f in _context.FlightEntitys on a.LastFlightId equals f.FlightId
                            join f2 in _context.CarrierEntitys on f.CarrierId equals f2.CarrierId
                            join vc in _context.VendorCarrierEntitys on a.VendorCarrierId equals vc.VendorCarrierId
                            join g in _context.VendorEntitys on vc.VendorId equals g.VendorId
                            join h in _context.StationEntitys on a.DestinationStationId equals h.StationId
                            join i in _context.LookupEntitys on b.Unit equals i.LookupKey

                            join j in _context.StationCityEntitys on d.StationId equals j.StationId
                            join k in _context.MasterCityEntitys on j.CityId equals k.MasterCityId
                            
                            join l in _context.StationCityEntitys on h.StationId equals l.StationId
                            join m in _context.MasterCityEntitys on l.CityId equals m.MasterCityId
                            where i.LookupKeyParent.Equals("Unit") && j.Priority.Equals("1") && l.Priority.Equals("1")
                            select new
                            {
                                a.CostCarrierId
                                , b.CostCarrierDetailId
                                , CarrierName = "[ "+c.CarrierCode+" ] - " + c.CarrierName
                                , OriginStation = "[ " + k.MasterCityCode + " ] - " + d.StationName
                                , FirstFlightNo = f1.CarrierCode + " - "+ e.FlightNo
                                , LastFlightNo = f2.CarrierCode + " - " + f.FlightNo
                                , g.VendorName
                                , DestinationStation = "[ " + m.MasterCityCode + " ] - " + h.StationName
                                , i.LookupValue
                                , b.BaseCost
                                , b.UseWeight
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.CostCarrierId
                            , s.CostCarrierDetailId
                            , s.CarrierName
                            , s.OriginStation
                            , s.FirstFlightNo
                            , s.LastFlightNo
                            , s.VendorName
                            , s.DestinationStation
                            , s.LookupValue
                            , s.BaseCost
                            , s.UseWeight
                        }).OrderByDescending(o => o.CostCarrierId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
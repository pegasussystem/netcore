﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Core.Entity.Main
{
    public class RatesEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? RatesId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? RatesType { get; set; }
        public String? TypesOfGoods { get; set; }
        public String? Carrier { get; set; }
        public String? QualityOfService { get; set; }
        public String? TypeOfService { get; set; }
        public String? OriginCity { get; set; }
        public String? DestinationCity { get; set; }
        public Decimal? Rates { get; set; }


    }
}

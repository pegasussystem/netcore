﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main
{

    public interface IManifestReceiverApp : IApp<ManifestEntity>
    {
        //ReturnFormat List(VManifestEntity entity, UiUserProfileEntity uiUserProfileEntity);

        ReturnFormat ListDetail(VManifestDetailEntity entity, UiUserProfileEntity uiUserProfileEntity);

        ReturnFormat ListSpb(VManifestEntity entity, UiUserProfileEntity uiUserProfileEntity);

        ReturnFormat SpbCustomer(UiUserProfileEntity uiUserProfile, VSpbEntity entity);
    }
}

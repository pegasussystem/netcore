﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class V_HistoryPrintConfig : IEntityTypeConfiguration<V_HistoryPrintEntity>
    {
        public void Configure(EntityTypeBuilder<V_HistoryPrintEntity> builder)
        {
            builder.HasKey(e => e.HistoryPrintId);
            builder.ToTable("V_HistoryPrint");
        }
    }
}

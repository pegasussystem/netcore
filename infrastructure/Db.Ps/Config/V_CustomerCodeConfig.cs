﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_CustomerCodeConfig : IEntityTypeConfiguration<V_CustomerCodeEntity>
    {
        public void Configure(EntityTypeBuilder<V_CustomerCodeEntity> builder)
        {
            builder.HasKey(e => e.CustomerCode);
            builder.ToTable("V_CustomerCode");
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Monitoring.SpbList.Query
{
    public partial class ListMonitoringSpbQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public String? Type { get; set; }
        public Int32? CompanyId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class ListMonitoringSpbQueryHandler : IRequestHandler<ListMonitoringSpbQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListMonitoringSpbQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListMonitoringSpbQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_MonitoringSpbEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataMonitoringSpbList(req.uiUserProfile.userId, req.Type, req.uiUserProfile.companyId, req.CreatedAt.Value.Day, req.CreatedAt.Value.Month, req.CreatedAt.Value.Year);

            if (entities != null)
            {
               rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        s.ManifestId
                        , s.CompanyId
                        , s.BranchId
                        , s.OriginBranch
                        , s.SubBranchId
                        , s.Corporation
                        , s.Carrier
                        , s.ManifestAlphabet
                        , s.Alphabet
                        , s.OriginCityId
                        , s.OriginCityCode
                        , s.OriginCityName
                        , s.DestinationCityId
                        , s.DestinationCityCode
                        , s.DestinationCityName
                        , s.KoliGerai
                        , s.KoliCorporate
                        , s.totalKoli
                        , s.TotalAw
                        , s.TotalCaw
                        , s.AwGerai
                        , s.CawGerai
                        , s.AwCorporate
                        , s.CawCorporate
                        , s.Ta
                        , s.Tt
                        , s.Tk
                        , s.TotalTaTt
                    });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_MonitoringSpbEntity> DataMonitoringSpbList(Int32? UserId, String? Type, Int32? CompanyId, Int32? Day, Int32? Month, Int32? Year)
        {
            String query = $@"EXEC SP_MonitoringSpb @userId = '{UserId}', @type = '{Type}', @companyId = '{CompanyId}', @day='{Day}', @month = '{Month}', @year = '{Year}' ";
            var res = this._context.SP_MonitoringSpbEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
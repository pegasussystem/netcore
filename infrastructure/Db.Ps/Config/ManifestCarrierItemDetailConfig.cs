﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class ManifestCarrierItemDetailConfig : IEntityTypeConfiguration<ManifestCarrierItemDetailEntity>
    {
        public void Configure(EntityTypeBuilder<ManifestCarrierItemDetailEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierItemDetailId);
            builder.ToTable("ManifestCarrierItemDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

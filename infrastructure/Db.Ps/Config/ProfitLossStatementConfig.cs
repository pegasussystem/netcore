using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class ProfitLossStatementConfig : IEntityTypeConfiguration<ProfitLossStatementEntity>
    {
        public void Configure(EntityTypeBuilder<ProfitLossStatementEntity> builder)
        {
            builder.HasKey(e => e.plsId);
            builder.ToTable("ProfitLossStatement");
        }
    }
}

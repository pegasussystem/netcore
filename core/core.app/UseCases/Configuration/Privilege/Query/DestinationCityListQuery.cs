﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main.Store_Procedure;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Privilege
{
     public partial class DestinationCityListQuery : IRequest<ReturnFormat>
    {
        public Int32? rolesId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class DestinationCityListQueryHandler : IRequestHandler<DestinationCityListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public DestinationCityListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(DestinationCityListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_PrivilegeRoleListTujuanEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.PrivilegeRoleListTujuanListData(req.rolesId);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                        id = s.MasterCityId
                        , name = s.MasterCityType + " " + s.MasterCityName + ( s.MasterCityCapital == null ? "" : " [ "+ s.MasterCityCapital+" ]")
                        , s.RolesCode
                        , s.RolesName
                        , s.MasterCityCode
                        , s.MasterCityType
                        , s.MasterCityName
                        , s.MasterCityCapital
                        , s.ItemNumber
                        , s.OrderCode
                    });
            }

            // return
            return rtn;
        }

        public IEnumerable<SP_PrivilegeRoleListTujuanEntity> PrivilegeRoleListTujuanListData(Int32? rolesId)
        {
            String query = $@"EXEC SP_PrivilegeRoleListTujuan @rolesId={rolesId}";
            var res = this._context.SP_PrivilegeRoleListTujuanEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_UserAgreementEntity
    {
        public Int32? SubBranchId { get; set; }
        public String? SubBranchCode { get; set; }
        public String? SubBranchName { get; set; }
        public String? SubBranchAddress { get; set; }
        public Int32? IsAgree { get; set; }
        public Int32? IsActive { get; set; }
        public String? StatusAgree { get; set; }
    }
}

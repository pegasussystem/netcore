﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.GoodsMonitoring.Queries.ListReceiver
{
   public partial class ListReceiverQuery : IRequest<ReturnFormat>
    {
        public String? CreatedAt { get; set; }
        public String? DestinationCityDataValue { get; set; }
        public String? BlockCityDataValue { get; set; }

        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public int Year() { return Int32.Parse(CreatedAt.Split('-')[0]); }
        public int Month() { return Int32.Parse(CreatedAt.Split('-')[1]); }
        public int Day() { return Int32.Parse(CreatedAt.Split('-')[2]); }
    }

    public class ListReceiverQueryHandler : IRequestHandler<ListReceiverQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;
        
        public ListReceiverQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListReceiverQuery req, CancellationToken cancellationToken)
        {
            // --- --- ---
            // variable
            // --- --- ---
            IEnumerable<ListGoodsMonitoringReceiverEntity> entities;
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.ReceiverListData(req.Year(), req.Month(), req.Day(), req.DestinationCityDataValue, req.BlockCityDataValue, req.uiUserProfileEntity.companyId);


            // --- --- ---
            // return
            // --- --- ---
            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities
                    .Select(s => new {
                        s.ManifestId
                        , s.CompanyId
                        , s.BranchId
                        , s.OriginBranch
                        , s.DestinationBranch
                        , s.SubBranchId
                        , s.Carrier
                        , s.ManifestAlphabet
                        , s.Alphabet
                        , s.OriginCityCode
                        , s.OriginCityName
                        , s.DestinationCityCode
                        , s.DestinationCityName
                        , s.totalKoli
                        , s.TotalAw
                        , s.TotalCaw
                        , s.Ta
                        , s.Tt
                        , s.TotalTaTt
                    }).OrderByDescending(o => o.ManifestId);
            }

            return rtn;
        }

        public IEnumerable<ListGoodsMonitoringReceiverEntity> ReceiverListData(int year = 0, int month = 0, int day = 0, string destinationDataValue = "", string blokirDataValue = "", int? companyId = 0)
        {
            String query = $@"EXEC List_GoodsMonitoring_Receiver @year={year}, @month={month}, @day= {day}, @destinationDataValue= '{destinationDataValue}', @blokirDataValue= '{blokirDataValue}', @companyId= {companyId}";
            var res = this._context.ListGoodsMonitoringReceiverEntities.FromSqlRaw(query).ToList();
            return res;
        }

    }
}

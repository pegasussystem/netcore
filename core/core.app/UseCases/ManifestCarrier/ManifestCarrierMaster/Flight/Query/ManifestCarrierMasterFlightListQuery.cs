﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Flight.Query
{
    public partial class ManifestCarrierMasterFlightListQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterFlightListQueryHandler : IRequestHandler<ManifestCarrierMasterFlightListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ManifestCarrierMasterFlightListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterFlightListQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var query = from a in _context.FlightEntitys
                            join b in _context.CarrierEntitys on a.CarrierId equals b.CarrierId
                            select new
                            {
                                a.FlightId
                                , b.CarrierCode
                                , a.FlightNo
                                ,  CarrierFlightNo = b.CarrierCode + " - " + a.FlightNo
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.FlightId
                            , s.CarrierCode
                            , s.FlightNo
                            ,s.CarrierFlightNo
                        }).OrderByDescending(o => o.FlightId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
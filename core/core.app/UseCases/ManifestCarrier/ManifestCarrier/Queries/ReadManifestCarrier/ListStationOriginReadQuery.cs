﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries
{
    public partial class ListStationOriginReadQuery : IRequest<ReturnFormat>
    {
        //public Int32? originVendorId { get; set; }
    }

    public class ListStationOriginReadQueryHandler : IRequestHandler<ListStationOriginReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListStationOriginReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListStationOriginReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from st in _context.StationEntitys
                        join vo in _context.VendorOriginEntitys on st.StationId equals vo.StationId
                        join sc in _context.StationCityEntitys on st.StationId equals sc.StationId
                        join mc in _context.MasterCityEntitys on sc.CityId equals mc.MasterCityId
                        where sc.Priority.Equals("1")
                        //where vo.VendorId.Equals(req.originVendorId)
                        select new
                        {
                            st.StationId
                            , st.StationType
                            , StationName = " [" +mc.MasterCityCode+"] - "+st.StationName
                        };

            var query2 = query.Distinct();
            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(
                    s => new
                    {
                        s.StationId
                        , s.StationType
                        , s.StationName
                    }).OrderByDescending(o => o.StationId);
            }

            //
            // return
            return rtn;
        }
    }
}

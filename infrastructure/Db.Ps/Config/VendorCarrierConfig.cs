﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class VendorCarrierConfig : IEntityTypeConfiguration<VendorCarrierEntity>
    {
        public void Configure(EntityTypeBuilder<VendorCarrierEntity> builder)
        {
            builder.HasKey(e => e.VendorCarrierId);
            builder.ToTable("VendorCarrier");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
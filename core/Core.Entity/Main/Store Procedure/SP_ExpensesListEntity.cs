using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_ExpensesListEntity
    {
        public Int64 expensesId { get; set; }
        public DateTime createdAt { get; set; } 
        public DateTime? updatedAt { get; set; } 
        public Int64? CoaId { get; set; } 
         public Int64? bukuKasId { get; set; } 
        public String? coaCode { get; set; }
        public String? coaName { get; set; }
        public String? keterangan { get; set; }
        public Decimal? debit { get; set; }
        public Decimal? credit { get; set; }
         public Decimal? saldo { get; set; }
         public Decimal? cumulativeBalance { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class BranchEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? BranchCode { get; set; }
        public String? BranchName { get; set; }
        public String? Address { get; set; }
        public String? BranchTimeZone { get; set; }
        public Int32? CityIdCode { get; set; }
        public Int32? AreaIdCode { get; set; }
        public String? CityHandled { get; set; }


    }
}

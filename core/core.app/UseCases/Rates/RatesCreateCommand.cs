using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace core.app.UseCases.Rates.Create
{
    public partial class RatesCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? RatesId { get; set; }
        public Int32? RatesSubDistrictId { get; set; }
        public Decimal? NewRates { get; set; }
        public Decimal? MinRates { get; set; }
        public Int32? Max { get; set; }
        public Int32? Min { get; set; }
        public String? Qos { get; set; }
        public String? Kos { get; set; }
        public String? Carrier { get; set; }
        public String? RatesType { get; set; }
        public Int32? OriginCity { get; set; }
        public Int32? DestinationCity { get; set; }
        public String? SubDistrict { get; set; }



        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class RatesCreateCommandHandler : IRequestHandler<RatesCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public RatesCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(RatesCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try{

                RatesEntity entity = new RatesEntity();
                RatesDetailEntity rdEntity = new RatesDetailEntity();

                RatesSubDistrictEntity ratesSubDistrictEntity = new RatesSubDistrictEntity();
                RatesSubDistrictDetailEntity ratesSubDistrictDetailEntity = new RatesSubDistrictDetailEntity();

                //
                // logic

                if (req.Carrier != null && req.Qos != null && req.Kos != null
                     && req.NewRates != null != null && req.OriginCity != null
                      && req.DestinationCity != null)
                {
                    if (req.SubDistrict != null)
                    {
                        var query = from a in _context.CompanyAreaEntitys
                                    where a.MasterSubDistrictId.Equals(req.SubDistrict)
                                    select new
                                    {
                                        a.CompanyAreaId
                                    };

                        var SubDistrict = query.FirstOrDefault().CompanyAreaId;

                        ratesSubDistrictEntity.Carrier                      = "<" + req.Carrier + ">";
                        ratesSubDistrictEntity.QualityOfService             = "<" + req.Qos + ">";
                        ratesSubDistrictEntity.TypeOfService                = "<" + req.Kos + ">";
                        ratesSubDistrictEntity.Rates                        = req.NewRates;
                        ratesSubDistrictEntity.RatesType                    = req.RatesType;
                        ratesSubDistrictEntity.TypesOfGoods                 = "<garment><sparepart><electronic><acessoris><genco><perishable><pebe>";
                        ratesSubDistrictEntity.OriginCity                   = "<" + req.OriginCity + ">";
                        ratesSubDistrictEntity.DestinationCity              = "<" + req.DestinationCity + ">";
                        ratesSubDistrictEntity.DestinationSubDistrictId     = "<" + SubDistrict + ">";
                        ratesSubDistrictEntity.CompanyId                    = req.uiUserProfileEntity.companyId;
                        ratesSubDistrictEntity.CreatedBy                    = req.uiUserProfileEntity.userId.ToString();
                        ratesSubDistrictEntity.CreatedAt                    = DateTime.UtcNow;

                        _context.RatesSubDistrictEntitys.Add(ratesSubDistrictEntity);
                        _context.SaveChanges();

                        req.RatesSubDistrictId = ratesSubDistrictEntity.RatesSubDistrictId;

                        var desc = ratesSubDistrictEntity.OriginCity + " ke " + ratesSubDistrictEntity.DestinationCity + " kecamatan " + SubDistrict + ", tarif : " + Convert.ToInt32(ratesSubDistrictEntity.Rates);
                        this.SetHistoryAction(req.uiUserProfileEntity.userId, ratesSubDistrictEntity.RatesSubDistrictId, "Create Rates", "Rates", desc);
                    }
                    else 
                    {
                        entity.Carrier                                      = "<" + req.Carrier + ">";
                        entity.QualityOfService                             = "<" + req.Qos + ">";
                        entity.TypeOfService                                = "<" + req.Kos + ">";
                        entity.Rates                                        = req.NewRates;
                        entity.RatesType                                    = req.RatesType;
                        entity.TypesOfGoods                                 = "<garment><sparepart><electronic><acessoris><genco><perishable><pebe>";
                        entity.OriginCity                                   = "<" + req.OriginCity + ">";
                        entity.DestinationCity                              = "<" + req.DestinationCity + ">";
                        entity.CompanyId                                    = req.uiUserProfileEntity.companyId;
                        entity.CreatedBy                                    = req.uiUserProfileEntity.userId.ToString();
                        entity.CreatedAt                                    = DateTime.UtcNow;

                        _context.RatesEntitys.Add(entity);
                        _context.SaveChanges();

                        req.RatesId = entity.RatesId;

                        var desc = entity.OriginCity + " ke " + entity.DestinationCity + ", tarif : " + Convert.ToInt32(entity.Rates);
                        this.SetHistoryAction(req.uiUserProfileEntity.userId, entity.RatesId, "Create Rates", "Rates", desc);
                    }
                }
                else
                {
                    rtn.Status = StatusCodes.Status204NoContent;
                    rtn.Data = null;

                    return rtn;
                }
           
                if (req.RatesType == "flat")
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = entity;

                    return rtn;
                }
                else if (req.RatesType == "flatmin" || req.RatesType == "flatminspb")
                {
                    if (req.SubDistrict != null)
                    {
                        ratesSubDistrictDetailEntity.Min                    = 0;
                        ratesSubDistrictDetailEntity.Max                    = req.Max;
                        ratesSubDistrictDetailEntity.Rates                  = req.MinRates;
                        ratesSubDistrictDetailEntity.CreatedBy              = req.uiUserProfileEntity.userId.ToString();
                        ratesSubDistrictDetailEntity.CreatedAt              = DateTime.UtcNow;
                        ratesSubDistrictDetailEntity.RatesSubDistrictId     = req.RatesSubDistrictId;
                        ratesSubDistrictDetailEntity.Type                   = "min";

                        _context.RatesSubDistrictDetailEntitys.Add(ratesSubDistrictDetailEntity);
                        _context.SaveChanges();
                    }
                    else 
                    {
                        rdEntity.Min                                        = 0;
                        rdEntity.Max                                        = req.Max;
                        rdEntity.Rates                                      = req.MinRates;
                        rdEntity.CreatedBy                                  = req.uiUserProfileEntity.userId.ToString();
                        rdEntity.CreatedAt                                  = DateTime.UtcNow;
                        rdEntity.RatesId                                    = req.RatesId;
                        rdEntity.Type                                       = "min";

                        _context.RatesDetailEntitys.Add(rdEntity);
                        _context.SaveChanges();
                    }
                }

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = "ok";

                //
                // return
                return rtn;
            }
            catch (Exception ex) {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu = Menu;
            dataItem.Desc = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }

    }
}

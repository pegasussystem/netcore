﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VMasterSubDistrictConfig : IEntityTypeConfiguration<VMasterSubDistrictEntity>
    {
        public void Configure(EntityTypeBuilder<VMasterSubDistrictEntity> builder)
        {
            builder.HasKey(e => e.MasterSubDistrictId);
            builder.ToTable("VMasterSubDistrict");
        }
    }
}

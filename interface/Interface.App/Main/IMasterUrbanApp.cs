﻿using core.helper;
using Core.Entity.Main;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main
{

    public interface IMasterUrbanApp : IApp<MasterUrbanEntity>
    {
        ReturnFormat getUrban(String subDistrict);
    }
}

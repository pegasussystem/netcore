﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class MasterUrbanConfig : IEntityTypeConfiguration<MasterUrbanEntity>
    {
        public void Configure(EntityTypeBuilder<MasterUrbanEntity> builder)
        {
            builder.HasKey(e => e.MasterUrbanId);
            builder.ToTable("MasterUrban");

        }
    }
}

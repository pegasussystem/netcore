﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class CustomerStoreRepo : Repo<CustomerStoreEntity>, ICustomerStoreRepo
    {
        public CustomerStoreRepo(DbContextPs ctx) : base(ctx)
        {
        }
    }
}

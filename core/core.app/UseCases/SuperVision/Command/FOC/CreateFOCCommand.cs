﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.SuperVision.Command.FOC
{
    public partial class CreateFOCCommand : IRequest<ReturnFormat>
    {
        public String? spbNo { get; set; }
        public String? reason { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateFOCCommandHandler : IRequestHandler<CreateFOCCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateFOCCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateFOCCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                List<SP_SuperVision_GetSpbIdEntity> res = this.GetSpbId(req.spbNo).ToList();

                if (res[0].result == "true")
                {
                    SuperVisionEntity entity = new SuperVisionEntity();

                    entity.SpbId = Int32.Parse(res[0].message);
                    entity.Type = "foc";
                    entity.Reason = req.reason;
                    entity.Status = 0;
                    entity.PicCreatedBy = req.uiUserProfile.userId;
                    entity.PicCreatedDate = DateTime.UtcNow;
                    entity.CreatedBy = req.uiUserProfile.userId.ToString();
                    entity.CreatedAt = DateTime.UtcNow;

                    _context.SuperVisionEntitys.Add(entity);
                    _context.SaveChanges();

                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = "Data sudah berhasil di ajukan";
                }
                else
                {
                    rtn.Status = StatusCodes.Status203NonAuthoritative;
                    rtn.Data = res;
                }

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_SuperVision_GetSpbIdEntity> GetSpbId(String? spbNo)
        {
            String query = $@"EXEC SP_SuperVision_GetSpbId @spbNo='{spbNo}' ";
            var res = this._context.SP_SuperVision_GetSpbIdEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class SpbGoodsRepo : Repo<SpbGoodsEntity>, ISpbGoodsRepo
    {
        public SpbGoodsRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public IEnumerable<SPSpbGoods_CalculatePriceEntity> CalculatePrice(long spbId=0)
        {
            var res = this._contextPs.Set<SPSpbGoods_CalculatePriceEntity>()
                .FromSqlRaw($@"EXEC SPSpbGoods_CalculatePrice 
                        @SpbId={spbId} ");
            return res;

         
        }


    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class MenuDataConfig : IEntityTypeConfiguration<MenuDataEntity>
    {
        public void Configure(EntityTypeBuilder<MenuDataEntity> builder)
        {
            builder.HasKey(e => e.MenuId);
            builder.ToTable("Menu");
        }
    }
}
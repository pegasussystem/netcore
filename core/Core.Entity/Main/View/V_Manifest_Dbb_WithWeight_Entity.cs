﻿using System;

namespace Core.Entity.Main.View
{
    public class V_Manifest_Dbb_WithWeight_Entity
    {

        public DateTime? CreatedAt { get; set; }
        public DateTime? CreatedAtLocal { get; set; }
        public Int64? ManifestId { get; set; }
        public Int32? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? SubBranchId { get; set; }
        public String? ManifestNo { get; set; }
        public String? OriginCity { get; set; }
        public String? DestinationCity { get; set; }
        public Decimal? SumAw { get; set; }
        public Decimal? SumCaw { get; set; }

    }


}

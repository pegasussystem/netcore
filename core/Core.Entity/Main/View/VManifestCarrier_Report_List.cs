﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VManifestCarrier_Report_List
    {
        public DateTime? SendDateUtc { get; set; }
        public String? SendDateLocal { get; set; }
        public String? NoSmu { get; set; }
        public String? NoManifest { get; set; }
        public DateTime? FlightDateUtc { get; set; }
        public String? FlightDateLocal { get; set; }
        public Int32? ManifestCarrierCostId { get; set; }
        public String? CostType { get; set; }
        public decimal? Cost { get; set; }
        public String? CostGroup { get; set; }
        public String? Origin { get; set; }
        public String? Destination { get; set; }
    }
}

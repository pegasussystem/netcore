﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Users.Commands
{
    public partial class UpdateUserDetailCommand : IRequest<ReturnFormat>
    {
        public Int32? userId { get; set; }
        public String? username { get; set; }
        public String? fullname { get; set; }
        public String? email { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class UpdateUserDetailCommandHandler : IRequestHandler<UpdateUserDetailCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdateUserDetailCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdateUserDetailCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.SaveUserDetail(req.userId, req.username, req.fullname, req.email, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_SaveDataUserDetailEntity> SaveUserDetail(Int32? userId, String? username, String? fullname, String? email, Int32? loginId)
        {
            String query = $@"EXEC SP_SaveDataUserDetail @userId='{userId}', @username='{username}', @fullname='{fullname}', @email='{email}', @loginId={loginId} ";
            var res = this._context.SP_SaveDataUserDetailEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

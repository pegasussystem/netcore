﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main
{

    public interface ISpbStatisticApp : IApp<SpbEntity>
    {
        ReturnFormat ReadInputTime(UiUserProfileEntity uiUserProfileEntity, DateTime dateFrom, DateTime dateTo);
    }
}

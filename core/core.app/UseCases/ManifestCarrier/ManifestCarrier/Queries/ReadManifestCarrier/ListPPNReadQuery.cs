﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListPPNReadQuery : IRequest<ReturnFormat>
    {
    }

    public class ListPPNReadQueryHandler : IRequestHandler<ListPPNReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPPNReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPPNReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from l in _context.LookupEntitys
                        where l.LookupKeyParent.Equals("cost") && l.Description.Equals("ppn")
                        select new
                        {
                            PpnId       = l.LookupId
                            , PpnValue  = l.LookupValue                        };


            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new {
                        s.PpnId
                       , s.PpnValue
                    });
            }

            //
            // return
            return rtn;
        }

    }
}

﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class SPSpbHourlyRepo : Repo<SPSpbHourlyEntity>, ISPSpbHourlyRepo
    {
        public SPSpbHourlyRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public IEnumerable<SPSpbHourlyEntity> List(UiUserProfileEntity user, int year, int month, int day)
        {
            return this._contextPs.Set<SPSpbHourlyEntity>()
                .FromSqlRaw($@"EXEC SpbHourly @year={year}, @month={month},@day={day}, 
                            @timeZoneHour={user.WorkTimeZoneHour}, @timeZoneMinute={user.WorkTimeZoneMinute} ");
        }
    }
}

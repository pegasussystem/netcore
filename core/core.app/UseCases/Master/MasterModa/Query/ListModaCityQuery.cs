﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MasterModa.Query
{
    public partial class ListModaCityQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListModaCityQueryHandler : IRequestHandler<ListModaCityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListModaCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListModaCityQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.ModaCityEntitys
                        join b in _context.MasterCityEntitys on a.OriginCityId equals b.MasterCityId
                        join d in _context.MasterCityEntitys on a.DestinationCityId equals d.MasterCityId
                        join c in _context.LookupEntitys on a.LookupId equals c.LookupId
                        select new
                        {
                            a.ModaCityId
                            , OriginCityId = b.MasterCityId 
                            , OriginCityCode = b.MasterCityCode
                            , OriginCityType = b.MasterCityType
                            , OriginCityName = b.MasterCityName
                            , OriginCityCapital = b.MasterCityCapital
                            , OriginCityCustom = b.MasterCityCode + " - " + b.MasterCityType + " " + b.MasterCityName + " " + (b.MasterCityCapital != null ? " [" + b.MasterCityCapital + "] " : "")

                            , DestinationCityId = d.MasterCityId
                            , DestinationCityCode = d.MasterCityCode
                            , DestinationCityType = d.MasterCityType
                            , DestinationCityName = d.MasterCityName
                            , DestinationCityCapital = d.MasterCityCapital
                            , DestinationCityCustom = d.MasterCityCode + " - " + d.MasterCityType + " " + d.MasterCityName + " " + (d.MasterCityCapital != null ? " [" + d.MasterCityCapital + "] " : "")
                            , c.LookupId
                            , c.LookupKeyParent
                            , c.LookupKey
                            , c.LookupValue
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.ModaCityId
                        , s.OriginCityId
                        , s.OriginCityCode
                        , s.OriginCityType
                        , s.OriginCityName
                        , s.OriginCityCapital
                        , s.OriginCityCustom
                        , s.DestinationCityId
                        , s.DestinationCityCode
                        , s.DestinationCityType
                        , s.DestinationCityName
                        , s.DestinationCityCapital
                        , s.DestinationCityCustom
                        , s.LookupId
                        , s.LookupKeyParent
                        , s.LookupKey
                        , s.LookupValue
                    }).OrderBy(o => o.OriginCityId);
            }

            // return
            return rtn;
        }
    }
}

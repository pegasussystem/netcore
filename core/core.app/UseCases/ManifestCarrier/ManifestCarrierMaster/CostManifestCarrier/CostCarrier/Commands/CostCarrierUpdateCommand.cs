﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostCarrier.Commands
{
    public partial class CostCarrierUpdateCommand : IRequest<ReturnFormat>
    {
        public Int32? costCarrierId { get; set; }
        public Int32? costCarrierDetailId { get; set; }
        public String? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostCarrierUpdateCommandHandler : IRequestHandler<CostCarrierUpdateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostCarrierUpdateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostCarrierUpdateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostCarrierUpdate(req.costCarrierId, req.costCarrierDetailId, req.baseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostCarrierUpdateEntity> CostCarrierUpdate(Int32? costCarrierId, Int32? costCarrierDetailId, String? baseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostCarrierUpdate @costCarrierId='{costCarrierId}', @costCarrierDetailId='{costCarrierDetailId}', @baseCost='{baseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostCarrierUpdateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
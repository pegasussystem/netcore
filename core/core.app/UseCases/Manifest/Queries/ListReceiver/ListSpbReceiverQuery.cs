﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Core.Entity.Main.Store_Procedure;

namespace core.app.UseCases.Manifest.Queries.ListReceiver
{
    public partial class ListSpbReceiverQuery : IRequest<ReturnFormat>
    {
        public Int64? manifestId { get; set; }
    }

    public class ListSpbReceiverQueryHandler : IRequestHandler<ListSpbReceiverQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListSpbReceiverQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListSpbReceiverQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_GetSpbManifestEntity> entities = this.ListSpb(req.manifestId);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                         data = s.SpbNoList
                     });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_GetSpbManifestEntity> ListSpb(Int64? manifestId)
        {
            String query = $@"EXEC SP_GetSpbManifest @manifestId = '{manifestId}' ";
            var res = this._context.SP_GetSpbManifestEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

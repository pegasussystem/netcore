﻿using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorCarrier.Query;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;


namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorCarrier
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]

    public class ListController : ApiController
    {
        [HttpGet("manifest-carrier-master-vendor-carrier-list")]
        public async Task<IActionResult> VendorCarrierList([FromQuery] ManifestCarrierMasterVendorCarrierListQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("manifest-carrier-master-vendor-carrier-list-vendor")]
        public async Task<IActionResult> ListVendor([FromQuery] ManifestCarrierMasterVendorCarrierListVendorQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("manifest-carrier-master-vendor-carrier-list-carrier")]
        public async Task<IActionResult> ListStation([FromQuery] ManifestCarrierMasterVendorCarrierListCarrierQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

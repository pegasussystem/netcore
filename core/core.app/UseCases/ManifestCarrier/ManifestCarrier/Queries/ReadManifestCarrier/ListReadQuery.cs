﻿using core.app.Common.Interfaces;
using core.app.Helpers;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries
{
    public partial class ListReadQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListReadQueryHandler : IRequestHandler<ListReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListReadQuery req, CancellationToken cancellationToken)
        {

             // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            var query = from vc in _context.V_ManifestCarrierListEntitys
                        join vd in _context.VendorEntitys on vc.VendorCarrierId equals vd.VendorId
                        select new
                        {
                            //vc.FromSendDate
                            //, vc.ToSendDate
                            vc.CreatedAt
                            , vc.CreatedBy
                            , vc.UpdatedAt
                            , vc.UpdatedBy
                            , vc.ManifestCarrierId
                            , vc.ManifestCarrierNo
                            , vc.NoSmu
                            , vc.FullName
                            , vc.TotalFinalWeightSMU
                            , vc.FlightDate
                            , FirstFlight = vd.VendorName + " " + vc.FirstFlight
                            , LastFlight = vd.VendorName + " " + vc.LastFlight
                            , vc.Revise
                            , vc.RevisiStatus
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        //s.FromSendDate
                        //, s.ToSendDate
                        s.CreatedAt
                        , s.CreatedBy
                        , s.UpdatedAt
                        , s.UpdatedBy
                        , s.ManifestCarrierId
                        , s.ManifestCarrierNo
                        , s.NoSmu
                        , s.FullName
                        , s.TotalFinalWeightSMU
                        , s.FlightDate
                        , s.FirstFlight
                        , s.LastFlight
                        , s.Revise
                        , s.RevisiStatus
                        , FlightDateLocal = s.FlightDate.Value.AddHours((double)req.uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((double)req.uiUserProfileEntity.WorkTimeZoneMinute)
                    }).OrderByDescending(o => o.ManifestCarrierId);
        }

            //
            // return
            return rtn;
        }
    }
}

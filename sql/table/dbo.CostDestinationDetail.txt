/****** Object:  Table [dbo].[CostDestinationDetail]    Script Date: 14/12/2020 09.15.27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CostDestinationDetail](
	[CreatedAt] [datetime2](7) NULL,
	[UpdatedAt] [datetime2](7) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[CostDestinationDetailId] [int] IDENTITY(1,1) NOT NULL,
	[CostDestinationId] [int] NOT NULL,
	[CostType] [varchar](255) NULL,
	[Unit] [varchar](50) NULL,
	[BaseCost] [decimal](10, 2) NULL,
	[CostGroup] [varchar](50) NULL,
	[UseWeight] [varchar](50) NULL,
 CONSTRAINT [PK_CostDestinationDetail] PRIMARY KEY CLUSTERED 
(
	[CostDestinationDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CostDestinationDetail] ADD  CONSTRAINT [DF_CostDestinationDetail_CreatedAt]  DEFAULT (getutcdate()) FOR [CreatedAt]
GO



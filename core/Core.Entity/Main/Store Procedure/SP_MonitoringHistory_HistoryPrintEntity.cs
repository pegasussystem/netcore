﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_MonitoringHistory_HistoryPrintEntity
    {
        public Int32? HistoryPrintId { get; set; }
        public Int32? UserId { get; set; }
        public String? Username { get; set; }
        public Int32? DocId { get; set; }
        public String? TypePrint { get; set; }
        public String? SpbNo { get; set; }
        public String? ManifestNo { get; set; }
        public String? Menu { get; set; }
        public String? MenuAction { get; set; }
        public DateTime? PrintDate { get; set; }
        public String? Desc { get; set; }
    }
}

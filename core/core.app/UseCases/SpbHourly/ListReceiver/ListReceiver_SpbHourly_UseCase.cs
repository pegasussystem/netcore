﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Interface.App.Main.SpbHourly;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace core.app.UseCases.SpbHourly.ListReceiver
{
    class ListReceiverSpbHourlyUseCase : IListReceiverSpbHourlyUseCase
    {
        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public ListReceiverSpbHourlyUseCase(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat List(UiUserProfileEntity profile, Object obj)
        {

            // --- --- ---
            // variable
            // --- --- ---
            IEnumerable<SPSpbHourlyEntity> entities;
            BranchEntity _branch;
            ListReceiverSpbHourlyInput input = obj as ListReceiverSpbHourlyInput;
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };


            // --- --- ---
            // logic
            // --- --- ---
            entities = _repoPs.SPSpbHourlyRepo.List(profile, input.Year(), input.Month(), input.Day());

            // filter by user login to get handle city
            // --- --- ---

            // from branch & sub branch
            if (profile.companyId != null && profile.BranchId != null)
            {
                // get branch city handle
                _branch = _repoPs.BranchRepo.Read(w => w.BranchId.Equals(profile.BranchId)).FirstOrDefault();
                entities = entities.Where(w => _branch.CityHandled.Contains(w.DestinationId));
            }
            else {
                entities = null;
            }

            // --- --- ---
            // return
            // --- --- ---
            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities;
            }
            return rtn;
        }



        public ReturnFormat Create(SPSpbHourlyEntity entity)
        {
            throw new NotImplementedException();
        }

        public SPSpbHourlyEntity Delete(SPSpbHourlyEntity entity)
        {
            throw new NotImplementedException();
        }

       

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(SPSpbHourlyEntity entity)
        {
            throw new NotImplementedException();
        }

        public SPSpbHourlyEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public SPSpbHourlyEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(SPSpbHourlyEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

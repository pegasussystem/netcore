﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostDestination.Query
{
    public partial class ListCostDestinationQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListCostDestinationQueryHandler : IRequestHandler<ListCostDestinationQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCostDestinationQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCostDestinationQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.CostDestinationEntitys
                        join b in _context.CostDestinationDetailEntitys on a.CostDestinationId equals b.CostDestinationId
                        join c in _context.VCompanyCityEntities on a.CityDestinationId equals c.CityId
                        join d in _context.CarrierEntitys on a.CarrierId equals d.CarrierId
                        join vo in _context.VendorOriginEntitys on a.VendorOriginId equals vo.VendorOriginId
                        join vd in _context.VendorDestinationEntitys on a.VendorDestinationId equals vd.VendorDestinationId
                        join e in _context.VendorEntitys on vo.VendorId equals e.VendorId
                        join f in _context.VendorEntitys on vd.VendorId equals f.VendorId
                        select new
                        {
                            a.CostDestinationId
                            , b.CostDestinationDetailId
                            , a.CityDestinationId
                            , cityDestinationName = c.CompanyCityNameCustom
                            , a.CarrierId
                            , CarrierName = "[ "+d.CarrierCode+" ] - "+d.CarrierName
                            , a.VendorOriginId
                            , vendorOriginName = e.VendorName
                            , a.VendorDestinationId
                            , vendorDestinationName = f.VendorName
                            , b.CostType
                            , b.Unit
                            , b.BaseCost
                            , b.CostGroup
                            , b.UseWeight
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CostDestinationId
                        , s.CostDestinationDetailId
                        , s.CityDestinationId
                        , s.cityDestinationName
                        , s.CarrierId
                        , s.CarrierName
                        , s.VendorOriginId
                        , s.vendorOriginName
                        , s.VendorDestinationId
                        , s.vendorDestinationName
                        , s.CostType
                        , s.Unit
                        , s.BaseCost
                        , s.CostGroup
                        , s.UseWeight
                    }).OrderByDescending(o => o.CostDestinationId);
            }

            // return
            return rtn;
        }

    }
}

﻿using Interface.Repo.Base;
using Interface.Repo.Main;

namespace Interface.Repo
{
    public interface IRepoWarpperPs
    {
        //
        // BASE
        //IMenuRepo MenuRepo { get; }
        ILookupRepo LookupRepo { get; }
        IUserRepo UserRepo { get; }
        IUserAtRepo UserAtRepo { get; }
        IPrivillegeRepo PrivillegeRepo { get; }
        IPrivilegeActionRepo PrivilegeActionRepo { get; }
        IMenuActionRepo MenuActionRepo { get; }
        IMenuDataRepo MenuDataRepo { get; }

        //
        // MAIN
        ICustomerRepo CustomerRepo { get; }
        ISpbRepo SpbRepo { get; }
        ISpbGoodsRepo SpbGoodsRepo { get; }
        ICustomerNameRepo CustomerNameRepo { get; }
        ICustomerPlaceRepo CustomerPlaceRepo { get; }
        ICustomerStoreRepo CustomerStoreRepo { get; }
        ICustomerAddressRepo CustomerAddressRepo { get; }
        IManifestRepo ManifestRepo{ get; }
        IBranchRepo BranchRepo{ get; }

        IMasterSubDistrictRepo MasterSubDistrictRepo { get;  }
        IMasterUrbanRepo masterUrbanRepo { get; }
        IMasterCityRepo MasterCityRepo { get; }
        IRatesRepo RatesRepo { get; }
        IRatesDetailRepo RatesDetailRepo { get; }
        IViaRepo ViaRepo{ get; }
        IRatesSubDistrictRepo RatesSubDistrictRepo { get; }
        IRatesSubDistrictDetailRepo RatesSubDistrictDetailRepo{ get; }
        ICarrierRepo CarrierRepo{ get; }
        IManifestCarrierRepo ManifestCarrierRepo { get; }
        IManifestCarrierItemRepo ManifestCarrierItemRepo { get; }

        // Finance
        IAccountReceivableRepo AccountReceivableRepo {get;}
        
        IManifestCarrierCostRepo ManifestCarrierCostRepo { get; }
        I_ManifestKoliSpb_Repo ManifestKoliSpb_Repo { get; }
        I_ManifestKoli_Repo ManifestKoli_Repo { get; }
        //I_CostCarrier_Repo CostCarrier_Repo { get; }
        ISPSpbHourlyRepo SPSpbHourlyRepo { get; }



        // VIEW
        IVCompanyCityRepo VCompanyCityRepo { get; }
        ICompanyRepo CompanyRepo { get; }
        IVCompanyAreaRepo VCompanyAreaRepo { get; }
        IVSpbRepo VSpbRepo{ get; }
        IVSpbGoodsRepo VSpbGoodsRepo{ get; }
        IVManifestRepo VManifestRepo{ get; }
        IVManifestDetailRepo VManifestDetailRepo{ get; }
        IVCustomerRepo VCustomerRepo{ get; }
        IVMasterUrbanRepo VMasterUrbanRepo{ get; }
        IVMasterSubDistrictRepo VMasterSubDistrictRepo{ get; }
        IVMasterCityRepo VMasterCityRepo{ get; }
        IVCustomerAddressRepo VCustomerAddressRepo{ get; }
        ISPManifestDetailRepo SPManifestDetailRepo{ get; }
        ISPSpbGoods_CalculatePriceRepo SPSpbGoods_CalculatePriceRepo{ get; }
        IVManifestCarrierRepo VManifestCarrierRepo{ get; }
        IVCustomerApprovalRepo VCustomerApprovalRepo { get; }

    }
}

﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VCustomerAddressConfig : IEntityTypeConfiguration<VCustomerAddressEntity>
    {
        public void Configure(EntityTypeBuilder<VCustomerAddressEntity> builder)
        {
            builder.HasKey(e => e.CustomerAddressId);
            builder.ToTable("VCustomerAddress");
        }
    }
}

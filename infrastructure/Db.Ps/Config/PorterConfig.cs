﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class PorterConfig : IEntityTypeConfiguration<PorterEntity>
    {
        public void Configure(EntityTypeBuilder<PorterEntity> builder)
        {
            builder.HasKey(e => e.PorterId);
            builder.ToTable("Porter");
        }
    }
}

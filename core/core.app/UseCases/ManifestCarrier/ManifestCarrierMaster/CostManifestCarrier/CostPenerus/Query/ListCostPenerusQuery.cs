﻿using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq ;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostPenerus.Query
{
    public partial class ListCostPenerusQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListCostPenerusQueryHandler : IRequestHandler<ListCostPenerusQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCostPenerusQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCostPenerusQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.CostPenerusEntitys
                        join b in _context.CostPenerusDetailEntitys on a.CostPenerusId equals b.CostPenerusId
                        join c in _context.StationEntitys on a.StationDestinationId equals c.StationId
                        join d in _context.VCompanyCityEntities on a.CityDestinationId equals d.CityId
                        join e in _context.VendorEntitys on a.VendorDestinationId equals e.VendorId
                        join f in _context.StationCityEntitys on c.StationId equals f.StationId
                        join g in _context.MasterCityEntitys on f.CityId equals g.MasterCityId
                        where f.Priority.Equals("1")
                        select new
                        {
                            a.CostPenerusId
                            , b.CostPenerusDetailId
                            , b.CostType
                            , b.Unit
                            , b.BaseCost
                            , b.CostGroup
                            , b.UseWeight
                            , a.StationDestinationId
                            , stationDestinationName = "[ " + g.MasterCityCode + " ] - " + c.StationName
                            , a.CityDestinationId
                            , cityDestinationName = d.CompanyCityNameCustom
                            , a.VendorDestinationId
                            , vendorDestinationName = e.VendorName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.CostPenerusId
                        , s.CostPenerusDetailId
                        , s.CostType
                        , s.Unit
                        , s.BaseCost
                        , s.CostGroup
                        , s.UseWeight
                        , s.StationDestinationId
                        , s.stationDestinationName
                        , s.CityDestinationId
                        , s.cityDestinationName
                        , s.VendorDestinationId
                        , s.vendorDestinationName
                    }).OrderByDescending(o => o.CostPenerusId);
            }

            // return
            return rtn;
        }

    }
}

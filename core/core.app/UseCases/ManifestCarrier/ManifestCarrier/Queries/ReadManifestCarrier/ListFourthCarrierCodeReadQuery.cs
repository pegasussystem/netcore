﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries
{
    public partial class ListFourthCarrierCodeReadQuery : IRequest<ReturnFormat>
    {
        public Int32? vendorCarrierId { get; set; }
        public Int32? firstCarrierCodeId { get; set; }
        public Int32? firstFlightId { get; set; }
        public Int32? secondCarrierCodeId { get; set; }
        public Int32? secondFlightId { get; set; }
        public Int32? thirdCarrierCodeId { get; set; }
        public Int32? thirdFlightId { get; set; }
        public Int32? originStationId { get; set; }
        public Int32? destinationStationId { get; set; }
        public Int32? modaId { get; set; }
    }

    public class ListFourthCarrierCodeReadQueryHandler : IRequestHandler<ListFourthCarrierCodeReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListFourthCarrierCodeReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListFourthCarrierCodeReadQuery req, CancellationToken cancellationToken)
        {
            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.FlightRouteEntitys
                        join b in _context.VendorCarrierEntitys on a.VendorCarrierId equals b.VendorCarrierId
                        join c in _context.CarrierEntitys on a.FourthFlightCodeId equals c.CarrierId
                        //join b in _context.CarrierEntitys on a.FourthFlightCodeId equals b.CarrierId
                        where a.OriginStationId.Equals(req.originStationId)
                            && a.DestinationStationId.Equals(req.destinationStationId)
                            && b.VendorId.Equals(req.vendorCarrierId)
                            && c.ModaId.Equals(req.modaId)
                            && a.FirstFlightCodeId.Equals(req.firstCarrierCodeId)
                            && a.FirstFlightId.Equals(req.firstFlightId)
                            && a.SecondFlightCodeId.Equals(req.secondCarrierCodeId)
                            && a.SecondFlightId.Equals(req.secondFlightId)
                            && a.ThirdFlightCodeId.Equals(req.thirdCarrierCodeId)
                            && a.ThirdFlightId.Equals(req.thirdFlightId)
                        select new
                        {
                            c.CarrierId
                            , c.CarrierCode
                        };

            var query2 = query.Distinct();
            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(
                    s => new
                    {
                        s.CarrierId
                        , s.CarrierCode
                    }).OrderByDescending(o => o.CarrierId);
            }
            //
            // return
            return rtn;
        }
    }
}

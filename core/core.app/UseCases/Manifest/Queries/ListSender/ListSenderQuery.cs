﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Manifest.Queries.ListSender
{
    public partial class ListSenderQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListSenderQueryHandler : IRequestHandler<ListSenderQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListSenderQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListSenderQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VManifestEntity> vManifestEntities = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data
            vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(req.uiUserProfileEntity.companyId))
                .ToList();


            // from sub branch
            if (req.uiUserProfileEntity.companyId != null && req.uiUserProfileEntity.BranchId != null && req.uiUserProfileEntity.SubBranchId != null)
            {
                vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(req.uiUserProfileEntity.companyId)
                && w.BranchId.Equals(req.uiUserProfileEntity.BranchId) && w.SubBranchId.Equals(req.uiUserProfileEntity.SubBranchId))
                .ToList();
            }

            // from branch
            if (req.uiUserProfileEntity.companyId != null && req.uiUserProfileEntity.BranchId != null && req.uiUserProfileEntity.SubBranchId == null)
            {
                vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(req.uiUserProfileEntity.companyId)
                && w.BranchId.Equals(req.uiUserProfileEntity.BranchId))
                .ToList();
            }

            // from company
            if (req.uiUserProfileEntity.companyId != null && req.uiUserProfileEntity.BranchId == null && req.uiUserProfileEntity.SubBranchId == null)
            {
                vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(req.uiUserProfileEntity.companyId))
                .ToList();
            }


            if (vManifestEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vManifestEntities
                    .Select(s => new {
                        s,
                        CreatedAtLocal = s.CreatedAtLocal
                    }).OrderByDescending(s => s.s.CreatedAt);
            }

            //
            // return
            return rtn;
        }
    }
}

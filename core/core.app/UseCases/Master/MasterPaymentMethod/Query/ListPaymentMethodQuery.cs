﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MasterPaymentMethod.Query
{
    public partial class ListPaymentMethodQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListPaymentMethodQueryHandler : IRequestHandler<ListPaymentMethodQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPaymentMethodQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPaymentMethodQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.PaymentMethodEntitys
                        join b in _context.MasterCityEntitys on a.MasterCityId equals b.MasterCityId
                        join c in _context.LookupEntitys on a.LookupId equals c.LookupId
                        select new
                        {
                            a.PaymentMethodId
                            , a.MasterCityId
                            , b.MasterCityCode
                            , b.MasterCityType
                            , b.MasterCityName
                            , b.MasterCityCapital
                            , MasterCityCustom = b.MasterCityCode + " - " + b.MasterCityType + " " + b.MasterCityName + " " + (b.MasterCityCapital != null ? " ["+ b.MasterCityCapital +"] " : "")
                            , c.LookupId
                            , c.LookupKeyParent
                            , c.LookupKey
                            , c.LookupValue
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.PaymentMethodId
                        , s.MasterCityId
                        , s.MasterCityCode
                        , s.MasterCityType
                        , s.MasterCityName
                        , s.MasterCityCapital
                        , s.MasterCityCustom
                        , s.LookupId
                        , s.LookupKeyParent
                        , s.LookupKey
                        , s.LookupValue
                    }).OrderBy(o => o.MasterCityId);
            }

            // return
            return rtn;
        }
    }
}

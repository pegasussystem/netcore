﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit.Queries
{
    public partial class MasterTransitReadVendorQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MasterTransitReadVendorQueryHandler : IRequestHandler<MasterTransitReadVendorQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MasterTransitReadVendorQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MasterTransitReadVendorQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // --- --- --- --- ---
            var qr = from s in _context.VendorEntitys
                        join c in _context.VendorCarrierEntitys on s.VendorId equals c.VendorId
                        select new
                        {
                            s.VendorId
                            , s.VendorName
                        };

            if (qr != null)
            {
                var query = qr.Distinct();
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.VendorId
                         , s.VendorName
                    }).OrderBy(o => o.VendorId);
            }

            //
            // return
            return rtn;
        }
    }
}

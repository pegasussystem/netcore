﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Rates
{
    public partial class HistoryActionQuery : IRequest<ReturnFormat>
    {
        public String? originCity { get; set; }
        public String? destinationCity { get; set; }
        public String? subDistrict { get; set; }
        public String? ratesType { get; set; }
        public String? moda { get; set; }
        public String? kos { get; set; }
        public String? qos { get; set; }
        public String? rates { get; set; }
        public String? minWeight { get; set; }
        public String? minRates { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class RatesListQueryHandler : IRequestHandler<HistoryActionQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public RatesListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(HistoryActionQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                //cek key exists di entity
                var entity = this.SP_CheckRates(req.originCity, req.destinationCity, req.subDistrict, req.ratesType, req.moda, req.kos, req.qos, req.rates, req.minWeight, req.minRates);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_CheckRatesEntity> SP_CheckRates(String? originCity, String? destinationCity, String? subDistrict, String? ratesType, String? moda, String? kos, String? qos, String? rates, String? minWeight, String? minRates)
        {
            String query = $@"EXEC SP_CheckRates @originCity ='{originCity}', @destinationCity ='{destinationCity}', @subDistrict ='{subDistrict}', @ratesType ='{ratesType}', @moda ='{moda}', @kos ='{kos}', @qos ='{qos}', @rates ='{rates}', @minWeight ='{minWeight}', @minRates ='{minRates}' ";
            var res = this._context.SP_CheckRatesEntitiys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VCustomerConfig : IEntityTypeConfiguration<VCustomerEntity>
    {
        public void Configure(EntityTypeBuilder<VCustomerEntity> builder)
        {
            builder.HasKey(e => e.CustomerId);
            builder.ToTable("VCustomer");
        }
    }
}

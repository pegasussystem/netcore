﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostOrigin.Query
{
    public partial class ListCostOriginQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListCostOriginQueryHandler : IRequestHandler<ListCostOriginQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCostOriginQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCostOriginQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var query = from a in _context.CostOriginEntitys
                            join b in _context.CostOriginDetailEntitys on a.CostOriginId equals b.CostOriginId
                            join c in _context.StationEntitys on a.StationOriginId equals c.StationId
                            join d in _context.CarrierEntitys on a.CarrierId equals d.CarrierId
                            join e in _context.VendorOriginEntitys on a.VendorOriginId equals e.VendorOriginId
                            join f in _context.VendorEntitys on e.VendorId equals f.VendorId
                            join g in _context.LookupEntitys on b.Unit equals g.LookupKey
                            join h in _context.StationCityEntitys on c.StationId equals h.StationId
                            join i in _context .MasterCityEntitys on h.CityId equals i.MasterCityId
                            where g.LookupKeyParent.Equals("unit") && h.Priority.Equals("1")
                            select new
                            {
                                a.CostOriginId
                                , b.CostOriginDetailId
                                , StationName = "[ "+i.MasterCityCode+" ] - "+c.StationName
                                , CarrierName =  "[ "+d.CarrierCode+" ] - " + d.CarrierName
                                , f.VendorName
                                , g.LookupValue
                                , b.BaseCost
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.CostOriginId
                            , s.CostOriginDetailId
                            , s.StationName
                            , s.CarrierName
                            , s.VendorName
                            , s.LookupValue
                            , s.BaseCost
                        }).OrderByDescending(o => o.CostOriginId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
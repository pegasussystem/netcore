﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.SpbUpdate.Commands
{
    public partial class UpdateSpbCommand : IRequest<ReturnFormat>
    {
          public Int64? spbId              { get; set;}
          public String? totalPrice        { get; set;}
          public String? spbCawFinal       { get; set;}
          public String? carrierId         { get; set;}
          public String? rates             { get; set; }

          public Int64? first_spbGoodsId   { get; set;}
          public String? first_totalKoli   { get; set;}
          public String? first_aw          { get; set; }
        public String? first_length        { get; set;}
          public String? first_width       { get; set;}
          public String? first_height      { get; set;}
          public String? first_vw          { get; set;}
          public String? first_cawFinal    { get; set;}

          public Int64? second_spbGoodsId  { get; set;}
          public String? second_totalKoli  { get; set;}
          public String? second_aw         { get; set;}
          public String? second_length     { get; set;}
          public String? second_width      { get; set;}
          public String? second_height     { get; set;}
          public String? second_vw         { get; set;}
          public String? second_cawFinal   { get; set;}

          public Int64? third_spbGoodsId   { get; set;}
          public String? third_totalKoli   { get; set;}
          public String? third_aw          { get; set;}
          public String? third_length      { get; set;}
          public String? third_width       { get; set;}
          public String? third_height      { get; set;}
          public String? third_vw          { get; set;}
          public String? third_cawFinal    { get; set;}

          public Int64? fourth_spbGoodsId  { get; set;}
          public String? fourth_totalKoli  { get; set;}
          public String? fourth_aw         { get; set;}
          public String? fourth_length     { get; set;}
          public String? fourth_width      { get; set;}
          public String? fourth_height     { get; set;}
          public String? fourth_vw         { get; set;}
          public String? fourth_cawFinal   { get; set;}

          public Int64? fifth_spbGoodsId   { get; set;}
          public String? fifth_totalKoli   { get; set;}
          public String? fifth_aw          { get; set;}
          public String? fifth_length      { get; set;}
          public String? fifth_width       { get; set;}
          public String? fifth_height      { get; set;}
          public String? fifth_vw          { get; set;}
          public String? fifth_cawFinal    { get; set;}

        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class UpdateSpbCommandHandler : IRequestHandler<UpdateSpbCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdateSpbCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdateSpbCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.UpdateKilo(
                    req.spbId               
                    , req.totalPrice        
                    , req.spbCawFinal       
                    , req.carrierId         
                    , req.rates  
                    
                    , req.first_spbGoodsId  
                    , req.first_aw          
                    , req.first_length      
                    , req.first_width       
                    , req.first_height      
                    , req.first_vw          
                    , req.first_cawFinal  

                    , req.second_spbGoodsId 
                    , req.second_aw
                    , req.second_length     
                    , req.second_width      
                    , req.second_height     
                    , req.second_vw         
                    , req.second_cawFinal   

                    , req.third_spbGoodsId  
                    , req.third_aw
                    , req.third_length      
                    , req.third_width       
                    , req.third_height      
                    , req.third_vw          
                    , req.third_cawFinal   
                    
                    , req.fourth_spbGoodsId 
                    , req.fourth_aw
                    , req.fourth_length     
                    , req.fourth_width      
                    , req.fourth_height     
                    , req.fourth_vw         
                    , req.fourth_cawFinal 

                    , req.fifth_spbGoodsId  
                    , req.fifth_aw
                    , req.fifth_length      
                    , req.fifth_width       
                    , req.fifth_height      
                    , req.fifth_vw          
                    , req.fifth_cawFinal   
                    
                    , req.uiUserProfile.userId            
                );

                await _context.SaveChangesAsync(cancellationToken);

                if (Int32.Parse(req.first_totalKoli) > 0)
                {
                    this.UpdateTotalKoli(req.spbId, req.first_spbGoodsId, req.first_totalKoli, req.first_cawFinal, req.uiUserProfile.userId);
                }
                else {
                    this.DeleteTotalKoli(req.first_spbGoodsId);
                }

                if (Int32.Parse(req.second_totalKoli) > 0)
                {
                    this.UpdateTotalKoli(req.spbId, req.second_spbGoodsId, req.second_totalKoli, req.second_cawFinal, req.uiUserProfile.userId);
                }
                else
                {
                    this.DeleteTotalKoli(req.second_spbGoodsId);
                }

                if (Int32.Parse(req.third_totalKoli) > 0)
                {
                    this.UpdateTotalKoli(req.spbId, req.third_spbGoodsId, req.third_totalKoli, req.third_cawFinal, req.uiUserProfile.userId);
                }
                else
                {
                    this.DeleteTotalKoli(req.third_spbGoodsId);
                }

                if (Int32.Parse(req.fourth_totalKoli) > 0)
                {
                    this.UpdateTotalKoli(req.spbId, req.fourth_spbGoodsId, req.fourth_totalKoli, req.fourth_cawFinal, req.uiUserProfile.userId);
                }
                else
                {
                    this.DeleteTotalKoli(req.fourth_spbGoodsId);
                }

                if (Int32.Parse(req.fifth_totalKoli) > 0)
                {
                    this.UpdateTotalKoli(req.spbId, req.fifth_spbGoodsId, req.fifth_totalKoli, req.fifth_cawFinal, req.uiUserProfile.userId);
                }
                else
                {
                    this.DeleteTotalKoli(req.fifth_spbGoodsId);
                }


                this.CalculatePrice(req.spbId);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                this.CalculatePrice(req.spbId);
                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public void UpdateTotalKoli(Int64? spbId, Int64? spbGoodsId, String? totalKoli, String? cawFinal, Int32? UserId) 
        {
            String query = $@"EXEC SP_UpdateTotalKoli @spbId = '{spbId}',  @spbGoodsId = '{spbGoodsId}',  @totalKoli = '{totalKoli}',  @caw = '{cawFinal.Replace(",", ".")}',  @UserId = '{UserId}'";
            var res = this._context.SP_UpdateTotalKoliEntitys.FromSqlRaw(query).ToList();
            var x = res;
        }

        public void DeleteTotalKoli(Int64? spbGoodsId)
        {
            String query = $@"EXEC SP_DeleteTotalKoli @spbGoodsId = '{spbGoodsId}'";
            var res = this._context.SP_DeleteTotalKoliEntitys.FromSqlRaw(query).ToList();
            var x = res;
        }

        public void CalculatePrice(long? spbId)
        {
            String query = $@"EXEC SPSpbGoods_CalculatePrice @spbId = '{spbId}'";
            this._context.SPSpbGoods_CalculatePriceEntitys.FromSqlRaw(query).ToList();
        }

        public IEnumerable<SP_UpdateKiloEntity> UpdateKilo(
            Int64? spbId            
            , String? totalPrice        
            , String? spbCawFinal       
            , String? carrierId         
            , String? rates     

            , Int64? first_spbGoodsId   
            , String? first_aw
            , String? first_length      
            , String? first_width       
            , String? first_height      
            , String? first_vw          
            , String? first_cawFinal  

            , Int64? second_spbGoodsId  
            , String? second_aw
            , String? second_length     
            , String? second_width      
            , String? second_height     
            , String? second_vw         
            , String? second_cawFinal   

            , Int64? third_spbGoodsId   
            , String? third_aw
            , String? third_length      
            , String? third_width       
            , String? third_height      
            , String? third_vw          
            , String? third_cawFinal    

            , Int64? fourth_spbGoodsId  
            , String? fourth_aw
            , String? fourth_length     
            , String? fourth_width      
            , String? fourth_height     
            , String? fourth_vw         
            , String? fourth_cawFinal   

            , Int64? fifth_spbGoodsId   
            , String? fifth_aw
            , String? fifth_length      
            , String? fifth_width       
            , String? fifth_height      
            , String? fifth_vw          
            , String? fifth_cawFinal   
            , Int32? UserId
        )
        {
            String query = $@"EXEC SP_UpdateKilo
                      @spbId             = '{spbId}'
                    , @totalPrice        = '{totalPrice}'
                    , @spbCawFinal       = '{spbCawFinal.Replace(",", ".")}'
                    , @carrierId         = '{carrierId}'
                    , @rates             = '{rates}'
                    , @first_spbGoodsId  = '{first_spbGoodsId}' 
                    , @first_aw          = '{first_aw.Replace(",", ".")}'
                    , @first_length      = '{first_length.Replace(",", ".")}'
                    , @first_width       = '{first_width.Replace(",", ".")}'
                    , @first_height      = '{first_height.Replace(",", ".")}'
                    , @first_vw          = '{first_vw.Replace(",", ".")}'
                    , @first_cawFinal    = '{first_cawFinal.Replace(",", ".")}'
                    , @second_spbGoodsId = '{second_spbGoodsId}' 
                    , @second_aw         = '{second_aw.Replace(",", ".")}'
                    , @second_length     = '{second_length.Replace(",", ".")}'
                    , @second_width      = '{second_width.Replace(",", ".")}'
                    , @second_height     = '{second_height.Replace(",", ".")}'
                    , @second_vw         = '{second_vw.Replace(",", ".")}'
                    , @second_cawFinal   = '{second_cawFinal.Replace(",", ".")}'
                    , @third_spbGoodsId  = '{third_spbGoodsId}' 
                    , @third_aw          = '{third_aw.Replace(",", ".")}'
                    , @third_length      = '{third_length.Replace(",", ".")}'
                    , @third_width       = '{third_width.Replace(",", ".")}'
                    , @third_height      = '{third_height.Replace(",", ".")}'
                    , @third_vw          = '{third_vw.Replace(",", ".")}'
                    , @third_cawFinal    = '{third_cawFinal.Replace(",", ".")}'
                    , @fourth_spbGoodsId = '{fourth_spbGoodsId}' 
                    , @fourth_aw         = '{fourth_aw.Replace(",", ".")}'
                    , @fourth_length     = '{fourth_length.Replace(",", ".")}'
                    , @fourth_width      = '{fourth_width.Replace(",", ".")}'
                    , @fourth_height     = '{fourth_height.Replace(",", ".")}'
                    , @fourth_vw         = '{fourth_vw.Replace(",", ".")}'
                    , @fourth_cawFinal   = '{fourth_cawFinal.Replace(",", ".")}'
                    , @fifth_spbGoodsId  = '{fifth_spbGoodsId}' 
                    , @fifth_aw          = '{fifth_aw.Replace(",", ".")}'
                    , @fifth_length      = '{fifth_length.Replace(",", ".")}'
                    , @fifth_width       = '{fifth_width.Replace(",", ".")}'
                    , @fifth_height      = '{fifth_height.Replace(",", ".")}'
                    , @fifth_vw          = '{fifth_vw.Replace(",", ".")}'
                    , @fifth_cawFinal    = '{fifth_cawFinal.Replace(",", ".")}'
                    , @userId            = '{UserId}'
";
            var res = this._context.SP_UpdateKiloEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

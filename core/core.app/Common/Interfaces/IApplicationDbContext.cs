﻿using Core.Entity.Base;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        // --- table
        // --- --- --- --- ---
        DbSet<CustomerEntity> CustomerEntitys { get; set; }
        DbSet<CustomerNameEntity> CustomerNameEntitys { get; set; }
        DbSet<CustomerStoreEntity> CustomerStoreEntitys { get; set; }
        DbSet<CustomerPlaceEntity> CustomerPlaceEntitys { get; set; }
        DbSet<CustomerAddressEntity> CustomerAddressEntitys { get; set; }
        DbSet<UserEntity> UserEntitys { get; set; }
        DbSet<UserDetailEntity> UserDetailEntitys { get; set; }
        DbSet<UserAtEntity> UserAtEntitys { get; set; }
        DbSet<CompanyEntity> CompanyEntitys { get; set; }
        DbSet<BranchEntity> BranchEntitys { get; set; }
        DbSet<SubBranchEntity> SubBranchEntitys { get; set; }
        DbSet<SpbEntity> SpbEntitys { get; set; }
        DbSet<RatesEntity> RatesEntitys { get; set; }
        DbSet<RatesDetailEntity> RatesDetailEntitys { get; set; }
        DbSet<RatesSubDistrictEntity> RatesSubDistrictEntitys { get; set; }
        DbSet<RatesSubDistrictDetailEntity> RatesSubDistrictDetailEntitys { get; set; }
        DbSet<ManifestCarrierEntity> ManifestCarrierEntitys { get; set; }
        DbSet<ManifestCarrierItemEntity> ManifestCarrierItemEntitys { get; set; }
        DbSet<ManifestCarrierItemDetailEntity> ManifestCarrierItemDetailEntitys { get; set; }
        DbSet<ManifestCarrierCostEntity> ManifestCarrierCostEntitys { get; set; }
        DbSet<ManifestCarrierCostSMUEntity> ManifestCarrierCostSMUEntitys { get; set; }
        DbSet<ManifestEntity> ManifestEntitys { get; set; }
        DbSet<AlphabetEntity> AlphabetEntitys { get; set; }
        DbSet<CompanyCityEntity> CompanyCityEntitys { get; set; }
        DbSet<MasterCityEntity> MasterCityEntitys { get; set; }
        DbSet<MasterSubDistrictEntity> MasterSubDistrictEntitys { get; set; }
        DbSet<StationEntity> StationEntitys { get; set; }
        DbSet<VendorEntity> VendorEntitys { get; set; }
        DbSet<ModaEntity> ModaEntitys { get; set; }
        DbSet<VendorTransitEntity> VendorTransitEntitys { get; set; }
        DbSet<ManifestKoliEntity> ManifestKoliEntitys { get; set; }
        DbSet<ManifestKoliSpbEntity> ManifestKoliSpbEntitys { get; set; }
        DbSet<SpbGoodsEntity> SpbGoodsEntitys { get; set; }

        DbSet<CarrierEntity> CarrierEntitys { get; set; }
        DbSet<CostCarrierEntity> CostCarrierEntitys { get; set; }
        DbSet<CostCarrierDetailEntity> CostCarrierDetailEntitys { get; set; }
        DbSet<CostCarrierDetailPpnEntity> CostCarrierDetailPpnEntitys { get; set; }
        DbSet<CostCarrierDetailSmuEntity> CostCarrierDetailSmuEntitys { get; set; }
        
        DbSet<CostOriginEntity> CostOriginEntitys { get; set; }
        DbSet<CostOriginDetailEntity> CostOriginDetailEntitys { get; set; }
        
        DbSet<CostDestinationEntity> CostDestinationEntitys { get; set; }
        DbSet<CostDestinationDetailEntity> CostDestinationDetailEntitys { get; set; }

        DbSet<CostTransitEntity> CostTransitEntitys { get; set; }
        DbSet<CostTransitDetailEntity> CostTransitDetailEntitys { get; set; }

        DbSet<CostGeraiEntity> CostGeraiEntitys { get; set; }
        DbSet<CostGeraiDetailEntity> CostGeraiDetailEntitys { get; set; }

        DbSet<CostPenerusEntity> CostPenerusEntitys { get; set; }
        DbSet<CostPenerusDetailEntity> CostPenerusDetailEntitys { get; set; }
        
        DbSet<CostInlandOriginEntity> CostInlandOriginEntitys { get; set; }
        DbSet<CostInlandOriginDetailEntity> CostInlandOriginDetailEntitys { get; set; }

        DbSet<LookupEntity> LookupEntitys { get; set; }
        DbSet<VendorCarrierEntity> VendorCarrierEntitys { get; set; }
        DbSet<VendorOriginEntity> VendorOriginEntitys { get; set; }
        DbSet<VendorDestinationEntity> VendorDestinationEntitys { get; set; }
        DbSet<StationCityEntity> StationCityEntitys { get; set; }
        DbSet<MatrixFirstCarrierEntity> MatrixFirstCarrierEntitys { get; set; }
        DbSet<MatrixLastCarrierEntity> MatrixLastCarrierEntitys { get; set; }
        DbSet<BranchCityEntity> BranchCityEntitys { get; set; }

        DbSet<PrivilegeUserEntity> PrivilegeUserEntitys { get; set; }
        DbSet<PrivilegeEntity> PrivilegeEntitys { get; set; }
        DbSet<PrivilegeActionEntity> PrivilegeActionEntitys { get; set; }
        DbSet<MenuActionEntity> MenuActionEntitys { get; set; }
        DbSet<MenuDataEntity> MenuDataEntitys { get; set; }
        DbSet<HistoryLoginEntity> HistoryLoginEntitys { get; set; }
        DbSet<HistoryActionEntity> HistoryActionEntitys { get; set; }
        DbSet<HistoryPrintEntity> HistoryPrintEntitys { get; set; }
        DbSet<CompanyAreaEntity> CompanyAreaEntitys { get; set; }
        DbSet<RolesEntity> RolesEntitys { get; set; }
        DbSet<WorkUnitEntity> WorkUnitEntitys { get; set; }

        DbSet<FlightEntity> FlightEntitys { get; set; }
        DbSet<FlightRouteEntity> FlightRouteEntitys { get; set; }
        DbSet<FlightTransitRouteEntity> FlightTransitRouteEntitys { get; set; }
        DbSet<MasterProvinceEntity> MasterProvinceEntitys { get; set; }

        DbSet<SuperVisionEntity> SuperVisionEntitys { get; set; }
        DbSet<PaymentMethodEntity> PaymentMethodEntitys { get; set; }
        DbSet<ModaCityEntity> ModaCityEntitys { get; set; }
        DbSet<MappingAreaNoExistsEntity> MappingAreaNoExistsEntitys { get; set; }
        DbSet<PrintStatusEntity> PrintStatusEntitys { get; set; }
        DbSet<MonitoringShipmentEntity> MonitoringShipmentEntitys { get; set; }
        
        DbSet<MasterStatistikEntity> MasterStatistikEntitys { get; set; }

        DbSet<PorterEntity> PorterEntitys { get; set; }
        DbSet<SpbDetailEntity> SpbDetailEntitys { get; set; }

        DbSet<RoundEntity> RoundEntitys { get; set; }
        DbSet<TransportEntity> TransportEntitys { get; set; }
         DbSet<CoaParentEntity> CoaParentEntities { get; set; }
        DbSet<CoaCodeEntity> CoaCodeEntities { get; set; }
        DbSet<CityBranchEntity> CityBranchEntities { get; set; }
        DbSet<CoaAccuredExpensesManagementEntity> CoaAccuredExpensesManagementEntities { get; set; }
        DbSet<AccountReceivableEntity> AccountReceivableEntities { get; set; }
        DbSet<RevenueEntity> RevenueEntities { get; set; }
        DbSet<ProfitLossStatementEntity> ProfitLossStatementEntities { get; set; }
        DbSet<CoaCityRevenueLiabilityEntity> CoaCityRevenueLiabilityEntities { get; set; }


        //-------- view---------
        //----------------------
        DbSet<V_CustomerCodeEntity> V_CustomerCodeEntitys { get; set; }
        DbSet<V_CustomerCodeDataEntity> V_CustomerCodeDataEntitys { get; set; }
        DbSet<V_StationStatusEntity> V_StationStatusEntitys { get; set; }
        DbSet<VSpbEntity> VSpbEntitys { get; set; }
        DbSet<V_UserPrivilegeEntity> V_UserPrivilegeEntitys { get; set; }
        DbSet<VUserNotPrivilegeEntity> VUserNotPrivilegeEntitys { get; set; }
        DbSet<VCompanyCityEntity> VCompanyCityEntities { get; set; }
        DbSet<VCompanyAreaEntity> VCompanyAreaEntitys { get; set; }
        DbSet<V_ManifestListEntity> V_ManifestListEntitys { get; set; }
        DbSet<V_PrivilegeListEntity> V_PrivilegeListEntitys { get; set; }
        DbSet<V_PrivilegeMenuListEntity> V_PrivilegeMenuListEntitys { get; set; }
        DbSet<V_PrivilegeUserListEntity> V_PrivilegeUserListEntitys { get; set; }
        DbSet<V_DataUserEntity> V_DataUserEntitys { get; set; }
        DbSet<V_DataOptionSettingEntity> V_DataOptionSettingEntitys { get; set; }
        DbSet<V_DataOptionCitySubDistrictEntity> V_DataOptionCitySubDistrictEntitys { get; set; }
        DbSet<V_RoleListEntity> V_RoleListEntitys { get; set; }
        DbSet<MasterDataCustomerEntity> MasterDataCustomerEntitys { get; set; }
        DbSet<MDataCustomerReceiverEntity> MDataCustomerReceiverEntitys { get; set; }
        DbSet<MDataCustomerSenderEntity> MDataCustomerSenderEntitys { get; set; }
        DbSet<V_ManifestDetailListEntity> V_ManifestDetailListEntitys { get; set; }
        DbSet<V_ManifestCarrierCostEntity> V_ManifestCarrierCostEntitys { get; set; }
        DbSet<V_ManifestCarrierDetailEntity> V_ManifestCarrierDetailEntitys { get; set; }
        DbSet<V_ManifestCarrierItemDetailEntity> V_ManifestCarrierItemDetailEntitys { get; set; }
        DbSet<V_ManifestCarrierApprovalEntity> V_ManifestCarrierApprovalEntitys { get; set; }
        DbSet<V_ManifestKoliListEntity> V_ManifestKoliListEntitys { get; set; }
        DbSet<V_ManifestCarrierListEntity> V_ManifestCarrierListEntitys { get; set; }
        DbSet<V_ManifestCarrierReviseListEntity> V_ManifestCarrierReviseListEntitys { get; set; }
        DbSet<V_MonitoringCapacityCarrierEntity> V_MonitoringCapacityCarrierEntitys { get; set; }
        DbSet<V_UserAgreementEntity> V_UserAgreementEntitys { get; set; }
        DbSet<V_FlightRouteEntity> V_FlightRouteEntitys { get; set; }
        DbSet<V_TypePrintEntity> V_TypePrintEntitys { get; set; }
        DbSet<V_HistoryNotPrintSpbEntity> V_HistoryNotPrintSpbEntitys { get; set; }
        DbSet<V_HistoryPrintEntity> V_HistoryPrintEntitys { get; set; }
        DbSet<V_FragilePerishableCityEntity> V_FragilePerishableCityEntitys { get; set; }
        DbSet<V_SpbDataFormEntity> V_SpbDataFormEntitys { get; set; }
        DbSet<V_SpbGoodsDataFormEntity> V_SpbGoodsDataFormEntitys { get; set; }
        DbSet<VBukuKasEntity> VBukuKasEntities { get; set; }
        DbSet<V_BukuKasCoaHierarchyCurrentMonthEntity> V_BukuKasCoaHierarchyCurrentMonthEntities { get; set; }



        // --- store procedure
        // --- --- --- --- ---
        DbSet<SP_RoudCreateEntity> SP_RoudCreateEntitys { get; set; }
        DbSet<SP_RoudDeleteEntity> SP_RoudDeleteEntitys { get; set; }

        DbSet<SP_UpdateTotalKoliEntity> SP_UpdateTotalKoliEntitys { get; set; }
        DbSet<SP_DeleteTotalKoliEntity> SP_DeleteTotalKoliEntitys { get; set; }
        DbSet<SP_MonitoringHistory_HistoryNotPrintEntity> SP_MonitoringHistory_HistoryNotPrintEntitys { get; set; }
        DbSet<SP_MonitoringHistory_HistoryPrintEntity> SP_MonitoringHistory_HistoryPrintEntitys { get; set; }
        DbSet<SP_MonitoringHistory_HistoryLoginEntity> SP_MonitoringHistory_HistoryLoginEntitys { get; set; }
        DbSet<SP_MonitoringHistory_HistoryActionEntity> SP_MonitoringHistory_HistoryActionEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostGeraiDeleteEntity> SP_ManifestCarrierCost_CostGeraiDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostGeraiUpdateEntity> SP_ManifestCarrierCost_CostGeraiUpdateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostGeraiCreateEntity> SP_ManifestCarrierCost_CostGeraiCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostPenerusCreateEntity> SP_ManifestCarrierCost_CostPenerusCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostPenerusUpdateEntity> SP_ManifestCarrierCost_CostPenerusUpdateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostPenerusDeleteEntity> SP_ManifestCarrierCost_CostPenerusDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostTransitCreateEntity> SP_ManifestCarrierCost_CostTransitCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostTransitDeleteEntity> SP_ManifestCarrierCost_CostTransitDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostTransitUpdateEntity> SP_ManifestCarrierCost_CostTransitUpdateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostDestinationCreateEntity> SP_ManifestCarrierCost_CostDestinationCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostDestinationDeleteEntity> SP_ManifestCarrierCost_CostDestinationDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostDestinationUpdateEntity> SP_ManifestCarrierCost_CostDestinationUpdateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostInlandOriginCreateEntity> SP_ManifestCarrierCost_CostInlandOriginCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostInlandOriginDeleteEntity> SP_ManifestCarrierCost_CostInlandOriginDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostInlandOriginUpdateEntity> SP_ManifestCarrierCost_CostInlandOriginUpdateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostOriginCreateEntity> SP_ManifestCarrierCost_CostOriginCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostOriginDeleteEntity> SP_ManifestCarrierCost_CostOriginDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostOriginUpdateEntity> SP_ManifestCarrierCost_CostOriginUpdateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostCarrierCreateEntity> SP_ManifestCarrierCost_CostCarrierCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostCarrierDeleteEntity> SP_ManifestCarrierCost_CostCarrierDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierCost_CostCarrierUpdateEntity> SP_ManifestCarrierCost_CostCarrierUpdateEntitys { get; set; }
        DbSet<SP_GetManifestCasrrierCountManifestEntity> SP_GetManifestCasrrierCountManifestEntitys { get; set; }
        DbSet<SP_GetManifestCarrierListManifestEntity> SP_GetManifestCarrierListManifestEntitys { get; set; }
        DbSet<SP_PaymentMethod_CreateEntity> SP_PaymentMethod_CreateEntitys { get; set; }
        DbSet<SP_PaymentMethod_DeleteEntity> SP_PaymentMethod_DeleteEntitys { get; set; }
        DbSet<SP_ModaCity_CreateEntity> SP_ModaCity_CreateEntitys { get; set; }
        DbSet<SP_ModaCity_DeleteEntity> SP_ModaCity_DeleteEntitys { get; set; }
        DbSet<SP_SuperVision_GetSpbIdEntity> SP_SuperVision_GetSpbIdEntitys { get; set; }
        DbSet<SP_CreateRolesEntity> SP_CreateRolesEntitys { get; set; }
        DbSet<SP_SaveDataUnitEntity> SP_SaveDataUnitEntitys { get; set; }
        DbSet<SP_CheckRatesEntity> SP_CheckRatesEntitiys { get; set; }
        DbSet<SPSpb_VoidEntity> SPSpb_VoidEntitiys { get; set; }
        DbSet<SPSpb_UpdatePaymentEntity> SPSpb_UpdatePaymentEntitys { get; set; }
        DbSet<SPSpb_FOCEntity> SPSpb_FOCEntitys { get; set; }
        DbSet<ListGoodsMonitoringReceiverEntity> ListGoodsMonitoringReceiverEntities { get; set; }
        DbSet<SPManifestCarrierNumberGeneratorEntity> SPManifestCarrierNumberGeneratorEntitys { get; set; }
        DbSet<SP_PrivilegeDistribusiPrintEntity> SP_PrivilegeDistribusiPrintEntitys { get; set; }
        DbSet<SP_SaveDataCustomerHeaderEntity> SP_SaveDataCustomerHeaderEntitys { get; set; }
        DbSet<SP_PrivilegeMenuListEntity> SP_PrivilegeMenuListEntitys { get; set; }
        DbSet<SP_PrivilegeMenuActionListEntity> SP_PrivilegeMenuActionListEntitys { get; set; }
        DbSet<SP_PrivilegeMenuActionDetailListEntity> SP_PrivilegeMenuActionDetailListEntitys { get; set; }
        DbSet<SP_PrivilegeRoleListBlockEntity> SP_PrivilegeRoleListBlockEntitys { get; set; }
        DbSet<SP_PrivilegeRoleListTujuanEntity> SP_PrivilegeRoleListTujuanEntitys { get; set; }
        DbSet<SP_CheckDestBlockCityEntity> SP_CheckDestBlockCityEntitys { get; set; }
        DbSet<SP_PrivilegeManageDataEntity> SP_PrivilegeManageDataEntitys { get; set; }
        DbSet<SP_RoleManageDataEntity> SP_RoleManageDataEntitys { get; set; }
        DbSet<SP_UserManageDataEntity> SP_UserManageDataEntitys { get; set; }
        DbSet<SP_PrivilegeRoleManageDataEntity> SP_PrivilegeRoleManageDataEntitys { get; set; }
        DbSet<SP_SaveDataDestBlockCityEntity> SP_SaveDataDestBlockCityEntitys { get; set; }
        DbSet<SP_SaveDataUserEntity> SP_SaveDataUserEntitys { get; set; }
        DbSet<SP_SaveDataUserDetailEntity> SP_SaveDataUserDetailEntitys { get; set; }
        DbSet<SP_SaveDataUnitSubBranchEntity> SP_SaveDataUnitSubBranchEntitys { get; set; }
        DbSet<SP_SaveDataPrivilegeEntity> SP_SaveDataPrivilegeEntitys { get; set; }
        DbSet<SP_SaveUnitPlaceEntity> SP_SaveUnitPlaceEntitys { get; set; }
        DbSet<SP_UpdateStatusEntity> SP_UpdateStatusEntitys { get; set; }
        DbSet<SP_GetDataCustomerDetailEntity> SP_GetDataCustomerDetailEntitys { get; set; }
        DbSet<SP_GetDataTotalCustomerDetailEntity> SP_GetDataTotalCustomerDetailEntitys { get; set; }
        DbSet<SP_CheckAgreeEntity> SP_CheckAgreeEntitys { get; set; }
        DbSet<SP_ManifestCarrier_GetManifestEntity> SP_ManifestCarrier_GetManifestEntitys { get; set; }
        DbSet<SP_ManifestCarrier_LastManifestEntity> SP_ManifestCarrier_LastManifestEntitys { get; set; }
        DbSet<SP_MasterCityCreateEntity> SP_MasterCityCreateEntitys { get; set; }
        DbSet<SP_MasterCityDeleteEntity> SP_MasterCityDeleteEntitys { get; set; }
        DbSet<SP_ManifestListEntity> SP_ManifestListEntitys { get; set; }
        DbSet<SP_SPBListEntity> SP_SPBListEntitys { get; set; }
        DbSet<SP_MonitoringBarangEntity> SP_MonitoringBarangEntitys { get; set; }
        DbSet<SP_MonitoringSpbEntity> SP_MonitoringSpbEntitys { get; set; }
        DbSet<SP_DeleteUserDetailEntity> SP_DeleteUserDetailEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_FlightRouteEntity> SP_ManifestCarrierMaster_FlightRouteEntitys { get; set; }
        DbSet<SP_SpbCreate_ListSubDistrictEntity> SP_SpbCreate_ListSubDistrictEntitys { get; set; }
        DbSet<SP_MappingAreaNoExistsCreateEntity> SP_MappingAreaNoExistsCreateEntitys { get; set; }
        DbSet<SP_MappingAreaNoExistsDeleteEntity> SP_MappingAreaNoExistsDeleteEntitys { get; set; }
        DbSet<SP_MonitoringCapacityEntity> SP_MonitoringCapacityEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorCreateEntity> SP_ManifestCarrierMaster_VendorCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorDeleteEntity> SP_ManifestCarrierMaster_VendorDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorOriginCreateEntity> SP_ManifestCarrierMaster_VendorOriginCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorOriginDeleteEntity> SP_ManifestCarrierMaster_VendorOriginDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorTransitCreateEntity> SP_ManifestCarrierMaster_VendorTransitCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorTransitDeleteEntity> SP_ManifestCarrierMaster_VendorTransitDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorDestinationCreateEntity> SP_ManifestCarrierMaster_VendorDestinationCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorDestinationDeleteEntity> SP_ManifestCarrierMaster_VendorDestinationDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorCarrierCreateEntity> SP_ManifestCarrierMaster_VendorCarrierCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_VendorCarrierDeleteEntity> SP_ManifestCarrierMaster_VendorCarrierDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_CarrierCreateEntity> SP_ManifestCarrierMaster_CarrierCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_CarrierDeleteEntity> SP_ManifestCarrierMaster_CarrierDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_FlightCreateEntity> SP_ManifestCarrierMaster_FlightCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_FlightDeleteEntity> SP_ManifestCarrierMaster_FlightDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_FlightRouteCreateEntity> SP_ManifestCarrierMaster_FlightRouteCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_FlightRouteDeleteEntity> SP_ManifestCarrierMaster_FlightRouteDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_TransitRouteCreateEntity> SP_ManifestCarrierMaster_TransitRouteCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_TransitRouteDeleteEntity> SP_ManifestCarrierMaster_TransitRouteDeleteEntitys { get; set; }
        DbSet<SP_CheckPrintStatusEntity> SP_CheckPrintStatusEntitys { get; set; }
        DbSet<SP_SuperVision_GetDocIdEntity> SP_SuperVision_GetDocIdEntitys { get; set; }
        DbSet<SP_SuperVisionRePrintEntity> SP_SuperVisionRePrintEntitys { get; set; }
        DbSet<SP_MonitoringShipmentCreateEntity> SP_MonitoringShipmentCreateEntitys { get; set; }
        DbSet<SP_MonitoringShipmentUpdateEntity> SP_MonitoringShipmentUpdateEntitys { get; set; }
        DbSet<SP_MonitoringShipmentDeleteEntity> SP_MonitoringShipmentDeleteEntitys { get; set; }
        DbSet<SP_MonitoringShipmentListEntity> SP_MonitoringShipmentListEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_StationCreateEntity> SP_ManifestCarrierMaster_StationCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_StationDeleteEntity> SP_ManifestCarrierMaster_StationDeleteEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_StationCityCreateEntity> SP_ManifestCarrierMaster_StationCityCreateEntitys { get; set; }
        DbSet<SP_ManifestCarrierMaster_StationCityDeleteEntity> SP_ManifestCarrierMaster_StationCityDeleteEntitys { get; set; }
        DbSet<SP_CheckFragilePerishableCityEntity> SP_CheckFragilePerishableCityEntitys { get; set; }
        DbSet<SP_GeneratePasswordEntity> SP_GeneratePasswordEntitys { get; set; }
        DbSet<SP_Statistic_OmsetEntity> SP_Statistic_OmsetEntitys { get; set; }
        DbSet<SP_GenerateCustomerCodeEntity> SP_GenerateCustomerCodeEntitys { get; set; }
        DbSet<SP_GeneratePorterCodeEntity> SP_GeneratePorterCodeEntitys { get; set; }
        DbSet<SP_PorterCode_CreateEntity> SP_PorterCode_CreateEntitys { get; set; }
        DbSet<SP_CawFinalKoliEntity> SP_CawFinalKoliEntitys { get; set; }
        DbSet<SP_CawFinalSpbEntity> SP_CawFinalSpbEntitys { get; set; }
        DbSet<SP_CawVolumeEntity> SP_CawVolumeEntitys { get; set; }
        DbSet<SP_UpdateKiloEntity> SP_UpdateKiloEntitys { get; set; }
        DbSet<SPSpbGoods_CalculatePriceEntity> SPSpbGoods_CalculatePriceEntitys { get; set; }
        DbSet<SP_DividedListEntity> SP_DividedListEntitys { get; set; }
        DbSet<SP_DividedCreateEntity> SP_DividedCreateEntitys { get; set; }
        DbSet<SP_GetSpbManifestEntity> SP_GetSpbManifestEntitys { get; set; }
        DbSet<SP_CoaCodeCreateEntity> SP_CoaCodeCreateEntities { get; set; }
        DbSet<SP_CoaCodeUpdateEntity> SP_CoaCodeUpdateEntities { get; set; }
        DbSet<SP_AddBukuKasEntity> SP_AddBukuKasEntities { get; set; }
        DbSet<SP_BukuKasCreateEntity> SP_BukuKasCreateEntities { get; set; }
        DbSet<SP_ExpensesCreateEntity> SP_ExpensesCreateEntities { get; set; }
        DbSet<SP_DeleteBukuKasEntity> SP_DeleteBukuKasEntities { get; set; }
        DbSet<SP_UpdateBukuKasEntity> SP_UpdateBukuKasEntities { get; set; }
        DbSet<SP_GeneralLedgerListEntity> SP_GeneralLedgerListEntities { get; set; }
        DbSet<SP_ExpensesListEntity> SP_ExpensesListEntities { get; set; }
        DbSet<SP_CostListEntity> SP_CostListEntities { get; set; }
        DbSet<SP_ProfitLossPerDayListEntity> SP_ProfitLossPerDayListEntities { get; set; }
        DbSet<SP_ARListEntity> SP_ARListEntities { get; set; }
        DbSet<SP_AccountReceivableListEntity> SP_AccountReceivableListEntities { get; set; }
        DbSet<SP_GetSalesReportOnlyTotalEntity> SP_GetSalesReportOnlyTotalEntities { get; set; }
        DbSet<SP_InsertAREntity> SP_InsertAREntities { get; set; }
        DbSet<SP_SalesReportEntity> SP_SalesReportEntities { get; set; }
        // --- 
        // --- --- --- --- ---
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        int SaveChanges();
        void RemoveRange(params object[] entities);
        //object Set<T>();
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit.Queries
{
    public partial class MasterTransitReadFirstFlightQuery : IRequest<ReturnFormat>
    {
        public Int32? carrierId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MasterTransitReadFirstFlightQueryHandler : IRequestHandler<MasterTransitReadFirstFlightQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MasterTransitReadFirstFlightQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MasterTransitReadFirstFlightQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // --- --- --- --- ---
            var query = from f in _context.FlightEntitys
                        join c in _context.CarrierEntitys on f.CarrierId equals c.CarrierId
                        where f.CarrierId.Equals(req.carrierId)
                        select new
                        {
                            f.FlightId
                            , FlightNo = c.CarrierCode + " - " + f.FlightNo
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.FlightId
                        , s.FlightNo
                    }).OrderBy(o => o.FlightId);
            }

            //
            // return
            return rtn;
        }
    }
}

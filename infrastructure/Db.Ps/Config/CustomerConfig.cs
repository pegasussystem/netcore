﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class CustomerConfig : IEntityTypeConfiguration<CustomerEntity>
    {
        public void Configure(EntityTypeBuilder<CustomerEntity> builder)
        {
            builder.HasKey(e => e.CustomerId);
            builder.ToTable("Customer");


            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

﻿using Core.Entity.Base;
using Db.Ps;
using Interface.Repo.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Base
{
    public class MenuRepo : Repo<MenuEntity>, IMenuRepo
    {
        public MenuRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public new MenuEntity Update(MenuEntity entity)
        { 
            var _data = new MenuEntity { MenuId = entity.MenuId };

            if (entity.IsPage != null)
            {
                _data.IsPage = entity.IsPage;
                this._contextPs.Entry(_data).Property("IsPage").IsModified = true;
            }

            if (entity.MenuName != null)
            {
                _data.MenuName = entity.MenuName;
                this._contextPs.Entry(_data).Property("MenuName").IsModified = true;
            }

            if (entity.Route != null)
            {
                _data.Route = entity.Route;
                this._contextPs.Entry(_data).Property("Route").IsModified = true;
            }

            if (entity.MenuCode != null)
            {
                _data.MenuCode = entity.MenuCode;
                this._contextPs.Entry(_data).Property("MenuCode").IsModified = true;
            }

            if (entity.MenuIdParent == null)
            {
                _data.MenuIdParent = 0;
                this._contextPs.Entry(_data).Property("MenuIdParent").IsModified = true;
            }

            if (entity.MenuIdParent != null)
            {
                _data.MenuIdParent = entity.MenuIdParent;
                this._contextPs.Entry(_data).Property("MenuIdParent").IsModified = true;
            }

            this._contextPs.SaveChanges();

            return _data;
        }
    }
}

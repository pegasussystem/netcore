using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;
using Dynamitey.DynamicObjects;

namespace core.app.UseCases.Master.CoaCode.Commands
{
    public partial class CoaCityRevenueLiabilityCreateCommands : IRequest<ReturnFormat>
    {
        public Int32 coaIdRevenue { get; set; }
        public Int32 coaIdLiability { get; set; }
        public Int32 coaIdAr { get; set; }
        public String moda { get; set; }
        public String cityId { get; set; }
        public String isCorporate { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CoaCityRevenueLiabilityCreateCommandsHandler : IRequestHandler<CoaCityRevenueLiabilityCreateCommands, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CoaCityRevenueLiabilityCreateCommandsHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CoaCityRevenueLiabilityCreateCommands req, CancellationToken cancellationToken)
        {
            ReturnFormat rtn = new ReturnFormat();

            try
            {
                int? isCorporate = null;
                if (req.isCorporate == "Non Corporate")
                {
                    isCorporate = null;  // Set menjadi null jika Non Corporate
                }
                else if (req.isCorporate == "Corporate")
                {
                    isCorporate = 1;  // Set menjadi 1 jika isCorporate adalah "1"
                }
                // Membuat objek CoaCityRevenueLiability baru
                var newCoaCityRevenueLiability = new CoaCityRevenueLiabilityEntity
                {
                    CoaIdRevenue = req.coaIdRevenue,
                    CoaIdLiability = req.coaIdLiability,
                    CarrierType = req.moda,
                    CoaIdAR = req.coaIdAr,
                    CityId = req.cityId,
                    IsCorporate = isCorporate,
                    CreatedAt = DateTime.Now
                };

                // Menambahkan entitas baru ke konteks
                _context.CoaCityRevenueLiabilityEntities.Add(newCoaCityRevenueLiability);

                // Menyimpan perubahan ke database
                await _context.SaveChangesAsync(cancellationToken);

                // Set status dan pesan berhasil
                rtn.Status = StatusCodes.Status201Created;

                rtn.Data = new
                {
                    result = "true",
                    message = "Insert Data Successfully"
                };

            }
            catch (Exception ex)
            {
                // Menangani jika terjadi error
                var err = ex.Message;
                rtn.Status = StatusCodes.Status500InternalServerError;
                rtn.Data = err;
            }

            return rtn;

        }


    }
}
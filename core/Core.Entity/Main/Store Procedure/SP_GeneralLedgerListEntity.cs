using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_GeneralLedgerListEntity
    {
        public String? tanggal { get; set; }
        public DateTime createdAt { get; set; }
        public Int32? coaParentId { get; set; }
        public String? coaCode { get; set; }
        public String? coaName { get; set; }
        public String? keterangan { get; set; }
        public Decimal? total { get; set; }
        public Int32? lastDigit { get; set; }
        public String? karakterPertama { get; set; }
        public String? karakterKedua { get; set; }
        public String? karakterKetiga { get; set; }
        public String? karakterKeempat { get; set; }
        public String? karakterKelima { get; set; }
        public String? karakterKeenam { get; set; }
        public String? karakterKetujuh { get; set; }
        public String? karakterKedelapan { get; set; }
    }
}

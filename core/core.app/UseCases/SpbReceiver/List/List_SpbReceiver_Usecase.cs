﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Main.SpbReceiver;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace core.app.UseCases.SpbReceiver.List
{
    class ListSpbReceiverUsecase : IListSpbReceiverUsecase
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public ListSpbReceiverUsecase(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Delete(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        ////------------------ Sudah tidak dipake di pindahkan ke Core.app -> UseCase -> Spb -> Commands -> ListReceiver -> ListReceiverQuery ------------------------//
        //public ReturnFormat List(UiUserProfileEntity uiUserProfileEntity, object obj)
        //{
        //    //
        //    // variable
        //    ReturnFormat rtn = new ReturnFormat();
        //    List<VSpbEntity> vSpbEntities = null;
        //    BranchEntity branch = null;

        //    //
        //    // set variable
        //    rtn.Status = StatusCodes.Status204NoContent;
        //    ListSpbReceiverInput input = obj as ListSpbReceiverInput;

        //    //
        //    // logic



        //    // from branch
        //    if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId == null)
        //    {
        //        // get branch handled 
        //        branch = _repoPs.BranchRepo.Read(w => w.BranchId.Equals(uiUserProfileEntity.BranchId)).FirstOrDefault();
        //        var branchList = (branch.CityHandled != null) ? branch.CityHandled.Replace("|<", "").Replace(">|","").Split("><") : null;

        //        vSpbEntities = _repoPs.VSpbRepo
        //            .Read(w => branchList.Contains(w.DestinationCityId) 
        //                        && w.CreatedAt.Value.Month.Equals(input.Month()) 
        //                        && w.CreatedAt.Value.Year.Equals(input.Year()) )
        //            .ToList();
        //    }

        //    // from company
        //    if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId == null && uiUserProfileEntity.SubBranchId == null)
        //    {
        //        vSpbEntities = _repoPs.VSpbRepo
        //            .Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
        //                        && w.CreatedAt.Value.Month.Equals(input.Month())
        //                        && w.CreatedAt.Value.Year.Equals(input.Year()))
        //            .ToList();
        //    }



        //    if (vSpbEntities != null) {
        //        rtn.Status = StatusCodes.Status200OK;
        //        rtn.Data = vSpbEntities
        //            .Select( s => new {
        //                s.SpbId, s.SpbNo,  s.Username,
        //                s.OriginCityNameCustom, s.DestinationCityNameCustom,
        //                s.Rates, s.Packing, s.Quarantine, s.Etc, s.Ppn, s.Discount,s.TotalPrice,
        //                s.SenderName, s.SenderPhone, s.SenderPlace, s.SenderStore, s.SenderAddress,
        //                s.ReceiverName, s.ReceiverPhone, s.ReceiverPlace, s.ReceiverStore, s.ReceiverAddress,
        //                s.CarrierName, s.PaymentMethod, s.QualityOfServiceName, s.TypeOfServiceName,
        //                CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)uiUserProfileEntity.WorkTimeZoneMinute)
        //            });
        //    }

        //    //
        //    // return
        //    return rtn;
        //}

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(SpbEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

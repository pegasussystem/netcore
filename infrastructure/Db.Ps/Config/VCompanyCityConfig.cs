﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VCompanyCityConfig : IEntityTypeConfiguration<VCompanyCityEntity>
    {
        public void Configure(EntityTypeBuilder<VCompanyCityEntity> builder)
        {
            builder.HasKey(e => e.CompanyCityId);
            builder.ToTable("VCompanyCity");
        }
    }
}

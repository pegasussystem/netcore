﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MonitoringShipment.Query
{
    public partial class MonitoringShipmentReadFlightQuery : IRequest<ReturnFormat>
    {
        public Int32? carrierId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MonitoringShipmentReadFlightQueryHandler : IRequestHandler<MonitoringShipmentReadFlightQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MonitoringShipmentReadFlightQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MonitoringShipmentReadFlightQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.FlightEntitys
                        where a.CarrierId.Equals(req.carrierId)

                        select new
                        {
                            a.FlightId
                            , a.FlightNo
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.FlightId,
                        s.FlightNo

                    }).OrderBy(o => o.FlightId);
            }

            // return
            return rtn;
        }
    }
}



using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_BukuKasCoaHierarchyCurrentMonthConfig : IEntityTypeConfiguration<V_BukuKasCoaHierarchyCurrentMonthEntity>
    {
        public void Configure(EntityTypeBuilder<V_BukuKasCoaHierarchyCurrentMonthEntity> builder)
        {
            builder.HasKey(e => e.bukuKasId);
            builder.ToTable("V_BukuKasCoaHierarchyCurrentMonth");
        }
    }
}

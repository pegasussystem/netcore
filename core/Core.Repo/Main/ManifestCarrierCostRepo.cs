﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Base;
using Core.Entity.Main.View;
using System.Runtime.CompilerServices;

namespace Core.Repo.Main
{


    public class ManifestCarrierCostRepo : Repo<ManifestCarrierCostEntity>, IManifestCarrierCostRepo
    {
        public ManifestCarrierCostRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public void DeleteRange(List<ManifestCarrierCostEntity> entitys)
        {
            this._contextPs.Set<ManifestCarrierCostEntity>().RemoveRange(entitys);
        }

        public IEnumerable<VManifestCarrierCostEntity> VManifestCarrierCost(int ManifestCarrierId)
        {
            var query =
                from mcc in _contextPs.Set<ManifestCarrierCostEntity>()
               
                join l in _contextPs.Set<LookupEntity>() on mcc.CostType equals l.LookupKey
                join lu in _contextPs.Set<LookupEntity>() on mcc.Unit equals lu.LookupKey

                // get destination city
                join mci in _contextPs.Set<ManifestCarrierItemEntity>() on mcc.ManifestCarrierItemId equals mci.ManifestCarrierItemId
                join m in _contextPs.Set<ManifestEntity>() on mci.ManifestId equals m.ManifestId
                join ccd in _contextPs.Set<CompanyCityEntity>() on m.DestinationCityCode equals ccd.CompanyCityId
                join mcd in _contextPs.Set<MasterCityEntity>() on ccd.CityId equals mcd.MasterCityId

                where l.LookupKeyParent.Equals("cost") && mcc.ManifestCarrierId.Equals(ManifestCarrierId)
                
                

                group mcc by new { mcc.ManifestCarrierId, mcc.CostType, mcc.BaseCost, mcc.Unit, 
                    CostName = l.LookupValue, UnitValue = lu.LookupValue, l.Order ,
                    CityCodeDestination = mcd.MasterCityCode
                    
                } into g
                
                select new VManifestCarrierCostEntity
                {
                    ManifestCarrierId = g.Key.ManifestCarrierId,
                    CostType = g.Key.CostType,
                    BaseCost = g.Key.BaseCost,
                    Unit = g.Key.Unit,
                    CostName = g.Key.CostName,
                    UnitValue = g.Key.UnitValue,
                    Order = g.Key.Order,
                   // Weight = g.Key.Weight,
                    CityCodeDestination = g.Key.CityCodeDestination
                };

            return query.ToList().OrderBy(o => o.Order).ThenBy(o => o.CityCodeDestination);

        }
    }
}

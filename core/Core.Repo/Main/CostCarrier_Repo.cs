﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Base;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Core.Repo.Main
{
    public class CostCarrier_Repo : Repo<CostCarrier_Entity>, I_CostCarrier_Repo
    {
        public CostCarrier_Repo(DbContextPs ctx) : base(ctx)
        {
        }

        
        //public IEnumerable<V_CostCarrier_Entity> V_CostCarrier(
        //        int? carrier1, int? carrier2, int? carrier3, int? carrier4
        //    )
        //{

        //    // create carrier string ==> filter data
        //    // --- --- ---
        //    //String carrier = "";
        //    //if (carrier1 != null) { carrier += "<" + carrier1.ToString() + ">"; }
        //    //if (carrier2 != null) { carrier += "<" + carrier2.ToString() + ">"; }
        //    //if (carrier3 != null) { carrier += "<" + carrier3.ToString() + ">"; }
        //    //if (carrier4 != null) { carrier += "<" + carrier4.ToString() + ">"; }
        //    //carrier = "|" + carrier + "|";

        //    // create query
        //    // --- --- ---
        //    IQueryable<V_CostCarrier_Entity> result =
        //        from cc in _contextPs.Set<CostCarrier_Entity>()
        //        join cci in _contextPs.Set<CostCarrierItem_Entity>() on cc.CostCarrierId equals cci.CostCarrierId

        //        where cc.CarrierId.Equals(carrier1)

        //        select new V_CostCarrier_Entity
        //        {
        //            CostCarrierId = cc.CostCarrierId,
        //            CarrierId = cc.CarrierId,
        //            CostCarrierItemId = cci.CostCarrierItemId,
        //            CostType = cci.CostType,
        //            Unit = cci.unit,
        //            BaseCost = cci.BaseCost,
        //            CostGroup = cci.CostGroup,
        //            UseWeight = cci.UseWeight,
        //            CityOrigin = cc.CityOrigin,
        //            CityDestination = cc.CityDestination
        //        };

        //    // return result as list
        //    // --- --- ---
        //    return result.ToList();

        //}

    }
}

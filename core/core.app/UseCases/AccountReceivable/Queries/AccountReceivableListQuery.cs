using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using core.app.Common.Interfaces;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.AccountReceivable.Queries
{
    public partial class AccountReceivableListQuery : IRequest<ReturnFormat>
    {
         public String cityId { get; set; }
        public String moda { get; set; }
        public String? isCorporate { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class AccountReceivableListQueryHandler : IRequestHandler<AccountReceivableListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public AccountReceivableListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(AccountReceivableListQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_AccountReceivableListEntity> entities;

            // DateTime utcDate = req.CreatedAt.Value.AddHours((double)(-1 * req.uiUserProfile.WorkTimeZoneHour)).AddMinutes((double)(req.uiUserProfile.WorkTimeZoneMinute * -1));


            entities = this.AccountReceivableList(req.cityId,req.moda);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                         s.arId
                         ,
                         s.createdAt
                         ,
                         s.keterangan
                         ,
                         s.coaCode
                         ,
                         s.coaName
                         ,
                         s.coaArCode
                         ,
                         s.coaArName
                         ,
                         s.cityNameCode
                         ,
                         s.debit
                         ,
                         s.credit
                         ,
                         s.saldo
                         ,
                         s.cumulativeBalance
                     });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_AccountReceivableListEntity> AccountReceivableList(String? cityId,String? moda)
        {
            String query = $@"EXEC SP_AccountReceivableList @cityId = '{cityId}',@moda = '{moda}'";
            var res = this._context.SP_AccountReceivableListEntities.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
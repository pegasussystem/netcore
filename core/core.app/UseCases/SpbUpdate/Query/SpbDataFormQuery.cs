﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.SpbUpdate.Query
{
    public partial class SpbDataFormQuery : IRequest<ReturnFormat>
    {
        public String? spbNo { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class SpbDataFormQueryHandler : IRequestHandler<SpbDataFormQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public SpbDataFormQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(SpbDataFormQuery req, CancellationToken cancellationToken)
        {
            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query =
                from mcc in _context.V_SpbDataFormEntitys
                where mcc.SpbNo.Equals(req.spbNo)

                select new
                {
                    mcc.SpbNo
	                , mcc.SpbId
	                , mcc.OriginCityCode
	                , mcc.OriginAreaCode
	                , mcc.OriginCityName
	                , mcc.OriginAreaName
	                , mcc.DestinationCityCode
	                , mcc.DestinationAreaCode
	                , mcc.DestinationCityName
	                , mcc.DestinationAreaName
	                , mcc.TypesOfGoods
	                , mcc.Carrier
                    , mcc.CarrierCode
	                , mcc.QualityOfService
	                , mcc.TypeOfService
	                , mcc.Description
	                , mcc.TotalPrice
	                , mcc.Rates
	                , mcc.Packing
	                , mcc.Quarantine
	                , mcc.Etc
	                , mcc.Ppn
                    , mcc.PpnValue
	                , mcc.Discount
                    , mcc.DiscountValue
	                , mcc.PaymentMethod
                    , mcc.SenderPhone
                    , mcc.SenderName
                    , mcc.SenderStore
                    , mcc.SenderPlace
                    , mcc.SenderAddress
                    , mcc.ReceiverPhone
                    , mcc.ReceiverName
                    , mcc.ReceiverStore
                    , mcc.ReceiverPlace
                    , mcc.ReceiverAddress
                    , mcc.ViaName
                    , mcc.SpbCawFinal
                    , mcc.SpbTotalKoli
                    , mcc.RatesType
                };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.SpbNo
	                    , s.SpbId
	                    , s.OriginCityCode
	                    , s.OriginAreaCode
	                    , s.OriginCityName
	                    , s.OriginAreaName
	                    , s.DestinationCityCode
	                    , s.DestinationAreaCode
	                    , s.DestinationCityName
	                    , s.DestinationAreaName
	                    , s.TypesOfGoods
	                    , s.Carrier
                        , s.CarrierCode
	                    , s.QualityOfService
	                    , s.TypeOfService
	                    , s.Description
	                    , s.TotalPrice
	                    , s.Rates
	                    , s.Packing
	                    , s.Quarantine
                        , s.Etc
	                    , s.Ppn
                        , s.PpnValue
	                    , s.Discount
                        , s.DiscountValue
	                    , s.PaymentMethod
                        , s.SenderPhone
                        , s.SenderName
                        , s.SenderStore
                        , s.SenderPlace
                        , s.SenderAddress
                        , s.ReceiverPhone
                        , s.ReceiverName
                        , s.ReceiverStore
                        , s.ReceiverPlace
                        , s.ReceiverAddress
                        , s.ViaName
                        , s.SpbCawFinal
                        , s.SpbTotalKoli
                        , s.RatesType

                    }).OrderBy(o => o.SpbNo);
            }

            //
            // return
            return rtn;
        }
    }
}

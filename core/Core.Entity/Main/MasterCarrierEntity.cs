﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MasterCarrierEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? MasterCarrierId { get; set; }
        public Int32? CompanyId { get; set; }
        public Int32? MasterCarrierVendorId { get; set; }
        public String? MasterCarrierName { get; set; }

    }
}

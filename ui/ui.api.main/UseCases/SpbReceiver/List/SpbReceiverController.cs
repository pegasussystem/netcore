﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using core.app.UseCases.SpbReceiver.List;
//using core.helper;
//using Core.Entity.Ui;
//using Interface.App;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Mvc;

////------------------ Sudah tidak dipake di pindahkan ke Controller -> UseCase -> Spb -> Receiver -> ListController ------------------------//

//namespace ui.api.main.UseCases.SpbListreceiver.List
//{
//    [Authorize]
//    [Route("spb-receiver")]
//    [ApiController]
//    public class SpbReceiverController : DefaultController
//    {
//        private readonly IAppWrapperPs _appPs;
//        public SpbReceiverController(IAppWrapperPs app)
//        {
//            _appPs = app;
//        }

//        [HttpGet("list")]
//        public async Task<IActionResult> List([FromQuery] ListSpbReceiverInput input)
//        {

//            //
//            // variable
//            ReturnFormat _returnFormat = new ReturnFormat();


//            //
//            // get data
//            //_returnFormat = _appPs.SpbListReceiverApp.Read(GetUserClaim(), input);
//            _returnFormat = _appPs.ListSpbReceiverUsecase.List(GetUserClaim(), input);


//            //
//            // return
//            return StatusCode(_returnFormat.Status, _returnFormat);
//        }
//    }
//}

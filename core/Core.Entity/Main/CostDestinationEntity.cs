﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class CostDestinationEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? CostDestinationId { get; set; }
        public String? CityDestinationId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? VendorOriginId { get; set; }
        public Int32? VendorDestinationId { get; set; }

    }
}

using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;



namespace core.app.UseCases.SalesReport.Queries
{
    public partial class SalesReportQuery : IRequest<ReturnFormat>
    {
        public String? startDate { get; set; }
        public String? endDate { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class SalesReportQueryHandler : IRequestHandler<SalesReportQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public SalesReportQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(SalesReportQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_SalesReportEntity> entities;


            // --- --- ---
            // logic
            // --- --- ---
            entities = this.SalesReportList(req.startDate, req.endDate);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                         s.DestinationCityCode
                         ,
                         s.CoaCode
                         ,
                         s.CoaName
                         ,
                         s.Carrier
                         ,
                         s.TotalAw
                         ,
                         s.TotalCaw
                         ,
                         s.Ta
                         ,
                         s.Tt
                         ,
                         s.Tk
                         ,
                         s.TotalTaTt
                         ,
                         s.DigitPertama
                         ,
                         s.DigitKedua,
                         s.DigitKetiga,
                         s.DigitKeempat,
                         s.DigitKelima,
                         s.DigitKeenam
                     });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_SalesReportEntity> SalesReportList(String? startDate, String? endDate)
        {

            if (!DateTime.TryParse(startDate, out DateTime parsedStartDate))
            {
                parsedStartDate = DateTime.Today;
            }

            if (!DateTime.TryParse(endDate, out DateTime parsedEndDate))
            {
                parsedEndDate = DateTime.Today.AddDays(1);
            }

            String query = $@"EXEC SP_SalesReport @startDate = '{parsedStartDate:yyyy-MM-dd}', @endDate = '{parsedEndDate:yyyy-MM-dd}'";
            var res = this._context.SP_SalesReportEntities.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
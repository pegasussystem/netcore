﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_CustomerCodeDataEntity
    {
        public Int32? CustomerHeaderId { get; set; }
        public String? CustomerCode { get; set; }
        public String? Telp { get; set; }
        public String? Phone { get; set; }
        public String? CustomerName { get; set; }
        public String? CustomerStore { get; set; }
        public String? CustomerPlace { get; set; }
        public String? CustomerAddress { get; set; }
        public Int32? Rt { get; set; }
        public Int32? Rw { get; set; }
        public String? UrbanName { get; set; }
        public String? PostalCode { get; set; }
        public String? SubDistrictName { get; set; }
        public String? CityId { get; set; }
        public String? CityCode { get; set; }
        public String? CityName { get; set; }
        public String? ProvinceName { get; set; }
    }
}

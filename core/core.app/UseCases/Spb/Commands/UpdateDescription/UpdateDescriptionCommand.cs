﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;


namespace core.app.UseCases.Spb.Commands.UpdateDescription
{
    public partial class UpdateDescriptionCommand : IRequest<ReturnFormat>
    {
        public Int64? SpbId { get; set; }
        public string Description { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class UpdateDescriptionCommandHandler : IRequestHandler<UpdateDescriptionCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdateDescriptionCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdateDescriptionCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                //cek key exists di entity
                var entity = await _context.SpbEntitys.FindAsync(req.SpbId);

                if (entity == null)
                {
                    return rtn;
                }

                //
                // logic
                var desc_lama       = entity.Description;
                entity.Description  = req.Description;
                entity.UpdatedBy    = req.uiUserProfile.userId.ToString();
                entity.UpdatedAt    = DateTime.UtcNow;

                await _context.SaveChangesAsync(cancellationToken);

                var desc = "Deskripsi lama ' "+desc_lama + " ' menjadi ' " + entity.Description + " ' ";

                this.SetHistoryAction(req.uiUserProfile.userId, entity.SpbId, "Update Description", "SPB List Sender", desc);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu = Menu;
            dataItem.Desc = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }
    }

}

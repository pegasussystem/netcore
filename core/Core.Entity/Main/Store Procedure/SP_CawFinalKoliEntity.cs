﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_CawFinalKoliEntity
    {
        public Decimal? Caw { get; set; }
        public Decimal? CawFinal { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_SpbDataFormEntity
    {
        public String? SpbNo { get; set; }
        public Int64? SpbId { get; set; }
        public Int32? OriginCityCode { get; set; }
        public Int32? OriginAreaCode { get; set; }
        public Int32? DestinationCityCode { get; set; }
        public Int32? DestinationAreaCode { get; set; }
        public String? OriginCityName { get; set; }
        public String? OriginAreaName { get; set; }
        public String? DestinationCityName { get; set; }
        public String? DestinationAreaName { get; set; }
        public String? TypesOfGoods { get; set; }
        public String? Carrier { get; set; }
        public String? CarrierCode { get; set; }
        public String? QualityOfService { get; set; }
        public String? TypeOfService { get; set; }
        public String? Description { get; set; }
        public Decimal? TotalPrice { get; set; }
        public Decimal? Rates { get; set; }
        public Decimal? Packing { get; set; }
        public Decimal? Quarantine { get; set; }
        public Decimal? Etc { get; set; }
        public Decimal? Ppn { get; set; }
        public Decimal? PpnValue { get; set; }
        public Decimal? Discount { get; set; }
        public Decimal? DiscountValue { get; set; }
        public String? PaymentMethod { get; set; }
        public String? SenderPhone { get; set; }
        public String? SenderName { get; set; }
        public String? SenderStore { get; set; }
        public String? SenderPlace { get; set; }
        public String? SenderAddress { get; set; }
        public String? ReceiverPhone { get; set; }
        public String? ReceiverName { get; set; }
        public String? ReceiverStore { get; set; }
        public String? ReceiverPlace { get; set; }
        public String? ReceiverAddress { get; set; }
        public String? ViaName { get; set; }
        public String? RatesType { get; set; }
        public Decimal? SpbCawFinal { get; set; }
        public Int32? SpbTotalKoli { get; set; }
    }
}

﻿using core.app.UseCases.UserSetting.ResetPassword.Command;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;


namespace ui.api.main.UseCases.UserSetting.ResetPassword
{
    [Authorize]
    [Route("user-setting")]
    [ApiController]
    public class ActionController : ApiController
    {
        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

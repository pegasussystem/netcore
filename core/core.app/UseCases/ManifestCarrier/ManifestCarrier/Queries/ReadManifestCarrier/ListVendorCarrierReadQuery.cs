﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListVendorCarrierReadQuery : IRequest<ReturnFormat>
    {
        public Int32? originStationId { get; set; }
        public Int32? destinationStationId { get; set; }
        public Int32? modaId { get; set; }
    }

    public class ListVendorCarrierReadQueryHandler : IRequestHandler<ListVendorCarrierReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListVendorCarrierReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListVendorCarrierReadQuery req, CancellationToken cancellationToken)
        {
            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.FlightRouteEntitys
                        join b in _context.VendorCarrierEntitys on a.VendorCarrierId equals b.VendorCarrierId
                        join c in _context.VendorEntitys on b.VendorId equals c.VendorId
                        where a.OriginStationId.Equals(req.originStationId) 
                            && a.DestinationStationId.Equals(req.destinationStationId)
                        select new
                        {
                            c.VendorId
                            , c.VendorName
                        };

            var query2 = query.Distinct();
            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(
                    s => new
                    {
                        s.VendorId
                        , s.VendorName
                    }).OrderByDescending(o => o.VendorId);
            }
            //
            // return
            return rtn;
        }
    }
}

﻿using System;

namespace Core.Entity.Main.View
{
    public class VManifestWithKoliEntity
    {

        public DateTime? CreatedAt { get; set; }
        public DateTime? CreatedAtLocal { get; set; }
        public Int64? ManifestId { get; set; }
        public Int32? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? SubBranchId { get; set; }
        public String? ManifestNo { get; set; }
        public Int64? ManifestKoliId { get; set; }
        public Int32? KoliNo { get; set; }
        public String? KoliType { get; set; }
        public byte? IsClose { get; set; }
        public Decimal? SumAw { get; set; }
        public Decimal? SumCaw { get; set; }

    }


}

﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_ManifestDetailListConfig : IEntityTypeConfiguration<V_ManifestDetailListEntity>
    {
        public void Configure(EntityTypeBuilder<V_ManifestDetailListEntity> builder)
        {
            builder.HasKey(e => e.ManifestId);
            builder.ToTable("V_ManifestDetailList");
        }
    }
}

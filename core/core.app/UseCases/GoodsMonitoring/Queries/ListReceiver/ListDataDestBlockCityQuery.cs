﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.GoodsMonitoring.Queries.ListReceiver
{
     public partial class ListDataDestBlockCityQuery : IRequest<ReturnFormat>
     {
        public Int32? UserId { get; set; }
        public String? CityValue { get; set; }
     }

    public class ListDataDestBlockCityQueryHandler : IRequestHandler<ListDataDestBlockCityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListDataDestBlockCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListDataDestBlockCityQuery req, CancellationToken cancellationToken)
        {
            // --- --- ---
            // variable
            // --- --- ---
            IEnumerable<SP_CheckDestBlockCityEntity> entities;
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.ListDestBlockCityDataValue(req.UserId, req.CityValue);


            // --- --- ---
            // return
            // --- --- ---
            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities
                    .Select(s => new
                    {
                        s.DataValue
                    }).FirstOrDefault().DataValue;
            }

            return rtn;
        }

        public IEnumerable<SP_CheckDestBlockCityEntity> ListDestBlockCityDataValue(Int32? UserId, String? CityValue)
        {
            String query = $@"SP_CheckDestBlockCity @userId={UserId}, @value='{CityValue}'";
            var res = this._context.SP_CheckDestBlockCityEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }
}

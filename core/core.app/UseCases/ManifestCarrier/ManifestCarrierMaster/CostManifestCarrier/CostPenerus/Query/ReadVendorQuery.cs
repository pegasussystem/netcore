﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using Core.Entity.Ui;
using core.helper;
using Interface.Other;
using core.app.Common.Interfaces;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostGerai.Query
{
    public partial class ReadVendorQuery : IRequest<ReturnFormat>
    {
        public Int32? stationId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadVendorQueryHandler : IRequestHandler<ReadVendorQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadVendorQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadVendorQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;
                var query = from a in _context.VendorEntitys
                            join b in _context.VendorDestinationEntitys on a.VendorId equals b.VendorId
                            where b.StationId.Equals(req.stationId)
                            select new
                            {
                                VendorId = b.VendorDestinationId
                                ,a.VendorName
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.VendorId
                            , s.VendorName
                        }).OrderBy(o => o.VendorId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.SpbHourly.List;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ui.api.main.UseCases.SpbHourly.List
{
    [Authorize]
    [Route("spb-hourly")]
    [ApiController]
    public class SpbHourlyController : DefaultController
    {
        private readonly IAppWrapperPs _appPs;
        public SpbHourlyController(IAppWrapperPs app)
        {
            _appPs = app;

        }


        [HttpGet("list")]
        public async Task<IActionResult> Execute([FromQuery] ListSpbHourlyInput input)
        {
            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();


            // --- --- ---
            // logic
            // --- --- ---
            _returnFormat = _appPs.ListSpbHourlyUseCase.List(GetUserClaim(), input);


            // --- --- ---
            // return
            // --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

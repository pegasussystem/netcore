﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VManifestCarrierItemEntity
    {




        public Int32? ManifestCarrierItemId { get; set; }
        public Int32? WeightSmu { get; set; }
        public Int32? Caw { get; set; }
        public long? ManifestId { get; set; }
        public String? ManifestNo { get; set; }
        public Int32? ManifestAlphabet { get; set; }
        public Int32? OriginCityCode { get; set; }
        public Int32? DestinationCityCode { get; set; }
        public String? Carrier { get; set; }
        public String? Alphabet { get; set; }
        public String? OriginMasterCityId { get; set; }
        public String? OriginMasterCityCode { get; set; }
        public String? DestinationMasterCityId { get; set; }
        public String? DestinationMasterCityCode { get; set; }

        public string SetManifestNo 
        {
            set { ManifestNo = ManifestNo = DestinationMasterCityCode + " " + Alphabet + " #" + ManifestId; } 
        }


    }





}

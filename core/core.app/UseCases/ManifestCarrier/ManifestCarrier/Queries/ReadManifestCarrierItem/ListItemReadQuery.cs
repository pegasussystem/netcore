﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries
{
    public partial class ListItemReadQuery : IRequest<ReturnFormat>
    {
        public Int32? ManifestCarrierId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListItemReadQueryHandler : IRequestHandler<ListItemReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListItemReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListItemReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            var query = from mci in _context.ManifestCarrierItemEntitys
                        join mf in _context.ManifestEntitys on mci.ManifestId equals mf.ManifestId
                        join al in _context.AlphabetEntitys on mf.ManifestAlphabet equals al.AlphabetId
                        join cc_ori in _context.CompanyCityEntitys on mf.OriginCityCode equals cc_ori.CompanyCityId
                        join mc_ori in _context.MasterCityEntitys on cc_ori.CityId equals mc_ori.MasterCityId
                        join cc_dest in _context.CompanyCityEntitys on mf.DestinationCityCode equals cc_dest.CompanyCityId
                        join mc_dest in _context.MasterCityEntitys on cc_dest.CityId equals mc_dest.MasterCityId
                        where mci.ManifestCarrierId.Equals(req.ManifestCarrierId)

                        select new
                        {
                            mci.ManifestCarrierItemId
                            , mci.Aw
                            , mci.Caw
                            , mf.ManifestId
                            , mf.ManifestAlphabet
                            , mf.OriginCityCode
                            , mf.DestinationCityCode
                            , mf.Carrier
                            , al.Alphabet
                            , OriginMasterCityId = mc_ori.MasterCityId
                            , OriginMasterCityCode = mc_ori.MasterCityCode
                            , DestinationMasterCityId = mc_dest.MasterCityId
                            , DestinationMasterCityCode = mc_dest.MasterCityCode
                            , ManifestNo = mc_dest.MasterCityCode + " " + al.Alphabet + " #" + mf.ManifestId
                            , mci.CawFinal
                            , mci.KoliFinal
                        };


            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.ManifestCarrierItemId
                        , s.Aw
                        , s.Caw
                        , s.ManifestId
                        , s.ManifestAlphabet
                        , s.OriginCityCode
                        , s.DestinationCityCode
                        , s.Carrier
                        , s.Alphabet
                        , s.OriginMasterCityId
                        , s.OriginMasterCityCode
                        , s.DestinationMasterCityId
                        , s.DestinationMasterCityCode
                        , s.ManifestNo
                        , s.CawFinal
                        , s.KoliFinal
                    }).OrderByDescending(o => o.ManifestCarrierItemId);
            }

            //
            // return
            return rtn;
        }
    }
}

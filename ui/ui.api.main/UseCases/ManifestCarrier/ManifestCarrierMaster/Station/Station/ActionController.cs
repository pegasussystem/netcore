﻿using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Station.Station.Command;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;


namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierMaster.Station.Station
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]

    public class ActionController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ActionController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("manifest-carrier-master-station-create")]
        public async Task<IActionResult> StationCreate([FromBody] ManifestCarrierMasterStationCreateCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("manifest-carrier-master-station-delete")]
        public async Task<IActionResult> StationDelete([FromBody] ManifestCarrierMasterStationDeleteCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

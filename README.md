# struktur project

### core : logic aplikasi
- core.app			: seluruh logic applikasi
- core.entity		: class entitas yang mempresentasikan model
- core.helper		: class bantuan yang dapat digunakan berulang ulang
- core.repo			: jembatan menuju database
- core.validator	: validator untuk entity

### Infrastructure : berhubungan dengan external seperti database, email, log, DLL

### interface
- infrastructure.log.serilog		: menulis log
- infrastructure.db.pegasussystem	: database utama pegasus system

### ui : layer presentasi yang dapat di akses oleh user
- ui.api.main		: API utama 
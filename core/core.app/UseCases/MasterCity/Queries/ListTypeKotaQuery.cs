﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;

namespace core.app.UseCases.MasterCity.Queries

{

    public partial class ListTypeKotaQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
    }
    public class ListTypeKotaQueryHandler : IRequestHandler<ListTypeKotaQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListTypeKotaQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListTypeKotaQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;



            var query_a = from a in _context.MasterCityEntitys

                        select new
                        {
                            a.MasterCityType
                        };

            if (query_a != null)
            {
                var query = query_a.Distinct();
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.MasterCityType

                    });
            }

            // return 
            return rtn;
        }


    }

}

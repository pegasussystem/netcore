﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CompanyAreaConfig : IEntityTypeConfiguration<CompanyAreaEntity>
    {
        public void Configure(EntityTypeBuilder<CompanyAreaEntity> builder)
        {
            builder.HasKey(e => e.CompanyAreaId);
            builder.ToTable("CompanyArea");


            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

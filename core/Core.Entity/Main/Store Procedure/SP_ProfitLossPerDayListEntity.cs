using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_ProfitLossPerDayListEntity
    {
        public DateTime Tanggal { get; set; } // Representasi tanggal
        public decimal Revenue { get; set; } // Representasi pendapatan
        public decimal Expenses { get; set; } // Representasi pengeluaran
        public decimal Biaya { get; set; } // Representasi biaya lainnya
        public decimal ProfitLoss { get; set; } // Representasi biaya lainnya
    }
}

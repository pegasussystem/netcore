﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Roles.Commands
{
    public partial class ManageDataCommand : IRequest<ReturnFormat>
    {
        public Int32? privilegeId { get; set; }
        public Int32? userId { get; set; }
        public Int32? roleId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManageDataCommandHandler : IRequestHandler<ManageDataCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManageDataCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManageDataCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.ManageData(req.privilegeId, req.userId, req.roleId, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_RoleManageDataEntity> ManageData(Int32? privilegeId, Int32? userId, Int32? roleId, Int32? login)
        {
            String query = $@"EXEC SP_RoleManageData @privilegeId={privilegeId}, @userId='{userId}', @roleId={roleId}, @login='{login}' ";
            var res = this._context.SP_RoleManageDataEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

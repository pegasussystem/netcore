﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class CustomerPlaceEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? CustomerId { get; set; }

        public Int32? CustomerPlaceId { get; set; }
        public String? CustomerPlace { get; set; }
    }
}

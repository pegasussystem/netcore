﻿using core.helper;
using Core.Entity.Main;
using Interface.App.Base;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace core.app.Main
{
    public class MasterUrbanApp : IMasterUrbanApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public MasterUrbanApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(MasterUrbanEntity entity)
        {
            throw new NotImplementedException();
        }

        public MasterUrbanEntity Delete(MasterUrbanEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat getUrban(String subDistrict)
        {
            ReturnFormat rtn = new ReturnFormat();
            try
            {
                List<MasterUrbanEntity> masterUrbanEntity = _repoPs.masterUrbanRepo.Read(w => w.SubDistrictCodeBps.Contains(subDistrict)).ToList();
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = masterUrbanEntity;
            }
            catch (Exception e)
            {
                rtn.Status = StatusCodes.Status500InternalServerError;
                rtn.Data = e.Message;
            }
            return rtn;
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(MasterUrbanEntity entity)
        {
            throw new NotImplementedException();
        }

        public MasterUrbanEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public MasterUrbanEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(MasterUrbanEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

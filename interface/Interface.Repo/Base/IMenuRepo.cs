﻿using Core.Entity.Base;

namespace Interface.Repo.Base
{
    public interface IMenuRepo: IRepo<MenuEntity>
    {
        MenuEntity Update(MenuEntity entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using MediatR;
using core.app.Common.Interfaces;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostCarrier.Query
{
    public partial class ReadVendorCarrierQuery : IRequest<ReturnFormat>
    {
        public Int32? carrierId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadVendorCarrierQueryHandler : IRequestHandler<ReadVendorCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadVendorCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadVendorCarrierQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                rtn.Status = StatusCodes.Status200OK;
                
                var qr = from a in _context.VendorCarrierEntitys
                         join b in _context.VendorEntitys on a.VendorId equals b.VendorId
                         where a.CarrierId.Equals(req.carrierId)
                        select new
                        {
                            a.VendorCarrierId
                            , VendorCarrierName = b.VendorName
                        };

                if (qr != null)
                {
                    var query = qr.Distinct();
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.VendorCarrierId
                            , s.VendorCarrierName
                        }).OrderBy(o => o.VendorCarrierId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}

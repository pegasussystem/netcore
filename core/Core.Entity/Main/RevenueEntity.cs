using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Text;

namespace Core.Entity.Main
{
    public class RevenueEntity
    {
        public Int64 revenueId { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public Int64? coaId { get; set; }
        public String? keterangan { get; set; }        
        public Decimal? totalPrice { get; set; }
         public Int64? spbId { get; set; }

    }
}

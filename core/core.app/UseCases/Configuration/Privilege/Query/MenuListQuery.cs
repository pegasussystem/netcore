﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Privilege
{
    public partial class MenuListQuery : IRequest<ReturnFormat>
    {
        public Int32? privilegeId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class MenuListQueryHandler : IRequestHandler<MenuListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MenuListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MenuListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_PrivilegeMenuListEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.PrivilegeMenuListData(req.privilegeId);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                        id = s.MenuId
                        , key = s.MenuKey
                        , name = s.MenuName
                        , parent = s.MenuParentId
                        , order = s.OrderCode
                        , value = (s.IsActive > 0 ? true : false)
                    }).OrderBy(o => o.parent).ThenBy(o=>o.order);
            }

            // return
            return rtn;
        }

        public IEnumerable<SP_PrivilegeMenuListEntity> PrivilegeMenuListData(Int32? privilegeId)
        {
            String query = $@"EXEC SP_PrivilegeMenuList @privilegeId={privilegeId}";
            var res = this._context.SP_PrivilegeMenuListEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }
}

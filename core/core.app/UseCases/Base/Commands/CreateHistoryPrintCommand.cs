﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Base
{
    public partial class CreateHistoryActionCommand : IRequest<ReturnFormat>
    {
        public String? spbNo { get; set; }
        public String? typePrint { get; set; }
        public String? menu { get; set; }
        public String? menuAction { get; set; }
        public Boolean? printed { get; set; }
        public String? desc { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateHistoryPrintCommandHandler : IRequestHandler<CreateHistoryActionCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateHistoryPrintCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateHistoryActionCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            var xNo = req.spbNo.Split(",");

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                HistoryPrintEntity dataItem = new HistoryPrintEntity();

                // Mark as Changed
                for (int x = 0; x < xNo.Count(); x++)
                {
                    List<SP_SuperVision_GetDocIdEntity> res = this.getDocId(xNo[x], req.typePrint).ToList();

                    dataItem = new HistoryPrintEntity();

                    dataItem.UserId = req.uiUserProfile.userId;
                    dataItem.DocId = Int32.Parse(res[0].message);
                    dataItem.TypePrint = req.typePrint;
                    dataItem.Menu = req.menu;
                    dataItem.MenuAction = req.menuAction;
                    dataItem.Printed = true;
                    dataItem.PrintDate = DateTime.UtcNow;
                    dataItem.Desc = req.desc;

                    _context.HistoryPrintEntitys.Add(dataItem);
                    _context.SaveChanges();
                }
                
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = dataItem;

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }
        public IEnumerable<SP_SuperVision_GetDocIdEntity> getDocId(String? docNo, String? typePrint)
        {
            String query = $@"EXEC SP_SuperVision_GetDocId @docNo='{docNo}', @typePrint='{typePrint}' ";
            var res = this._context.SP_SuperVision_GetDocIdEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }

}
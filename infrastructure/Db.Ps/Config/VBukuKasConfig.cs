using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class BukuKasConfig : IEntityTypeConfiguration<VBukuKasEntity>
    {
        public void Configure(EntityTypeBuilder<VBukuKasEntity> builder)
        {
            builder.HasKey(e => e.bukuKasId);
            builder.ToTable("VBukuKas");
        }
    }
}

using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Base;
using Core.Entity.Ui;

namespace Core.Repo.Main
{
    public class AccountReceivableRepo : Repo<AccountReceivableEntity>, IAccountReceivableRepo
    {
        public AccountReceivableRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public void InsertAr(SpbEntity spbEntity, UiUserProfileEntity entity)
        {

            var workUnitId = _contextPs.Set<UserAtEntity>()
                            .Where(userAt => userAt.UserId == entity.userId)
                            .Select(userAt => userAt.WorkUnitId)
                            .FirstOrDefault();

            int? isCorporate = _contextPs.Set<WorkUnitEntity>()
                .Where(w => w.WorkUnitId == workUnitId)
                .Select(w => w.WorkUnitName.Contains("Corporate") ? (int?)1 : (int?)null)
                .FirstOrDefault();

            var cityId = (from companyCity in _contextPs.Set<CompanyCityEntity>()
                          join masterCity in _contextPs.Set<MasterCityEntity>()
                          on companyCity.CityId equals masterCity.MasterCityId
                          where companyCity.CompanyCityId == spbEntity.DestinationCityCode
                          select masterCity.MasterCityId).FirstOrDefault();

            int? coaIdRevenue;
            int? revenueCoa;
            if (isCorporate.HasValue && isCorporate != 0)
            {
                revenueCoa = _contextPs.Set<CoaCityRevenueLiabilityEntity>()
                   .Where(cr => cr.CarrierType == spbEntity.Carrier && cr.IsCorporate == 1)
                   .Select(cr => cr.CoaIdRevenue)
                   .FirstOrDefault();

                if (spbEntity.PaymentMethod == "ta")
                {
                    coaIdRevenue = _contextPs.Set<CoaCityRevenueLiabilityEntity>()
                   .Where(cr => cr.CarrierType == spbEntity.Carrier && cr.IsKk1 == 1 && cr.IsCorporate == 1)
                   .Select(cr => cr.CoaIdRevenue)
                   .FirstOrDefault();
                }
                else
                {
                    coaIdRevenue = _contextPs.Set<CoaCityRevenueLiabilityEntity>()
                   .Where(cr => cr.CarrierType == spbEntity.Carrier && (cr.IsKk1 == null || cr.IsKk1 == 0) && cr.IsCorporate == 1 && cr.CityId == cityId)
                   .Select(cr => cr.CoaIdRevenue)
                   .FirstOrDefault();
                }
            }
            else
            {
                revenueCoa = _contextPs.Set<CoaCityRevenueLiabilityEntity>()
                  .Where(cr => cr.CarrierType == spbEntity.Carrier && (cr.IsCorporate == null || cr.IsCorporate == 0)  && cr.CityId == cityId)
                  .Select(cr => cr.CoaIdRevenue)
                  .FirstOrDefault();

                if (spbEntity.PaymentMethod == "ta")
                {
                    coaIdRevenue = _contextPs.Set<CoaCityRevenueLiabilityEntity>()
                   .Where(cr => cr.CarrierType == spbEntity.Carrier && cr.IsKk1 == 1 && (cr.IsCorporate == null || cr.IsCorporate == 0))
                   .Select(cr => cr.CoaIdRevenue)
                   .FirstOrDefault();
                }
                else
                {
                    coaIdRevenue = _contextPs.Set<CoaCityRevenueLiabilityEntity>()
                   .Where(cr => cr.CarrierType == spbEntity.Carrier && (cr.IsKk1 == null || cr.IsKk1 == 0) && (cr.IsCorporate == null || cr.IsCorporate == 0) && cr.CityId == cityId)
                   .Select(cr => cr.CoaIdRevenue)
                   .FirstOrDefault();
                }
            }

            var newArEntry = new AccountReceivableEntity
            {
                createdAt = DateTime.UtcNow,
                coaId = coaIdRevenue,
                keterangan = "SPB " + spbEntity.SpbNo + " EX " + DateTime.UtcNow.ToString("dd MMM yyyy"),
                debit = spbEntity.TotalPrice,
                credit = 0,
                saldo = spbEntity.TotalPrice - 0,
                spbId = spbEntity.SpbId
            };

            _contextPs.Set<AccountReceivableEntity>().Add(newArEntry);
            _contextPs.SaveChanges();  // Sinkron

           


            var newRevenueEntry = new RevenueEntity
            {
                createdAt = DateTime.UtcNow,
                updatedAt = null,
                coaId = revenueCoa, 
                keterangan = "SPB " + spbEntity.SpbNo + " EX " + DateTime.UtcNow.ToString("dd MMM yyyy"),
                spbId = spbEntity.SpbId,
                totalPrice = spbEntity.TotalPrice
            };

            _contextPs.Set<RevenueEntity>().Add(newRevenueEntry);
            _contextPs.SaveChanges();


            //  var newArId = newArEntry.arId;
             var newRevenueId = newRevenueEntry.revenueId;
              var newProfitLossStatementEntry = new ProfitLossStatementEntity
            {
                createdAt = DateTime.UtcNow,
                revenueId = newRevenueId,
                // arId = newArId,
                updatedAt = null,
                bukuKasId = null
            };

            _contextPs.Set<ProfitLossStatementEntity>().Add(newProfitLossStatementEntry);
            _contextPs.SaveChanges();

        }

    }
}

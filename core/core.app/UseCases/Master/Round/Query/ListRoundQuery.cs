﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.Round.Query
{
    public partial class ListRoundQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListRoundQueryHandler : IRequestHandler<ListRoundQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListRoundQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListRoundQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.RoundEntitys
                        join b in _context.LookupEntitys on a.CarrierId equals b.LookupKey
                        where b.LookupKeyParent.Equals("carrier")
                        select new
                        {
                            a.RoundId
                            , Carrier = b.LookupValue
							, a.first_value
							, a.last_value
							, a.final_value
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.RoundId
                        , s.Carrier
                        , s.first_value
                        , s.last_value
						, s.final_value
                    }).OrderBy(o => o.Carrier);
            }

            // return
            return rtn;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Base; // Pati 26Agt21
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace ui.api.main.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {

        private readonly IAppWrapperPs _appPs;
        public CustomerController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        UiUserProfileEntity GetUserClaim()
        {
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            // get claim user
            var claims = User.Claims.Where(w => w.Type.Equals("userProfile")).First().Value;
            uiUserProfileEntity = JsonConvert.DeserializeObject<UiUserProfileEntity>(claims);

            return uiUserProfileEntity;
        }

        [HttpPost("add")]
        public async Task<IActionResult> post([FromForm]CustomerEntity customerEntity, [FromForm]String name, [FromForm]String place, [FromForm]String store, [FromForm]String address)
        {
            //
            // variable
            CustomerEntity _customerEntity = new CustomerEntity();
            ReturnFormat _returnFormat = new ReturnFormat();

            _returnFormat = _appPs.CustomerApp.Create(customerEntity, name, place, store, address);
            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpGet("phone")]
        public async Task<IActionResult> Get(String CityId = "", String searchValue = "", int customerId = 0)
        {


            //
            // variable
            CustomerEntity _customerEntity = new CustomerEntity();
            ReturnFormat _returnFormat = new ReturnFormat();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();
            StringValues workCityId = "";

            //
            // set variable

            
            uiUserProfileEntity = GetUserClaim();
            _customerEntity.CityCode = CityId;
            _customerEntity.CustomerId = customerId;
            _customerEntity.Phone = searchValue;
            _customerEntity.CompanyId = uiUserProfileEntity.companyId;

            //
            // get data
            _returnFormat = _appPs.CustomerApp.Read(_customerEntity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("cekphone")]    // diunremark Pati 18Agust21
        public async Task<IActionResult> Get(String phone = "")
        {
            //
            // variable
            CustomerEntity _customerEntity = new CustomerEntity();
            ReturnFormat _returnFormat = new ReturnFormat();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();
            StringValues workCityId = "";

            //
            // set variable


            uiUserProfileEntity = GetUserClaim();
            //_customerEntity.CityCode = CityId;
            //_customerEntity.CustomerId = customerId;
            _customerEntity.Phone = phone;
            _customerEntity.CompanyId = uiUserProfileEntity.companyId;

            //
            // get data
            _returnFormat = _appPs.CustomerApp.CekPhone(_customerEntity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("approvalcust")]    // diunremark Pati 18Agust21
        public IActionResult GetAppCust(String username = "")
        {
            //
            // variable
            UserEntity _userEntity = new UserEntity();
            ReturnFormat _returnFormat = new ReturnFormat();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();
            StringValues workCityId = "";

            //
            // set variable


            uiUserProfileEntity = GetUserClaim();
            //_customerEntity.CityCode = CityId;
            //_customerEntity.CustomerId = customerId;
            _userEntity.Username = username;
            //_customerEntity.CompanyId = uiUserProfileEntity.companyId;

            //
            // get data
            _returnFormat = _appPs.CustomerApp.ApprovalCust(_userEntity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        //[HttpGet("approve")]    // diunremark Pati 18Agust21
        //public async Task<IActionResult> Get(String username = "")
        //{
        //    //
        //    // variable
        //    CustomerEntity _customerEntity = new CustomerEntity();
        //    ReturnFormat _returnFormat = new ReturnFormat();
        //    UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();
        //    StringValues workCityId = "";

        //    //
        //    // set variable


        //    uiUserProfileEntity = GetUserClaim();
        //    //_customerEntity.CityCode = CityId;
        //    //_customerEntity.CustomerId = customerId;
        //    _customerEntity.Phone = phone;
        //    _customerEntity.CompanyId = uiUserProfileEntity.companyId;

        //    //
        //    // get data
        //    _returnFormat = _appPs.CustomerApp.CekPhone(_customerEntity);

        //    //
        //    // return
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}



        [HttpGet("name")]
        public async Task<IActionResult> GetName(int customerId = 0, int customerNameId=0)
        {

            //
            // variable
            CustomerNameEntity _entity = new CustomerNameEntity();
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // set variable
            _entity.CustomerId = customerId;
            _entity.CustomerNameId = customerNameId;

            //
            // get data
            _returnFormat = _appPs.CustomerApp.ReadName(_entity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpGet("address")]
        public async Task<IActionResult> GetAddress(int customerId = 0, int customerAddressId = 0)
        {

            //
            // variable
            CustomerAddressEntity _entity = new CustomerAddressEntity();
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // set variable
            _entity.CustomerId = customerId;
            _entity.CustomerAddressId = customerAddressId;

            //
            // get data
            _returnFormat = _appPs.CustomerApp.ReadAddress(_entity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("place")]
        public async Task<IActionResult> GetPlace(int customerId = 0, int customerPlaceId = 0)
        {

            //
            // variable
            CustomerPlaceEntity _entity = new CustomerPlaceEntity();
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // set variable
            _entity.CustomerId = customerId;
            _entity.CustomerPlaceId = customerPlaceId;

            //
            // get data
            _returnFormat = _appPs.CustomerApp.ReadPlace(_entity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        
        [HttpGet("store")]
        public async Task<IActionResult> GetStore(int customerId = 0, int customerStoreId = 0)
        {

            //
            // variable
            CustomerStoreEntity _entity = new CustomerStoreEntity();
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // set variable
            _entity.CustomerId = customerId;
            _entity.CustomerStoreId = customerStoreId;

            //
            // get data
            _returnFormat = _appPs.CustomerApp.ReadStore(_entity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        #region <<<<< <<<<< customer sender >>>>> >>>>>
      
        [HttpGet("sender/city")]
        public IActionResult SenderCity() {
            //
            // variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            ReturnFormat _returnFormat = new ReturnFormat();
            UiUserProfileEntity uiUserProfileEntity = GetUserClaim();

            //
            // set variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            //
            // logic
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            _returnFormat = _appPs.MasterCityApp.ReadByProvince(null, uiUserProfileEntity);

            //
            // return
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("sender/sub-district")]
        public IActionResult SenderSubDistrict()
        {
            //
            // variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // set variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            //
            // logic
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            //
            // return
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("sender/urban")]
        public IActionResult SenderUrban()
        {
            //
            // variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // set variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            //
            // logic
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            //
            // return
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpGet("sender/postal-code")]
        public IActionResult SenderPostalCode()
        {
            //
            // variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // set variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            //
            // logic
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            //
            // return
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            return StatusCode(_returnFormat.Status, _returnFormat);
        }



        #endregion <<<<< <<<<< customer sender >>>>> >>>>>



        #region customer Receiver
        /// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        /// Customer Receiver
        /// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        #endregion customer Receiver



    }
}
﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface ISpbRepo : IRepo<SpbEntity>
    {
        IEnumerable<SPSpbNumberGeneratorEntity> SpbNumberGenerator(UiUserProfileEntity entity);

        IEnumerable<SPSpb_SetManifestIdEntity> SPSpb_SetManifestIdEntity(SpbEntity spbEntity);

        IEnumerable<SPSpb_VoidEntity> SPSpb_Void(UiUserProfileEntity entity, VSpbEntity spbEntity);

        
        // STATISTIK
        // -----------------------
        IEnumerable<SPSpb_WaktuInputDailyEntity> SPSpb_WaktuInputDailyEntity(DateTime dateFrom, DateTime dateTo);
    }
}

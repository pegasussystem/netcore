﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class CompanyEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }

        public Int32? CompanyId { get; set; }
        public String? CompanyCode { get; set; }
        public String? CompanyName { get; set; }
        public String? Address { get; set; }
        public String? TimeZone { get; set; }
        public Int32? CompanyCityId { get; set; }
        public Int32? CompanyAreaId { get; set; }



        //public Int32? CityId { get; set; }
        //public String? CityCode { get; set; }
        //public String? CItyName { get; set; }
        //public String? AreaType { get; set; }


    }
}

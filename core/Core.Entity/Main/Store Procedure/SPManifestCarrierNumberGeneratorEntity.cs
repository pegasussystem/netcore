﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SPManifestCarrierNumberGeneratorEntity
    {
        public String? ManifestCarrierNo { get; set; }
    }
}

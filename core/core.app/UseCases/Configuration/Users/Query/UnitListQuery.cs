﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Configuration.Users.Query
{
    public partial class UnitListQuery : IRequest<ReturnFormat>
    {
        public String? type { get; set; }
    }

    public class UnitListQueryHandler : IRequestHandler<UnitListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public UnitListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(UnitListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.WorkUnitEntitys
                        where a.IsActive.Equals(false)
                        select new
                        {
                            Id = a.WorkUnitId
                            , Code  = a.WorkUnitCode
                            , Name  = a.WorkUnitName
                            , Value = a.Value
                            , a.IsActive
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.Id
                        , s.Code
                        , s.Name
                        , s.Value
                    }).OrderBy(o => o.Id);
            }

            // return
            return rtn;
        }

    }
}

﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Main.View;

namespace Core.Repo.Main
{
    public class ManifestCarrierItemRepo : Repo<ManifestCarrierItemEntity>, IManifestCarrierItemRepo
    {
        public ManifestCarrierItemRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public void DeleteRange(List<ManifestCarrierItemEntity> entitys)
        {
            //this._contextPs.Set<T>().Attach(entity);
            this._contextPs.Set<ManifestCarrierItemEntity>().RemoveRange(entitys);
            //return this._contextPs.Set<T>().Remove(entity).Entity;
        }

        public IEnumerable<VManifestCarrierItemEntity> VManifestCarrierItem(int ManifestCarrierId) {
            var query =
                from mci in _contextPs.Set<ManifestCarrierItemEntity>()
                join m in _contextPs.Set<ManifestEntity>() on mci.ManifestId equals m.ManifestId
                join a in _contextPs.Set<AlphabetEntity>() on m.ManifestAlphabet equals a.AlphabetId
                // origin
                join cco in _contextPs.Set<CompanyCityEntity>() on m.OriginCityCode equals cco.CompanyCityId
                join mco in _contextPs.Set<MasterCityEntity>()  on cco.CityId equals mco.MasterCityId
                // destination
                join ccd in _contextPs.Set<CompanyCityEntity>() on m.DestinationCityCode equals ccd.CompanyCityId
                join mcd in _contextPs.Set<MasterCityEntity>() on ccd.CityId equals mcd.MasterCityId

                where mci.ManifestCarrierId.Equals(ManifestCarrierId)

                select new VManifestCarrierItemEntity {
                    ManifestCarrierItemId = mci.ManifestCarrierItemId,
                    //Caw = mci.Caw,
                    ManifestId = m.ManifestId,
                    ManifestAlphabet = m.ManifestAlphabet,
                    OriginCityCode= m.OriginCityCode,
                    DestinationCityCode = m.DestinationCityCode,
                    Carrier = m.Carrier,
                    Alphabet = a.Alphabet,
                    OriginMasterCityId = mco.MasterCityId, OriginMasterCityCode =  mco.MasterCityCode ,
                    DestinationMasterCityId = mcd.MasterCityId ,	DestinationMasterCityCode = mcd.MasterCityCode ,
                    SetManifestNo = "",
                    //WeightSmu = mci.WeightSmu
                };

            return query.ToList();


                
        }
    }
}

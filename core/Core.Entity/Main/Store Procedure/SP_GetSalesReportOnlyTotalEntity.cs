using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_GetSalesReportOnlyTotalEntity
    {
        public string DestinationCityCode { get; set; }  // CityCode dari tujuan (destination)
        public string OriginCityCode { get; set; }       // CityCode dari asal (origin)
        public decimal TotalPrice { get; set; }        // Total harga
        public string Carrier { get; set; }        // Total harga
        public DateTime LastCreatedAtManifest { get; set; }  // Tanggal terakhir pembuatan Manifest
    }
}

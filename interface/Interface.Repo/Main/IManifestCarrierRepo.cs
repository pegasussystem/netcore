﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface IManifestCarrierRepo : IRepo<ManifestCarrierEntity>
    {
        public IEnumerable<VManifestCarrierEntity> VManifestCarrier(UiUserProfileEntity uiUserProfileEntity);

        public IEnumerable<VManifestCarrier_Report_List> ManifestCarrier_Report_List(UiUserProfileEntity userProfile, String flightDate);
    }
}

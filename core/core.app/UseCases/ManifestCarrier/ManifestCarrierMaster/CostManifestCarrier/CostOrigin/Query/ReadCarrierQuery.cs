﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using core.helper;
using Interface.Repo;
using Interface.Other;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostOrigin.Query
{
    public partial class ReadCarrierQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadCarrierQueryHandler : IRequestHandler<ReadCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadCarrierQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;
                var query = from a in _context.CarrierEntitys
                        select new
                        {
                            a.CarrierId
                            , a.CarrierCode
                            , CarrierName = "[ "+a.CarrierCode+" ] " + a.CarrierName
                        };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.CarrierId
                            , s.CarrierCode
                            , s.CarrierName
                        }).OrderBy(o => o.CarrierId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
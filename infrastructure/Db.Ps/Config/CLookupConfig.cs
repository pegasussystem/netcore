﻿using Core.Entity.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class CLookupConfig : IEntityTypeConfiguration<LookupEntity>
    {
        public void Configure(EntityTypeBuilder<LookupEntity> builder)
        {
            builder.HasKey(e => e.LookupId);
            builder.ToTable("Lookup");


        }
    }
}

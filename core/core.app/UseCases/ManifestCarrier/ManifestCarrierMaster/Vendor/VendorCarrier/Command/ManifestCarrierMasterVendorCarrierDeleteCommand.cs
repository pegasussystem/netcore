﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;


namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorCarrier.Command
{
    public partial class ManifestCarrierMasterVendorCarrierDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? VendorCarrierId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterVendorCarrierDeleteCommandHandler : IRequestHandler<ManifestCarrierMasterVendorCarrierDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterVendorCarrierDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterVendorCarrierDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.VendorCarrierDelete(req.VendorCarrierId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_VendorCarrierDeleteEntity> VendorCarrierDelete(Int32? VendorCarrierId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_VendorCarrierDelete @VendorCarrierId='{VendorCarrierId}'";
            var res = this._context.SP_ManifestCarrierMaster_VendorCarrierDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

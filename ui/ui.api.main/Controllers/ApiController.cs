﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace ui.api.main.Controllers
{
    public abstract class ApiController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

       public UiUserProfileEntity GetUserClaim()
        {
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            // get claim user
            var claims = User.Claims.Where(w => w.Type.Equals("userProfile")).First().Value;
            uiUserProfileEntity = JsonConvert.DeserializeObject<UiUserProfileEntity>(claims);

            return uiUserProfileEntity;
        }
    }
}

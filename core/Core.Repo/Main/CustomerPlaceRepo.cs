﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class CustomerPlaceRepo : Repo<CustomerPlaceEntity>, ICustomerPlaceRepo
    {
        public CustomerPlaceRepo(DbContextPs ctx) : base(ctx)
        {
        }
    }
}

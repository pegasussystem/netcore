using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using core.app.Common.Interfaces;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace core.app.UseCases.ProftiLossStatement.Queries
{
    public partial class ProfitLossPerDayListQuery : IRequest<ReturnFormat>
    {
        public int? month { get; set; }
        public int? year { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class ProfitLossPerDayListQueryHandler : IRequestHandler<ProfitLossPerDayListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ProfitLossPerDayListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ProfitLossPerDayListQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_ProfitLossPerDayListEntity> entities;

            if (req.month != null && req.year != null && req.month != 0 && req.year != 0)
            {
                entities = this.GetProfitLossListByMonth(req.month, req.year);
            }
            else
            {
                entities = this.DataProfitLossList();
            }

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                        s.Tanggal,
                        s.Revenue,
                        s.Expenses,
                        s.Biaya,
                        s.ProfitLoss
                     });
            }




            // return 
            return rtn;
        }

        public IEnumerable<SP_ProfitLossPerDayListEntity> DataProfitLossList()
        {
            String query = $@"EXEC SP_ProfitLossPerDayList";
            var res = this._context.SP_ProfitLossPerDayListEntities.FromSqlRaw(query).ToList();
            return res;
        }

        public IEnumerable<SP_ProfitLossPerDayListEntity> GetProfitLossListByMonth(int? month, int? year)
        {
            String query = $@"EXEC SP_ProfitLossPerDayList @month = {month},@year = {year}";
            var res = this._context.SP_ProfitLossPerDayListEntities.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class MasterSubDistrictRepo : Repo<MasterSubDistrictEntity>, IMasterSubDistrictRepo
    {
        public MasterSubDistrictRepo(DbContextPs ctx) : base(ctx)
        {
        }


        public IEnumerable<MasterSubDistrictEntity> ExecWithStoreProcedure(string query, params object[] parameters)
        {
            //return this._contextPs.Query<CompanyEntity>(query, parameters);
            return this._contextPs.Query<MasterSubDistrictEntity>();
        }
    }
}

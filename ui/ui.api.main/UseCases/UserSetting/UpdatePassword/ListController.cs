﻿using core.app.UseCases.UpdatePassword.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.UpdatePassword
{
    [Authorize]
    [Route("user-setting")]
    [ApiController]

    public class RatesController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public RatesController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list-email-password")]
        public async Task<IActionResult> List([FromQuery] ListUpdatePasswordQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

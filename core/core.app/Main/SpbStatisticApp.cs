﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace core.app.Main
{
    public class SpbStatisticApp : ISpbStatisticApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public SpbStatisticApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Delete(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat ReadInputTime(UiUserProfileEntity uiUserProfileEntity, DateTime dateFrom, DateTime dateTo)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<SPSpb_WaktuInputDailyEntity> sPSpb_WaktuInputDailyEntities = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            // logic
            // ----------------------

            // from branch
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId == null)
            {
                sPSpb_WaktuInputDailyEntities = _repoPs.SpbRepo.SPSpb_WaktuInputDailyEntity(dateFrom, dateTo).ToList();
            }

            // from company
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId == null && uiUserProfileEntity.SubBranchId == null)
            {
                sPSpb_WaktuInputDailyEntities = _repoPs.SpbRepo.SPSpb_WaktuInputDailyEntity(dateFrom, dateTo).ToList();
            }



            if (sPSpb_WaktuInputDailyEntities != null) {
                rtn.Data = sPSpb_WaktuInputDailyEntities;
                rtn.Status = StatusCodes.Status200OK;
            }

            //
            // return
            return rtn;
        }

        public ReturnFormat Read(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }
    }
}

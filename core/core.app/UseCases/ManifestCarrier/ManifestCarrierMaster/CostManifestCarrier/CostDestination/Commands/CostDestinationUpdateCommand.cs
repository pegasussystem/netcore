﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostDestination.Commands
{
    public partial class CostDestinationUpdateCommand : IRequest<ReturnFormat>
    {
        public Int32? costDestinationId { get; set; }
        public Int32? costDestinationDetailId { get; set; }
        public String? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostDestinationUpdateCommandHandler : IRequestHandler<CostDestinationUpdateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostDestinationUpdateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostDestinationUpdateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostDestinationUpdate(req.costDestinationId, req.costDestinationDetailId, req.baseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostDestinationUpdateEntity> CostDestinationUpdate(Int32? costDestinationId, Int32? costDestinationDetailId, String? baseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostDestinationUpdate @costDestinationId='{costDestinationId}', @costDestinationDetailId='{costDestinationDetailId}', @baseCost='{baseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostDestinationUpdateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
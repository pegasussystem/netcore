﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_DataUserConfig : IEntityTypeConfiguration<V_DataUserEntity>
    {
        public void Configure(EntityTypeBuilder<V_DataUserEntity> builder)
        {
            builder.HasKey(e => e.UserId);
            builder.ToTable("V_DataUser");
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.SuperVision.Command.FOC
{
    public partial class RejectFOCCommand : IRequest<ReturnFormat>
    {
        public Int32? superVisionId { get; set; }
        public Int64? spbId { get; set; }
        public String? spbNo { get; set; }
        public String? reason { get; set; }
        public String? reasonReject { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class RejectFOCCommandHandler : IRequestHandler<RejectFOCCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public RejectFOCCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(RejectFOCCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                var entity = _context.SuperVisionEntitys.Where(w => w.SuperVisionId.Equals(req.superVisionId)).FirstOrDefault();

                if (entity == null)
                {
                    return rtn;
                }

                entity.ReasonReject = req.reasonReject;
                entity.ApproveBy = req.uiUserProfile.userId;
                entity.ApproveDate = DateTime.UtcNow;
                entity.Status = 2;
                entity.UpdatedBy = req.uiUserProfile.userId.ToString();
                entity.UpdatedAt = DateTime.UtcNow;

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = "Data Success di Reject";

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_CostPenerus_Entity
    {
        // CostDestination
        public Int32? CostPenerusId { get; set; }
        public Int32? StationDestinationId { get; set; }
        public String? CityDestinationId { get; set; }
        public Int32? VendorDestinationId { get; set; }

        // CostDestinationDetail
        public Int32? CostPenerusDetailId { get; set; }
        public String? CostType { get; set; }
        public String? Unit { get; set; }
        public Decimal? BaseCost { get; set; }
        public String? CostGroup { get; set; }
        public String? UseWeight { get; set; }
        public String? Desc { get; set; }
    }
}

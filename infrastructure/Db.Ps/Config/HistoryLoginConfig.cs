﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class HistoryLoginConfig : IEntityTypeConfiguration<HistoryLoginEntity>
    {
        public void Configure(EntityTypeBuilder<HistoryLoginEntity> builder)
        {
            builder.HasKey(e => e.HistoryLoginId);
            builder.ToTable("HistoryLogin");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.Spb.Commands.UpdateDescription;
using core.helper;
using Core.Entity.Base;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ui.api.main.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class SpbController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public SpbController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpGet("number-generator")]
        public async Task<IActionResult> SpbNumberGenerator()
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();


            //
            // get data
            uiUserProfileEntity = GetUserClaim();
            _returnFormat = _appPs.SpbApp.SpbNumberGenerator(uiUserProfileEntity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpPut("update-description")]
        public async Task<ActionResult<ReturnFormat>> Update(UpdateDescriptionCommand command)
        {
            ReturnFormat _returnFormat = new ReturnFormat();
            _returnFormat = await Mediator.Send(command).ConfigureAwait(false);
            return StatusCode(_returnFormat.Status, _returnFormat);
        }





        UiUserProfileEntity GetUserClaim() {
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            // get claim user
            var claims = User.Claims.Where(w => w.Type.Equals("userProfile")).First().Value;
            uiUserProfileEntity = JsonConvert.DeserializeObject<UiUserProfileEntity>(claims);

            return uiUserProfileEntity;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Core.Entity.Main
{
    public class SpbDetailEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int64? SpbDetailId { get; set; }
        public Int64? SpbId { get; set; }
        public Int32? ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public Int32? SavedBy { get; set; }
        public DateTime? SavedDate { get; set; }
        public String? SenderCode { get; set; }
        public String? ReceiverCode { get; set; }
        public String? PorterCode { get; set; }
        public Int32? PorterId { get; set; }
    }
}

﻿using core.app.UseCases.Privillege.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Privilege
{
    [Authorize]
    [Route("privilege-menu")]
    [ApiController]

    public class ListPrivilegeMenuOriController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ListPrivilegeMenuOriController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list-menu")]
        public async Task<IActionResult> List([FromQuery] ListPrivilegeMenuOriQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}
﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Repo.Main
{ 

    public interface IAccountReceivableRepo : IRepo<AccountReceivableEntity>
    {
        // IEnumerable<SP_InsertAREntity> InsertAr(SpbEntity spbEntity,UiUserProfileEntity entity);
        public void InsertAr(SpbEntity spbEntity,UiUserProfileEntity entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using MediatR;
using core.app.Common.Interfaces;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostDestination.Query
{
    public partial class ReadCarrierQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadCarrierQueryHandler : IRequestHandler<ReadCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadCarrierQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                rtn.Status = StatusCodes.Status200OK;
                
                var query = from a in _context.CarrierEntitys
                        select new
                        {
                            a.CarrierId
                            , a.CarrierName
                            , a.CarrierCode
                            , a.CarrierType
                            , CarrierNameCustom = "["+a.CarrierCode+"] - "+ a.CarrierName 
                        };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.CarrierId
                            , s.CarrierName
                            , s.CarrierCode
                            , s.CarrierType
                            , s.CarrierNameCustom
                        }).OrderBy(o => o.CarrierId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
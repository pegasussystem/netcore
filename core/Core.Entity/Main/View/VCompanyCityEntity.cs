﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class VCompanyCityEntity
    {

        public Int32? CompanyCityId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? CityType { get; set; }
        public String? CityId { get; set; }
        public String? CompanyCityCode { get; set; }
        public String? CompanyCityType { get; set; }
        public String? CompanyCityName { get; set; }
        public String? MasterCityCapital { get; set; }
        public String? CompanyCityNameCustom { get; set; }

    }





}

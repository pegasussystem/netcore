﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace core.app.UseCases.Master.MonitoringShipment.Commands
{
    public partial class MonitoringShipmentUpdateCommand : IRequest<ReturnFormat>
    {
        public Int32? monitoringShipmentId { get; set; }
        public String? status { get; set; }
        public String? pod { get; set; }
        public DateTime? podDate { get; set; }
        public String? description { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MonitoringShipmentUpdateCommandHandler : IRequestHandler<MonitoringShipmentUpdateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public MonitoringShipmentUpdateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(MonitoringShipmentUpdateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try 
            {
                var podDt = "";
                if (req.podDate != null)
                {
                    podDt = req.podDate.Value.Year + "-" + req.podDate.Value.Month + "-" + req.podDate.Value.Day;
                }
                else
                {
                    podDt = null;
                }

                // Mark as Changed
                var res = this.MonitoringShipmentUpdate(req.monitoringShipmentId, req.status, req.pod, podDt, req.description, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_MonitoringShipmentUpdateEntity> MonitoringShipmentUpdate(Int32? monitoringShipmentId, String? status, String? pod, String? podDate, String? description, int? userId)
        {
            String query = $@"EXEC SP_MonitoringShipmentUpdate @monitoringShipmentId = '{monitoringShipmentId}', @status = '{status}', @pod = '{pod}', @podDate = '{podDate}', @description = '{description}', @userId= {userId} ";
            var res = this._context.SP_MonitoringShipmentUpdateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

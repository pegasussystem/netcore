﻿using core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrierCost;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.ReadManifestCarrierCost
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ListCostController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public ListCostController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list-cost")]
        public async Task<IActionResult> List([FromQuery] ListCostReadQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}
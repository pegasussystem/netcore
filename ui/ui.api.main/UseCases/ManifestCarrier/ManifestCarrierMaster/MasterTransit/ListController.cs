﻿using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit.Queries;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]

    public class ListController : ApiController
    {
        [HttpGet("master-transit-read-transit")]
        public async Task<IActionResult> List([FromQuery] ListTransitReadQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();


            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("master-transit-read-origin-station")]
        public async Task<IActionResult> MasterTransitReadOriginStation([FromQuery] MasterTransitReadOriginStationQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("master-transit-read-carrier")]
        public async Task<IActionResult> MasterTransitReadCarrier([FromQuery] MasterTransitReadCarrierQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("master-transit-read-vendor")]
        public async Task<IActionResult> MasterTransitReadVendor([FromQuery] MasterTransitReadVendorQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("master-transit-read-vendor-carrier")]
        public async Task<IActionResult> MasterTransitReadVendorCarrier([FromQuery] MasterTransitReadVendorCarrierQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("master-transit-read-first-flight-code")]
        public async Task<IActionResult> MasterTransitReadFirstCarrier([FromQuery] MasterTransitReadCarrierQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpGet("master-transit-read-last-flight-code")]
        public async Task<IActionResult> MasterTransitReadLastCarrier([FromQuery] MasterTransitReadCarrierQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("master-transit-read-first-flight")]
        public async Task<IActionResult> MasterTransitReadFirstFlight([FromQuery] MasterTransitReadFirstFlightQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("master-transit-read-city-transit")]
        public async Task<IActionResult> MasterTransitReadCityTransit([FromQuery] MasterTransitReadCityTransitQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("master-transit-read-last-flight")]
        public async Task<IActionResult> MasterTransitReadLastFlight([FromQuery] MasterTransitReadLastFlightQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("master-transit-read-destination-station")]
        public async Task<IActionResult> MasterTransitReadDestinationStation([FromQuery] MasterTransitReadDestinationStationQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

using System.Threading.Tasks;
using core.app.UseCases.BukuKas.Commands;
using core.app.UseCases.BukuKas.Queries;
using core.app.UseCases.GeneralLedger.Queries;
using core.app.UseCases.ProftiLossStatement.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Finance
{
    [Authorize]
    [Route("profit-loss")]
    [ApiController]

    public class ProfitLossStatementController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ProfitLossStatementController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list-per-day")]
        public async Task<IActionResult> List([FromQuery] ProfitLossPerDayListQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);
            

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

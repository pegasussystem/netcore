﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostTransit.Commands
{
    public partial class CostTransitUpdateCommand : IRequest<ReturnFormat>
    {
        public Int32? CostTransitDetailId        { get; set; }
        public Int32? CostTransitId              { get; set; }
        public Int32? BaseCost                  { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostTransitUpdateCommandHandler : IRequestHandler<CostTransitUpdateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostTransitUpdateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostTransitUpdateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostTransitUpdate(req.CostTransitDetailId, req.CostTransitId, req.BaseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostTransitUpdateEntity> CostTransitUpdate(Int32? CostTransitDetailId, Int32? CostTransitId, Int32? BaseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostTransitUpdate @CostTransitDetailId= {CostTransitDetailId}, @CostTransitId ={CostTransitId}, @BaseCost='{BaseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostTransitUpdateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Station.StationCity.Query
{
    public partial class ManifestCarrierMasterStationCityListQuery : IRequest<ReturnFormat>
    {   
        public Int32? isTransit { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterStationCityListQueryHandler : IRequestHandler<ManifestCarrierMasterStationCityListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ManifestCarrierMasterStationCityListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterStationCityListQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;
                
                if(req.isTransit == 0){
                    var query = from a in _context.StationEntitys
                            join b in _context.StationCityEntitys on a.StationId equals b.StationId
                            join c in _context.MasterCityEntitys on b.CityId equals c.MasterCityId
                            select new
                            {
                                b.StationCityId
                                , a.StationName
                                , b.CityId
                                , CityName = "[ "+ c.MasterCityCode + " ] - "+ c.MasterCityName
                                , Status = b.Priority == "1" ? "Utama" : "-" 
                            };

                             if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.StationCityId
                            , s.StationName
                            , s.CityId
                            , s.CityName
                            , s.Status
                        }).OrderByDescending(o => o.StationCityId);
                }
                }else{
                     var query = from a in _context.StationEntitys
                            join b in _context.StationCityEntitys on a.StationId equals b.StationId
                            join c in _context.MasterCityEntitys on b.CityId equals c.MasterCityId
                            where b.Priority.Equals("1")
                            select new
                            {
                                b.StationId
                                , StationName = "["+ c.MasterCityCode +"] - " + a.StationName

                            };

                             if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.StationId
                            , s.StationName
                        }).OrderByDescending(o => o.StationId);
                }
                }

                

               

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
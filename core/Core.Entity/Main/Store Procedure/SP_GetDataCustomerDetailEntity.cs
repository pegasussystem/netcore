﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_GetDataCustomerDetailEntity
    {
        public Int64? SpbId { get; set; }
        public String? SpbNo { get; set; }
        public DateTime? CreatedAt { get; set; }
        public String? Telepon { get; set; }
        public String? Phone { get; set; }
        public String? CustomerName { get; set; }
        public String? CustomerStore { get; set; }
        public String? CustomerPlace { get; set; }
        public String? CustomerAddress { get; set; }
        public Int32? Rt { get; set; }
        public Int32? Rw { get; set; }
        public String? MasterUrbanName { get; set; }
        public String? MasterSubDistrictName { get; set; }
        public String? MasterCityName { get; set; }
        public String? MasterUrbanPostalCode { get; set; }
        public String? ViaName { get; set; }
        public Decimal? TotalCAW { get; set; }
        public Int32? TotalKoli { get; set; }
        public Decimal? TotalPrice { get; set; }
        
    }
}

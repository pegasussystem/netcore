﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Base;
//using Core.Validator.Base;
using FluentValidation;
using FluentValidation.Results;
using Interface.App.Base;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace core.app.Base
{
    public class LookupApp : ILookupApp
    {
        // TODO: standarisai API
        // TODO: gunakan try catch
        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public LookupApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(LookupEntity entity)
        {
            throw new NotImplementedException();
        }

        public LookupEntity Delete(LookupEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();


            //
            // get data
            rtn.Data = _repoPs.LookupRepo.Read().ToList();
            rtn.Status = StatusCodes.Status200OK;

            //  
            // menu tree
            rtn.Data = (rtn.Data as List<LookupEntity>).OrderBy(o => o.Order).ToList();

            //
            // return
            return rtn;
        }

        public ReturnFormat Read(LookupEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();


            //
            // get data
            rtn.Data = _repoPs.LookupRepo.Read(w => w.LookupKeyParent.Equals(entity.LookupKeyParent)).ToList();
            rtn.Status = StatusCodes.Status200OK;

            //  
            // menu tree
            rtn.Data = (rtn.Data as List<LookupEntity>).OrderBy(o => o.Order).ToList();

            //
            // return
            return rtn;
        }

        public LookupEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public LookupEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(LookupEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

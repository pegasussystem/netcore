﻿using Core.Entity.Base;
using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class UserDetail : IEntityTypeConfiguration<UserDetailEntity>
    {
        public void Configure(EntityTypeBuilder<UserDetailEntity> builder)
        {
            builder.HasKey(e => e.UserId);
            builder.ToTable("UserDetail");
        }
    }
}

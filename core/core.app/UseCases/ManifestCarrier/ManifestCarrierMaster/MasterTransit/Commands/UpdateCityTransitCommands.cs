﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Master.MasterTransit.Commands
{
    public partial class UpdateCityTransitCommands : IRequest<ReturnFormat>
    {
        public Int32? transitRouteId { get; set; }
        public Int32? originStationId { get; set; }
        public Int32? firstFlightCodeId { get; set; }
        public Int32? firstFlightId { get; set; }
        public Int32? stationTransitId { get; set; }
        public Int32? lastFlightCodeId { get; set; }
        public Int32? lastFlightId { get; set; }
        public Int32? finalDestinatioStationId { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class UpdateCityTransitCommandsHandler : IRequestHandler<UpdateCityTransitCommands, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdateCityTransitCommandsHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdateCityTransitCommands req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            //check exists 
            //var chk = this.CheckExistData(req);

            //if (chk > 0) {
            //    rtn.Status = StatusCodes.Status201Created;
            //    rtn.Data = null;

            //    return rtn;
            //}

            try
            {
                // Mark as Changed
                var dataItem = _context.FlightTransitRouteEntitys.Where(w => w.TransitRouteId.Equals(req.transitRouteId)).FirstOrDefault();

                if (dataItem == null)
                {
                    return rtn;
                }

                dataItem.StationTransitId = req.stationTransitId;
                dataItem.UpdatedBy = req.uiUserProfile.userId.ToString();
                dataItem.UpdatedAt = DateTime.UtcNow;

                _context.FlightTransitRouteEntitys.Update(dataItem);
                _context.SaveChanges();


                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = dataItem;

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        //public Int32? CheckExistData(UpdateCityTransitCommands req) {

        //    var query = from ft in _context.FlightTransitRouteEntitys
        //                where ft.TransitRouteId.Equals(Int32.Parse(req.transitRouteId))
        //                && ft.OriginCityId.Equals(req.originCityId)
        //                && ft.FirstFlightCodeId.Equals(Int32.Parse(req.firstFlightCodeId))
        //                && ft.FirstFlightId.Equals(Int32.Parse(req.firstFlightId))
        //                && ft.CityTransitId.Equals(req.cityTransitId)
        //                && ft.LastFlightCodeId.Equals(Int32.Parse(req.lastFlightCodeId))
        //                && ft.LastFlightId.Equals(Int32.Parse(req.lastFlightId))
        //                && ft.FinalDestinationCityId.Equals(req.finalDestinationCityId)
        //                select ft;

        //    return query.Count();
        //}
       
    }
}

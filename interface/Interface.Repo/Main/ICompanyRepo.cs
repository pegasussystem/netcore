﻿using Core.Entity.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface ICompanyRepo : IRepo<CompanyEntity>
    {
    }
}

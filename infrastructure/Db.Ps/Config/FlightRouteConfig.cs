﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class FlightRouteConfig : IEntityTypeConfiguration<FlightRouteEntity>
    {
        public void Configure(EntityTypeBuilder<FlightRouteEntity> builder)
        {
            builder.HasKey(e => e.FlightRouteId);
            builder.ToTable("FlightRoute");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_PrivilegeMenuActionListEntity
    {
        public Int32? MenuActionId { get; set; }
        public String? MenuActionKey { get; set; }
        public String? MenuActionName { get; set; }
        public Int32? IsActive { get; set; }

    }
}

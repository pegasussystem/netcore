﻿using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main.SpbHourly
{
    public interface IListSpbHourlyUseCase : IApp<SPSpbHourlyEntity>
    {
        public ReturnFormat List(UiUserProfileEntity uiUserProfileEntity, Object obj);
    }
}

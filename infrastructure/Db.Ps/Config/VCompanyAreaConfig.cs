﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VCompanyAreaConfig : IEntityTypeConfiguration<VCompanyAreaEntity>
    {
        public void Configure(EntityTypeBuilder<VCompanyAreaEntity> builder)
        {
            builder.HasKey(e => e.CompanyAreaId);
            builder.ToTable("VCompanyArea");
        }
    }
}

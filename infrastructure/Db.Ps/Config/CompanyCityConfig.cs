﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class CompanyCityConfig : IEntityTypeConfiguration<CompanyCityEntity>
    {
        public void Configure(EntityTypeBuilder<CompanyCityEntity> builder)
        {
            builder.HasKey(e => e.CompanyCityId);
            builder.ToTable("CompanyCity");

        }
    }
}

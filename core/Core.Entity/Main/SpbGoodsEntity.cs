﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Core.Entity.Main
{
    public class SpbGoodsEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int64 SpbGoodsId { get; set; }
        public Int64 SpbId { get; set; }
        public Int32 TotalKoli { get; set; }
        public Decimal Aw { get; set; }
        public Decimal Vw { get; set; }
        public Decimal Caw { get; set; }
        public Decimal Length { get; set; }
        public Decimal Width { get; set; }
        public Decimal Height { get; set; }

        public Int32? Min { get; set; }
        public Decimal? MinRates { get; set; }
        public Decimal? CawFinal { get; set; }
    }
}

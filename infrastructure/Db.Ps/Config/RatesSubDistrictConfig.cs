﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class RatesSubDistrictConfig : IEntityTypeConfiguration<RatesSubDistrictEntity>
    {
        public void Configure(EntityTypeBuilder<RatesSubDistrictEntity> builder)
        {
            builder.HasKey(e => e.RatesSubDistrictId);
            builder.ToTable("RatesSubDistrict");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;


namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorTransit.Command
{
    public partial class ManifestCarrierMasterVendorTransitDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? VendorTransitId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterVendorTransitDeleteCommandHandler : IRequestHandler<ManifestCarrierMasterVendorTransitDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterVendorTransitDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterVendorTransitDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.VendorTransitDelete(req.VendorTransitId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_VendorTransitDeleteEntity> VendorTransitDelete(Int32? VendorTransitId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_VendorTransitDelete @VendorTransitId='{VendorTransitId}'";
            var res = this._context.SP_ManifestCarrierMaster_VendorTransitDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_MonitoringHistory_HistoryNotPrintEntity
    {
        public DateTime? CreatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public Int64? SpbId { get; set; }
        public String? SpbNo { get; set; }
        public String? Username { get; set; }
        public String? TypesOfGoods { get; set; }
        public String? Carrier { get; set; }
        public String? QualityOfService { get; set; }
        public String? TypeOfService { get; set; }
        public String? PaymentMethod { get; set; }
    }
}

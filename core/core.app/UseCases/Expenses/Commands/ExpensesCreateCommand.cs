using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Expenses.Commands
    {
        public partial class ExpensesCreateCommand : IRequest<ReturnFormat>
        {
        public Int64? coaId { get; set; }
        public String? keterangan { get; set; }
        public Decimal? debit { get; set; }
        public Decimal? credit { get; set; }
        
        public UiUserProfileEntity uiUserProfile { get; set; }
        }

        public class ExpensesCreateCommandHandler : IRequestHandler<ExpensesCreateCommand, ReturnFormat>
        {
            private readonly IApplicationDbContext _context;

            public ExpensesCreateCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<ReturnFormat> Handle(ExpensesCreateCommand req, CancellationToken cancellationToken)
            {
                //
                // variable
                ReturnFormat rtn = new ReturnFormat();
                //
                // set variable

                // default status 
                rtn.Status = StatusCodes.Status204NoContent;

                try
                {
                    // Mark as Changed
                    var res = this.ExpensesCreate(req.coaId,req.keterangan,req.debit,req.credit);

                    await _context.SaveChangesAsync(cancellationToken);

                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = res;

                    //
                    // return
                    return rtn;

                }
                catch (Exception ex)
                {
                    var err = ex.Message;
                    rtn.Status = StatusCodes.Status204NoContent;
                    rtn.Data = null;
                }
                return rtn;
            }

            public IEnumerable<SP_ExpensesCreateEntity> ExpensesCreate(Int64? coaId, String? keterangan, Decimal? debit , Decimal? credit)
            {
                String query = $@"EXEC SP_ExpensesCreate  @coaId={coaId}, @keterangan = '{keterangan}', @debit={debit}, @credit={credit}";
            var res = this._context.SP_ExpensesCreateEntities.FromSqlRaw(query).ToList();
                return res;
            }
        }
    }

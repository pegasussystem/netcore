﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Monitoring.ManifestList.Query
{
    public partial class ListManifestQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public String? Type { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class ListManifestQueryHandler : IRequestHandler<ListManifestQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListManifestQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListManifestQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_ManifestListEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataManifestList(req.CreatedAt, req.Type, req.uiUserProfile.userId);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        s.CreatedAt
                        , CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("dddd, dd MMMM yyyy")
                        , s.CompanyId
                        , s.BranchId
                        , s.SubBranchId
                        , s.ManifestId
                        , s.ManifestNo
                        , s.Alphabet
                        , s.Carrier
                        , s.OriginCityId
                        , s.OriginCityCode
                        , s.OriginCityName
                        , s.DestinationCityId
                        , s.DestinationCityCode
                        , s.DestinationnCityName
                        , s.ProvinceId
                        , s.ProvinceCode
                        , s.ProvinceName
                        , s.IsOpen
                    }).OrderByDescending(o => o.CreatedAt);
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_ManifestListEntity> DataManifestList(DateTime? CreatedAt, String? Type, Int32? UserId)
        {
            String query = $@"EXEC SP_ManifestList @day = '{CreatedAt.Value.Day}', @month = '{CreatedAt.Value.Month}', @year = '{CreatedAt.Value.Year}', @userId='{UserId}', @type = '{Type}' ";
            var res = this._context.SP_ManifestListEntitys.FromSqlRaw(query).ToList();
            return res;
        }


    }

}

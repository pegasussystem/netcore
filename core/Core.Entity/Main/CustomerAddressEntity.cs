﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class CustomerAddressEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? CustomerId { get; set; }

        public Int32? CustomerAddressId { get; set; }
        public String? CustomerAddress { get; set; }
        public Int32? Rt { get; set; }
        public Int32? Rw { get; set; }

        public String? Urban { get; set; }
        public String? SubDistrict { get; set; }
        public String? City { get; set; }
        public String? PostalCode { get; set; }
        public String? Via { get; set; }

    }
}

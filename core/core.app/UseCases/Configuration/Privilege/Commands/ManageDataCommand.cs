﻿using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.app.Common.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Core.Entity.Main.Store_Procedure;

namespace core.app.UseCases.Configuration.Privilege.Commands
{
    public partial class ManageDataCommand : IRequest<ReturnFormat>
    {
        public Int32? privilegeId { get; set; }
        public String? action { get; set; }
        public Int32? id { get; set; }
        public String? location { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManageDataCommandHandler : IRequestHandler<ManageDataCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManageDataCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManageDataCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.ManageData(req.privilegeId, req.action, req.id, req.location, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_PrivilegeManageDataEntity> ManageData(Int32? privilegeId, String? action, Int32? id, String? location, Int32? userid)
        {
            String query = $@"EXEC SP_PrivilegeManageData @privilegeId={privilegeId}, @action='{action}', @id={id}, @location='{location}', @userId={userid} ";
            var res = this._context.SP_PrivilegeManageDataEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }

}

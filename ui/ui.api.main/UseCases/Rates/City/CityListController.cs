﻿using core.app.UseCases.Rates.City;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Rates.City
{
    [Authorize]
    [Route("rates")]
    [ApiController]

    public class CityListController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public CityListController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("readCity")]
        public async Task<IActionResult> Read([FromQuery] CityListQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

﻿using core.app.UseCases.Base.Query;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Privilege
{
    [Authorize]
    [Route("distribusi-print")]
    [ApiController]
    public class DistribusiPrintListController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public DistribusiPrintListController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("list")]
        public async Task<IActionResult> List([FromBody] DistribusiPrintListQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

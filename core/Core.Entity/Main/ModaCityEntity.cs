﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Core.Entity.Main
{
    public class ModaCityEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? ModaCityId { get; set; }
        public String? OriginCityId { get; set; }
        public String? DestinationCityId { get; set; }
        public Int32? LookupId { get; set; }
    }
}

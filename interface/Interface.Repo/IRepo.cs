﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Interface.Repo
{
    public interface IRepo<T>
    {
        //Task<IQueryable<T>> FindByCondition(Expression<Func<T, bool>> expression);
        T Create(T entity);
        IQueryable<T> Read();
        IQueryable<T> Read(Expression<Func<T, bool>> expression);
        void Update(T entity);
        T Delete(T entity);
        T Destroy(T entity);
        void Save();
        IQueryable<T> Raw(string query);
        IQueryable<T> RawExt<EXT>(string query);



    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class VendorOriginEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? VendorOriginId { get; set; }
        public Int32? VendorId { get; set; }
        public Int32? StationId { get; set; }
    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class MatrixLastCarrierConfig : IEntityTypeConfiguration<MatrixLastCarrierEntity>
    {
        public void Configure(EntityTypeBuilder<MatrixLastCarrierEntity> builder)
        {
            builder.HasKey(e => e.MatrixLastCarrierId);
            builder.ToTable("MatrixLastCarrier");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

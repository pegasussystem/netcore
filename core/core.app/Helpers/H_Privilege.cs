﻿using Core.Entity.Ui;
using System;
using System.Collections.Generic;
using System.Text;

namespace core.app.Helpers
{
    public static class H_Privilege
    {
        public static bool Read(UiUserProfileEntity user, PrivilegeData privilegeData) {

            // check if gerai / sub branch
            // --- --- ---
            if (!String.IsNullOrWhiteSpace(user.SubBranchId.ToString())
               && !String.IsNullOrWhiteSpace(user.BranchId.ToString())
               && !String.IsNullOrWhiteSpace(user.companyId.ToString())
               )
            {
                return privilegeData.SubBranch_Read;
            }

            // check if branch
            // --- --- ---
            if (String.IsNullOrWhiteSpace(user.SubBranchId.ToString())
               && !String.IsNullOrWhiteSpace(user.BranchId.ToString())
               && !String.IsNullOrWhiteSpace(user.companyId.ToString())
               )
            {
                return privilegeData.Branch_Read;
            }

            // check if HO
            // --- --- ---
            if (String.IsNullOrWhiteSpace(user.SubBranchId.ToString())
                && String.IsNullOrWhiteSpace(user.BranchId.ToString())
                && !String.IsNullOrWhiteSpace(user.companyId.ToString())
                )
            {
                return privilegeData.Ho_Read;
            }

            return false;
        }

        public static bool Create(UiUserProfileEntity user, PrivilegeData privilegeData)
        {

            // check if gerai / sub branch
            // --- --- ---
            if (!String.IsNullOrWhiteSpace(user.SubBranchId.ToString()))
            {
                return privilegeData.SubBranch_Create;
            }

            // check if branch
            // --- --- ---
            if (String.IsNullOrWhiteSpace(user.SubBranchId.ToString())
                && !String.IsNullOrWhiteSpace(user.BranchId.ToString())
                )
            {
                return privilegeData.Branch_Create;
            }

            // check if HO
            if (String.IsNullOrWhiteSpace(user.SubBranchId.ToString())
                && String.IsNullOrWhiteSpace(user.BranchId.ToString())
                && !String.IsNullOrWhiteSpace(user.companyId.ToString())
                )
            {
                return privilegeData.Ho_Create;
            }

            return false;
        }

        public static bool Delete(UiUserProfileEntity user, PrivilegeData privilegeData)
        {

            // check if gerai / sub branch
            // --- --- ---
            if (!String.IsNullOrWhiteSpace(user.SubBranchId.ToString()))
            {
                return privilegeData.SubBranch_Delete;
            }

            // check if branch
            // --- --- ---
            if (String.IsNullOrWhiteSpace(user.SubBranchId.ToString())
                && !String.IsNullOrWhiteSpace(user.BranchId.ToString())
                )
            {
                return privilegeData.Branch_Delete;
            }

            // check if HO
            if (String.IsNullOrWhiteSpace(user.SubBranchId.ToString())
                && String.IsNullOrWhiteSpace(user.BranchId.ToString())
                && !String.IsNullOrWhiteSpace(user.companyId.ToString())
                )
            {
                return privilegeData.Ho_Delete;
            }

            return false;
        }
    }



    public class PrivilegeData {

        // read
        public Boolean SubBranch_Read { get; set; } = false;
        public Boolean Branch_Read { get; set; } = false;
        public Boolean Ho_Read { get; set; } = false;

        // create
        public Boolean SubBranch_Create { get; set; } = false;
        public Boolean Branch_Create { get; set; } = false;
        public Boolean Ho_Create { get; set; } = false;

        // update

        // delete
        public Boolean SubBranch_Delete { get; set; } = false;
        public Boolean Branch_Delete { get; set; } = false;
        public Boolean Ho_Delete { get; set; } = false;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Core.Entity.Main
{
    public class ManifestKoliEntity
    {

        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedBy { get; set; }
        public DateTime? UpdatedBy { get; set; }

        public Int64 ManifestKoliId { get; set; }
        public Guid? InsertId { get; set; }
        public Int64? ManifestId { get; set; }
        public Int32? KoliNo { get; set; }
        public String? KoliType { get; set; }
        public byte? IsClose { get; set; }
    }
}

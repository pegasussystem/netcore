using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CoaCityRevenueLiabilityConfig : IEntityTypeConfiguration<CoaCityRevenueLiabilityEntity>
    {
        public void Configure(EntityTypeBuilder<CoaCityRevenueLiabilityEntity> builder)
        {
            builder.HasKey(e => e.Id);
            builder.ToTable("CoaCityRevenueLiability");
        }
    }
}

﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class ViaConfig : IEntityTypeConfiguration<ViaEntity>
    {
        public void Configure(EntityTypeBuilder<ViaEntity> builder)
        {
            builder.HasKey(e => e.ViaId);
            builder.ToTable("Via");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VCarrierEntity
    {

        public Int32? MasterCarrierId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? CarrierName { get; set; }

    }


}

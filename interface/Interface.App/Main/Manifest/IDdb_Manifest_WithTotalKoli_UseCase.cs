﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main.Manifest
{
    public interface IDdb_Manifest_WithTotalKoli_UseCase : IApp<ManifestEntity>
    {
        ReturnFormat Read(UiUserProfileEntity uiUserProfileEntity, Object input);
    }
}

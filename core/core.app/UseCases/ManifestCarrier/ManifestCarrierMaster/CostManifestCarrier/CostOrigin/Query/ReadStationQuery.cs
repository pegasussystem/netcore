﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using core.helper;
using Interface.Repo;
using Interface.Other;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostOrigin.Query
{
    public partial class ReadStationQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadStationQueryHandler : IRequestHandler<ReadStationQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadStationQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadStationQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;
                var qr = from a in _context.StationEntitys
                            join b in _context.VendorOriginEntitys on a.StationId equals b.StationId
                        select new
                        {
                            a.StationId
                            , a.StationName
                        };

                if (qr != null)
                {
                    var query = qr.Distinct();
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.StationId
                            , s.StationName
                        }).OrderBy(o => o.StationId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
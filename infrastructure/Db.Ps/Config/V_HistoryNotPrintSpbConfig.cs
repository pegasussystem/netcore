﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class V_HistoryNotPrintSpbConfig : IEntityTypeConfiguration<V_HistoryNotPrintSpbEntity>
    {
        public void Configure(EntityTypeBuilder<V_HistoryNotPrintSpbEntity> builder)
        {
            builder.HasKey(e => e.SpbId);
            builder.ToTable("V_HistoryNotPrintSpb");
        }
    }
}

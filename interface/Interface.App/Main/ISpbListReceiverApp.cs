﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main
{

    public interface ISpbListReceiverApp : IApp<SpbEntity>
    {


        ReturnFormat Read(UiUserProfileEntity uiUserProfileEntity, Object obj);

        ReturnFormat BulkPrint(UiUserProfileEntity uiUserProfileEntity, SpbEntity spbEntity);

        ReturnFormat ReadSpbPrint(UiUserProfileEntity uiUserProfile, VSpbEntity entity);
    }
}

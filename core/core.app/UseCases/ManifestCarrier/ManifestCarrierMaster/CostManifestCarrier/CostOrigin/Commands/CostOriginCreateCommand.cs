﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;


namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostOrigin.Commands
{
    public partial class CostOriginCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? stationOriginId { get; set; }
        public Int32? carrierId { get; set; }
        public Int32? vendorOriginId { get; set; }
        public Int32? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostOriginCreateCommandHandler : IRequestHandler<CostOriginCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostOriginCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostOriginCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostOriginCreate(req.stationOriginId, req.carrierId, req.vendorOriginId, req.baseCost);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostOriginCreateEntity> CostOriginCreate(Int32? stationOriginId, Int32? carrierId, Int32? vendorOriginId, Int32? baseCost)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostOriginCreate @stationOriginId='{stationOriginId}', @carrierId='{carrierId}', @vendorOriginId='{vendorOriginId}', @baseCost='{baseCost}'";
            var res = this._context.SP_ManifestCarrierCost_CostOriginCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

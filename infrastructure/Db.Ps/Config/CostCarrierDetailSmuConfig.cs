﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostCarrierDetailSmuConfig : IEntityTypeConfiguration<CostCarrierDetailSmuEntity>
    {
        public void Configure(EntityTypeBuilder<CostCarrierDetailSmuEntity> builder)
        {
            builder.HasKey(e => e.CostCarrierDetailSmuId);
            builder.ToTable("CostCarrierDetailSmu");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
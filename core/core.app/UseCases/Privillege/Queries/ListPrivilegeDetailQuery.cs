﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Queries
{
    public partial class ListPrivilegeDetailQuery : IRequest<ReturnFormat>
    {
        public Int32? Pid { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListPrivilegeDetailQueryHandler : IRequestHandler<ListPrivilegeDetailQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPrivilegeDetailQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPrivilegeDetailQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query2 = from b in _context.PrivilegeActionEntitys 
                         join c in _context.MenuActionEntitys on b.MenuActionId equals c.MenuActionId
                         join d in _context.MenuDataEntitys on c.MenuId equals d.MenuId
                         where b.PrivilegeId == req.Pid
                         select new { 
                             b.PrivilegeId, 
                             b.PrivilegeActionId, 
                             c.MenuActionId, 
                             d.MenuKey,
                             d.MenuName, 
                             c.MenuAction,
                             //Items = string.Join(",", b.Value),
                             b.Value,
                             b.Desc
                         };

            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Distinct()
                    .Select(s => new
                    {
                        s.PrivilegeId,
                        s.PrivilegeActionId,
                        s.MenuActionId,
                        s.MenuKey,
                        s.MenuName,
                        s.MenuAction,
                        s.Value,
                        s.Desc
                    }).OrderByDescending(o => o.MenuActionId);
            }

            // return
            return rtn;
        }
    }
}
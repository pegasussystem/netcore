﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class MatrixFirstCarrierConfig : IEntityTypeConfiguration<MatrixFirstCarrierEntity>
    {
        public void Configure(EntityTypeBuilder<MatrixFirstCarrierEntity> builder)
        {
            builder.HasKey(e => e.MatrixFirstCarrierId);
            builder.ToTable("MatrixFirstCarrier");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

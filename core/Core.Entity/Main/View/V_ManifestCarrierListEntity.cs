﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_ManifestCarrierListEntity
    {
        public DateTime? CreatedAt { get; set; }
        public Int32? CreatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public Int32? UpdatedBy { get; set; }
        public Int32? ManifestCarrierId { get; set; }
        public String? ManifestCarrierNo { get; set; }
        public String? NoSmu { get; set; }
        public String? FullName { get; set; }
        public Decimal? TotalFinalWeightSMU { get; set; }
        public DateTime? FlightDate { get; set; }
        public String? FirstFlight { get; set; }
        public String? LastFlight { get; set; }
        public Int32? Revise { get; set; }
        public String? RevisiStatus { get; set; }
        public Int32? VendorCarrierId { get; set; }
    }
}

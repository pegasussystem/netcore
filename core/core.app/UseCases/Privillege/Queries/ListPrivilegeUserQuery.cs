﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Queries
{
    public partial class ListPrivilegeUserQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public Int32? PrivilegeId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListPrivilegeUserQueryHandler : IRequestHandler<ListPrivilegeUserQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPrivilegeUserQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPrivilegeUserQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            if (req.PrivilegeId != null) {
                var query = from a in _context.V_UserPrivilegeEntitys
                            where a.PrivilegeId.Equals(req.PrivilegeId)
                            select new
                            {
                                a.UserId
                                , a.Username
                                , a.Email
                                , a.TableName
                                , a.TableNameId
                                , a.PlaceName
                                , a.PlaceAddress
                                , a.PrivilegeId
                            };


                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.UserId
                            , s.Username
                            , s.Email
                            , s.PlaceName
                            , s.PlaceAddress
                            , s.PrivilegeId
                        }).OrderBy(o => o.Username);
                }
            }
            else
            {
                var query = from a in _context.VUserNotPrivilegeEntitys
                            select new
                            {
                                a.UserId
                                , a.Username
                                , a.Email
                                , a.TableName
                                , a.TableNameId
                                , a.PlaceName
                                , a.PlaceAddress
                                , a.PrivilegeId
                            };


                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.UserId
                            , s.Username
                            , s.Email
                            , s.PlaceName
                            , s.PlaceAddress
                            , s.PrivilegeId
                        }).OrderBy(o => o.Username);
                }
            }

            // return
            return rtn;
        }
    }
}
﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace core.app.Main
{
    public class ManifestReceiverApp : IManifestReceiverApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public ManifestReceiverApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Delete(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }


        ////------------------ Sudah tidak dipake di pindahkan ke Core.App -> UseCase -> Manifest -> Receiver ------------------------//
        //public ReturnFormat List(VManifestEntity entity, UiUserProfileEntity uiUserProfileEntity)
        //{
        //    //
        //    // variable
        //    ReturnFormat rtn = new ReturnFormat();
        //    List<VManifestEntity> vManifestEntities = null;
        //    BranchEntity branch = null;

        //    //
        //    // set variable
        //    rtn.Status = StatusCodes.Status204NoContent;

        //    //
        //    // logic


        //    // 




        //    // from branch
        //    if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId == null)
        //    {
        //        // get branch handled 
        //        branch = _repoPs.BranchRepo.Read(w => w.BranchId.Equals(uiUserProfileEntity.BranchId)).FirstOrDefault();
        //        var branchList = (branch.CityHandled != null) ? branch.CityHandled.Replace("|<", "").Replace(">|", "").Split("><") : null;

        //        vManifestEntities = _repoPs.VManifestRepo.Read(w => branchList.Contains(w.DestinationCityId))
        //        .ToList();
        //    }

        //    // from company
        //    if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId == null && uiUserProfileEntity.SubBranchId == null)
        //    {
        //        vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId))
        //        .ToList();
        //    }


        //    if (vManifestEntities != null) {
        //        rtn.Status = StatusCodes.Status200OK;
        //        rtn.Data = vManifestEntities
        //            .Select(s => new {
        //            s, CreatedAtLocal = s.CreatedAt.Value.AddHours((Double) uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)uiUserProfileEntity.WorkTimeZoneMinute)
        //        });
        //    }

        //    //
        //    // return
        //    return rtn;

        //}

        public ReturnFormat ListDetail(VManifestDetailEntity entity, UiUserProfileEntity uiUserProfileEntity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<SPManifestDetailEntity> sPManifestDetailEntities = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data
            sPManifestDetailEntities = _repoPs.SPManifestDetailRepo.ReadSp(uiUserProfileEntity, entity.ManifestId)
                .OrderBy(o => o.KoliNo).ThenBy(o => o.SpbNo).ToList();

            if (sPManifestDetailEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = sPManifestDetailEntities;
            }

            //
            // return
            return rtn;

        }

        public ReturnFormat ListSpb(VManifestEntity entity, UiUserProfileEntity uiUserProfileEntity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<SpbEntity> SpbEntitys = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data
            SpbEntitys = _repoPs.SpbRepo.Read(w => w.ManifestId.Equals(entity.ManifestId) && w.TotalPrice != null)
                .ToList();

            if (SpbEntitys != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = String.Join(',', SpbEntitys.Select(s => s.SpbNo ).ToList());
            }

            //
            // return
            return rtn;

        }

        public ReturnFormat SpbCustomer(UiUserProfileEntity uiUserProfile, VSpbEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VSpbEntity> vSpbEntities = null; 
            String printAtLocalTime = DateTime.UtcNow.AddHours((double)uiUserProfile.WorkTimeZoneHour).AddMinutes((double)uiUserProfile.WorkTimeZoneMinute).ToString("dd MMM yyyy HH:mm:ss");

            String[] splitSpbNo = entity.SpbNo.Split(",");

            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data
            vSpbEntities = _repoPs.VSpbRepo.Read(w => splitSpbNo.Contains(w.SpbNo)).ToList();
            var vSpbEntitiesForCustomer = vSpbEntities.OrderBy(o => o.SpbId)
                    .Select((s, index) => new
                    {
                        s.Carrier, s.CarrierName, s.CompanyId,
                        s.CreatedAt, s.Description, s.DestinationAreaCode,
                        s.DestinationAreaId, s.DestinationAreaNameCustom,  s.DestinationCityCode, 
                        s.DestinationCityId, s.DestinationCityNameCustom, s.Discount,
                        s.Etc, s.OriginAreaCode, s.OriginAreaId, s.OriginAreaNameCustom,
                        s.OriginCityCode, s.OriginCityId, s.OriginCityNameCustom,
                        s.Packing, s.PaymentMethod, s.Ppn,
                        s.QualityOfService, s.QualityOfServiceName, s.Quarantine,
                        s.Rates, s.ReceiverAddress, s.ReceiverName,
                        s.ReceiverPhone, s.ReceiverPlace, s.ReceiverStore,
                        s.SenderAddress, s.SenderName, s.SenderPhone, s.SenderPlace,
                        s.SenderStore, s.SpbId, s.SpbNo,
                        s.TotalPrice, s.TypeOfService, s.TypeOfServiceName,
                        s.TypesOfGoods, s.TypesOfGoodsName, s.Username,
                        s.ViaName,
                        PrintAtLocalTime = printAtLocalTime,
                        SheetFor = (s.PaymentMethod.ToLower() == "ta") ? "2B - Salinan Penerima" : "1 - Asli Pelanggan",
                        //SheetFor = SetSheetFor(index, s),
                    }).ToList();

            var vSpbEntitiesForBranch = vSpbEntities.OrderBy(o => o.SpbId)
                    .Select((s, index) => new
                    {
                        s.Carrier, s.CarrierName, s.CompanyId,
                        s.CreatedAt, s.Description, s.DestinationAreaCode,
                        s.DestinationAreaId, s.DestinationAreaNameCustom,  s.DestinationCityCode, 
                        s.DestinationCityId, s.DestinationCityNameCustom, s.Discount,
                        s.Etc, s.OriginAreaCode, s.OriginAreaId, s.OriginAreaNameCustom,
                        s.OriginCityCode, s.OriginCityId, s.OriginCityNameCustom,
                        s.Packing, s.PaymentMethod, s.Ppn,
                        s.QualityOfService, s.QualityOfServiceName, s.Quarantine,
                        s.Rates, s.ReceiverAddress, s.ReceiverName,
                        s.ReceiverPhone, s.ReceiverPlace, s.ReceiverStore,
                        s.SenderAddress, s.SenderName, s.SenderPhone, s.SenderPlace,
                        s.SenderStore, s.SpbId, s.SpbNo,
                        s.TotalPrice, s.TypeOfService, s.TypeOfServiceName,
                        s.TypesOfGoods, s.TypesOfGoodsName, s.Username,
                        s.ViaName,
                        PrintAtLocalTime = printAtLocalTime,
                        SheetFor = (s.PaymentMethod.ToLower() == "ta") ? "7 - Cabang" : "7 - Cabang",
                        //SheetFor = SetSheetFor(index, s),
                    }).ToList();

            vSpbEntitiesForCustomer.AddRange(vSpbEntitiesForBranch);

            if (vSpbEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vSpbEntitiesForCustomer.OrderBy(o => o.SpbId);
            }

            //
            // return
            return rtn;

        }

        public ManifestEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

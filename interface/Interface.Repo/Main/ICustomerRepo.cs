﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface ICustomerRepo : IRepo<CustomerEntity>
    {

        public IQueryable<VCustomerV2Entity> Read_VCustomerV2Entity(UiSpbCreateEntity uiSpbCreateEntity, List<string[]> filter, string phone, String SubCityCode);
        IEnumerable<SP_SaveDataCustomerHeaderEntity> SaveDataCustomer(int? senderId, int? senderNameId, int? senderStoreId, int? senderPlaceId, int? senderAddressId, string customerType, int userId);

        public IQueryable<VCustomerIdPelangganEntity> Read_VCustomerIdPelangganEntity(UiSpbCreateEntity uiSpbCreateEntity, List<string[]> filter, string customercode, String SubCityCode);

        public IQueryable<VCustomerV2Entity> Read_VCustomerNameV2Entity(UiSpbCreateEntity uiSpbCreateEntity, List<string[]> filter, string phone, String SubCityCode); // Pati 10jun22
        public IQueryable<VCustomerV2Entity> Read_VCustomerV2Telp(UiSpbCreateEntity uiSpbCreateEntity, List<string[]> filter, string telp, String SubCityCode); // Pati 10jun22
    }
}

﻿using core.app;
using Core.Repo;
using Db.Ps;
using Interface.App;
using Interface.Other;
using Interface.Repo;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Log.Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace ui.api.main
{
    public static class ServiceExt
    {
        public static void ConfigureRepoPs(this IServiceCollection services)
        {
            services.AddScoped<IRepoWarpperPs, RepoWarpperPs>();
        }

        public static void ConfigureAppPs(this IServiceCollection services)
        {
            services.AddScoped<IAppWrapperPs, AppWrapperPs>();
        }

        public static void ConfigureContextPs(this IServiceCollection services, IConfiguration config)
        {

            var connectionString = config["ConnectionString:Ps_SqlServer"];
            services.AddDbContext<DbContextPs>(o => o.UseSqlServer(connectionString));
        }

        public static void ConfigureLogSerilog(this IServiceCollection services, IConfiguration config)
        {
            var path = config["Logging:Serilog:Path"];
            var FileSize = config["Logging:Serilog:FileSize"];
            services.AddSingleton<ILog>(new SerilogManager(path, FileSize));
        }

        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                 //   builder => builder.AllowAnyOrigin()
                 //   .AllowAnyMethod()
                 //   .AllowAnyHeader());
                 policy =>
                 {
                     policy.WithOrigins("*.pegatara.com", "pegatara.com",
                                         "http://localhost:4200", "http://ps.pegatara.com", "http://demo.pegatara.com", "https://ps.pegatara.com", "https://demo.pegatara.com")
                                         .AllowAnyHeader()
                                         .AllowAnyMethod();
//                      policy.AllowAnyOrigin()
//  .AllowAnyHeader().AllowAnyMethod();
                 });
            });
        }

        public static void ConfigureJwt(this IServiceCollection services, IConfiguration config)
        {
            // TODO : HARDCODE
            services.AddAuthentication("OAuth").AddJwtBearer("OAuth", conf =>
            {
                var secretBytes = Encoding.UTF8.GetBytes("not_too_short_secret_otherwise_it_might_error");
                var key = new SymmetricSecurityKey(secretBytes);

                conf.Events = new JwtBearerEvents()
                {
                    OnMessageReceived = context =>
                    {
                        if (context.Request.Query.ContainsKey("access_token"))
                        {
                            context.Token = context.Request.Query["access_token"];
                        }
                        return Task.CompletedTask;
                    }
                };

                conf.TokenValidationParameters = new TokenValidationParameters()
                {
                    ClockSkew = TimeSpan.Zero,
                    ValidIssuer = "https://localhost:44382/",
                    ValidAudience = "https://localhost:44382/",
                    IssuerSigningKey = key,
                };


            });
        }
    }
}

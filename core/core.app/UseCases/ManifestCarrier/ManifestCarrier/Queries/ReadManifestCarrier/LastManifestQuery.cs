﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class LastManifestQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class LastManifestQueryHandler : IRequestHandler<LastManifestQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public LastManifestQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(LastManifestQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_ManifestCarrier_LastManifestEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---

                DateTime ManifestDate   = DateTime.Now;

            entities = this.ManifestCarrier_LastManifest(ManifestDate.Year, ManifestDate.Month, ManifestDate.Day).Where(x => x.IsVoid.Equals(0));

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                         s.CreatedAt
                        , s.ManifestId
                        , s.ManifestNo
                        , s.SumAw
                        , s.SumCaw
                        , s.CountKoli
                        , s.OriginCityId
                        , s.OriginCityCode
                        , s.OriginCityName
                        , s.OriginStationId
                        , s.DestinationCityId
                        , s.DestinationCityCode
                        , s.DestinationCityName
                        , s.SisaCaw
                        , s.SisaKoli
                        , s.SisaCawFinal
                        , s.SisaKoliFinal
                        , s.columnColour
                        , CreatedAtLocal = s.CreatedAt.Value.AddHours((double)req.uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((double)req.uiUserProfileEntity.WorkTimeZoneMinute)
                    }).OrderByDescending(w => w.CreatedAt);
            }

            // return
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrier_LastManifestEntity> ManifestCarrier_LastManifest(Int32? year, Int32? month, Int32? day)
        {
            String query = $@"EXEC SP_ManifestCarrier_LastManifest @year = {year}, @month = {month}, @day = {day}";
            var res = this._context.SP_ManifestCarrier_LastManifestEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }
}
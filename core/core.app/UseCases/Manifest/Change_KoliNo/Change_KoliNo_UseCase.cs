﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Main.Manifest;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace core.app.UseCases.Manifest.Change_KoliNo
{
    class Change_KoliNo_UseCase : I_Change_KoliNo_UseCase
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public Change_KoliNo_UseCase(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Update(UiUserProfileEntity uiUserProfileEntity, Object req)
        {
            // variable
            // ///// ///// /////
            Change_KoliNo_Input input = req as Change_KoliNo_Input;

            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status400BadRequest
            };

            // logic
            // ///// ///// /////

            // get  
            ManifestKoliSpbEntity entity = _repoPs.ManifestKoliSpb_Repo.Read(w => w.ManifestKoliSpbId.Equals(input.ManifestKoliSpbId)).FirstOrDefault();

            // update
            if (entity != null) {
                entity.UpdatedBy = entity.UpdatedBy + " > " + entity.ManifestKoliId.ToString();
                entity.ManifestKoliId = input.NextManifestKoliId;
                _repoPs.ManifestKoliSpb_Repo.Update(entity);
                _repoPs.ManifestKoliSpb_Repo.Save();
            }
           


            // return
            // ///// ///// /////
            if (entity != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;
            }
            return rtn;
        }

        public ReturnFormat Create(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Delete(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

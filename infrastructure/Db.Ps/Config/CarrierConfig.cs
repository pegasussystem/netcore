﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class CarrierConfig : IEntityTypeConfiguration<CarrierEntity>
    {
        public void Configure(EntityTypeBuilder<CarrierEntity> builder)
        {
            builder.HasKey(e => e.CarrierId);
            builder.ToTable("Carrier");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

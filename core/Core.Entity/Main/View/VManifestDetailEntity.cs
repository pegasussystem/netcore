﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VManifestDetailEntity
    {

        public Int32? CompanyId { get; set; }
        public long? ManifestId { get; set; }
        public Int32? KoliNo { get; set; }
        public Decimal? aw { get; set; }
        public Decimal? Caw { get; set; }
        public Decimal? Cons { get; set; }
        public String? SpbNo { get; set; }
        public String? DestinationAreaCode { get; set; }
        public String? PaymentMethod { get; set; }
        public String? SenderName { get; set; }
        public String? ReceiverName { get; set; }
        public Decimal? TunaiAsal { get; set; } 
        public Decimal? TunaiTujuan { get; set; }
        //public String? CawBDJ { get; set; }
        //public String? CawBJU { get; set; }
        //public String? CawMTP { get; set; }
        //public String? CawSRI { get; set; }
        //public Decimal? CawBNT { get; set; }
        //public Decimal? CawSGT { get; set; }

    }





}

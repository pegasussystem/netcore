﻿using Core.Repo.Base;
using Core.Repo.Main;
using Db.Ps;
using Interface.Repo;
using Interface.Repo.Base;
using Interface.Repo.Main;
using System;

namespace Core.Repo
{
    public class RepoWarpperPs : IRepoWarpperPs
    {
        private DbContextPs _dbContextPs;
        public RepoWarpperPs(DbContextPs ctx)
        {
            _dbContextPs = ctx;
        }

        //=====================================================================================================
        // BASE
        //private IMenuRepo _menuRepo;
        //public IMenuRepo MenuRepo
        //{
        //    get
        //    {
        //        if (_menuRepo == null) { _menuRepo = new MenuRepo(_dbContextPs); }
        //        return _menuRepo;
        //    }
        //}

        private ILookupRepo _lookupRepo;
        public ILookupRepo LookupRepo
        {
            get
            {
                if (_lookupRepo == null) { _lookupRepo = new LookupRepo(_dbContextPs); }
                return _lookupRepo;
            }
        }

        private IUserRepo _userRepo;
        public IUserRepo UserRepo
        {
            get
            {
                if (_userRepo == null) { _userRepo = new UserRepo(_dbContextPs); }
                return _userRepo;
            }
        }

        private IUserAtRepo _userAtRepo;
        public IUserAtRepo UserAtRepo
        {
            get
            {
                if (_userAtRepo == null) { _userAtRepo = new UserAtRepo(_dbContextPs); }
                return _userAtRepo;
            }
        }

        private IViaRepo _viaRepo;
        public IViaRepo ViaRepo
        {
            get
            {
                if (_viaRepo == null) { _viaRepo = new ViaRepo(_dbContextPs); }
                return _viaRepo;
            }
        }

        private IBranchRepo _branchRepo;
        public IBranchRepo BranchRepo
        {
            get
            {
                if (_branchRepo == null) { _branchRepo = new BranchRepo(_dbContextPs); }
                return _branchRepo;
            }
        }


        private ICarrierRepo _carrierRepo;
        public ICarrierRepo CarrierRepo
        {
            get
            {
                if (_carrierRepo == null) { _carrierRepo = new CarrierRepo(_dbContextPs); }
                return _carrierRepo;
            }
        }

        private IManifestCarrierRepo _manifestCarrierRepo;
        public IManifestCarrierRepo ManifestCarrierRepo
        {
            get
            {
                if (_manifestCarrierRepo == null) { _manifestCarrierRepo = new ManifestCarrierRepo(_dbContextPs); }
                return _manifestCarrierRepo;
            }
        }


        private IManifestCarrierItemRepo _manifestCarrierItemRepo;
        public IManifestCarrierItemRepo ManifestCarrierItemRepo
        {
            get
            {
                if (_manifestCarrierItemRepo == null) { _manifestCarrierItemRepo = new ManifestCarrierItemRepo(_dbContextPs); }
                return _manifestCarrierItemRepo;
            }
        }




        private IManifestCarrierCostRepo _manifestCarrierCostRepo;
        public IManifestCarrierCostRepo ManifestCarrierCostRepo
        {
            get
            {
                if (_manifestCarrierCostRepo == null) { _manifestCarrierCostRepo = new ManifestCarrierCostRepo(_dbContextPs); }
                return _manifestCarrierCostRepo;
            }
        }


        private I_ManifestKoliSpb_Repo _manifestKoliSpb_Repo;
        public I_ManifestKoliSpb_Repo ManifestKoliSpb_Repo
        {
            get
            {
                if (_manifestKoliSpb_Repo == null) { _manifestKoliSpb_Repo = new ManifestKoliSpbRepo(_dbContextPs); }
                return _manifestKoliSpb_Repo;
            }
        }

        private I_ManifestKoli_Repo _manifestKoli_Repo;
        public I_ManifestKoli_Repo ManifestKoli_Repo
        {
            get
            {
                if (_manifestKoliSpb_Repo == null) { _manifestKoli_Repo = new ManifestKoliRepo(_dbContextPs); }
                return _manifestKoli_Repo;
            }
        }



        //private I_CostCarrier_Repo _costCarrier_Repo;
        //public I_CostCarrier_Repo CostCarrier_Repo
        //{
        //    get
        //    {
        //        if (_costCarrier_Repo == null) { _costCarrier_Repo = new CostCarrier_Repo(_dbContextPs); }
        //        return _costCarrier_Repo;
        //    }
        //}



        //=====================================================================================================


        // VIEW
        private IVCompanyCityRepo _vCompanyCityRepo;
        public IVCompanyCityRepo VCompanyCityRepo
        {
            get
            {
                if (_vCompanyCityRepo == null) { _vCompanyCityRepo = new VCompanyCityRepo(_dbContextPs); }
                return _vCompanyCityRepo;
            }
        }

        private IVCompanyAreaRepo _vCompanyAreaRepo;
        public IVCompanyAreaRepo VCompanyAreaRepo
        {
            get
            {
                if (_vCompanyAreaRepo == null) { _vCompanyAreaRepo = new VCompanyAreaRepo(_dbContextPs); }
                return _vCompanyAreaRepo;
            }
        }

        private IVMasterUrbanRepo _vMasterUrbanRepo;
        public IVMasterUrbanRepo VMasterUrbanRepo
        {
            get
            {
                if (_vMasterUrbanRepo == null) { _vMasterUrbanRepo = new VMasterUrbanRepo(_dbContextPs); }
                return _vMasterUrbanRepo;
            }
        }

        private IVMasterSubDistrictRepo _VMasterSubDistrictRepo;
        public IVMasterSubDistrictRepo VMasterSubDistrictRepo
        {
            get
            {
                if (_VMasterSubDistrictRepo == null) { _VMasterSubDistrictRepo = new VMasterSubDistrictRepo(_dbContextPs); }
                return _VMasterSubDistrictRepo;
            }
        }


        private IVMasterCityRepo _vMasterCityRepo;
        public IVMasterCityRepo VMasterCityRepo
        {
            get
            {
                if (_vMasterCityRepo == null) { _vMasterCityRepo = new VMasterCityRepo(_dbContextPs); }
                return _vMasterCityRepo;
            }
        }


        private IVCustomerAddressRepo _vCustomerAddressRepo;
        public IVCustomerAddressRepo VCustomerAddressRepo
        {
            get
            {
                if (_vCustomerAddressRepo == null) { _vCustomerAddressRepo = new VCustomerAddressRepo(_dbContextPs); }
                return _vCustomerAddressRepo;
            }
        }

        private IVManifestCarrierRepo _vManifestCarrierRepo;
        public IVManifestCarrierRepo VManifestCarrierRepo
        {
            get
            {
                if (_vManifestCarrierRepo == null) { _vManifestCarrierRepo = new VManifestCarrierRepo(_dbContextPs); }
                return _vManifestCarrierRepo;
            }
        }


        private ISPManifestDetailRepo _sPManifestDetailRepo;
        public ISPManifestDetailRepo SPManifestDetailRepo
        {
            get
            {
                if (_sPManifestDetailRepo == null) { _sPManifestDetailRepo = new SPManifestDetailRepo(_dbContextPs); }
                return _sPManifestDetailRepo;
            }
        }

        private ISPSpbGoods_CalculatePriceRepo _sPSpbGoods_CalculatePriceRepo;
        public ISPSpbGoods_CalculatePriceRepo SPSpbGoods_CalculatePriceRepo
        {
            get
            {
                if (_sPSpbGoods_CalculatePriceRepo == null) { _sPSpbGoods_CalculatePriceRepo = new SPSpbGoods_CalculatePriceRepo(_dbContextPs); }
                return _sPSpbGoods_CalculatePriceRepo;
            }
        }


        // MAIN

        private ISpbRepo _SpbRepo;
        public ISpbRepo SpbRepo
        {
            get
            {
                if (_SpbRepo == null) { _SpbRepo = new SpbRepo(_dbContextPs); }
                return _SpbRepo;
            }
        }

        private ISpbGoodsRepo _spbGoodsRepo;
        public ISpbGoodsRepo SpbGoodsRepo
        {
            get
            {
                if (_spbGoodsRepo == null) { _spbGoodsRepo = new SpbGoodsRepo(_dbContextPs); }
                return _spbGoodsRepo;
            }
        }

        private IMasterSubDistrictRepo _MasterSubDistrictRepo;
        public IMasterSubDistrictRepo MasterSubDistrictRepo
        {
            get
            {
                if (_MasterSubDistrictRepo == null) { _MasterSubDistrictRepo = new MasterSubDistrictRepo(_dbContextPs); }
                return _MasterSubDistrictRepo;
            }
        }


        private IMasterUrbanRepo _MasterUrbanRepo;
        public IMasterUrbanRepo masterUrbanRepo
        {
            get
            {
                if (_MasterUrbanRepo == null) { _MasterUrbanRepo = new MasterUrbanRepo(_dbContextPs); }
                return _MasterUrbanRepo;
            }
        }

        private ICustomerRepo _customerRepo;
        public ICustomerRepo CustomerRepo
        {
            get
            {
                if (_customerRepo == null) { _customerRepo = new CustomerRepo(_dbContextPs); }
                return _customerRepo;
            }
        }

        private ICustomerNameRepo _customerNameRepo;
        public ICustomerNameRepo CustomerNameRepo
        {
            get
            {
                if (_customerNameRepo == null) { _customerNameRepo = new CustomerNameRepo(_dbContextPs); }
                return _customerNameRepo;
            }
        }

        private ICustomerPlaceRepo _customerPlaceRepo;
        public ICustomerPlaceRepo CustomerPlaceRepo
        {
            get
            {
                if (_customerPlaceRepo == null) { _customerPlaceRepo = new CustomerPlaceRepo(_dbContextPs); }
                return _customerPlaceRepo;
            }
        }

        private ICustomerStoreRepo _customerStoreRepo;
        public ICustomerStoreRepo CustomerStoreRepo
        {
            get
            {
                if (_customerStoreRepo == null) { _customerStoreRepo = new CustomerStoreRepo(_dbContextPs); }
                return _customerStoreRepo;
            }
        }

        private ICustomerAddressRepo _customerAddressRepo;
        public ICustomerAddressRepo CustomerAddressRepo
        {
            get
            {
                if (_customerAddressRepo == null) { _customerAddressRepo = new CustomerAddressRepo(_dbContextPs); }
                return _customerAddressRepo;
            }
        }

        private ICompanyRepo _companyRepo;
        public ICompanyRepo CompanyRepo
        {
            get
            {
                if (_companyRepo == null) { _companyRepo = new CompanyRepo(_dbContextPs); }
                return _companyRepo;
            }
        }

        private IManifestRepo _manifestRepo;
        public IManifestRepo ManifestRepo
        {
            get
            {
                if (_manifestRepo == null) { _manifestRepo = new ManifestRepo(_dbContextPs); }
                return _manifestRepo;
            }
        }



        private IMasterCityRepo _masterCityRepo;
        public IMasterCityRepo MasterCityRepo
        {
            get
            {
                if (_masterCityRepo == null) { _masterCityRepo = new MasterCityRepo(_dbContextPs); }
                return _masterCityRepo;
            }
        }

        private IVSpbRepo _vSpbRepo;
        public IVSpbRepo VSpbRepo
        {
            get
            {
                if (_vSpbRepo == null) { _vSpbRepo = new VSpbRepo(_dbContextPs); }
                return _vSpbRepo;
            }
        }

        private IVSpbGoodsRepo _vSpbGoodsRepo;
        public IVSpbGoodsRepo VSpbGoodsRepo
        {
            get
            {
                if (_vSpbGoodsRepo == null) { _vSpbGoodsRepo = new VSpbGoodsRepo(_dbContextPs); }
                return _vSpbGoodsRepo;
            }
        }


        private IVManifestRepo _vManifestRepo;
        public IVManifestRepo VManifestRepo
        {
            get
            {
                if (_vManifestRepo == null) { _vManifestRepo = new VManifestRepo(_dbContextPs); }
                return _vManifestRepo;
            }
        }

        private IVManifestDetailRepo _vManifestDetailRepo;
        public IVManifestDetailRepo VManifestDetailRepo
        {
            get
            {
                if (_vManifestDetailRepo == null) { _vManifestDetailRepo = new VManifestDetailRepo(_dbContextPs); }
                return _vManifestDetailRepo;
            }
        }

        private IRatesRepo _ratesRepo;
        public IRatesRepo RatesRepo
        {
            get
            {
                if (_ratesRepo == null) { _ratesRepo = new RatesRepo(_dbContextPs); }
                return _ratesRepo;
            }
        }


        private IRatesDetailRepo _ratesDetailRepo;
        public IRatesDetailRepo RatesDetailRepo
        {
            get
            {
                if (_ratesDetailRepo == null) { _ratesDetailRepo = new RatesDetailRepo(_dbContextPs); }
                return _ratesDetailRepo;
            }
        }


        private IVCustomerRepo _vCustomerRepo;
        public IVCustomerRepo VCustomerRepo
        {
            get
            {
                if (_vCustomerRepo == null) { _vCustomerRepo = new VCustomerRepo(_dbContextPs); }
                return _vCustomerRepo;
            }
        }

        // Finance

        private IAccountReceivableRepo _AccountReceivableRepo;
        public IAccountReceivableRepo AccountReceivableRepo
        {
            get
            {
                if (_AccountReceivableRepo == null) { _AccountReceivableRepo = new AccountReceivableRepo(_dbContextPs); }
                return _AccountReceivableRepo;
            }
        }


        private IRatesSubDistrictRepo _ratesSubDistrictRepo;
        public IRatesSubDistrictRepo RatesSubDistrictRepo
        {
            get
            {
                if (_ratesSubDistrictRepo == null) { _ratesSubDistrictRepo = new RatesSubDistrictRepo(_dbContextPs); }
                return _ratesSubDistrictRepo;
            }
        }

        private IRatesSubDistrictDetailRepo _ratesSubDistrictDetailRepo;
        public IRatesSubDistrictDetailRepo RatesSubDistrictDetailRepo
        {
            get
            {
                if (_ratesSubDistrictDetailRepo == null) { _ratesSubDistrictDetailRepo = new RatesSubDistrictDetailRepo(_dbContextPs); }
                return _ratesSubDistrictDetailRepo;
            }
        }

        private ISPSpbHourlyRepo _sPSpbHourlyRepo;
        public ISPSpbHourlyRepo SPSpbHourlyRepo
        {
            get
            {
                if (_sPSpbHourlyRepo == null) { _sPSpbHourlyRepo = new SPSpbHourlyRepo(_dbContextPs); }
                return _sPSpbHourlyRepo;
            }
        }

        private IPrivillegeRepo _privillegeRepo;
        public IPrivillegeRepo PrivillegeRepo
        {
            get
            {
                if (_privillegeRepo == null) { _privillegeRepo = new PrivillegeRepo(_dbContextPs); }
                return _privillegeRepo;
            }
        }

        private PrivilegeActionRepo _privilegeActionRepo;
        public IPrivilegeActionRepo PrivilegeActionRepo
        {
            get
            {
                if (_privilegeActionRepo == null) { _privilegeActionRepo = new PrivilegeActionRepo(_dbContextPs); }
                return _privilegeActionRepo;
            }
        }

        private IMenuActionRepo _menuActionRepo;
        public IMenuActionRepo MenuActionRepo
        {
            get
            {
                if (_menuActionRepo == null) { _menuActionRepo = new MenuActionRepo(_dbContextPs); }
                return _menuActionRepo;
            }
        }

        private IMenuDataRepo _menuDataRepo;
        public IMenuDataRepo MenuDataRepo
        {
            get
            {
                if (_menuDataRepo == null) { _menuDataRepo = new MenuDataRepo(_dbContextPs); }
                return _menuDataRepo;
            }
        }

        private IVCustomerApprovalRepo _vCustomerApprovalRepo;
        public IVCustomerApprovalRepo VCustomerApprovalRepo
        {
            get
            {
                if (_vCustomerApprovalRepo == null) { _vCustomerApprovalRepo = new VCustomerApprovalRepo(_dbContextPs); }
                return _vCustomerApprovalRepo;
            }
        }

    }
}

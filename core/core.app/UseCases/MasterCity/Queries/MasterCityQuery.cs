﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;

namespace core.app.UseCases.MasterCity.Queries

{

    public partial class MasterCityQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; } 
    }
    public class MasterCityQueryHandler : IRequestHandler<MasterCityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MasterCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MasterCityQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;



            var query = from a in _context.MasterCityEntitys
                        join b in _context.MasterProvinceEntitys on a.MasterProvinceId equals b.MasterProvinceId

                        select new
                        {
                            a.MasterCityId,
                            a.MasterProvinceId,
                            b.MasterProvinceName,
                            a.MasterCityType,
                            a.MasterCityCode,
                            a.MasterCityName,
                            a.MasterCityCapital
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.MasterCityId,
                        s.MasterProvinceId,
                        s.MasterProvinceName,
                        s.MasterCityType,
                        s.MasterCityCode,
                        s.MasterCityName,
                        s.MasterCityCapital

                    }).OrderByDescending(o => o.MasterCityId);
            }

            // return 
            return rtn;
        }


    }

}

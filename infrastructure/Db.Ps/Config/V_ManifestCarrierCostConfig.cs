﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_ManifestCarrierCostConfig : IEntityTypeConfiguration<V_ManifestCarrierCostEntity>
    {
        public void Configure(EntityTypeBuilder<V_ManifestCarrierCostEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierId);
            builder.ToTable("V_ManifestCarrierCost");
        }
    }
}

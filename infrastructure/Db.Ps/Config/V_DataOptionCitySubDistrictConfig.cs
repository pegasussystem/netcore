﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
     class V_DataOptionCitySubDistrictConfig : IEntityTypeConfiguration<V_DataOptionCitySubDistrictEntity>
    {
        public void Configure(EntityTypeBuilder<V_DataOptionCitySubDistrictEntity> builder)
        {
            builder.HasKey(e => e.Id);
            builder.ToTable("V_DataOptionCitySubDistrict");
        }
    }
}

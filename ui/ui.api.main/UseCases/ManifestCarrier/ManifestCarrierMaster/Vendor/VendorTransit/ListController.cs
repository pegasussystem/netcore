﻿using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorTransit.Query;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;


namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorTransit
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]

    public class ListController : ApiController
    {
        [HttpGet("manifest-carrier-master-vendor-transit-list")]
        public async Task<IActionResult> VendorTransitList([FromQuery] ManifestCarrierMasterVendorTransitListQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("manifest-carrier-master-vendor-transit-list-vendor")]
        public async Task<IActionResult> ListVendor([FromQuery] ManifestCarrierMasterVendorTransitListVendorQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("manifest-carrier-master-vendor-transit-list-station")]
        public async Task<IActionResult> ListStation([FromQuery] ManifestCarrierMasterVendorTransitListStationQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VCustomerIdPelangganEntity
    {
        public String? RowNo { get; set; }
        public String? CityCode { get; set; }
        public Int32? CustomerId { get; set; }
        public String? Phone { get; set; }
        public String? Type { get; set; }
        public String? Area { get; set; }


        //public String? ###CustomerName	{ get; set; }
        public Int32? CustomerNameId { get; set; }
        public String? CustomerName { get; set; }


        //public String? ###CustomerPlace	{ get; set; }
        public Int32? CustomerPlaceId { get; set; }
        public String? CustomerPlace { get; set; }


        //public String? ###CustomerStore	{ get; set; }
        public Int32? CustomerStoreId { get; set; }
        public String? CustomerStore { get; set; }


        //public String? ###CustomerAddress	{ get; set; }
        public Int32? CustomerAddressId { get; set; }
        public String? CustomerAddress { get; set; }
        public Int32? Rt { get; set; }
        public Int32? Rw { get; set; }
        public String? Urban { get; set; }
        public String? MasterUrbanName { get; set; }
        public String? SubDistrict { get; set; }
        public String? MasterSubDistrictName { get; set; }
        public String? City { get; set; }
        public String? MasterCityName { get; set; }
        public String? PostalCode { get; set; }
        public String? MasterUrbanPostalCode { get; set; }
        public String? DataConcat { get; set; }

        public String? TypeCustomer { get; set; }

        public String? CustomerCode { get; set; }

        public String? MasterCityCode { get; set; }  // 5 Jul 22
    }
}

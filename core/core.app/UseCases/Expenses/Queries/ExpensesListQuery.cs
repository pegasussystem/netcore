using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using core.app.Common.Interfaces;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace core.app.UseCases.Expenses.Queries
{
    public partial class ExpensesListQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class ExpensesListQueryHandler : IRequestHandler<ExpensesListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ExpensesListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ExpensesListQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_ExpensesListEntity> entities;

            // DateTime utcDate = req.CreatedAt.Value.AddHours((double)(-1 * req.uiUserProfile.WorkTimeZoneHour)).AddMinutes((double)(req.uiUserProfile.WorkTimeZoneMinute * -1));


            entities = this.GetExpensesList();


          if (entities != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = entities.Where(e => e.createdAt.Year == DateTime.Now.Year && e.createdAt.Month == DateTime.Now.Month).Select(
                         s => new
                         {
                             s.expensesId
                             ,
                             s.bukuKasId
                             ,
                             s.createdAt
                             ,
                             s.updatedAt
                             ,
                             s.CoaId
                             ,
                             s.coaCode
                             ,
                             s.coaName
                             ,
                             s.keterangan
                             ,
                             s.debit
                             ,
                             s.credit
                             ,
                             s.saldo
                             ,
                             s.cumulativeBalance
                         });
                }

            // return 
            return rtn;
        }

        public IEnumerable<SP_ExpensesListEntity> GetExpensesList()
        {
            String query = $@"EXEC SP_ExpensesList";
            var res = this._context.SP_ExpensesListEntities.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
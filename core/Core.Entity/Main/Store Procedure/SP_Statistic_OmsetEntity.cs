﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_Statistic_OmsetEntity
    {
        public Int64? row_number { get; set; }
        public Int32? month_code { get; set; }
        public Int32? years { get; set; }
        public String? month_year { get; set; }
        public Int32? count_spb { get; set; }
        public Decimal? sum_caw { get; set; }
        public Int32? sum_price { get; set; }
    }
}

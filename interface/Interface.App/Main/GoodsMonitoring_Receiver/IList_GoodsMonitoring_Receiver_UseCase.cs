﻿using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main.GoodsMonitoring_Receiver
{
    public interface IListGoodsMonitoringReceiverUseCase : IApp<ListGoodsMonitoringReceiverEntity>
    {
        ////---------------------------- Sudah tidak dipake di pindahkan ke Core.app -> UseCase -> GoodsMonitoring -> ListReceiver -> ListReceiverQuery -------------------------------//
        // public ReturnFormat List(UiUserProfileEntity profile, Object input);
    }
}

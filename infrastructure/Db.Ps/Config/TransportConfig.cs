﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class TransportConfig : IEntityTypeConfiguration<TransportEntity>
    {
        public void Configure(EntityTypeBuilder<TransportEntity> builder)
        {
            builder.HasKey(e => e.TransportId);
            builder.ToTable("Transport");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostPenerus.Commands
{
    public partial class CostPenerusUpdateCommand : IRequest<ReturnFormat>
    {
        public Int32? costPenerusId { get; set; }
        public Int32? costPenerusDetailId { get; set; }
        public Int32? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostPenerusUpdateCommandHandler : IRequestHandler<CostPenerusUpdateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostPenerusUpdateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostPenerusUpdateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostPenerusUpdate(req.costPenerusId, req.costPenerusDetailId, req.baseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostPenerusUpdateEntity> CostPenerusUpdate(Int32? costPenerusId, Int32? costPenerusDetailId, Int32? baseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostPenerusUpdate @costPenerusid='{costPenerusId}', @costPenerusDetailid='{costPenerusDetailId}', @baseCost='{baseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostPenerusUpdateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

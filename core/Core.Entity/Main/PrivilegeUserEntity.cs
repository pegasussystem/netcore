﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class PrivilegeUserEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? PrivilegeUserId { get; set; }
        public Int32? PrivilegeId { get; set; }
        public Int32? UserId { get; set; }
    }
}

﻿using Core.Entity.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class LookupConfig : IEntityTypeConfiguration<LookupEntity>
    {
        public void Configure(EntityTypeBuilder<LookupEntity> builder)
        {
            builder.HasKey(e => e.LookupId);
            builder.ToTable("Lookup");
        }
    }
}

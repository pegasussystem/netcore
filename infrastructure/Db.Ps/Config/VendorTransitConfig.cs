﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VendorTransitConfig : IEntityTypeConfiguration<VendorTransitEntity>
    {
        public void Configure(EntityTypeBuilder<VendorTransitEntity> builder)
        {
            builder.HasKey(e => e.VendorTransitId);
            builder.ToTable("VendorTransit");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
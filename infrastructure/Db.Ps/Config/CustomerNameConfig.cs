﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class CustomerNameConfig : IEntityTypeConfiguration<CustomerNameEntity>
    {
        public void Configure(EntityTypeBuilder<CustomerNameEntity> builder)
        {
            builder.HasKey(e => e.CustomerNameId);
            builder.ToTable("CustomerName");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

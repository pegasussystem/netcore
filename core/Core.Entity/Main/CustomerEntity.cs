﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class CustomerEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public String? CityCode { get; set; }
        public Int32? CustomerId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? Phone { get; set; }
        public String? Telp { get; set; }

        public String? Type { get; set; }
        public String? Area { get; set; }
        public Boolean? IsActive { get; set; }

    }
}

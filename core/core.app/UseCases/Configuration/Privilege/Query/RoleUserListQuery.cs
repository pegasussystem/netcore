﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Configuration.Privilege
{
    public partial class RoleUserListQuery : IRequest<ReturnFormat>
    {
        public Int32? privilegeId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class RoleUserListQueryHandler : IRequestHandler<RoleUserListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public RoleUserListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(RoleUserListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.V_PrivilegeUserListEntitys
                        where a.PrivilegeId.Equals(req.privilegeId)
                        select new
                        {
                            a.UserId
                            , a.Username
                            , a.FullName
                            , a.Email
                            , a.PrivilegeId
                            , a.RolesId
                            , a.RolesName
                            , a.Desc
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.UserId
                        , s.Username
                        , s.FullName
                        , s.Email
                        , s.PrivilegeId
                        , s.RolesId
                        , s.RolesName
                        , s.Desc
                    }).OrderBy(o => o.Username);
            }

            // return
            return rtn;
        }
    }
}

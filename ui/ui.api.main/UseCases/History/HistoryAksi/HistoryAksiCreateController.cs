﻿using core.app.UseCases.Base;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.HistoryAksi
{
    [Authorize]
    [Route("history-aksi")]
    [ApiController]
    public class HistoryAksiCreateController : ApiController
    {
        [HttpPost("create")]
        public async Task<IActionResult> UpdateDescription([FromBody] CreateHistoryLoginCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

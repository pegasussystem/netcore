﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_DataUserEntity
    {
        public Int32? UserId { get; set; }
        public String? Username { get; set; }
        public String? Email { get; set; }
        public String? FullName { get; set; }
        public String? TableName { get; set; }
        public Int32? TableNameId { get; set; }
        public String? WorkUnit { get; set; }
        public String? WorkPlace { get; set; }
        public Int32? CityIdCode { get; set; }
        public Int32? IsActive { get; set; }
        public Int32? IsCorporate { get; set; }
        public String? Status { get; set; }
        public Int32? PrivilegeId { get; set; }
        public String? PrivilegeName { get; set; }
        public Int32? RolesId { get; set; }
        public String? RolesName { get; set; }
        public String? Description { get; set; }

    }
}

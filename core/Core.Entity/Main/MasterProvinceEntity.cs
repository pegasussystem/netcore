﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MasterProvinceEntity
    {
        public String MasterProvinceId { get; set; }
        public String MasterProvinceCode { get; set; }
        public String MasterProvinceName { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class SubBranchEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? SubBranchId { get; set; }
        public Int32? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? CityIdCode { get; set; }
        public Int32? AreaIdCode { get; set; }
        public String? SubBranchCode { get; set; }
        public String? SubBranchName { get; set; }
        public String? SubBranchAddress { get; set; }
        public String? SubBranchTimeZone { get; set; }
        public Boolean? IsCorporate { get; set; }
        public Int32? IsAgree { get; set; }
        public Boolean? IsActive { get; set; }
    }
}

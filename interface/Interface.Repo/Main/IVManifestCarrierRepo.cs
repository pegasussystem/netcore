﻿using Core.Entity.Main.View;

namespace Interface.Repo.Main
{ 

    public interface IVManifestCarrierRepo : IRepo<VManifestCarrierEntity>
    {

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
   public class V_FlightRouteEntity
    {
        public Int32? FlightRouteId { get; set; }
        public Int32? OriginStationId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? FirstFlightCodeId { get; set; }
        public Int32? FirstFlightId { get; set; }
        public Int32? SecondFlightCodeId { get; set; }
        public Int32? SecondFlightId { get; set; }
        public Int32? ThirdFlightCodeId { get; set; }
        public Int32? ThirdFlightId { get; set; }
        public Int32? FourthFlightId { get; set; }
        public Int32? FourthFlightCodeId { get; set; }
        public Int32? VendorCarrierId { get; set; } 
        public Int32? DestinationStationId { get; set; }
        public Int32? LastFlightCodeId { get; set; }
        public Int32? LastFlightId { get; set; }
    }
}

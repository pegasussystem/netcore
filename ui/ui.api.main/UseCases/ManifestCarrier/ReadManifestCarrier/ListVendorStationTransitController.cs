﻿using core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.ReadManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ListVendorStationTransitController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public ListVendorStationTransitController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("vendor-station-transit")]
        public async Task<IActionResult> ListVendorStationTransit([FromQuery] ListVendorStationTransitReadQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("vendor-station-transit-first")]
        public async Task<IActionResult> ListVendorStationTransitFirst([FromQuery] ListVendorFirstStationTransitReadQuery q)
        {

           // --- variable
           // --- --- --- --- ---
           ReturnFormat _returnFormat = new ReturnFormat();

           // --- get data
           // --- --- --- --- ---
           _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


           // --- return
           // --- --- --- --- ---
           return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpGet("vendor-station-transit-second")]
        public async Task<IActionResult> ListVendorStationTransitSecond([FromQuery] ListVendorSecondStationTransitReadQuery q)
        {

           // --- variable
           // --- --- --- --- ---
           ReturnFormat _returnFormat = new ReturnFormat();

           // --- get data
           // --- --- --- --- ---
           _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


           // --- return
           // --- --- --- --- ---
           return StatusCode(_returnFormat.Status, _returnFormat);
        }


        //[HttpGet("vendor-station-transit-third")]
        //public async Task<IActionResult> ListVendorStationTransitThird([FromQuery] ListVendorStationTransitThirdReadQuery q)
        //{

        //    // --- variable
        //    // --- --- --- --- ---
        //    ReturnFormat _returnFormat = new ReturnFormat();

        //    // --- get data
        //    // --- --- --- --- ---
        //    _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


        //    // --- return
        //    // --- --- --- --- ---
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}
    }
}


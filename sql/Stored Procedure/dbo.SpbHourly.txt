/****** Object:  StoredProcedure [dbo].[SpbHourly]    Script Date: 21/10/2020 10:49:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpbHourly]
	@year int,
	@month int,
	@day int,
	@timeZoneHour int,
	@timeZoneMinute int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	/** 
		EXEC SpbHourly  @year=2020, @month=8,@day=3,  @timeZoneHour=7, @timeZoneMinute=0
	*/
	SET NOCOUNT ON;

   SELECT 
   t.CompanyId, t.BranchId, t.SubBranchId,
	t.Carrier,t.ManifestId, mco.MasterCityCode 'Origin', bo.BranchName 'BranchOrigin',
	mcd.MasterCityId 'DestinationId', mcd.MasterCityCode 'Destination', bd.BranchName 'BranchDestination',
	a.Alphabet,
	SUM(t.Koli_1200) 'Koli_1200', SUM(t.Aw_1200) 'Aw_1200',SUM(t.Caw_1200) 'Caw_1200',
	SUM(t.Koli_1400) 'Koli_1400', SUM(t.Aw_1400) 'Aw_1400',SUM(t.Caw_1400) 'Caw_1400',
	SUM(t.Koli_1530) 'Koli_1530', SUM(t.Aw_1530) 'Aw_1530',SUM(t.Caw_1530) 'Caw_1530',
	SUM(t.Koli_1600) 'Koli_1600', SUM(t.Aw_1600) 'Aw_1600',SUM(t.Caw_1600) 'Caw_1600',
	SUM(t.Koli_Final) 'Koli_Final',SUM(t.Aw_Final)  'Aw_Final',SUM(t.Caw_Final) 'Caw_Final'
FROM (

	SELECT 
		s.CompanyId, s.BranchId, s.SubBranchId,
		m.ManifestId, s.OriginCityCode,s.DestinationCityCode, m.ManifestAlphabet, m.Carrier,
		COUNT(DISTINCT mk.KoliNo) 'Koli_1200', SUM(sg.Aw) 'Aw_1200',SUM(sg.Caw) 'Caw_1200',
		0 'Koli_1400', 0 'Aw_1400',0 'Caw_1400',
		0 'Koli_1530', 0 'Aw_1530',0 'Caw_1530',
		0 'Koli_1600', 0 'Aw_1600',0 'Caw_1600',
		0 'Koli_Final', 0 'Aw_Final',0 'Caw_Final'
	FROM Spb s
	-- jumlah koli
	LEFT JOIN SpbGoods sg ON s.SpbId = sg.SpbId

	-- Manifest
	LEFT JOIN ManifestKoliSpb mks ON sg.SpbGoodsId = mks.SpbGoodsId
	LEFT JOIN ManifestKoli mk ON mks.ManifestKoliId = mk.ManifestKoliId
	LEFT JOIN Manifest m ON mk.ManifestId = m.ManifestId

	-- WHERE
	WHERE DAY( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@day  
		AND MONTH( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@month  
		AND YEAR( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@year 

		AND s.IsVoid IS NULL AND s.OriginCityCode IS NOT NULL AND s.DestinationCityCode IS NOT NULL
		AND CAST(DATEADD(HOUR,7,s.CreatedAt) AS TIME) BETWEEN '00:00' and '12:00'

		AND s.TotalPrice IS NOT NULL AND m.Carrier IS NOT NULL
		
	GROUP BY s.CompanyId, s.BranchId, s.SubBranchId,m.ManifestId, s.OriginCityCode,s.DestinationCityCode,m.ManifestAlphabet,m.Carrier

	UNION ALL -- ----------------------------------------------------------------------------------------------------------------
	
	SELECT 
		s.CompanyId, s.BranchId, s.SubBranchId,
		m.ManifestId, s.OriginCityCode,s.DestinationCityCode, m.ManifestAlphabet, m.Carrier,
		0 'Koli_1200', 0 'Aw_1200',0 'Caw_1200',
		COUNT(DISTINCT mk.KoliNo) 'Koli_1400', SUM(sg.Aw) 'Aw_1400',SUM(sg.Caw) 'Caw_1400',
		0 'Koli_1530', 0 'Aw_1530',0 'Caw_1530',
		0 'Koli_1600', 0 'Aw_1600',0 'Caw_1600',
		0 'Koli_Final', 0 'Aw_Final',0 'Caw_Final'
	FROM Spb s
	-- jumlah koli
	LEFT JOIN SpbGoods sg ON s.SpbId = sg.SpbId

	-- Manifest
	LEFT JOIN ManifestKoliSpb mks ON sg.SpbGoodsId = mks.SpbGoodsId
	LEFT JOIN ManifestKoli mk ON mks.ManifestKoliId = mk.ManifestKoliId
	LEFT JOIN Manifest m ON mk.ManifestId = m.ManifestId

	-- WHERE
	WHERE DAY( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@day  
		AND MONTH( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@month  
		AND YEAR( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@year 

		AND s.IsVoid IS NULL AND s.OriginCityCode IS NOT NULL AND s.DestinationCityCode IS NOT NULL
		AND CAST(DATEADD(HOUR,7,s.CreatedAt) AS TIME) BETWEEN '00:00' and '14:00'

		AND s.TotalPrice IS NOT NULL AND m.Carrier IS NOT NULL
		
	GROUP BY s.CompanyId, s.BranchId, s.SubBranchId,m.ManifestId, s.OriginCityCode,s.DestinationCityCode,m.ManifestAlphabet,m.Carrier

	UNION ALL -- ----------------------------------------------------------------------------------------------------------------
	
	SELECT 
		s.CompanyId, s.BranchId, s.SubBranchId,
		m.ManifestId, s.OriginCityCode,s.DestinationCityCode, m.ManifestAlphabet, m.Carrier,
		0 'Koli_1200', 0 'Aw_1200',0 'Caw_1200',
		0 'Koli_1400', 0 'Aw_1400',0 'Caw_1400',
		COUNT(DISTINCT mk.KoliNo) 'Koli_1530', SUM(sg.Aw) 'Aw_1530',SUM(sg.Caw) 'Caw_1530',
		0 'Koli_1600', 0 'Aw_1600',0 'Caw_1600',
		0 'Koli_Final', 0 'Aw_Final',0 'Caw_Final'
	FROM Spb s
	-- jumlah koli
	LEFT JOIN SpbGoods sg ON s.SpbId = sg.SpbId

	-- Manifest
	LEFT JOIN ManifestKoliSpb mks ON sg.SpbGoodsId = mks.SpbGoodsId
	LEFT JOIN ManifestKoli mk ON mks.ManifestKoliId = mk.ManifestKoliId
	LEFT JOIN Manifest m ON mk.ManifestId = m.ManifestId

	-- WHERE
	WHERE DAY( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@day  
		AND MONTH( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@month  
		AND YEAR( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@year 

		AND s.IsVoid IS NULL AND s.OriginCityCode IS NOT NULL AND s.DestinationCityCode IS NOT NULL
		AND CAST(DATEADD(HOUR,7,s.CreatedAt) AS TIME) BETWEEN '00:00' and '15:30'

		AND s.TotalPrice IS NOT NULL AND m.Carrier IS NOT NULL
		
	GROUP BY s.CompanyId, s.BranchId, s.SubBranchId,m.ManifestId, s.OriginCityCode,s.DestinationCityCode,m.ManifestAlphabet,m.Carrier

	UNION ALL -- ----------------------------------------------------------------------------------------------------------------
	
	SELECT 
		s.CompanyId, s.BranchId, s.SubBranchId,
		m.ManifestId, s.OriginCityCode,s.DestinationCityCode, m.ManifestAlphabet, m.Carrier,
		0 'Koli_1200', 0 'Aw_1200',0 'Caw_1200',
		0 'Koli_1400', 0 'Aw_1400',0 'Caw_1400',
		0 'Koli_1530', 0 'Aw_1530',0 'Caw_1530',
		COUNT(DISTINCT mk.KoliNo) 'Koli_1600', SUM(sg.Aw) 'Aw_1600',SUM(sg.Caw) 'Caw_1600',
		0 'Koli_Final', 0 'Aw_Final',0 'Caw_Final'
	FROM Spb s
	-- jumlah koli
	LEFT JOIN SpbGoods sg ON s.SpbId = sg.SpbId

	-- Manifest
	LEFT JOIN ManifestKoliSpb mks ON sg.SpbGoodsId = mks.SpbGoodsId
	LEFT JOIN ManifestKoli mk ON mks.ManifestKoliId = mk.ManifestKoliId
	LEFT JOIN Manifest m ON mk.ManifestId = m.ManifestId

	-- WHERE
	WHERE DAY( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@day  
		AND MONTH( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@month  
		AND YEAR( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@year 

		AND s.IsVoid IS NULL AND s.OriginCityCode IS NOT NULL AND s.DestinationCityCode IS NOT NULL
		AND CAST(DATEADD(HOUR,7,s.CreatedAt) AS TIME) BETWEEN '00:00' and '16:00'

		AND s.TotalPrice IS NOT NULL AND m.Carrier IS NOT NULL
		
	GROUP BY s.CompanyId, s.BranchId, s.SubBranchId,m.ManifestId, s.OriginCityCode,s.DestinationCityCode,m.ManifestAlphabet,m.Carrier

	UNION ALL -- ----------------------------------------------------------------------------------------------------------------
	
	SELECT 
		s.CompanyId, s.BranchId, s.SubBranchId,
		m.ManifestId, s.OriginCityCode,s.DestinationCityCode, m.ManifestAlphabet, m.Carrier,
		0 'Koli_1200', 0 'Aw_1200',0 'Caw_1200',
		0 'Koli_1400', 0 'Aw_1400',0 'Caw_1400',
		0 'Koli_1530', 0 'Aw_1530',0 'Caw_1530',
		0 'Koli_1600', 0 'Aw_1600',0 'Caw_1600',
		COUNT(DISTINCT mk.KoliNo) 'Koli_Final', SUM(sg.Aw) 'Aw_Final',SUM(sg.Caw) 'Caw_Final'
	FROM Spb s
	-- jumlah koli
	LEFT JOIN SpbGoods sg ON s.SpbId = sg.SpbId

	-- Manifest
	LEFT JOIN ManifestKoliSpb mks ON sg.SpbGoodsId = mks.SpbGoodsId
	LEFT JOIN ManifestKoli mk ON mks.ManifestKoliId = mk.ManifestKoliId
	LEFT JOIN Manifest m ON mk.ManifestId = m.ManifestId

	-- WHERE
	WHERE DAY( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@day  
		AND MONTH( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@month  
		AND YEAR( DATEADD(minute,@timeZoneMinute,DATEADD(HOUR,@timeZoneHour,s.CreatedAt)) )=@year 

		AND s.IsVoid IS NULL AND s.OriginCityCode IS NOT NULL AND s.DestinationCityCode IS NOT NULL
		AND CAST(DATEADD(HOUR,7,s.CreatedAt) AS TIME) BETWEEN '00:00' and '23:59'

		AND s.TotalPrice IS NOT NULL AND m.Carrier IS NOT NULL
		
	GROUP BY s.CompanyId, s.BranchId, s.SubBranchId,m.ManifestId, s.OriginCityCode,s.DestinationCityCode,m.ManifestAlphabet,m.Carrier


) as t

-- ORIGIN
LEFT JOIN CompanyCity cco ON t.OriginCityCode = cco.CompanyCityId 
LEFT JOIN MasterCity mco ON cco.CityId = mco.MasterCityId
LEFT JOIN Branch bo ON bo.CityHandled like '%<'+mco.MasterCityId+'>%'

-- DESTINATION
LEFT JOIN CompanyCity ccd ON t.DestinationCityCode = ccd.CompanyCityId 
LEFT JOIN MasterCity mcd ON ccd.CityId = mcd.MasterCityId
LEFT JOIN Branch bd ON bd.CityHandled like '%<'+mcd.MasterCityId+'>%'

LEFT JOIN Alphabet a ON t.ManifestAlphabet = a.AlphabetId

GROUP BY 
   t.CompanyId, t.BranchId,bd.BranchName, t.SubBranchId, t.Carrier,t.ManifestId, mco.MasterCityCode,mcd.MasterCityCode, a.Alphabet, mcd.MasterCityId, bo.BranchName

END
GO
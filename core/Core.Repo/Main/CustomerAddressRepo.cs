﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class CustomerAddressRepo : Repo<CustomerAddressEntity>, ICustomerAddressRepo
    {
        public CustomerAddressRepo(DbContextPs ctx) : base(ctx)
        {
        }
    }
}

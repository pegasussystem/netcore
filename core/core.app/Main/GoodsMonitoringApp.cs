﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace core.app.Main
{
    public class GoodsMonitoringApp : IGoodsMonitoringApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public GoodsMonitoringApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Delete(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat List(UiUserProfileEntity uiUserProfileEntity, DateTime dateLocal) {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<SPManifest_MonitoringEntity> sPManifest_MonitoringEntities;
            DateTime dateUtc = dateLocal.AddHours((Double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)uiUserProfileEntity.WorkTimeZoneMinute);

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;

            //
            //// logic
            //sPManifest_MonitoringEntities = _repoPs.ManifestRepo.ListManfestMonitoring(dateUtc.Year, dateUtc.Month, dateUtc.Day, branchConfigKey, (Int32)uiUserProfileEntity.companyId).ToList();

            //// from sub branch. sub branch cannot SEE the data
            //if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId != null)
            //{
            //    sPManifest_MonitoringEntities = sPManifest_MonitoringEntities.Where(w => w.BranchId.Equals(99999999999)).ToList();
            //}

            //// from branch
            //else if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId == null)
            //{
            //    sPManifest_MonitoringEntities = sPManifest_MonitoringEntities.Where(w => w.BranchId.Equals(uiUserProfileEntity.BranchId)).ToList();
            //}


            //if (sPManifest_MonitoringEntities != null) {
            //    rtn.Status = StatusCodes.Status200OK;
            //    rtn.Data = sPManifest_MonitoringEntities;
            //}

            // new logic
            sPManifest_MonitoringEntities = _repoPs.ManifestRepo.ListManfestMonitoring(dateUtc.Year, dateUtc.Month, dateUtc.Day, (Int32)uiUserProfileEntity.companyId).ToList();

            // from sub branch
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId != null)
            {
                sPManifest_MonitoringEntities = sPManifest_MonitoringEntities.Where(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
                && w.BranchId.Equals(uiUserProfileEntity.BranchId)
                && w.SubBranchId.Equals(uiUserProfileEntity.SubBranchId)).ToList();
            }

            // from branch
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId == null)
            {
                sPManifest_MonitoringEntities = sPManifest_MonitoringEntities.Where(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
                && w.BranchId.Equals(uiUserProfileEntity.BranchId)).ToList();
            }

            // from company
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId == null && uiUserProfileEntity.SubBranchId == null)
            {
                sPManifest_MonitoringEntities = sPManifest_MonitoringEntities.Where(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)).ToList();
            }

            // not company, not branch, not sub branch
            if (uiUserProfileEntity.companyId == null && uiUserProfileEntity.BranchId == null && uiUserProfileEntity.SubBranchId == null)
            {
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }

            if (sPManifest_MonitoringEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = sPManifest_MonitoringEntities;
            }
            else {
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }

            // return

            return rtn;

        }
    }
}

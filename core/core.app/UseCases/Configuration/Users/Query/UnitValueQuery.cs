﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using MediatR;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Configuration.Users.Query
{
    public partial class UnitValueQuery : IRequest<ReturnFormat>
    {
        public Int32? unitId { get; set; }
    }

    public class UnitValueQueryHandler : IRequestHandler<UnitValueQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public UnitValueQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(UnitValueQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.WorkUnitEntitys
                        where a.WorkUnitId.Equals(req.unitId)
                        select new
                        {
                            a.WorkUnitId
                            , a.WorkUnitCode
                            , a.WorkUnitName
                            , a.IsActive
                            , a.Parent
                            , a.Value
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.WorkUnitId
                        , s.WorkUnitCode
                        , s.WorkUnitName
                        , s.IsActive
                        , s.Parent
                        , s.Value
                    }).OrderBy(o => o.WorkUnitId);
            }

            // return
            return rtn;
        }

    }
}

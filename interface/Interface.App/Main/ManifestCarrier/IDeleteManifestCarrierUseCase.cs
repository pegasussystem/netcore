﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main
{

    public interface IDeleteManifestCarrierUseCase : IApp<ManifestCarrierEntity>
    {
        ReturnFormat Delete(UiUserProfileEntity uiUserProfileEntity, Object input);
    }
}

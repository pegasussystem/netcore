﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MasterSubDistrictEntity
    {

        public String MasterSubDistrictId { get; set; }
        public String MasterCityId { get; set; }
        public String MasterSubDistrictCode { get; set; }
        public String MasterSubDistrictName { get; set; }
        public String MasterSubDistrictUrl { get; set; }

    }
}

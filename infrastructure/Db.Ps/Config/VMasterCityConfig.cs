﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VMasterCityConfig : IEntityTypeConfiguration<VMasterCityEntity>
    {
        public void Configure(EntityTypeBuilder<VMasterCityEntity> builder)
        {
            builder.HasKey(e => e.MasterCityId);
            builder.ToTable("VMasterCity");
        }
    }
}

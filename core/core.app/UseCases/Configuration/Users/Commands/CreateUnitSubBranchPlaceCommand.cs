﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Users.Commands
{
    public partial class CreateUnitSubBranchPlaceCommand : IRequest<ReturnFormat>
    {
        public String? cityId { get; set; }
        public String? subDistrictId { get; set; }
        public Int32? unitId { get; set; }
        public Int32? unitBranchId { get; set; }
        public String? unitPlaceCode { get; set; }
        public String? unitPlaceName { get; set; }
        public String? unitPlaceAddress { get; set; }
        public String? timeZone { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateUnitSubBranchPlaceCommandHandler : IRequestHandler<CreateUnitSubBranchPlaceCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateUnitSubBranchPlaceCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateUnitSubBranchPlaceCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.SaveUnitSubBranchPlace(req.cityId, req.subDistrictId, req.unitId, req.unitBranchId, req.unitPlaceCode, req.unitPlaceName, req.unitPlaceAddress, req.timeZone, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_SaveDataUnitSubBranchEntity> SaveUnitSubBranchPlace(String? cityId, String? subDistrictId, Int32? unitId, Int32? unitBranchId, String? unitPlaceCode, String? unitPlaceName, String? unitPlaceAddress, String? timeZone, Int32? loginId)
        {
            String query = $@"EXEC SP_SaveDataUnitSubBranch @cityId='{cityId}', @subDistrictId='{subDistrictId}', @unitId={unitId}, @unitBranchId={unitBranchId}, @unitPlaceCode='{unitPlaceCode}', @unitPlaceName='{unitPlaceName}', @unitPlaceAddress='{unitPlaceAddress}', @timeZone='{timeZone}', @loginId={loginId} ";
            var res = this._context.SP_SaveDataUnitSubBranchEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

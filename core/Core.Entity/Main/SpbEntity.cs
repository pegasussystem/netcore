﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class SpbEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int64 SpbId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? SpbNo { get; set; }
        public Int32? SpbYearMonth { get; set; }
        public Int32? SpbGoodsNo { get; set; }
        public Int32? TableNameId { get; set; }
        public String? TableName { get; set; }



        public Int32? OriginCityCode { get; set; }
        public Int32? OriginAreaCode { get; set; }
        public Int32? DestinationCityCode { get; set; }
        public Int32? DestinationAreaCode { get; set; }
        public String? TypesOfGoods { get; set; }
        public String? Carrier { get; set; }
        public String? QualityOfService { get; set; }
        public String? TypeOfService { get; set; }


        public Decimal? Rates { get; set; }
        public Decimal? Packing { get; set; }
        public Decimal? Quarantine { get; set; }
        public Decimal? Etc { get; set; }
        public Decimal? Ppn { get; set; }
        public Decimal? Discount { get; set; }
        public Decimal? TotalPrice { get; set; }
        public String? PaymentMethod { get; set; }


        public Int32? SenderId { get; set; }
        public Int32? SenderNameId { get; set; }
        public Int32? SenderStoreId { get; set; }
        public Int32? SenderPlaceId { get; set; }
        public Int32? SenderAddressId { get; set; }
        public Int32? ReceiverId { get; set; }
        public Int32? ReceiverNameId { get; set; }
        public Int32? ReceiverStoreId { get; set; }
        public Int32? ReceiverPlaceId { get; set; }
        public Int32? ReceiverAddressId { get; set; }

        public String? Description { get; set; }
        public long? ManifestId { get; set; }
        public Int32? ViaId { get; set; }
        public Boolean? IsVoid { get; set; }
        public DateTime? VoidDate { get; set; }
        public String? VoidBy { get; set; }
        public String? VoidReason { get; set; }

        public Int32? Min { get; set; }
        public Decimal? MinRates { get; set; }
        public Decimal? CawFinal { get; set; }
        public String? RatesType { get; set; }
        public Int32? MinWeight { get; set; }
    }
}

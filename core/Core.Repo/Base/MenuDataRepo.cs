﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Base
{
    public class MenuDataRepo : Repo<MenuDataEntity>, IMenuDataRepo
    {
        public MenuDataRepo(DbContextPs ctx) : base(ctx)
        {
        }
    }
}

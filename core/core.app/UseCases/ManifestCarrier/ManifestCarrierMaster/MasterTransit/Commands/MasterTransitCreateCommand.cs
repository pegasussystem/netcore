﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit.Commands
{
    public partial class MasterTransitCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? originStationId { get; set; }
        public Int32? firstFlightId { get; set; }
        // public Int32? firstFlightCodeId { get; set; }
        public Int32? stationTransitId { get; set; }
        public Int32? lastFlightId { get; set; }
        // public Int32? lastFlightCodeId { get; set; } 
        public Int32? finalDestinationStationId { get; set; }
        public Int32? vendorTransitId { get; set; }
        public Int32? sequenceTransit { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MasterTransitCreateCommandHandler : IRequestHandler<MasterTransitCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public MasterTransitCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(MasterTransitCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {   

                var firstFlightCodeId =  _context.FlightEntitys
                                        .Where(b => b.FlightId == req.firstFlightId)
                                        .Select(c => c.CarrierId).FirstOrDefault();
                var lastFlightCodeId =  _context.FlightEntitys
                                        .Where(b => b.FlightId == req.firstFlightId)
                                        .Select(c => c.CarrierId).FirstOrDefault();
                // Mark as Changed
                var res = this.TransitRouteCreate(
                req.originStationId, 
                req.firstFlightId, 
                firstFlightCodeId,
                req.lastFlightId, 
                lastFlightCodeId,
                req.stationTransitId, 
                req.finalDestinationStationId,
                req.vendorTransitId,
                req.sequenceTransit);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_TransitRouteCreateEntity> TransitRouteCreate(
            Int32? originStationId, 
                Int32? firstFlightId, 
                Int32? firstFlightCodeId,
                Int32? lastFlightId, 
                Int32? lastFlightCodeId,
                Int32? stationTransitId, 
                Int32? finalDestinationStationId,
                Int32? vendorTransitId,
                Int32? sequenceTransit)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_TransitRouteCreate 
            @originStationId = '{ originStationId }', 
            @firstFlightId = '{ firstFlightId }', 
            @firstFlightCodeId = '{ firstFlightCodeId }', 
            @lastFlightId = '{ lastFlightId }',  
            @lastFlightCodeId = '{ lastFlightCodeId }', 
            @vendorTransitId = '{ vendorTransitId }', 
            @sequenceTransit = '{ sequenceTransit }', 
            @stationTransitId = '{ stationTransitId }', 
            @finalDestinationStationId = '{ finalDestinationStationId }'";
            var res = this._context.SP_ManifestCarrierMaster_TransitRouteCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
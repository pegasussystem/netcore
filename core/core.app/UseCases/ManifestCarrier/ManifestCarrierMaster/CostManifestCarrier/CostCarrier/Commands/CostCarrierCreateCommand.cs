﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostCarrier.Commands
{
    public partial class CostCarrierCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? originStationId { get; set; }
        public Int32? destinationStationId { get; set; }
        public Int32? carrierId { get; set; }
        public Int32? vendorCarrierId { get; set; }
        public Int32? firstflightId { get; set; }
        public Int32? lastflightId { get; set; }
        public Int32? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostCarrierCreateCommandHandler : IRequestHandler<CostCarrierCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostCarrierCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostCarrierCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostCarrierCreate(req.originStationId, req.destinationStationId, req.carrierId, req.vendorCarrierId, req.firstflightId, req.lastflightId, req.baseCost);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostCarrierCreateEntity> CostCarrierCreate(Int32? originStationId, Int32? destinationStationId, Int32? carrierId, Int32? vendorCarrierId, Int32? firstflightId, Int32? lastflightId, Int32? baseCost)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostCarrierCreate @originStationId='{originStationId}', @destinationStationId='{destinationStationId}', @carrierId='{carrierId}', @vendorCarrierId='{vendorCarrierId}', @firstflightId='{firstflightId}', @lastflightId='{lastflightId}', @baseCost='{baseCost}'";
            var res = this._context.SP_ManifestCarrierCost_CostCarrierCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
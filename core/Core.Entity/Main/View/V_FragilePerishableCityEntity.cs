﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
   public class V_FragilePerishableCityEntity
    {
        public Int32? CompanyCityId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? CityType { get; set; }
        public String? CityId { get; set; }
        public String? CompanyCityCode { get; set; }
        public String? CompanyCityType { get; set; }
        public String? CompanyCityName { get; set; }
        public String? MasterCityCapital { get; set; }
        public Int32? MasterProvinceId { get; set; }
        public String? MasterProvinceCode { get; set; }
        public String? CompanyCityCodeCustom { get; set; }
        public String? CompanyCityNameCustom { get; set; } 
    }
}

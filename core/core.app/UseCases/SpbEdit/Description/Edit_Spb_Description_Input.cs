﻿using System;
using System.Collections.Generic;
using System.Text;

namespace core.app.UseCases.SpbEdit.Description
{
    public class EditSpbDescriptionInput
    {
        public String SpbId { get; set; }
        public String Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.GoodsMonitoring_Receiver.List;
using core.app.UseCases.Privillege.Queries;
using core.helper;
using Interface.App;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace ui.api.main.UseCases.GoodsMonitoringReceiver.List
{
    ////---------------------------- Sudah tidak dipake di pindahkan ke Controller -> UseCase -> GoodsMonitoring -> Receiver -> ListController -------------------------------//
    //[Authorize]
    //[Route("goods-monitoring-receiver2")]
    //[ApiController]
    //public class GoodsMonitoringReceiverController : DefaultController
    //{
    //    private readonly IAppWrapperPs _appPs;
    //    private readonly IMediator _mediator;
    //    public GoodsMonitoringReceiverController(IAppWrapperPs app, IMediator mediator)
    //    {
    //        _appPs = app;
    //        _mediator = mediator;

    //    }

    //    [HttpGet("list")]
    //    public async Task<IActionResult> Execute([FromQuery] ListGoodsMonitoringReceiverInput input)
    //    {
    //        // --- --- ---
    //        // variable
    //        // --- --- ---
    //        ReturnFormat _returnFormat = new ReturnFormat();

    //        CheckedPrivillegeQuery req = new CheckedPrivillegeQuery();

    //        req.uiUserProfileEntity = GetUserClaim();
    //        req.uiUserProfileEntity.MenuKey = "gmr";
    //        req.uiUserProfileEntity.MenuAction = "list";

    //        // --- --- ---
    //        // logic
    //        // --- --- ---
    //        var response = _mediator.Send(req);
    //        var res = response.Result.Data;

    //        if (res == "none") { _returnFormat   = response.Result; return StatusCode(_returnFormat.Status, _returnFormat); }

    //        input.DataValue = res.ToString();

    //        _returnFormat = _appPs.ListGoodsMonitoringReceiverUseCase.List(GetUserClaim(), input);


    //        // --- --- ---
    //        // return
    //        // --- --- ---
    //        return StatusCode(_returnFormat.Status, _returnFormat);
    //    }

    //}
}

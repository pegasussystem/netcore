﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;


namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorDestination.Command
{
    public partial class ManifestCarrierMasterVendorDestinationDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? VendorDestinationId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterVendorDestinationDeleteCommandHandler : IRequestHandler<ManifestCarrierMasterVendorDestinationDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterVendorDestinationDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterVendorDestinationDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.VendorDestinationDelete(req.VendorDestinationId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_VendorDestinationDeleteEntity> VendorDestinationDelete(Int32? VendorDestinationId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_VendorDestinationDelete @VendorDestinationId='{VendorDestinationId}'";
            var res = this._context.SP_ManifestCarrierMaster_VendorDestinationDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace core.app.UseCases.Spb.Commands.CreateSpb
{


    public partial class GetKoliNoCommand : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public SpbGoodsEntity entity { get; set; }
        public String SpbNo { get; set; }
        public Int64 SpbId { get; set; }
        public String SpbNoManual { get; set; }
        public int destCity { get; set; }
        public int destArea { get; set; }
        public int totalKoli { get; set; }
        public String carrier { get; set; }
        public UiSpbCreateEntity UiSpbCreateEntity { get; set; }
        public Main.RatesObj Rates { get; set; }
    }

    public class GetKoliNoCommandHandler : IRequestHandler<GetKoliNoCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetKoliNoCommandHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetKoliNoCommand request, CancellationToken cancellationToken)
        {
            Random rnd = new Random();
            String data = $" SpbNo : {request.SpbNo} # SpbId: {request.SpbId} # SpbNoManual : {request.SpbNoManual} # carrier {request.carrier} # ";
            String dataGoods = $" aw : {request.entity.Aw} # CAW : {request.entity.Caw} # Height : {request.entity.Height} # Length : {request.entity.Length} # Width : {request.entity.Width}";

            String processId = rnd.Next(1, 999999999).ToString() + " _Fn_GetKoliNo_ ";
            _log.LogDebug(processId, ">>> >>> >>> GetKoliNo");
            _log.LogDebug(processId, $"[DATA] {data}");
            _log.LogDebug(processId, $"[dataGoods] {dataGoods}");
            //
            // -- variable & set variable
            int modaDarat = 4000; // TODO : hardcode
            int modaUdara = 6000; // TODO : hardcode
            Decimal caw = 0;
            ReturnFormat rtn = new ReturnFormat();
            SpbEntity spbEntity;
            SPManifestNumberGeneratorEntity sPManifestNumberGeneratorEntity;
            rtn.Status = StatusCodes.Status400BadRequest;

            // -- validation
            // --- --- --- --- ---

            // -- check manual with SpbNo
            _log.LogDebug(processId, "check manual with SpbNo");
            if (request.SpbNo != request.SpbNoManual)
            {
                rtn.Data = "Terjadi kesalahan. Harap Refresh browser Anda..";
                rtn.Status = StatusCodes.Status400BadRequest;
                return rtn;
            }

            // -- check spb. spb id should be equal with SpbId
            _log.LogDebug(processId, ">>> spbEntity");
            spbEntity = _repoPs.SpbRepo.Read(w => w.SpbNo.Equals(request.SpbNo)).First();
            _log.LogDebug(processId, "<<< spbEntity");
            if (spbEntity.SpbId != request.SpbId)
            {
                _log.LogDebug(processId, "ERROR - spbEntity.SpbId != SpbId");
                rtn.Data = "Terjadi kesalahan. Harap Refresh browser Anda..";
                rtn.Status = StatusCodes.Status400BadRequest;
                return rtn;
            }
            if (spbEntity.PaymentMethod != null)
            {
                _log.LogDebug(processId, "ERROR - spbEntity.PaymentMethod != null");
                rtn.Data = "Terjadi kesalahan. Harap Refresh browser Anda..";
                rtn.Status = StatusCodes.Status400BadRequest;
                return rtn;
            }


            // return if spbEntity is null
            if (spbEntity == null)
            {
                _log.LogDebug(processId, "ERROR - spbEntity == null");
                return rtn;
            }
            request.entity.SpbId = spbEntity.SpbId;
            _log.LogDebug(processId, $"spbEntity.SpbId : {spbEntity.SpbId} # ");


            // calculate volume 
            // (caw)
            request.entity.Vw = (request.entity.Length * request.entity.Width * request.entity.Height) / 4000;
            if (request.carrier == "air") { request.entity.Vw = (request.entity.Length * request.entity.Width * request.entity.Height) / modaUdara; }
            if (request.carrier == "truck") { request.entity.Vw = (request.entity.Length * request.entity.Width * request.entity.Height) / modaDarat; }

            //request.entity.Vw = Math.Round(request.entity.Vw);  Pati 28 jan 22

            //if flat
            if (request.entity.Aw > request.entity.Vw) { caw = request.entity.Aw; }
            if (request.entity.Aw <= request.entity.Vw) { caw = request.entity.Vw; }
            //request.entity.Caw = Math.Round(caw);  Pati 18Jan22
            decimal setengah = 0.5m; decimal roundCaw = Convert.ToDecimal(Math.Round(caw));

            //21Mrt22 PEMBULATAN UTK JABAR-JATENG  truck_jabar  truck_jateng  0,3=>0   0,4=>1  Math.round(0,49) = 0   Math.round(0,5)=1
            decimal nolkomatiga = 0.3m; decimal nolkomasatu = 0.1m;
            if (request.carrier == "truck_jabar" || request.carrier == "truck_jateng")
            {
                 if (roundCaw < caw)
                {
                    if ((roundCaw + nolkomatiga) >= caw)
                    {
                        request.entity.Caw = roundCaw;
                    }
                    else
                    {
                        decimal pluskomasatu = caw + nolkomasatu;
                        request.entity.Caw = Convert.ToDecimal(Math.Round(pluskomasatu,MidpointRounding.AwayFromZero)); //caw + 0.1 lalu di Round
                    }
                }
                else
                {
                    request.entity.Caw = roundCaw;
                }
            }
            else
            {
                if (roundCaw < caw)
                {
                    request.entity.Caw = roundCaw + setengah;
                }
                else
                {
                    if (roundCaw - setengah == caw)
                    {
                        request.entity.Caw = caw;
                    }
                    else
                    {
                        request.entity.Caw = roundCaw;
                    }
                }
            }

            request.entity.TotalKoli = request.totalKoli;

            // SET CAW FOR MINIMUM KG
            // --- --- --- --- ---

            // get rule pricing
            // if flatmin THEN get min KG 
            // jika lebih kecil dari perhitungan caw diatas // (caw), maka kenakan minimum KG

            if (request.Rates.RatesType == "flatmin" || request.Rates.RatesType == "flatminspb") {
                var ratesDetail = request.Rates.Detail.Where(w=> request.entity.Caw >= w.Min && w.Min <= w.Max).FirstOrDefault();

                if (ratesDetail != null && request.Rates.RatesType == "flatmin") {
                    //request.entity.Caw = Math.Round(caw);
                    request.entity.Caw = Convert.ToDecimal(ratesDetail.Max) > request.entity.Caw ? Convert.ToDecimal(ratesDetail.Max) : request.entity.Caw;
                }
            }


            try
            {
                // update location spb
                if (request.destCity != 0 && request.destArea != 0 && spbEntity.OriginAreaCode == null)
                {
                    _log.LogDebug(processId, ">>> update location spb");
                    spbEntity.OriginCityCode = request.uiUserProfileEntity.workCityId;
                    spbEntity.OriginAreaCode = request.uiUserProfileEntity.WorkAreaId;
                    spbEntity.DestinationCityCode = request.destCity;
                    spbEntity.DestinationAreaCode = request.destArea;
                    _repoPs.SpbRepo.Update(spbEntity);
                    _repoPs.SpbRepo.Save();
                    _log.LogDebug(processId, "<<< update location spb");
                }

                //save Caw Final
                //request.entity.CawFinal = request.entity.Caw;
                request.entity.CawFinal = request.entity.Caw * request.entity.TotalKoli;  // by Pati 10mrt22

                // save to SpbGoodsRepo
                _log.LogDebug(processId, ">>> save to SpbGoodsRepo");
                var createData = _repoPs.SpbGoodsRepo.Create(request.entity);
                _repoPs.SpbGoodsRepo.Save();
                _log.LogDebug(processId, $"SpbGoodsId : {createData.SpbGoodsId}");
                _log.LogDebug(processId, "<<< save to SpbGoodsRepo");

                // get koli number
                sPManifestNumberGeneratorEntity = null;
                var nomorKoli = "";

                for (int i = 0; i < request.totalKoli; i++)
                {
                    _log.LogDebug(processId, ">>>  get koli number");
                    sPManifestNumberGeneratorEntity = _repoPs.ManifestRepo
                        .GetKoliNo(request.uiUserProfileEntity, request.entity, request.carrier).First(); // TODO : HARDCODE

                    while (sPManifestNumberGeneratorEntity.KoliNo == null)
                    {
                        sPManifestNumberGeneratorEntity = _repoPs.ManifestRepo.GetKoliNo(request.uiUserProfileEntity, request.entity, request.carrier).First();

                        var dataItem = _context.ManifestKoliSpbEntitys.Where(w => w.SpbGoodsId.Equals(request.entity.SpbGoodsId) && w.ManifestKoliId.Equals(null)).FirstOrDefault();

                        _context.ManifestKoliSpbEntitys.Remove(dataItem);
                        _context.SaveChanges();
                    }

                    _log.LogDebug(processId, $"KoliNo : {sPManifestNumberGeneratorEntity.KoliNo}");
                    _log.LogDebug(processId, "<<<  get koli number");

                    if (i == 0) { nomorKoli = sPManifestNumberGeneratorEntity.KoliNo.ToString(); }
                    if (i != 0)
                    {
                        nomorKoli = nomorKoli + ", " + sPManifestNumberGeneratorEntity.KoliNo.ToString();
                    }
                }

                    if (sPManifestNumberGeneratorEntity != null)
                    {
                        rtn.Data = nomorKoli;
                        rtn.Status = StatusCodes.Status200OK;
                    }

                //transaction.Complete();

            }

            catch (TransactionAbortedException ex)
            {
                //transaction.Dispose();
            }
            catch (Exception ex)
            {
                _log.LogDebug(processId, $"ERROR Exception ### {ex}");
                //transaction.Dispose();
            }
            //}


            _log.LogDebug(processId, "<<< <<< <<< GetKoliNo");
            // return
            return rtn;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MenuActionEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? MenuActionId { get; set; }
        public Int32? MenuId { get; set; }
        public String? MenuAction { get; set; }
        public String? MenuActionName { get; set; }
    }
}

﻿using core.app.UseCases.Base;
using core.app.UseCases.HistoryAksi;
using core.app.UseCases.HistoryAksi.Queries;
using core.app.UseCases.Privillege.Queries;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Base
{
    [Authorize]
    [Route("history-aksi")]
    [ApiController]
    public class HistoryActionController : ApiController
    {
        [HttpGet("list")]
        public async Task<IActionResult> List([FromQuery] HistoryAksiQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}


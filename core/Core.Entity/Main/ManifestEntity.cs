﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class ManifestEntity
    {

        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedBy { get; set; }
        public DateTime? UpdatedBy { get; set; }
        public long? ManifestId { get; set; }
        public Guid? InsertId { get; set; }
        public Int32? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? SubBranchId { get; set; }
        public Int32? ManifestAlphabet { get; set; }
        public String? ManifestNo { get; set; }
        public Int32? ManifestIncrement { get; set; }
        public Int32? TableNameId { get; set; }
        public String? TableName { get; set; }
        public String? Carrier { get; set; }
        public Int32? OriginCityCode { get; set; }
        public Int32? DestinationCityCode { get; set; }
        public long? ManifestGroupId { get; set; }
        public Boolean? IsOpen { get; set; }
    }
}

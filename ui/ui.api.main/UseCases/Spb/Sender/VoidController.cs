﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.Spb.Commands.Void;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Spb.Sender
{
    [Authorize]
    [Route("spblistsender")]
    [ApiController]
    public class VoidController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public VoidController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("void")]
        public async Task<IActionResult> UpdateDataVoid([FromBody] RatesCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();


            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

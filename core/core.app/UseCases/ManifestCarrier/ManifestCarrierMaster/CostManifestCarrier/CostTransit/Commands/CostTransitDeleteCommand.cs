﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostTransit.Commands
{
    public partial class CostTransitDeleteCommand : IRequest<ReturnFormat>
    {
        public String? costTransitId { get; set; }
        public String? costTransitDetailId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostTransitDeleteCommandHandler : IRequestHandler<CostTransitDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostTransitDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostTransitDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostTransitDelete(req.costTransitId, req.costTransitDetailId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostTransitDeleteEntity> CostTransitDelete(String? costTransitId, String? costTransitDetailId)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostTransitDelete @costTransitId='{costTransitId}', @costTransitDetailId='{costTransitDetailId}'";
            var res = this._context.SP_ManifestCarrierCost_CostTransitDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
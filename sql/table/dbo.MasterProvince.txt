/****** Object:  Table [dbo].[MasterProvince]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterProvince](
	[MasterProvinceId] [varchar](5) NOT NULL,
	[MasterProvinceCode] [varchar](50) NULL,
	[MasterProvinceName] [varchar](50) NULL,
 CONSTRAINT [PK_MasterProvinceNew] PRIMARY KEY CLUSTERED 
(
	[MasterProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

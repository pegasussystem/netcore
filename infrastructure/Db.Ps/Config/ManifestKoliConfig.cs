﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class ManifestKoliConfig : IEntityTypeConfiguration<ManifestKoliEntity>
    {
        public void Configure(EntityTypeBuilder<ManifestKoliEntity> builder)
        {
            builder.HasKey(e => e.ManifestKoliId);
            builder.ToTable("ManifestKoli");

        }
    }
}

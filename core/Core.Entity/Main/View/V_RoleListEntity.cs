﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_RoleListEntity
    {
        public Int32? RolesId { get; set; }
        public String? RolesName { get; set; }
        public String? Description { get; set; }
        public Int32? CountUser { get; set; }
    }
}

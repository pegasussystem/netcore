﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class SP_SpbCreate_ListSubDistrictEntity
    {
        public Int32? CompanyAreaId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? MasterCityId { get; set; }
        public String? MasterSubDistrictId { get; set; }
        public String? CompanyAreaCode { get; set; }
        public String? CompanyAreaName { get; set; }
        public String? CompanyAreaNameCustom { get; set; }
        public String? ParentId { get; set; }
        public Boolean? IsVia { get; set; }
    }
}

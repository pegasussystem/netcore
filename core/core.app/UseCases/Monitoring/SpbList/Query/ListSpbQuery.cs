﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using core.app.Common.Interfaces;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Monitoring.SpbList.Query
{
    public partial class ListSpbQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public String? Type { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class ListSpbQueryHandler : IRequestHandler<ListSpbQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListSpbQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListSpbQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_SPBListEntity> entities;

            DateTime utcDate = req.CreatedAt.Value.AddHours((double)(-1 * req.uiUserProfile.WorkTimeZoneHour)).AddMinutes((double)(req.uiUserProfile.WorkTimeZoneMinute * -1));

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataSpbList(req.uiUserProfile.userId, req.Type, utcDate.Day, utcDate.Month, utcDate.Year);

            if (entities != null)
            {
               rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        s.CreatedAt
                        , CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("dddd, dd MMMM yyyy")
                        , s.CompanyId
                        , s.BranchId
                        , s.SubBranchId
                        , s.Username
                        , s.SpbId
                        , s.SpbNo
                        , s.TypesOfGoods
                        , s.TypesOfGoodsName
                        , s.Carrier
                        , s.CarrierName
                        , s.QualityOfService
                        , s.QualityOfServiceName
                        , s.TypeOfService
                        , s.TypeOfServiceName
                        , s.Rates
                        , s.Packing
                        , s.Quarantine
                        , s.Etc
                        , s.Ppn
                        , s.Discount
                        , s.TotalPrice
                        , s.PaymentMethod
                        , s.Description
                        , s.OriginCityId
                        , s.OriginCityCode
                        , s.OriginCityNameCustom
                        , s.OriginAreaId
                        , s.OriginAreaCode
                        , s.OriginAreaNameCustom
                        , s.DestinationCityId
                        , s.DestinationCityCode
                        , s.DestinationCityNameCustom
                        , s.DestinationAreaId
                        , s.DestinationAreaCode
                        , s.DestinationAreaNameCustom
                        , s.SenderPhone
                        , s.SenderAddress
                        , s.SenderName
                        , s.SenderPlace
                        , s.SenderStore
                        , s.ReceiverPhone
                        , s.ReceiverAddress
                        , s.ReceiverName
                        , s.ReceiverPlace
                        , s.ReceiverStore
                        , s.ViaName
                        , s.IsVoid
                        , s.VoidDate
                        , s.VoidBy
                        , s.VoidReason
                    }).OrderByDescending(s => s.CreatedAt);
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_SPBListEntity> DataSpbList(Int32? UserId, String? Type, Int32? Day, Int32? Month, Int32? Year)
        {
            String query = $@"EXEC SP_SPBList @userId = '{UserId}', @type = '{Type}', @day='{Day}', @month = '{Month}', @year = '{Year}' ";
            var res = this._context.SP_SPBListEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
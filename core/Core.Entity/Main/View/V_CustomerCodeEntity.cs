﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_CustomerCodeEntity
    {
        public Int32? CustomerHeaderId { get; set; }
        public Int32? CustomerId { get; set; }
        public String? Area { get; set; }
        public String? CityCode { get; set; }
        public String? CityCodeName { get; set; }
        public String? Phone { get; set; }
        public String? Type { get; set; }
        public Int32? CustomerNameId { get; set; }
        public String? CustomerName { get; set; }
        public Int32? CustomerStoreId { get; set; }
        public String? CustomerStore { get; set; }
        public Int32? CustomerPlaceId { get; set; }
        public String? CustomerPlace { get; set; }
        public Int32? CustomerAddressId { get; set; }
        public String? CustomerAddress { get; set; }
        public Int32? Rt { get; set; }
        public Int32? Rw { get; set; }
        public String? Urban { get; set; }
        public String? MasterUrbanName { get; set; }
        public String? SubDistrict { get; set; }
        public String? MasterSubDistrictCode { get; set; }
        public String? City { get; set; }
        public String? CityCodeName2 { get; set; }
        public String? PostalCode { get; set; }
        public String? MasterUrbanPostalCode { get; set; }
        public String? CustomerType { get; set; }
        public String? CustomerCode { get; set; }
    }
}

﻿using FluentValidation.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Validator
{
    class Language : LanguageManager
    {
        public Language()
        {
            AddTranslation("id", "NotNullValidator", "'{PropertyName}' wajib terisi.");
        }
    }
}

using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;

namespace core.app.UseCases.Master.CoaCode.Queries

{

    public partial class ListCoaCityRevenueLiabilityQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class ListCoaCityRevenueLiabilityQueryHandler : IRequestHandler<ListCoaCityRevenueLiabilityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCoaCityRevenueLiabilityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCoaCityRevenueLiabilityQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;



            var query = from a in _context.CoaCityRevenueLiabilityEntities
                        join b in _context.CoaCodeEntities
                            on (long)a.CoaIdRevenue equals b.CoaId // CoaIdRevenue di-cast menjadi long
                        join c in _context.CoaCodeEntities
                            on (long)a.CoaIdLiability equals c.CoaId // CoaIdLiability di-cast menjadi long
                        join d in _context.MasterCityEntitys
                            on a.CityId equals d.MasterCityId
                        join e in _context.CoaCodeEntities
                            on (long)a.CoaIdAR equals e.CoaId
                        select new
                        {
                            CoaRevenueName = b.CoaName ?? "-",
                            CoaRevenueCode = b.CoaCode ?? "-",
                            CoaLiabilityName = c.CoaName ?? "-",
                            CoaLiabilityCode = c.CoaCode ?? "-",
                            Moda = a.CarrierType == "air" ? "UDARA" :
                                   a.CarrierType == "sea" ? "LAUT" : "DARAT",
                            Desc = a.desc ?? "-",
                            Branch = a.IsKk1.HasValue ? "Pusat" : "Cabang",
                            a.Id,
                            a.CityId,
                            MasterCityName = "[ " + d.MasterCityCode + " ] " + d.MasterCityName,
                            CoaArName = e.CoaName ?? "-",
                            CoaArCode = e.CoaCode ?? "-"
                        };


            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.Id,
                        s.CoaRevenueName,
                        s.CoaArName,
                        s.CoaArCode,
                        s.CoaLiabilityName,
                        s.CoaRevenueCode,
                        s.Moda,
                        s.CoaLiabilityCode,
                        s.Desc,
                        s.CityId,
                        s.MasterCityName


                    }).OrderBy(o => o.Id);
            }

            // return 
            return rtn;
        }


    }

}

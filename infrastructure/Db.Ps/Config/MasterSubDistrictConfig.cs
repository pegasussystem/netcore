﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class MasterSubDistrictConfig : IEntityTypeConfiguration<MasterSubDistrictEntity>
    {
        public void Configure(EntityTypeBuilder<MasterSubDistrictEntity> builder)
        {
            builder.HasKey(e => e.MasterSubDistrictId);
            builder.ToTable("MasterSubDistrict");


        }
    }
}

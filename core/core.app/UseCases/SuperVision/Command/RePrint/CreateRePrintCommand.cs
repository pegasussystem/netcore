﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.SuperVision.Command.RePrint
{
   public partial class CreateRePrintCommand : IRequest<ReturnFormat>
    {
        public String? docNo { get; set; }
        public String? typePrint { get; set; }
        public String? reason { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateRePrintCommandHandler : IRequestHandler<CreateRePrintCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateRePrintCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateRePrintCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                List<SP_SuperVision_GetDocIdEntity> res = this.GetDocId(req.docNo, req.typePrint).ToList();

                if (res[0].result == "true")
                {
                    SuperVisionEntity entity = new SuperVisionEntity();

                    entity.SpbId            = Int32.Parse(res[0].message);
                    entity.Type             = "reprint";
                    entity.Reason           = req.reason;
                    entity.Status           = 0;
                    entity.Param1           = res[0].message;
                    entity.Param2           = req.typePrint;
                    entity.Param3           = req.uiUserProfile.userId.ToString();
                    entity.PicCreatedBy     = req.uiUserProfile.userId;
                    entity.PicCreatedDate   = DateTime.UtcNow;
                    entity.CreatedBy        = req.uiUserProfile.userId.ToString();
                    entity.CreatedAt        = DateTime.UtcNow;

                    _context.SuperVisionEntitys.Add(entity);
                    _context.SaveChanges();

                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = "Data sudah berhasil di ajukan";
                }
                else
                {
                    rtn.Status = StatusCodes.Status203NonAuthoritative;
                    rtn.Data = res;
                }

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_SuperVision_GetDocIdEntity> GetDocId(String? docNo, String? typePrint)
        {
            String query = $@"EXEC SP_SuperVision_GetDocId @docNo='{docNo}', @typePrint='{typePrint}' ";
            var res = this._context.SP_SuperVision_GetDocIdEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

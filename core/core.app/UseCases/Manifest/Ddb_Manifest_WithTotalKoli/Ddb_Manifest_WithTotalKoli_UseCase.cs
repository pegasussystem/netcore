﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Main.Manifest;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace core.app.UseCases.Manifest.Ddb_Manifest_WithTotalKoli
{
    class Ddb_Manifest_WithTotalKoli_UseCase : IDdb_Manifest_WithTotalKoli_UseCase
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public Ddb_Manifest_WithTotalKoli_UseCase(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Read(UiUserProfileEntity uiUserProfileEntity, Object req)
        {
            // variable
            // ///// ///// /////
            Ddb_Manifest_WithTotalKoli_Input input = req as Ddb_Manifest_WithTotalKoli_Input;

            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // logic
            // ///// ///// /////

            // get manifest
            ManifestEntity manifest = _repoPs.ManifestRepo.Read(w => w.ManifestId.Equals(input.manifestId)).FirstOrDefault();

            // get manifest with koli
            IEnumerable<VManifestWithKoliEntity> entities = _repoPs.ManifestRepo.VManifest_Ddb_WithTotalKoli(uiUserProfileEntity, manifest);

            // return
            // ///// ///// /////
            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities;
            }
            return rtn;
        }

        public ReturnFormat Create(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Delete(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

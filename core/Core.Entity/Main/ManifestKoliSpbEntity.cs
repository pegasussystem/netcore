﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Core.Entity.Main
{
    public class ManifestKoliSpbEntity
    {

        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }

        public Int64? ManifestKoliSpbId { get; set; }
        public Int64? ManifestKoliId { get; set; }
        public Int64? SpbGoodsId { get; set; }
    }
}

using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;

namespace core.app.UseCases.Master.CoaCode.Queries

{

    public partial class ListCoaParentQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
    }
    public class ListCoaParentHandler : IRequestHandler<ListCoaParentQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCoaParentHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCoaParentQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;



            var query = from a in _context.CoaParentEntities

                        select new
                        {
                            a.CoaParentId,
                            a.CoaParentName,
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CoaParentId,
                        s.CoaParentName

                    }).OrderBy(o => o.CoaParentId);
            }

            // return 
            return rtn;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_HistoryNotPrintSpbEntity
    {
        public Int64? SpbId { get; set; }
        public String? SpbNo { get; set; }
        public DateTime? CreatedAt { get; set; }
        public Int32? CreatedBy { get; set; }
        public String? TypesOfGoods { get; set; }
        public String? Carrier { get; set; }
        public String? QualityOfService { get; set; }
        public String? TypeOfService { get; set; }
        public String? PaymentMethod { get; set; }
    }
}

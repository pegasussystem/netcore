using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.BukuKas.Queries
{
    public partial class ListBukuKasQuery : IRequest<ReturnFormat>
    {
        public String? period { get; set; }
        public String? date { get; set; }
        public String? paramStartDate { get; set; }
        public String? paramEndDate { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListBukuKasQueryHandler : IRequestHandler<ListBukuKasQuery, ReturnFormat>
    {

        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListBukuKasQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListBukuKasQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;


            var currentDate = DateTime.Now;

            var previousMonthDate = currentDate.AddMonths(-1);

            var firtDate = "02 " + currentDate.ToString("MMM yyyy");
            var firstKeterangan = "Saldo awal bulan " + currentDate.ToString("MMMM yyyy");

            // Ambil saldo cumulative balance terakhir dari bulan sebelumnya
            var previousMonthSaldoAwal = _context.VBukuKasEntities
                .Where(a => a.createdAt.Year == currentDate.Year
                           && a.createdAt.Month == currentDate.Month
                && a.createdAt.Day == 1)
                .OrderByDescending(a => a.createdAt)
                .Select(a => a.cumulativeBalance)
                .FirstOrDefault(); // ambil cumulative balance

            var query = from a in _context.VBukuKasEntities
                        where (currentDate.Day == 1 &&
                 a.createdAt.Year == currentDate.AddMonths(-1).Year &&
                 a.createdAt.Month == currentDate.AddMonths(-1).Month &&
                 a.createdAt.Day >= 2)
                ||
                // Jika sudah tanggal 2 atau lebih, ambil data bulan ini mulai tanggal 2
                (currentDate.Day > 1 &&
                 a.createdAt.Year == currentDate.Year &&
                 a.createdAt.Month == currentDate.Month &&
                 a.createdAt.Day >= 2)
                        select new
                        {
                            a.bukuKasId,
                            a.createdAt,
                            a.coaCode,
                            a.coaName,
                            a.keterangan,
                            a.debit,
                            a.credit,
                            a.saldo,
                            a.cumulativeBalance,
                            a.coaId,
                            a.tanggal,
                        };



            if (!string.IsNullOrEmpty(req.period) && !string.IsNullOrEmpty(req.date))
            {
                DateTime targetDate = DateTime.ParseExact(req.date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                firtDate = targetDate.ToString("dd MMMM yyyy");
                var previousDayDate = targetDate.AddDays(-1);
                var previousMotnhTargetDate = targetDate.AddMonths(-1);
                var previousYearDate = targetDate.AddYears(-1);
                if (req.period == "PerDate" || req.period == "Today")
                {

                    if (targetDate.Day == 1)
                    {
                        previousMonthSaldoAwal = _context.VBukuKasEntities
                        .Where(a => a.createdAt.Year == targetDate.Year && a.createdAt.Month == previousMotnhTargetDate.Month)
                        .OrderByDescending(a => a.createdAt)
                        .Select(a => a.cumulativeBalance)
                        .FirstOrDefault();
                        firstKeterangan = "Saldo Awal Tanggal " + firtDate;
                    }
                    else
                    {
                        previousMonthSaldoAwal = _context.VBukuKasEntities
                       .Where(a => a.createdAt.Year == targetDate.Year && a.createdAt.Month == targetDate.Month && a.createdAt.Day == previousDayDate.Day)
                       .OrderByDescending(a => a.createdAt)
                       .Select(a => a.cumulativeBalance)
                       .FirstOrDefault();
                        firstKeterangan = "Saldo Awal Tanggal " + firtDate;
                    }
                    query = query.Where(a => a.createdAt.Date == targetDate.Date);
                }
                else if (req.period == "PerMonth")
                {
                    query = query.Where(a => a.createdAt.Year == targetDate.Year && a.createdAt.Month == targetDate.Month);
                }
                else if (req.period == "PerYear")
                {
                    previousMonthSaldoAwal = _context.VBukuKasEntities
              .Where(a => a.createdAt.Year == previousYearDate.Year)
              .OrderByDescending(a => a.createdAt)
              .Select(a => a.cumulativeBalance)
              .FirstOrDefault();
                    firstKeterangan = "Saldo Awal Tahun " + targetDate.Year;
                    query = query.Where(a => a.createdAt.Year == targetDate.Year);
                }
                else
                {
                    throw new ArgumentException("Invalid period format. Please use 'PerDate', 'PerMonth', or 'PerYear'.");
                }
            }

            if (!string.IsNullOrEmpty(req.paramEndDate) && !string.IsNullOrEmpty(req.paramStartDate))
            {
                DateTime startDate = DateTime.ParseExact(req.paramStartDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                DateTime endDate = DateTime.ParseExact(req.paramEndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                var previousDayDate = startDate.AddDays(-1);
                var previousMotnhTargetDate = startDate.AddMonths(-1);

                if (startDate.Day == 1)
                {
                    previousMonthSaldoAwal = _context.VBukuKasEntities
                    .Where(a => a.createdAt.Year == startDate.Year && a.createdAt.Month == previousMotnhTargetDate.Month)
                    .OrderByDescending(a => a.createdAt)
                    .Select(a => a.cumulativeBalance)
                    .FirstOrDefault();
                    firstKeterangan = "Saldo Awal Tanggal " + firtDate;
                }
                else
                {
                    previousMonthSaldoAwal = _context.VBukuKasEntities
                   .Where(a => a.createdAt.Year == startDate.Year && a.createdAt.Month == startDate.Month && a.createdAt.Day == previousDayDate.Day)
                   .OrderByDescending(a => a.createdAt)
                   .Select(a => a.cumulativeBalance)
                   .FirstOrDefault();
                    firstKeterangan = "Saldo Awal Tanggal " + firtDate;
                }


                query = query.Where(a => a.createdAt.Date >= startDate.Date && a.createdAt.Date <= endDate.Date);
            }




            if (query != null)
            {
                // rtn.Status = StatusCodes.Status200OK;
                // rtn.Data = query.Select(
                //     s => new
                //     {
                //         s.bukuKasId,
                //         s.tanggal,
                //         s.createdAt,
                //         s.coaCode,
                //         s.coaName,
                //         s.keterangan,
                //         s.debit,
                //         s.credit,
                //         s.saldo,
                //         s.cumulativeBalance,
                //         s.coaId,

                //     }).OrderBy(o => o.createdAt);


                // Ubah status menjadi OK jika ada data
                rtn.Status = StatusCodes.Status200OK;

                // Ambil data dari bulan berjalan dan urutkan berdasarkan createdAt

                var saldoAwal = new
                {
                    bukuKasId = (long?)null,
                    tanggal = firtDate,
                    createdAt = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day),
                    coaCode = "",
                    coaName = "",
                    keterangan = firstKeterangan,
                    debit = (decimal?)0,
                    credit = (decimal?)0,
                    saldo = (decimal?)0,
                    cumulativeBalance = previousMonthSaldoAwal,
                    coaId = (long?)null,
                };

                var queryResult = query.Select(s => new
                {
                    s.bukuKasId,
                    s.tanggal,
                    s.createdAt,
                    s.coaCode,
                    s.coaName,
                    s.keterangan,
                    s.debit,
                    s.credit,
                    s.saldo,
                    s.cumulativeBalance,
                    s.coaId
                })
.OrderBy(o => o.createdAt)
.ToList();
                queryResult.Insert(0, saldoAwal); // Masukkan saldo awal ke posisi pertama
                rtn.Data = queryResult;
            }


            // return 
            return rtn;
        }
    }
}

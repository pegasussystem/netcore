﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class V_FragilePerishableCityConfig : IEntityTypeConfiguration<V_FragilePerishableCityEntity>
    {
        public void Configure(EntityTypeBuilder<V_FragilePerishableCityEntity> builder)
        {
            builder.HasKey(e => e.CompanyCityId);
            builder.ToTable("V_FragilePerishableCity");
        }
    }
}

﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_CustomerCodeDataConfig : IEntityTypeConfiguration<V_CustomerCodeDataEntity>
    {
        public void Configure(EntityTypeBuilder<V_CustomerCodeDataEntity> builder)
        {
            builder.HasKey(e => e.CustomerHeaderId);
            builder.ToTable("V_CustomerCodeData");
        }
    }
}

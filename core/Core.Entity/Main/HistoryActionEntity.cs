﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
      public class HistoryActionEntity
    {
        public Int32? HistoryActionId { get; set; }
        public Int32? UserId { get; set; }
        public DateTime? ActionDate { get; set; }
        public Int32? ActionId { get; set; }
        public String? ActionDesc { get; set; }
        public String? Menu { get; set; }
        public String? Desc { get; set; }
    }
}

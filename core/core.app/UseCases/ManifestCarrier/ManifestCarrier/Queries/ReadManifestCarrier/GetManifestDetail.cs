﻿using core.app.Common.Interfaces;
using Core.Entity.Ui;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Entity.Main.Store_Procedure;

namespace core.app.UseCases.ManifestCarrier.Queries
{
    public partial class GetManifestCarrirDetailQuery : IRequest<ReturnFormat>
    {
        public Int64? ManifestId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class GetManifestCarrirDetailQueryHandler : IRequestHandler<GetManifestCarrirDetailQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetManifestCarrirDetailQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetManifestCarrirDetailQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;




            try
            {
                Int32? cntData = this.manifestCarrierCount(req.ManifestId);

                if (cntData > 0)
                {


                    IEnumerable<SP_GetManifestCarrierListManifestEntity> entities = this.SP_GetManifestCarrierListManifest(req.ManifestId);

                    if (entities != null)
                    {
                        rtn.Status = StatusCodes.Status200OK;
                        rtn.Data = entities.Select(
                            s => new
                            {
                                s.NoSmu
                                , s.CreatedAt
                                , FlightDate = Convert.ToDateTime(s.FlightDate).ToString("yyyy-MM-dd h:mm tt")
                                , s.TotalFinalWeightSMU
                                , s.FirstFlightNo
                                , s.LastFlightNo
                                , cntData = cntData
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            //
            // return
            return rtn;
        }

        public Int32? manifestCarrierCount(Int64? manifestId)
        {
            String query = $@"EXEC SP_GetManifestCarrierCountManifest @manifestId='{manifestId}'";
            var res = this._context.SP_GetManifestCasrrierCountManifestEntitys.FromSqlRaw(query).ToList();
            return res[0].cntManifest;
        }

        public IEnumerable<SP_GetManifestCarrierListManifestEntity> SP_GetManifestCarrierListManifest(Int64? manifestId)
        {
            String query = $@"EXEC SP_GetManifestCarrierListManifest @manifestId='{manifestId}'";
            var res = this._context.SP_GetManifestCarrierListManifestEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

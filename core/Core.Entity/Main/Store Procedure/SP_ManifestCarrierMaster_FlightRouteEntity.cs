﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_ManifestCarrierMaster_FlightRouteEntity
    {
        public Int32? FlightRouteId { get; set; }
        public String? CarrierName { get; set; }
        public String? VendorCarrierName { get; set; }
        public String? OriginStationName { get; set; }
        public String? FirstFlightNo { get; set; }
        public String? SecondFlightNo { get; set; }
        public String? ThirdFlightNo { get; set; }
        public String? FourthFlightNo { get; set; }
        public String? DestinationStationName { get; set; }
    }
}

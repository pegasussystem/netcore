﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Base;
using Interface.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ui.api.main.Controllers.Base
{
    [Route("[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IAppWrapperPs _appPs;
        public MenuController(IAppWrapperPs app)
        {
            _appPs = app;

        }


        [HttpGet("get")]
        public async Task<IActionResult> Get(bool rawMode = false)
        {

            //
            // variable
            List<MenuEntity> _menuEntitys = new List<MenuEntity>();
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // get data
            _returnFormat = _appPs.MenuApp.Read();

            //
            // return
            if (rawMode) { return StatusCode(_returnFormat.Status, _returnFormat.Data); }
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("get-tree")]
        public async Task<IActionResult> GetTree(bool rawMode = false)
        {

            //
            // variable
            List<MenuEntity> _menuEntitys = new List<MenuEntity>();
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // get data
            _returnFormat = _appPs.MenuApp.ReadTree();

            //
            // return
            if (rawMode) { return StatusCode(_returnFormat.Status, _returnFormat.Data); }
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> Delete([FromForm] int key)
        {
            //
            // variable
            MenuEntity _menuEntity = new MenuEntity();

            //
            // set
            _menuEntity.MenuId = key;

            //
            // validation
            // TODO:  tidak bisa di hapus jika punya anak 

            //
            // get data
            _menuEntity = _appPs.MenuApp.Delete(_menuEntity);

            //
            // return
            return Ok(_menuEntity);
        }


        [HttpPost("post")]
        public async Task<IActionResult> Post([FromForm] String values)
        {
            //
            // variable
            MenuEntity _menuEntity = new MenuEntity();
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // set
            JsonConvert.PopulateObject(values, _menuEntity);
            _menuEntity.IsPage = (_menuEntity.IsPage == null) ? false : _menuEntity.IsPage;
            _menuEntity.MenuIdParent = (_menuEntity.MenuIdParent == null) ? 0 : _menuEntity.MenuIdParent;

            //
            // get data
            //_returnFormat = _appPs.MenuApp.Create(_menuEntity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPut("put")]
        public async Task<IActionResult> Put([FromForm] int key, [FromForm] String values)
        {
            //
            // variable
            MenuEntity _menuEntity = new MenuEntity();
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // set
            JsonConvert.PopulateObject(values, _menuEntity);
            _menuEntity.MenuId = key;

            //
            // get data
            _returnFormat = _appPs.MenuApp.Update(_menuEntity);
            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


    }
}
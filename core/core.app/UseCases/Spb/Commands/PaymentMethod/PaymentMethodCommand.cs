﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Spb.Commands.PaymentMethod
{
    public partial class PaymentMethodCommand : IRequest<ReturnFormat>
    {
        public String? SpbNo { get; set; }
        public String? PaymentMethod { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class PaymentMethodCommandHandler : IRequestHandler<PaymentMethodCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public PaymentMethodCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(PaymentMethodCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            //cek key exists di entity
            try {
                //logic
                var entity = _context.SpbEntitys.Where(w => w.SpbNo.Equals(req.SpbNo)).FirstOrDefault();

                if (entity == null)
                {
                    return rtn;
                }

                var pm_lama = entity.PaymentMethod;
                entity.SpbNo = req.SpbNo;
                entity.PaymentMethod = req.PaymentMethod;
                entity.UpdatedBy = req.uiUserProfile.userId.ToString();
                entity.UpdatedAt = DateTime.UtcNow;

                await _context.SaveChangesAsync(cancellationToken);

                var desc = "Dari ' "+pm_lama + " ', menjadi ' " + entity.PaymentMethod + " '";

                this.SetHistoryAction(req.uiUserProfile.userId, entity.SpbId, "Update Payment Method", "SPB List Sender", desc);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;


                //return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }

            return rtn;
        }

        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu = Menu;
            dataItem.Desc = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }
    }
}

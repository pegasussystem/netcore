﻿using core.helper;
using Core.Entity.Base;
using Core.Entity.Main;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Interface.Repo.Base
{
    public interface IUserRepo : IRepo<UserEntity>
    {
        IQueryable<VUserWithProfileEntity> ReadVUserWithProfile(Expression<Func<VUserWithProfileEntity, bool>> expression);
    }
}

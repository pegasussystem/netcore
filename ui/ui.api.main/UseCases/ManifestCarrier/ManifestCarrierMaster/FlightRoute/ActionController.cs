﻿using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.FlightRoute.Command;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierMaster.FlightRoute
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]

    public class ActionController : ApiController
    {

        [HttpPost("flight-route-create")]
        public async Task<IActionResult> FlightRouteCreate([FromBody] FlightRouteCreateCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("flight-route-delete")]
        public async Task<IActionResult> FlightRouteDelete([FromBody] FlightRouteDeleteCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Base
{
    public partial class CreateHistoryCloseCommand : IRequest<ReturnFormat>
    {
        public DateTime? loginDate { get; set; }
        public String? menu { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class CreateHistoryCloseCommandHandler : IRequestHandler<CreateHistoryCloseCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateHistoryCloseCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateHistoryCloseCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                //HistoryLoginEntity dataItem = new HistoryLoginEntity();

                //dataItem.UserLogin = req.uiUserProfile.userId.ToString();
                //dataItem.LoginDate = DateTime.UtcNow;
                //dataItem.Menu = req.menu;

                //_context.HistoryLoginEntitys.Add(dataItem);
                //_context.SaveChanges();

                //rtn.Status = StatusCodes.Status200OK;
                //rtn.Data = dataItem;

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }
    }

}
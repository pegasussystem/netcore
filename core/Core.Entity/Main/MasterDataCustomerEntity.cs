﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MasterDataCustomerEntity
    {
        public Int32? CustomerId { get; set; }
        public Int32? CustomerNameId { get; set; }
        public Int32? CustomerStoreId { get; set; }
        public Int32? CustomerPlaceId { get; set; }
        public Int32? CustomerAddressId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public String? Telepon { get; set; }
        public String? Phone { get; set; }
        public String? CustomerName { get; set; }
        public String? CustomerStore { get; set; }
        public String? CustomerPlace { get; set; }
        public String? CustomerAddress { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_DataOptionSettingEntity
    {
        public Int32? Id { get; set; }
        public String? Code { get; set; }
        public String? Name { get; set; }
        public String? Type { get; set; }
        public Int32? Key { get; set; }
    }
}

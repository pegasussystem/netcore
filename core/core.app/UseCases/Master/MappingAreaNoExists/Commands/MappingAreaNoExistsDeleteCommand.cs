﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Master.MappingAreaNoExists.Commands
{
     public partial class MappingAreaNoExistsDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? mappingAreaNoExistsId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MappingAreaNoExistsDeleteCommandHandler : IRequestHandler<MappingAreaNoExistsDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public MappingAreaNoExistsDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(MappingAreaNoExistsDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {

        // Mark as Changed
        var res = this.MappingAreaNoExistsDelete(req.mappingAreaNoExistsId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_MappingAreaNoExistsDeleteEntity> MappingAreaNoExistsDelete(Int32? mappingAreaNoExistsId)
        {
            String query = $@"EXEC SP_MappingAreaNoExistsDelete @mappingAreaNoExistsId= {mappingAreaNoExistsId} ";
            var res = this._context.SP_MappingAreaNoExistsDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

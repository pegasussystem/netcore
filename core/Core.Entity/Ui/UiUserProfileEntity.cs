﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Ui
{
    public class UiUserProfileEntity
    {
        public Int32? userId { get; set; }
        public String? username { get; set; }
        public String? email { get; set; }

        public Int32? companyId { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? SubBranchId { get; set; }
        public String? companyName { get; set; }
        public String? companyAddress { get; set; }

        public String? workType { get; set; }
        public Int32? workId { get; set; }
        public String? WorkName { get; set; }
        public String? WorkAddress { get; set; }
        public String? workTimeZone { get; set; }
        public int? WorkTimeZoneHour { get; set; }
        public int? WorkTimeZoneMinute { get; set; }
        public Int32? workCityId { get; set; }
        public Int32? WorkAreaId { get; set; }
        public String? workCityName { get; set; }
        public String? WorkAreaName { get; set; }

        public Int32? privilegeId { get; set; }
        public String?[] MenuKeyLogin { get; set; }
        public String?[] MenuActionLogin { get; set; }
        public String?[] MenuActionIdLogin { get; set; }

        //declare variable untuk send param menggunakan ui send profile
        public String? MenuKey { get; set; }
        public String? MenuAction { get; set; }
        public String? DataValue { get; set; }
        //public String? param4 { get; set; }

    }
}

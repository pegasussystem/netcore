﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class RatesSubDistrictDetailConfig : IEntityTypeConfiguration<RatesSubDistrictDetailEntity>
    {
        public void Configure(EntityTypeBuilder<RatesSubDistrictDetailEntity> builder)
        {
            builder.HasKey(e => e.RatesSubDistrictDetailId);
            builder.ToTable("RatesSubDistrictDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

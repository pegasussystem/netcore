﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit.Commands
{
    public partial class MasterTransitDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? transitRouteId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MasterTransitDeleteCommandHandler : IRequestHandler<MasterTransitDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public MasterTransitDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(MasterTransitDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.TransitDelete(req.transitRouteId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_TransitRouteDeleteEntity> TransitDelete(Int32? transitRouteId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_TransitRouteDelete @transitRouteId='{transitRouteId}'";
            var res = this._context.SP_ManifestCarrierMaster_TransitRouteDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
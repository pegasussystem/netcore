﻿using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.FlightRoute.Query;
using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.Vendor.Query;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.Vendor
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]

    public class ListController : ApiController
    {
        [HttpGet("manifest-carrier-master-vendor-list")]
        public async Task<IActionResult> ListFlightRoute([FromQuery] ManifestCarrierMasterVendorListQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

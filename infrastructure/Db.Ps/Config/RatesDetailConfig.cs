﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class RatesDetailConfig : IEntityTypeConfiguration<RatesDetailEntity>
    {
        public void Configure(EntityTypeBuilder<RatesDetailEntity> builder)
        {
            builder.HasKey(e => e.RatesDetailId);
            builder.ToTable("RatesDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

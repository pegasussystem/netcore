﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class VendorDestinationConfig : IEntityTypeConfiguration<VendorDestinationEntity>
    {
        public void Configure(EntityTypeBuilder<VendorDestinationEntity> builder)
        {
            builder.HasKey(e => e.VendorDestinationId);
            builder.ToTable("VendorDestination");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
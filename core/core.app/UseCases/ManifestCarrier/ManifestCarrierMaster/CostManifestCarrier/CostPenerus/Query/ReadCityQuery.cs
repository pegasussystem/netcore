﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostPenerus.Query
{
    public partial class ReadCityQuery : IRequest<ReturnFormat>
    {
        public Int32? stationId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadCityQueryHandler : IRequestHandler<ReadCityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadCityQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;
                var query = from a in _context.VCompanyCityEntities
                            join b in _context.StationCityEntitys on a.CityId equals b.CityId
                            where b.StationId.Equals(req.stationId)
                            select new
                            {
                                a.CityId
                                , a.CompanyCityCode
                                , a.CompanyCityType
                                , a.CompanyCityName
                                , a.CompanyCityNameCustom
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.CityId
                            , s.CompanyCityCode
                            , s.CompanyCityType
                            , s.CompanyCityName
                            , s.CompanyCityNameCustom
                        }).OrderBy(o => o.CompanyCityCode);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
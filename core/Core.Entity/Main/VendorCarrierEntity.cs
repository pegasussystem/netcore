﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class VendorCarrierEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? VendorCarrierId { get; set; }
        public Int32? VendorId { get; set; }
        public Int32? CarrierId { get; set; }
    }
}

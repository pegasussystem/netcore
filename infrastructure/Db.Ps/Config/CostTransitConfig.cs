﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostTransitConfig : IEntityTypeConfiguration<CostTransitEntity>
    {
        public void Configure(EntityTypeBuilder<CostTransitEntity> builder)
        {
            builder.HasKey(e => e.CostTransitId);
            builder.ToTable("CostTransit");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

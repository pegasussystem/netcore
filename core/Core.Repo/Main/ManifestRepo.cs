﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Main.View;
using Core.Entity.Base;

namespace Core.Repo.Main
{
    public class ManifestRepo : Repo<ManifestEntity>, IManifestRepo
    {
        public ManifestRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public IEnumerable<SPManifestNumberGeneratorEntity> GetKoliNo(UiUserProfileEntity uiUserProfileEntity, SpbGoodsEntity entity, String carrier)
        {
            String q = $@"EXEC [ManifestNumberGenerator] 
                        @CompanyId={uiUserProfileEntity.companyId}, @WorkId= {uiUserProfileEntity.workId}, @WorkType={uiUserProfileEntity.workType}, 
                        @BranchId = { (uiUserProfileEntity.BranchId == null ? "NULL" : uiUserProfileEntity.BranchId.ToString()) }, 
                        @SubBranchId = { (uiUserProfileEntity.SubBranchId == null ? "NULL" : uiUserProfileEntity.SubBranchId.ToString()) }, 
                        @WorkTimeZoneHour={uiUserProfileEntity.WorkTimeZoneHour}, @WorkTimeZoneMinute = {uiUserProfileEntity.WorkTimeZoneMinute}, 
                        @spbId = {entity.SpbId}, @spbGoodsId= {entity.SpbGoodsId},       @caw={entity.Caw}, @carrier={carrier} ";

            return this._contextPs.Set<SPManifestNumberGeneratorEntity>()
                .FromSqlRaw(q);

          
        }

        public IEnumerable<SPManifest_MonitoringEntity> ListManfestMonitoring(int year = 0, int month=0, int day = 0, int companyId = 0)
        {
            return this._contextPs.Set<SPManifest_MonitoringEntity>()
                .FromSqlRaw($@"EXEC SPManifest_Monitoring 
                       @year={year}, @month={month}, @day= {day}, @companyId= {companyId} ");

           
        }


        public IEnumerable<VManifestWithKoliEntity> VManifest_Ddb_WithTotalKoli(UiUserProfileEntity uiUserProfileEntity, ManifestEntity manifest)
        {
            var q = 
                from m in _contextPs.Set<ManifestEntity>()
                join mk in _contextPs.Set<ManifestKoliEntity>() on m.ManifestId equals mk.ManifestId
                join mks in _contextPs.Set<ManifestKoliSpbEntity>() on mk.ManifestKoliId equals mks.ManifestKoliId into gmks from submks in gmks.DefaultIfEmpty()
                join sg in _contextPs.Set<SpbGoodsEntity>() on submks.SpbGoodsId equals sg.SpbGoodsId into gsg from subgsg in gsg.DefaultIfEmpty()
                join a in _contextPs.Set<AlphabetEntity>() on m.ManifestAlphabet equals a.AlphabetId
                join ccd in _contextPs.Set<CompanyCityEntity>() on m.DestinationCityCode equals ccd.CompanyCityId
                join mcd in _contextPs.Set<MasterCityEntity>() on ccd.CityId equals mcd.MasterCityId

                where m.OriginCityCode.Equals(manifest.OriginCityCode)
                    //&& m.DestinationCityCode.Equals(manifest.DestinationCityCode)
                    && m.CompanyId.Equals(manifest.CompanyId)
                    && ( m.CreatedAt >= manifest.CreatedAt.Value.AddDays(-3))
                //&& m.CreatedAt.Value.Year.Equals(manifest.CreatedAt.Value.Year)
                //&& m.CreatedAt.Value.Month.Equals(manifest.CreatedAt.Value.Month)
                //&& m.CreatedAt.Value.Day <= (manifest.CreatedAt.Value.AddDays(-2).Day)
                //&& m.CreatedAt.Value.Day >= (manifest.CreatedAt.Value.AddDays(2).Day)

                select new { 
                    m.CreatedAt,m.ManifestId, 
	                a.Alphabet,m.CompanyId, m.BranchId, m.SubBranchId,
	                m.OriginCityCode, m.DestinationCityCode,
	                mk.ManifestKoliId, mk.KoliNo, mk.KoliType, mk.IsClose,
                    subgsg.Aw, subgsg.Caw,
                    mcd.MasterCityCode
                } into x

                group x by new { 
                    x.CreatedAt,x.ManifestId,
                    x.Alphabet,x.CompanyId, x.BranchId, x.SubBranchId,
                    x.OriginCityCode, x.DestinationCityCode,
                    x.ManifestKoliId, x.KoliNo, x.KoliType, x.IsClose,
                    x.MasterCityCode
                } into g

                select new VManifestWithKoliEntity {
                    CreatedAt = g.Key.CreatedAt,
                    CreatedAtLocal = g.Key.CreatedAt.Value.AddHours((double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((double)uiUserProfileEntity.WorkTimeZoneMinute),
                    ManifestId = g.Key.ManifestId,
                    CompanyId = g.Key.CompanyId,
                    BranchId = g.Key.BranchId,
                    SubBranchId = g.Key.SubBranchId,
                    ManifestKoliId = g.Key.ManifestKoliId,
                    KoliNo = g.Key.KoliNo,
                    KoliType = g.Key.KoliType,
                    IsClose = g.Key.IsClose,
                    ManifestNo = g.Key.MasterCityCode + " " + g.Key.Alphabet + " # " + g.Key.ManifestId,
                    SumAw = g.Sum(x => x.Aw),
                    SumCaw = g.Sum(x => x.Caw),
                };

            return q.ToList();

        }

        public IEnumerable<V_Manifest_Dbb_WithWeight_Entity> V_Manifest_Dbb_WithWeight(
            UiUserProfileEntity uiUserProfileEntity
            )
        {
            DateTime dateTime = DateTime.Now.Date;

            var q = 
                from m in _contextPs.Set<ManifestEntity>()
                join mk in _contextPs.Set<ManifestKoliEntity>() on m.ManifestId equals mk.ManifestId
                join mks in _contextPs.Set<ManifestKoliSpbEntity>() on mk.ManifestKoliId equals mks.ManifestKoliId into gmks from submks in gmks.DefaultIfEmpty()
                join sg in _contextPs.Set<SpbGoodsEntity>() on submks.SpbGoodsId equals sg.SpbGoodsId into gsg from subgsg in gsg.DefaultIfEmpty()
                join a in _contextPs.Set<AlphabetEntity>() on m.ManifestAlphabet equals a.AlphabetId

                // origin
                join cco in _contextPs.Set<CompanyCityEntity>() on m.OriginCityCode equals cco.CompanyCityId
                join mco in _contextPs.Set<MasterCityEntity>() on cco.CityId equals mco.MasterCityId

                // Destination
                join ccd in _contextPs.Set<CompanyCityEntity>() on m.DestinationCityCode equals ccd.CompanyCityId
                join mcd in _contextPs.Set<MasterCityEntity>() on ccd.CityId equals mcd.MasterCityId

                // where
                //where m.OriginCityCode.Equals(userOrigin)
                //    && m.CompanyId.Equals(userCompany)
                //    && ( m.CreatedAt >= fromDate.Value.AddDays(-7))

                where  m.CompanyId.Equals(uiUserProfileEntity.companyId)
                   && (m.CreatedAt >= dateTime.AddDays(-40))


                select new { 
                    m.CreatedAt,m.ManifestId, 
	                a.Alphabet,m.CompanyId, m.BranchId, m.SubBranchId,
	                m.OriginCityCode, m.DestinationCityCode,
	                //mk.ManifestKoliId, mk.KoliNo, mk.KoliType, mk.IsClose,
                    subgsg.Aw, subgsg.Caw,
                    OriginCity = mco.MasterCityCode,
                    DestinationCity = mcd.MasterCityCode,

                } into x

                group x by new { 
                    x.CreatedAt,x.ManifestId,
                    x.Alphabet,x.CompanyId, x.BranchId, x.SubBranchId,
                    x.OriginCityCode, x.DestinationCityCode,
                  //  x.ManifestKoliId, x.KoliNo, x.KoliType, x.IsClose,
                    x.DestinationCity, x.OriginCity
                } into g

                select new V_Manifest_Dbb_WithWeight_Entity 
                {
                    CreatedAt = g.Key.CreatedAt,
                    CreatedAtLocal = g.Key.CreatedAt.Value.AddHours((double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((double)uiUserProfileEntity.WorkTimeZoneMinute),
                    ManifestId = g.Key.ManifestId,
                    CompanyId = g.Key.CompanyId,
                    BranchId = g.Key.BranchId,
                    SubBranchId = g.Key.SubBranchId,
                    ManifestNo = g.Key.DestinationCity + " " + g.Key.Alphabet + " # " + g.Key.ManifestId,
                    SumAw = g.Sum(x => x.Aw),
                    SumCaw = g.Sum(x => x.Caw),
                    OriginCity = g.Key.OriginCity,
                    DestinationCity = g.Key.DestinationCity
                };

            return q.ToList();

        }



        ////---------------------------- Sudah tidak dipake di pindahkan ke Core.app -> UseCase -> GoodsMonitoring -> ListReceiver -> ListReceiverQuery -------------------------------//
        //// --- --- ---
        //// Receiver
        //// get Monitoring Goods
        //// --- --- ---
        //public IEnumerable<ListGoodsMonitoringReceiverEntity> List_MonitoringGoods_Receiver(int year = 0, int month = 0, int day = 0, string dataValue="", int? companyId = 0)
        //{
        //    var query = $@"EXEC List_GoodsMonitoring_Receiver @year={year}, @month={month}, @day= {day}, @dataValue= '{dataValue}', @companyId= {companyId} ";
        //    var res = this._contextPs.Set<ListGoodsMonitoringReceiverEntity>()
        //                .FromSqlRaw(query).ToList();

        //    return res;

        //}

        


    }



}

﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class RatesConfig : IEntityTypeConfiguration<RatesEntity>
    {
        public void Configure(EntityTypeBuilder<RatesEntity> builder)
        {
            builder.HasKey(e => e.RatesId);
            builder.ToTable("Rates");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

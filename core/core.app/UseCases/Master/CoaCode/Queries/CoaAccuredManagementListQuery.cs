using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;

namespace core.app.UseCases.Master.CoaCode.Queries

{

    public partial class CoaAccuredManagementListQuery : IRequest<ReturnFormat>
    {
       
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class CoaAccuredManagementListQueryHandler : IRequestHandler<CoaAccuredManagementListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public CoaAccuredManagementListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(CoaAccuredManagementListQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;



            var query = from a in _context.CoaAccuredExpensesManagementEntities
                        join b in _context.CoaCodeEntities
                            on a.coaId equals b.CoaId // CoaIdRevenue di-cast menjadi long
                        join c in _context.CoaParentEntities
                            on b.CoaParentId equals c.CoaParentId
                        where a.IsActive == 1
                        select new
                        {
                           a.AccuredExpensesId,
                           b.CoaCode,
                           b.CoaName,
                           a.IsActive,
                           c.CoaParentName,
                           a.coaId,
                           CoaFullName = String.Format("[{0}] - {1}", b.CoaCode, b.CoaName)
                        };


            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.AccuredExpensesId,
                        s.CoaCode,
                        s.CoaName,
                        s.IsActive,
                        s.coaId,
                        s.CoaParentName,
                        s.CoaFullName
                    }).OrderBy(o => o.AccuredExpensesId);
            }

            // return 
            return rtn;
        }


    }

}

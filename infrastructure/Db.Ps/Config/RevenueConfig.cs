using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class RevenueConfig : IEntityTypeConfiguration<RevenueEntity>
    {
        public void Configure(EntityTypeBuilder<RevenueEntity> builder)
        {
            builder.HasKey(e => e.revenueId);
            builder.ToTable("Revenue");
        }
    }
}

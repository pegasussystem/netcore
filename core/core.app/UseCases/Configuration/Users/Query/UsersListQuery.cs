﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Configuration.Users.Query
{
    public partial class UsersListQuery : IRequest<ReturnFormat>
    {
        public Int32? statusId { get; set; }
    }

    public class UsersListQueryHandler : IRequestHandler<UsersListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public UsersListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(UsersListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.V_DataUserEntitys
                        where a.IsActive.Equals(req.statusId)
                        select new
                        {
                            a.UserId
                            , a.Username
                            , a.Email
                            , a.FullName
                            , a.TableName
                            , a.TableNameId
                            , a.WorkUnit
                            , a.WorkPlace
                            , a.PrivilegeId
                            , a.PrivilegeName
                            , a.RolesId
                            , a.RolesName
                            , a.Description
                            , a.IsActive
                            , a.Status
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.UserId
                        , s.Username
                        , s.Email
                        , s.FullName
                        , s.TableName
                        , s.TableNameId
                        , s.WorkUnit
                        , s.WorkPlace
                        , s.PrivilegeId
                        , s.PrivilegeName
                        , s.RolesId
                        , s.RolesName
                        , s.Description
                        , s.IsActive
                        , s.Status
                    }).OrderByDescending(o => o.UserId);
            }

            // return
            return rtn;
        }

    }
}

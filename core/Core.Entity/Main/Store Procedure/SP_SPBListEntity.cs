﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_SPBListEntity
    {
        public DateTime? CreatedAt { get; set; }
        public Int32? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? SubBranchId { get; set; }
        public String? Username { get; set; }
        public long? SpbId { get; set; }
        public String? SpbNo { get; set; }
        public String? TypesOfGoods { get; set; }
        public String? TypesOfGoodsName { get; set; }
        public String? Carrier { get; set; }
        public String? CarrierName { get; set; }
        public String? QualityOfService { get; set; }
        public String? QualityOfServiceName { get; set; }
        public String? TypeOfService { get; set; }
        public String? TypeOfServiceName { get; set; }

        public Decimal? Rates { get; set; }
        public Decimal? Packing { get; set; }
        public Decimal? Quarantine { get; set; }
        public Decimal? Etc { get; set; }
        public Decimal? Ppn { get; set; }
        public Decimal? Discount { get; set; }


        public Decimal? TotalPrice { get; set; }
        public String? PaymentMethod { get; set; }
        public String? Description { get; set; }


        // ###OriginCity 
        public String? OriginCityId { get; set; }
        public String? OriginCityCode { get; set; }
        public String? OriginCityNameCustom { get; set; }


        //  ###OriginArea	 
        public String? OriginAreaId { get; set; }
        public String? OriginAreaCode { get; set; }
        public String? OriginAreaNameCustom { get; set; }


        // ###DestinationCity	 
        public String? DestinationCityId { get; set; }
        public String? DestinationCityCode { get; set; }
        public String? DestinationCityNameCustom { get; set; }


        //###DestinationArea	
        public String? DestinationAreaId { get; set; }
        public String? DestinationAreaCode { get; set; }
        public String? DestinationAreaNameCustom { get; set; }



        //###Sender	
        public String? SenderPhone { get; set; }
        public String? SenderAddress { get; set; }
        public String? SenderName { get; set; }
        public String? SenderPlace { get; set; }
        public String? SenderStore { get; set; }


        //###Receiver
        public String? ReceiverPhone { get; set; }
        public String? ReceiverAddress { get; set; }
        public String? ReceiverName { get; set; }
        public String? ReceiverPlace { get; set; }
        public String? ReceiverStore { get; set; }
        public String? ViaName { get; set; }

        public Boolean? IsVoid { get; set; }
        public DateTime? VoidDate { get; set; }
        public String? VoidBy { get; set; }
        public String? VoidReason { get; set; }

        public String? Phone { get; set; }
        public Int32? CompanyCityId { get; set; } //Pati 15 feb22
    }
}

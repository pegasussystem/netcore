﻿using core.app.UseCases.MasterDataCustomer.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.MasterDataCustomer
{
    [Authorize]
    [Route("mdata-customer-receiver")]
    [ApiController]
    public class MDataCustomerReceiverController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public MDataCustomerReceiverController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list")]
        public async Task<IActionResult> List([FromQuery] MDataCustomerReceiverQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}
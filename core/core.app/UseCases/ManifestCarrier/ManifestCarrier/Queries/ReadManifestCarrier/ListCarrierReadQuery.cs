﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListCarrierReadQuery : IRequest<ReturnFormat>
    {
        public Int32? vendorCarrierId { get; set; }
        public Int32? firstCarrierCodeId { get; set; }
        public Int32? firstFlightId { get; set; }
        public Int32? secondCarrierCodeId { get; set; }
        public Int32? secondFlightId { get; set; }
        public Int32? thirdCarrierCodeId { get; set; }
        public Int32? thirdFlightId { get; set; }
        public Int32? fourthCarrierCodeId { get; set; }
        public Int32? fourthFlightId { get; set; }
        public Int32? originStationId { get; set; }
        public Int32? destinationStationId { get; set; }
        public Int32? modaId { get; set; }
    }

    public class ListCarrierReadQueryHandler : IRequestHandler<ListCarrierReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCarrierReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCarrierReadQuery req, CancellationToken cancellationToken)
        {
            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.FlightRouteEntitys
                        join b in _context.VendorCarrierEntitys on a.VendorCarrierId equals b.VendorCarrierId
                        where a.OriginStationId.Equals(req.originStationId)
                            && a.FirstFlightCodeId.Equals(req.firstCarrierCodeId)
                            && a.FirstFlightId.Equals(req.firstFlightId)
                            && a.SecondFlightCodeId.Equals(req.secondCarrierCodeId)
                            && a.SecondFlightId.Equals(req.secondFlightId)
                            && a.ThirdFlightCodeId.Equals(req.thirdCarrierCodeId)
                            && a.ThirdFlightId.Equals(req.thirdFlightId)
                            && a.FourthFlightCodeId.Equals(req.fourthCarrierCodeId)
                            && a.FourthFlightId.Equals(req.fourthFlightId)
                            && b.VendorId.Equals(req.vendorCarrierId)
                            && a.DestinationStationId.Equals(req.destinationStationId)

                        select new
                        {
                            a.CarrierId
                        };

            var query2 = query.Distinct();
            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(
                    s => new
                    {
                        s.CarrierId
                    }).OrderByDescending(o => o.CarrierId);
            }
            //
            // return
            return rtn;
        }
    }
}

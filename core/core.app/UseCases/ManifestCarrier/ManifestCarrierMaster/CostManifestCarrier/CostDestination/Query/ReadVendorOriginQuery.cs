﻿using core.helper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostDestination.Query
{
    public partial class ReadVendorOriginQuery : IRequest<ReturnFormat>
    {
        public Int32? stationOriginId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadVendorOriginQueryHandler : IRequestHandler<ReadVendorOriginQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadVendorOriginQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadVendorOriginQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                rtn.Status = StatusCodes.Status200OK;
                var qr = from a in _context.VendorOriginEntitys
                            join b in _context.VendorEntitys on a.VendorId equals b.VendorId
                            join c in _context.StationEntitys on a.StationId equals c.StationId
                            where c.StationId.Equals(req.stationOriginId)
                            select new
                        {
                            VendorId = a.VendorOriginId
                            , b.VendorName
                        };

                if (qr != null)
                {
                    var query = qr.Distinct();
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.VendorId
                            , s.VendorName
                        }).OrderBy(o => o.VendorId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
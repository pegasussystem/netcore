﻿using System;

namespace Core.Entity.Main.View
{
    public class VManifestCarrierEntity
    {

        public DateTime? SendDateUtc { get; set; }
        public DateTime? SendDateLocal { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? ManifestCarrierId { get; set; }
        public Int32? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? SubBranchId { get; set; }
        public String? OriginCity { get; set; }
        public String? DestinationCity { get; set; }
        public Int32? CarrierId1 { get; set; }
        public Int32? CarrierId2 { get; set; }
        public Int32? CarrierId3 { get; set; }
        public Int32? CarrierId4 { get; set; }
        public String? NoSmu { get; set; }
        public decimal? FinalWeight { get; set; }
        public String? CityOriginCode { get; set; }
        public String? CityDestinationCode { get; set; }
        public String? CarrierName1 { get; set; }
        public String? CarrierName2 { get; set; }
        public String? CarrierName3 { get; set; }
        public String? CarrierName4 { get; set; }

        public DateTime? FlightDateUtc { get; set; }
        public DateTime? FlightDateLocal { get; set; }
    }


}

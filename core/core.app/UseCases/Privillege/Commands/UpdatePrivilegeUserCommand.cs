﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Commands
{
    public partial class UpdatePrivilegeUserCommand : IRequest<ReturnFormat>
    {
        public Int32? userId { get; set; }
        public String? username { get; set; }
        public Int32? privilegeId { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class UpdatePrivilegeUserCommandHandler : IRequestHandler<UpdatePrivilegeUserCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdatePrivilegeUserCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdatePrivilegeUserCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            var ck = _context.PrivilegeUserEntitys.Where(w => w.UserId.Equals(req.userId)).Count();

            if (ck == 0) {
                //insert Privilege User

                PrivilegeUserEntity pu = new PrivilegeUserEntity();

                pu.CreatedBy = req.uiUserProfile.userId.ToString();
                pu.CreatedAt = DateTime.Now;
                pu.PrivilegeId = req.privilegeId;
                pu.UserId = req.userId;

                _context.PrivilegeUserEntitys.Add(pu);
                _context.SaveChanges();
            }
            


            try
            {
                // Mark as Changed
                var dataItem = _context.PrivilegeUserEntitys.Where(w => w.UserId.Equals(req.userId)).FirstOrDefault();

                dataItem.PrivilegeId    = req.privilegeId;
                dataItem.UpdatedBy      = req.uiUserProfile.userId.ToString();
                dataItem.UpdatedAt      = DateTime.UtcNow;

                _context.PrivilegeUserEntitys.Update(dataItem);
                _context.SaveChanges();

                this.SetHistoryAction(req.uiUserProfile.userId, dataItem.PrivilegeId, "Update Privilege User", "Privilege Master", "");

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = dataItem;
                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu = Menu;
            dataItem.Desc = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }
    }

}

﻿using core.helper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Interface.App.Base
{
    public interface IApp<T>
    {
        //Create Data
        ReturnFormat Create(T entity);


        ReturnFormat Read();
        ReturnFormat Read(T entity);
        T Read(Guid id);
        T Read(Int32 id);

        ReturnFormat Update(T entity);

        T Delete(T entity);
    }
}

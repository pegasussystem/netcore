﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_ManifestKoliListConfig : IEntityTypeConfiguration<V_ManifestKoliListEntity>
    {
        public void Configure(EntityTypeBuilder<V_ManifestKoliListEntity> builder)
        {
            builder.HasKey(e => e.ManifestId);
            builder.ToTable("V_ManifestKoliList");
        }
    }
}

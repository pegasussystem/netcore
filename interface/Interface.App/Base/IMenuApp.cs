﻿using core.helper;
using Core.Entity.Base;
using System.Dynamic;

namespace Interface.App.Base
{
    public interface IMenuApp: IApp<MenuEntity>
    {
        ReturnFormat ReadTree();
    }
}

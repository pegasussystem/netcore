﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface IManifestCarrierCostRepo : IRepo<ManifestCarrierCostEntity>
    {
        void DeleteRange(List<ManifestCarrierCostEntity> entitys);

        public IEnumerable<VManifestCarrierCostEntity> VManifestCarrierCost(
            int ManifestCarrierId
            );
    }
}

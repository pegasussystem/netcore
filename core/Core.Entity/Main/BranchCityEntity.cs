﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class BranchCityEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? BranchCityId { get; set; }
        public String? CityId { get; set; }
        public Int32? BranchId { get; set; }
        public String? Carrier { get; set; }
    }
}

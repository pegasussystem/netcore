﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.Spb.Queries.ListSender;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Spb.Sender
{
    [Authorize]
    [Route("spblistsender")]
    [ApiController]
    public class ListController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ListController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list")]
        public async Task<IActionResult> List([FromQuery] ListSenderQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();


            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

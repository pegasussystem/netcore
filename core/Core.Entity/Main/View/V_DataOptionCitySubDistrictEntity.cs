﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_DataOptionCitySubDistrictEntity
    {
        public String? Id { get; set; }
        public String? Code { get; set; }
        public String? Name { get; set; }
        public String? Type { get; set; }
    }
}

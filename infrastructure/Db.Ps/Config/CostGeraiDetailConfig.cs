﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostGeraiDetailConfig : IEntityTypeConfiguration<CostGeraiDetailEntity>
    {
        public void Configure(EntityTypeBuilder<CostGeraiDetailEntity> builder)
        {
            builder.HasKey(e => e.CostGeraiDetailId);
            builder.ToTable("CostGeraiDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
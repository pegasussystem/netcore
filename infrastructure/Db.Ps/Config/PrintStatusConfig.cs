﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
     class PrintStatusConfig : IEntityTypeConfiguration<PrintStatusEntity>
    {
        public void Configure(EntityTypeBuilder<PrintStatusEntity> builder)
        {
            builder.HasKey(e => e.PrintStatusId);
            builder.ToTable("PrintStatus");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

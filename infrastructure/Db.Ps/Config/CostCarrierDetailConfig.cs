﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostCarrierDetailConfig : IEntityTypeConfiguration<CostCarrierDetailEntity>
    {
        public void Configure(EntityTypeBuilder<CostCarrierDetailEntity> builder)
        {
            builder.HasKey(e => e.CostCarrierDetailId);
            builder.ToTable("CostCarrierDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using core.app.Common.Interfaces;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.PrintStatus.Query
{
     public partial class CheckStatusPrintQuery : IRequest<ReturnFormat>
    {
        public String? Type { get; set; }
        public Int32? DocId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class CheckStatusPrintQueryHandler : IRequestHandler<CheckStatusPrintQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public CheckStatusPrintQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(CheckStatusPrintQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_CheckPrintStatusEntity> entities;

            entities = this.CheckStatusPrint(req.uiUserProfile.userId, req.Type,req.DocId);

            if (entities != null)
            {
               rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                         s.result
                     });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_CheckPrintStatusEntity> CheckStatusPrint(Int32? UserId, String? Type, Int32? DocId)
        {
            String query = $@"EXEC SP_CheckPrintStatus @userId = '{UserId}', @type = '{Type}', @docId='{DocId}' ";
            var res = this._context.SP_CheckPrintStatusEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
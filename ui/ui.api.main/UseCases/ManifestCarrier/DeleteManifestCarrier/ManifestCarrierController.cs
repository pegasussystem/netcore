﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.ManifestCarrier.DeleteManifestCarrier;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ui.api.main.UseCases.ManifestCarrier.DeleteManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ManifestCarrierController : DefaultController
    {
        private readonly IAppWrapperPs _appPs;
        public ManifestCarrierController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpDelete("delete2")]
        public async Task<IActionResult> Execute([FromQuery] DeleteManifestCarrierInput request)
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();

            // logic
            // ///// ///// /////

            _returnFormat = _appPs.DeleteManifestCarrierUseCase.Delete(GetUserClaim(), request);

            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}
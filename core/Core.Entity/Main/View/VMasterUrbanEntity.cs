﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VMasterUrbanEntity
    {
        public Int32? CompanyCityId { get; set; }
        public String? MasterProvinceId { get; set; }
        public String? MasterCityId { get; set; }
        public String? MasterSubDistrictId { get; set; }
        public String? MasterUrbanId { get; set; }
        public String? MasterUrbanName { get; set; }
        public String? MasterUrbanNameCustom { get; set; }
        public String? MasterUrbanPostalCode { get; set; }
        public String? MasterUrbanPostalCodeCustom { get; set; }

    }





}

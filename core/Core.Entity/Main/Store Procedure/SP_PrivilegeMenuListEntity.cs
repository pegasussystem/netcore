﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_PrivilegeMenuListEntity
    {
        public Int32? MenuId { get; set; }
        public String? MenuKey { get; set; }
        public String? MenuName { get; set; }
        public Int32? MenuParentId { get; set; }
        public Int32? OrderCode { get; set; }
        public Int32? IsActive { get; set; }

    }
}

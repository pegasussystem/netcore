﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostOrigin.Commands
{
    public partial class CostOriginDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? costOriginDetailId { get; set; }
        public Int32? costOriginId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostOriginDeleteCommandHandler : IRequestHandler<CostOriginDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostOriginDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostOriginDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostOriginDelete(req.costOriginId, req.costOriginDetailId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostOriginDeleteEntity> CostOriginDelete(Int32? costOriginId, Int32? costOriginDetailId)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostOriginDelete @costOriginId='{costOriginId}', @costOriginDetailId='{costOriginDetailId}' ";
            var res = this._context.SP_ManifestCarrierCost_CostOriginDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListVendorStationTransitFirstReadQuery : IRequest<ReturnFormat>
    {
        public String? originCityId { get; set; }
        public String? cityDestinationId { get; set; }
        public Int32? firstFlightId { get; set; }
        public Int32? firstCarrierCodeId { get; set; }
        public Int32? secondFlightId { get; set; }
        public Int32? secondCarrierCodeId { get; set; }
        public Int32? modaId { get; set; }
    }

    public class ListVendorStationTransitFirstReadQueryHandler : IRequestHandler<ListVendorStationTransitFirstReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListVendorStationTransitFirstReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListVendorStationTransitFirstReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.FlightRouteEntitys
                        join b in _context.StationCityEntitys on a.FirstCityTransitId equals b.CityId
                        join c in _context.StationEntitys on b.StationId equals c.StationId
                        join d in _context.VendorTransitEntitys on b.StationId equals d.StationId
                        join e in _context.VendorEntitys on d.VendorId equals e.VendorId
                        join f in _context.CarrierEntitys on a.CarrierId equals f.CarrierId
                        where a.OriginCityId.Equals(req.originCityId)
                            && a.DestinationCityId.Equals(req.cityDestinationId)
                            && a.FirstFlightId.Equals(req.firstFlightId)
                            && a.FirstFlightCodeId.Equals(req.firstCarrierCodeId)
                            && a.SecondFlightId.Equals(req.secondFlightId)
                            && a.SecondFlightCodeId.Equals(req.secondCarrierCodeId)
                            && f.ModaId.Equals(req.modaId)
                        select new
                        {
                            e.VendorId
                            , e.VendorName
                            , c.StationId
                            , c.StationName
                            , CityTransitCode = a.FirstCityTransitId
                        };

            var query2 = query.Distinct();
            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(s => new { s.VendorId, s.VendorName, s.StationId, s.StationName, s.CityTransitCode });
            }

            //
            // return
            return rtn;
        }
    }
}

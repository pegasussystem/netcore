﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.HistoryPrint.Queries
{
    
    public partial class ListHistoryPrintQuery : IRequest<ReturnFormat>
    {
        public DateTime? dateFrom { get; set; }
        public DateTime? dateTo { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class ListHistoryPrintQueryHandler : IRequestHandler<ListHistoryPrintQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListHistoryPrintQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListHistoryPrintQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_MonitoringHistory_HistoryPrintEntity> entities;
            
            var YearFrom    = req.dateFrom.Value.Year;
            var MonthFrom   = req.dateFrom.Value.Month;
            var DayFrom     = req.dateFrom.Value.Day;

            var YearTo      = req.dateTo.Value.Year;
            var MonthTo     = req.dateTo.Value.Month;
            var DayTo       = req.dateTo.Value.Day;

            var DateFrom    = YearFrom + "-" + MonthFrom + "-" + DayFrom;
            var DateTo      = YearTo + "-" + MonthTo + "-" + DayTo;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataHistoryPrint(DateFrom, DateTo);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                         s.HistoryPrintId
                        , s.UserId
                        , s.Username
                        , s.DocId
                        , s.TypePrint
                        , s.SpbNo
                        , s.ManifestNo
                        , s.Menu
                        , s.MenuAction
                        , s.PrintDate
                        , PrintDateLocal = s.PrintDate.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("dddd, dd MMMM yyyy")
                        , PrintTimeLocal = s.PrintDate.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("HH:mm")
                        , s.Desc
                    }).OrderByDescending(o => o.PrintDate);
            }

            // return
            return rtn;
        }
        public IEnumerable<SP_MonitoringHistory_HistoryPrintEntity> DataHistoryPrint(String? DateFrom, String? DateTo)
        {
            String query = $@"EXEC SP_MonitoringHistory_HistoryPrint @dateFrom='{DateFrom}', @dateTo='{DateTo}'";
            var res = this._context.SP_MonitoringHistory_HistoryPrintEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }

}

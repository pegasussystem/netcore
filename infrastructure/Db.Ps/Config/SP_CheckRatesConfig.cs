﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class SP_CheckRatesConfig : IEntityTypeConfiguration<SP_CheckRatesEntity>
    {
        public void Configure(EntityTypeBuilder<SP_CheckRatesEntity> builder)
        {
            builder.HasKey(e => e.RatesId);
            builder.ToTable("SP_CheckRates");
        }
    }
}

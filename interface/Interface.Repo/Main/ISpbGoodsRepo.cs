﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface ISpbGoodsRepo : IRepo<SpbGoodsEntity>
    {
        IEnumerable<SPSpbGoods_CalculatePriceEntity> CalculatePrice(long spbId = 0);
    }
}

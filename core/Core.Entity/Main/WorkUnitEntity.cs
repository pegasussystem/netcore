﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class WorkUnitEntity
    {
        public Int32? WorkUnitId { get; set; }
        public String? WorkUnitCode { get; set; }
        public String? WorkUnitName { get; set; }
        public Boolean? IsActive { get; set; }
        public Int32? Parent { get; set; }
        public String? Value { get; set; }

    }
}

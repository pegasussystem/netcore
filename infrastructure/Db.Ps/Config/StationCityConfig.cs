﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class StationCityConfig : IEntityTypeConfiguration<StationCityEntity>
    {
        public void Configure(EntityTypeBuilder<StationCityEntity> builder)
        {
            builder.HasKey(e => e.StationCityId);
            builder.ToTable("StationCity");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Master.Round.Command
{
    public partial class CreateRoundCommand : IRequest<ReturnFormat>
    {
        public String? carrierId { get; set; }
        public String? first_value { get; set; }
        public String? last_value { get; set; }
        public String? final_value { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateRoundCommandHandler : IRequestHandler<CreateRoundCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateRoundCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateRoundCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.ManageData(req.carrierId, req.first_value, req.last_value, req.final_value, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_RoudCreateEntity> ManageData(String? carrierId, String? first_value, String? last_value, String? final_value, Int32? userId)
        {
            String query = $@"EXEC SP_Round_Create @carrierId='{carrierId}', @first_value='{first_value}', @last_value='{last_value}', @final_value='{final_value}', @UserId={userId} ";
            var res = this._context.SP_RoudCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
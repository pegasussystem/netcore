﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main
{

    public interface IManifestCarrierCostListUseCase : IApp<ManifestCarrierCostEntity>
    {
        ReturnFormat List(UiUserProfileEntity uiUserProfileEntity, Object input);
    }
}

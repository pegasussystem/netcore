﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Master.MappingAreaNoExists.Commands
{
     public partial class MappingAreaNoExistsCreateCommand : IRequest<ReturnFormat>
    {
        public String? areaId { get; set; }
        public String? cityId { get; set; }
        public String? subDistrictId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MappingAreaNoExistsCreateCommandHandler : IRequestHandler<MappingAreaNoExistsCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public MappingAreaNoExistsCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(MappingAreaNoExistsCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {

        // Mark as Changed
        var res = this.MappingAreaNoExistsCreate(req.areaId, req.cityId, req.subDistrictId, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_MappingAreaNoExistsCreateEntity> MappingAreaNoExistsCreate(String? areaId, String? cityId, String? subDistrictId, Int32? userId)
        {
            String query = $@"EXEC SP_MappingAreaNoExistsCreate @areaId= '{areaId}',@cityId= '{cityId}',@subDistrictId= '{subDistrictId}',@userId= '{userId}' ";
            var res = this._context.SP_MappingAreaNoExistsCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

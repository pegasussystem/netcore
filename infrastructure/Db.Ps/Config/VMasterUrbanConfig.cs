﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VMasterUrbanConfig : IEntityTypeConfiguration<VMasterUrbanEntity>
    {
        public void Configure(EntityTypeBuilder<VMasterUrbanEntity> builder)
        {
            builder.HasKey(e => e.MasterUrbanId);
            builder.ToTable("VMasterUrban");
        }
    }
}

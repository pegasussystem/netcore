﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class ManifestCarrierEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public Int32? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }

        public Int32? ManifestCarrierId { get; set; }
        public Int32? OriginStationId { get; set; }
        public Int32? DestinationStationId { get; set; }
        public String? ManifestCarrierNo { get; set; }
        public String? NoSmu { get; set; }
        public DateTime? FlightDate { get; set; }
        public Int32? ModaId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? VendorCarrierId { get; set; }
        public Int32? VendorOriginId { get; set; }

        public Int32? FirstFlightCodeId { get; set; }
        public Int32? FirstFlightNoId { get; set; }
        public Int32? FirstStationTransitId { get; set; }
        public Int32? FirstVendorTransitId { get; set; }

        public Int32? SecondFlightCodeId { get; set; }
        public Int32? SecondFlightNoId { get; set; }
        public Int32? SecondStationTransitId { get; set; }
        public Int32? SecondVendorTransitId { get; set; }

        public Int32? ThirdFlightCodeId { get; set; }
        public Int32? ThirdFlightNoId { get; set; }
        public Int32? ThirdStationTransitId { get; set; }
        public Int32? ThirdVendorTransitId { get; set; }

        public Int32? FourthFlightCodeId { get; set; }
        public Int32? FourthFlightNoId { get; set; }

        public Decimal? TotalFinalWeightSMU { get; set; }

        public String? ReasonKoli { get; set; }
        public String? ReasonManifest { get; set; }
        public String? ReasonSMU { get; set; }
        public Int32? ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public String? StatRevise { get; set; }
        public Int32? Revise { get; set; }
    }
}

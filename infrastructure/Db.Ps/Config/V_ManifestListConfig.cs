﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_ManifestListConfig : IEntityTypeConfiguration<V_ManifestListEntity>
    {
        public void Configure(EntityTypeBuilder<V_ManifestListEntity> builder)
        {
            builder.HasKey(e => e.ManifestId);
            builder.ToTable("V_ManifestList");
        }
    }
}

﻿using core.app.Base;
using core.app.Main;
//using core.app.UseCases.CarrierManagement;
using core.app.UseCases.GoodsMonitoring_Receiver.List;
using core.app.UseCases.Manifest.Change_KoliNo;
using core.app.UseCases.Manifest.Ddb_Manifest_WithTotalKoli;
using core.app.UseCases.Manifest.Manifest_Ddb_WithWeight;
using core.app.UseCases.Manifest.OpenCloseKoli;
using core.app.UseCases.Manifest.OpenCloseManifest;
//using core.app.UseCases.ManifestCarrier.DeleteManifestCarrier;
//using core.app.UseCases.ManifestCarrier.ManifestCarrierCostList;
//using core.app.UseCases.ManifestCarrier.ManifestCarrierItemList;
//using core.app.UseCases.ManifestCarrier.ReadManifestCarrier;
//using core.app.UseCases.ManifestCarrier.Sb_Carrier;
//using core.app.UseCases.ManifestCarrier_Report.List;
using core.app.UseCases.SpbHourly.List;
using core.app.UseCases.SpbReceiver.List;
using core.app.UseCases.SpbHourly.ListReceiver;
//using core.app.UseCases.ManifestCarrier.ReadManifestCarrierItemByManifestCarrier;
using Interface.App;
using Interface.App.Base;
using Interface.App.Main;
using Interface.App.Main.CarrierManagement;
using Interface.App.Main.GoodsMonitoring_Receiver;
using Interface.App.Main.Manifest;
using Interface.App.Main.ManifestCarrier;
using Interface.App.Main.ManifestCarrier_Report;
using Interface.App.Main.SpbHourly;
using Interface.App.Main.SpbReceiver;
using Interface.Other;
using Interface.Repo;
using System;
using System.Collections.Generic;
using System.Text;

namespace core.app
{
    public class AppWrapperPs : IAppWrapperPs
    {
        private IRepoWarpperPs _iRepoWarpperPs;
        private ILog _log;
        public AppWrapperPs(IRepoWarpperPs iRepoWarpper, ILog log)
        {
            _iRepoWarpperPs = iRepoWarpper;
            _log = log;
        }

        //
        // BASE
        //public IMenuApp MenuApp =>  new MenuApp(_iRepoWarpperPs, _log);
        public ILookupApp LookupApp => new LookupApp(_iRepoWarpperPs, _log);
        public IUserApp UserApp => new UserApp(_iRepoWarpperPs, _log);



        // MAIN
         public ICoaParentApp CoaParentApp => new CoaParentApp(_iRepoWarpperPs, _log);
        public ICoaCodeApp CoaCodeApp => new CoaCodeApp(_iRepoWarpperPs, _log);
        
        public ICustomerApp CustomerApp => new CustomerApp(_iRepoWarpperPs, _log);
        public ICompanyApp CompanyApp => new CompanyApp(_iRepoWarpperPs, _log);
        public ISpbApp SpbApp => new SpbApp(_iRepoWarpperPs, _log);

        public IMasterSubDistrictApp MasterSubDistrictApp => new MasterSubDistrictApp(_iRepoWarpperPs, _log);
        public IMasterUrbanApp MasterUrbanApp => new MasterUrbanApp(_iRepoWarpperPs, _log);
        public IMasterCityApp MasterCityApp => new MasterCityApp(_iRepoWarpperPs, _log);
        public ISpbCreateApp SpbCreateApp => new SpbCreateApp(_iRepoWarpperPs, _log);
        public IManifestSenderApp ManifestSenderApp => new ManifestSenderApp(_iRepoWarpperPs, _log);
        public IManifestReceiverApp ManifestReceiverApp => new ManifestReceiverApp(_iRepoWarpperPs, _log);
        public ISpbListSenderApp SpbListSenderApp => new SpbListSenderApp(_iRepoWarpperPs, _log);
        public ISpbListReceiverApp SpbListReceiverApp=> new SpbListReceiverApp(_iRepoWarpperPs, _log);
        public IGoodsMonitoringApp GoodsMonitoringApp => new GoodsMonitoringApp(_iRepoWarpperPs, _log);
        public IManifestCarrierApp ManifestCarrierApp => new ManifestCarrierApp(_iRepoWarpperPs, _log);
        public ISpbStatisticApp SpbStatisticApp => new SpbStatisticApp(_iRepoWarpperPs, _log);
        //public ICreateManifestCarrierUseCase CreateManifestCarrierUseCase => new CreateManifestCarrierUseCase(_iRepoWarpperPs, _log);
        //public IReadManifestCarrierUseCase ReadManifestCarrierUseCase =>    new ReadManifestCarrierUseCase(_iRepoWarpperPs, _log);
        //public IDeleteManifestCarrierUseCase DeleteManifestCarrierUseCase =>    new DeleteManifestCarrierUseCase(_iRepoWarpperPs, _log);
        //public IReadManifestCarrierItemByManifestCarrierUseCase ReadManifestCarrierItemByManifestCarrierUseCase =>    new ReadManifestCarrierItemByManifestCarrierUseCase(_iRepoWarpperPs, _log);
        //public IManifestCarrierItemListUseCase ManifestCarrierItemListUseCase =>    new ManifestCarrierItemListUseCase(_iRepoWarpperPs, _log);
        //public IManifestCarrierCostListUseCase ManifestCarrierCostListUseCase =>    new ManifestCarrierCostListUseCase(_iRepoWarpperPs, _log);
        public IDdb_Manifest_WithTotalKoli_UseCase Ddb_Manifest_WithTotalKoli_UseCase =>    new Ddb_Manifest_WithTotalKoli_UseCase(_iRepoWarpperPs, _log);
        public I_Change_KoliNo_UseCase Change_KoliNo_UseCase =>    new Change_KoliNo_UseCase(_iRepoWarpperPs, _log);
        public I_OpenClose_Manifest_UseCase OpenClose_Manifest_UseCase =>    new OpenClose_Manifest_UseCase(_iRepoWarpperPs, _log);
        public I_OpenClose_Koli_UseCase OpenClose_Koli_UseCase =>   new OpenClose_Koli_UseCase(_iRepoWarpperPs, _log);
        public I_Manifest_Ddb_WithWeight_UseCase Manifest_Ddb_WithWeight_UseCase =>   new Manifest_Ddb_WithWeight_UseCase(_iRepoWarpperPs, _log);
        //public I_Sb_Carrier_UseCase Sb_Carrier_UseCase =>   new Sb_Carrier_UseCase(_iRepoWarpperPs, _log);
        //public IManifestCarrier_Report_List_UseCase ManifestCarrier_Report_List_UseCase =>   new ManifestCarrier_Report_List_UseCase(_iRepoWarpperPs, _log);
        public IListSpbHourlyUseCase ListSpbHourlyUseCase =>   new ListSpbHourlyUseCase(_iRepoWarpperPs, _log);
        public IListSpbReceiverUsecase ListSpbReceiverUsecase =>   new ListSpbReceiverUsecase(_iRepoWarpperPs, _log);
        public IListReceiverSpbHourlyUseCase ListReceiverSpbHourlyUseCase =>   new ListReceiverSpbHourlyUseCase(_iRepoWarpperPs, _log);
        public IListGoodsMonitoringReceiverUseCase ListGoodsMonitoringReceiverUseCase =>   new ListGoodsMonitoringReceiverUseCase(_iRepoWarpperPs, _log);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SPSpbHourlyEntity
    {
        public Int32? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? SubBranchId { get; set; }
        public String? Carrier { get; set; }
        public Int64? ManifestId { get; set; }
        public String? Origin { get; set; }
        public String? BranchOrigin { get; set; }
        public String? DestinationId { get; set; }
        public String? Destination { get; set; }
        public String? BranchDestination { get; set; }
        public String? Alphabet { get; set; }
        public Int32? Koli_1200 { get; set; }
        public Decimal? Aw_1200 { get; set; }
        public Decimal? Caw_1200 { get; set; }
        public Int32? Koli_1400 { get; set; }
        public Decimal? Aw_1400 { get; set; }
        public Decimal? Caw_1400 { get; set; }
        public Int32? Koli_1530 { get; set; }
        public Decimal? Aw_1530 { get; set; }
        public Decimal? Caw_1530 { get; set; }
        public Int32? Koli_1600 { get; set; }
        public Decimal? Aw_1600 { get; set; }
        public Decimal? Caw_1600 { get; set; }
        public Int32? Koli_Final { get; set; }
        public Decimal? Aw_Final { get; set; }
        public Decimal? Caw_Final { get; set; }


    }
}

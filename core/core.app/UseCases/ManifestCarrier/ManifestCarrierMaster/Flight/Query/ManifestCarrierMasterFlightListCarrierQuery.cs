﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Threading;
using core.app.Common.Interfaces;
using Interface.Repo;
using Interface.Other;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Flight.Query
{
    public partial class ManifestCarrierMasterFlightListCarrierQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListCarrierQueryHandler : IRequestHandler<ManifestCarrierMasterFlightListCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterFlightListCarrierQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var query = from a in _context.CarrierEntitys
                            select new
                            {
                                a.CarrierId
                                , a.CarrierCode
                                , a.CarrierType
                                , CarrierName = a.CarrierCode + " - " + a.CarrierName
                                , a.ModaId
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.CarrierId
                            , s.CarrierCode
                            , s.CarrierType
                            , s.CarrierName
                            , s.ModaId
                            
                        }).OrderBy(o => o.CarrierName);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
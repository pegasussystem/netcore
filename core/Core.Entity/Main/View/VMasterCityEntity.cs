﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VMasterCityEntity
    {
        public String? MasterProvinceId { get; set; }
        public String? MasterProvinceCode { get; set; }
        public String? MasterProvinceName { get; set; }

        //###MasterCity	
        public String? MasterCityId { get; set; }
        public String? MasterCityCode { get; set; }
        public String? MasterCityType { get; set; }
        public String? MasterCityName { get; set; }

    }





}

﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class VCompanyCityRepo : Repo<VCompanyCityEntity>, IVCompanyCityRepo
    {
        public VCompanyCityRepo(DbContextPs ctx) : base(ctx)
        {
        }
    }
}

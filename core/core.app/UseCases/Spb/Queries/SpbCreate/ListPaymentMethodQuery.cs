﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Spb.Queries.SpbCreate
{
    public partial class ListPaymentMethodQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public Int32? companyCityId { get; set; }
    }

    public class ListPaymentMethodQueryHandler : IRequestHandler<ListPaymentMethodQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPaymentMethodQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPaymentMethodQuery req, CancellationToken cancellationToken)
        {
            var checkCorporate = CheckCorporate(req.uiUserProfileEntity.userId);

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

           
            if (checkCorporate == 0) {

                var query = from u in _context.LookupEntitys
                            join p in _context.PaymentMethodEntitys on u.LookupId equals p.LookupId
                            join m in _context.CompanyCityEntitys on p.MasterCityId equals m.CityId
                            where u.LookupKeyParent.Equals("pm") && m.CompanyCityId.Equals(req.companyCityId)
                            select new
                            {
                                u.LookupId
                                , u.LookupKeyParent
                                , u.LookupKey
                                , u.LookupValue
                            };

                 if (query != null)
                    {
                        rtn.Status = StatusCodes.Status200OK;
                        rtn.Data = query.Select(
                            s => new
                            {
                                s.LookupId
                                , s.LookupKeyParent
                                , s.LookupKey
                                , s.LookupValue
                            }).OrderBy(o => o.LookupId);
                    }
            }
            else {
                var query = from u in _context.LookupEntitys
                            where u.LookupKeyParent.Equals("pm") && u.LookupKey.Equals("ta")
                            select new
                            {
                                u.LookupId
                                , u.LookupKeyParent
                                , u.LookupKey
                                , u.LookupValue
                            };

                 if (query != null)
                    {
                        rtn.Status = StatusCodes.Status200OK;
                        rtn.Data = query.Select(
                            s => new
                            {
                                s.LookupId
                                , s.LookupKeyParent
                                , s.LookupKey
                                , s.LookupValue
                            }).OrderBy(o => o.LookupId);
                    }
            }

           

            //
            // return
            return rtn;
        }

        public Int32? CheckCorporate(Int32? UserId) {
            var query = from u in _context.V_DataUserEntitys
                        where u.UserId.Equals(UserId)
                        select new
                        {
                            u.IsCorporate
                        };

            var corporate = query.Select(
                s => new
                {
                    s.IsCorporate
                });

            return corporate.SingleOrDefault().IsCorporate;
        }
    }
}

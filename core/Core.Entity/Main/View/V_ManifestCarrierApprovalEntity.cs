﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_ManifestCarrierApprovalEntity
    {
        public Int32? PrivilegeId { get; set; }
        public Int32? UserId { get; set; }
    }
}

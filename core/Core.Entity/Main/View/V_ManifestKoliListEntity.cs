﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_ManifestKoliListEntity
    {
        public DateTime? CreatedAt { get; set; }
        public Int64? ManifestId { get; set; }
        public String? ManifestNo { get; set; }
        public String? OriginCityId { get; set; }
        public String? OriginCityCode { get; set; }
        public String? OriginCityName { get; set; }
        public String? DestinationCityId { get; set; }
        public String? DestinationCityCode { get; set; }
        public String? DestinationCityName { get; set; }
        public String? KoliAlphabet { get; set; }
        public Int32? KoliNo { get; set; }
        public Int64? SpbId { get; set; }
        public Int64? SpbGoodsId { get; set; }
        public Decimal? Aw { get; set; }
        public Decimal? Caw { get; set; }
        public Decimal? Vw { get; set; }
        public Decimal? Length { get; set; }
        public Decimal? Width { get; set; }
        public Decimal? Height { get; set; }
        public Int64? SpbGoodsItemId { get; set; }
        public String? Alphabet { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SPManifest_MonitoringEntity
    {
        public long? ManifestId { get; set; }
        public Int32? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public String? OriginBranch { get; set; }
        public Int32? SubBranchId { get; set; }
        public String? Carrier { get; set; }
        public Int32? ManifestAlphabet { get; set; }
        public String? Alphabet { get; set; }
        public String? OriginCityCode { get; set; }
        public String? OriginCityName { get; set; }
        public String? DestinationCityCode { get; set; }
        public String? DestinationCityName { get; set; }
        public String? Corporation { get; set; }
        public Int32? TotalKoli { get; set; }
        public Int32? KoliGerai { get; set; }
        public Int32? KoliCorporate { get; set; }
        public Decimal? TotalAw { get; set; }
        public Decimal? TotalCaw { get; set; }
        public Decimal? AwCorporate { get; set; }
        public Decimal? CawCorporate { get; set; }
        public Decimal? AwGerai { get; set; }
        public Decimal? CawGerai { get; set; }
        public Decimal? Ta { get; set; }
        public Decimal? Tt { get; set; }
        public Decimal? Tk { get; set; }
        public Decimal? TotalTaTt { get; set; }

    }
}

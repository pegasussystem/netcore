﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MasterModa.Query
{
    public partial class GetModaCityQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class GetModaCityQueryHandler : IRequestHandler<GetModaCityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetModaCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetModaCityQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.LookupEntitys
                        where a.LookupKeyParent == "carrier"
                        select new
                        {
                            a.LookupId
                            , a.LookupKeyParent
                            , a.LookupKey
                            , a.LookupValue
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.LookupId
                        , s.LookupKeyParent
                        , s.LookupKey
                        , s.LookupValue
                    }).OrderBy(o => o.LookupKey);
            }

            // return
            return rtn;
        }
    }
}
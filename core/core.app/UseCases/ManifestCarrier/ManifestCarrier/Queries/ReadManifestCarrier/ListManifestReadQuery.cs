﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries
{
   public partial class ListManifestReadQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public Int32? originStationId { get; set; }
        public Int32? destinationStationId { get; set; }
    }

    public class ListManifestReadQueryHandler : IRequestHandler<ListManifestReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListManifestReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListManifestReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            DateTime ManifestDate;
            
            DateTime dNow           = DateTime.Now.Date;
            int hNow                = dNow.Hour;

            if (hNow > 11)
            {
                ManifestDate        = dNow;
            }
            else {
                ManifestDate        = dNow.AddDays(-1);
            }


            var query = from vmf in _context.V_ManifestListEntitys
                        where vmf.CreatedAt.Value.Year.Equals(ManifestDate.Year) && vmf.CreatedAt.Value.Month.Equals(ManifestDate.Month) && vmf.CreatedAt.Value.Day.Equals(ManifestDate.Day)
                        where
                            vmf.OriginStationId.Equals(req.originStationId)
                            && vmf.DestinationStationId.Equals(req.destinationStationId)
                            && (vmf.SisaKoli > 0 || vmf.SisaCaw > 0)
                        select new
                        {
                            vmf.CreatedAt
                            , vmf.ManifestId
                            , vmf.ManifestNo
                            , vmf.SumAw
                            , vmf.SumCaw
                            , vmf.CountKoli
                            , vmf.OriginCityId
                            , vmf.OriginCityCode
                            , vmf.OriginCityName
                            , vmf.OriginStationId
                            , vmf.DestinationCityId
                            , vmf.DestinationCityCode
                            , vmf.DestinationCityName
                            , vmf.SisaCaw
                            , vmf.SisaKoli
                            , SisaCawFinal = Convert.ToString(decimal.Round(vmf.SisaCaw, 2)) + "  (" + Convert.ToString(decimal.Round(vmf.SumCaw, 2)) + ")"
                            , SisaKoliFinal = vmf.SisaKoli.ToString() + "  (" + vmf.CountKoli.ToString() + ")"
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CreatedAt
                        , s.ManifestId
                        , s.ManifestNo
                        , s.SumAw
                        , s.SumCaw
                        , s.CountKoli
                        , s.OriginCityId
                        , s.OriginCityCode
                        , s.OriginCityName
                        , s.OriginStationId
                        , s.DestinationCityId
                        , s.DestinationCityCode
                        , s.DestinationCityName
                        , s.SisaCaw
                        , s.SisaKoli
                        , s.SisaCawFinal
                        , s.SisaKoliFinal
                        , CreatedAtLocal = s.CreatedAt.Value.AddHours((double)req.uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((double)req.uiUserProfileEntity.WorkTimeZoneMinute)
                    }).OrderByDescending(o => o.ManifestId);
            }

            //
            // return
            return rtn;
        }
    }
}
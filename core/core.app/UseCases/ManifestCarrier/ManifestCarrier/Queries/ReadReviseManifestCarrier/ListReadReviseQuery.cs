﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadReviseManifestCarrier
{
     public partial class ListReadReviseQuery : IRequest<ReturnFormat>
    {
        public String? manifestCarrierNo { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListReadReviseQueryHandler : IRequestHandler<ListReadReviseQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListReadReviseQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListReadReviseQuery req, CancellationToken cancellationToken)
        {

             // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            var query = from vc in _context.V_ManifestCarrierReviseListEntitys
                        where vc.ManifestCarrierNo.Equals(req.manifestCarrierNo)
                        select new
                        {
                            vc.CreatedAt
                            , vc.CreatedBy
                            , vc.UpdatedAt
                            , vc.UpdatedBy
                            , vc.ManifestCarrierId
                            , vc.ManifestCarrierNo
                            , vc.NoSmu
                            , vc.FullName
                            , vc.TotalFinalWeightSMU
                            , vc.VendorName
                            , vc.FlightDate
                            , vc.FirstFlight
                            , vc.LastFlight
                            , vc.Revise
                            , vc.RevisiStatus
                            , vc.StatRevise
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CreatedAt
                        , s.CreatedBy
                        , s.UpdatedAt
                        , s.UpdatedBy
                        , s.ManifestCarrierId
                        , s.ManifestCarrierNo
                        , s.NoSmu
                        , s.FullName
                        , s.TotalFinalWeightSMU
                        , s.VendorName
                        , s.FlightDate
                        , s.FirstFlight
                        , s.LastFlight
                        , s.Revise
                        , s.RevisiStatus
                        , s.StatRevise
                    }).OrderByDescending(o => o.ManifestCarrierId);
        }

            //
            // return
            return rtn;
        }
    }
}

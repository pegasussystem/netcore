﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class PrivilegeActionEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? PrivilegeActionId { get; set; }
        public Int32? PrivilegeId { get; set; }
        public Int32? MenuActionId { get; set; }
        public String? Value { get; set; }
        public String? Desc { get; set; }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostDestination.Commands
{
    public partial class CostDestinationCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? carrierId { get; set; }
        public Int32? vendorOriginId { get; set; }
        public Int32? vendorDestinationId { get; set; }
        public String? cityDestinationId { get; set; }
        public String? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostDestinationCreateCommandHandler : IRequestHandler<CostDestinationCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostDestinationCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostDestinationCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostDestinationCreate(req.carrierId, req.vendorOriginId, req.vendorDestinationId, req.cityDestinationId, req.baseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostDestinationCreateEntity> CostDestinationCreate(Int32? carrierId, Int32? vendorOriginId, Int32? vendorDestinationId, String? cityDestinationId, String? baseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostDestinationCreate @carrierId='{carrierId}',@vendorOriginId='{vendorOriginId}',@vendorDestinationId='{vendorDestinationId}',@cityDestinationId='{cityDestinationId}', @baseCost='{baseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostDestinationCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

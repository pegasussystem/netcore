﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.SuperVision.Query
{
    public partial class ListHistoryQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListHistoryQueryHandler : IRequestHandler<ListHistoryQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListHistoryQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListHistoryQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.SuperVisionEntitys
                        join b in _context.SpbEntitys on a.SpbId equals b.SpbId
                        join c in _context.UserEntitys on a.PicCreatedBy equals c.UserId
                        join d in _context.UserEntitys on a.ApproveBy equals d.UserId
                        where a.Status > 0
                        select new
                        {
                            a.SuperVisionId
                            , a.SpbId
                            , b.SpbNo
                            , TypeCode = a.Type
                            , Type =
                                 (
                                    a.Type.Equals("void") ? "VOID" :
                                    a.Type.Equals("foc") ? "FOC" :
                                    a.Type.Equals("updatePayment") ? "METODE BAYAR" :
                                    a.Type.Equals("reprint") ? "RE-PRINT" :
                                    "-"
                                 )
                            , a.Reason
                            , PicCreatedBy = c.Username
                            , PicCreatedDate = a.PicCreatedDate.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("dddd, dd MMMM yyyy")
                            , ApproveBy = d.Username
                            , ApproveDate = a.ApproveDate.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("dddd, dd MMMM yyyy")
                            , a.ReasonReject
                            , Status = a.Status > 0 ? (a.Status > 1 ? "Reject" : "Approve") : "-"
                            , ApproveDateOri = a.ApproveDate.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute)
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.SuperVisionId
                        , s.SpbId
                        , s.SpbNo
                        , s.TypeCode
                        , s.Type
                        , s.Reason
                        , s.PicCreatedBy
                        , s.PicCreatedDate
                        , s.ApproveBy
                        , s.ApproveDate
                        , s.ReasonReject
                        , s.Status
                        , s.ApproveDateOri
                    }).OrderByDescending(o => o.ApproveDateOri);
            }


            // return
            return rtn;
        }

    }
}

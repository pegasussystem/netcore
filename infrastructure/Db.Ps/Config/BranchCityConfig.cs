﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class BranchCityConfig : IEntityTypeConfiguration<BranchCityEntity>
    {
        public void Configure(EntityTypeBuilder<BranchCityEntity> builder)
        {
            builder.HasKey(e => e.BranchCityId);
            builder.ToTable("BranchCity");


            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

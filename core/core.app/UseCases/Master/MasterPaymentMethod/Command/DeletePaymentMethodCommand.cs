﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Master.MasterPaymentMethod.Command
{
    public partial class DeletePaymentMethodCommand : IRequest<ReturnFormat>
    {
        public String? masterCityId { get; set; }
        public Int32? lookupId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class DeletePaymentMethodCommandHandler : IRequestHandler<DeletePaymentMethodCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public DeletePaymentMethodCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(DeletePaymentMethodCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.ManageData(req.masterCityId, req.lookupId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_PaymentMethod_DeleteEntity> ManageData(String? masterCityId, Int32? lookupId)
        {
            String query = $@"EXEC SP_PaymentMethod_Delete @MasterCityId={masterCityId}, @LookupId='{lookupId}' ";
            var res = this._context.SP_PaymentMethod_DeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

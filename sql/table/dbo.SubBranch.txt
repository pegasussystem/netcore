/****** Object:  Table [dbo].[SubBranch]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubBranch](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[SubBranchId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NULL,
	[BranchId] [int] NULL,
	[CityIdCode] [int] NULL,
	[AreaIdCode] [int] NULL,
	[SubBranchCode] [varchar](20) NULL,
	[SubBranchName] [varchar](50) NULL,
	[SubBranchAddress] [varchar](200) NULL,
	[SubBranchTimeZone] [varchar](50) NULL,
 CONSTRAINT [PK__SubBranc__D0959F6392DA0824] PRIMARY KEY CLUSTERED 
(
	[SubBranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SubBranch] ADD  CONSTRAINT [DF__SubBranch__Creat__0880433F]  DEFAULT (getutcdate()) FOR [CreatedAt]
GO

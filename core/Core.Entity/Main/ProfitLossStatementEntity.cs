using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Text;

namespace Core.Entity.Main
{
    public class ProfitLossStatementEntity
    {
        public Int64 plsId { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public Int64? arId { get; set; }
        public Int64? revenueId { get; set; }
        public Int64? bukuKasId { get; set; }

    }
}

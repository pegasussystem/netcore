﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;
using Core.Entity.Main.Store_Procedure;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.HistoryAksi.Queries
{

    public partial class HistoryAksiQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfile;

        public String CreatedAt { get; set; }
        //public String Username { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public int Year() { return Int32.Parse(CreatedAt.Split('-')[0]); }
        public int Month() { return Int32.Parse(CreatedAt.Split('-')[1]); }
        public int Day() { return Int32.Parse(CreatedAt.Split('-')[2]); }
    }
    public class HistoryAksiQueryHandler : IRequestHandler<HistoryAksiQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;
        private object year;
        private object month;
        private object day;
        private object userId;

        public HistoryAksiQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(HistoryAksiQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            //IEnumerable<HistoryAksiEntity> entities;
            ReturnFormat rtn = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            {
                rtn.Status = StatusCodes.Status204NoContent;
            }

            //entities = this.ReceiverListData(req.Year(), req.Month(), req.Day(), req.uiUserProfileEntity.DataValue, req.uiUserProfileEntity.companyId, req.uiUserProfileEntity.userId);

            DateTime? utcDate = null;
            if (req.CreatedAt != null)
            {
                // konversi ke UTC
                utcDate = Convert.ToDateTime(req.CreatedAt);
            }


            var query = from a in _context.HistoryActionEntitys

                        join b in _context.UserEntitys on a.UserId equals b.UserId
                        where a.ActionDate.Value.Year.Equals(utcDate.Value.Year) 
                              && a.ActionDate.Value.Month.Equals(utcDate.Value.Month)
                              && a.ActionDate.Value.Day.Equals(utcDate.Value.Day)
                             // && b.Username.Contains(req.Username)
                        select new
                        {
                            b.Username
                            ,
                            a.ActionDate
                            ,
                            a.ActionDesc
                            ,
                            a.Menu

                        };
            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.Username
                        ,
                        s.ActionDate
                        ,
                        s.ActionDesc
                        ,
                        s.Menu

                    }).OrderBy(o => o.ActionDate);
            }

            // return
            return rtn;
        }

        //private IEnumerable<HistoryAksiEntity> ReceiverListData(int year=0, int month=0, int day=0, string dataValue="", int? companyId=0, int? userId=0)
        //{
        //    String query = $@"EXEC List_GoodsMonitoring_Receiver @year={year}, @month={month}, @day= {day}, @dataValue= '{dataValue}', @companyId= {companyId}, @userId= {userId}";
        //    var res = this._context.HistoryAksiEntities.fromsql(query).ToList();
        //    return res;
        //}
    }

}

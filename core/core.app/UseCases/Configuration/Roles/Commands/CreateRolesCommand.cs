﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Roles.Commands
{
    public partial class CreateRolesCommand : IRequest<ReturnFormat>
    {
        public String? rolesName { get; set; }
        public String? description { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateRolesCommandHandler : IRequestHandler<CreateRolesCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateRolesCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateRolesCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CreateRoles(req.rolesName, req.description, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_CreateRolesEntity> CreateRoles(String? rolesName, String? description, Int32? login)
        {
            String query = $@"EXEC SP_CreateRoles @rolesName='{rolesName}', @description='{description}', @loginId='{login}' ";
            var res = this._context.SP_CreateRolesEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

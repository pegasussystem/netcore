/****** Object:  Table [dbo].[CompanyArea]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyArea](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[CompanyAreaId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NULL,
	[CityType] [varchar](10) NULL,
	[MasterCityId] [varchar](10) NULL,
	[MasterSubDistrictId] [varchar](20) NULL,
	[CompanyAreaCode] [varchar](10) NULL,
	[CompanyAreaName] [varchar](50) NULL,
 CONSTRAINT [PK__CompanyA__C4CD021A956816DA] PRIMARY KEY CLUSTERED 
(
	[CompanyAreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CompanyArea] ADD  CONSTRAINT [DF__CompanyAr__Creat__75A278F5]  DEFAULT (getutcdate()) FOR [CreatedAt]
GO
ALTER TABLE [dbo].[CompanyArea]  WITH CHECK ADD  CONSTRAINT [fk_CompanyArea_Company_1] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
ALTER TABLE [dbo].[CompanyArea] CHECK CONSTRAINT [fk_CompanyArea_Company_1]
GO

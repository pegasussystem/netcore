using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace core.app.UseCases.Rates.Delete
{
    public partial class RatesDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? ratesId { get; set; }
        public String? subDistrict { get; set; }



        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class RatesDeleteCommandCommandHandler : IRequestHandler<RatesDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public RatesDeleteCommandCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(RatesDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try{

                RatesEntity entity = new RatesEntity();
                RatesDetailEntity rdEntity = new RatesDetailEntity();
                RatesSubDistrictEntity RatesSubDistrictEntity = new RatesSubDistrictEntity();
                RatesSubDistrictDetailEntity RatesSubDistrictDetailEntity = new RatesSubDistrictDetailEntity();

                var rates = _context.RatesEntitys.Where(w => w.RatesId.Equals(req.ratesId)).FirstOrDefault();
                var ratesDetail = _context.RatesDetailEntitys.Where(w => w.RatesId.Equals(req.ratesId)).FirstOrDefault();
                var ratesSubDistrict = _context.RatesSubDistrictEntitys.Where(w => w.RatesSubDistrictId.Equals(req.ratesId)).FirstOrDefault();
                var ratesSubDistrictDetail = _context.RatesSubDistrictDetailEntitys.Where(w => w.RatesSubDistrictId.Equals(req.ratesId)).FirstOrDefault();

                if (req.subDistrict != null)
                {
                    _context.RatesSubDistrictEntitys.Remove(ratesSubDistrict);

                    if (ratesSubDistrictDetail != null)
                    {
                        _context.RatesSubDistrictDetailEntitys.Remove(ratesSubDistrictDetail);
                    }

                    _context.SaveChanges();

                    var desc = ratesSubDistrict.OriginCity + " ke " + ratesSubDistrict.DestinationCity + " kecamatan " + ratesSubDistrict.DestinationSubDistrictId + ", tarif : " + Convert.ToInt32(ratesSubDistrict.Rates);

                    this.SetHistoryAction(req.uiUserProfileEntity.userId, req.ratesId, "Delete Rates", "Rates", desc);
                }
                else 
                {
                    _context.RatesEntitys.Remove(rates);

                    if (ratesDetail != null)
                    {
                        _context.RatesDetailEntitys.Remove(ratesDetail);
                    }

                    _context.SaveChanges();

                    var desc = rates.OriginCity + " ke " + rates.DestinationCity + ", tarif : " + Convert.ToInt32(rates.Rates);

                    this.SetHistoryAction(req.uiUserProfileEntity.userId, req.ratesId, "Delete Rates", "Rates", desc);
                }

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = rates;

                //
                // return
                return rtn;
            }
            catch (Exception ex) {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu = Menu;
            dataItem.Desc = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }

    }
}

using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace core.app.UseCases.Spb.Commands.Void
{
    public partial class RatesCommand : IRequest<ReturnFormat>
    {
        public Int64? SpbId { get; set; }
        public string VoidReason { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class VoidCommandHandler : IRequestHandler<RatesCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public VoidCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(RatesCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try{
                //cek key exists di entity
                var entity = await _context.SpbEntitys.FindAsync(req.SpbId);

                if (entity == null)
                {
                    return rtn;
                }

                if (req.uiUserProfile.privilegeId != 1)
                {

                    if (entity.CreatedAt.Value.ToLocalTime().Day != DateTime.UtcNow.ToLocalTime().Day)
                    {
                        return rtn;
                    }
                }

                //
                // logic
                entity.VoidReason = req.VoidReason;

                this.SPSpb_Void(entity, req.uiUserProfile.userId);
                this.SetHistoryAction(req.uiUserProfile.userId, entity.SpbId, "void", "SPB List Sender", entity.VoidReason);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;

                //
                // return
                return rtn;
            }
            catch (Exception ex) {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SPSpb_VoidEntity> SPSpb_Void(SpbEntity entity, int? userId)
        {
            String query = $@"EXEC SPSpb_Void @SpbId={entity.SpbId}, @VoidBy={userId}, @VoidReason='{entity.VoidReason}'";
            var res =  this._context.SPSpb_VoidEntitiys.FromSqlRaw(query).ToList();
            return res;
        }

        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc) {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId     = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId   = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu       = Menu;
            dataItem.Desc       = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }

    }
}

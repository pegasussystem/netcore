using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CoaCodeConfig : IEntityTypeConfiguration<CoaCodeEntity>
    {
        public void Configure(EntityTypeBuilder<CoaCodeEntity> builder)
        {
            builder.HasKey(e => e.CoaId);
            builder.ToTable("CoaCode");
        }
    }
}

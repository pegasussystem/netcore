﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_GetManifestCarrierListManifestEntity
    {
        public String? NoSmu { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? FlightDate { get; set; }
        public Decimal? TotalFinalWeightSMU { get; set; }
        public String? FirstFlightNo { get; set; }
        public String? LastFlightNo { get; set; }
    }
}

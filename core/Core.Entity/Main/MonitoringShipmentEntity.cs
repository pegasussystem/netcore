﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MonitoringShipmentEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? MonitoringShipmentId { get; set; }
        public String? Via { get; set; }
        public String? OriginCityId { get; set; }
        public String? DestinationCityId { get; set; }
        public String? SttPegasus { get; set; }
        public String? SttLP { get; set; }
        public String? SmuNo { get; set; }
        public Int32? Koli { get; set; }
        public Decimal? Caw { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? FlightId { get; set; }
        public DateTime? FlightDate { get; set; }
        public DateTime? Etd { get; set; }
        public DateTime? Eta { get; set; }
        public String? Status { get; set; }
        public String? Pod { get; set; }
        public DateTime? PodDate { get; set; }

    }

}

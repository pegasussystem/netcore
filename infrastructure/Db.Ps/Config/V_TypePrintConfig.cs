﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_TypePrintConfig : IEntityTypeConfiguration<V_TypePrintEntity>
    {
        public void Configure(EntityTypeBuilder<V_TypePrintEntity> builder)
        {
            builder.HasKey(e => e.code);
            builder.ToTable("V_TypePrint");


        }
    }
}

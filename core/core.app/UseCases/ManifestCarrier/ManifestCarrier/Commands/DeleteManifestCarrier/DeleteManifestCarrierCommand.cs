﻿using core.app.Common.Interfaces;
using core.app.Helpers;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Commands.DeleteManifestCarrier
{
   public partial class DeleteManifestCarrierCommand : IRequest<ReturnFormat>
    {
        public Int32? manifestCarrierId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class DeleteManifestCarrierCommandHandler : IRequestHandler<DeleteManifestCarrierCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public DeleteManifestCarrierCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(DeleteManifestCarrierCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;
            //
            // set variable
            PrivilegeData privilegeData;
            
            // --- --- ---
            // Privilege
            // --- --- ---
            privilegeData = new PrivilegeData
            {
                SubBranch_Delete = false,
                Branch_Delete = true,
                Ho_Delete = true
            };
            if (H_Privilege.Delete(req.uiUserProfile, privilegeData) == false) { return rtn; }


            // --- --- ---
            // logic
            // --- --- ---

            // delete biaya - biaya ( manifest carrier cost )
            var dataCost = _context.ManifestCarrierCostEntitys.Where(w => w.ManifestCarrierId.Equals(req.manifestCarrierId)).ToList();
            _context.ManifestCarrierCostEntitys.RemoveRange(dataCost);
            _context.SaveChanges();

            // delete manifest carrier item
            var dataItem = _context.ManifestCarrierItemEntitys.Where(w => w.ManifestCarrierId.Equals(req.manifestCarrierId)).ToList();
            _context.ManifestCarrierItemEntitys.RemoveRange(dataItem);
            _context.SaveChanges();

            // delete manifest carier
            var dataCarrier = _context.ManifestCarrierEntitys.Where(w => w.ManifestCarrierId.Equals(req.manifestCarrierId)).ToList();
            _context.ManifestCarrierEntitys.RemoveRange(dataCarrier);
            _context.SaveChanges();

            this.SetHistoryAction(req.uiUserProfile.userId, req.manifestCarrierId, "Delete Manifest Carrier", "Manifest Carrier", "");

            // --- --- ---
            // return
            // --- --- ---
            rtn.Status = StatusCodes.Status200OK;
            return rtn;
        }

        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu = Menu;
            dataItem.Desc = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }
    }

}
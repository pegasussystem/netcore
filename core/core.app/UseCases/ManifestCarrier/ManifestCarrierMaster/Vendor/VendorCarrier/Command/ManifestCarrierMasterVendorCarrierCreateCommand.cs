﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorCarrier.Command
{
    public partial class ManifestCarrierMasterVendorCarrierCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? vendorId { get; set; }
        public Int32? carrierId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterVendorCarrierCreateCommandHandler : IRequestHandler<ManifestCarrierMasterVendorCarrierCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterVendorCarrierCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterVendorCarrierCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.VendorCarrierCreate(req.vendorId, req.carrierId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_VendorCarrierCreateEntity> VendorCarrierCreate(Int32? vendorId, Int32? carrierId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_VendorCarrierCreate @vendorId='{vendorId}', @carrierId='{carrierId}'";
            var res = this._context.SP_ManifestCarrierMaster_VendorCarrierCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
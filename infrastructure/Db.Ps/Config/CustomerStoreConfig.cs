﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class CustomerStoreConfig : IEntityTypeConfiguration<CustomerStoreEntity>
    {
        public void Configure(EntityTypeBuilder<CustomerStoreEntity> builder)
        {
            builder.HasKey(e => e.CustomerStoreId);
            builder.ToTable("CustomerStore");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListCityOriginReadQuery : IRequest<ReturnFormat>
    {
        public Int32? stationId { get; set; }
    }

    public class ListCityOriginReadQueryHandler : IRequestHandler<ListCityOriginReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCityOriginReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCityOriginReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from sc in _context.StationCityEntitys
                        join mc in _context.MasterCityEntitys on sc.CityId equals mc.MasterCityId
                        where sc.StationId.Equals(req.stationId) && sc.Priority.Equals("1")
                        select new
                        {
                            OriginCityId = sc.CityId
                           , OriginCityName = 
                            (
                                sc.CityId == "31.75" ? "Jakarta" : sc.CityId == "31.73" ? "Jakarta" : mc.MasterCityName
                            )
                        };


            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(s => new { s.OriginCityId, s.OriginCityName });
            }

            //
            // return
            return rtn;
        }
    }
}

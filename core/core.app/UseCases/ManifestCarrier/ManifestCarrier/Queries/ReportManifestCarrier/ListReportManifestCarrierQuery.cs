﻿using core.app.Common.Interfaces;
using core.app.Helpers;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReportManifestCarrier
{
   public partial class ListReportManifestCarrierQuery : IRequest<ReturnFormat>
    {
        public Int32 flightDateUtcYear { get; set; }
        public Int32 flightDateUtcMonth { get; set; }
        public Int32 flightDateUtcDay { get; set; }
        public Int32 flightDateUtcHour { get; set; }
        public Int32 flightDateUtcMinute { get; set; }
        public Int32 flightDateUtcSecond { get; set; }
        public Int32 flightDateUtcMillisecond { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public DateTime FlightDateUtc()
        {
            string dateString = $"{flightDateUtcYear}-{(flightDateUtcMonth < 10 ? "0" : "")}{flightDateUtcMonth}-{(flightDateUtcDay < 10 ? "0" : "")}{flightDateUtcDay} {(flightDateUtcHour < 10 ? "0" : "")}{flightDateUtcHour}:{(flightDateUtcMinute < 10 ? "0" : "")}{flightDateUtcMinute}:{(flightDateUtcSecond < 10 ? "0" : "")}{flightDateUtcSecond}.000";
            string format = "yyyy-MM-dd HH:mm:ss.fff";
            DateTime dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
            dateTime = dateTime.AddMilliseconds(flightDateUtcMillisecond);
            return dateTime;
        }
    }

    public class ListReportManifestCarrierQueryHandler : IRequestHandler<ListReportManifestCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListReportManifestCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListReportManifestCarrierQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            PrivilegeData privilegeData;

            // --- --- ---
            // Privilege
            // --- --- ---
            privilegeData = new PrivilegeData
            {
                SubBranch_Read = false,
                Branch_Read = true,
                Ho_Read = true
            };
            if (H_Privilege.Read(user: req.uiUserProfileEntity, privilegeData: privilegeData) == false) { return rtn; }

            // --- --- ---
            // logic
            // --- --- ---


            // --- --- ---
            // variable
            // --- --- ---
            DateTime _flightDate        = req.FlightDateUtc();
            DateTime _flightDateStart   = new DateTime(_flightDate.Year, _flightDate.Month, _flightDate.Day);
            DateTime _flightDateEnd     = new DateTime(_flightDate.Year, _flightDate.Month, _flightDate.Day, 23, 59, 59);

            // get data from ManifestCarrier
            // --- --- ---
            var query = from mci in _context.ManifestCarrierItemEntitys
                        join mce in _context.ManifestCarrierEntitys on mci.ManifestCarrierId equals mce.ManifestCarrierId
                        join mcc in _context.ManifestCarrierCostEntitys on mce.ManifestCarrierId equals mcc.ManifestCarrierId
                        //join dengan master
                        join m in _context.ManifestEntitys on mci.ManifestId equals m.ManifestId
                        join a in _context.AlphabetEntitys on m.ManifestAlphabet equals a.AlphabetId
                        //get data origin
                        join cc_ori in _context.CompanyCityEntitys on m.OriginCityCode equals cc_ori.CompanyCityId
                        join mc_ori in _context.MasterCityEntitys on cc_ori.CityId equals mc_ori.MasterCityId
                        //get data destination
                        join cc_dest in _context.CompanyCityEntitys on m.DestinationCityCode equals cc_dest.CompanyCityId
                        join mc_dest in _context.MasterCityEntitys on cc_dest.CityId equals mc_dest.MasterCityId
                        //get data lookup
                        join l_type in _context.LookupEntitys on mcc.CostType equals l_type.LookupKey
                        join l_group in _context.LookupEntitys on mcc.CostGroup equals l_group.LookupKey

                        where (
                           mce.FlightDate.Value.AddHours((double)req.uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((double)req.uiUserProfileEntity.WorkTimeZoneMinute) >= _flightDateStart
                           && mce.FlightDate.Value.AddHours((double)req.uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((double)req.uiUserProfileEntity.WorkTimeZoneMinute) <= _flightDateEnd)
                        select new
                        {
                            ManifestCarrierCostId   = mcc.ManifestCarrierCostId
                            , NoSmu                 = mce.NoSmu
                            , NoManifest            = mc_dest.MasterCityCode + " " + a.Alphabet + " #" + m.ManifestId.ToString()
                            
                            , CostGroup             = l_group.LookupValue
                            , CostType              = l_type.LookupValue
                             
                            , Origin                = mc_ori.MasterCityCode
                            , Destination           = mc_dest.MasterCityCode
                             
                            , FlightDateLocal       = mce.FlightDate.Value.AddHours((double)req.uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((double)req.uiUserProfileEntity.WorkTimeZoneMinute).ToString("yyyy-MMM-dd")
                            , FlightDateUtc         = mce.FlightDate

                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.ManifestCarrierCostId
                        , s.NoSmu
                        , s.NoManifest
                        , s.CostGroup
                        , s.CostType
                        , s.Origin
                        , s.Destination
                        , s.FlightDateLocal
                        , s.FlightDateUtc
                    }).OrderByDescending(o => o.ManifestCarrierCostId);
            }

            //
            // return
            return rtn;
        }
    }
}

﻿using core.app.UseCases.Privillege.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Privilege
{
    [Authorize]
    [Route("privilege-menu-action")]
    [ApiController]
    public class ListPrivilegeMenuActionOriController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ListPrivilegeMenuActionOriController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list-menu-action")]
        public async Task<IActionResult> List([FromQuery] ListPrivilegeMenuActionOriQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

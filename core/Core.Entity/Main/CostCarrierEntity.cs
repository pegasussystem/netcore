﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class CostCarrierEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? CostCarrierId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? OriginStationId { get; set; }
        public Int32? FirstFlightId { get; set; }
        public Int32? LastFlightId { get; set; }
        public Int32? VendorCarrierId { get; set; }
        public Int32? DestinationStationId { get; set; }
    }
}

using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using core.app.Common.Interfaces;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.AccountReceivable.Queries
{
    public partial class ListAccountReceivableQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class ListAccountReceivableQueryHandler : IRequestHandler<ListAccountReceivableQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListAccountReceivableQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListAccountReceivableQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_ARListEntity> entities;

            // DateTime utcDate = req.CreatedAt.Value.AddHours((double)(-1 * req.uiUserProfile.WorkTimeZoneHour)).AddMinutes((double)(req.uiUserProfile.WorkTimeZoneMinute * -1));


            entities = this.DataAccountReceivableList();

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                         s.arId
                         ,
                         s.createdAt
                         ,
                         s.keterangan
                         ,
                         s.coaCode
                         ,
                         s.coaName
                         ,
                         s.debit
                         ,
                         s.credit
                         ,
                         s.saldo
                         ,
                         s.cumulativeBalance
                     });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_ARListEntity> DataAccountReceivableList()
        {
            String query = $@"EXEC SP_ARList";
            var res = this._context.SP_ARListEntities.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
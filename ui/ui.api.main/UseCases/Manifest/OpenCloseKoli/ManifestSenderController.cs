﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.Manifest.OpenCloseKoli;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ui.api.main.UseCases.Manifest.OpenCloseKoli
{
    [Authorize]
    [Route("manifest")]
    [ApiController]
    public class ManifestSenderController : DefaultController
    {
        private readonly IAppWrapperPs _appPs;
        public ManifestSenderController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpPost("openclosekoli")]
        public async Task<IActionResult> Execute([FromBody] OpenClose_Koli_Input input)
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();

            // logic
            // ///// ///// /////
            _returnFormat = _appPs.OpenClose_Koli_UseCase.Update(GetUserClaim(), input);

            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostGerai.Commands
{
    public partial class CostGeraiDeleteCommand : IRequest<ReturnFormat>
    {
        public String? costGeraiId { get; set; }
        public String? costGeraiDetailId { get; set; }
        public String? manifestOriginCityId { get; set; }
        public String? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostGeraiDeleteCommandHandler : IRequestHandler<CostGeraiDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostGeraiDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostGeraiDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostGeraiDelete(req.costGeraiId, req.costGeraiDetailId, req.manifestOriginCityId, req.baseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostGeraiDeleteEntity> CostGeraiDelete(String? costGeraiId, String? costGeraiDetailId, String? manifestOriginCityId, String? baseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostGeraiDelete @costGeraiId='{costGeraiId}', @costGeraiDetailId='{costGeraiDetailId}', @manifestOriginCityId='{manifestOriginCityId}', @baseCost='{baseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostGeraiDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
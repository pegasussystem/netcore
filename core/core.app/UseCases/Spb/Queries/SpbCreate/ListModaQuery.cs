﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Spb.Queries.SpbCreate
{
    public partial class ListModaQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public Int32? destinationCityId { get; set; }
    }

    public class ListModaQueryHandler : IRequestHandler<ListModaQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListModaQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListModaQuery req, CancellationToken cancellationToken)
        {
            Int32? originCityId = GetOriginCityId(req.uiUserProfileEntity.userId);

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from u in _context.LookupEntitys
                        join p in _context.ModaCityEntitys on u.LookupId equals p.LookupId
                        join m in _context.CompanyCityEntitys on p.OriginCityId equals m.CityId
                        join n in _context.CompanyCityEntitys on p.DestinationCityId equals n.CityId
                        where u.LookupKeyParent.Equals("carrier") && m.CompanyCityId.Equals(originCityId) && n.CompanyCityId.Equals(req.destinationCityId)
                        select new
                        {
                            u.LookupId
                            , u.LookupKeyParent
                            , u.LookupKey
                            , u.LookupValue
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.LookupId
                        , s.LookupKeyParent
                        , s.LookupKey
                        , s.LookupValue
                    }).OrderBy(o => o.LookupId);
            }

            //
            // return
            return rtn;
        }

        public Int32? GetOriginCityId(Int32? userId) {
            var query = from u in _context.V_DataUserEntitys
                        where u.UserId.Equals(userId)
                        select new
                        {
                            u.CityIdCode
                        };


            var cityId = query.Select(
                s => new
                {
                    s.CityIdCode
                });

            return cityId.SingleOrDefault().CityIdCode;
        }
    }
}

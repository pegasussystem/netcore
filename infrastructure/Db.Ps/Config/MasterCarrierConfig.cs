﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class MasterCarrierConfig : IEntityTypeConfiguration<MasterCarrierEntity>
    {
        public void Configure(EntityTypeBuilder<MasterCarrierEntity> builder)
        {
            builder.HasKey(e => e.MasterCarrierId);
            builder.ToTable("MasterCarrier");

        }
    }
}

using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CoaParentConfig : IEntityTypeConfiguration<CoaParentEntity>
    {
        public void Configure(EntityTypeBuilder<CoaParentEntity> builder)
        {
            builder.HasKey(e => e.CoaParentId);
            builder.ToTable("CoaParent");
        }
    }
}

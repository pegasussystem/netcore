﻿using core.app.UseCases.UserSetting.ResetPassword.Query;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.UserSetting.ResetPassword
{
    [Authorize]
    [Route("user-setting")]
    [ApiController]

    public class RatesController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public RatesController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list-user-password")]
        public async Task<IActionResult> ListUser([FromQuery] ListUserQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

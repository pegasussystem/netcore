﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ui.api.main.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {

        private readonly IAppWrapperPs _appPs;
        public CompanyController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpGet("city")]
        public async Task<IActionResult> GetCity()
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // get data
            _returnFormat = _appPs.CompanyApp.ReadVCompanyCity();

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("area")]
        public async Task<IActionResult> GetArea(String cityId="")
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            //
            // set variable
            uiUserProfileEntity = GetUserClaim();
            _returnFormat = _appPs.CompanyApp.ReadVCompanyArea(cityId, uiUserProfileEntity);

          


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        UiUserProfileEntity GetUserClaim()
        {
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            // get claim user
            var claims = User.Claims.Where(w => w.Type.Equals("userProfile")).First().Value;
            uiUserProfileEntity = JsonConvert.DeserializeObject<UiUserProfileEntity>(claims);

            return uiUserProfileEntity;
        }

    }
}
using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class AccountReceivableConfig : IEntityTypeConfiguration<AccountReceivableEntity>
    {
        public void Configure(EntityTypeBuilder<AccountReceivableEntity> builder)
        {
            builder.HasKey(e => e.arId);
            builder.ToTable("AccountReceivable");
        }
    }
}

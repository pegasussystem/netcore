﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class MasterUrbanRepo : Repo<MasterUrbanEntity>, IMasterUrbanRepo
    {
        public MasterUrbanRepo(DbContextPs ctx) : base(ctx)
        {
        }


        public IEnumerable<MasterUrbanEntity> ExecWithStoreProcedure(string query, params object[] parameters)
        {
            //return this._contextPs.Query<CompanyEntity>(query, parameters);
            return this._contextPs.Query<MasterUrbanEntity>();
        }
    }
}

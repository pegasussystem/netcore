﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Spb.Queries.ListSender
{
   public partial class DaysQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class DaysQueryHandler : IRequestHandler<DaysQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public DaysQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(DaysQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query2 = from b in _context.PrivilegeUserEntitys
                         where b.UserId.Equals(req.uiUserProfileEntity.userId)
                         select new
                         {
                            Days =  b.PrivilegeId == 1 ? 99999999 : 
                                    //b.PrivilegeId == 2 ? 14 :
                                    //b.PrivilegeId == 3 ? 14 :
                                    //b.PrivilegeId == 4 ? 14 :
                                    //b.PrivilegeId == 5 ? 14 :
                                    //b.PrivilegeId == 6 ? 14 :
                                    //b.PrivilegeId == 7 ? 14 :
                                    //b.PrivilegeId == 8 ? 14 : 
                                    14
                         };

            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(s => new
                            {
                                s.Days
                            }).OrderByDescending(o => o.Days);
            }


            //
            // return
            return rtn;
        }
    }
}

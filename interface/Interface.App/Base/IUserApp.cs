﻿using core.helper;
using Core.Entity.Base;
using System;
using System.Dynamic;

namespace Interface.App.Base
{
    public interface IUserApp : IApp<UserEntity>
    {
        ReturnFormat Login(UserEntity entity,out String refreshToken);
        ReturnFormat RefreshToken(String refreshToken, out String refreshTokenOut);
    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
   class CostPenerusConfig : IEntityTypeConfiguration<CostPenerusEntity>
    {
        public void Configure(EntityTypeBuilder<CostPenerusEntity> builder)
        {
            builder.HasKey(e => e.CostPenerusId);
            builder.ToTable("CostPenerus");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

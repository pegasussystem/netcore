﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostDestination.Commands
{
    public partial class CostDestinationDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? costDestinationId { get; set; }
        public Int32? costDestinationDetailId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostDestinationDeleteCommandHandler : IRequestHandler<CostDestinationDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostDestinationDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostDestinationDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostDestinationDelete(req.costDestinationId, req.costDestinationDetailId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostDestinationDeleteEntity> CostDestinationDelete(Int32? costDestinationId, Int32? costDestinationDetailId)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostDestinationDelete @costDestinationId='{costDestinationId}', @costDestinationDetailId='{costDestinationDetailId}'";
            var res = this._context.SP_ManifestCarrierCost_CostDestinationDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
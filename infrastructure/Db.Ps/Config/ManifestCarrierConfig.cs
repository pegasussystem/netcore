﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class ManifestCarrierConfig : IEntityTypeConfiguration<ManifestCarrierEntity>
    {
        public void Configure(EntityTypeBuilder<ManifestCarrierEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierId);
            builder.ToTable("ManifestCarrier");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace core.app.UseCases.Manifest.OpenCloseKoli
{
    public class OpenClose_Koli_Input
    {
        public String Action { get; set; }
        public Int64 manifestKoliId { get; set; }

        public Byte GetAction()
        {
            if (this.Action == "open")
            {
                return 0;
            }
            if (this.Action == "close")
            {
                return 1;
            }

            return 0;
        }
    }
}

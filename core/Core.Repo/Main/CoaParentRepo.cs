using Core.Entity.Main;
using Core.Entity.Main.View;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Repo.Main
{
    public class CoaParentRepo : Repo<CoaParentEntity>, ICoaparentRepo
    {
        public CoaParentRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public async Task<IEnumerable<CoaParentEntity>> GetAllAsync()
        {
            return await Task.Run(() => _contextPs.Set<CoaParentEntity>().ToList());
        }

    }
}

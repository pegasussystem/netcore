﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class PrivilegeActionConfig : IEntityTypeConfiguration<PrivilegeActionEntity>
    {
        public void Configure(EntityTypeBuilder<PrivilegeActionEntity> builder)
        {
            builder.HasKey(e => e.PrivilegeActionId);
            builder.ToTable("PrivilegeAction");
        }
    }
}
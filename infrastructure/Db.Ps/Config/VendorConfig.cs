﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VendorConfig : IEntityTypeConfiguration<VendorEntity>
    {
        public void Configure(EntityTypeBuilder<VendorEntity> builder)
        {
            builder.HasKey(e => e.VendorId);
            builder.ToTable("Vendor");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
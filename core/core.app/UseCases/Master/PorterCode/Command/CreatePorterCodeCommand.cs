﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Master.PorterCode.Command
{
    public partial class CreatePorterCodeCommand : IRequest<ReturnFormat>
    {
        public String? phone { get; set; }
        public String? name { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreatePorterCodeCommandHandler : IRequestHandler<CreatePorterCodeCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreatePorterCodeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreatePorterCodeCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.ManageData(req.phone, req.name, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_PorterCode_CreateEntity> ManageData(String? phone, String? name, Int32? userId)
        {
            String query = $@"EXEC SP_PorterCode_Create @phone='{phone}', @name='{name}', @UserId={userId} ";
            var res = this._context.SP_PorterCode_CreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
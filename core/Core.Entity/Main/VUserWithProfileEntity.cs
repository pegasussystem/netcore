﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class VUserWithProfileEntity
    {
        public Int32? UserId { get; set; }
        public String? Username { get; set; }
        public String? Email { get; set; }
        public Int32? UserAtId { get; set; }
        public String? WorkType { get; set; }

        //public Int32? #####Company	 { get; set; }
        public Int32? CompanyCityId { get; set; }
        public Int32? CompanyAreaId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? CompanyName { get; set; }
        public String? CompanyAddress { get; set; }
        public String? CompanyTimeZone { get; set; }


        //public Int32? #####Branch	 { get; set; }
        public Int32? BranchCityIdCode { get; set; }
        public Int32? BranchAreaIdCode { get; set; }
        public Int32? BranchId { get; set; }
        public String? BranchName { get; set; }
        public String? BranchAddress { get; set; }
        public String? BranchTimeZone { get; set; }


        //public Int32? #####SubBranch	 { get; set; }
        public Int32? SubBranchCityIdCode { get; set; }
        public Int32? SubBranchAreaIdCode { get; set; }
        public Int32? SubBranchId { get; set; }
        public String? SubBranchName { get; set; }
        public String? SubBranchAddress { get; set; }
        public String? SubBranchTimeZone { get; set; }


        public String? RefreshToken { get; set; }

    }





}

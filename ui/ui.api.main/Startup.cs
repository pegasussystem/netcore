using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AspNetCore.RouteAnalyzer;
using core.app;
using Db.Ps;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace ui.api.main
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();


            #region ServiceExtensions
            services.ConfigureAppPs();
            services.ConfigureRepoPs();
            services.ConfigureCors();
            services.ConfigureContextPs(Configuration);
            services.ConfigureLogSerilog(Configuration);
            services.ConfigureJwt(Configuration);

            services.AddApplication();
            services.AddInfrastructure(Configuration);

            // https://www.talkingdotnet.com/disable-automatic-model-state-validation-in-asp-net-core-2-1/
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddRouteAnalyzer();

            #endregion ServiceExtensions



            //
            // using ID
            ValidatorOptions.LanguageManager.Culture = new CultureInfo("id");

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            // who are you ?
            app.UseAuthentication();

            // are you allowed ?
            app.UseAuthorization();

            // TODO : configurable
            app.UseCors("CorsPolicy");
             

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

            

            });
        }
    }
}

﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_FlightRouteConfig : IEntityTypeConfiguration<V_FlightRouteEntity>
    {
        public void Configure(EntityTypeBuilder<V_FlightRouteEntity> builder)
        {
            builder.HasKey(e => e.FlightRouteId);
            builder.ToTable("V_FlightRoute");
        }
    }
}

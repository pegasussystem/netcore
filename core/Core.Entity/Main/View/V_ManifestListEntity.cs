﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_ManifestListEntity
    {
        public DateTime? CreatedAt { get; set; }
        public Int64? ManifestId { get; set; }
        public Int32? CityIdCode { get; set; }
        public String? ManifestNo { get; set; }
        public Decimal SumAw { get; set; }
        public Decimal SumCaw { get; set; }
        public Int32? CountKoli { get; set; }
        public String? OriginCityId { get; set; }
        public String? OriginCityCode { get; set; }
        public String? OriginCityName { get; set; }
        public Int32? OriginStationId { get; set; }
        public String? DestinationCityId { get; set; }
        public String? DestinationCityCode { get; set; }
        public String? DestinationCityName { get; set; }
        public Int32? DestinationStationId { get; set; }
        public Decimal SisaCaw { get; set; }
        public Int32? SisaKoli { get; set; }

    }
}

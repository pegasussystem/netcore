﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.SpbUpdate.Query
{
     public partial class GetDataVolumeQuery : IRequest<ReturnFormat>
    {
        public Int64? spbId { get; set; }
        public String? totalKoli { get; set; }
        public String? aw { get; set; }
        public String? height { get; set; }
        public String? width { get; set; }
        public String? length { get; set; }
        public String? carrier { get; set; }
        public String? ratesType { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class GetDataVolumeQueryHandler : IRequestHandler<GetDataVolumeQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetDataVolumeQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetDataVolumeQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_CawVolumeEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataCawVolume(req.spbId, req.totalKoli, req.aw, req.height, req.width, req.length, req.carrier, req.ratesType);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                         s.Volume
                         , s.CawFinal
                     });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_CawVolumeEntity> DataCawVolume(Int64? SpbId, String? TotalKoli, String? Aw, String? Height, String? Width, String? Lenght, String? Carrier, String? RatesType)
        {
            String query = $@"EXEC SP_CawVolume @spbId = '{SpbId}', @totalKoli = '{TotalKoli}', @aw = '{Aw.Replace(",", ".")}', @height = '{Height.Replace(",", ".")}', @width = '{Width.Replace(",", ".")}', @lenght = '{Lenght.Replace(",", ".")}', @carrier = '{Carrier}', @ratesType = '{RatesType}' ";
            var res = this._context.SP_CawVolumeEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
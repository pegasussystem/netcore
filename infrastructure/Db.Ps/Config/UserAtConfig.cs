﻿using Core.Entity.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class UserAtConfig : IEntityTypeConfiguration<UserAtEntity>
    {
        public void Configure(EntityTypeBuilder<UserAtEntity> builder)
        {
            builder.HasKey(e => e.UserAtId);
            builder.ToTable("UserAt");
        }
    }
}

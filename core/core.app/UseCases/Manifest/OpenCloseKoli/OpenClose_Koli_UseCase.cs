﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Main.Manifest;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace core.app.UseCases.Manifest.OpenCloseKoli
{
    class OpenClose_Koli_UseCase : I_OpenClose_Koli_UseCase
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public OpenClose_Koli_UseCase(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Update(UiUserProfileEntity uiUserProfileEntity, object req)
        {
            // variable
            // ///// ///// /////
            OpenClose_Koli_Input input = req as OpenClose_Koli_Input;

            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status400BadRequest
            };

            // logic
            // ///// ///// /////

            // get  
            ManifestKoliEntity entity = _repoPs.ManifestKoli_Repo.Read(w => w.ManifestKoliId.Equals(input.manifestKoliId)).FirstOrDefault();

            // update
            if (entity != null)
            {
                entity.IsClose = input.GetAction();
                _repoPs.ManifestKoli_Repo.Update(entity);
                _repoPs.ManifestKoli_Repo.Save();
            }



            // return
            // ///// ///// /////
            if (entity != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;
            }
            return rtn;
        }

        public ReturnFormat Create(ManifestKoliEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestKoliEntity Delete(ManifestKoliEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(ManifestKoliEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestKoliEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public ManifestKoliEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        

        public ReturnFormat Update(ManifestKoliEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

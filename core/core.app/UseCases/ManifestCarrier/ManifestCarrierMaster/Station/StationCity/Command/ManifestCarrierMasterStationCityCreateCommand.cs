﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Station.StationCity.Command
{
    public partial class ManifestCarrierMasterStationCityCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? stationId { get; set; }
        public String? cityId { get; set; }
        public Int32? statusId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterStationCityCreateCommandHandler : IRequestHandler<ManifestCarrierMasterStationCityCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterStationCityCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterStationCityCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                if (req.statusId == null) {
                    req.statusId = 2;
                }

                // Mark as Changed
                var res = this.StationCityCreate(req.stationId, req.cityId, req.statusId, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_StationCityCreateEntity> StationCityCreate(Int32? StationId, String? CityId, Int32? StatusId, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_StationCityCreate @stationId='{StationId}', @cityId='{CityId}', @statusId='{StatusId}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierMaster_StationCityCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

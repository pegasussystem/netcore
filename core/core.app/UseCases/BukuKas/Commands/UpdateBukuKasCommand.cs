using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.BukuKas.Commands
{
    public partial class UpdateBukuKasCommand : IRequest<ReturnFormat>
    {
        public Int64? bukuKasId { get; set; }
        public Int64? coaId { get; set; }
        public String? keterangan { get; set; }
        public String? noReferensi { get; set; }
        public Decimal? debit { get; set; }
        public Decimal? credit { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class UpdateBukuKasCommandHandler : IRequestHandler<UpdateBukuKasCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdateBukuKasCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdateBukuKasCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.UpdateBukuKas(req.bukuKasId,req.coaId, req.keterangan, req.noReferensi, req.debit, req.credit);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_UpdateBukuKasEntity> UpdateBukuKas(Int64? bukuKasId,Int64? coaId, String? keterangan, String? noReferensi, Decimal? debit, Decimal? credit)
        {
            String query = $@"EXEC SP_UpdateBukuKas @bukuKasId ={bukuKasId},  @coaId={coaId}, @keterangan = '{keterangan}',@noReferensi='{noReferensi}', @debit={debit}, @credit={credit}";
            var res = this._context.SP_UpdateBukuKasEntities.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

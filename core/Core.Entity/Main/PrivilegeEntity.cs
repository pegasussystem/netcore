﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class PrivilegeEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? PrivilegeId { get; set; }
        public String? PrivilegeName { get; set; }
        public Int32? OrderCode { get; set; }
        public Boolean? IsActive { get; set; }
    }
}

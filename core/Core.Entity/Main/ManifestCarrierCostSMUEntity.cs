﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class ManifestCarrierCostSMUEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }

        public Int32? ManifestCarrierCostSMUId { get; set; }
        public Int32? ManifestCarrierId { get; set; }

        public String? CostType { get; set; }
        public Decimal? BaseCost { get; set; }
        public String? Unit { get; set; }
        public String? CostGroup { get; set; }
        public String? UseWeight { get; set; }
        public Decimal? FinalWeight { get; set; }
        public Decimal? TotalCost { get; set; }
        public Int32? Key { get; set; }
        public String? Desc { get; set; }
    }
}

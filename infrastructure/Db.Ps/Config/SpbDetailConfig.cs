﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class SpbDetailConfig : IEntityTypeConfiguration<SpbDetailEntity>
    {
        public void Configure(EntityTypeBuilder<SpbDetailEntity> builder)
        {
            builder.HasKey(e => e.SpbDetailId);
            builder.ToTable("SpbDetail");
        }
    }
}

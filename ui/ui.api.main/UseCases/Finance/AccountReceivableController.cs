using System.Threading.Tasks;
using core.app.UseCases.AccountReceivable.Commands;
using core.app.UseCases.AccountReceivable.Queries;
using core.app.UseCases.BukuKas.Commands;
using core.app.UseCases.BukuKas.Queries;
using core.app.UseCases.GeneralLedger.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Finance
{
    [Authorize]
    [Route("account-receivable")]
    [ApiController]

    public class AccountReceivableController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public AccountReceivableController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list")]
        public async Task<IActionResult> List([FromQuery] AccountReceivableListQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);
            

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("city-branch-list")]
        public async Task<IActionResult> CityBranchList([FromQuery] CityBranchListQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);
            

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

         [HttpPost("insert-ar")]
        public async Task<IActionResult> InsertAr([FromBody] AccountReceivableCreateCommand q)
        {   

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

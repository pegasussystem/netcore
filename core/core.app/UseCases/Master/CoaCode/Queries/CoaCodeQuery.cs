using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;

namespace core.app.UseCases.Master.CoaCode.Queries

{

    public partial class CoaCodeQuery : IRequest<ReturnFormat>
    {   
        public Int32? coaParentId { get; set; }
        public DateTime? CreatedAt { get; set; } 
    }
    public class MasterCityQueryHandler : IRequestHandler<CoaCodeQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MasterCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(CoaCodeQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;



            var query = from a in _context.CoaCodeEntities
                        join b in _context.CoaParentEntities on a.CoaParentId equals b.CoaParentId

                        select new
                        {
                            a.CoaId,
                            a.CoaCode,
                            a.CoaName,
                            b.CoaParentName,
                            a.CoaParentId,
                            CoaFullName = String.Format("[{0}] - {1}", a.CoaCode, a.CoaName)
                        };

            if(req.coaParentId != null && req.coaParentId != 0){
                query = from a in _context.CoaCodeEntities
                        join b in _context.CoaParentEntities on a.CoaParentId equals b.CoaParentId
                        where a.CoaParentId == req.coaParentId    
                        select new
                        {
                            a.CoaId,
                            a.CoaCode,
                            a.CoaName,
                            b.CoaParentName,
                            a.CoaParentId,
                            CoaFullName = String.Format("[{0}] - {1}", a.CoaCode, a.CoaName)
                        };
            }            

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.CoaId,
                            s.CoaCode,
                            s.CoaName,
                            s.CoaParentName,
                            s.CoaParentId,
                            s.CoaFullName

                    }).OrderBy(o => o.CoaCode);
            }

            // return 
            return rtn;
        }


    }

}

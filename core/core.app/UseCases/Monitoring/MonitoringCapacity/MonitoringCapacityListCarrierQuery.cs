﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Monitoring.MonitoringCapacity
{
    public partial class MonitoringCapacityListCarrierQuery : IRequest<ReturnFormat>
    {
    }

    public class MonitoringCapacityListCarrierQueryHandler : IRequestHandler<MonitoringCapacityListCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MonitoringCapacityListCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MonitoringCapacityListCarrierQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.V_MonitoringCapacityCarrierEntitys
                        select new
                        {
                            a.cid
                            , a.carrierCode
                            , a.carrierDesc
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.cid
                        , s.carrierCode
                        , s.carrierDesc
                    }).OrderBy(o => o.cid);
            }

            // return
            return rtn;
        }

    }
}

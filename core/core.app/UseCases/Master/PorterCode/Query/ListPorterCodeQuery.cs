﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.PorterCode.Query
{
    public partial class ListPorterCodeQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListPorterCodeQueryHandler : IRequestHandler<ListPorterCodeQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPorterCodeQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPorterCodeQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.PorterEntitys
                        select new
                        {
                            a.PorterId
							, a.Code
							, a.Phone
							, a.Name
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.PorterId
                        , s.Code
                        , s.Phone
						, s.Name
                    }).OrderBy(o => o.PorterId);
            }

            // return
            return rtn;
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Commands
{
    public partial class CreateMenuActionCommand : IRequest<ReturnFormat>
    {
        public Int32? privilegeId { get; set; }
        public Int32? menuId { get; set; }
        public Int32? menuActionId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateMenuActionCommandHandler : IRequestHandler<CreateMenuActionCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateMenuActionCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateMenuActionCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                PrivilegeActionEntity dataItem = new PrivilegeActionEntity();

                dataItem.CreatedBy = req.uiUserProfile.userId.ToString();
                dataItem.CreatedAt = DateTime.Now;
                dataItem.PrivilegeId = req.privilegeId;
                dataItem.MenuActionId = req.menuActionId;

                _context.PrivilegeActionEntitys.Add(dataItem);
                _context.SaveChanges();

                this.SetHistoryAction(req.uiUserProfile.userId, dataItem.PrivilegeId, "Create Menu Action", "Privilege Master", "");

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = dataItem;

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId     = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId   = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu       = Menu;
            dataItem.Desc       = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }
    }

}
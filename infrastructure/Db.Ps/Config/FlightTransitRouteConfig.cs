﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class FlightTransitRouteConfig : IEntityTypeConfiguration<FlightTransitRouteEntity>
    {
        public void Configure(EntityTypeBuilder<FlightTransitRouteEntity> builder)
        {
            builder.HasKey(e => e.TransitRouteId);
            builder.ToTable("FlightTransitRoute");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

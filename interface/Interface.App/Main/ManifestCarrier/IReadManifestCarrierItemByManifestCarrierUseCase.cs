﻿using Core.Entity.Main;
using Interface.App.Base;

namespace Interface.App.Main.ManifestCarrier
{
    public interface IReadManifestCarrierItemByManifestCarrierUseCase: IApp<ManifestCarrierItemEntity>
    {
    }
}

﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_UserAgreementConfig : IEntityTypeConfiguration<V_UserAgreementEntity>
    {
        public void Configure(EntityTypeBuilder<V_UserAgreementEntity> builder)
        {
            builder.HasKey(e => e.SubBranchId);
            builder.ToTable("V_UserAgreement");
        }
    }
}

﻿using Core.Entity.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class CUserAtConfig : IEntityTypeConfiguration<UserAtEntity>
    {
        public void Configure(EntityTypeBuilder<UserAtEntity> builder)
        {
            builder.HasKey(e => e.UserAtId);
            builder.ToTable("UserAt");
        }
    }
}

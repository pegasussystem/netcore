﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main.Manifest
{
    public interface I_Change_KoliNo_UseCase : IApp<ManifestEntity>
    {
        ReturnFormat Update(UiUserProfileEntity uiUserProfileEntity, Object req);
    }
}

﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Main.Manifest;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace core.app.UseCases.Manifest.Manifest_Ddb_WithWeight
{
    public class Manifest_Ddb_WithWeight_UseCase : I_Manifest_Ddb_WithWeight_UseCase
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public Manifest_Ddb_WithWeight_UseCase(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Read(UiUserProfileEntity uiUserProfileEntity)
        {
            // variable
            // ///// ///// /////
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };


            // logic
            // ///// ///// /////

            // get manifest with Weight
            IEnumerable<V_Manifest_Dbb_WithWeight_Entity> entities = _repoPs.ManifestRepo.V_Manifest_Dbb_WithWeight(uiUserProfileEntity);

            // return
            // ///// ///// /////
            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities;
            }
            return rtn;
        }

        public ReturnFormat Create(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Delete(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

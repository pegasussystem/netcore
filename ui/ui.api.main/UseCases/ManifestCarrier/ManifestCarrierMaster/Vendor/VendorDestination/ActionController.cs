﻿using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorDestination.Command;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorDestination
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]

    public class ActionController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ActionController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("manifest-carrier-master-vendor-destination-create")]
        public async Task<IActionResult> VendorDestinationCreate([FromBody] ManifestCarrierMasterVendorDestinationCreateCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("manifest-carrier-master-vendor-destination-delete")]
        public async Task<IActionResult> VendorDestinationDelete([FromBody] ManifestCarrierMasterVendorDestinationDeleteCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

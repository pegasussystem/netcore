﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main
{

    public interface ISpbCreateApp : IApp<SpbEntity>
    {
        ReturnFormat ReadSpbPrint(UiUserProfileEntity uiUserProfile, VSpbEntity entity);

        ReturnFormat ReadSpbGoodPrint(VSpbGoodsEntity entity);

        ReturnFormat DoSubmitForUpdateSpb(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, SpbEntity spbEntity);

       // ReturnFormat GetKoliNo(UiUserProfileEntity uiUserProfileEntity, SpbGoodsEntity entity, String SpbNo, Int64 SpbId, String SpbNoManual, int destCity, int destArea, String carrier);

        ReturnFormat GetRates(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity);

        ReturnFormat GetReceiverUrban(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity);

        ReturnFormat GetReceiverPostalCode(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity);
        ReturnFormat GetReceiverSubDistrict(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity);

        ReturnFormat GetReceiverCity(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity);

        ReturnFormat ReadVia(UiUserProfileEntity uiUserProfile, UiSpbCreateEntity entity);

        ReturnFormat ScreenCapture(UiUserProfileEntity uiUserProfile, VSpbEntity entity);

        //ReturnFormat ReadSenderList(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow);

        ReturnFormat ReadSenderList2(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow);  // Pati 25 Agt
        ReturnFormat ReadSenderTelpList(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow);  // Pati 25 Agt
        

        ReturnFormat ReadSenderNameList(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow);  // Pati 09Jun22

        ReturnFormat ReadReceiverNameList(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow);  // Pati 09Jun22

        ReturnFormat ReadCekPhone(VSpbEntity entity); // 15feb22
        ReturnFormat ReadCekTelp(VSpbEntity entity); // 14jul22

        ReturnFormat ReadSenderIdPel(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow);

        ReturnFormat ReadReceiverIdPel(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow);  // 27 mei 22
    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class ModaConfig : IEntityTypeConfiguration<ModaEntity>
    {
        public void Configure(EntityTypeBuilder<ModaEntity> builder)
        {
            builder.HasKey(e => e.ModaId);
            builder.ToTable("Moda");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Configuration.Roles.Query
{
    public partial class ListRoleUserQuery : IRequest<ReturnFormat>
    {
        public Int32? rolesId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListRoleUserQueryHandler : IRequestHandler<ListRoleUserQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListRoleUserQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListRoleUserQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.V_PrivilegeUserListEntitys
                        join b in _context.PrivilegeEntitys on a.PrivilegeId equals b.PrivilegeId
                        where a.RolesId.Equals(req.rolesId)
                        select new
                        {
                            a.UserId
                            , a.Username
                            , a.FullName
                            , a.Email
                            , a.PrivilegeId
                            , b.PrivilegeName
                            , a.RolesId
                            , a.RolesName
                            , a.Desc
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.UserId
                        , s.Username
                        , s.FullName
                        , s.Email
                        , s.PrivilegeId
                        , s.PrivilegeName
                        , s.RolesId
                        , s.RolesName
                        , s.Desc
                    }).OrderBy(o => o.Username);
            }

            // return
            return rtn;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class ViaEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? ViaId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? CityId { get; set; }
        public String? ViaName { get; set; }

    }
}

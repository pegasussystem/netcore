﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.SuperVision.Command.Void
{
    public partial class ApproveVoidCommand : IRequest<ReturnFormat>
    {
        public Int32? superVisionId { get; set; }
        public Int64? spbId { get; set; }
        public String? spbNo { get; set; }
        public String? reason { get; set; }
        public String? reasonReject { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ApproveVoidCommandHandler : IRequestHandler<ApproveVoidCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ApproveVoidCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ApproveVoidCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                var entity = _context.SuperVisionEntitys.Where(w => w.SuperVisionId.Equals(req.superVisionId)).FirstOrDefault();

                if (entity == null)
                {
                    return rtn;
                }

                entity.ReasonReject     = req.reasonReject;
                entity.ApproveBy        = req.uiUserProfile.userId;
                entity.ApproveDate      = DateTime.UtcNow;
                entity.Status           = 1;
                entity.UpdatedBy        = req.uiUserProfile.userId.ToString();
                entity.UpdatedAt        = DateTime.UtcNow;

                await _context.SaveChangesAsync(cancellationToken);

                var r = this.SetVoid(req.spbId, req.uiUserProfile.userId, req.reason).ToList();
                await _context.SaveChangesAsync(cancellationToken);
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = r;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SPSpb_VoidEntity> SetVoid(Int64? spbId, Int32? voidBy, String? voidReason)
        {
            String query = $@"EXEC SPSpb_Void @SpbId={spbId}, @VoidBy={voidBy}, @VoidReason='{voidReason}'";
            var res = this._context.SPSpb_VoidEntitiys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
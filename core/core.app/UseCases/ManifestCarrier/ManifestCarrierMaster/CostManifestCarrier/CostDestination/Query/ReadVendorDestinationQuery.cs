﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostDestination.Query
{
    public partial class ReadVendorDestinationQuery : IRequest<ReturnFormat>
    {
        public Int32? stationDestinationId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadVendorDestinationQueryHandler : IRequestHandler<ReadVendorDestinationQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadVendorDestinationQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadVendorDestinationQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                rtn.Status = StatusCodes.Status200OK;
                var qr = from a in _context.VendorDestinationEntitys
                            join b in _context.VendorEntitys on a.VendorId equals b.VendorId
                            join c in _context.StationEntitys on a.StationId equals c.StationId
                            where c.StationId.Equals(req.stationDestinationId)
                            select new
                        {
                            VendorId = a.VendorDestinationId
                            , b.VendorName
                            };

                if (qr != null)
                {
                    var query = qr.Distinct();
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.VendorId
                            , s.VendorName
                        }).OrderBy(o => o.VendorId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
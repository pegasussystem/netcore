using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_AccountReceivableListEntity
    {
        public Int64 arId { get; set; }
        public DateTime createdAt { get; set; } 
        public String? keterangan { get; set; }
        public String? coaCode { get; set; }
        public String? coaName { get; set; }
        public String? coaArCode { get; set; }
        public String? coaArName { get; set; }
        public String? cityNameCode { get; set; }
        public Decimal? debit { get; set; }
        public Decimal? credit { get; set; }
         public Decimal? saldo { get; set; }
         public Decimal? cumulativeBalance { get; set; }
    }
}

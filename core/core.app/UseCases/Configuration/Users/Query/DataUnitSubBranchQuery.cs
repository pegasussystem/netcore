﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Configuration.Users.Query
{
    public partial class DataUnitSubBranchQuery : IRequest<ReturnFormat>
    {
        public String? type { get; set; }
        public String? branchId { get; set; }
    }

    public class DataUnitSubBranchQueryHandler : IRequestHandler<DataUnitSubBranchQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public DataUnitSubBranchQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(DataUnitSubBranchQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.V_DataOptionSettingEntitys
                        join b in _context.SubBranchEntitys on a.Id equals b.SubBranchId
                        where a.Type.Equals(req.type) && b.BranchId.Equals(Int32.Parse(req.branchId))
                        select new
                        {
                            a.Id
                            , a.Code
                            , a.Name
                            , a.Type
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.Id
                        , s.Code
                        , s.Name
                        , s.Type
                    }).OrderBy(o => o.Id);
            }

            // return
            return rtn;
        }

    }
}

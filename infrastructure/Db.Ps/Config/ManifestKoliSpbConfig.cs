﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class ManifestKoliSpbConfig : IEntityTypeConfiguration<ManifestKoliSpbEntity>
    {
        public void Configure(EntityTypeBuilder<ManifestKoliSpbEntity> builder)
        {
            builder.HasKey(e => e.ManifestKoliSpbId);
            builder.ToTable("ManifestKoliSpb");

        }
    }
}

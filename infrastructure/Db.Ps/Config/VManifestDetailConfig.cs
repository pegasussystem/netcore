﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VManifestDetailConfig : IEntityTypeConfiguration<VManifestDetailEntity>
    {
        public void Configure(EntityTypeBuilder<VManifestDetailEntity> builder)
        {
            builder.HasNoKey();
            builder.ToTable("VManifestDetail");
        }
    }
}

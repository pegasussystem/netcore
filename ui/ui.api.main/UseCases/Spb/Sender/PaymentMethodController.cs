﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.Spb.Commands.PaymentMethod;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Spb.Sender
{
    [Authorize]
    [Route("spblistsender")]
    [ApiController]
    public class PaymentMethodController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public PaymentMethodController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("paymentmethod-update")]
        public async Task<IActionResult> UpdateDataPaymentMethod([FromBody] PaymentMethodCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();


            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface ICarrierRepo : IRepo<CarrierEntity>
    {
        IEnumerable<VCarrierEntity> VCarrier();
        public IEnumerable<VCarrierEntity> ManifestCarrier_Sb_Carrier();
    }
}

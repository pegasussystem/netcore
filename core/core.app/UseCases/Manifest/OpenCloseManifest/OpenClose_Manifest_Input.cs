﻿using System;
using System.Collections.Generic;
using System.Text;

namespace core.app.UseCases.Manifest.OpenCloseManifest
{
    public class OpenClose_Manifest_Input
    {
        public String Action { get; set; }
        public Int64 ManifestId { get; set; }

        public Boolean GetAction() {
            if (this.Action == "open") {
                return true;
            }
            if (this.Action == "close")
            {
                return false;
            }

            return true;
        }
    }
}

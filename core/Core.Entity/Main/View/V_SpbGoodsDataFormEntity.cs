﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_SpbGoodsDataFormEntity
    {
        public Int64? SpbId { get; set; }
        public Int64? SpbGoodsId { get; set; }
        public String? SpbNo { get; set; }
        public Int32? TotalKoli { get; set; }
        public Decimal? TotalAw { get; set; }
        public String? KoliNo { get; set; }
        public Decimal? Aw { get; set; }
        public Decimal? Caw { get; set; }
        public Decimal? Vw { get; set; }
        public Decimal? Length { get; set; }
        public Decimal? Width { get; set; }
        public Decimal? Height { get; set; }
        public Decimal? Price { get; set; }
        public Int32? Min { get; set; }
        public Decimal? MinRates { get; set; }
        public Decimal? CawFinal { get; set; }
    }
}

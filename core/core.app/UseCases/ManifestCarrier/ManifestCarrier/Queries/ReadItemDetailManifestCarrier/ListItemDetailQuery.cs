﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadItemDetailManifestCarrier
{
     public partial class ListItemDetailQuery : IRequest<ReturnFormat>
    {
        public String? manifestCarrierId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListItemDetailDetailQueryHandler : IRequestHandler<ListItemDetailQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListItemDetailDetailQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListItemDetailQuery req, CancellationToken cancellationToken)
        {
            Int32? mcId = Int32.Parse(req.manifestCarrierId);

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query =
                from mcc in _context.V_ManifestCarrierItemDetailEntitys
                where mcc.ManifestCarrierId.Equals(mcId)

                select new
                {
                    mcc.ManifestCarrierItemId
                    , mcc.ManifestCarrierId
                    , mcc.ManifestId
                    , mcc.ManifestNo
                    , mcc.Koli
                    , mcc.Aw
                    , mcc.Caw
                    , mcc.KoliFinal
                    , mcc.CawFinal
                    , mcc.FinalWeightSMU
                    , mcc.DestinationCityId
                    , mcc.MasterCityName
                    , mcc.VendorDestinationId
                    , mcc.VendorName
                };

            var x = query.Count();
            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.ManifestCarrierItemId
                        , s.ManifestCarrierId
                        , s.ManifestId
                        , s.ManifestNo
                        , s.Koli
                        , s.Aw
                        , s.Caw
                        , s.KoliFinal
                        , s.CawFinal
                        , s.FinalWeightSMU
                        , s.DestinationCityId
                        , s.MasterCityName
                        , s.VendorDestinationId
                        , s.VendorName

                    }).OrderByDescending(o => o.ManifestCarrierItemId);
            }

            //
            // return
            return rtn;
        }
    }
}

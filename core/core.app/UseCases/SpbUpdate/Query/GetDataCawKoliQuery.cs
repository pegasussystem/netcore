﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.SpbUpdate.Query
{
    public partial class GetDataCawKoliQuery : IRequest<ReturnFormat>
    {
        public Int64? spbId { get; set; }
        public String? totalKoli { get; set; }
        public String? vw { get; set; }
        public String? aw { get; set; }
        public String? ratesType { get; set; }
        public String? carrier { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class GetDataCawKoliQueryHandler : IRequestHandler<GetDataCawKoliQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetDataCawKoliQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetDataCawKoliQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_CawFinalKoliEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataCawKoli(req.spbId, req.totalKoli, req.vw, req.aw, req.ratesType, req.carrier);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        s.Caw
                        , s.CawFinal
                    });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_CawFinalKoliEntity> DataCawKoli(Int64? SpbId , String? TotalKoli, String? Vw, String? Aw, String? RatesType, String? Carrier)
        {
            String query = $@"EXEC SP_CawFinalKoli @spbId = '{SpbId}', @totalKoli = '{TotalKoli}', @vw = '{Vw.Replace(",", ".")}', @aw='{Aw.Replace(",", ".")}', @ratesType = '{RatesType}', @carrier = '{Carrier}' ";
            var res = this._context.SP_CawFinalKoliEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Configuration.Roles.Commands
{
    public partial class SaveCityCommand : IRequest<ReturnFormat>
    {
        public String? cityId { get; set; }
        public String? mode { get; set; }
        public String? type { get; set; }
        public Int32? roleId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class SaveCityCommandHandler : IRequestHandler<SaveCityCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public SaveCityCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(SaveCityCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.SaveCity(req.cityId, req.mode, req.type, req.roleId, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_SaveDataDestBlockCityEntity> SaveCity(String? cityId, String? mode, String? type, Int32? roleId, Int32? login)
        {
            String query = $@"EXEC SP_SaveDataDestBlockCity @CityId='{cityId}', @Mode='{mode}', @Type='{type}', @RolesId={roleId}, @Login={login} ";
            var res = this._context.SP_SaveDataDestBlockCityEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

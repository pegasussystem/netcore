﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;




    public partial class MasterCityDeleteCommand : IRequest<ReturnFormat>
    {

        public String? masterCityId { get; set; }
        public Int32? masterProvinceId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MasterCityDeleteCommandHandler : IRequestHandler<MasterCityDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public MasterCityDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(MasterCityDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.MasterCityDelete(req.masterCityId, req.masterProvinceId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_MasterCityDeleteEntity> MasterCityDelete(String? masterCityId, Int32? masterProvinceId)
        {
            String query = $@"EXEC SP_MasterCityDelete  @masterProvinceId={masterProvinceId}, @masterCityId='{masterCityId}'";
            var res = this._context.SP_MasterCityDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
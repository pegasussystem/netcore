﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace core.app.Main
{
    public class ManifestCarrierApp : IManifestCarrierApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public ManifestCarrierApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Delete(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat ReadManifest(UiUserProfileEntity uiUserProfileEntity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VManifestEntity> vManifestEntities = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data
            vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId))
                .ToList();


            // from sub branch
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId != null)
            {
                vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
                && w.BranchId.Equals(uiUserProfileEntity.BranchId) && w.SubBranchId.Equals(uiUserProfileEntity.SubBranchId))
               .ToList();
            }

            // from branch
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId == null)
            {
                vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
                && w.BranchId.Equals(uiUserProfileEntity.BranchId))
                .ToList();
            }

            // from company
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId == null && uiUserProfileEntity.SubBranchId == null)
            {
                vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId))
              .ToList();
            }


            if (vManifestEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vManifestEntities
                    .Select(s => new
                    {
                        s,
                        CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)uiUserProfileEntity.WorkTimeZoneMinute)
                    });

                //rtn.Data = vManifestEntities;
            }

            //
            // return
            return rtn;
        }

        public ReturnFormat ReadCity(UiUserProfileEntity uiUserProfileEntity)
        {
            
            // variable
            // ///// ///// /////
            ReturnFormat rtn = new ReturnFormat();
            List<VCompanyCityEntity> vCompanyCityEntities = null;

            // set variable
            // ///// ///// /////
            rtn.Status = StatusCodes.Status204NoContent;

            // logic
            // ///// ///// /////
            vCompanyCityEntities = _repoPs.VCompanyCityRepo.Read()
                .Where(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)).ToList();

            if (vCompanyCityEntities != null) {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vCompanyCityEntities;
            }

            // return
            // ///// ///// /////
            return rtn;
        }


        public ReturnFormat ReadCarrier(UiUserProfileEntity uiUserProfileEntity)
        {

            // variable
            // ///// ///// /////
            ReturnFormat rtn = new ReturnFormat();
            List<VCarrierEntity> vCarrierEntities;

            // set variable
            // ///// ///// /////
            rtn.Status = StatusCodes.Status204NoContent;

            // logic
            // ///// ///// /////
            vCarrierEntities = _repoPs.CarrierRepo.VCarrier().ToList();

            if (vCarrierEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vCarrierEntities;
            }

            // return
            // ///// ///// /////
            return rtn;
        }

        public ReturnFormat Read(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat List(VManifestEntity entity, UiUserProfileEntity uiUserProfileEntity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VManifestEntity> vManifestEntities = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data
            vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId))
                .ToList();


            // from sub branch
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId != null)
            {
                vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
                && w.BranchId.Equals(uiUserProfileEntity.BranchId) && w.SubBranchId.Equals(uiUserProfileEntity.SubBranchId))
                .ToList();
            }

            // from branch
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId == null)
            {
                vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
                && w.BranchId.Equals(uiUserProfileEntity.BranchId))
                .ToList();
            }

            // from company
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId == null && uiUserProfileEntity.SubBranchId == null)
            {
                vManifestEntities = _repoPs.VManifestRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId))
                .ToList();
            }


            if (vManifestEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vManifestEntities
                    .Select(s => new {
                        s,
                        CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)uiUserProfileEntity.WorkTimeZoneMinute)
                    });
            }

            //
            // return
            return rtn;

        }
    }
}

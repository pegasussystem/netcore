﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_CheckRatesEntity
    {
        public Int32? RatesId { get; set; }
        public Int32? RatesDetailId { get; set; }
        public String? Carrier { get; set; }
        public String? RatesType { get; set; }
        public String? TypeOfService { get; set; }
        public String? QualityOfService { get; set; }
        public String? OriginCity { get; set; }
        public String? OriginCityCode { get; set; }
        public String? OriginCityName { get; set; }
        public String? DestinationCity { get; set; }
        public String? DestinationCityCode { get; set; }
        public String? DestinationCityName { get; set; }
        public String? SubDistrict { get; set; }
        public String? SubDistrictCode { get; set; }
        public String? SubDistrictName { get; set; }
        public Decimal? Rates { get; set; }
        public Int32? Min { get; set; }
        public Int32? Max { get; set; }
        public Decimal? MinRates { get; set; }
        public String? Type { get; set; }

    }
}

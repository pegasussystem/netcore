﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class ManifestCarrierItemConfig : IEntityTypeConfiguration<ManifestCarrierItemEntity>
    {
        public void Configure(EntityTypeBuilder<ManifestCarrierItemEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierItemId);
            builder.ToTable("ManifestCarrierItem");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

﻿using System;
namespace Core.Entity.Main
{
    public class CompanyAreaEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }

        public Int32? CompanyAreaId { get; set; }
        public Int32? CompanyId { get; set; }
        public String? CityType { get; set; }
        public String? MasterCityId { get; set; }
        public String? MasterSubDistrictId { get; set; }
        public String? CompanyAreaCode { get; set; }
        public String? CompanyAreaName { get; set; }


    }
}

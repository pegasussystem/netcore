﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VManifestCarrierCostEntity
    {

        public Int32? ManifestCarrierId { get; set; }
        public String? CostType { get; set; }
        public decimal? BaseCost { get; set; }
        public String? Unit { get; set; }
        public String? CostName { get; set; }
        public String? UnitValue { get; set; }
        public Int32? Order { get; set; }
        public Decimal? Weight { get; set; }
        public Decimal? Cost { get; set; }
        public String? CityCodeDestination { get; set; }

    }





}

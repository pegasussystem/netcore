﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VCustomerIdPelangganConfig : IEntityTypeConfiguration<VCustomerIdPelangganEntity>
    {
        public void Configure(EntityTypeBuilder<VCustomerIdPelangganEntity> builder)
        {
            builder.HasKey(e => e.RowNo);
            builder.ToTable("VCustomerIdPelanggan");
        }
    }
}

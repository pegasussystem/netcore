﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Core.Entity.Main
{
    public class PaymentMethodEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? PaymentMethodId { get; set; }
        public String? MasterCityId { get; set; }
        public Int32? LookupId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_CostOrigin_Entity
    {
        // CostOrigin
        public Int32? CostOriginId { get; set; }
        public Int32? StationOriginId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? VendorOriginId { get; set; }

        // CostOriginDetail
        public Int32? CostOriginDetailId { get; set; }
        public String? CostType { get; set; }
        public String? Unit { get; set; }
        public Decimal? BaseCost { get; set; }
        public String? CostGroup { get; set; }
        public String? UseWeight { get; set; }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Configuration.Users.Query
{
    public partial class UnitNameQuery : IRequest<ReturnFormat>
    {
        public Int32? unitId { get; set; }
        public String? type { get; set; }
        public String? unitValue { get; set; }
    }

    public class UnitNameQueryHandler : IRequestHandler<UnitNameQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public UnitNameQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(UnitNameQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            if (req.unitValue == "SubBranch")
            {
                var query = from a in _context.V_DataOptionSettingEntitys
                            join b in _context.WorkUnitEntitys on a.Key equals b.WorkUnitId
                            where a.Type.Equals(req.type) && a.Id.Equals(req.unitId)

                            select new
                            {
                                Id = b.WorkUnitId
                                , Code = b.WorkUnitCode
                                , Name = b.WorkUnitName
                                , a.Type
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.Id
                            , s.Code
                            , s.Name
                            , s.Type
                        }).OrderBy(o => o.Id);
                }
            }
            else { 
                var query = from a in _context.V_DataOptionSettingEntitys
                        where a.Type.Equals(req.type) && a.Id.Equals(req.unitId)
                        select new
                        {
                            a.Id
                            , a.Code
                            , a.Name
                            , a.Type
                        };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.Id
                            , s.Code
                            , s.Name
                            , s.Type
                        }).OrderBy(o => o.Id);
                }
            }

            // return
            return rtn;
        }

    }
}

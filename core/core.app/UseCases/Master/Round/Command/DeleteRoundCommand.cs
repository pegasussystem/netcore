﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Master.Round.Command
{
    public partial class DeleteRoundCommand : IRequest<ReturnFormat>
    {
        public String? roundId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class DeleteRoundCommandHandler : IRequestHandler<DeleteRoundCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public DeleteRoundCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(DeleteRoundCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.ManageData(req.roundId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_RoudDeleteEntity> ManageData(String? roundId)
        {
            String query = $@"EXEC SP_Round_Delete @roundId='{roundId}'";
            var res = this._context.SP_RoudDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
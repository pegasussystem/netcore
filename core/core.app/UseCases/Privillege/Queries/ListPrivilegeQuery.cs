﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Queries
{
    public partial class ListPrivilegeQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListPrivilegeQueryHandler : IRequestHandler<ListPrivilegeQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPrivilegeQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPrivilegeQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            int chk = this.CheckPrivilegeAccess(req.uiUserProfileEntity.userId);
            if (chk == 0) {
                rtn.Status = StatusCodes.Status203NonAuthoritative;
                return rtn;
            }

            var query = from a in _context.PrivilegeEntitys
                        join c in _context.PrivilegeActionEntitys on a.PrivilegeId equals c.PrivilegeId
                        join d in _context.MenuActionEntitys on c.MenuActionId equals d.MenuActionId
                        join e in _context.MenuDataEntitys on d.MenuId equals e.MenuId
                        select new
                        {
                            a.PrivilegeId
                            , a.PrivilegeName
                            , e.MenuKey
                            , e.MenuName
                            , d.MenuAction
                            , d.MenuActionName
                            ,c.Value
                            ,c.Desc
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.PrivilegeId
                        , s.PrivilegeName
                        , s.MenuKey
                        , s.MenuName
                        , s.MenuAction
                        , s.MenuActionName
                        , s.Value
                        , s.Desc
                    }).OrderBy(o => o.PrivilegeId);
            }

            // return
            return rtn;
        }

        public int CheckPrivilegeAccess(Int32? userId) {

            var qr1 = from pu in _context.PrivilegeUserEntitys
                        join pa in _context.PrivilegeActionEntitys on pu.PrivilegeId equals pa.PrivilegeId
                        join ma in _context.MenuActionEntitys on pa.MenuActionId equals ma.MenuActionId
                        join mu in _context.MenuDataEntitys on ma.MenuId equals mu.MenuId
                        where mu.MenuKey.Equals("privilege-master") && pu.UserId.Equals(userId) && ma.MenuAction.Equals("list")
                        select new
                        {
                            pu.UserId
                        };
            return qr1.Count(); ;
        }
    }
}
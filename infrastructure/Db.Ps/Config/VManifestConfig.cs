﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VManifestConfig : IEntityTypeConfiguration<VManifestEntity>
    {
        public void Configure(EntityTypeBuilder<VManifestEntity> builder)
        {
            builder.HasKey(e => e.ManifestId);
            builder.ToTable("VManifest");
        }
    }
}

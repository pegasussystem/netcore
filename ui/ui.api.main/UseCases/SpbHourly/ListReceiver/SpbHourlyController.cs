﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.SpbHourly.ListReceiver;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace ui.api.main.UseCases.SpbHourly.ListReceiver
{

    [Authorize]
    [Route("spb-hourly-receiver")]
    [ApiController]
    public class SpbHourlyController : DefaultController
    {

        private readonly IAppWrapperPs _appPs;
        public SpbHourlyController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpGet("list")]
        public async Task<IActionResult> Execute([FromQuery] ListReceiverSpbHourlyInput input)
        {
            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();


            // --- --- ---
            // logic
            // --- --- ---
            _returnFormat = _appPs.ListReceiverSpbHourlyUseCase.List(GetUserClaim(), input);


            // --- --- ---
            // return
            // --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

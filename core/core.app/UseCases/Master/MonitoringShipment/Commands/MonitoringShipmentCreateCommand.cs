﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Master.MonitoringShipment.Commands
{
    public partial class MonitoringShipmentCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? viaId { get; set; }
        public String? originCityId { get; set; }
        public Int32? corporateId { get; set; }
        public String? destinationCityId { get; set; }
        public String? sttPegasus { get; set; }
        public String? sttLP { get; set; }
        public String? smuNo { get; set; }
        public Int32? koli { get; set; }
        public Decimal? caw { get; set; }
        public Int32? carrierId { get; set; }
        public Int32? flightId { get; set; }
        public DateTime? flightDate { get; set; }
        public DateTime? etd { get; set; }
        public DateTime? eta { get; set; }
        public String? status { get; set; }
        public String? pod { get; set; }
        public DateTime? podDate { get; set; }
        public String? description { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MonitoringShipmentCreateCommandHandler : IRequestHandler<MonitoringShipmentCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public MonitoringShipmentCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(MonitoringShipmentCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                var podDt = "";
                if (req.podDate != null)
                {
                    podDt = req.podDate.Value.Year + "-" + req.podDate.Value.Month + "-" + req.podDate.Value.Day;
                }
                else {
                    podDt = null;
                }

                // Mark as Changed
                var res = this.MonitoringShipmentCreate(req.viaId, req.originCityId, req.corporateId, req.destinationCityId, req.sttPegasus, req.sttLP, req.smuNo, req.koli, req.caw, req.carrierId, req.flightId, req.flightDate, req.etd, req.eta, req.status, req.pod, podDt, req.description, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_MonitoringShipmentCreateEntity> MonitoringShipmentCreate(Int32? viaId, String? originCityId, Int32? corporateId, String? destinationCityId, String? sttPegasus, String? sttLP, String? smuNo, Int32? koli, Decimal? caw, Int32? carrierId, Int32? flightId, DateTime? flightDate, DateTime? etd, DateTime? eta, String? status, String? pod, String? podDate, String? description, int? userId)
        {
            String query = $@"EXEC SP_MonitoringShipmentCreate
                    @viaId = '{viaId}'
                    , @originCityId = '{originCityId}'
                    , @corporateId = '{corporateId}'
                    , @destinationCityId = '{destinationCityId}'
                    , @sttPegasus = '{sttPegasus}'
                    , @sttLP = '{sttLP}'
                    , @smuNo = '{smuNo}'
                    , @koli = '{koli}'
                    , @caw = '{caw}'
                    , @carrierId = '{carrierId}'
                    , @flightId = '{flightId}'
                    , @flightDate = '{flightDate.Value.Year + "-" + flightDate.Value.Month + "-" + flightDate.Value.Day}'
                    , @etd = '{etd.Value.Year + "-" + etd.Value.Month + "-" + etd.Value.Day + " " + etd.Value.Hour + ":" + etd.Value.Minute + ":" + etd.Value.Second}'
                    , @eta = '{eta.Value.Year + "-" + eta.Value.Month + "-" + eta.Value.Day + " " + eta.Value.Hour + ":" + eta.Value.Minute + ":" + eta.Value.Second}'
                    , @status = '{status}'
                    , @pod = '{pod}'
                    , @podDate = '{podDate}'
                    , @description = '{description}'
                    , @userId= {userId} ";
            var res = this._context.SP_MonitoringShipmentCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

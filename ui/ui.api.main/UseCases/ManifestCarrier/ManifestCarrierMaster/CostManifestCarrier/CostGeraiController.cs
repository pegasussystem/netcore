﻿using core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostGerai.Commands;
using core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostGerai.Query;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.CostManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]

    public class CostGeraiController : ApiController
    {
        [HttpGet("list-cost-gerai")]
        public async Task<IActionResult> ListCostGerai([FromQuery] ListCostGeraiQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

       [HttpGet("read-city")]
        public async Task<IActionResult> ReadCity([FromQuery] ReadCityQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("cost-gerai-create")]
        public async Task<IActionResult> CostGeraiCreate([FromBody] CostGeraiCreateCommand q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("cost-gerai-update")]
        public async Task<IActionResult> CostGeraiUpdate([FromBody] CostGeraiUpdateCommand q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("cost-gerai-delete")]
        public async Task<IActionResult> CostGeraiDelete([FromBody] CostGeraiDeleteCommand q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

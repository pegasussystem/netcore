﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main;

namespace core.app.UseCases.Spb.Commands.CreateSpb
{
    public partial class UpdateDataCommand : IRequest<ReturnFormat>
    {
        public Int32? paramSender1 { get; set; }
		public Int32? paramSender2 { get; set; }
		public Int32? paramSender3 { get; set; }
		public Int32? paramSender4 { get; set; }
		public Int32? paramSender5 { get; set; }
		public Int32? paramSender6 { get; set; }

        public Int32? paramReceiver1 { get; set; }
		public Int32? paramReceiver2 { get; set; }
		public Int32? paramReceiver3 { get; set; }
		public Int32? paramReceiver4 { get; set; }
		public Int32? paramReceiver5 { get; set; }
		public Int32? paramReceiver6 { get; set; }

        public String? mode { get; set; }

        public Int32? senderId { get; set; }
        public Int32? senderNameId { get; set; }
        public Int32? senderStoreId { get; set; }
        public Int32? senderPlaceId { get; set; }
        public Int32? senderAddressId { get; set; }

		public String? senderTelp { get; set; }
		public String? senderPhone { get; set; }
        public String? senderName { get; set; }
        public String? senderStore { get; set; }
        public String? senderPlace { get; set; }
        public String? senderAddress { get; set; }

		public Int32? receiverId { get; set; }
        public Int32? receiverNameId { get; set; }
        public Int32? receiverStoreId { get; set; }
        public Int32? receiverPlaceId { get; set; }
        public Int32? receiverAddressId { get; set; }

		public String? receiverTelp { get; set; }
		public String? receiverPhone { get; set; }
        public String? receiverName { get; set; }
        public String? receiverStore { get; set; }
        public String? receiverPlace { get; set; }
        public String? receiverAddress { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class FormDataQueryHandler : IRequestHandler<UpdateDataCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public FormDataQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(UpdateDataCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;
            //
            // logic

            //cek key exists di entity
            var senderPhoneEntity       = _context.CustomerEntitys.Where(x => x.CustomerId.Equals(req.senderId)).FirstOrDefault();
            var senderTelpEntity        = _context.CustomerEntitys.Where(x => x.CustomerId.Equals(req.senderId)).FirstOrDefault();
            var senderNameEntity        = _context.CustomerNameEntitys.Where(x => x.CustomerId.Equals(req.senderId) && x.CustomerNameId.Equals(req.senderNameId)).FirstOrDefault();//.FindAsync(req.senderNameId)
            var senderStoreEntity       = _context.CustomerStoreEntitys.Where(x => x.CustomerId.Equals(req.senderId) && x.CustomerStoreId.Equals(req.senderStoreId)).FirstOrDefault();//.FindAsync(req.senderStoreId);
            var senderPlaceEntity       = _context.CustomerPlaceEntitys.Where(x => x.CustomerId.Equals(req.senderId) && x.CustomerPlaceId.Equals(req.senderPlaceId)).FirstOrDefault();//.FindAsync(req.senderPlaceId);
            var senderAddressEntity     = _context.CustomerAddressEntitys.Where(x => x.CustomerId.Equals(req.senderId) && x.CustomerAddressId.Equals(req.senderAddressId)).FirstOrDefault();//.FindAsync(req.senderAddressId);

            var receiverTelpEntity      = _context.CustomerEntitys.Where(x => x.CustomerId.Equals(req.receiverId)).FirstOrDefault();
            var receiverPhoneEntity     = _context.CustomerEntitys.Where(x => x.CustomerId.Equals(req.receiverId)).FirstOrDefault();
            var receiverNameEntity      = _context.CustomerNameEntitys.Where(x => x.CustomerId.Equals(req.receiverId) && x.CustomerNameId.Equals(req.receiverNameId)).FirstOrDefault();//.FindAsync(req.receiverNameId);
            var receiverStoreEntity     = _context.CustomerStoreEntitys.Where(x => x.CustomerId.Equals(req.receiverId) && x.CustomerStoreId.Equals(req.receiverStoreId)).FirstOrDefault();//.FindAsync(req.receiverStoreId);
            var receiverPlaceEntity     = _context.CustomerPlaceEntitys.Where(x => x.CustomerId.Equals(req.receiverId) && x.CustomerPlaceId.Equals(req.receiverPlaceId)).FirstOrDefault();//.FindAsync(req.receiverPlaceId);
            var receiverAddressEntity   = _context.CustomerAddressEntitys.Where(x => x.CustomerId.Equals(req.receiverId) && x.CustomerAddressId.Equals(req.receiverAddressId)).FirstOrDefault();//.FindAsync(req.receiverAddressId);

            if (req.mode == "sender")
            {
                //----------------------------PENGIRIM------------------------------------------------------
                //-----------PHONE-------------------------
                if (req.paramSender1 == 0)
                {
                    if (senderPhoneEntity == null)
                    {
                        this.CreateSenderPhone(req.senderPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPhoneEntity.Phone      = req.senderPhone;
                        senderPhoneEntity.UpdatedBy  = req.uiUserProfileEntity.userId.ToString();
                        senderPhoneEntity.UpdatedAt  = DateTime.UtcNow;
                    }
                }

                if (req.paramSender1 == 1)
                {
                    if (senderNameEntity == null)
                    {
                        req.senderNameId = this.CreateSenderName(req.senderId, req.senderPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderNameEntity.CustomerName = req.senderPhone;
                        senderNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderNameEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender1 == 2)
                {
                    if (senderStoreEntity == null)
                    {
                        req.senderStoreId = this.CreateSenderStore(req.senderId, req.senderPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderStoreEntity.CustomerStore = req.senderPhone;
                        senderStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender1 == 3)
                {
                    if (senderPlaceEntity == null)
                    {
                        req.senderPlaceId = this.CreateSenderPlace(req.senderId, req.senderPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPlaceEntity.CustomerPlace = req.senderPhone;
                        senderPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender1 == 4)
                {
                    if (senderAddressEntity == null)
                    {
                        req.senderAddressId = this.CreateSenderAddress(req.senderId, req.senderPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderAddressEntity.CustomerAddress = req.senderPhone;
                        senderAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender1 == 5)
                {
                    if (senderTelpEntity == null)
                    {
                        this.CreateSenderTelp(req.senderPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderTelpEntity.Telp       = req.senderPhone;
                        senderTelpEntity.UpdatedBy  = req.uiUserProfileEntity.userId.ToString();
                        senderTelpEntity.UpdatedAt  = DateTime.UtcNow;
                    }
                }

                //-----------NAME-------------------------
                if (req.paramSender2 == 0)
                {
                    if (senderPhoneEntity == null)
                    {
                        this.CreateSenderPhone(req.senderName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPhoneEntity.Phone     = req.senderName;
                        senderPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender2 == 1)
                {
                    if (senderNameEntity == null)
                    {
                        req.senderNameId = this.CreateSenderName(req.senderId, req.senderName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderNameEntity.CustomerName = req.senderName;
                        senderNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderNameEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender2 == 2)
                {
                    if (senderStoreEntity == null)
                    {
                        req.senderStoreId = this.CreateSenderStore(req.senderId, req.senderName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderStoreEntity.CustomerStore = req.senderName;
                        senderStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender2 == 3)
                {
                    if (senderPlaceEntity == null)
                    {
                        req.senderPlaceId = this.CreateSenderPlace(req.senderId, req.senderName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPlaceEntity.CustomerPlace = req.senderName;
                        senderPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }                    
                }

                if (req.paramSender2 == 4)
                {
                    if (senderAddressEntity == null)
                    {
                        req.senderAddressId = this.CreateSenderAddress(req.senderId, req.senderName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderAddressEntity.CustomerAddress = req.senderName;
                        senderAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender2 == 5)
                {
                    if (senderTelpEntity == null)
                    {
                        this.CreateSenderTelp(req.senderName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderTelpEntity.Telp = req.senderName;
                        senderTelpEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderTelpEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                //-----------STORE-------------------------
                if (req.paramSender3 == 0)
                {
                    if (senderPhoneEntity == null)
                    {
                        this.CreateSenderPhone(req.senderStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPhoneEntity.Phone     = req.senderStore;
                        senderPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender3 == 1)
                {
                    if (senderNameEntity == null)
                    {
                        req.senderNameId = this.CreateSenderName(req.senderId, req.senderStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderNameEntity.CustomerName = req.senderStore;
                        senderNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderNameEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender3 == 2)
                {
                    if (senderStoreEntity == null)
                    {
                        req.senderStoreId = this.CreateSenderStore(req.senderId, req.senderStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderStoreEntity.CustomerStore = req.senderStore;
                        senderStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender3 == 3)
                {
                    if (senderPlaceEntity == null)
                    {
                        req.senderPlaceId = this.CreateSenderPlace(req.senderId, req.senderStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPlaceEntity.CustomerPlace = req.senderStore;
                        senderPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender3 == 4)
                {
                    if (senderAddressEntity == null)
                    {
                        req.senderAddressId = this.CreateSenderAddress(req.senderId, req.senderStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderAddressEntity.CustomerAddress = req.senderStore;
                        senderAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender3 == 5)
                {
                    if (senderTelpEntity == null)
                    {
                        this.CreateSenderTelp(req.senderStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderTelpEntity.Telp       = req.senderStore;
                        senderTelpEntity.UpdatedBy  = req.uiUserProfileEntity.userId.ToString();
                        senderTelpEntity.UpdatedAt  = DateTime.UtcNow;
                    }
                }

                //-----------PLACE-------------------------
                if (req.paramSender4 == 0)
                {
                    if (senderPhoneEntity == null)
                    {
                        this.CreateSenderPhone(req.senderPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPhoneEntity.Phone     = req.senderPlace;
                        senderPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender4 == 1)
                {
                    if (senderNameEntity == null)
                    {
                        req.senderNameId = this.CreateSenderName(req.senderId, req.senderPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderNameEntity.CustomerName = req.senderPlace;
                        senderNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderNameEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender4 == 2)
                {
                    if (senderStoreEntity == null)
                    {
                        req.senderStoreId = this.CreateSenderStore(req.senderId, req.senderPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderStoreEntity.CustomerStore = req.senderPlace;
                        senderStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender4 == 3)
                {
                    if (senderPlaceEntity == null)
                    {
                        req.senderPlaceId = this.CreateSenderPlace(req.senderId, req.senderPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPlaceEntity.CustomerPlace = req.senderPlace;
                        senderPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                    
                }

                if (req.paramSender4 == 4)
                {
                    if (senderAddressEntity == null)
                    {
                        req.senderAddressId = this.CreateSenderAddress(req.senderId, req.senderPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderAddressEntity.CustomerAddress = req.senderPlace;
                        senderAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                    
                }


                if (req.paramSender4 == 5)
                {
                    if (senderTelpEntity == null)
                    {
                        this.CreateSenderTelp(req.senderPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderTelpEntity.Telp       = req.senderPlace;
                        senderTelpEntity.UpdatedBy  = req.uiUserProfileEntity.userId.ToString();
                        senderTelpEntity.UpdatedAt  = DateTime.UtcNow;
                    }
                }

                //-----------ADDRESS-------------------------
                if (req.paramSender5 == 0)
                {
                    if (senderPhoneEntity == null)
                    {
                        this.CreateSenderPhone(req.senderAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPhoneEntity.Phone = req.senderAddress;
                        senderPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender5 == 1)
                {
                    if (senderNameEntity == null)
                    {
                        req.senderNameId = this.CreateSenderName(req.senderId, req.senderAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderNameEntity.CustomerName = req.senderAddress;
                        senderNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderNameEntity.UpdatedAt = DateTime.UtcNow;
                    }
                    
                }

                if (req.paramSender5 == 2)
                {
                    if (senderStoreEntity == null)
                    {
                        req.senderStoreId = this.CreateSenderStore(req.senderId, req.senderAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderStoreEntity.CustomerStore = req.senderAddress;
                        senderStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender5 == 3)
                {
                    if (senderPlaceEntity == null)
                    {
                        req.senderPlaceId = this.CreateSenderPlace(req.senderId, req.senderAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPlaceEntity.CustomerPlace = req.senderAddress;
                        senderPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender5 == 4)
                {
                    if (senderAddressEntity == null)
                    {
                        req.senderAddressId = this.CreateSenderAddress(req.senderId, req.senderAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderAddressEntity.CustomerAddress = req.senderAddress;
                        senderAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender5 == 5)
                {
                    if (senderTelpEntity == null)
                    {
                        this.CreateSenderTelp(req.senderAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderTelpEntity.Telp = req.senderAddress;
                        senderTelpEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderTelpEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }


                //-----------TELEPON-------------------------
                if (req.paramSender6 == 0)
                {
                    if (senderPhoneEntity == null)
                    {
                        this.CreateSenderPhone(req.senderTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPhoneEntity.Phone = req.senderTelp;
                        senderPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender6 == 1)
                {
                    if (senderNameEntity == null)
                    {
                        req.senderNameId = this.CreateSenderName(req.senderId, req.senderTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderNameEntity.CustomerName = req.senderTelp;
                        senderNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderNameEntity.UpdatedAt = DateTime.UtcNow;
                    }

                }

                if (req.paramSender6 == 2)
                {
                    if (senderStoreEntity == null)
                    {
                        req.senderStoreId = this.CreateSenderStore(req.senderId, req.senderTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderStoreEntity.CustomerStore = req.senderTelp;
                        senderStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender6 == 3)
                {
                    if (senderPlaceEntity == null)
                    {
                        req.senderPlaceId = this.CreateSenderPlace(req.senderId, req.senderTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderPlaceEntity.CustomerPlace = req.senderTelp;
                        senderPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender6 == 4)
                {
                    if (senderAddressEntity == null)
                    {
                        req.senderAddressId = this.CreateSenderAddress(req.senderId, req.senderTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderAddressEntity.CustomerAddress = req.senderTelp;
                        senderAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramSender6 == 5)
                {
                    if (senderTelpEntity == null)
                    {
                        this.CreateSenderTelp(req.senderTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        senderTelpEntity.Telp = req.senderTelp;
                        senderTelpEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        senderTelpEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }
            }
            else
            {
                //------------------------------------------- receiver ------------------------------------------

                //-----------PHONE-------------------------
                if (req.paramReceiver1 == 0)
                {
                    if (receiverPhoneEntity == null)
                    {
                        this.CreateReceiverPhone(req.receiverPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPhoneEntity.Phone = req.receiverPhone;
                        receiverPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver1 == 1)
                {
                    if (receiverNameEntity == null)
                    {
                        req.receiverNameId = this.CreateReceiverName(req.receiverId, req.receiverPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverNameEntity.CustomerName = req.receiverPhone;
                        receiverNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverNameEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver1 == 2)
                {
                    if (receiverStoreEntity == null)
                    {
                        req.receiverStoreId = this.CreateReceiverStore(req.receiverId, req.receiverPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverStoreEntity.CustomerStore = req.receiverPhone;
                        receiverStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver1 == 3)
                {
                    if (receiverPlaceEntity == null)
                    {
                        req.receiverPlaceId = this.CreateReceiverPlace(req.receiverId, req.receiverPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPlaceEntity.CustomerPlace = req.receiverPhone;
                        receiverPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver1 == 4)
                {
                    if (receiverAddressEntity == null)
                    {
                        req.receiverAddressId = this.CreateReceiverAddress(req.receiverId, req.receiverPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverAddressEntity.CustomerAddress = req.receiverPhone;
                        receiverAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver1 == 5)
                {
                    if (receiverTelpEntity == null)
                    {
                        this.CreateReceiverTelp(req.receiverPhone, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverTelpEntity.Telp = req.receiverPhone;
                        receiverTelpEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverTelpEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                //-----------NAME-------------------------
                if (req.paramReceiver2 == 0)
                {
                    if (receiverPhoneEntity == null)
                    {
                        this.CreateReceiverPhone(req.receiverName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPhoneEntity.Phone = req.receiverName;
                        receiverPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver2 == 1)
                {
                    if (receiverNameEntity == null)
                    {
                        req.receiverNameId = this.CreateReceiverName(req.receiverId, req.receiverName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverNameEntity.CustomerName = req.receiverName;
                        receiverNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverNameEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver2 == 2)
                {
                    if (receiverStoreEntity == null)
                    {
                        req.receiverStoreId = this.CreateReceiverStore(req.receiverId, req.receiverName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverStoreEntity.CustomerStore = req.receiverName;
                        receiverStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver2 == 3)
                {
                    if (receiverPlaceEntity == null)
                    {
                        req.receiverPlaceId = this.CreateReceiverPlace(req.receiverId, req.receiverName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPlaceEntity.CustomerPlace = req.receiverName;
                        receiverPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver2 == 4)
                {
                    if (receiverAddressEntity == null)
                    {
                        req.receiverAddressId = this.CreateReceiverAddress(req.receiverId, req.receiverName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverAddressEntity.CustomerAddress = req.receiverName;
                        receiverAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver2 == 5)
                {
                    if (receiverTelpEntity == null)
                    {
                        this.CreateReceiverTelp(req.receiverName, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverTelpEntity.Telp = req.receiverName;
                        receiverTelpEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverTelpEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                //-----------STORE-------------------------
                if (req.paramReceiver3 == 0)
                {
                    if (receiverPhoneEntity == null)
                    {
                        this.CreateReceiverPhone(req.receiverStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPhoneEntity.Phone = req.receiverStore;
                        receiverPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver3 == 1)
                {
                    if (receiverNameEntity == null)
                    {
                        req.receiverNameId = this.CreateReceiverName(req.receiverId, req.receiverStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverNameEntity.CustomerName = req.receiverStore;
                        receiverNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverNameEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver3 == 2)
                {
                    if (receiverStoreEntity == null)
                    {
                        req.receiverStoreId = this.CreateReceiverStore(req.receiverId, req.receiverStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverStoreEntity.CustomerStore = req.receiverStore;
                        receiverStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver3 == 3)
                {
                    if (receiverPlaceEntity == null)
                    {
                        req.receiverPlaceId = this.CreateReceiverPlace(req.receiverId, req.receiverStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPlaceEntity.CustomerPlace = req.receiverStore;
                        receiverPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver3 == 4)
                {
                    if (receiverAddressEntity == null)
                    {
                        req.receiverAddressId = this.CreateReceiverAddress(req.receiverId, req.receiverStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverAddressEntity.CustomerAddress = req.receiverStore;
                        receiverAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver3 == 5)
                {
                    if (receiverTelpEntity == null)
                    {
                        this.CreateReceiverTelp(req.receiverStore, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverTelpEntity.Telp = req.receiverStore;
                        receiverTelpEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverTelpEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                //-----------PLACE-------------------------
                if (req.paramReceiver4 == 0)
                {
                    if (receiverPhoneEntity == null)
                    {
                        this.CreateReceiverPhone(req.receiverPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPhoneEntity.Phone = req.receiverPlace;
                        receiverPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver4 == 1)
                {
                    if (receiverNameEntity == null)
                    {
                        req.receiverNameId = this.CreateReceiverName(req.receiverId, req.receiverPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverNameEntity.CustomerName = req.receiverPlace;
                        receiverNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverNameEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver4 == 2)
                {
                    if (receiverStoreEntity == null)
                    {
                        req.receiverStoreId = this.CreateReceiverStore(req.receiverId, req.receiverPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverStoreEntity.CustomerStore = req.receiverPlace;
                        receiverStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver4 == 3)
                {
                    if (receiverPlaceEntity == null)
                    {
                        req.receiverPlaceId = this.CreateReceiverPlace(req.receiverId, req.receiverPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPlaceEntity.CustomerPlace = req.receiverPlace;
                        receiverPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }

                }

                if (req.paramReceiver4 == 4)
                {
                    if (receiverAddressEntity == null)
                    {
                        req.receiverAddressId = this.CreateReceiverAddress(req.receiverId, req.receiverPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverAddressEntity.CustomerAddress = req.receiverPlace;
                        receiverAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }

                }


                if (req.paramReceiver4 == 5)
                {
                    if (receiverTelpEntity == null)
                    {
                        this.CreateReceiverTelp(req.receiverPlace, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverTelpEntity.Telp = req.receiverPlace;
                        receiverTelpEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverTelpEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                //-----------ADDRESS-------------------------
                if (req.paramReceiver5 == 0)
                {
                    if (receiverPhoneEntity == null)
                    {
                        this.CreateReceiverPhone(req.receiverAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPhoneEntity.Phone = req.receiverAddress;
                        receiverPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver5 == 1)
                {
                    if (receiverNameEntity == null)
                    {
                        req.receiverNameId = this.CreateReceiverName(req.receiverId, req.receiverAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverNameEntity.CustomerName = req.receiverAddress;
                        receiverNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverNameEntity.UpdatedAt = DateTime.UtcNow;
                    }

                }

                if (req.paramReceiver5 == 2)
                {
                    if (receiverStoreEntity == null)
                    {
                        req.receiverStoreId = this.CreateReceiverStore(req.receiverId, req.receiverAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverStoreEntity.CustomerStore = req.receiverAddress;
                        receiverStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver5 == 3)
                {
                    if (receiverPlaceEntity == null)
                    {
                        req.receiverPlaceId = this.CreateReceiverPlace(req.receiverId, req.receiverAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPlaceEntity.CustomerPlace = req.receiverAddress;
                        receiverPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver5 == 4)
                {
                    if (receiverAddressEntity == null)
                    {
                        req.receiverAddressId = this.CreateReceiverAddress(req.receiverId, req.receiverAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverAddressEntity.CustomerAddress = req.receiverAddress;
                        receiverAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver5 == 5)
                {
                    if (receiverTelpEntity == null)
                    {
                        this.CreateReceiverTelp(req.receiverAddress, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverTelpEntity.Telp = req.receiverAddress;
                        receiverTelpEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverTelpEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }


                //-----------TELEPON-------------------------
                if (req.paramReceiver6 == 0)
                {
                    if (receiverPhoneEntity == null)
                    {
                        this.CreateReceiverPhone(req.receiverTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPhoneEntity.Phone = req.receiverTelp;
                        receiverPhoneEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPhoneEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver6 == 1)
                {
                    if (receiverNameEntity == null)
                    {
                        req.receiverNameId = this.CreateReceiverName(req.receiverId, req.receiverTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverNameEntity.CustomerName = req.receiverTelp;
                        receiverNameEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverNameEntity.UpdatedAt = DateTime.UtcNow;
                    }

                }

                if (req.paramReceiver6 == 2)
                {
                    if (receiverStoreEntity == null)
                    {
                        req.receiverStoreId = this.CreateReceiverStore(req.receiverId, req.receiverTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverStoreEntity.CustomerStore = req.receiverTelp;
                        receiverStoreEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverStoreEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver6 == 3)
                {
                    if (receiverPlaceEntity == null)
                    {
                        req.receiverPlaceId = this.CreateReceiverPlace(req.receiverId, req.receiverTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverPlaceEntity.CustomerPlace = req.receiverTelp;
                        receiverPlaceEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverPlaceEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver6 == 4)
                {
                    if (receiverAddressEntity == null)
                    {
                        req.receiverAddressId = this.CreateReceiverAddress(req.receiverId, req.receiverTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverAddressEntity.CustomerAddress = req.receiverTelp;
                        receiverAddressEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverAddressEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

                if (req.paramReceiver6 == 5)
                {
                    if (receiverTelpEntity == null)
                    {
                        this.CreateReceiverTelp(req.receiverTelp, req.uiUserProfileEntity.userId);
                    }
                    else
                    {
                        receiverTelpEntity.Telp = req.receiverTelp;
                        receiverTelpEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                        receiverTelpEntity.UpdatedAt = DateTime.UtcNow;
                    }
                }

            }

            await _context.SaveChangesAsync(cancellationToken);

            rtn.Status  = StatusCodes.Status200OK;
            rtn.Data    = req;

            // return
            return rtn;
        }

        public Int32? CreateSenderPhone(String? senderPhone, Int32? userId)
        {
            CustomerEntity entity = new CustomerEntity();

            entity.Phone        = senderPhone;
            entity.CreatedBy    = userId.ToString();
            entity.CreatedAt    = DateTime.UtcNow;

            _context.CustomerEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerId;
        }

        public Int32? CreateSenderTelp(String? senderTelp, Int32? userId)
        {
            CustomerEntity entity = new CustomerEntity();

            entity.Telp      = senderTelp;
            entity.CreatedBy = userId.ToString();
            entity.CreatedAt = DateTime.UtcNow;

            _context.CustomerEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerId;
        }

        public Int32? CreateSenderName(Int32? senderId, String? senderName, Int32? userId)
        {
            CustomerNameEntity entity = new CustomerNameEntity();

            entity.CustomerId       = senderId;
            entity.CustomerName     = senderName;
            entity.CreatedBy        = userId.ToString();
            entity.CreatedAt        = DateTime.UtcNow;

            _context.CustomerNameEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerNameId;
        }
        public Int32? CreateSenderStore(Int32? senderId, String? senderStore, Int32? userId)
        {
            CustomerStoreEntity entity = new CustomerStoreEntity();

            entity.CustomerId       = senderId;
            entity.CustomerStore    = senderStore;
            entity.CreatedBy        = userId.ToString();
            entity.CreatedAt        = DateTime.UtcNow;

            _context.CustomerStoreEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerStoreId;
        }

        public Int32? CreateSenderPlace(Int32? senderId, String? senderPlace, Int32? userId)
        {
            CustomerPlaceEntity entity = new CustomerPlaceEntity();

            entity.CustomerId       = senderId;
            entity.CustomerPlace    = senderPlace;
            entity.CreatedBy        = userId.ToString();
            entity.CreatedAt        = DateTime.UtcNow;

            _context.CustomerPlaceEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerPlaceId;
        }

        public Int32? CreateSenderAddress(Int32? senderId, String? senderAddress, Int32? userId)
        {
            CustomerAddressEntity entity = new CustomerAddressEntity();

            entity.CustomerId       = senderId;
            entity.CustomerAddress  = senderAddress;
            entity.CreatedBy        = userId.ToString();
            entity.CreatedAt        = DateTime.UtcNow;

            _context.CustomerAddressEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerAddressId;
        }

        public Int32? CreateReceiverPhone(String? receiverPhone, Int32? userId)
        {
            CustomerEntity entity = new CustomerEntity();

            entity.Phone     = receiverPhone;
            entity.CreatedBy = userId.ToString();
            entity.CreatedAt = DateTime.UtcNow;

            _context.CustomerEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerId;
        }

        public Int32? CreateReceiverTelp(String? receiverTelp, Int32? userId)
        {
            CustomerEntity entity = new CustomerEntity();

            entity.Telp      = receiverTelp;
            entity.CreatedBy = userId.ToString();
            entity.CreatedAt = DateTime.UtcNow;

            _context.CustomerEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerId;
        }

        public Int32? CreateReceiverName(Int32? receiverId, String? receiverName, Int32? userId)
        {
            CustomerNameEntity entity = new CustomerNameEntity();

            entity.CustomerId = receiverId;
            entity.CustomerName = receiverName;
            entity.CreatedBy = userId.ToString();
            entity.CreatedAt = DateTime.UtcNow;

            _context.CustomerNameEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerNameId;
        }
        public Int32? CreateReceiverStore(Int32? receiverId, String? receiverStore, Int32? userId)
        {
            CustomerStoreEntity entity = new CustomerStoreEntity();

            entity.CustomerId = receiverId;
            entity.CustomerStore = receiverStore;
            entity.CreatedBy = userId.ToString();
            entity.CreatedAt = DateTime.UtcNow;

            _context.CustomerStoreEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerStoreId;
        }

        public Int32? CreateReceiverPlace(Int32? receiverId, String? receiverPlace, Int32? userId)
        {
            CustomerPlaceEntity entity = new CustomerPlaceEntity();

            entity.CustomerId = receiverId;
            entity.CustomerPlace = receiverPlace;
            entity.CreatedBy = userId.ToString();
            entity.CreatedAt = DateTime.UtcNow;

            _context.CustomerPlaceEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerPlaceId;
        }

        public Int32? CreateReceiverAddress(Int32? receiverId, String? receiverAddress, Int32? userId)
        {
            CustomerAddressEntity entity = new CustomerAddressEntity();

            entity.CustomerId = receiverId;
            entity.CustomerAddress = receiverAddress;
            entity.CreatedBy = userId.ToString();
            entity.CreatedAt = DateTime.UtcNow;

            _context.CustomerAddressEntitys.Add(entity);
            _context.SaveChanges();

            return entity.CustomerAddressId;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VSpbGoodsEntity
    {

        public String? SpbNo { get; set; }
        public long? SpbId { get; set; }
        public long? SpbGoodsId { get; set; }
        public decimal? Aw { get; set; }
        public decimal? Caw { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public String? KoliNo { get; set; }

    }





}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Commands
{
    public partial class CreateMenuPrivillegeCommand : IRequest<ReturnFormat>
    {
        public Int32? privilegeId { get; set; }
        public String? privilegeName { get; set; }
        public Int32? menuId { get; set; }

        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateMenuPrivillegeCommandHandler : IRequestHandler<CreateMenuPrivillegeCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateMenuPrivillegeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateMenuPrivillegeCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                //var dataItem = _context.PrivilegeUserEntitys.Where(w => w.UserId.Equals(req.userId)).FirstOrDefault();

                //dataItem.PrivilegeId = req.privilegeId;
                //dataItem.UpdatedBy = req.uiUserProfile.userId.ToString();
                //dataItem.UpdatedAt = DateTime.Now;

                //_context.PrivilegeUserEntitys.Update(dataItem);
                //_context.SaveChanges();

                //rtn.Status = StatusCodes.Status200OK;
                //rtn.Data = dataItem;
                ////
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }
    }

}

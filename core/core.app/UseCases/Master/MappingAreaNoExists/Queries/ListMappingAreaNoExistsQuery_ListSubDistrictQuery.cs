﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MappingAreaNoExists.Queries
{
    public partial class ListMappingAreaNoExistsQuery_ListSubDistrictQuery : IRequest<ReturnFormat>
    {
        public String? cityId { get; set; }
    }

    public class ListMappingAreaNoExistsQuery_ListSubDistrictQueryHandler : IRequestHandler<ListMappingAreaNoExistsQuery_ListSubDistrictQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListMappingAreaNoExistsQuery_ListSubDistrictQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListMappingAreaNoExistsQuery_ListSubDistrictQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.VCompanyAreaEntitys
                        where a.MasterCityId.Equals(req.cityId)
                        select new
                        {
                            a.MasterCityId
                            , a.MasterSubDistrictId
                            , MasterSubDistrictName = a.CompanyAreaNameCustom
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.MasterCityId
                        ,s.MasterSubDistrictId
                        ,s.MasterSubDistrictName
                    }).OrderBy(o => o.MasterSubDistrictName);
            }

            // return
            return rtn;
        }

    }
}

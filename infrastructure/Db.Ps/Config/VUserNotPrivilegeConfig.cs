﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class VUserNotPrivilegeConfig : IEntityTypeConfiguration<VUserNotPrivilegeEntity>
    {
        public void Configure(EntityTypeBuilder<VUserNotPrivilegeEntity> builder)
        {
            builder.HasKey(e => e.UserId);
            builder.ToTable("VUserNotPrivilege");
        }
    }
}

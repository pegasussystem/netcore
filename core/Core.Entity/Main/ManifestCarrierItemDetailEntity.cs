﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class ManifestCarrierItemDetailEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }

        public Int32? ManifestCarrierItemDetailId { get; set; }
        public Int64? ManifestId { get; set; }
        public Int64? SpbId { get; set; }
        public Int64? SpbGoodsId { get; set; }
        public Int32? Koli { get; set; }
    }
}

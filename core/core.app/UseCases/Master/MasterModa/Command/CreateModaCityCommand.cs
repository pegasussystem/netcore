﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Master.MasterModa.Command
{
    public partial class CreateModaCityCommand : IRequest<ReturnFormat>
    {
        public String? originCityId { get; set; }
        public String? destinationCityId { get; set; }
        public Int32? lookupId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateModaCityCommandHandler : IRequestHandler<CreateModaCityCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateModaCityCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateModaCityCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.ManageData(req.originCityId, req.destinationCityId, req.lookupId, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ModaCity_CreateEntity> ManageData(String? originCityId, String? destinationCityId, Int32? lookupId, Int32? userid)
        {
            String query = $@"EXEC SP_ModaCity_Create @OriginCityId='{originCityId}', @DestinationCityId='{destinationCityId}', @LookupId='{lookupId}', @UserId={userid} ";
            var res = this._context.SP_ModaCity_CreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
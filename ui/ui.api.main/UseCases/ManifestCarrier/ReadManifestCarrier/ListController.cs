﻿using core.app.UseCases.ManifestCarrier.Queries;
using core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ListController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public ListController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list")]
        public async Task<IActionResult> List([FromQuery] ListReadQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();


            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("get-detail")]
        public async Task<IActionResult> GetDetail([FromBody] GetManifestCarrirDetailQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();


            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("list-detail-cost")]
        public async Task<IActionResult> ListDetailCost([FromQuery] ListDetailCostReadQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();


            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SPManifestDetailEntity
    {
        public Decimal? Cons { get; set; }
        public Decimal? ConsCaw { get; set; }
        public long? ManifestKoliSpbId { get; set; }
        public Int32? CompanyId { get; set; }
        public long? ManifestId { get; set; }
        public long? ManifestKoliId { get; set; }
        public Int32? KoliNo { get; set; }
        public Decimal? aw { get; set; }
        public Decimal? Caw { get; set; }
        public String? SpbNo { get; set; }
        public String? PaymentMethod { get; set; }
        public String? DestinationAreaCode { get; set; }
        public String? SenderName { get; set; }
        public String? SenderStore { get; set; }
        public String? ReceiverName { get; set; }
        public String? ReceiverStore { get; set; }
        public Decimal? TunaiAsal { get; set; }
        public Decimal? TunaiTujuan { get; set; }
        public Byte? IsClose { get; set; }

        public String? Carrier { get; set; }
        public Decimal? Rates { get; set; }
        public Decimal? CawBDJ { get; set; }
        public Decimal? CawBJU { get; set; }
        public Decimal? CawMTP { get; set; }
        public Decimal? CawSRI { get; set; }
        public Decimal? CawBNT { get; set; }
        public Decimal? CawSGT { get; set; }
        public Int32? IsAgree { get; set; } // Pati 25Jan22
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_ARListEntity
    {
        public Int64 arId { get; set; }
        public DateTime createdAt { get; set; } 
        public String? keterangan { get; set; }
        public String? coaCode { get; set; }
        public String? coaName { get; set; }
        public Decimal? debit { get; set; }
        public Decimal? credit { get; set; }
         public Decimal? saldo { get; set; }
         public Decimal? cumulativeBalance { get; set; }
    }
}

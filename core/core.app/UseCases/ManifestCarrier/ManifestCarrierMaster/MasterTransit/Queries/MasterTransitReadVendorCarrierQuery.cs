﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit.Queries
{
     public partial class MasterTransitReadVendorCarrierQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MasterTransitReadVendorCarrierQueryHandler : IRequestHandler<MasterTransitReadVendorCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MasterTransitReadVendorCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MasterTransitReadVendorCarrierQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // --- --- --- --- ---
            var qr = from a in _context.VendorCarrierEntitys
                        join b in _context.VendorEntitys on a.VendorId equals b.VendorId
                        select new
                        {
                            VendorCarrierId = b.VendorId
                            , VendorCarrierName = b.VendorName
                        };

            if (qr != null)
            {
                var query = qr.Distinct();
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.VendorCarrierId
                        , s.VendorCarrierName
                    }).OrderBy(o => o.VendorCarrierId);
            }

            //
            // return
            return rtn;
        }
    }
}

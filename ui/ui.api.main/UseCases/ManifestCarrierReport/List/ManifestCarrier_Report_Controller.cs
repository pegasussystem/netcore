﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.ManifestCarrier_Report.List;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ui.api.main.UseCases.ManifestCarrierReport.List
{
    [Authorize]
    [Route("manifest-carrier-report")]
    [ApiController]
    public class ManifestCarrierReportController : DefaultController
    {

        private readonly IAppWrapperPs _appPs;
        public ManifestCarrierReportController(IAppWrapperPs app)
        {
            _appPs = app;
            

        }


        [HttpGet("list")]
        public IActionResult Execute([FromQuery] ManifestCarrier_Report_List_Input input)
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();

            // logic
            // ///// ///// /////
            _returnFormat = _appPs.ManifestCarrier_Report_List_UseCase.Read(GetUserClaim(), input);

            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

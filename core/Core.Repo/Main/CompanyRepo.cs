﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class CompanyRepo : Repo<CompanyEntity>, ICompanyRepo
    {
        public CompanyRepo(DbContextPs ctx) : base(ctx)
        {
        }


            public IEnumerable<CompanyEntity> ExecWithStoreProcedure(string query, params object[] parameters)
        {
            //return this._contextPs.Query<CompanyEntity>(query, parameters);
            return this._contextPs.Query<CompanyEntity>();
        }
    }
}

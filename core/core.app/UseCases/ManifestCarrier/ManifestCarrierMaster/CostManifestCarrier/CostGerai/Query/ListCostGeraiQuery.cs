﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostGerai.Query
{
    public partial class ListCostGeraiQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListCostGeraiQueryHandler : IRequestHandler<ListCostGeraiQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCostGeraiQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCostGeraiQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.CostGeraiEntitys
                        join b in _context.CostGeraiDetailEntitys on a.CostGeraiId equals b.CostGeraiId
                        join c in _context.MasterCityEntitys on a.ManifestOriginCityId equals c.MasterCityId
                        select new
                        {
                            a.CostGeraiId
                            , a.ManifestOriginCityId
                            , b.CostGeraiDetailId
                            , b.CostType
                            , b.Unit
                            , b.BaseCost
                            , b.CostGroup
                            , b.UseWeight
                            , c.MasterCityCode
                            , c.MasterCityName
                            , c.MasterCityCapital
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.CostGeraiId
                        , s.ManifestOriginCityId
                        , s.CostGeraiDetailId
                        , s.CostType
                        , s.Unit
                        , s.BaseCost
                        , s.CostGroup
                        , s.UseWeight
                        , s.MasterCityCode
                        , s.MasterCityName
                        , s.MasterCityCapital
                    }).OrderBy(o => o.CostGeraiId);
            }

            // return
            return rtn;
        }

    }
}

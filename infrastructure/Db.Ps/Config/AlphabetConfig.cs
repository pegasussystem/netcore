﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class AlphabetConfig : IEntityTypeConfiguration<AlphabetEntity>
    {
        public void Configure(EntityTypeBuilder<AlphabetEntity> builder)
        {
            builder.HasKey(e => e.AlphabetId);
            builder.ToTable("Alphabet");
        }
    }
}

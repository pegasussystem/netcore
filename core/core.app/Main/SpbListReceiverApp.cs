﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace core.app.Main
{
    public class SpbListReceiverApp : ISpbListReceiverApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public SpbListReceiverApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Delete(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(UiUserProfileEntity uiUserProfileEntity, object obj)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VSpbEntity> vSpbEntities = null;
            BranchEntity branch = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic





            // from branch
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId != null && uiUserProfileEntity.SubBranchId == null)
            {
                // get branch handled 
                branch = _repoPs.BranchRepo.Read(w => w.BranchId.Equals(uiUserProfileEntity.BranchId)).FirstOrDefault();
                var branchList = (branch.CityHandled != null) ? branch.CityHandled.Replace("|<", "").Replace(">|", "").Split("><") : null;

                vSpbEntities = _repoPs.VSpbRepo
                    .Read(w => branchList.Contains(w.DestinationCityId))
                    .ToList();
            }

            // from company
            if (uiUserProfileEntity.companyId != null && uiUserProfileEntity.BranchId == null && uiUserProfileEntity.SubBranchId == null)
            {
                vSpbEntities = _repoPs.VSpbRepo.Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)).ToList();
            }



            if (vSpbEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vSpbEntities
                    .Select(s => new
                    {
                        s.SpbId,
                        s.SpbNo,
                        s.Username,
                        s.OriginCityNameCustom,
                        s.DestinationCityNameCustom,
                        s.Rates,
                        s.Packing,
                        s.Quarantine,
                        s.Etc,
                        s.Ppn,
                        s.Discount,
                        s.TotalPrice,
                        s.SenderName,
                        s.SenderPhone,
                        s.SenderPlace,
                        s.SenderStore,
                        s.SenderAddress,
                        s.ReceiverName,
                        s.ReceiverPhone,
                        s.ReceiverPlace,
                        s.ReceiverStore,
                        s.ReceiverAddress,
                        s.CarrierName,
                        s.PaymentMethod,
                        s.QualityOfServiceName,
                        s.TypeOfServiceName,
                        CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)uiUserProfileEntity.WorkTimeZoneMinute)
                    });
            }

            //
            // return
            return rtn;
        }

        public ReturnFormat BulkPrint(UiUserProfileEntity uiUserProfileEntity, SpbEntity spbEntity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<SpbEntity> spbEntitys;
            DateTime serverDate = spbEntity.CreatedAt.Value.AddHours((Double) (uiUserProfileEntity.WorkTimeZoneHour*-1)).AddMinutes((Double)(uiUserProfileEntity.WorkTimeZoneMinute*-1));

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // get data
            //var ggg = _repoPs.SpbRepo
            //    .Read( w => w.CreatedAt.Value.Year.Equals(serverDate.Year) && w.CreatedAt.Value.Month.Equals(serverDate.Month) && w.CreatedAt.Value.Day.Equals(serverDate.Day))
            //    .ToList();

            spbEntitys = _repoPs.SpbRepo
               .Read(w => 
                    w.CreatedAt.Value.AddHours((Double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)uiUserProfileEntity.WorkTimeZoneMinute).Year.Equals(spbEntity.CreatedAt.Value.Year)
                    && w.CreatedAt.Value.AddHours((Double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)uiUserProfileEntity.WorkTimeZoneMinute).Month.Equals(spbEntity.CreatedAt.Value.Month)
                    && w.CreatedAt.Value.AddHours((Double)uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)uiUserProfileEntity.WorkTimeZoneMinute).Day.Equals(spbEntity.CreatedAt.Value.Day)
                    && w.TotalPrice != null
                    )
               .ToList();


            if (spbEntitys != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = String.Join(',', spbEntitys.Select(s => s.SpbNo).ToList());
            }

            //
            // return
            return rtn;
        }

        public ReturnFormat ReadSpbPrint(UiUserProfileEntity uiUserProfile, VSpbEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VSpbEntity> vSpbEntities = null;
            String printAtLocalTime = DateTime.UtcNow.AddHours((double)uiUserProfile.WorkTimeZoneHour).AddMinutes((double)uiUserProfile.WorkTimeZoneMinute).ToString("dd MMM yyyy HH:mm:ss");


            String[] splitSpbNo = entity.SpbNo.Split(",");

            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data
            vSpbEntities = _repoPs.VSpbRepo.Read(w => splitSpbNo.Contains(w.SpbNo)).ToList();

            if (vSpbEntities != null) {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vSpbEntities.OrderBy( o => o.SpbId)
                    .Select( (s, index) => new { 
                        s.Carrier, s.CarrierName, s.CompanyId,
                        s.CreatedAt, s.Description, s.DestinationAreaCode,
                        s.DestinationAreaId, s.DestinationAreaNameCustom,  s.DestinationCityCode, 
                        s.DestinationCityId, s.DestinationCityNameCustom, s.Discount,
                        s.Etc, s.OriginAreaCode, s.OriginAreaId, s.OriginAreaNameCustom,
                        s.OriginCityCode, s.OriginCityId, s.OriginCityNameCustom,
                        s.Packing, s.PaymentMethod, s.Ppn,
                        s.QualityOfService, s.QualityOfServiceName, s.Quarantine,
                        s.Rates, s.ReceiverAddress, s.ReceiverName,
                        s.ReceiverPhone, s.ReceiverPlace, s.ReceiverStore,
                        s.SenderAddress, s.SenderName, s.SenderPhone, s.SenderPlace,
                        s.SenderStore, s.SpbId, s.SpbNo,
                        s.TotalPrice, s.TypeOfService, s.TypeOfServiceName,
                        s.TypesOfGoods, s.TypesOfGoodsName, s.Username,
                        s.ViaName,
                        PrintAtLocalTime = printAtLocalTime,
                        SheetFor = "",
                    });
            }

            //
            // return
            return rtn;
        }

        public SpbEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(SpbEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

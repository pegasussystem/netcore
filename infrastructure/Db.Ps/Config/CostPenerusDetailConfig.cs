﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostPenerusDetailConfig : IEntityTypeConfiguration<CostPenerusDetailEntity>
    {
        public void Configure(EntityTypeBuilder<CostPenerusDetailEntity> builder)
        {
            builder.HasKey(e => e.CostPenerusDetailId);
            builder.ToTable("CostPenerusDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
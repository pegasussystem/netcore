﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostCarrierDetailPpnConfig : IEntityTypeConfiguration<CostCarrierDetailPpnEntity>
    {
        public void Configure(EntityTypeBuilder<CostCarrierDetailPpnEntity> builder)
        {
            builder.HasKey(e => e.CostCarrierDetailPpnId);
            builder.ToTable("CostCarrierDetailPpn");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
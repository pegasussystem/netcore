﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class VManifestRepo : Repo<VManifestEntity>, IVManifestRepo
    {
        public VManifestRepo(DbContextPs ctx) : base(ctx)
        {
        }

        
    }
}

﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
   class V_UserPrivilegeConfig : IEntityTypeConfiguration<V_UserPrivilegeEntity>
    {
        public void Configure(EntityTypeBuilder<V_UserPrivilegeEntity> builder)
        {
            builder.HasKey(e => e.UserId);
            builder.ToTable("V_UserPrivilege");
        }
    }
}

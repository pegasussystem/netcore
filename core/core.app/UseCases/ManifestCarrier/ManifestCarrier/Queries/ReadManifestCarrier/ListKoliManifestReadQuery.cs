﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListKoliManifestReadQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public Int64? manifestId { get; set; }
    }

    public class ListKoliManifestReadQueryHandler : IRequestHandler<ListKoliManifestReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListKoliManifestReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListKoliManifestReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from vmf in _context.V_ManifestKoliListEntitys
                        where vmf.ManifestId.Equals(req.manifestId)
                        select new
                        {
                            vmf.CreatedAt
                            , vmf.ManifestId
                            , vmf.ManifestNo
                            , vmf.OriginCityId
                            , vmf.OriginCityCode
                            , vmf.OriginCityName
                            , vmf.DestinationCityId
                            , vmf.DestinationCityCode
                            , vmf.DestinationCityName
                            , Koli = vmf.KoliNo
                            , KoliNo = vmf.KoliAlphabet
                            , vmf.SpbId
                            , vmf.SpbGoodsId
                            , vmf.Aw
                            , vmf.Caw
                            , vmf.Vw
                            , vmf.Length
                            , vmf.Width
                            , vmf.Height
                            , vmf.SpbGoodsItemId
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.CreatedAt
                        , s.ManifestId
                        , s.ManifestNo
                        , s.OriginCityId
                        , s.OriginCityCode
                        , s.OriginCityName
                        , s.DestinationCityId
                        , s.DestinationCityCode
                        , s.DestinationCityName
                        , s.Koli
                        , s.KoliNo
                        , s.SpbId
                        , s.SpbGoodsId
                        , s.Aw
                        , s.Caw
                        , s.Vw
                        , s.Length
                        , s.Width
                        , s.Height
                        , s.SpbGoodsItemId
                    }).OrderBy(o => o.Koli);
            }

            //
            // return
            return rtn;
        }
    }
}
﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_ManifestCarrierListConfig : IEntityTypeConfiguration<V_ManifestCarrierListEntity>
    {
        public void Configure(EntityTypeBuilder<V_ManifestCarrierListEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierId);
            builder.ToTable("V_ManifestCarrierList");
        }
    }
}

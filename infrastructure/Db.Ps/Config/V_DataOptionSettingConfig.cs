﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
     class V_DataOptionSettingConfig : IEntityTypeConfiguration<V_DataOptionSettingEntity>
    {
        public void Configure(EntityTypeBuilder<V_DataOptionSettingEntity> builder)
        {
            builder.HasKey(e => e.Id);
            builder.ToTable("V_DataOptionSetting");
        }
    }
}

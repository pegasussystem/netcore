﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListCityDestinationReadQuery : IRequest<ReturnFormat>
    {
        public Int32? destinationStationId { get; set; }
    }

    public class ListCityDestinationReadQueryHandler : IRequestHandler<ListCityDestinationReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCityDestinationReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCityDestinationReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from sc in _context.StationCityEntitys
                        join mc in _context.MasterCityEntitys on sc.CityId equals mc.MasterCityId
                        where sc.StationId.Equals(req.destinationStationId)
                        select new
                        {
                            sc.CityId
                           , CityName = mc.MasterCityName
                        };

            var query2 = query.Distinct();
            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(
                    s => new { 
                        s.CityId
                       ,  s.CityName
                    });
            }

            //
            // return
            return rtn;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MasterUrbanEntity
    {

        public Int32 MasterUrbanId { get; set; }
        public String UrbanCodeBps       { get; set; }
        public String UrbanNameBps       { get; set; }
        public String UrbanCodeDagri     { get; set; }
        public String UrbanNameDagri     { get; set; }
        public String SubDistrictCodeBps { get; set; }


    }
}

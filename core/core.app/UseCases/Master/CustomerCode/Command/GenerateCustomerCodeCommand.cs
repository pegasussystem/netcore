﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Master.CustomerCode.Command
{
    public partial class GenerateCustomerCodeCommand : IRequest<ReturnFormat>
    {
    }

    public class GenerateCustomerCodeCommandHandler : IRequestHandler<GenerateCustomerCodeCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public GenerateCustomerCodeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(GenerateCustomerCodeCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.GenerateCode();

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_GenerateCustomerCodeEntity> GenerateCode()
        {
            String query = $@"declare @a varchar(10), @b varchar(max)
                        EXEC SP_GenerateCustomerCode @a output, @b output
                        select @a as result, @b as message";
            var res = this._context.SP_GenerateCustomerCodeEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

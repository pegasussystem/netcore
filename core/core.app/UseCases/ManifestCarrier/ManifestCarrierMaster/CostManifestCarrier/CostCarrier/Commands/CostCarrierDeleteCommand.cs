﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostCarrier.Commands
{
    public partial class CostCarrierDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? costCarrierId { get; set; }
        public Int32? costCarrierDetailId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostCarrierDeleteCommandHandler : IRequestHandler<CostCarrierDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostCarrierDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostCarrierDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostCarrierDelete(req.costCarrierId, req.costCarrierDetailId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostCarrierDeleteEntity> CostCarrierDelete(Int32? costCarrierId, Int32? costCarrierDetailId)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostCarrierDelete @costCarrierId='{costCarrierId}', @costCarrierDetailId='{costCarrierDetailId}' ";
            var res = this._context.SP_ManifestCarrierCost_CostCarrierDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
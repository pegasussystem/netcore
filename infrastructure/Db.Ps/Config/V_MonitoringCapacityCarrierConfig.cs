﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_MonitoringCapacityCarrierConfig : IEntityTypeConfiguration<V_MonitoringCapacityCarrierEntity>
    {
        public void Configure(EntityTypeBuilder<V_MonitoringCapacityCarrierEntity> builder)
        {
            builder.HasKey(e => e.cid);
            builder.ToTable("V_MonitoringCapacityCarrier");
        }
    }
}

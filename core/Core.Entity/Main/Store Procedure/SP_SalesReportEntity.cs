using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_SalesReportEntity
    {
        public String? DestinationCityCode { get; set; }
        public String? Carrier { get; set; }
        public String? CoaCode { get; set; }
        public String? CoaName { get; set; }
        public Decimal? TotalAw { get; set; }
        public Decimal? TotalCaw { get; set; }
        public Decimal? Ta { get; set; }
        public Decimal? Tt { get; set; }
        public Decimal? Tk { get; set; }
        public Decimal? TotalTaTt { get; set; }
        public String? DigitPertama { get; set; }
        public String? DigitKedua { get; set; }
        public String? DigitKetiga { get; set; }
        public String? DigitKeempat { get; set; }
        public String? DigitKelima { get; set; }
        public String? DigitKeenam { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MDataCustomerSenderEntity
    {
        public Int64? Id { get; set; }
        public String? NoSPB { get; set; }
        public String? SPBCreate { get; set; }
        public String? Nomor { get; set; }
        public String? Nama { get; set; }
        public String? Toko { get; set; }
        public String? Tempat { get; set; }
        public String? Alamat { get; set; }
        public Int32? Rt { get; set; }
        public Int32? Rw { get; set; }
        public String? DesaKelurahan { get; set; }
        public String? Kecamatan { get; set; }
        public String? KabupatenKota { get; set; }
        public String? KodePos { get; set; }
        public String? Via { get; set; }
        public Decimal? TotalCAW { get; set; }
        public Int32? TotalKoli { get; set; }
        public Decimal? TotalHarga { get; set; }
        // public String? StatusVoid { get; set; }
    }
}

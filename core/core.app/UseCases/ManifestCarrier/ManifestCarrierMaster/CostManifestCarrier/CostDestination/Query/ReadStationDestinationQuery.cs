﻿using core.helper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostDestination.Query
{
   public partial class ReadStationDestinationQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadStationDestinationQueryHandler : IRequestHandler<ReadStationDestinationQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadStationDestinationQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadStationDestinationQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                rtn.Status = StatusCodes.Status200OK;
                var qr = from a in _context.VendorDestinationEntitys
                            join b in _context.StationEntitys on a.StationId equals b.StationId
                            select new
                        {
                            a.StationId
                            , b.StationName
                        };

                if (qr != null)
                {
                    var query = qr.Distinct();
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.StationId
                            , s.StationName
                        }).OrderBy(o => o.StationId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
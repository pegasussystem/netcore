﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;
using Core.Entity.Ui;

namespace core.app.UseCases.MasterDataCustomer.Queries
{

    public partial class MDataCustomerReceiverQuery : IRequest<ReturnFormat>
    {
        public Int32? customerId { get; set; }
        public Int32? customerNameId { get; set; }
        public Int32? customerStoreId { get; set; }
        public Int32? customerPlaceId { get; set; }
        public Int32? customerAddressId { get; set; }
        public String? dateStart { get; set; }
        public String? dateEnd { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class MDataCustomerReceiverQueryHandler : IRequestHandler<MDataCustomerReceiverQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MDataCustomerReceiverQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MDataCustomerReceiverQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_GetDataCustomerDetailEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataCustomerDetail(req.customerId, req.customerNameId, req.customerStoreId, req.customerPlaceId, req.customerAddressId, req.dateStart, req.dateEnd);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                        s.SpbId
	                    , s.SpbNo
	                    , s.CreatedAt
                        , CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("dddd, dd MMMM yyyy")
                        , s.Telepon
	                    , s.Phone
	                    , s.CustomerName
	                    , s.CustomerStore
	                    , s.CustomerPlace
	                    , s.CustomerAddress
	                    , s.Rt
	                    , s.Rw
	                    , s.MasterUrbanName
	                    , s.MasterSubDistrictName
	                    , s.MasterCityName
	                    , s.MasterUrbanPostalCode
	                    , s.ViaName
	                    , s.TotalCAW
	                    , s.TotalKoli
	                    , s.TotalPrice
                    }).OrderByDescending(o => o.CreatedAt);
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_GetDataCustomerDetailEntity> DataCustomerDetail(Int32? customerId, Int32? customerNameId, Int32? customerStoreId, Int32? customerPlaceId, Int32? customerAddressId, String? dateStart, String? dateEnd)
        {
            String query = $@"EXEC SP_GetDataCustomerDetail @customerId='{customerId}', @customerNameId='{customerNameId}', @customerStoreId='{customerStoreId}', @customerPlaceId='{customerPlaceId}', @customerAddressId='{customerAddressId}', @dateStart='{dateStart}', @dateEnd='{dateEnd}' ,@type='receiver'";
            var res = this._context.SP_GetDataCustomerDetailEntitys.FromSqlRaw(query).ToList();
            return res;
        }


    }

}

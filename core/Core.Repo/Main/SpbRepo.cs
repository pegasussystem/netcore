﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class SpbRepo : Repo<SpbEntity>, ISpbRepo
    {
        public SpbRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public IEnumerable<SPSpbNumberGeneratorEntity> SpbNumberGenerator(UiUserProfileEntity entity)
        {
            String query = $@"EXEC [SpbNumberGenerator] @CompanyId={entity.companyId}, @WorkType={entity.workType}, @WorkId={entity.workId}, @BranchId = { (entity.BranchId == null ?  "NULL" : entity.BranchId.ToString()) }, @SubBranchId = { (entity.SubBranchId == null ? "NULL" : entity.SubBranchId.ToString()) },
                             @WorkTimeZoneHour = {entity.WorkTimeZoneHour}, @WorkTimeZoneMinute = {entity.WorkTimeZoneMinute}, @UserId= {entity.userId}";
            //return this._contextPs.Query<CompanyEntity>(query, parameters);
            return this._contextPs.Set<SPSpbNumberGeneratorEntity>()
                .FromSqlRaw(query);
        }

        public IEnumerable<SPSpb_SetManifestIdEntity> SPSpb_SetManifestIdEntity(SpbEntity spbEntity)
        {
            return this._contextPs.Set<SPSpb_SetManifestIdEntity>()
                .FromSqlRaw($@"EXEC SPSpb_SetManifestId @SpbId={spbEntity.SpbId}");
        }

        public IEnumerable<SPSpb_VoidEntity> SPSpb_Void(UiUserProfileEntity entity, VSpbEntity spbEntity)
        {
            String query = $@"EXEC SPSpb_Void @SpbId={spbEntity.SpbId}, @VoidBy={entity.userId}, @VoidReason='{spbEntity.VoidReason}'";
            return this._contextPs.Set<SPSpb_VoidEntity>()
                .FromSqlRaw(query);
        }

        // STATISTIK
        // -----------------------

        public IEnumerable<SPSpb_WaktuInputDailyEntity> SPSpb_WaktuInputDailyEntity(DateTime dateFrom, DateTime dateTo)
        {
            String query = $@"EXEC Spb_WaktuInputDaily 
                @FROM_YEAR_={dateFrom.Year} ,@FROM_MONTH_={dateFrom.Month}, @FROM_DAY_={dateFrom.Day},
                @TO_YEAR_={dateTo.Year} ,@TO_MONTH_={dateTo.Month}, @TO_DAY_={dateTo.Day}
";
            return this._contextPs.Set<SPSpb_WaktuInputDailyEntity>()
                .FromSqlRaw(query);
        }

    }
}

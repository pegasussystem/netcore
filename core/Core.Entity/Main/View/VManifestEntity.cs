﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VManifestEntity
    {

        public DateTime? CreatedAt { get; set; }
        public String? CreatedAtLocal { get; set; }
        public int? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public Int32? SubBranchId { get; set; }
        public long? ManifestId { get; set; }
        public String? ManifestNo { get; set; }
        public String? Alphabet { get; set; }
        public String? Carrier { get; set; }


        //#OriginCity	
        public String? OriginCityId { get; set; }
        public String? OriginCityCode { get; set; }
        public String? OriginCityName { get; set; }


        //#DestinationCity	
        public String? DestinationCityId { get; set; }
        public String? DestinationCityCode { get; set; }
        public String? DestinationnCityName { get; set; }

        //#Province	
        public String? ProvinceId { get; set; }
        public String? ProvinceCode { get; set; }
        public String? ProvinceName { get; set; }

        public Boolean? IsOpen { get; set; }

    }





}

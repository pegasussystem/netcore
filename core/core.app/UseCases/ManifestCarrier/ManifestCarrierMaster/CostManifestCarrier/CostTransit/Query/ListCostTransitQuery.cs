﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostTransit.Query
{
    public partial class ListCostTransitQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListCostTransitQueryHandler : IRequestHandler<ListCostTransitQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCostTransitQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCostTransitQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.CostTransitEntitys
                        join b in _context.StationEntitys on a.StationTransitId equals b.StationId
                        join c in _context.CarrierEntitys on a.CarrierId equals c.CarrierId
                        join d in _context.VendorTransitEntitys on a.VendorTransitId equals d.VendorTransitId
                        join g in _context.VendorEntitys on d.VendorId equals g.VendorId
                        join e in _context.CostTransitDetailEntitys on a.CostTransitId equals e.CostTransitId
                        join f in _context.LookupEntitys on e.Unit equals f.LookupKey

                        join sc in _context.StationCityEntitys on b.StationId equals sc.StationId
                        join h in _context.MasterCityEntitys on sc.CityId equals h.MasterCityId
                        where f.LookupKeyParent.Equals("Unit") && sc.Priority.Equals("1")

                        select new
                        {
                            a.CostTransitId
                            , e.CostTransitDetailId
                            , StationName = "[ " + h.MasterCityCode + " ] - " + b.StationName
                            , CarrierName = "[ "+ c.CarrierCode + " ] - "+c.CarrierName
                            , g.VendorName
                            , f.LookupValue
                            , e.BaseCost
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CostTransitId,
                        s.CostTransitDetailId,
                        s.StationName,
                        s.CarrierName,
                        s.VendorName,
                        s.LookupValue,
                        s.BaseCost
                        
                    }).OrderByDescending(o => o.CostTransitId);
            }

            // return
            return rtn;
        }

    }
}

﻿using core.app.UseCases.Base;
using core.app.UseCases.Base.Commands;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Base
{
    [Authorize]
    [Route("history-login")]
    [ApiController]

    public class HistoryLoginController : ApiController
    {
        [HttpPost("start-time")]
        public async Task<IActionResult> StartTimePage([FromBody] CreateHistoryLoginCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        //[HttpPost("start-time")]
        //public async Task<IActionResult> StartTimePage([FromBody] StartTimePageCommand q)
        //{

        //    // --- variable
        //    // --- --- --- --- ---
        //    ReturnFormat _returnFormat = new ReturnFormat();

        //    // --- set variable
        //    // --- --- --- --- ---
        //    q.uiUserProfile = GetUserClaim();

        //    // --- get data
        //    _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


        //    // --- return
        //    // --- --- --- --- ---
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}

        [HttpPost("end-time")]
        public async Task<IActionResult> EndTimePage([FromBody] EndTimePageCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}
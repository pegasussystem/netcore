﻿using core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostTransit.Commands;
using core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostTransit.Query;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.CostManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]

    public class CostTransitController : ApiController
    {
        [HttpGet("list-cost-transit")]
        public async Task<IActionResult> ListCostTransit([FromQuery] ListCostTransitQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("manifest-carrier-cost-transit-list-station")]
        public async Task<IActionResult> ReadCity([FromQuery] ManifestCarrierCostTransitListStationQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("manifest-carrier-cost-transit-list-carrier")]
        public async Task<IActionResult> ReadCarrier([FromQuery] ManifestCarrierCostTransitListCarrierQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpGet("manifest-carrier-cost-transit-list-vendor")]
        public async Task<IActionResult> ReadVendor([FromQuery] ManifestCarrierCostTransitListVendorQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }



        [HttpPost("cost-transit-create")]
        public async Task<IActionResult> CostTransitCreate([FromBody] CostTransitCreateCommand q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

      [HttpPost("cost-transit-update")]
      public async Task<IActionResult> CostTransitUpdate([FromBody] CostTransitUpdateCommand q)
      {
          // --- variable
          // --- --- --- --- ---
          ReturnFormat _returnFormat = new ReturnFormat();

          // --- set variable
          // --- --- --- --- ---
          q.uiUserProfile = GetUserClaim();

          // --- get data
          // --- --- --- --- ---
          _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


          // --- return
          // --- --- --- --- ---
          return StatusCode(_returnFormat.Status, _returnFormat);
      }

       [HttpPost("cost-transit-delete")]
       public async Task<IActionResult> CostTransitDelete([FromBody] CostTransitDeleteCommand q)
       {
           // --- variable
           // --- --- --- --- ---
           ReturnFormat _returnFormat = new ReturnFormat();

           // --- set variable
           // --- --- --- --- ---
           q.uiUserProfile = GetUserClaim();

           // --- get data
           // --- --- --- --- ---
           _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


           // --- return
           // --- --- --- --- ---
           return StatusCode(_returnFormat.Status, _returnFormat);
       }

    }
}

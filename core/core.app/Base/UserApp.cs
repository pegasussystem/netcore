﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Base;
using Core.Entity.Main;
using Core.Entity.Ui;
//using Core.Validator.Base;
using FluentValidation;
using FluentValidation.Results;
using Interface.App.Base;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace core.app.Base
{
    public class UserApp : IUserApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public UserApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(UserEntity entity)
        {
            throw new NotImplementedException();
        }


        public UserEntity Delete(UserEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// used for validated data before login
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ReturnFormat Read(UserEntity entity)
        {
            //
            // -- variable
            ReturnFormat rtn = new ReturnFormat();
            List<UserEntity> userEntities = new List<UserEntity>();

            //
            // -- set variable
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // TODO : validation

            //
            // TOOD : enkirp password
            userEntities = _repoPs.UserRepo.Read(w => w.Username.Equals(entity.Username) && w.Password.Equals(entity.Password))
            .ToList();


            // get user profile
            if (userEntities.Count >= 1)
            {
                rtn.Data = _repoPs.UserRepo.ReadVUserWithProfile(w => w.UserId.Equals(userEntities[0].UserId)).ToList();
                rtn.Status = StatusCodes.Status200OK;
            }


            // -- return
            return rtn;
        }

        public ReturnFormat Login(UserEntity entity, out String refreshToken)
        {
            //
            // -- variable
            ReturnFormat rtn = new ReturnFormat();
            List<UserEntity> userEntities = new List<UserEntity>();
            List<UserEntity> userCheck = new List<UserEntity>();
            UserAtEntity userAtEntity = new UserAtEntity();
            VUserWithProfileEntity vUserWithProfileEntity = new VUserWithProfileEntity();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();
            PrivilegeUserEntity privilegeUserEntity = new PrivilegeUserEntity();

            //
            // -- set variable
            rtn.Status = StatusCodes.Status204NoContent;
            refreshToken = EncryptMD5.Get();

            //
            // TODO : validation


            //
            // TODO : check IsActive
            userCheck = _repoPs.UserRepo.Read(w => w.Username.Equals(entity.Username) && (w.IsActive == false || w.IsActive == null)).ToList();
            if (userCheck.Count == 0) { return rtn; }
            
            //
            // TOOD : enkirp password
            userEntities = _repoPs.UserRepo.Read(w => w.Username.Equals(entity.Username) && w.Password.Equals(entity.Password))
            .ToList();

            if (userEntities.Count == 0) { return rtn; }

            // get user profile
            // not found then return
            vUserWithProfileEntity = _repoPs.UserRepo.ReadVUserWithProfile(w => w.UserId.Equals(userEntities[0].UserId)).FirstOrDefault();
            if (vUserWithProfileEntity == null) { return rtn; }

            // get user at
            // not found then return
            userAtEntity = _repoPs.UserAtRepo.Read(w => w.UserId.Equals(vUserWithProfileEntity.UserId)).FirstOrDefault();
            if (userAtEntity == null) { return rtn; }

            // update token refresh to userat
            userAtEntity.RefreshToken = refreshToken;
            userAtEntity.UpdatedAt = DateTime.UtcNow;
            _repoPs.UserAtRepo.Update(userAtEntity);
            _repoPs.UserAtRepo.Save();

            vUserWithProfileEntity.RefreshToken = refreshToken;

            // return to UI
            uiUserProfileEntity = this.SetUiUserProfileEntity(vUserWithProfileEntity);




            // -- return
            rtn.Data = uiUserProfileEntity;
            rtn.Status = StatusCodes.Status200OK;
            return rtn;
        }

        public ReturnFormat LoginApproveCust(UserEntity entity, out String refreshToken)
        {
            //
            // -- variable
            ReturnFormat rtn = new ReturnFormat();
            List<UserEntity> userEntities = new List<UserEntity>();
            UserAtEntity userAtEntity = new UserAtEntity();
            VUserWithProfileEntity vUserWithProfileEntity = new VUserWithProfileEntity();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();
            PrivilegeUserEntity privilegeUserEntity = new PrivilegeUserEntity();

            //
            // -- set variable
            rtn.Status = StatusCodes.Status204NoContent;
            refreshToken = EncryptMD5.Get();

            //
            // TODO : validation

            //
            // TOOD : enkirp password
            userEntities = _repoPs.UserRepo.Read(w => w.Username.Equals(entity.Username) && w.Password.Equals(entity.Password))
            .ToList();



            if (userEntities.Count == 0) { return rtn; }

            // get user profile
            // not found then return
            vUserWithProfileEntity = _repoPs.UserRepo.ReadVUserWithProfile(w => w.UserId.Equals(userEntities[0].UserId)).FirstOrDefault();
            if (vUserWithProfileEntity == null) { return rtn; }

            // get user at
            // not found then return
            userAtEntity = _repoPs.UserAtRepo.Read(w => w.UserId.Equals(vUserWithProfileEntity.UserId)).FirstOrDefault();
            if (userAtEntity == null) { return rtn; }

            // update token refresh to userat
            userAtEntity.RefreshToken = refreshToken;
            userAtEntity.UpdatedAt = DateTime.UtcNow;
            _repoPs.UserAtRepo.Update(userAtEntity);
            _repoPs.UserAtRepo.Save();

            vUserWithProfileEntity.RefreshToken = refreshToken;

            // return to UI
            uiUserProfileEntity = this.SetUiUserProfileEntity(vUserWithProfileEntity);




            // -- return
            rtn.Data = uiUserProfileEntity;
            rtn.Status = StatusCodes.Status200OK;
            return rtn;
        }


        public ReturnFormat RefreshToken(String refreshToken, out String refreshTokenOut)
        {
            //
            // -- variable
            ReturnFormat rtn = new ReturnFormat();
            VUserWithProfileEntity vUserWithProfileEntity = new VUserWithProfileEntity();
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();
            UserAtEntity userAtEntity = new UserAtEntity();

            //
            //-- set variable
            rtn.Status = StatusCodes.Status204NoContent;
            refreshTokenOut = "";



            // get user by refresh token
            // not found then return
            vUserWithProfileEntity = _repoPs.UserRepo.ReadVUserWithProfile(w => w.RefreshToken.Equals(refreshToken)).FirstOrDefault();
            if (vUserWithProfileEntity == null) { return rtn; }

            // get user at
            // not found then return
            userAtEntity = _repoPs.UserAtRepo.Read(w => w.UserId.Equals(vUserWithProfileEntity.UserId)).FirstOrDefault();
            if (userAtEntity == null) { return rtn; }

            // update token refresh to userat
            refreshTokenOut = EncryptMD5.Get();
            userAtEntity.RefreshToken = refreshTokenOut;
            userAtEntity.UpdatedAt = DateTime.UtcNow;
            _repoPs.UserAtRepo.Update(userAtEntity);
            _repoPs.UserAtRepo.Save();

            // return to UI
            uiUserProfileEntity = this.SetUiUserProfileEntity(vUserWithProfileEntity);
            //uiUserProfileEntity.userId = vUserWithProfileEntity.UserId;
            //uiUserProfileEntity.username = vUserWithProfileEntity.Username;
            //uiUserProfileEntity.email = vUserWithProfileEntity.Email;

            //uiUserProfileEntity.companyId = vUserWithProfileEntity.CompanyId;
            //uiUserProfileEntity.BranchId = vUserWithProfileEntity.BranchId;
            //uiUserProfileEntity.SubBranchId = vUserWithProfileEntity.SubBranchId;
            //uiUserProfileEntity.companyName = vUserWithProfileEntity.CompanyName;
            //uiUserProfileEntity.companyAddress = vUserWithProfileEntity.CompanyAddress;

            //uiUserProfileEntity.workType = vUserWithProfileEntity.WorkType;
            //uiUserProfileEntity.workId = (vUserWithProfileEntity.SubBranchId != null) ? vUserWithProfileEntity.SubBranchId :
            //           ((vUserWithProfileEntity.BranchId != null) ? vUserWithProfileEntity.BranchId : vUserWithProfileEntity.CompanyId);
            //uiUserProfileEntity.WorkName = (vUserWithProfileEntity.SubBranchName != null) ? vUserWithProfileEntity.SubBranchName :
            //           ((vUserWithProfileEntity.BranchName != null) ? vUserWithProfileEntity.BranchName : vUserWithProfileEntity.CompanyName);
            //uiUserProfileEntity.WorkAddress = (vUserWithProfileEntity.SubBranchAddress != null) ? vUserWithProfileEntity.SubBranchAddress :
            //           ((vUserWithProfileEntity.BranchAddress != null) ? vUserWithProfileEntity.BranchAddress : vUserWithProfileEntity.CompanyAddress);
            //uiUserProfileEntity.workTimeZone = (vUserWithProfileEntity.SubBranchTimeZone != null) ? vUserWithProfileEntity.SubBranchTimeZone :
            //          ((vUserWithProfileEntity.BranchTimeZone != null) ? vUserWithProfileEntity.BranchTimeZone : vUserWithProfileEntity.CompanyTimeZone);

            //uiUserProfileEntity.WorkTimeZoneHour = Int32.Parse(uiUserProfileEntity.workTimeZone.Split(':')[0]);
            //uiUserProfileEntity.WorkTimeZoneMinute = Int32.Parse(uiUserProfileEntity.workTimeZone.Split(':')[1]);

            //uiUserProfileEntity.workCityId = (vUserWithProfileEntity.SubBranchCityIdCode != null) ? vUserWithProfileEntity.SubBranchCityIdCode :
            //           ((vUserWithProfileEntity.BranchCityIdCode != null) ? vUserWithProfileEntity.BranchCityIdCode : vUserWithProfileEntity.CompanyCityId);

            //uiUserProfileEntity.WorkAreaId = (vUserWithProfileEntity.SubBranchAreaIdCode != null) ? vUserWithProfileEntity.SubBranchAreaIdCode :
            //           ((vUserWithProfileEntity.BranchAreaIdCode != null) ? vUserWithProfileEntity.BranchAreaIdCode : vUserWithProfileEntity.CompanyAreaId);



            //uiUserProfileEntity.workCityName = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(uiUserProfileEntity.workCityId)).First().CompanyCityNameCustom;

            //uiUserProfileEntity.WorkAreaName = _repoPs.VCompanyAreaRepo.Read(w => w.CompanyAreaId.Equals(uiUserProfileEntity.WorkAreaId)).First().CompanyAreaNameCustom;



            //
            // -- return
            rtn.Data = uiUserProfileEntity;
            rtn.Status = StatusCodes.Status200OK;
            return rtn;

        }

        public UserEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public UserEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(UserEntity entity)
        {
            throw new NotImplementedException();
        }

        private UiUserProfileEntity SetUiUserProfileEntity(VUserWithProfileEntity vUserWithProfileEntity)
        {

            //
            // variable
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            //
            // set variable

            uiUserProfileEntity.userId = vUserWithProfileEntity.UserId;
            uiUserProfileEntity.username = vUserWithProfileEntity.Username;
            uiUserProfileEntity.email = vUserWithProfileEntity.Email;

            uiUserProfileEntity.companyId = vUserWithProfileEntity.CompanyId;
            uiUserProfileEntity.BranchId = vUserWithProfileEntity.BranchId;
            uiUserProfileEntity.SubBranchId = vUserWithProfileEntity.SubBranchId;
            uiUserProfileEntity.companyName = vUserWithProfileEntity.CompanyName;
            uiUserProfileEntity.companyAddress = vUserWithProfileEntity.CompanyAddress;

            uiUserProfileEntity.workType = vUserWithProfileEntity.WorkType;
            uiUserProfileEntity.workId = (vUserWithProfileEntity.SubBranchId != null) ? vUserWithProfileEntity.SubBranchId :
                       ((vUserWithProfileEntity.BranchId != null) ? vUserWithProfileEntity.BranchId : vUserWithProfileEntity.CompanyId);
            uiUserProfileEntity.WorkName = (vUserWithProfileEntity.SubBranchName != null) ? vUserWithProfileEntity.SubBranchName :
                       ((vUserWithProfileEntity.BranchName != null) ? vUserWithProfileEntity.BranchName : vUserWithProfileEntity.CompanyName);
            uiUserProfileEntity.WorkAddress = (vUserWithProfileEntity.SubBranchAddress != null) ? vUserWithProfileEntity.SubBranchAddress :
                       ((vUserWithProfileEntity.BranchAddress != null) ? vUserWithProfileEntity.BranchAddress : vUserWithProfileEntity.CompanyAddress);
            uiUserProfileEntity.workTimeZone = (vUserWithProfileEntity.SubBranchTimeZone != null) ? vUserWithProfileEntity.SubBranchTimeZone :
                      ((vUserWithProfileEntity.BranchTimeZone != null) ? vUserWithProfileEntity.BranchTimeZone : vUserWithProfileEntity.CompanyTimeZone);

            uiUserProfileEntity.WorkTimeZoneHour = Int32.Parse(uiUserProfileEntity.workTimeZone.Split(':')[0]);
            uiUserProfileEntity.WorkTimeZoneMinute = Int32.Parse(uiUserProfileEntity.workTimeZone.Split(':')[1]);

            uiUserProfileEntity.workCityId = (vUserWithProfileEntity.SubBranchCityIdCode != null) ? vUserWithProfileEntity.SubBranchCityIdCode :
                       ((vUserWithProfileEntity.BranchCityIdCode != null) ? vUserWithProfileEntity.BranchCityIdCode : vUserWithProfileEntity.CompanyCityId);

            uiUserProfileEntity.WorkAreaId = (vUserWithProfileEntity.SubBranchAreaIdCode != null) ? vUserWithProfileEntity.SubBranchAreaIdCode :
                       ((vUserWithProfileEntity.BranchAreaIdCode != null) ? vUserWithProfileEntity.BranchAreaIdCode : vUserWithProfileEntity.CompanyAreaId);



            uiUserProfileEntity.workCityName = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(uiUserProfileEntity.workCityId)).First().CompanyCityNameCustom;

            uiUserProfileEntity.WorkAreaName = _repoPs.VCompanyAreaRepo.Read(w => w.CompanyAreaId.Equals(uiUserProfileEntity.WorkAreaId)).First().CompanyAreaNameCustom;

            var pid = _repoPs.PrivillegeRepo.Read(w => w.UserId.Equals(vUserWithProfileEntity.UserId)).First().PrivilegeId;

            //var pid_list = ""; 
            //foreach (var item in pr)
            //{
            //    pid_list = pid_list + item.PrivilegeId.ToString() + ",";
            //}

            if (pid != null)
            {
                uiUserProfileEntity.privilegeId = pid;//pid_list.Remove(pid_list.Length - 1);

                var menu_action_id_list = this.GetDataMenuActionId(uiUserProfileEntity.privilegeId);
                var menu_action_list = this.GetDataMenuAction(menu_action_id_list);
                var menu_data_id_list = this.GetDataMenuId(menu_action_id_list);
                var menu_data_list = this.GetDataMenu(menu_data_id_list);
                var menu_actionid_list = this.GetDataMenuActionIdList(uiUserProfileEntity.privilegeId);

                uiUserProfileEntity.MenuKeyLogin = menu_data_list.Split(",");
                uiUserProfileEntity.MenuActionLogin = menu_action_list.Split(",");
                uiUserProfileEntity.MenuActionIdLogin = menu_actionid_list.Split(",");
            }

            //
            // return
            return uiUserProfileEntity;
        }

        public string GetDataMenuActionIdList(Int32? pid_list)
        {
            var a = _repoPs.PrivilegeActionRepo.Read(x => x.PrivilegeId.Equals(pid_list)).ToList();

            var ma_list = "";
            foreach (var item in a)
            {
                var b = _repoPs.MenuActionRepo.Read(x => item.MenuActionId.ToString().Contains(x.MenuActionId.ToString())).ToList();
                foreach (var dx in b)
                {
                    ma_list = ma_list + dx.MenuId + "~" + item.MenuActionId.ToString() + ",";
                }
            }

            return ma_list.Remove(ma_list.Length - 1);
        }

        public string GetDataMenuId(string menu_action_id_list)
        {
            var maid = menu_action_id_list.Split(",");
            var a = _repoPs.MenuActionRepo.Read(x => (maid != null && maid.Contains(x.MenuActionId.ToString()))).ToList();

            var ma_list = "";
            foreach (var item in a)
            {
                ma_list = ma_list + item.MenuId.ToString() + ",";
            }

            return ma_list.Remove(ma_list.Length - 1);
        }

        public string GetDataMenuActionId(Int32? pid_list)
        {
            //(pid_list != null && pid_list.Contains(x.PrivilegeId.ToString()))
            var a = _repoPs.PrivilegeActionRepo.Read(x => x.PrivilegeId.Equals(pid_list)).ToList();

            var ma_list = "";
            foreach (var item in a)
            {
                ma_list = ma_list + item.MenuActionId.ToString() + ",";
            }

            return ma_list.Remove(ma_list.Length - 1);
        }

        public string GetDataMenu(string menu_action_id_list)
        {
            var mcid = menu_action_id_list.Split(",");
            var c = _repoPs.MenuDataRepo.Read(x => (mcid != null && mcid.Contains(x.MenuId.ToString()))).ToList();

            var ma_list = "";
            foreach (var item in c)
            {
                ma_list = ma_list + item.MenuId + "~" + item.MenuKey.ToString() + ",";
            }
            return ma_list.Remove(ma_list.Length - 1);
        }

        public string GetDataMenuAction(string menu_action_id_list)
        {
            var miid = menu_action_id_list.Split(",");
            var b = _repoPs.MenuActionRepo.Read(x => (miid != null && miid.Contains(x.MenuActionId.ToString()))).ToList();

            var mc_list = "";
            foreach (var item in b)
            {
                mc_list = mc_list + item.MenuId + "~" + item.MenuAction.ToString() + ",";
            }

            return mc_list.Remove(mc_list.Length - 1);
        }
    }
}
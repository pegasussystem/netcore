﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MasterCityEntity
    {
        public String MasterCityId { get; set; }
        public String MasterProvinceId { get; set; }
        public String MasterCityCode { get; set; }
        public String MasterCityType { get; set; }
        public String MasterCityName { get; set; }
        public String MasterCityCapital { get; set; }


    }
}

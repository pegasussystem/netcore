﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class SpbGoodsConfig : IEntityTypeConfiguration<SpbGoodsEntity>
    {
        public void Configure(EntityTypeBuilder<SpbGoodsEntity> builder)
        {
            builder.HasKey(e => e.SpbGoodsId);
            builder.ToTable("SpbGoods");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

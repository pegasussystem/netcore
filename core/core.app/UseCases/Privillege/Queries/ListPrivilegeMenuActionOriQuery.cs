﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Queries
{
     public partial class ListPrivilegeMenuActionOriQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public Int32? menuId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListPrivilegeMenuActionOriQueryHandler : IRequestHandler<ListPrivilegeMenuActionOriQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPrivilegeMenuActionOriQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPrivilegeMenuActionOriQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.MenuActionEntitys
                        where a.MenuId.Equals(req.menuId)
                        select new
                        {
                            a.MenuActionId
                            , a.MenuAction
                            , a.MenuActionName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.MenuActionId
                        , s.MenuAction
                        , s.MenuActionName
                    }).OrderBy(o => o.MenuActionId);
            }

            // return
            return rtn;
        }
    }
}
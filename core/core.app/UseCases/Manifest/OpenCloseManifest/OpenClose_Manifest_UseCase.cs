﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Main.Manifest;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace core.app.UseCases.Manifest.OpenCloseManifest
{
    class OpenClose_Manifest_UseCase : I_OpenClose_Manifest_UseCase
    {
        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public OpenClose_Manifest_UseCase(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Update(UiUserProfileEntity uiUserProfileEntity, Object req)
        {
            // variable
            // ///// ///// /////
            OpenClose_Manifest_Input input = req as OpenClose_Manifest_Input;

            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status400BadRequest
            };

            // logic
            // ///// ///// /////

            // get  
            ManifestEntity entity = _repoPs.ManifestRepo.Read(w => w.ManifestId.Equals(input.ManifestId)).FirstOrDefault();

            // update
            if (entity != null)
            {
                entity.IsOpen = input.GetAction();
                _repoPs.ManifestRepo.Update(entity);
                _repoPs.ManifestRepo.Save();
            }



            // return
            // ///// ///// /////
            if (entity != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;
            }
            return rtn;
        }

        public ReturnFormat Create(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Delete(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public ManifestEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(ManifestEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Carrier.Command
{
    public partial class ManifestCarrierMasterCarrierDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? carrierId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterCarrierDeleteCommandHandler : IRequestHandler<ManifestCarrierMasterCarrierDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterCarrierDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterCarrierDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CarrierDelete(req.carrierId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_CarrierDeleteEntity> CarrierDelete(Int32? carrierId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_CarrierDelete @carrierId='{carrierId}'";
            var res = this._context.SP_ManifestCarrierMaster_CarrierDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
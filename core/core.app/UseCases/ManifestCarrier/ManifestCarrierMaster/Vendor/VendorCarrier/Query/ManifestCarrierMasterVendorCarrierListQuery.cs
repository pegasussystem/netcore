﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorCarrier.Query
{
    public partial class ManifestCarrierMasterVendorCarrierListQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterVendorCarrierListQueryHandler : IRequestHandler<ManifestCarrierMasterVendorCarrierListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ManifestCarrierMasterVendorCarrierListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterVendorCarrierListQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var query = from a in _context.VendorCarrierEntitys
                            join b in _context.CarrierEntitys on a.CarrierId equals b.CarrierId
                            join c in _context.VendorEntitys on a.VendorId equals c.VendorId
                            select new
                            {
                                a.VendorCarrierId
                                , a.VendorId
                                , c.VendorName
                                , c.Description
                                , b.CarrierId
                                , CarrierName = "[ "+b.CarrierCode + " ] - " +b.CarrierName
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.VendorCarrierId
                            , s.VendorId
                            , s.VendorName
                            , s.Description
                            , s.CarrierId
                            , s.CarrierName
                        }).OrderByDescending(o => o.VendorCarrierId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
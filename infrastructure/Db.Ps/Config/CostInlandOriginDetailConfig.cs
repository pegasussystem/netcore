﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostInlandOriginDetailConfig : IEntityTypeConfiguration<CostInlandOriginDetailEntity>
    {
        public void Configure(EntityTypeBuilder<CostInlandOriginDetailEntity> builder)
        {
            builder.HasKey(e => e.CostInlandOriginDetailId);
            builder.ToTable("CostInlandOriginDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
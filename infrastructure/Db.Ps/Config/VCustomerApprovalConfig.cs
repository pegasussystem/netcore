﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config 
{
    class VCustomerApprovalConfig : IEntityTypeConfiguration<VCustomerApprovalEntity>
    {
        public void Configure(EntityTypeBuilder<VCustomerApprovalEntity> builder)
        {
            builder.HasKey(e => e.UserName);
            builder.ToTable("VCustomerApproval");
        }
    }
}

﻿using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Station.StationCity.Query;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;


namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierMaster.Station.StationCity
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]
    public class ListController : ApiController
    {
        [HttpGet("manifest-carrier-master-station-city-list")]
        public async Task<IActionResult> ListCarrier([FromQuery] ManifestCarrierMasterStationCityListQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("manifest-carrier-master-station-city-list-station")]
        public async Task<IActionResult> ListStation([FromQuery] ManifestCarrierMasterStationCityListStationQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("manifest-carrier-master-station-city-list-city")]
        public async Task<IActionResult> ListCity([FromQuery] ManifestCarrierMasterStationCityListCityQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("manifest-carrier-master-station-city-list-status")]
        public async Task<IActionResult> ListStatus([FromQuery] ManifestCarrierMasterStationCityListStatusQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}
﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.App.Base;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace core.app.Main
{
    public class MasterCityApp : IMasterCityApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public MasterCityApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(MasterCityEntity entity)
        {
            throw new NotImplementedException();
        }

        public MasterCityEntity Delete(MasterCityEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }
        
        public ReturnFormat Read(MasterCityEntity entity)
        {
            throw new NotImplementedException();
        }


        public ReturnFormat ReadByProvince(MasterCityEntity entity, UiUserProfileEntity uiUserProfileEntity = null)
        {
            //
            // variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            ReturnFormat rtn = new ReturnFormat();
            List<MasterCityEntity> masterCityEntities = null;
            MasterCityEntity masterCityEntity;
            VCompanyCityEntity vCompanyCityEntity = null;

            //
            // set variable
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            // 
            // logic
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            // if uiUserProfileEntity not null AND entity is null  then get proviceId from companyCity
            if (uiUserProfileEntity != null && entity == null) {
                vCompanyCityEntity = _repoPs.VCompanyCityRepo
                    .Read( w => w.CompanyCityId.Equals(uiUserProfileEntity.workCityId)).FirstOrDefault();
            }

            // //  read city by province
            if (vCompanyCityEntity != null) {
                // firsst get provice id, then get data city by provice
                masterCityEntity = _repoPs.MasterCityRepo.Read(w => w.MasterCityId.Equals(vCompanyCityEntity.CityId)).FirstOrDefault();
                masterCityEntities = _repoPs.MasterCityRepo.Read(w => w.MasterProvinceId.Equals(masterCityEntity.MasterProvinceId)).ToList();
            }

            // if uiUserProfileEntity is null AND entity !+ ull
            if (uiUserProfileEntity == null && entity != null) {
                masterCityEntities = _repoPs.MasterCityRepo.Read(w => w.MasterProvinceId.Equals(entity.MasterProvinceId)).ToList();
            }

            // change status if not null
            if (masterCityEntities != null) {  rtn.Status = StatusCodes.Status200OK; }

            //
            // return
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            rtn.Data = masterCityEntities;
            return rtn;
        }

        public MasterCityEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public MasterCityEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(MasterCityEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Configuration.Users.Query
{
    public partial class GetaBranchIdQuery : IRequest<ReturnFormat>
    {
        public Int32? subBranchId { get; set; }
    }

    public class GetaBranchIdQueryHandler : IRequestHandler<GetaBranchIdQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetaBranchIdQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetaBranchIdQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.WorkUnitEntitys
                        where a.WorkUnitId.Equals(req.subBranchId)
                        select new
                        {
                            a.WorkUnitId
                            , a.WorkUnitCode
                            , a.WorkUnitName
                            , a.IsActive
                            , a.Parent
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        id = ( s.Parent == 0 ? s.WorkUnitId : s.Parent )
                    });
            }

            // return
            return rtn;
        }

    }
}

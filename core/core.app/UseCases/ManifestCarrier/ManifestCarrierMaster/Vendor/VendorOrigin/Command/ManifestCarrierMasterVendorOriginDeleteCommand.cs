﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;


namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorOrigin.Command
{
    public partial class ManifestCarrierMasterVendorCarrierDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? VendorOriginId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterVendorOriginDeleteCommandHandler : IRequestHandler<ManifestCarrierMasterVendorCarrierDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterVendorOriginDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterVendorCarrierDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.VendorOriginDelete(req.VendorOriginId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_VendorOriginDeleteEntity> VendorOriginDelete(Int32? VendorOriginId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_VendorOriginDelete @VendorOriginId='{VendorOriginId}'";
            var res = this._context.SP_ManifestCarrierMaster_VendorOriginDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

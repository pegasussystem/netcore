﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Privilege
{
    public partial class MenuActionListQuery : IRequest<ReturnFormat>
    {
        public Int32? privilegeId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class MenuActionListQueryHandler : IRequestHandler<MenuActionListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MenuActionListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MenuActionListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_PrivilegeMenuActionListEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.PrivilegeMenuActionListData(req.privilegeId);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                        id = s.MenuActionId
                        , key = s.MenuActionKey
                        , name = s.MenuActionName
                        , value = (s.IsActive > 0 ? true : false)
                    });
            }

            // return
            return rtn;
        }

        public IEnumerable<SP_PrivilegeMenuActionListEntity> PrivilegeMenuActionListData(Int32? privilegeId)
        {
            String query = $@"EXEC SP_PrivilegeMenuActionList @privilegeId={privilegeId}";
            var res = this._context.SP_PrivilegeMenuActionListEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class SubBranchConfig : IEntityTypeConfiguration<SubBranchEntity>
    {
        public void Configure(EntityTypeBuilder<SubBranchEntity> builder)
        {
            builder.HasKey(e => e.SubBranchId);
            builder.ToTable("SubBranch");


            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostDestination.Query
{
    public partial class ReadCityDestinationQuery : IRequest<ReturnFormat>
    {
        public Int32? stationDestinationId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ReadCityDestinationQueryHandler : IRequestHandler<ReadCityDestinationQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ReadCityDestinationQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadCityDestinationQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                rtn.Status = StatusCodes.Status200OK;
                var query = from a in _context.StationEntitys
                            join b in _context.StationCityEntitys on a.StationId equals b.StationId
                            join c in _context.MasterCityEntitys on b.CityId equals c.MasterCityId
                            where a.StationId.Equals(req.stationDestinationId)
                            select new
                            {
                                c.MasterCityId
                               ,
                                c.MasterCityCode
                           ,
                                c.MasterCityType
                           ,
                                c.MasterCityName
                           ,
                                c.MasterCityCapital
                           ,
                                MasterCityCustom = c.MasterCityCode + " - " + c.MasterCityType + " " + c.MasterCityName + " " + (c.MasterCityCapital != null ? " [" + c.MasterCityCapital + "] " : "")
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.MasterCityId
                            ,
                            s.MasterCityCode
                            ,
                            s.MasterCityName
                            ,
                            s.MasterCityCapital
                            ,
                            s.MasterCityCustom
                        }).OrderBy(o => o.MasterCityId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
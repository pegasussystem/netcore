﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Entity.Main
{
    public class LoginApprovalEntity
    {
        public String? UserName { get; set; }
        public String? Password { get; set; }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Master.MonitoringShipment.Query
{

    public partial class ListMonitoringShipmentQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListMonitoringShipmentQueryHandler : IRequestHandler<ListMonitoringShipmentQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListMonitoringShipmentQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListMonitoringShipmentQuery req, CancellationToken cancellationToken)
        {
// --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_MonitoringShipmentListEntity> entities;

            //DateTime utcDate = req.CreatedAt.Value.AddHours((double)(-1 * req.uiUserProfile.WorkTimeZoneHour)).AddMinutes((double)(req.uiUserProfile.WorkTimeZoneMinute * -1));

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.MonitoringShipmentList();

            if (entities != null)
            {
               rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        //, CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("dddd, dd MMMM yyyy")
                        s.MonitoringShipmentId
                        , s.Via
                        , s.OriginCityName
                        , s.CorporateName
                        , s.DestinationCityName
                        , s.SttPegasus
                        , s.SttLP
                        , s.SmuNo
                        , s.Koli
                        , s.Caw
                        , s.CarrierCode
                        , s.FlightNo
                        , s.FlightDate
                        , s.ETD
                        , s.ETA
                        , FlightDateLocal = s.FlightDate.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("dddd, dd MMMM yyyy")
                        , ETDLocal = s.ETD.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("HH:mm")
                        , ETALocal = s.ETA.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("HH:mm")
                        , s.FirstDayAfternoon
                        , s.FirstDayMorning
                        , s.SecondDayAfternoon
                        , s.SecondDayMorning
                        , s.ThirdDayAfternoon
                        , s.ThirdDayMorning
                        , s.FourthDayAfternoon
                        , s.FourthDayMorning
                        , s.FifthDayAfternoon
                        , s.FifthDayMorning
                        , s.POD
                        , s.PODDate
                        , PODDateLocal =  ( s.PODDate == null ) ? null : s.PODDate.Value.AddHours((Double)req.uiUserProfile.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfile.WorkTimeZoneMinute).ToString("dddd, dd MMMM yyyy")
                        , s.Desc
                    }).OrderByDescending(s => s.MonitoringShipmentId);
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_MonitoringShipmentListEntity> MonitoringShipmentList()
        {
            String query = $@"EXEC SP_MonitoringShipmentList ";
            var res = this._context.SP_MonitoringShipmentListEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Master.CoaCode.Commands{
    public partial class CoaCodeUpdateCommands : IRequest<ReturnFormat>
        {
       public Int64? coaId { get; set; }
       public String? coaCode { get; set; }
        public Int32? companyId { get; set; }
        public Int32?  branchId { get; set; }
        public String? coaName { get; set; }
        public Int32? coaParentId { get; set; }
        
        public UiUserProfileEntity uiUserProfile { get; set; }
        }

                public class CoaCodeUpdateCommandsHandler : IRequestHandler<CoaCodeUpdateCommands, ReturnFormat>
        {
            private readonly IApplicationDbContext _context;

            public CoaCodeUpdateCommandsHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<ReturnFormat> Handle(CoaCodeUpdateCommands req, CancellationToken cancellationToken)
            {
                //
                // variable
                ReturnFormat rtn = new ReturnFormat();
                //
                // set variable

                // default status 
                rtn.Status = StatusCodes.Status204NoContent;

                try
                {
                    // Mark as Changed
                    var res = this.CoaCodeUpdate(req.coaId,req.companyId,req.coaCode,req.branchId,req.coaName,req.coaParentId);

                    await _context.SaveChangesAsync(cancellationToken);

                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = res;

                    //
                    // return
                    return rtn;

                }
                catch (Exception ex)
                {
                    var err = ex.Message;
                    rtn.Status = StatusCodes.Status202Accepted;
                    rtn.Data = err;
                }
                return rtn;
            }

            public IEnumerable<SP_CoaCodeUpdateEntity> CoaCodeUpdate(Int64? coaId,Int32? companyId, String? coaCode, Int32? branchId, String? coaName,Int32? coaParentId )
            {
                String query = $@"EXEC SP_CoaCodeUpdate @coaId = {coaId}, @companyId={companyId}, @coaCode ='{coaCode}',@branchId={branchId}, @coaName='{coaName}', @coaParentId={coaParentId}";
            var res = this._context.SP_CoaCodeUpdateEntities.FromSqlRaw(query).ToList();
                return res;
            }
        }
}
﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class MenuActionConfig : IEntityTypeConfiguration<MenuActionEntity>
    {
        public void Configure(EntityTypeBuilder<MenuActionEntity> builder)
        {
            builder.HasKey(e => e.MenuActionId);
            builder.ToTable("MenuAction");
        }
    }
}
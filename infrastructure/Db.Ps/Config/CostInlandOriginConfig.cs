﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
   class CostInlandOriginConfig : IEntityTypeConfiguration<CostInlandOriginEntity>
    {
        public void Configure(EntityTypeBuilder<CostInlandOriginEntity> builder)
        {
            builder.HasKey(e => e.CostInlandOriginId);
            builder.ToTable("CostInlandOrigin");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

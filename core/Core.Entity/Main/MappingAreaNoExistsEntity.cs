﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class MappingAreaNoExistsEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }

        public Int32? MappingAreaNoExistsId { get; set; }
        public String? AreaId { get; set; }
        public String? CityId { get; set; }
        public String? SubDistrictId { get; set; }
    }
}

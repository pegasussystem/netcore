﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface IManifestCarrierItemRepo : IRepo<ManifestCarrierItemEntity>
    {
        void DeleteRange(List<ManifestCarrierItemEntity> entitys);
        IEnumerable<VManifestCarrierItemEntity> VManifestCarrierItem(int ManifestCarrierId);
    }
}

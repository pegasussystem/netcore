﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Other
{
    public interface ILog
    {
        void LogInfo(string idLog, string message);
        //void LogInfo(string idLog, Exception ex, string message);

        void LogWarn(string idLog, string message);
        //void LogWarn(string idLog, Exception ex, string message);

        void LogDebug(string idLog, string message);
        //void LogDebug(string idLog, Exception ex, string message);

        void LogError(string idLog, string message);
        //void LogError(string idLog, Exception ex, string message);
    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class SpbConfig : IEntityTypeConfiguration<SpbEntity>
    {
        public void Configure(EntityTypeBuilder<SpbEntity> builder)
        {
            builder.HasKey(e => e.SpbId);
            builder.ToTable("Spb");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

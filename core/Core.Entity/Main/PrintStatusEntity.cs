﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class PrintStatusEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? PrintStatusId { get; set; }
        public Int64? DokumenId { get; set; }
        public String? Type { get; set; }
        public Int32? DivisionId { get; set; }
        public Int32? RolesId { get; set; }
        public Int32? Status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.app.UseCases.ManifestCarrier.ReadManifestCarrierItemByManifestCarrier;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ui.api.main.UseCases.ManifestCarrier.ReadManifestCarrierItemByManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ReadManifestCarrierItemByManifestCarrierController : DefaultController
    {
        private readonly IAppWrapperPs _appPs;
        public ReadManifestCarrierItemByManifestCarrierController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpGet("read-item")]
        public async Task<IActionResult> Execute([FromQuery] ReadManifestCarrierItemByManifestCarrierRequest request)
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();

            // logic
            // ///// ///// /////
            _returnFormat = _appPs.ReadManifestCarrierUseCase.Read(GetUserClaim());

            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

﻿using core.app.Common.Interfaces;
using Core.Entity.Base;
using Core.Entity.Main; // Pati 21Agt21
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Db.Ps
{
    public class DbContextPs : DbContext, IApplicationDbContext
    {
        public DbContextPs(DbContextOptions<DbContextPs> options)
            : base(options)
        {
        }

        //
        // BASE
        // public DbSet<MenuEntity> MenuEntities { get; set; }
        public DbSet<LookupEntity> LookupEntitys { get; set; }
        //public DbSet<RouteCostEntity> RouteCostEntities { get; set; }

        //
        // MAIN
        //public DbSet<CustomerEntity> CustomerEntities { get; set; }
        public DbSet<CustomerEntity> CustomerEntitys { get; set; }
        public DbSet<CustomerNameEntity> CustomerNameEntitys { get; set; }
        public DbSet<CustomerStoreEntity> CustomerStoreEntitys { get; set; }
        public DbSet<CustomerPlaceEntity> CustomerPlaceEntitys { get; set; }
        public DbSet<CustomerAddressEntity> CustomerAddressEntitys { get; set; }
        public DbSet<SpbEntity> SpbEntitys { get; set; }

        public DbSet<RatesEntity> RatesEntitys { get; set; }
        public DbSet<RatesDetailEntity> RatesDetailEntitys { get; set; }
        public DbSet<RatesSubDistrictEntity> RatesSubDistrictEntitys { get; set; }
        public DbSet<RatesSubDistrictDetailEntity> RatesSubDistrictDetailEntitys { get; set; }

        public DbSet<ManifestEntity> ManifestEntitys { get; set; }
        public DbSet<ManifestCarrierEntity> ManifestCarrierEntitys { get; set; }
        public DbSet<ManifestCarrierItemEntity> ManifestCarrierItemEntitys { get; set; }
        public DbSet<ManifestCarrierItemDetailEntity> ManifestCarrierItemDetailEntitys { get; set; }
        public DbSet<ManifestCarrierCostEntity> ManifestCarrierCostEntitys { get; set; }
        public DbSet<ManifestCarrierCostSMUEntity> ManifestCarrierCostSMUEntitys { get; set; }

        public DbSet<AlphabetEntity> AlphabetEntitys { get; set; }

        public DbSet<StationEntity> StationEntitys { get; set; }
        public DbSet<CarrierEntity> CarrierEntitys { get; set; }
        public DbSet<MasterCityEntity> MasterCityEntitys { get; set; }
        public DbSet<CompanyCityEntity> CompanyCityEntitys { get; set; }
        public DbSet<CompanyEntity> CompanyEntitys { get; set; }
        public DbSet<MasterSubDistrictEntity> MasterSubDistrictEntitys { get; set; }
        public DbSet<VendorEntity> VendorEntitys { get; set; }
        public DbSet<VendorOriginEntity> VendorOriginEntitys { get; set; }
        public DbSet<VendorDestinationEntity> VendorDestinationEntitys { get; set; }
        public DbSet<VendorTransitEntity> VendorTransitEntitys { get; set; }
        public DbSet<ModaEntity> ModaEntitys { get; set; }
        public DbSet<ManifestKoliEntity> ManifestKoliEntitys { get; set; }
        public DbSet<ManifestKoliSpbEntity> ManifestKoliSpbEntitys { get; set; }
        public DbSet<SpbGoodsEntity> SpbGoodsEntitys { get; set; }

        public DbSet<CostCarrierEntity> CostCarrierEntitys { get; set; }
        public DbSet<CostCarrierDetailEntity> CostCarrierDetailEntitys { get; set; }
        public DbSet<CostCarrierDetailPpnEntity> CostCarrierDetailPpnEntitys { get; set; }
        public DbSet<CostCarrierDetailSmuEntity> CostCarrierDetailSmuEntitys { get; set; }

        public DbSet<CostOriginEntity> CostOriginEntitys { get; set; }
        public DbSet<CostOriginDetailEntity> CostOriginDetailEntitys { get; set; }

        public DbSet<CostDestinationEntity> CostDestinationEntitys { get; set; }
        public DbSet<CostDestinationDetailEntity> CostDestinationDetailEntitys { get; set; }

        public DbSet<CostTransitEntity> CostTransitEntitys { get; set; }
        public DbSet<CostTransitDetailEntity> CostTransitDetailEntitys { get; set; }

        public DbSet<CostGeraiEntity> CostGeraiEntitys { get; set; }
        public DbSet<CostGeraiDetailEntity> CostGeraiDetailEntitys { get; set; }

        public DbSet<CostPenerusEntity> CostPenerusEntitys { get; set; }
        public DbSet<CostPenerusDetailEntity> CostPenerusDetailEntitys { get; set; }

        public DbSet<CostInlandOriginEntity> CostInlandOriginEntitys { get; set; }
        public DbSet<CostInlandOriginDetailEntity> CostInlandOriginDetailEntitys { get; set; }

        public DbSet<VendorCarrierEntity> VendorCarrierEntitys { get; set; }
        public DbSet<StationCityEntity> StationCityEntitys { get; set; }
        public DbSet<MatrixFirstCarrierEntity> MatrixFirstCarrierEntitys { get; set; }
        public DbSet<MatrixLastCarrierEntity> MatrixLastCarrierEntitys { get; set; }
        public DbSet<BranchCityEntity> BranchCityEntitys { get; set; }
        public DbSet<RolesEntity> RolesEntitys { get; set; }
        public DbSet<PrivilegeUserEntity> PrivilegeUserEntitys { get; set; }
        public DbSet<PrivilegeEntity> PrivilegeEntitys { get; set; }
        public DbSet<PrivilegeActionEntity> PrivilegeActionEntitys { get; set; }
        public DbSet<MenuActionEntity> MenuActionEntitys { get; set; }
        public DbSet<MenuDataEntity> MenuDataEntitys { get; set; }
        public DbSet<UserEntity> UserEntitys { get; set; }
        public DbSet<UserDetailEntity> UserDetailEntitys { get; set; }
        public DbSet<UserAtEntity> UserAtEntitys { get; set; }
        public DbSet<BranchEntity> BranchEntitys { get; set; }
        public DbSet<SubBranchEntity> SubBranchEntitys { get; set; }
        public DbSet<HistoryLoginEntity> HistoryLoginEntitys { get; set; }
        public DbSet<HistoryActionEntity> HistoryActionEntitys { get; set; }
        public DbSet<HistoryPrintEntity> HistoryPrintEntitys { get; set; }
        public DbSet<MasterDataCustomerEntity> MasterDataCustomerEntitys { get; set; }
        public DbSet<MDataCustomerReceiverEntity> MDataCustomerReceiverEntitys { get; set; }
        public DbSet<MDataCustomerSenderEntity> MDataCustomerSenderEntitys { get; set; }
        public DbSet<WorkUnitEntity> WorkUnitEntitys { get; set; }
        public DbSet<CompanyAreaEntity> CompanyAreaEntitys { get; set; }
        public DbSet<FlightEntity> FlightEntitys { get; set; }
        public DbSet<FlightRouteEntity> FlightRouteEntitys { get; set; }
        public DbSet<FlightTransitRouteEntity> FlightTransitRouteEntitys { get; set; }
        public DbSet<MasterProvinceEntity> MasterProvinceEntitys { get; set; }

        public DbSet<SuperVisionEntity> SuperVisionEntitys { get; set; }
        public DbSet<PaymentMethodEntity> PaymentMethodEntitys { get; set; }
        public DbSet<ModaCityEntity> ModaCityEntitys { get; set; }
        public DbSet<MappingAreaNoExistsEntity> MappingAreaNoExistsEntitys { get; set; }
        public DbSet<MasterStatistikEntity> MasterStatistikEntitys { get; set; }
        public DbSet<PrintStatusEntity> PrintStatusEntitys { get; set; }
        public DbSet<MonitoringShipmentEntity> MonitoringShipmentEntitys { get; set; }
        public DbSet<PorterEntity> PorterEntitys { get; set; }
        public DbSet<SpbDetailEntity> SpbDetailEntitys { get; set; }
        public DbSet<RoundEntity> RoundEntitys { get; set; }
        public DbSet<TransportEntity> TransportEntitys { get; set; }
        public DbSet<CoaParentEntity> CoaParentEntities { get; set; }
        public DbSet<CoaCodeEntity> CoaCodeEntities { get; set; }
        public DbSet<CityBranchEntity> CityBranchEntities { get; set; }
        public DbSet<CoaAccuredExpensesManagementEntity> CoaAccuredExpensesManagementEntities { get; set; }
        public DbSet<AccountReceivableEntity> AccountReceivableEntities { get; set; }
        public DbSet<RevenueEntity> RevenueEntities { get; set; }
        public DbSet<ProfitLossStatementEntity> ProfitLossStatementEntities { get; set; }
        public DbSet<CoaCityRevenueLiabilityEntity> CoaCityRevenueLiabilityEntities { get; set; }

        //-------- view---------
        //----------------------
        public DbSet<V_CustomerCodeEntity> V_CustomerCodeEntitys { get; set; }
        public DbSet<V_CustomerCodeDataEntity> V_CustomerCodeDataEntitys { get; set; }
        public DbSet<V_StationStatusEntity> V_StationStatusEntitys { get; set; }
        public DbSet<VCompanyCityEntity> VCompanyCityEntities { get; set; }
        public DbSet<VUserNotPrivilegeEntity> VUserNotPrivilegeEntitys { get; set; }
        public DbSet<VSpbEntity> VSpbEntitys { get; set; }
        public DbSet<V_ManifestCarrierCostEntity> V_ManifestCarrierCostEntitys { get; set; }
        public DbSet<V_ManifestCarrierDetailEntity> V_ManifestCarrierDetailEntitys { get; set; }
        public DbSet<V_ManifestCarrierItemDetailEntity> V_ManifestCarrierItemDetailEntitys { get; set; }
        public DbSet<V_UserPrivilegeEntity> V_UserPrivilegeEntitys { get; set; }
        public DbSet<V_ManifestListEntity> V_ManifestListEntitys { get; set; }
        public DbSet<V_ManifestDetailListEntity> V_ManifestDetailListEntitys { get; set; }
        public DbSet<V_ManifestCarrierApprovalEntity> V_ManifestCarrierApprovalEntitys { get; set; }
        public DbSet<V_ManifestKoliListEntity> V_ManifestKoliListEntitys { get; set; }
        public DbSet<V_ManifestCarrierListEntity> V_ManifestCarrierListEntitys { get; set; }
        public DbSet<V_ManifestCarrierReviseListEntity> V_ManifestCarrierReviseListEntitys { get; set; }
        public DbSet<VCompanyAreaEntity> VCompanyAreaEntitys { get; set; }
        public DbSet<V_PrivilegeListEntity> V_PrivilegeListEntitys { get; set; }
        public DbSet<V_PrivilegeMenuListEntity> V_PrivilegeMenuListEntitys { get; set; }
        public DbSet<V_PrivilegeUserListEntity> V_PrivilegeUserListEntitys { get; set; }
        public DbSet<V_DataUserEntity> V_DataUserEntitys { get; set; }
        public DbSet<V_DataOptionSettingEntity> V_DataOptionSettingEntitys { get; set; }
        public DbSet<V_DataOptionCitySubDistrictEntity> V_DataOptionCitySubDistrictEntitys { get; set; }
        public DbSet<V_RoleListEntity> V_RoleListEntitys { get; set; }
        public DbSet<V_UserAgreementEntity> V_UserAgreementEntitys { get; set; }
        public DbSet<V_FlightRouteEntity> V_FlightRouteEntitys { get; set; }
        public DbSet<V_MonitoringCapacityCarrierEntity> V_MonitoringCapacityCarrierEntitys { get; set; }
        public DbSet<V_TypePrintEntity> V_TypePrintEntitys { get; set; }
        public DbSet<V_HistoryNotPrintSpbEntity> V_HistoryNotPrintSpbEntitys { get; set; }
        public DbSet<V_HistoryPrintEntity> V_HistoryPrintEntitys { get; set; }
        public DbSet<V_FragilePerishableCityEntity> V_FragilePerishableCityEntitys { get; set; }
        public DbSet<V_SpbDataFormEntity> V_SpbDataFormEntitys { get; set; }
        public DbSet<V_SpbGoodsDataFormEntity> V_SpbGoodsDataFormEntitys { get; set; }
        public DbSet<VBukuKasEntity> VBukuKasEntities { get; set; }
        public DbSet<V_BukuKasCoaHierarchyCurrentMonthEntity> V_BukuKasCoaHierarchyCurrentMonthEntities { get; set; }

        // --- store procedure
        // --- --- --- --- ---
        public DbSet<SP_ManifestCarrierCost_CostGeraiDeleteEntity> SP_ManifestCarrierCost_CostGeraiDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostGeraiUpdateEntity> SP_ManifestCarrierCost_CostGeraiUpdateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostGeraiCreateEntity> SP_ManifestCarrierCost_CostGeraiCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostPenerusCreateEntity> SP_ManifestCarrierCost_CostPenerusCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostPenerusUpdateEntity> SP_ManifestCarrierCost_CostPenerusUpdateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostPenerusDeleteEntity> SP_ManifestCarrierCost_CostPenerusDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostTransitCreateEntity> SP_ManifestCarrierCost_CostTransitCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostTransitDeleteEntity> SP_ManifestCarrierCost_CostTransitDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostTransitUpdateEntity> SP_ManifestCarrierCost_CostTransitUpdateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostDestinationCreateEntity> SP_ManifestCarrierCost_CostDestinationCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostDestinationDeleteEntity> SP_ManifestCarrierCost_CostDestinationDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostDestinationUpdateEntity> SP_ManifestCarrierCost_CostDestinationUpdateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostInlandOriginCreateEntity> SP_ManifestCarrierCost_CostInlandOriginCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostInlandOriginDeleteEntity> SP_ManifestCarrierCost_CostInlandOriginDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostInlandOriginUpdateEntity> SP_ManifestCarrierCost_CostInlandOriginUpdateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostOriginCreateEntity> SP_ManifestCarrierCost_CostOriginCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostOriginDeleteEntity> SP_ManifestCarrierCost_CostOriginDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostOriginUpdateEntity> SP_ManifestCarrierCost_CostOriginUpdateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostCarrierCreateEntity> SP_ManifestCarrierCost_CostCarrierCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostCarrierDeleteEntity> SP_ManifestCarrierCost_CostCarrierDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierCost_CostCarrierUpdateEntity> SP_ManifestCarrierCost_CostCarrierUpdateEntitys { get; set; }
        public DbSet<SP_SuperVision_GetSpbIdEntity> SP_SuperVision_GetSpbIdEntitys { get; set; }
        public DbSet<ListGoodsMonitoringReceiverEntity> ListGoodsMonitoringReceiverEntities { get; set; }
        public DbSet<SP_PrivilegeMenuListEntity> SP_PrivilegeMenuListEntitys { get; set; }
        public DbSet<SP_PrivilegeMenuActionListEntity> SP_PrivilegeMenuActionListEntitys { get; set; }
        public DbSet<SP_CheckRatesEntity> SP_CheckRatesEntitiys { get; set; }
        public DbSet<SPManifestCarrierNumberGeneratorEntity> SPManifestCarrierNumberGeneratorEntitys { get; set; }
        public DbSet<SPSpb_VoidEntity> SPSpb_VoidEntitiys { get; set; }
        public DbSet<SPSpb_UpdatePaymentEntity> SPSpb_UpdatePaymentEntitys { get; set; }
        public DbSet<SPSpb_FOCEntity> SPSpb_FOCEntitys { get; set; }
        public DbSet<SP_PrivilegeMenuActionDetailListEntity> SP_PrivilegeMenuActionDetailListEntitys { get; set; }
        public DbSet<SP_PrivilegeRoleListBlockEntity> SP_PrivilegeRoleListBlockEntitys { get; set; }
        public DbSet<SP_PrivilegeRoleListTujuanEntity> SP_PrivilegeRoleListTujuanEntitys { get; set; }
        public DbSet<SP_CheckDestBlockCityEntity> SP_CheckDestBlockCityEntitys { get; set; }
        public DbSet<SP_PrivilegeManageDataEntity> SP_PrivilegeManageDataEntitys { get; set; }
        public DbSet<SP_RoleManageDataEntity> SP_RoleManageDataEntitys { get; set; }
        public DbSet<SP_UserManageDataEntity> SP_UserManageDataEntitys { get; set; }
        public DbSet<SP_PrivilegeRoleManageDataEntity> SP_PrivilegeRoleManageDataEntitys { get; set; }
        public DbSet<SP_SaveDataDestBlockCityEntity> SP_SaveDataDestBlockCityEntitys { get; set; }
        public DbSet<SP_SaveDataUserEntity> SP_SaveDataUserEntitys { get; set; }
        public DbSet<SP_SaveDataUserDetailEntity> SP_SaveDataUserDetailEntitys { get; set; }
        public DbSet<SP_SaveDataPrivilegeEntity> SP_SaveDataPrivilegeEntitys { get; set; }
        public DbSet<SP_UpdateStatusEntity> SP_UpdateStatusEntitys { get; set; }
        public DbSet<SP_SaveUnitPlaceEntity> SP_SaveUnitPlaceEntitys { get; set; }
        public DbSet<SP_SaveDataUnitEntity> SP_SaveDataUnitEntitys { get; set; }
        public DbSet<SP_SaveDataUnitSubBranchEntity> SP_SaveDataUnitSubBranchEntitys { get; set; }
        public DbSet<SP_PrivilegeDistribusiPrintEntity> SP_PrivilegeDistribusiPrintEntitys { get; set; }
        public DbSet<SP_SaveDataCustomerHeaderEntity> SP_SaveDataCustomerHeaderEntitys { get; set; }
        public DbSet<SP_CreateRolesEntity> SP_CreateRolesEntitys { get; set; }
        public DbSet<SP_GetDataCustomerDetailEntity> SP_GetDataCustomerDetailEntitys { get; set; }
        public DbSet<SP_GetDataTotalCustomerDetailEntity> SP_GetDataTotalCustomerDetailEntitys { get; set; }
        public DbSet<SP_CheckAgreeEntity> SP_CheckAgreeEntitys { get; set; }
        public DbSet<SP_PaymentMethod_CreateEntity> SP_PaymentMethod_CreateEntitys { get; set; }
        public DbSet<SP_PaymentMethod_DeleteEntity> SP_PaymentMethod_DeleteEntitys { get; set; }
        public DbSet<SP_ModaCity_CreateEntity> SP_ModaCity_CreateEntitys { get; set; }
        public DbSet<SP_ModaCity_DeleteEntity> SP_ModaCity_DeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrier_GetManifestEntity> SP_ManifestCarrier_GetManifestEntitys { get; set; }
        public DbSet<SP_ManifestCarrier_LastManifestEntity> SP_ManifestCarrier_LastManifestEntitys { get; set; }
        public DbSet<SP_GetManifestCasrrierCountManifestEntity> SP_GetManifestCasrrierCountManifestEntitys { get; set; }
        public DbSet<SP_GetManifestCarrierListManifestEntity> SP_GetManifestCarrierListManifestEntitys { get; set; }
        public DbSet<SP_ManifestListEntity> SP_ManifestListEntitys { get; set; }
        public DbSet<SP_SPBListEntity> SP_SPBListEntitys { get; set; }
        public DbSet<SP_MonitoringBarangEntity> SP_MonitoringBarangEntitys { get; set; }
        public DbSet<SP_MonitoringSpbEntity> SP_MonitoringSpbEntitys { get; set; }
        public DbSet<SP_MasterCityCreateEntity> SP_MasterCityCreateEntitys { get; set; }
        public DbSet<SP_MasterCityDeleteEntity> SP_MasterCityDeleteEntitys { get; set; }
        public DbSet<SP_DeleteUserDetailEntity> SP_DeleteUserDetailEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_FlightRouteEntity> SP_ManifestCarrierMaster_FlightRouteEntitys { get; set; }
        public DbSet<SP_MappingAreaNoExistsCreateEntity> SP_MappingAreaNoExistsCreateEntitys { get; set; }
        public DbSet<SP_MappingAreaNoExistsDeleteEntity> SP_MappingAreaNoExistsDeleteEntitys { get; set; }
        public DbSet<SP_SpbCreate_ListSubDistrictEntity> SP_SpbCreate_ListSubDistrictEntitys { get; set; }
        public DbSet<SP_Statistic_OmsetEntity> SP_Statistic_OmsetEntitys { get; set; }
        public DbSet<SP_MonitoringCapacityEntity> SP_MonitoringCapacityEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorCreateEntity> SP_ManifestCarrierMaster_VendorCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorDeleteEntity> SP_ManifestCarrierMaster_VendorDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorOriginCreateEntity> SP_ManifestCarrierMaster_VendorOriginCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorOriginDeleteEntity> SP_ManifestCarrierMaster_VendorOriginDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorTransitCreateEntity> SP_ManifestCarrierMaster_VendorTransitCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorTransitDeleteEntity> SP_ManifestCarrierMaster_VendorTransitDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorDestinationCreateEntity> SP_ManifestCarrierMaster_VendorDestinationCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorDestinationDeleteEntity> SP_ManifestCarrierMaster_VendorDestinationDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorCarrierCreateEntity> SP_ManifestCarrierMaster_VendorCarrierCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_VendorCarrierDeleteEntity> SP_ManifestCarrierMaster_VendorCarrierDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_CarrierCreateEntity> SP_ManifestCarrierMaster_CarrierCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_CarrierDeleteEntity> SP_ManifestCarrierMaster_CarrierDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_FlightCreateEntity> SP_ManifestCarrierMaster_FlightCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_FlightDeleteEntity> SP_ManifestCarrierMaster_FlightDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_FlightRouteCreateEntity> SP_ManifestCarrierMaster_FlightRouteCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_FlightRouteDeleteEntity> SP_ManifestCarrierMaster_FlightRouteDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_TransitRouteCreateEntity> SP_ManifestCarrierMaster_TransitRouteCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_TransitRouteDeleteEntity> SP_ManifestCarrierMaster_TransitRouteDeleteEntitys { get; set; }
        public DbSet<SP_CheckPrintStatusEntity> SP_CheckPrintStatusEntitys { get; set; }
        public DbSet<SP_SuperVision_GetDocIdEntity> SP_SuperVision_GetDocIdEntitys { get; set; }
        public DbSet<SP_SuperVisionRePrintEntity> SP_SuperVisionRePrintEntitys { get; set; }
        public DbSet<SP_MonitoringShipmentCreateEntity> SP_MonitoringShipmentCreateEntitys { get; set; }
        public DbSet<SP_MonitoringShipmentUpdateEntity> SP_MonitoringShipmentUpdateEntitys { get; set; }
        public DbSet<SP_MonitoringShipmentDeleteEntity> SP_MonitoringShipmentDeleteEntitys { get; set; }
        public DbSet<SP_MonitoringShipmentListEntity> SP_MonitoringShipmentListEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_StationCreateEntity> SP_ManifestCarrierMaster_StationCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_StationDeleteEntity> SP_ManifestCarrierMaster_StationDeleteEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_StationCityCreateEntity> SP_ManifestCarrierMaster_StationCityCreateEntitys { get; set; }
        public DbSet<SP_ManifestCarrierMaster_StationCityDeleteEntity> SP_ManifestCarrierMaster_StationCityDeleteEntitys { get; set; }
        public DbSet<SP_CheckFragilePerishableCityEntity> SP_CheckFragilePerishableCityEntitys { get; set; }
        public DbSet<SP_GeneratePasswordEntity> SP_GeneratePasswordEntitys { get; set; }
        public DbSet<SP_MonitoringHistory_HistoryNotPrintEntity> SP_MonitoringHistory_HistoryNotPrintEntitys { get; set; }
        public DbSet<SP_MonitoringHistory_HistoryPrintEntity> SP_MonitoringHistory_HistoryPrintEntitys { get; set; }
        public DbSet<SP_MonitoringHistory_HistoryLoginEntity> SP_MonitoringHistory_HistoryLoginEntitys { get; set; }
        public DbSet<SP_MonitoringHistory_HistoryActionEntity> SP_MonitoringHistory_HistoryActionEntitys { get; set; }
        public DbSet<SP_GenerateCustomerCodeEntity> SP_GenerateCustomerCodeEntitys { get; set; }
        public DbSet<SP_GeneratePorterCodeEntity> SP_GeneratePorterCodeEntitys { get; set; }
        public DbSet<SP_PorterCode_CreateEntity> SP_PorterCode_CreateEntitys { get; set; }
        public DbSet<SP_CawFinalKoliEntity> SP_CawFinalKoliEntitys { get; set; }
        public DbSet<SP_CawFinalSpbEntity> SP_CawFinalSpbEntitys { get; set; }
        public DbSet<SP_CawVolumeEntity> SP_CawVolumeEntitys { get; set; }
        public DbSet<SP_UpdateKiloEntity> SP_UpdateKiloEntitys { get; set; }
        public DbSet<SPSpbGoods_CalculatePriceEntity> SPSpbGoods_CalculatePriceEntitys { get; set; }
        public DbSet<SP_UpdateTotalKoliEntity> SP_UpdateTotalKoliEntitys { get; set; }
        public DbSet<SP_DeleteTotalKoliEntity> SP_DeleteTotalKoliEntitys { get; set; }
        public DbSet<SP_RoudCreateEntity> SP_RoudCreateEntitys { get; set; }
        public DbSet<SP_RoudDeleteEntity> SP_RoudDeleteEntitys { get; set; }
        public DbSet<SP_DividedListEntity> SP_DividedListEntitys { get; set; }
        public DbSet<SP_DividedCreateEntity> SP_DividedCreateEntitys { get; set; }
        public DbSet<SP_GetSpbManifestEntity> SP_GetSpbManifestEntitys { get; set; }
        public DbSet<SP_CoaCodeCreateEntity> SP_CoaCodeCreateEntities { get; set; }
        public DbSet<SP_CoaCodeUpdateEntity> SP_CoaCodeUpdateEntities { get; set; }
        public DbSet<SP_AddBukuKasEntity> SP_AddBukuKasEntities { get; set; }
        public DbSet<SP_BukuKasCreateEntity> SP_BukuKasCreateEntities { get; set; }
        public DbSet<SP_ExpensesCreateEntity> SP_ExpensesCreateEntities { get; set; }
        public DbSet<SP_DeleteBukuKasEntity> SP_DeleteBukuKasEntities { get; set; }
        public DbSet<SP_UpdateBukuKasEntity> SP_UpdateBukuKasEntities { get; set; }
        public DbSet<SP_GeneralLedgerListEntity> SP_GeneralLedgerListEntities { get; set; }
        public DbSet<SP_ExpensesListEntity> SP_ExpensesListEntities { get; set; }
        public DbSet<SP_CostListEntity> SP_CostListEntities { get; set; }
        public DbSet<SP_ProfitLossPerDayListEntity> SP_ProfitLossPerDayListEntities { get; set; }
        public DbSet<SP_ARListEntity> SP_ARListEntities { get; set; }
        public DbSet<SP_AccountReceivableListEntity> SP_AccountReceivableListEntities { get; set; }
        public DbSet<SP_GetSalesReportOnlyTotalEntity> SP_GetSalesReportOnlyTotalEntities { get; set; }
        public DbSet<SP_InsertAREntity> SP_InsertAREntities { get; set; }
        public DbSet<SP_SalesReportEntity> SP_SalesReportEntities { get; set; }
        public object HistoryAksiEntities => throw new System.NotImplementedException();
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return base.SaveChangesAsync(cancellationToken);
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DbContextPs).Assembly);

            modelBuilder.Entity<SP_GetSpbManifestEntity>().HasNoKey();
            modelBuilder.Entity<SP_DividedListEntity>().HasNoKey();
            modelBuilder.Entity<SP_DividedCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_DeleteTotalKoliEntity>().HasNoKey();
            modelBuilder.Entity<SP_UpdateTotalKoliEntity>().HasNoKey();
            modelBuilder.Entity<SP_RoudCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_RoudDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_UpdateKiloEntity>().HasNoKey();
            modelBuilder.Entity<SP_CawFinalKoliEntity>().HasNoKey();
            modelBuilder.Entity<SP_CawFinalSpbEntity>().HasNoKey();
            modelBuilder.Entity<SP_CawVolumeEntity>().HasNoKey();
            modelBuilder.Entity<SP_PorterCode_CreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_GenerateCustomerCodeEntity>().HasNoKey();
            modelBuilder.Entity<SP_GeneratePorterCodeEntity>().HasNoKey();
            modelBuilder.Entity<SP_Statistic_OmsetEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringHistory_HistoryNotPrintEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringHistory_HistoryPrintEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringHistory_HistoryLoginEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringHistory_HistoryActionEntity>().HasNoKey();
            modelBuilder.Entity<SP_GeneratePasswordEntity>().HasNoKey();
            modelBuilder.Entity<SP_CheckPrintStatusEntity>().HasNoKey();
            modelBuilder.Entity<SP_SuperVisionRePrintEntity>().HasNoKey();
            modelBuilder.Entity<SP_SuperVision_GetDocIdEntity>().HasNoKey();
            modelBuilder.Entity<SP_GetManifestCasrrierCountManifestEntity>().HasNoKey();
            modelBuilder.Entity<SP_GetManifestCarrierListManifestEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_FlightCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_FlightDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_FlightRouteCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_FlightRouteDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_CarrierCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_CarrierDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorOriginCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorOriginDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorTransitCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorTransitDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorDestinationCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorDestinationDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorCarrierCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_VendorCarrierDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringCapacityEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringShipmentListEntity>().HasNoKey();
            modelBuilder.Entity<SP_SpbCreate_ListSubDistrictEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_FlightRouteEntity>().HasNoKey();
            modelBuilder.Entity<SP_DeleteUserDetailEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestListEntity>().HasNoKey();
            modelBuilder.Entity<SP_SPBListEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringBarangEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringSpbEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrier_LastManifestEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrier_GetManifestEntity>().HasNoKey();
            modelBuilder.Entity<SP_ModaCity_CreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ModaCity_DeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_SuperVision_GetSpbIdEntity>().HasNoKey();
            modelBuilder.Entity<SP_PaymentMethod_CreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_PaymentMethod_DeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_SuperVision_GetSpbIdEntity>().HasNoKey();
            modelBuilder.Entity<SP_GetDataCustomerDetailEntity>().HasNoKey();
            modelBuilder.Entity<SP_GetDataTotalCustomerDetailEntity>().HasNoKey();
            modelBuilder.Entity<SP_SaveDataPrivilegeEntity>().HasNoKey();
            modelBuilder.Entity<SP_SaveDataUserEntity>().HasNoKey();
            modelBuilder.Entity<SP_CreateRolesEntity>().HasNoKey();
            modelBuilder.Entity<SP_SaveDataUserDetailEntity>().HasNoKey();
            modelBuilder.Entity<SP_UpdateStatusEntity>().HasNoKey();
            modelBuilder.Entity<SP_SaveDataUnitEntity>().HasNoKey();
            modelBuilder.Entity<SP_SaveDataUnitSubBranchEntity>().HasNoKey();
            modelBuilder.Entity<SPSpbNumberGeneratorEntity>().HasNoKey();
            modelBuilder.Entity<SP_SaveUnitPlaceEntity>().HasNoKey();
            modelBuilder.Entity<SP_PrivilegeMenuActionListEntity>().HasNoKey();
            modelBuilder.Entity<SP_PrivilegeMenuActionDetailListEntity>().HasNoKey();
            modelBuilder.Entity<SP_PrivilegeMenuListEntity>().HasNoKey();
            modelBuilder.Entity<SP_PrivilegeRoleListTujuanEntity>().HasNoKey();
            modelBuilder.Entity<SP_PrivilegeRoleListBlockEntity>().HasNoKey();
            modelBuilder.Entity<SP_PrivilegeManageDataEntity>().HasNoKey();
            modelBuilder.Entity<SP_RoleManageDataEntity>().HasNoKey();
            modelBuilder.Entity<SP_UserManageDataEntity>().HasNoKey();
            modelBuilder.Entity<SP_PrivilegeRoleManageDataEntity>().HasNoKey();
            modelBuilder.Entity<SP_SaveDataDestBlockCityEntity>().HasNoKey();
            modelBuilder.Entity<SP_SaveDataCustomerHeaderEntity>().HasNoKey();
            modelBuilder.Entity<SP_PrivilegeDistribusiPrintEntity>().HasNoKey();
            modelBuilder.Entity<SPSpbNumberGeneratorEntity>().HasNoKey();
            modelBuilder.Entity<SPManifestNumberGeneratorEntity>().HasNoKey();
            modelBuilder.Entity<SP_CheckDestBlockCityEntity>().HasNoKey();
            modelBuilder.Entity<SPManifestDetailEntity>().HasNoKey();
            modelBuilder.Entity<SPSpbGoods_CalculatePriceEntity>().HasNoKey();
            modelBuilder.Entity<SPSpb_SetManifestIdEntity>().HasNoKey();
            modelBuilder.Entity<SPSpb_VoidEntity>().HasNoKey();
            modelBuilder.Entity<SPSpb_UpdatePaymentEntity>().HasNoKey();
            modelBuilder.Entity<SPSpb_FOCEntity>().HasNoKey();
            modelBuilder.Entity<SPManifest_MonitoringEntity>().HasNoKey();
            modelBuilder.Entity<SP_MasterCityCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_MasterCityCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_MasterCityDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SPManifestCarrierNumberGeneratorEntity>().HasNoKey();
            modelBuilder.Entity<SP_CheckAgreeEntity>().HasNoKey();
            modelBuilder.Entity<SPSpb_WaktuInputDailyEntity>().HasKey(k => k.Username);
            modelBuilder.Entity<SPSpbHourlyEntity>().HasKey(k => new { k.ManifestId, k.Destination });
            modelBuilder.Entity<SP_ManifestCarrierCost_CostTransitCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostTransitDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostTransitUpdateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostGeraiDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostGeraiUpdateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostGeraiCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostPenerusCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostPenerusUpdateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostPenerusDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostDestinationCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostDestinationUpdateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostDestinationDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostInlandOriginCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostInlandOriginUpdateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostInlandOriginDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostOriginCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostOriginUpdateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostOriginDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostCarrierCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostCarrierUpdateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierCost_CostCarrierDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_MappingAreaNoExistsCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_MappingAreaNoExistsDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_TransitRouteCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_TransitRouteDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringShipmentCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringShipmentUpdateEntity>().HasNoKey();
            modelBuilder.Entity<SP_MonitoringShipmentDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_StationCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_StationDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_StationCityCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ManifestCarrierMaster_StationCityDeleteEntity>().HasNoKey();
            modelBuilder.Entity<SP_CheckFragilePerishableCityEntity>().HasNoKey();
            modelBuilder.Entity<SP_CoaCodeCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_CoaCodeUpdateEntity>().HasNoKey();
            modelBuilder.Entity<SP_AddBukuKasEntity>().HasNoKey();
            modelBuilder.Entity<SP_BukuKasCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_ExpensesCreateEntity>().HasNoKey();
            modelBuilder.Entity<SP_DeleteBukuKasEntity>().HasNoKey();
            modelBuilder.Entity<SP_UpdateBukuKasEntity>().HasNoKey();
            modelBuilder.Entity<SP_GeneralLedgerListEntity>().HasNoKey();
            modelBuilder.Entity<SP_ExpensesListEntity>().HasNoKey();
            modelBuilder.Entity<SP_CostListEntity>().HasNoKey();
            modelBuilder.Entity<SP_ProfitLossPerDayListEntity>().HasNoKey();
            modelBuilder.Entity<SP_ARListEntity>().HasNoKey();
            modelBuilder.Entity<SP_AccountReceivableListEntity>().HasNoKey();
            modelBuilder.Entity<SP_GetSalesReportOnlyTotalEntity>().HasNoKey();
            modelBuilder.Entity<SP_InsertAREntity>().HasNoKey();
            modelBuilder.Entity<SP_SalesReportEntity>().HasNoKey();
            
            modelBuilder.Entity<ListGoodsMonitoringReceiverEntity>().ToTable("List_GoodsMonitoring_Receiver").HasKey(k => k.ManifestId);



            modelBuilder.Entity<VCarrierEntity>().ToTable("VCarrier").HasKey(k => k.MasterCarrierId);

            //https://www.c-sharpcorner.com/article/asp-net-core-entity-framework-call-store-procedure/

        }
    }
}

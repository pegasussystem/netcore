﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class DataManifestQuery : IRequest<ReturnFormat>
    {
        public Int32? originStationId { get; set; }
        public Int32? destinationStationId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class DataManifestQueryHandler : IRequestHandler<DataManifestQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public DataManifestQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(DataManifestQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_ManifestCarrier_GetManifestEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---

            DateTime ManifestDate = DateTime.UtcNow.AddHours((double)req.uiUserProfileEntity.WorkTimeZoneHour);
            DateTime dNow = DateTime.UtcNow.AddHours((double)req.uiUserProfileEntity.WorkTimeZoneHour);

            int hNow = dNow.Hour;
            ManifestDate = dNow;
            // if (hNow > 12)
            // {
            //     ManifestDate = dNow;
            // }
            // else
            // {
            //     ManifestDate = dNow.AddDays(-1);
            // }


            entities = this.ManifestCarrier_GetManifest(ManifestDate.Year, ManifestDate.Month, ManifestDate.Day, req.originStationId, req.destinationStationId);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                        s.CreatedAt
                        ,
                        s.ManifestId
                        ,
                        s.ManifestNo
                        ,
                        s.SumAw
                        ,
                        s.SumCaw
                        ,
                        s.CountKoli
                        ,
                        s.OriginCityId
                        ,
                        s.OriginCityCode
                        ,
                        s.OriginCityName
                        ,
                        s.OriginStationId
                        ,
                        s.DestinationCityId
                        ,
                        s.DestinationCityCode
                        ,
                        s.DestinationCityName
                        ,
                        s.SisaCaw
                        ,
                        s.SisaKoli
                        ,
                        s.SisaCawFinal
                        ,
                        s.SisaKoliFinal
                        ,
                        CreatedAtLocal = s.CreatedAt.Value.AddHours((double)req.uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((double)req.uiUserProfileEntity.WorkTimeZoneMinute)
                    });
            }

            // return
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrier_GetManifestEntity> ManifestCarrier_GetManifest(Int32? year, Int32? month, Int32? day, Int32? originStationId, Int32? destinationStationId)
        {
            String query = $@"EXEC SP_ManifestCarrier_GetManifest @year = {year}, @month = {month}, @day = {day}, @originStationId = {originStationId}, @destinationStationId = {destinationStationId}";
            var res = this._context.SP_ManifestCarrier_GetManifestEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }
}
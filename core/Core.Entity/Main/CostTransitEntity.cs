﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class CostTransitEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? CostTransitId { get; set; }
        public Int32? StationTransitId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? VendorTransitId { get; set; }
    }
}

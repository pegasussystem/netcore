﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface ISPSpbHourlyRepo : IRepo<SPSpbHourlyEntity>
    {
        public IEnumerable<SPSpbHourlyEntity> List(UiUserProfileEntity entity, int year, int month, int day);
    }
}

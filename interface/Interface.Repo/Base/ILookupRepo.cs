﻿using Core.Entity.Base;

namespace Interface.Repo.Base
{
    public interface ILookupRepo : IRepo<LookupEntity>
    { 
    }
}

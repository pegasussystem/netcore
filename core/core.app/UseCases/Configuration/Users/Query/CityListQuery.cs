﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Configuration.Users.Query
{
    public partial class CityListQuery : IRequest<ReturnFormat>
    {
        public String? type { get; set; }
    }

    public class CityListQueryHandler : IRequestHandler<CityListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public CityListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(CityListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.V_DataOptionCitySubDistrictEntitys
                        where a.Type.Equals(req.type)
                        select new
                        {
                            a.Id
                            , a.Code
                            , a.Name
                            , a.Type
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.Id
                        , s.Code
                        , s.Name
                        , s.Type
                    }).OrderBy(o => o.Id);
            }

            // return
            return rtn;
        }

    }
}

﻿using System.Threading.Tasks;
using core.app.UseCases.Master.MappingAreaNoExists.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Master.MappingAreaNoExists
{
   
    [Authorize]
    [Route("master-mapping-area-no-exists")]
    [ApiController]
    public class ListController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public ListController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list-mapping-area-no-exist")]
        public async Task<IActionResult> ListMappingAreaNoExists([FromQuery] ListMappingAreaNoExistsQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("mapping-area-no-exist-list-area")]
        public async Task<IActionResult> ListMappingAreaNoExistsArea([FromQuery] ListMappingAreaNoExistsQuery_ListAreaQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("mapping-area-no-exist-list-city")]
        public async Task<IActionResult> ListMappingAreaNoExistsCity([FromQuery] ListMappingAreaNoExistsQuery_ListCityQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("mapping-area-no-exist-list-subdistrict")]
        public async Task<IActionResult> ListMappingAreaNoExistsSubDistrict([FromQuery] ListMappingAreaNoExistsQuery_ListSubDistrictQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Queries
{
    public partial class ListPrivilegeMenuActionQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public Int32? PrivilegeId { get; set; }
        public Int32? MenuId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListPrivilegeMenuActionQueryHandler : IRequestHandler<ListPrivilegeMenuActionQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPrivilegeMenuActionQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPrivilegeMenuActionQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.PrivilegeActionEntitys
                        join b in _context.MenuActionEntitys on a.MenuActionId equals b.MenuActionId
                        join c in _context.MenuDataEntitys on b.MenuId equals c.MenuId
                        join d in _context.PrivilegeEntitys on a.PrivilegeId equals d.PrivilegeId
                        where a.PrivilegeId.Equals(req.PrivilegeId) && b.MenuId.Equals(req.MenuId)
                        select new
                        {
                            b.MenuActionId
                            , b.MenuAction
                            , b.MenuActionName
                            , a.PrivilegeId
                            , d.PrivilegeName
                            , c.MenuId
                            , c.MenuName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.MenuActionId
                        , s.MenuAction
                        , s.MenuActionName
                        , s.PrivilegeId
                        , s.PrivilegeName
                        , s.MenuId
                        , s.MenuName
                    }).OrderBy(o => o.MenuActionId);
            }

            // return
            return rtn;
        }
    }
}
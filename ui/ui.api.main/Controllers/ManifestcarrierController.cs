﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCore.RouteAnalyzer;
using core.helper;
using Core.Entity.Base;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Newtonsoft.Json;

namespace ui.api.main.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ManifestCarrierController : ControllerBase
    {

        private readonly IAppWrapperPs _appPs;
        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;

        public ManifestCarrierController(IAppWrapperPs app, IActionDescriptorCollectionProvider actionDescriptorCollectionProvider)
        {
            _appPs = app;
            _actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
        }

        [HttpGet("/")]
        public async Task<IActionResult> List([FromForm]VManifestEntity entity = null)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.ManifestCarrierApp.List(entity, GetUserClaim());


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

      

        [HttpGet("read-manifest")]
        public async Task<IActionResult> ManifestRead()
        {


            var routes = _actionDescriptorCollectionProvider.ActionDescriptors.Items.ToList();




            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.ManifestCarrierApp.ReadManifest(GetUserClaim());


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("read-city")]
        public async Task<IActionResult> ReadCity()
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();


            // logic
            // ///// ///// /////
            _returnFormat = _appPs.ManifestCarrierApp.ReadCity(GetUserClaim());


            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpGet("read-carrier")]
        public async Task<IActionResult> ReadCarrier()
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();


            // logic
            // ///// ///// /////
            //_returnFormat = _appPs.ManifestCarrierApp.ReadCarrier(GetUserClaim());
            _returnFormat = _appPs.Sb_Carrier_UseCase.Read(GetUserClaim());


            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        //[HttpGet("list-detail")]
        //public async Task<IActionResult> ListDetail([FromQuery]VManifestDetailEntity entity = null)
        //{

        //    //
        //    // variable
        //    ReturnFormat _returnFormat = new ReturnFormat();


        //    //
        //    // logic

        //    // get data Vspb
        //    _returnFormat = _appPs.ManifestSenderApp.ListDetail(entity, GetUserClaim());


        //    //
        //    // return
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}

        //[HttpGet("list-spb")]
        //public async Task<IActionResult> ListSpb([FromQuery]VManifestEntity entity = null)
        //{

        //    //
        //    // variable
        //    ReturnFormat _returnFormat = new ReturnFormat();


        //    //
        //    // logic

        //    // get data Vspb
        //    _returnFormat = _appPs.ManifestSenderApp.ListSpb(entity, GetUserClaim());


        //    //
        //    // return
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}

        //[HttpPost("spb-customer-print")]
        //public async Task<IActionResult> SpbCustomer([FromForm]VSpbEntity entity)
        //{

        //    //
        //    // variable
        //    ReturnFormat _returnFormat = new ReturnFormat();


        //    //
        //    // logic

        //    // get data Vspb
        //    _returnFormat = _appPs.ManifestSenderApp.SpbCustomer(GetUserClaim(), entity);


        //    //
        //    // return
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}

        UiUserProfileEntity GetUserClaim() {
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            // get claim user
            var claims = User.Claims.Where(w => w.Type.Equals("userProfile")).First().Value;
            uiUserProfileEntity = JsonConvert.DeserializeObject<UiUserProfileEntity>(claims);

            return uiUserProfileEntity;
        }
    }
}
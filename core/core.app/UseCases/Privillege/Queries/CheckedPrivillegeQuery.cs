﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Queries
{
    public partial class CheckedPrivillegeQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class CheckedPrivillegeQueryHandler : IRequestHandler<CheckedPrivillegeQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public CheckedPrivillegeQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(CheckedPrivillegeQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();

            // cek data exists di menuKey
            rtn.Data    = "none";
            rtn.Status  = StatusCodes.Status204NoContent;

            var menuList = req.uiUserProfileEntity.MenuKeyLogin;

            if (menuList == null) { return rtn; }

            int cnt_a = 0; var menu_id = "";
            foreach (string item in menuList)
            {
                var a = item.Split("~").FirstOrDefault();
                var b = item.Split("~").LastOrDefault();

                if (req.uiUserProfileEntity.MenuKey == b) {
                    cnt_a++;
                    menu_id = a;
                }
            }

            if (cnt_a == 0) { return rtn; }

            // cek data exists di menuAction berdasarkan MenuId yang sama dengan menuKey
            var menuActionList = req.uiUserProfileEntity.MenuActionLogin;

            if (menuActionList == null) { return rtn; }

            int cnt_b = 0;
            foreach (string item in menuActionList)
            {
                var a = item.Split("~").FirstOrDefault();
                var b = item.Split("~").LastOrDefault();

                if (req.uiUserProfileEntity.MenuAction == b && a == menu_id)
                {
                    cnt_b++;
                }
            }

            if (cnt_b == 0) { return rtn; }

            //return
            return rtn;
        }
    }
}

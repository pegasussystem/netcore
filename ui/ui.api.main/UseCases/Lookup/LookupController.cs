﻿using core.app.UseCases.Lookup;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Lookup
{
    [Authorize]
    [Route("lookup-list")]
    [ApiController]

    public class LookupController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public LookupController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("read")]
        public async Task<IActionResult> Read([FromBody] LookupListQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }


        [HttpPost("check")]
        public async Task<IActionResult> Read([FromBody] LookupCheckQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        //[HttpPost("update")]
        //public async Task<IActionResult> Update([FromBody] RatesCommand q)
        //{

        //    ---variable
        //    -- - ------------
        //   ReturnFormat _returnFormat = new ReturnFormat();

        //    ---set variable
        //   -- - ------------
        //  q.uiUserProfileEntity = GetUserClaim();

        //    ---get data
        //   -- - ------------
        //  _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

        //    --- return
        //    ---------------
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}

        //[HttpGet("delete")]
        //public async Task<IActionResult> Delete([FromQuery] ListUpdatePasswordQuery q)
        //{

        //    // --- variable
        //    // --- --- --- --- ---
        //    ReturnFormat _returnFormat = new ReturnFormat();

        //    // --- set variable
        //    // --- --- --- --- ---
        //    q.uiUserProfileEntity = GetUserClaim();

        //    // --- get data
        //    // --- --- --- --- ---
        //    _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

        //    // --- return
        //    // --- --- --- --- ---
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}

    }
}

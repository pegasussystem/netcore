﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListVendorDestinationReadQuery : IRequest<ReturnFormat>
    {
        public Int32? destinationStationId { get; set; }
    }

    public class ListVendorDestinationReadQueryHandler : IRequestHandler<ListVendorDestinationReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListVendorDestinationReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListVendorDestinationReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from vo in _context.VendorDestinationEntitys
                        join vd in _context.VendorEntitys on vo.VendorId equals vd.VendorId
                        where vo.StationId.Equals(req.destinationStationId)
                        select new
                        {
                            vo.VendorId
                            , vd.VendorName
                        };

            var query2 = query.Distinct();

            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(
                    s => new
                    {
                        s.VendorId
                        , s.VendorName
                    }).OrderByDescending(o => o.VendorId);
            }

            //
            // return
            return rtn;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
   public class FlightTransitRouteEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? TransitRouteId { get; set; }
        public Int32? OriginStationId { get; set; }
        public Int32? FirstFlightCodeId { get; set; }
        public Int32? FirstFlightId { get; set; }
        public Int32? StationTransitId { get; set; }
        public Int32? LastFlightCodeId { get; set; }
        public Int32? LastFlightId { get; set; }
        public Int32? FinalDestinationStationId { get; set; }
        public Int32? SequenceTransit { get; set; }
        public Int32? VendorTransitId { get; set; }
    }
}

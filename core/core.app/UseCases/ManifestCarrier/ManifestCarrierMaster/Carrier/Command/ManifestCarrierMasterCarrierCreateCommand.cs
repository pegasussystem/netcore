﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Carrier.Command
{
    public partial class ManifestCarrierMasterCarrierCreateCommand : IRequest<ReturnFormat>
    {
        public String? carrierCode { get; set; }
        public String? carrierName { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterCarrierCreateCommandHandler : IRequestHandler<ManifestCarrierMasterCarrierCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterCarrierCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterCarrierCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CarrierCreate(req.carrierCode, req.carrierName, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_CarrierCreateEntity> CarrierCreate(String? carrierCode, String? carrierName, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_CarrierCreate @carrierCode='{carrierCode}', @carrierName='{carrierName}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierMaster_CarrierCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class ManifestCarrierCostSMUConfig : IEntityTypeConfiguration<ManifestCarrierCostSMUEntity>
    {
        public void Configure(EntityTypeBuilder<ManifestCarrierCostSMUEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierCostSMUId);
            builder.ToTable("ManifestCarrierCostSMU");

            builder.Property(p => p.CreatedAt)
              .HasDefaultValueSql("getutcdate()");
        }
    }
}

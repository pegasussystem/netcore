﻿    using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Entity.Base
{
    public class LookupEntity
    {
        public Int32? LookupId { get; set; }
        public String? LookupKeyParent { get; set; }
        public String? LookupKey { get; set; }
        public String? LookupValue { get; set; }
        public Int32? Order { get; set; }
        public String? Description { get; set; }
        public Int32? SubBranchId { get; set; }
    }
}

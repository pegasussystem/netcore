﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.CostManifestCarrier.CostTransit.Commands
{
    public partial class CostTransitCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? StationTransitId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? VendorTransitId { get; set; }
        public String? BaseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }

 
    }

    public class CostTransitCreateCommandHandler : IRequestHandler<CostTransitCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostTransitCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostTransitCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {

        // Mark as Changed
        var res = this.CostTransitCreate(req.StationTransitId, req.CarrierId, req.VendorTransitId, req.BaseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostTransitCreateEntity> CostTransitCreate(Int32? StationTransitId, Int32? CarrierId, Int32? VendorTransitId, String? BaseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostTransitCreate @StationTransitId= {StationTransitId}, @CarrierId ={CarrierId}, @VendorTransitId = {VendorTransitId}, @BaseCost='{BaseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostTransitCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_CostTransit_Entity
    {
        // CostOrigin
        public Int32? CostTransitId { get; set; }
        public Int32? StationTransitId { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? VendorTransitId { get; set; }

        // CostOriginDetail
        public Int32? CostTransitDetailId { get; set; }
        public String? CostType { get; set; }
        public String? Unit { get; set; }
        public Decimal? BaseCost { get; set; }
        public String? CostGroup { get; set; }
        public String? UseWeight { get; set; }
    }
}

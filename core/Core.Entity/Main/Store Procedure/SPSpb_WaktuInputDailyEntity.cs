﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SPSpb_WaktuInputDailyEntity
    {


        public String? Username { get; set; }
        public Int32? TotalSpb { get; set; }
        public decimal? TotalSpbAvgPerHour { get; set; }
        public String? MinuteTotal { get; set; }
        public String? MinuteAvg { get; set; }
        public String? MinuteMax { get; set; }
        public String? MinuteMin { get; set; }
        public Int32? S1_30 { get; set; }
        public Int32? S31_60 { get; set; }
        public Int32? S61_90 { get; set; }
        public Int32? S91_120 { get; set; }
        public Int32? S121_150 { get; set; }
        public Int32? S151_180 { get; set; }
        public Int32? S181_210 { get; set; }
        public Int32? S211_240 { get; set; }
        public Int32? S241_270 { get; set; }
        public Int32? S271_300 { get; set; }
        public Int32? S301_330 { get; set; }
        public Int32? S331_360 { get; set; }
        public Int32? S361_390 { get; set; }
        public Int32? S391_420 { get; set; }
        public Int32? S421_450 { get; set; }
        public Int32? S451_480 { get; set; }
        public Int32? S481_510 { get; set; }
        public Int32? S511_540 { get; set; }
        public Int32? S541_570 { get; set; }
        public Int32? S571_600 { get; set; }
        public Int32? S601_ { get; set; }


    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MonitoringShipment.Query
{
    public partial class MonitoringShipmentReadViaQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MonitoringShipmentReadViaQueryHandler : IRequestHandler<MonitoringShipmentReadViaQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MonitoringShipmentReadViaQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MonitoringShipmentReadViaQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.StationEntitys
                        join b in _context.StationCityEntitys on a.StationId equals b.StationId
                        join c in _context.MasterCityEntitys on b.CityId equals c.MasterCityId
                        where b.Priority.Equals("1")
                        select new
                        {
                            a.StationId
                            , StationName = "[ "+ c.MasterCityCode+ " ] - " + a.StationName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.StationId,
                        s.StationName

                    }).OrderBy(o => o.StationName);
            }

            // return
            return rtn;
        }
    }
}


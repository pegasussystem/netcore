﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class CarrierEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }

        public Int32? CarrierId { get; set; }
        public String? CarrierCode { get; set; }
        public String? CarrierType { get; set; }
        public String? CarrierName { get; set; }
        public Int32? ModaId { get; set; }
    }
}

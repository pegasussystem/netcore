﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadDetailManifestCarrier
{
    public partial class ListDetailQuery : IRequest<ReturnFormat>
    {
        public String? manifestCarrierId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListDetailDetailQueryHandler : IRequestHandler<ListDetailQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListDetailDetailQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListDetailQuery req, CancellationToken cancellationToken)
        {
            Int32? mcId = Int32.Parse(req.manifestCarrierId);

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query =
                from mcc in _context.V_ManifestCarrierDetailEntitys
                where mcc.ManifestCarrierId.Equals(mcId)

                select new
                {
                    mcc.ManifestCarrierId
                    , mcc.ManifestCarrierNo
                    , mcc.OriginStationId
                    , mcc.OriginStation
                    , mcc.VendorOriginId
                    , mcc.OriginVendor
                    , mcc.OriginCityId
                    , mcc.OriginCity
                    , mcc.DestinationStationId
                    , mcc.DestinationStation
                    , mcc.NoSmu
                    , mcc.FlightDate
                    , mcc.ModaId
                    , mcc.ModaName
                    , mcc.CarrierId
                    , mcc.VendorCarrierId
                    , mcc.vendorCarrier
                    , mcc.FirstFlightCodeId
                    , mcc.FirstFlightCode
                    , mcc.FirstFlightNoId
                    , mcc.FirstFlightNo
                    , mcc.SecondFlightCodeId
                    , mcc.SecondFlightCode
                    , mcc.SecondFlightNoId
                    , mcc.SecondFlightNo
                    , mcc.ThirdFlightCodeId
                    , mcc.ThirdFlightCode
                    , mcc.ThirdFlightNoId
                    , mcc.ThirdFlightNo
                    , mcc.FourthFlightCodeId
                    , mcc.FourthFlightCode
                    , mcc.FourthFlightNoId
                    , mcc.FourthFlightNo
                    , mcc.FirstStationTransitId
                    , mcc.FirstStationTransit
                    , mcc.FirstVendorTransitId
                    , mcc.FirstVendorTransit
                    , mcc.SecondStationTransitId
                    , mcc.SecondStationTransit
                    , mcc.SecondVendorTransitId
                    , mcc.SecondVendorTransit
                    , mcc.ThirdStationTransitId
                    , mcc.ThirdStationTransit
                    , mcc.ThirdVendorTransitId
                    , mcc.ThirdVendorTransit
                    , mcc.ReasonKoli
                    , mcc.ReasonManifest
                    , mcc.ReasonSMU
                    , mcc.ApprovedById
                    , mcc.ApprovedByPassword
                    , mcc.ApprovedBy
                    , mcc.ApprovedDate
                    , mcc.PpnId
                    , mcc.Ppn
                    , cost_smu = mcc.CostSmu
                    , mcc.TotalFinalWeightSMU
                    , mcc.TotalKoliFinal
                    , mcc.Revise
                };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.ManifestCarrierId
                        , s.ManifestCarrierNo
                        , s.OriginStationId
                        , s.OriginStation
                        , s.VendorOriginId
                        , s.OriginVendor
                        , s.OriginCityId
                        , s.OriginCity
                        , s.DestinationStationId
                        , s.DestinationStation
                        , s.NoSmu
                        , s.FlightDate
                        , s.ModaId
                        , s.ModaName
                        , s.CarrierId
                        , s.VendorCarrierId
                        , s.vendorCarrier
                        , s.FirstFlightCodeId
                        , s.FirstFlightCode
                        , s.FirstFlightNoId
                        , s.FirstFlightNo
                        , s.SecondFlightCodeId
                        , s.SecondFlightCode
                        , s.SecondFlightNoId
                        , s.SecondFlightNo
                        , s.ThirdFlightCodeId
                        , s.ThirdFlightCode
                        , s.ThirdFlightNoId
                        , s.ThirdFlightNo
                        , s.FourthFlightCodeId
                        , s.FourthFlightCode
                        , s.FourthFlightNoId
                        , s.FourthFlightNo
                        , s.FirstStationTransitId
                        , s.FirstStationTransit
                        , s.FirstVendorTransitId
                        , s.FirstVendorTransit
                        , s.SecondStationTransitId
                        , s.SecondStationTransit
                        , s.SecondVendorTransitId
                        , s.SecondVendorTransit
                        , s.ThirdStationTransitId
                        , s.ThirdStationTransit
                        , s.ThirdVendorTransitId
                        , s.ThirdVendorTransit
                        , s.ReasonKoli
                        , s.ReasonManifest
                        , s.ReasonSMU
                        , s.ApprovedById
                        , s.ApprovedByPassword
                        , s.ApprovedBy
                        , s.ApprovedDate
                        , s.PpnId
                        , s.Ppn
                        , s.cost_smu
                        , s.TotalFinalWeightSMU
                        , s.TotalKoliFinal
                        , s.Revise

                    }).OrderByDescending(o => o.ManifestCarrierId);
            }

            //
            // return
            return rtn;
        }
    }
}

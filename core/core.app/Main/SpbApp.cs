﻿using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace core.app.Main
{
    public class SpbApp : ISpbApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        public SpbApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(SpbEntity entity)
        {
            throw new NotImplementedException();
        }
      
        public SpbEntity Delete(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat SpbNumberGenerator(UiUserProfileEntity enity)
        {
            //
            // -- variable & set variable
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;
            SPSpbNumberGeneratorEntity entity = new SPSpbNumberGeneratorEntity();

            //
            // -- validation
            if (enity.companyId == 0) {
                return rtn;
            }

            //
            // -- get data
            entity = _repoPs.SpbRepo.SpbNumberGenerator(enity).First();

            if (entity  != null) {
                rtn.Data = entity;
                rtn.Status = StatusCodes.Status200OK;
            }

            // return
            return rtn;
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }


        public ReturnFormat Read(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        


        public SpbEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        
      

        public ReturnFormat Create(SpbEntity entity, string name, string place, string store, string address)
        {
            throw new NotImplementedException();
        }
    }
}

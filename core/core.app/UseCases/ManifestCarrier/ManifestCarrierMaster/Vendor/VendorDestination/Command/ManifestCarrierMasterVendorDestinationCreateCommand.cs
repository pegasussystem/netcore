﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorDestination.Command
{
    public partial class ManifestCarrierMasterVendorDestinationCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? vendorId { get; set; }
        public Int32? stationId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterVendorDestinationCreateCommandHandler : IRequestHandler<ManifestCarrierMasterVendorDestinationCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public ManifestCarrierMasterVendorDestinationCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterVendorDestinationCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.VendorDestinationCreate(req.vendorId, req.stationId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_VendorDestinationCreateEntity> VendorDestinationCreate(Int32? vendorId, Int32? stationId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_VendorDestinationCreate @vendorId='{vendorId}', @stationId='{stationId}' ";
            var res = this._context.SP_ManifestCarrierMaster_VendorDestinationCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
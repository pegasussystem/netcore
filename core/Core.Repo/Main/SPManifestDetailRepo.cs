﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class SPManifestDetailRepo : Repo<SPManifestDetailEntity>, ISPManifestDetailRepo
    {
        public SPManifestDetailRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public IEnumerable<SPManifestDetailEntity> ReadSp(UiUserProfileEntity entity, long? manifestId=0)
        {
            var res = this._contextPs.Set<SPManifestDetailEntity>()
                .FromSqlRaw($@"EXEC [SPManifestDetail] @CompanyId={entity.companyId},  @ManifestId={manifestId}");
            //return this._contextPs.Query<CompanyEntity>(query, parameters);
            return res;
        }
    }
}

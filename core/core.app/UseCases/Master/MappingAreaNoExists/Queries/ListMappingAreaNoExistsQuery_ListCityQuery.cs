﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MappingAreaNoExists.Queries
{
    public partial class ListMappingAreaNoExistsQuery_ListCityQuery : IRequest<ReturnFormat>
    {
    }

    public class ListMappingAreaNoExistsQuery_ListCityQueryHandler : IRequestHandler<ListMappingAreaNoExistsQuery_ListCityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListMappingAreaNoExistsQuery_ListCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListMappingAreaNoExistsQuery_ListCityQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.VCompanyCityEntities
                        select new
                        {
                            a.CityId
                            , CityName = a.CompanyCityNameCustom
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CityId
                        ,s.CityName
                    }).OrderBy(o => o.CityId);
            }

            // return
            return rtn;
        }

    }
}

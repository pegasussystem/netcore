﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Spb.Queries.SpbCreate
{
    public partial class ListSubDistrictQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public String? destinationCityId { get; set; }
    }

    public class ListSubDistrictQueryHandler : IRequestHandler<ListSubDistrictQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListSubDistrictQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListSubDistrictQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_SpbCreate_ListSubDistrictEntity> entities = this.DataListSubDistrict(req.uiUserProfileEntity.userId, req.destinationCityId);

            if (entities != null)
            {
               rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        s.CompanyAreaId
                        , s.CompanyId
                        , s.MasterCityId
                        , s.MasterSubDistrictId
                        , s.CompanyAreaCode
                        , s.CompanyAreaName
                        , s.CompanyAreaNameCustom
                        , s.ParentId
                        , s.IsVia
                    }).OrderBy(o => o.MasterSubDistrictId);
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_SpbCreate_ListSubDistrictEntity> DataListSubDistrict(Int32? UserId, String? DestinationCityId)
        {
            String query = $@"EXEC SP_SpbCreate_ListSubDistrict @userId = '{UserId}', @mastercityId = '{DestinationCityId}' ";
            var res = this._context.SP_SpbCreate_ListSubDistrictEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

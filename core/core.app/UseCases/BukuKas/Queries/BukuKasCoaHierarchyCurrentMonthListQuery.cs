using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.BukuKas.Queries
{
    public partial class BukuKasCoaHierarchyCurrentMonthListQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class BukuKasCoaHierarchyCurrentMonthListQueryHandler : IRequestHandler<BukuKasCoaHierarchyCurrentMonthListQuery, ReturnFormat>
    {

        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public BukuKasCoaHierarchyCurrentMonthListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(BukuKasCoaHierarchyCurrentMonthListQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;
            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var query = from a in _context.V_BukuKasCoaHierarchyCurrentMonthEntities
                            select new
                            {
                                a.bukuKasId
                                ,
                                a.tanggal
                                ,
                                a.coaCode
                                ,
                                a.coaParentName
                                ,
                                a.coaName
                                ,
                                a.keterangan
                                ,
                                a.total
                                ,
                                a.karakterPertama
                                ,
                                a.karakterKedua
                                ,
                                a.karakterKetiga
                                ,
                                a.karakterKeempat
                                ,
                                a.karakterKelima
                                ,
                                a.karakterKeenam
                                ,
                                a.karakterKetujuh
                                ,
                                a.karakterKedelapan
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.bukuKasId
                                ,
                            s.tanggal
                                ,
                            s.coaCode
                                ,
                            s.coaParentName
                                ,
                            s.coaName
                                ,
                            s.keterangan
                                ,
                            s.total
                                ,
                            s.karakterPertama
                                ,
                            s.karakterKedua
                                ,
                            s.karakterKetiga
                                ,
                            s.karakterKeempat
                                ,
                            s.karakterKelima
                                ,
                            s.karakterKeenam
                                ,
                            s.karakterKetujuh
                                ,
                            s.karakterKedelapan
                        }).OrderBy(o => o.karakterPertama)
.ThenBy(o => o.bukuKasId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }


            // return 
            return rtn;
        }
    }
}

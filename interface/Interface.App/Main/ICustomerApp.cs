﻿using core.helper;
using Core.Entity.Base;  //Pati 26Agt21
using Core.Entity.Main;
using Interface.App.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App.Main
{

    public interface ICustomerApp : IApp<CustomerEntity>
    {
        ReturnFormat ReadPhone(CustomerEntity customerEntity);
        ReturnFormat ReadName(CustomerNameEntity entity);
        ReturnFormat ReadAddress(CustomerAddressEntity entity);
        ReturnFormat ReadPlace(CustomerPlaceEntity entity);
        ReturnFormat ReadStore(CustomerStoreEntity entity);

        ReturnFormat CekPhone(CustomerEntity customerEntity);  // Pati 19Agt21
        ReturnFormat ApprovalCust(UserEntity userEntity);  // Pati 26Agt21

        ReturnFormat Create(CustomerEntity customerEntity, string name, string place, string store, string address);
    }
}

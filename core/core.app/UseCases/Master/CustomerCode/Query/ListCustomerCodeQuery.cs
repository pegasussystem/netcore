﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;


namespace core.app.UseCases.Master.CustomerCode.Query
{
    public partial class ListCustomerCodeQuery : IRequest<ReturnFormat>
    {
        public String? cityId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListCustomerCodeQueryHandler : IRequestHandler<ListCustomerCodeQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCustomerCodeQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCustomerCodeQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.V_CustomerCodeDataEntitys
                        where a.CityId.Equals(req.cityId)
                        select new
                        {
                            a.CustomerHeaderId
							, a.CustomerCode
                            , a.Telp
							, a.Phone
							, a.CustomerName
							, a.CustomerStore
							, a.CustomerPlace
							, a.CustomerAddress
							, a.Rt
							, a.Rw
							, a.UrbanName
							, a.PostalCode
							, a.SubDistrictName
                            , a.CityId
							, a.CityCode
							, a.CityName
                            , a.ProvinceName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CustomerHeaderId
						, s.CustomerCode
                        , s.Telp
						, s.Phone
						, s.CustomerName
						, s.CustomerStore
						, s.CustomerPlace
						, s.CustomerAddress
						, s.Rt
						, s.Rw
						, s.UrbanName
						, s.PostalCode
						, s.SubDistrictName
                        , s.CityId
						, s.CityCode
						, s.CityName
						, s.ProvinceName
                    }).OrderBy(o => o.CityCode);
            }

            // return
            return rtn;
        }
    }
}

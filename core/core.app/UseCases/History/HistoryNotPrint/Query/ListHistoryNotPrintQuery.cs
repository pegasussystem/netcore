﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Ui;
using Microsoft.EntityFrameworkCore;
using Core.Entity.Main.Store_Procedure;

namespace core.app.UseCases.History.HistoryNotPrint.Query
{
    public partial class ListHistoryNotPrintQuery : IRequest<ReturnFormat>
    {
        public DateTime? dateFrom { get; set; }
        public DateTime? dateTo { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class ListHistoryNotPrintQueryHandler : IRequestHandler<ListHistoryNotPrintQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListHistoryNotPrintQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListHistoryNotPrintQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_MonitoringHistory_HistoryNotPrintEntity> entities;
            
            var YearFrom    = req.dateFrom.Value.Year;
            var MonthFrom   = req.dateFrom.Value.Month;
            var DayFrom     = req.dateFrom.Value.Day;

            var YearTo      = req.dateTo.Value.Year;
            var MonthTo     = req.dateTo.Value.Month;
            var DayTo       = req.dateTo.Value.Day;

            var DateFrom    = YearFrom + "-" + MonthFrom + "-" + DayFrom;
            var DateTo      = YearTo + "-" + MonthTo + "-" + DayTo;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataHistoryNotPrint(DateFrom, DateTo);


            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                        s.SpbId
                        , s.SpbNo
                        , s.CreatedAt
                        , s.CreatedBy
                        , s.TypesOfGoods
                        , s.Carrier
                        , s.QualityOfService
                        , s.TypeOfService
                        , s.PaymentMethod
                        , s.Username
                    }).OrderByDescending(o => o.SpbId);
            }

            // return
            return rtn;
        }
        public IEnumerable<SP_MonitoringHistory_HistoryNotPrintEntity> DataHistoryNotPrint(String? DateFrom, String? DateTo)
        {
            String query = $@"EXEC SP_MonitoringHistory_HistoryNotPrint @dateFrom='{DateFrom}', @dateTo='{DateTo}'";
            var res = this._context.SP_MonitoringHistory_HistoryNotPrintEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }

}

﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
     class V_StationStatusConfig : IEntityTypeConfiguration<V_StationStatusEntity>
        {
            public void Configure(EntityTypeBuilder<V_StationStatusEntity> builder)
            {
                builder.HasKey(e => e.StatusId);
                builder.ToTable("V_StationStatus");


            }
        }
}

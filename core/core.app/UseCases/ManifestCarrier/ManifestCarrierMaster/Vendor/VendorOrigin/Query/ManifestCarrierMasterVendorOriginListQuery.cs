﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Vendor.VendorOrigin.Query
{
    public partial class ManifestCarrierMasterVendorOriginListQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterVendorOriginListQueryHandler : IRequestHandler<ManifestCarrierMasterVendorOriginListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ManifestCarrierMasterVendorOriginListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterVendorOriginListQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var query = from a in _context.VendorOriginEntitys
                            join b in _context.StationEntitys on a.StationId equals b.StationId
                            join c in _context.VendorEntitys on a.VendorId equals c.VendorId
                            join d in _context.StationCityEntitys on b.StationId equals d.StationId
                            join e in _context.MasterCityEntitys on d.CityId equals e.MasterCityId
                            where d.Priority.Equals("1")
                            select new
                            {
                                a.VendorOriginId
                                , a.VendorId
                                , c.VendorName
                                , c.Description
                                , StationName = "[ " +e.MasterCityCode + " ] - " + b.StationName
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.VendorOriginId
                            , s.VendorId
                            , s.VendorName
                            , s.Description
                            , s.StationName
                        }).OrderByDescending(o => o.VendorOriginId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
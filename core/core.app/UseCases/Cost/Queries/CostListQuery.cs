using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using core.app.Common.Interfaces;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace core.app.UseCases.Cost.Queries
{
    public partial class CostListQuery : IRequest<ReturnFormat>
    {
        public int month { get; set; }
        public int year { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class CostListQueryHandler : IRequestHandler<CostListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public CostListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(CostListQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_CostListEntity> entities;

            // DateTime utcDate = req.CreatedAt.Value.AddHours((double)(-1 * req.uiUserProfile.WorkTimeZoneHour)).AddMinutes((double)(req.uiUserProfile.WorkTimeZoneMinute * -1));


            entities = this.GetCostListByMonth(req.month,req.year);


            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                        s.costId,
                        s.coaCode,
                        s.coaName,
                        s.createdAt,
                        s.total,
                        s.digitPertama,
                        s.digitKedua,
                        s.digitKetiga,
                        s.digitKeempat,
                        s.digitKelima,
                        s.digitKeenam

                     });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_CostListEntity> GetCostListByMonth(int month,int year)
        {
            // String query = $@"EXEC SP_CostList @month = {month},@year = {year}";
            // var res = this._context.SP_CostListEntities.FromSqlRaw(query).ToList();
            // return res;
        
            const string query = "EXEC SP_CostList @month = @p0, @year = @p1";
            return _context.SP_CostListEntities.FromSqlRaw(query, month, year).ToList();
        }

    }
}
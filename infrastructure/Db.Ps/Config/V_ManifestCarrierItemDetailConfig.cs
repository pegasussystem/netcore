﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_ManifestCarrierItemDetailConfig : IEntityTypeConfiguration<V_ManifestCarrierItemDetailEntity>
    {
        public void Configure(EntityTypeBuilder<V_ManifestCarrierItemDetailEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierItemId);
            builder.ToTable("V_ManifestCarrierItemDetail");
        }
    }
}

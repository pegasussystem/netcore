﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using core.helper;
using Interface.Repo;
using Interface.Other;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostOrigin.Query
{
     public partial class ReadVendorQuery : IRequest<ReturnFormat>
    {
        public Int32? stationOriginId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ListVendorQueryHandler : IRequestHandler<ReadVendorQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListVendorQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ReadVendorQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var query = from a in _context.VendorOriginEntitys
                            join b in _context.StationEntitys on a.StationId equals b.StationId
                            join c in _context.VendorEntitys on a.VendorId equals c.VendorId
                            where a.StationId.Equals(req.stationOriginId)
                            select new
                            {
                                VendorId = a.VendorOriginId
                                , c.VendorName
                            };  

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                            s.VendorId,
                            s.VendorName,

                        }).OrderBy(o => o.VendorName);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_UpdateStatusEntity
    {
        public String? result { get; set; }
        public String? message { get; set; }
    }
}

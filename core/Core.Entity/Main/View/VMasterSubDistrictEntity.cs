﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VMasterSubDistrictEntity
    {

        public Int32? DestArea { get; set; }
        public String? MasterProvinceId { get; set; }
        public String? MasterProvinceCode { get; set; }
        public String? MasterProvinceName { get; set; }

        //###MasterCity	 
        public String? MasterCityId { get; set; }
        public String? MasterCityCode { get; set; }
        public String? MasterCityType { get; set; }
        public String? MasterCityName { get; set; }

        //###MasterSubDistrict
        public String? MasterSubDistrictId { get; set; }
        public String? MasterSubDistrictCode { get; set; }
        public String? MasterSubDistrictName { get; set; }
        public String? MasterSubDistrictNameCustom { get; set; }


        public String? ParentId { get; set; }

    }





}

﻿
using core.app.Common.Interfaces; 
using core.helper; 
using Interface.Other; 
using Interface.Repo; 
using MediatR; 
using Microsoft.AspNetCore.Http; 
using System; 
using System.Collections.Generic; 
using System.Text; 
using System.Linq; 
using System.Threading; 
using System.Threading.Tasks; 
 
namespace core.app.UseCases.MasterDataCustomer.Queries
{

    public partial class MasterDataCustomerQuery : IRequest<ReturnFormat>
    {
        //public DateTime? CreatedAt { get; set; } 
        //public UiUserProfileEntity uiUserProfileEntity { get; set; } 
    }
    public class MasterDataCustomerQueryHandler : IRequestHandler<MasterDataCustomerQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MasterDataCustomerQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MasterDataCustomerQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;



            var query = from a in _context.MasterDataCustomerEntitys
                        select new
                        {
                            a.CustomerId,
                            a.CustomerNameId,
                            a.CustomerStoreId,
                            a.CustomerPlaceId,
                            a.CustomerAddressId,
                            a.CreatedAt,
                            a.Telepon,
                            a.Phone,
                            a.CustomerName,
                            a.CustomerStore,
                            a.CustomerPlace,
                            a.CustomerAddress
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CustomerId,
                        s.CustomerNameId,
                        s.CustomerStoreId,
                        s.CustomerPlaceId,
                        s.CustomerAddressId,
                        s.CreatedAt,
                        s.Telepon,
                        s.Phone,
                        s.CustomerName,
                        s.CustomerStore,
                        s.CustomerPlace,
                        s.CustomerAddress

                    }).OrderByDescending(o => o.CreatedAt);
            }
            // return 
            return rtn;
        }


    }

}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Spb.Queries.SpbCreate
{
    public partial class CheckFragilePerishableCityQuery : IRequest<ReturnFormat>
    {
        public String? tog { get; set; }
        public String? moda { get; set; }
        public Decimal? goodWeight { get; set; }
        public Int32? destinationCityId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class CheckFragilePerishableCityQueryHandler : IRequestHandler<CheckFragilePerishableCityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public CheckFragilePerishableCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(CheckFragilePerishableCityQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_CheckFragilePerishableCityEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.FragilePerishableCityCheck(req.tog, req.moda, req.goodWeight, req.destinationCityId);

            if (entities != null)
            {
               rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        s.res
                    });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_CheckFragilePerishableCityEntity> FragilePerishableCityCheck(String? tog, String? moda, Decimal? goodWeight, Int32? destinationCityId)
        {
            String query = $@"EXEC SP_CheckFragilePerishableCity @tog = '{tog}', @moda = '{moda}', @goodWeight = '{goodWeight}', @destinationCityId = '{destinationCityId}' ";
            var res = this._context.SP_CheckFragilePerishableCityEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_MonitoringCapacityCarrierEntity
    {
        public Int32? cid { get; set; }
        public String? carrierCode { get; set; }
        public String? carrierDesc { get; set; }
    }
}

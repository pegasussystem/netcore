﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.SpbUpdate.Query
{
    public partial class GetDataCawFinalSpbQuery : IRequest<ReturnFormat>
    {
        public Int64? spbId { get; set; }
        public String? first_cawFinal { get; set; }
        public String? second_cawFinal { get; set; }
        public String? third_cawFinal { get; set; }
        public String? fourth_cawFinal { get; set; }
        public String? fifth_cawFinal { get; set; }
        public String? ratesType { get; set; }
        public String? rates { get; set; }
        public String? ppn { get; set; }
        public String? discount { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class GetDataCawFinalSpbQueryHandler : IRequestHandler<GetDataCawFinalSpbQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetDataCawFinalSpbQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetDataCawFinalSpbQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_CawFinalSpbEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.DataCawFinal(req.spbId, req.first_cawFinal, req.second_cawFinal, req.third_cawFinal, req.fourth_cawFinal, req.fifth_cawFinal, req.ratesType, req.rates, req.ppn, req.discount);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
               rtn.Data = entities.Select(
                    s => new
                    {
                        s.CawFinalSpb
                        , s.TotalPrice
                        , s.PpnValue
                        , s.DiscountValue
                    });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_CawFinalSpbEntity> DataCawFinal(Int64? SpbId , String? FirstCawFinal, String? SecondCawFinal, String? ThirdCawFinal, String? FourthCawFinal, String? FifthCawFinal, String? RatesType, String? Rates, String? Ppn, String? Disc)
        {
            String query = $@"EXEC SP_CawFinalSpb @spbId = '{SpbId}', @first_cawFinal = '{FirstCawFinal.Replace(",", ".")}', @second_cawFinal = '{SecondCawFinal.Replace(",", ".")}', @third_cawFinal='{ThirdCawFinal.Replace(",", ".")}', @fourth_cawFinal = '{FourthCawFinal.Replace(",", ".")}', @fifth_cawFinal = '{FifthCawFinal.Replace(",", ".")}', @ratesType = '{RatesType}', @rates = '{Rates}', @ppn = '{Ppn.Replace(",", ".")}' , @disc = '{Disc}' ";
            var res = this._context.SP_CawFinalSpbEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

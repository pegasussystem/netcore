﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.FlightRoute.Command
{
    public partial class FlightRouteCreateCommand : IRequest<ReturnFormat>
    {
        public Int32? originStationId { get; set; }
        public Int32? destinationStationId  { get; set; }
        public Int32? carrierId { get; set; }
        public Int32? vendorCarrierId { get; set; }
        public Int32? firstFlightId { get; set; }
        public Int32? secondFlightId { get; set; }
        public Int32? thirdFlightId { get; set; }
        public Int32? fourthFlightId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class FlightRouteCreateCommandHandler : IRequestHandler<FlightRouteCreateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public FlightRouteCreateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(FlightRouteCreateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.FlightRouteCreate(
                    req.originStationId       
                    , req.destinationStationId  
                    , req.carrierId             
                    , req.vendorCarrierId       
                    , req.firstFlightId         
                    , req.secondFlightId        
                    , req.thirdFlightId         
                    , req.fourthFlightId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_FlightRouteCreateEntity> FlightRouteCreate(
                    Int32? originStationId
                    , Int32? destinationStationId
                    , Int32? carrierId
                    , Int32? vendorCarrierId
                    , Int32? firstFlightId
                    , Int32? secondFlightId
                    , Int32? thirdFlightId
                    , Int32? fourthFlightId
            )
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_FlightRouteCreate 
                                            @originStationId        = '{ originStationId }',
                                            @destinationStationId   = '{ destinationStationId }',
                                            @carrierId              = '{ carrierId }',
                                            @vendorCarrierId        = '{ vendorCarrierId }',
                                            @firstFlightId          = '{ firstFlightId }',
                                            @secondFlightId         = '{ secondFlightId }',
                                            @thirdFlightId          = '{ thirdFlightId }',
                                            @fourthFlightId         = '{ fourthFlightId }'";
            var res = this._context.SP_ManifestCarrierMaster_FlightRouteCreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
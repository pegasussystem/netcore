﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostOriginDetailConfig : IEntityTypeConfiguration<CostOriginDetailEntity>
    {
        public void Configure(EntityTypeBuilder<CostOriginDetailEntity> builder)
        {
            builder.HasKey(e => e.CostOriginDetailId);
            builder.ToTable("CostOriginDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
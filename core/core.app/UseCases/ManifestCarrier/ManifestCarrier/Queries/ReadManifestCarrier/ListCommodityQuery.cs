﻿using core.app.Common.Interfaces;
using core.helper;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier
{
    public partial class ListCommodityQuery : IRequest<ReturnFormat>
    {
    }
    public class ListCommodityQueryHandler : IRequestHandler<ListCommodityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCommodityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCommodityQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from lc in _context.LookupEntitys
                        where lc.LookupKeyParent.Equals("tog")
                        select new
                        {
                            lc.LookupKey
                            ,lc.LookupValue
                            ,lc.Order
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.LookupKey
                        , s.LookupValue
                        , s.Order
                    }).OrderByDescending(o => o.Order);
            }

            //
            // return
            return rtn;
        }
    }
}

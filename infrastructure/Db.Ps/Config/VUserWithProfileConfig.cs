﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VUserWithProfileConfig : IEntityTypeConfiguration<VUserWithProfileEntity>
    {
        public void Configure(EntityTypeBuilder<VUserWithProfileEntity> builder)
        {
            builder.HasKey(e => e.UserId);
            builder.ToView("VUserWithProfile", "dbo");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace core.app.UseCases.SpbHourly.List
{
    public class ListSpbHourlyInput
    {
        public String? DateString { get; set; }


        public int Year() {  return Int32.Parse(DateString.Split('-')[0]); }
        public int Month() {  return Int32.Parse(DateString.Split('-')[1]); }
        public int Day() {  return Int32.Parse(DateString.Split('-')[2]); }


        
    }
}

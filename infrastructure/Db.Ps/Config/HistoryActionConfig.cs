﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
   class HistoryActionConfig : IEntityTypeConfiguration<HistoryActionEntity>
    {
        public void Configure(EntityTypeBuilder<HistoryActionEntity> builder)
        {
            builder.HasKey(e => e.HistoryActionId);
            builder.ToTable("HistoryAction");
        }
    }
}

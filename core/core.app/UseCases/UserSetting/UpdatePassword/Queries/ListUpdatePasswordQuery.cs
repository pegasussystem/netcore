﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.UpdatePassword.Queries
{
    public partial class ListUpdatePasswordQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListUpdatePasswordQueryHandler : IRequestHandler<ListUpdatePasswordQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListUpdatePasswordQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListUpdatePasswordQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from u in _context.UserEntitys
                        where u.UserId.Equals(req.uiUserProfileEntity.userId)
                        select new
                        {
                            u.UserId
                            , u.Username
                            , u.Email
                            , u.Password
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.UserId
                        , s.Username
                        , s.Email
                        , s.Password
                    }).OrderByDescending(o => o.UserId);
            }

            //
            // return
            return rtn;
        }
    }
}

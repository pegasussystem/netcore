﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Master.MasterTransit.Queries
{
    public partial class ListDataCityTransitReadQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public String? cityTransitId { get; set; }
    }

    public class ListDataCityTransitReadQueryHandler : IRequestHandler<ListDataCityTransitReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListDataCityTransitReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListDataCityTransitReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // --- --- --- --- ---
            var query = from sc in _context.StationCityEntitys
                        join mc in _context.MasterCityEntitys on sc.CityId equals mc.MasterCityId
                        where sc.Priority.Equals("1")
                        select new
                        {
                            CityTransitId = sc.CityId
                            , CityTransitName = "[ " + mc.MasterCityCode + " ] - " + mc.MasterCityName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CityTransitId
                        , s.CityTransitName
                    }).OrderBy(o => o.CityTransitId);
            }




            //
            // return
            return rtn;
        }
    }
}

﻿using Core.Entity.Main.View;
using Db.Ps;
using Interface.Repo.Main;

namespace Core.Repo.Main
{
    public class VManifestCarrierRepo : Repo<VManifestCarrierEntity>, IVManifestCarrierRepo
    {
        public VManifestCarrierRepo(DbContextPs ctx) : base(ctx)
        {
        }

        
    }
}

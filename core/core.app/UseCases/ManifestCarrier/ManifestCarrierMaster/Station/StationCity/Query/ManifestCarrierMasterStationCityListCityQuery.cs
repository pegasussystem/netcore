﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Station.StationCity.Query
{
     public partial class ManifestCarrierMasterStationCityListCityQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class ManifestCarrierMasterStationCityListCityQueryHandler : IRequestHandler<ManifestCarrierMasterStationCityListCityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ManifestCarrierMasterStationCityListCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ManifestCarrierMasterStationCityListCityQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                rtn.Status = StatusCodes.Status200OK;

                var query = from a in _context.VCompanyCityEntities
                            select new
                            {
                                a.CityId
                                , CityName = a.CompanyCityNameCustom
                            };

                if (query != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = query.Select(
                        s => new
                        {
                           s.CityId
                            , s.CityName
                        }).OrderByDescending(o => o.CityId);
                }

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}
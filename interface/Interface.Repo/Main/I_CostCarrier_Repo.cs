﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface I_CostCarrier_Repo : IRepo<CostCarrier_Entity>
    {
        public IEnumerable<V_CostCarrier_Entity> V_CostCarrier(
               int? carrier1, int? carrier2, int? carrier3, int? carrier4
           );
    }
}

using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Transactions;

namespace core.app.Main
{
    public class SpbCreateApp : ISpbCreateApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;

        private UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();
        public SpbCreateApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Delete(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat ReadSpbPrint(UiUserProfileEntity uiUserProfile, VSpbEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VSpbEntity> vSpbEntities = null;
            String printAtLocalTime = DateTime.UtcNow.AddHours((double)uiUserProfile.WorkTimeZoneHour).AddMinutes((double)uiUserProfile.WorkTimeZoneMinute).ToString("dd MMM yyyy HH:mm:ss");


            String[] splitSpbNo = entity.SpbNo.Split(",");

            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data
            vSpbEntities = _repoPs.VSpbRepo.Read(w => splitSpbNo.Contains(w.SpbNo)).ToList();
            vSpbEntities.Add(vSpbEntities[0]);

            if (vSpbEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vSpbEntities.OrderBy(o => o.SpbId)
                    .Select((s, index) => new {
                        s.Carrier,
                        s.CarrierName,
                        s.CompanyId,
                        s.CreatedAt,
                        s.Description,
                        s.DestinationAreaCode,
                        s.DestinationAreaId,
                        s.DestinationAreaNameCustom,
                        s.DestinationCityCode,
                        s.DestinationCityId,
                        s.DestinationCityNameCustom,
                        s.Discount,
                        s.Etc,
                        s.OriginAreaCode,
                        s.OriginAreaId,
                        s.OriginAreaNameCustom,
                        s.OriginCityCode,
                        s.OriginCityId,
                        s.OriginCityNameCustom,
                        s.Packing,
                        s.PaymentMethod,
                        s.Ppn,
                        s.QualityOfService,
                        s.QualityOfServiceName,
                        s.Quarantine,
                        s.Rates,
                        s.ReceiverAddress,
                        s.ReceiverName,
                        s.ReceiverPhone,
                        s.ReceiverPlace,
                        s.ReceiverStore,
                        s.SenderAddress,
                        s.SenderName,
                        s.SenderPhone,
                        s.SenderPlace,
                        s.SenderStore,
                        s.SpbId,
                        s.SpbNo,
                        s.TotalPrice,
                        s.TypeOfService,
                        s.TypeOfServiceName,
                        s.TypesOfGoods,
                        s.TypesOfGoodsName,
                        s.Username,
                        s.ViaName,
                        PrintAtLocalTime = printAtLocalTime,
                        SheetFor = SetSheetFor(index, s),
                    });
            }

            //
            // return
            return rtn;
        }

        public ReturnFormat ScreenCapture(UiUserProfileEntity uiUserProfile, VSpbEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();

            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data


            //
            // return
            return rtn;
        }


        public ReturnFormat ReadSpbGoodPrint(VSpbGoodsEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VSpbGoodsEntity> vSpbGoodsEntitys1 = null;
            List<VSpbGoodsEntity> vSpbGoodsEntitys2 = null;
            Dictionary<String, object> tmpDatas = new Dictionary<String, object>();
            object tmpData = new object();


            String[] splitSpbNo = entity.SpbNo.Split(",");

            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            //
            // logic

            // get data
            foreach (string spbNo in splitSpbNo)
            {
                vSpbGoodsEntitys1 = _repoPs.VSpbGoodsRepo.Read(w => w.SpbNo.Equals(spbNo)).Take(5).ToList();
                vSpbGoodsEntitys2 = _repoPs.VSpbGoodsRepo.Read(w => w.SpbNo.Equals(spbNo)).Take(5).Skip(5).ToList();

                if (vSpbGoodsEntitys1 != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    //rtn.Data = new { part1 = vSpbGoodsEntitys1, part2 = "" };
                    tmpData = new { part1 = vSpbGoodsEntitys1, part2 = "" };
                }

                if (vSpbGoodsEntitys2 != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    //rtn.Data = new { part1 = vSpbGoodsEntitys1, part2 = vSpbGoodsEntitys2 };
                    tmpData = new { part1 = vSpbGoodsEntitys1, part2 = vSpbGoodsEntitys2 };
                }

                tmpDatas.Add(spbNo, tmpData);
            }


            //
            // return
            rtn.Data = tmpDatas;
            return rtn;
        }

        public ReturnFormat ReadCekPhone(VSpbEntity entity) // Pati 15feb22
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            CustomerEntity entityCheck;
            VCompanyCityEntity vCompanyCity;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // get city ID
            vCompanyCity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(entity.CompanyCityId)).FirstOrDefault();

            entityCheck = _repoPs.CustomerRepo.Read(w => w.Phone.Equals(entity.Phone) && w.Phone != null && w.CityCode.Equals(vCompanyCity.CityId) && w.IsActive != true).FirstOrDefault();
           
            if (entityCheck != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entityCheck;
            }
            else
            { rtn.Data ="tidak"; }

            //
            // return

            return rtn;
        }

        public ReturnFormat ReadCekTelp(VSpbEntity entity) // Pati 15feb22
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            CustomerEntity entityCheck;
            VCompanyCityEntity vCompanyCity;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // get city ID
            vCompanyCity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(entity.CompanyCityId)).FirstOrDefault();

            entityCheck = _repoPs.CustomerRepo.Read(w => w.Telp.Equals(entity.Telp) && w.Telp != null && w.CityCode.Equals(vCompanyCity.CityId) && w.IsActive != true).FirstOrDefault();

            if (entityCheck != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entityCheck;
            }
            else
            { rtn.Data = "tidak"; }

            //
            // return

            return rtn;
        }

        public ReturnFormat ReadCekPhoneReceiver(VSpbEntity entity) // Pati 15feb22
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            CustomerEntity entityCheck;
            VCompanyCityEntity vCompanyCity;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // get city ID
            vCompanyCity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(entity.CompanyCityId)).FirstOrDefault();

            entityCheck = _repoPs.CustomerRepo.Read(w => w.Phone.Equals(entity.Phone) && w.Phone != null && w.CityCode.Equals(vCompanyCity.CityId)).FirstOrDefault();

            if (entityCheck != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entityCheck;
            }
            else
            { rtn.Data = "tidak"; }

            //
            // return

            return rtn;
        }


        public ReturnFormat ReadVia(UiUserProfileEntity uiUserProfile, UiSpbCreateEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<ViaEntity> viaEntities = null;
            VCompanyCityEntity vCompanyCity;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // get city ID
            vCompanyCity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(Int32.Parse(entity.DestCity))).FirstOrDefault();

            // get data
            if (vCompanyCity != null)
            {
                viaEntities = _repoPs.ViaRepo
                .Read(w => w.CompanyId.Equals(uiUserProfile.companyId) && w.CityId.Equals(vCompanyCity.CityId)).ToList();
            }


            if (viaEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = viaEntities;
            }

            //
            // return

            return rtn;
        }

        public ReturnFormat DoSubmitForUpdateSpb(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, SpbEntity entity)
        {
            this.uiUserProfileEntity = uiUserProfileEntity;

            //
            // -- variable & set variable
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;
            SpbEntity spbEntity;

            //
            // -- validation
            if (String.IsNullOrEmpty(uiSpbCreateEntity.SpbNo))
            {
                return rtn;
            }

            //cek no telp existing / no telp barrier Pati 12Agust21
            var chkSender = 0;
            var chkReceiver = 0;

            if (uiSpbCreateEntity.Sender != null && uiSpbCreateEntity.Receiver == null) // jika yg tambah baru adalah pengirim
            { chkSender = this.CheckDataExistsSender(uiSpbCreateEntity.Sender); }

            if (uiSpbCreateEntity.Sender == null && uiSpbCreateEntity.Receiver != null) // jika yg tambah baru adalah penerima
            { chkReceiver = this.CheckDataExistsSender(uiSpbCreateEntity.Receiver); }

            if (uiSpbCreateEntity.Sender != null && uiSpbCreateEntity.Receiver != null) // jika tambah baru pengirim dan penerima
            { chkSender = this.CheckDataExistsSender(uiSpbCreateEntity.Sender); chkReceiver = this.CheckDataExistsSender(uiSpbCreateEntity.Receiver); }
            else { } // jika TIDAK tambah baru pengirim dan penerima

            //if (chkSender > 0 || chkReceiver > 0)
            //{

            //    //keluarkan dialog box "Apakah mau dilanjut"
            //    //Jika lanjut keluarkan dialog box Approval berupa login SM/Direksi
            //    rtn.Data = null;
            //    rtn.Status = StatusCodes.Status201Created;

            //}
            //else
            //{

                //
                //// get data
                //spbEntity = _repoPs.SpbRepo.Read(x => x.SpbNo.Equals(uiSpbCreateEntity.SpbNo)).First();

                //
                // save new sender & receiver if new data
                if (entity.SenderId == null) { entity.SenderId = SaveCustomer(uiSpbCreateEntity); }
                if (entity.SenderNameId == null) { entity.SenderNameId = SaveCustomerName(entity.SenderId, uiSpbCreateEntity.SenderName); }
                if (entity.SenderStoreId == null) { entity.SenderStoreId = SaveCustomerStore(entity.SenderId, uiSpbCreateEntity.SenderStore); }
                if (entity.SenderPlaceId == null) { entity.SenderPlaceId = SaveCustomerPlace(entity.SenderId, uiSpbCreateEntity.SenderPlace); }
                if (entity.SenderAddressId == null) { entity.SenderAddressId = SaveCustomerAddress(entity.SenderId, uiSpbCreateEntity.SenderAddress, null, null, null, null, null, null); }

                if (entity.ReceiverId == null) { entity.ReceiverId = SaveCustomerReceiver(uiSpbCreateEntity); }
                if (entity.ReceiverNameId == null) { entity.ReceiverNameId = SaveCustomerName(entity.ReceiverId, uiSpbCreateEntity.ReceiverName); }
                if (entity.ReceiverStoreId == null) { entity.ReceiverStoreId = SaveCustomerStore(entity.ReceiverId, uiSpbCreateEntity.ReceiverStore); }
                if (entity.ReceiverPlaceId == null) { entity.ReceiverPlaceId = SaveCustomerPlace(entity.ReceiverId, uiSpbCreateEntity.ReceiverPlace); }
                if (entity.ReceiverAddressId == null)
                {
                    entity.ReceiverAddressId = SaveCustomerAddress(entity.ReceiverId, uiSpbCreateEntity.ReceiverAddress
    , uiSpbCreateEntity.ReceiverRt, uiSpbCreateEntity.ReceiverRw, uiSpbCreateEntity.ReceiverUrban, uiSpbCreateEntity.ReceiverSubDistrict, uiSpbCreateEntity.ReceiverCity
    , uiSpbCreateEntity.ReceiverPostalCode);
                }

                if (uiSpbCreateEntity.receiverViaId == null) { entity.ViaId = SaveReceiverVia(uiUserProfileEntity, uiSpbCreateEntity, uiSpbCreateEntity.receiverVia); }
            //}

            // get data
            spbEntity = _repoPs.SpbRepo.Read(x => x.SpbNo.Equals(uiSpbCreateEntity.SpbNo)).First(); //Pindah kesini by Pati 12Agust21

            spbEntity.ViaId = (uiSpbCreateEntity.receiverViaId != null) ? uiSpbCreateEntity.receiverViaId : entity.ViaId;
            
                //
                // populate data and post to DB
                spbEntity.UpdatedAt = DateTime.UtcNow;
                spbEntity.TypesOfGoods = entity.TypesOfGoods;
                spbEntity.Carrier = entity.Carrier;
                spbEntity.QualityOfService = entity.QualityOfService;
                spbEntity.TypeOfService = entity.TypeOfService;

                spbEntity.Rates = uiSpbCreateEntity.Rates;
                spbEntity.Packing = entity.Packing;
                spbEntity.Quarantine = entity.Quarantine;
                spbEntity.Etc = entity.Etc;
                spbEntity.Ppn = entity.Ppn;
                spbEntity.Discount = entity.Discount;
                spbEntity.TotalPrice = uiSpbCreateEntity.TotalPrice;
                spbEntity.PaymentMethod = entity.PaymentMethod;
                spbEntity.Description = (entity.Description == "null") ? null : entity.Description;

                spbEntity.Min = entity.Min;
                spbEntity.MinRates = entity.MinRates;
                spbEntity.CawFinal = entity.CawFinal;

                spbEntity.RatesType = entity.RatesType;
                spbEntity.MinWeight = entity.MinWeight;

                spbEntity.SenderId = entity.SenderId;
                spbEntity.SenderNameId = entity.SenderNameId;
                spbEntity.SenderStoreId = entity.SenderStoreId;
                spbEntity.SenderPlaceId = entity.SenderPlaceId;
                spbEntity.SenderAddressId = entity.SenderAddressId;

                spbEntity.ReceiverId = entity.ReceiverId;
                spbEntity.ReceiverNameId = entity.ReceiverNameId;
                spbEntity.ReceiverStoreId = entity.ReceiverStoreId;
                spbEntity.ReceiverPlaceId = entity.ReceiverPlaceId;
                spbEntity.ReceiverAddressId = entity.ReceiverAddressId;
                

                _repoPs.CustomerRepo.SaveDataCustomer(entity.SenderId, entity.SenderNameId, entity.SenderStoreId, entity.SenderPlaceId, entity.SenderAddressId, "sender", (Int32)uiUserProfileEntity.userId).ToList();
                _repoPs.CustomerRepo.SaveDataCustomer(entity.ReceiverId, entity.ReceiverNameId, entity.ReceiverStoreId, entity.ReceiverPlaceId, entity.ReceiverAddressId, "receiver", (Int32)uiUserProfileEntity.userId).ToList();

            //spbEntity.UpdatedAt = "";
            spbEntity.CreatedBy = uiUserProfileEntity.userId.ToString();
                _repoPs.SpbRepo.Update(spbEntity);
                _repoPs.SpbRepo.Save();
                   _repoPs.AccountReceivableRepo.InsertAr(spbEntity,uiUserProfileEntity);
                   // update price 
                 var CalculatePrice = _repoPs.SpbGoodsRepo.CalculatePrice(spbEntity.SpbId).ToList();

                // update manifestId
                var SPSpb_SetManifestIdEntity = _repoPs.SpbRepo.SPSpb_SetManifestIdEntity(spbEntity).ToList();

             

                if (spbEntity != null)
                {
                    rtn.Data = spbEntity.SpbNo;
                    rtn.Status = StatusCodes.Status200OK;
                }
            

            // return
            return rtn;
        }

        //public ReturnFormat GetKoliNo(UiUserProfileEntity uiUserProfileEntity, SpbGoodsEntity entity, String SpbNo, Int64 SpbId, String SpbNoManual, int destCity, int destArea, String carrier)
        //{
        //    Random rnd = new Random();
        //    String data = $" SpbNo : {SpbNo} # SpbId: {SpbId} # SpbNoManual : {SpbNoManual} # carrier {carrier} # ";
        //    String dataGoods = $" aw : {entity.Aw} # CAW : {entity.Caw} # Height : {entity.Height} # Length : {entity.Length} # Width : {entity.Width}";

        //    String processId = rnd.Next(1, 999999999).ToString() + " _Fn_GetKoliNo_ ";
        //    _log.LogDebug(processId, ">>> >>> >>> GetKoliNo");
        //    _log.LogDebug(processId, $"[DATA] {data}");
        //    _log.LogDebug(processId, $"[dataGoods] {dataGoods}");
        //    //
        //    // -- variable & set variable
        //    int modaDarat = 4000; // TODO : hardcode
        //    int modaUdara = 6000; // TODO : hardcode
        //    Decimal caw = 0;
        //    ReturnFormat rtn = new ReturnFormat();
        //    SpbEntity spbEntity;
        //    SPManifestNumberGeneratorEntity sPManifestNumberGeneratorEntity;
        //    rtn.Status = StatusCodes.Status400BadRequest;

        //    // -- validation
        //    // --- --- --- --- ---

        //    // -- check manual with SpbNo
        //    _log.LogDebug(processId, "check manual with SpbNo");
        //    if (SpbNo != SpbNoManual)
        //    {
        //        rtn.Data = "Terjadi kesalahan. Harap Refresh browser Anda..";
        //        rtn.Status = StatusCodes.Status400BadRequest;
        //        return rtn;
        //    }

        //    // -- check spb. spb id should be equal with SpbId
        //    _log.LogDebug(processId, ">>> spbEntity");
        //    spbEntity = _repoPs.SpbRepo.Read(w => w.SpbNo.Equals(SpbNo)).First();
        //    _log.LogDebug(processId, "<<< spbEntity");
        //    if (spbEntity.SpbId != SpbId)
        //    {
        //        _log.LogDebug(processId, "ERROR - spbEntity.SpbId != SpbId");
        //        rtn.Data = "Terjadi kesalahan. Harap Refresh browser Anda..";
        //        rtn.Status = StatusCodes.Status400BadRequest;
        //        return rtn;
        //    }
        //    if (spbEntity.PaymentMethod != null)
        //    {
        //        _log.LogDebug(processId, "ERROR - spbEntity.PaymentMethod != null");
        //        rtn.Data = "Terjadi kesalahan. Harap Refresh browser Anda..";
        //        rtn.Status = StatusCodes.Status400BadRequest;
        //        return rtn;
        //    }


        //    // return if spbEntity is null
        //    if (spbEntity == null)
        //    {
        //        _log.LogDebug(processId, "ERROR - spbEntity == null");
        //        return rtn;
        //    }
        //    entity.SpbId = spbEntity.SpbId;
        //    _log.LogDebug(processId, $"spbEntity.SpbId : {spbEntity.SpbId} # ");


        //    // calculate volume
        //    caw = (entity.Length * entity.Width * entity.Height) / 4000;
        //    if (carrier == "air") { caw = (entity.Length * entity.Width * entity.Height) / modaUdara; }
        //    if (carrier == "truck") { caw = (entity.Length * entity.Width * entity.Height) / modaDarat; }

        //    if (entity.Aw > caw) { caw = entity.Aw; }
        //    entity.Caw = Math.Round(caw);


        //    // SET CAW FOR MINIMUM KG
        //    // --- --- --- --- ---

        //    // get rule pricing
        //    // if flatmin THEN get min KG 




        //    //using (var transaction = new TransactionScope(
        //    //                                        TransactionScopeOption.Required,
        //    //                                        new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted } )
        //    //    )
        //    //{
        //    try
        //    {

        //        // update location spb

        //        if (destCity != 0 && destArea != 0 && spbEntity.OriginAreaCode == null)
        //        {
        //            _log.LogDebug(processId, ">>> update location spb");
        //            spbEntity.OriginCityCode = uiUserProfileEntity.workCityId;
        //            spbEntity.OriginAreaCode = uiUserProfileEntity.WorkAreaId;
        //            spbEntity.DestinationCityCode = destCity;
        //            spbEntity.DestinationAreaCode = destArea;
        //            _repoPs.SpbRepo.Update(spbEntity);
        //            _repoPs.SpbRepo.Save();
        //            _log.LogDebug(processId, "<<< update location spb");
        //        }


        //        // save to SpbGoodsRepo
        //        _log.LogDebug(processId, ">>> save to SpbGoodsRepo");
        //        var createData = _repoPs.SpbGoodsRepo.Create(entity);
        //        _repoPs.SpbGoodsRepo.Save();
        //        _log.LogDebug(processId, $"SpbGoodsId : {createData.SpbGoodsId}");
        //        _log.LogDebug(processId, "<<< save to SpbGoodsRepo");

        //        // get koli number
        //        _log.LogDebug(processId, ">>>  get koli number");
        //        sPManifestNumberGeneratorEntity = _repoPs.ManifestRepo
        //            .GetKoliNo(uiUserProfileEntity, entity, carrier).First(); // TODO : HARDCODE
        //        _log.LogDebug(processId, $"KoliNo : {sPManifestNumberGeneratorEntity.KoliNo}");
        //        _log.LogDebug(processId, "<<<  get koli number");




        //        if (sPManifestNumberGeneratorEntity != null)
        //        {
        //            rtn.Data = sPManifestNumberGeneratorEntity;
        //            rtn.Status = StatusCodes.Status200OK;
        //        }

        //        //transaction.Complete();

        //    }

        //    catch (TransactionAbortedException ex)
        //    {
        //        //transaction.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        _log.LogDebug(processId, $"ERROR Exception ### {ex}");
        //        //transaction.Dispose();
        //    }
        //    //}


        //    _log.LogDebug(processId, "<<< <<< <<< GetKoliNo");
        //    // return
        //    return rtn;
        //}

        public Int32 CheckDataExistsSender(String ? phone) {
            CustomerEntity entityCheck;
            entityCheck = _repoPs.CustomerRepo.Read(w => w.Phone.Equals(phone) && w.IsActive != true).FirstOrDefault();
            int ada = 0;
            if (entityCheck != null) { ada = (int)entityCheck.CustomerId; }
            else { ada = 0; }
            return ada;
        }

       
        public ReturnFormat GetRates(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<RatesEntity> ratesEntities;
            List<RatesDetailEntity> ratesDetailEntities;

            List<RatesSubDistrictEntity> ratesSubDistrictEntities;
            List<RatesSubDistrictDetailEntity> ratesSubDistrictDetailEntities;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // get rates sub district
            ratesSubDistrictEntities = _repoPs.RatesSubDistrictRepo
                .Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
                        && w.TypesOfGoods.Contains(uiSpbCreateEntity.Tog)
                        && w.Carrier.Contains(uiSpbCreateEntity.Carrier)
                        && w.QualityOfService.Contains(uiSpbCreateEntity.Qos)
                        && w.TypeOfService.Contains(uiSpbCreateEntity.Tos)
                        && w.OriginCity.Contains(uiUserProfileEntity.workCityId.ToString())
                        && w.DestinationCity.Contains(uiSpbCreateEntity.DestCity)
                        && w.DestinationSubDistrictId.Contains(uiSpbCreateEntity.DestArea)
                ).ToList();

            // cek only kecamatan
            if (ratesSubDistrictEntities.Count == 0)
            {
                ratesSubDistrictEntities = _repoPs.RatesSubDistrictRepo
                .Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
                        && w.TypesOfGoods.Contains(uiSpbCreateEntity.Tog)
                        && w.Carrier.Contains(uiSpbCreateEntity.Carrier)
                        //&& w.QualityOfService.Contains(uiSpbCreateEntity.Qos)
                        //&& w.TypeOfService.Contains(uiSpbCreateEntity.Tos)
                        && w.OriginCity.Contains(uiUserProfileEntity.workCityId.ToString())
                        && w.DestinationCity.Contains(uiSpbCreateEntity.DestCity)
                        && w.DestinationSubDistrictId.Contains(uiSpbCreateEntity.DestArea)
                ).ToList();

                if (ratesSubDistrictEntities.Count > 0) { return rtn; }
            }


            if (ratesSubDistrictEntities.Count > 1) { return rtn; }


            if (ratesSubDistrictEntities.Count == 1)
            {
                // GET ratesSubDistrictDetail

                ratesSubDistrictDetailEntities = _repoPs.RatesSubDistrictDetailRepo
                    .Read(w => w.RatesSubDistrictId.Equals(ratesSubDistrictEntities.First().RatesSubDistrictId)).ToList();

                rtn.Data = ratesSubDistrictEntities
                   .Select(s => new RatesObj(s.Rates, s.RatesType, ratesSubDistrictDetailEntities.Select(s => new RatesDetail(s.Min, s.Max, s.Rates, s.Type)))).First();
                rtn.Status = StatusCodes.Status200OK;
            }
            // get rates 
            else
            {
                // get rates 
                ratesEntities = _repoPs.RatesRepo
                    .Read(w => w.CompanyId.Equals(uiUserProfileEntity.companyId)
                            && w.TypesOfGoods.Contains("<" + uiSpbCreateEntity.Tog + ">")
                            && w.Carrier.Contains("<" + uiSpbCreateEntity.Carrier + ">")
                            && w.QualityOfService.Contains("<" + uiSpbCreateEntity.Qos + ">")
                            && w.TypeOfService.Contains("<" + uiSpbCreateEntity.Tos)
                            && w.OriginCity.Contains("<" + uiUserProfileEntity.workCityId.ToString() + ">")
                            && w.DestinationCity.Contains("<" + uiSpbCreateEntity.DestCity + ">")
                        ).ToList();


                if (ratesEntities.Count > 1)
                {
                    //int i = 0;
                    //foreach (RatesEntity ratesEntity in ratesEntities) {
                    //    String qos = ratesEntity.QualityOfService.Replace("<", "").Replace(">","");
                    //    if (qos != uiSpbCreateEntity.Qos) { ratesEntities.RemoveAt(i); }
                    //    i = i + 1;
                    //}

                    for (int i = 0; i < ratesEntities.Count; i++)
                    {
                        String qos = ratesEntities[i].QualityOfService.Replace("<", "").Replace(">", "");
                        if (qos != uiSpbCreateEntity.Qos) { ratesEntities.RemoveAt(i); }
                    }
                }


                // should only ONE
                if (ratesEntities.Count != 1) { return rtn; }


                // GET RATES DETAIL
                ratesDetailEntities = _repoPs.RatesDetailRepo
                    .Read(w => w.RatesId.Equals(ratesEntities.First().RatesId)).ToList();


                rtn.Data = ratesEntities
                    .Select(s => new RatesObj(s.Rates, s.RatesType, ratesDetailEntities.Select(s => new RatesDetail(s.Min, s.Max, s.Rates, s.Type)))).First();
                rtn.Status = StatusCodes.Status200OK;
            }







            //
            // return
            return rtn;


        }


        //public ReturnFormat GetSenderUrban(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity)
        //{

        //    //
        //    // variable
        //    ReturnFormat rtn = new ReturnFormat();
        //    List<VMasterUrbanEntity> vMasterUrbanEntities = null;
        //    VCompanyAreaEntity vCompanyArea = null;

        //    //
        //    // set variable
        //    rtn.Status = StatusCodes.Status204NoContent;


        //    //
        //    // logic

        //    // get urban 
        //    if (uiSpbCreateEntity.DestArea != null)
        //    {
        //        vCompanyArea = _repoPs.VCompanyAreaRepo.Read(w => w.CompanyAreaId.Equals(Int32.Parse(uiSpbCreateEntity.DestArea))).FirstOrDefault();
        //    }

        //    // angularny gk kuat munculin data ribuan di drop down
        //    //if (uiSpbCreateEntity.isVia == true && (uiSpbCreateEntity.ReceiverSubDistrict == null || uiSpbCreateEntity.ReceiverSubDistrict == "null")) {
        //    //    String idProvince = vCompanyArea.MasterCityId.Split('.')[0];
        //    //    vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.StartsWith(idProvince)).ToList();
        //    //}


        //    // find if user select area tujuan
        //    if (uiSpbCreateEntity.isVia == true && (uiSpbCreateEntity.ReceiverSubDistrict != null && uiSpbCreateEntity.ReceiverSubDistrict != "null"))
        //    {
        //        vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.Equals(uiSpbCreateEntity.ReceiverSubDistrict)).ToList();
        //    }

        //    // cari by vCompanyArea
        //    if (uiSpbCreateEntity.isVia == false && vCompanyArea != null)
        //    {
        //        vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.Equals(vCompanyArea.MasterSubDistrictId)).ToList();

        //    }

        //    // by kecamatan
        //    if (uiSpbCreateEntity.isVia == false
        //        && vCompanyArea == null
        //        && (uiSpbCreateEntity.ReceiverSubDistrict != null || uiSpbCreateEntity.ReceiverSubDistrict != "null"))
        //    {
        //        vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.Equals(uiSpbCreateEntity.ReceiverSubDistrict)).ToList();

        //    }

        //    // cari by kota bukan kecamatan. karena user tidak pilih kecamatan
        //    if (uiSpbCreateEntity.isVia == false && vCompanyArea == null
        //        && (uiSpbCreateEntity.ReceiverSubDistrict == null || uiSpbCreateEntity.ReceiverSubDistrict == "null")
        //        && uiSpbCreateEntity.ReceiverCity != null)
        //    {
        //        vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterCityId.Equals(uiSpbCreateEntity.ReceiverCity)).ToList();

        //    }



        //    if (vMasterUrbanEntities != null)
        //    {
        //        rtn.Data = vMasterUrbanEntities;
        //        rtn.Status = StatusCodes.Status200OK;
        //    }


        //    //
        //    // return
        //    return rtn;


        //}

        public ReturnFormat GetReceiverUrban(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VMasterUrbanEntity> vMasterUrbanEntities = null;
            VCompanyAreaEntity vCompanyArea = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // get urban 
            if (uiSpbCreateEntity.DestArea != null)
            {
                vCompanyArea = _repoPs.VCompanyAreaRepo.Read(w => w.CompanyAreaId.Equals(Int32.Parse(uiSpbCreateEntity.DestArea))).FirstOrDefault();
            }

            // angularny gk kuat munculin data ribuan di drop down
            //if (uiSpbCreateEntity.isVia == true && (uiSpbCreateEntity.ReceiverSubDistrict == null || uiSpbCreateEntity.ReceiverSubDistrict == "null")) {
            //    String idProvince = vCompanyArea.MasterCityId.Split('.')[0];
            //    vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.StartsWith(idProvince)).ToList();
            //}


            // find if user select area tujuan
            if (uiSpbCreateEntity.isVia == true && (uiSpbCreateEntity.ReceiverSubDistrict != null && uiSpbCreateEntity.ReceiverSubDistrict != "null"))
            {
                vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.Equals(uiSpbCreateEntity.ReceiverSubDistrict)).ToList();
            }

            // cari by vCompanyArea
            if (uiSpbCreateEntity.isVia == false && vCompanyArea != null)
            {
                vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.Equals(vCompanyArea.MasterSubDistrictId)).ToList();

            }

            // by kecamatan
            if (uiSpbCreateEntity.isVia == false
                && vCompanyArea == null
                && (uiSpbCreateEntity.ReceiverSubDistrict != null || uiSpbCreateEntity.ReceiverSubDistrict != "null"))
            {
                vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.Equals(uiSpbCreateEntity.ReceiverSubDistrict)).ToList();

            }

            // cari by kota bukan kecamatan. karena user tidak pilih kecamatan
            if (uiSpbCreateEntity.isVia == false && vCompanyArea == null
                && (uiSpbCreateEntity.ReceiverSubDistrict == null || uiSpbCreateEntity.ReceiverSubDistrict == "null")
                && uiSpbCreateEntity.ReceiverCity != null)
            {
                vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterCityId.Equals(uiSpbCreateEntity.ReceiverCity)).ToList();

            }



            if (vMasterUrbanEntities != null)
            {
                rtn.Data = vMasterUrbanEntities;
                rtn.Status = StatusCodes.Status200OK;
            }


            //
            // return
            return rtn;


        }

        public ReturnFormat GetReceiverPostalCode(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VMasterUrbanEntity> vMasterUrbanEntities = null;
            VCompanyAreaEntity vCompanyArea = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // get postal code
            if (uiSpbCreateEntity.DestArea != null)
            {
                vCompanyArea = _repoPs.VCompanyAreaRepo.Read(w => w.CompanyAreaId.Equals(Int32.Parse(uiSpbCreateEntity.DestArea))).FirstOrDefault();
            }


            //if (uiSpbCreateEntity.isVia == true)
            if (uiSpbCreateEntity.isVia == true && (uiSpbCreateEntity.ReceiverSubDistrict != null && uiSpbCreateEntity.ReceiverSubDistrict != "null"))
            {
                vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.Equals(uiSpbCreateEntity.ReceiverSubDistrict)).ToList();
            }


            //if (uiSpbCreateEntity.isVia == false)
            if (uiSpbCreateEntity.isVia == false && vCompanyArea != null)
            {
                vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.Equals(vCompanyArea.MasterSubDistrictId)).ToList();
            }



            // by kecamatan
            if (uiSpbCreateEntity.isVia == false
                && vCompanyArea == null
                && (uiSpbCreateEntity.ReceiverSubDistrict != null || uiSpbCreateEntity.ReceiverSubDistrict != "null"))
            {
                vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterSubDistrictId.Equals(uiSpbCreateEntity.ReceiverSubDistrict)).ToList();

            }

            // cari by kota bukan kecamatan. karena user tidak pilih kecamatan
            if (uiSpbCreateEntity.isVia == false && vCompanyArea == null
                && (uiSpbCreateEntity.ReceiverSubDistrict == null || uiSpbCreateEntity.ReceiverSubDistrict == "null")
                && uiSpbCreateEntity.ReceiverCity != null)
            {
                vMasterUrbanEntities = _repoPs.VMasterUrbanRepo.Read(w => w.MasterCityId.Equals(uiSpbCreateEntity.ReceiverCity)).ToList();

            }





            if (vMasterUrbanEntities != null)
            {
                rtn.Data = vMasterUrbanEntities;
                rtn.Status = StatusCodes.Status200OK;
            }


            //
            // return
            return rtn;


        }

        public ReturnFormat GetReceiverSubDistrict(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VMasterSubDistrictEntity> vMasterSubDistrictEntities = null;
            VCompanyAreaEntity vCompanyArea = null;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // saat hanya tau kode pos atau kelurahan
            if (uiSpbCreateEntity.ReceiverPostalCode != "null" && uiSpbCreateEntity.ReceiverPostalCode != null)
            {

                var tmp = uiSpbCreateEntity.ReceiverPostalCode.Split('.');
                String subDistrictId = String.Join('.', tmp[0], tmp[1], tmp[2]);

                vMasterSubDistrictEntities = _repoPs.VMasterSubDistrictRepo.Read(w => w.MasterSubDistrictId.Equals(subDistrictId)).ToList();
               
                rtn.Data = vMasterSubDistrictEntities;
                rtn.Status = StatusCodes.Status200OK;
                return rtn;
            }

            // get sub district
            if (uiSpbCreateEntity.DestArea != null)
            {
                vCompanyArea = _repoPs.VCompanyAreaRepo.Read(w => w.CompanyAreaId.Equals(Int32.Parse(uiSpbCreateEntity.DestArea))).FirstOrDefault();

            }

            // isVia = true
            if (vCompanyArea != null && uiSpbCreateEntity.isVia == true)
            {
                vMasterSubDistrictEntities = _repoPs.VMasterSubDistrictRepo.Read(w => w.MasterCityId.Equals(uiSpbCreateEntity.ReceiverCity)).ToList();
            }

            // isVia = false
            if (vCompanyArea != null && uiSpbCreateEntity.isVia == false)
            {

                vMasterSubDistrictEntities = _repoPs.VMasterSubDistrictRepo.Read(w => w.MasterSubDistrictId.Equals(vCompanyArea.MasterSubDistrictId)).ToList();

                // find parent. digunakan saat pemilihan area bukan kecamatan
                if (vMasterSubDistrictEntities.Count == 1)
                {
                    if (vMasterSubDistrictEntities[0].ParentId != null)
                    {
                        vMasterSubDistrictEntities = _repoPs.VMasterSubDistrictRepo.Read(w => w.MasterSubDistrictId.Equals(vMasterSubDistrictEntities[0].ParentId)).ToList();
                    }
                }

            }
            else if (uiSpbCreateEntity.isVia == false && uiSpbCreateEntity.ReceiverSubDistrict != "undefined") // pencarian sub distik by ketik
            {
                vMasterSubDistrictEntities = _repoPs.VMasterSubDistrictRepo.Read(w => w.MasterSubDistrictName.Contains(uiSpbCreateEntity.ReceiverSubDistrict)).ToList();
            }
            else if (uiSpbCreateEntity.isVia == false && uiSpbCreateEntity.ReceiverCity != null)
            {
                vMasterSubDistrictEntities = _repoPs.VMasterSubDistrictRepo.Read(w => w.MasterCityId.Equals(uiSpbCreateEntity.ReceiverCity)).ToList();
            }


            if (vMasterSubDistrictEntities != null)
            {
                rtn.Data = vMasterSubDistrictEntities;
                rtn.Status = StatusCodes.Status200OK;
            }


            //
            // return
            return rtn;


        }

        public ReturnFormat GetReceiverCity(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VMasterCityEntity> vMasterCityEntities;
            VCompanyCityEntity vCompanyCity;
            String provinceIdOrCityId = "";

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;


            //
            // logic

            // get city ID
            vCompanyCity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(Int32.Parse(uiSpbCreateEntity.DestCity))).FirstOrDefault();
            provinceIdOrCityId = (uiSpbCreateEntity.isVia == true) ? vCompanyCity.CityId.Split('.')[0] : vCompanyCity.CityId;
            vMasterCityEntities = _repoPs.VMasterCityRepo.Read(w => w.MasterCityId.Equals(provinceIdOrCityId)).ToList();

            // get city 

            //// isVia = true
            //if (uiSpbCreateEntity.isVia == true && vCompanyCity != null) {

            //    // get province id exp. 11
            //    provinceId = vCompanyCity.CityId.Split('.')[0];
            //    // get data
            //    vMasterCityEntities = _repoPs.VMasterCityRepo.Read(w => w.MasterCityId.StartsWith(provinceId)).ToList();
            //}

            //// isVia = false
            //if (uiSpbCreateEntity.isVia == false)
            //{
            //    if (vCompanyCity != null)
            //    {
            //        if (uiSpbCreateEntity.ReceiverCity == "null" || uiSpbCreateEntity.ReceiverCity == null)
            //        {
            //            vMasterCityEntities = _repoPs.VMasterCityRepo.Read(w => w.MasterCityId.Equals(vCompanyCity.CityId)).ToList();
            //        }
            //        else
            //        {
            //            vMasterCityEntities = _repoPs.VMasterCityRepo.Read(w => w.MasterCityName.Contains(uiSpbCreateEntity.ReceiverCity) && w.MasterCityId.Equals(vCompanyCity.CityId)).ToList();
            //        }
            //    }
            //    else
            //    {
            //        vMasterCityEntities = _repoPs.VMasterCityRepo.Read(w => w.MasterCityName.Contains(uiSpbCreateEntity.ReceiverCity)).ToList();
            //    }

            //}

            if (vMasterCityEntities != null)
            {
                rtn.Data = vMasterCityEntities;
                rtn.Status = StatusCodes.Status200OK;
            }


            //
            // return
            return rtn;


        }


        public SpbEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public SpbEntity Read(int id)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Update(SpbEntity entity)
        {
            throw new NotImplementedException();
        }

        //public ReturnFormat ReadSenderList(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow)
        //{



        //    //
        //    // variable
        //    ReturnFormat rtn = new ReturnFormat();
        //    List<VCustomerV2Entity> vCustomerV2Entities;
        //    List<string[]> filterItems = new List<string[]>();
        //    VCompanyCityEntity vCompanyCityEntity = new VCompanyCityEntity();
        //    Int32 subCityCode = 0;

        //    //
        //    // set variable
        //    rtn.Status = StatusCodes.Status204NoContent;
        //    totalRow = 0;
        //    if (uiSpbCreateEntity.Filter != null
        //        && uiSpbCreateEntity.Filter.Contains("[["))
        //    {
        //        filterItems = JsonConvert.DeserializeObject<List<string[]>>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ","));

        //    }

        //    if (uiSpbCreateEntity.Filter != null
        //        && !uiSpbCreateEntity.Filter.Contains("[["))
        //    {
        //        filterItems.Add(JsonConvert.DeserializeObject<string[]>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ",")));

        //    }




        //    //
        //    // logic

        //    // find city code
        //    vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(uiSpbCreateEntity.WorkCityId)).FirstOrDefault();

        //    // set var
        //    Int32.TryParse(vCompanyCityEntity.CityId.Substring(0, 2), out subCityCode);


        //    //vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(filterItems, subCityCode)
        //    //    .Skip(uiSpbCreateEntity.Skip).Take(uiSpbCreateEntity.Take).ToList()
        //    //    .OrderBy(o => o.Phone).ThenBy(x => x.CustomerName).ThenBy(y => y.CustomerPlace).ThenBy(z => z.CustomerStore).ThenBy(zz => zz.CustomerAddress)
        //    //    .ToList();



        //    vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, vCompanyCityEntity.CityId)
        //        .Skip(uiSpbCreateEntity.Skip).Take(uiSpbCreateEntity.Take).ToList();
        //    // .OrderBy(o => o.Phone).ThenBy(x => x.CustomerName).ThenBy(y => y.CustomerPlace).ThenBy(z => z.CustomerStore).ThenBy(zz => zz.CustomerAddress)
        //    //.ToList();
        //    if (vCustomerV2Entities != null)
        //    {
        //        rtn.Data = vCustomerV2Entities;
        //        rtn.Status = StatusCodes.Status200OK;
        //    }

        //    // count total
        //    if (uiSpbCreateEntity.RequireTotalCount == true)
        //    {
        //        //totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(filterItems, subCityCode).Count();
        //        totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, vCompanyCityEntity.CityId)
        //                    .Count();
        //    }



        //    //
        //    // return
        //    return rtn;


        //}

        public ReturnFormat ReadSenderList2(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow)
        {



            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VCustomerV2Entity> vCustomerV2Entities;
            List<string[]> filterItems = new List<string[]>();
            VCompanyCityEntity vCompanyCityEntity = new VCompanyCityEntity();
            Int32 subCityCode = 0;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;
            totalRow = 0;
            if (uiSpbCreateEntity.Filter != null
                && uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems = JsonConvert.DeserializeObject<List<string[]>>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ","));

            }

            if (uiSpbCreateEntity.Filter != null
                && !uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems.Add(JsonConvert.DeserializeObject<string[]>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ",")));

            }




            //
            // logic

            // find city code
            vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(uiSpbCreateEntity.WorkCityId)).FirstOrDefault();

            // set var
            Int32.TryParse(vCompanyCityEntity.CityId.Substring(0, 2), out subCityCode);


            //vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(filterItems, subCityCode)
            //    .Skip(uiSpbCreateEntity.Skip).Take(uiSpbCreateEntity.Take).ToList()
            //    .OrderBy(o => o.Phone).ThenBy(x => x.CustomerName).ThenBy(y => y.CustomerPlace).ThenBy(z => z.CustomerStore).ThenBy(zz => zz.CustomerAddress)
            //    .ToList();


            vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId).ToList();

            //vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, vCompanyCityEntity.CityId)
            //    .Skip(uiSpbCreateEntity.Skip).Take(uiSpbCreateEntity.Take).ToList();
            // .OrderBy(o => o.Phone).ThenBy(x => x.CustomerName).ThenBy(y => y.CustomerPlace).ThenBy(z => z.CustomerStore).ThenBy(zz => zz.CustomerAddress)
            //.ToList();
            if (vCustomerV2Entities != null)
            {
                rtn.Data = vCustomerV2Entities;
                rtn.Status = StatusCodes.Status200OK;
            }

            // count total
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                //totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(filterItems, subCityCode).Count();
                totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId)
                            .Count();
            }



            //
            // return
            return rtn;


        }

        public ReturnFormat ReadSenderTelpList(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VCustomerV2Entity> vCustomerV2Entities;
            List<string[]> filterItems = new List<string[]>();
            VCompanyCityEntity vCompanyCityEntity = new VCompanyCityEntity();
            Int32 subCityCode = 0;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;
            totalRow = 0;
            if (uiSpbCreateEntity.Filter != null
                && uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems = JsonConvert.DeserializeObject<List<string[]>>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ","));

            }

            if (uiSpbCreateEntity.Filter != null
                && !uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems.Add(JsonConvert.DeserializeObject<string[]>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ",")));

            }

            //
            // logic

            // find city code
            vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(uiSpbCreateEntity.WorkCityId)).FirstOrDefault();

            // set var
            Int32.TryParse(vCompanyCityEntity.CityId.Substring(0, 2), out subCityCode);
            
            vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Telp(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.SenderTelp, vCompanyCityEntity.CityId).ToList();

            if (vCustomerV2Entities != null)
            {
                rtn.Data = vCustomerV2Entities;
                rtn.Status = StatusCodes.Status200OK;
            }

            // count total
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Telp(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.SenderTelp, vCompanyCityEntity.CityId).Count();
            }

            //
            // return
            return rtn;


        }

        public ReturnFormat ReadSenderNameList(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow)
        {
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VCustomerV2Entity> vCustomerV2Entities;
            List<string[]> filterItems = new List<string[]>();
            VCompanyCityEntity vCompanyCityEntity = new VCompanyCityEntity();
            Int32 subCityCode = 0;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;
            totalRow = 0;
            if (uiSpbCreateEntity.Filter != null
                && uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems = JsonConvert.DeserializeObject<List<string[]>>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ","));

            }

            if (uiSpbCreateEntity.Filter != null
                && !uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems.Add(JsonConvert.DeserializeObject<string[]>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ",")));

            }




            //
            // logic

            // find city code
            vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(uiSpbCreateEntity.WorkCityId)).FirstOrDefault();

            // set var
            Int32.TryParse(vCompanyCityEntity.CityId.Substring(0, 2), out subCityCode);


            //vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(filterItems, subCityCode)
            //    .Skip(uiSpbCreateEntity.Skip).Take(uiSpbCreateEntity.Take).ToList()
            //    .OrderBy(o => o.Phone).ThenBy(x => x.CustomerName).ThenBy(y => y.CustomerPlace).ThenBy(z => z.CustomerStore).ThenBy(zz => zz.CustomerAddress)
            //    .ToList();

            vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerNameV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.SenderName, vCompanyCityEntity.CityId).ToList();

            //vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, vCompanyCityEntity.CityId)
            //    .Skip(uiSpbCreateEntity.Skip).Take(uiSpbCreateEntity.Take).ToList();
            // .OrderBy(o => o.Phone).ThenBy(x => x.CustomerName).ThenBy(y => y.CustomerPlace).ThenBy(z => z.CustomerStore).ThenBy(zz => zz.CustomerAddress)
            //.ToList();
            if (vCustomerV2Entities != null)
            {
                rtn.Data = vCustomerV2Entities;
                rtn.Status = StatusCodes.Status200OK;
            }

            // count total
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                //totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(filterItems, subCityCode).Count();
                totalRow = _repoPs.CustomerRepo.Read_VCustomerNameV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.SenderName, vCompanyCityEntity.CityId)
                            .Count();
            }

            // return
            return rtn;

        }

        public ReturnFormat ReadReceiverNameList(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow)
        {
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VCustomerV2Entity> vCustomerV2Entities;
            List<string[]> filterItems = new List<string[]>();
            VCompanyCityEntity vCompanyCityEntity = new VCompanyCityEntity();
            Int32 subCityCode = 0;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;
            totalRow = 0;
            if (uiSpbCreateEntity.Filter != null
                && uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems = JsonConvert.DeserializeObject<List<string[]>>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ","));

            }

            if (uiSpbCreateEntity.Filter != null
                && !uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems.Add(JsonConvert.DeserializeObject<string[]>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ",")));

            }




            //
            // logic

            // find city code
            vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(uiSpbCreateEntity.WorkCityId)).FirstOrDefault();

            // set var
            Int32.TryParse(vCompanyCityEntity.CityId.Substring(0, 2), out subCityCode);


            //vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(filterItems, subCityCode)
            //    .Skip(uiSpbCreateEntity.Skip).Take(uiSpbCreateEntity.Take).ToList()
            //    .OrderBy(o => o.Phone).ThenBy(x => x.CustomerName).ThenBy(y => y.CustomerPlace).ThenBy(z => z.CustomerStore).ThenBy(zz => zz.CustomerAddress)
            //    .ToList();

            vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerNameV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.ReceiverName, vCompanyCityEntity.CityId).ToList();

            //vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, vCompanyCityEntity.CityId)
            //    .Skip(uiSpbCreateEntity.Skip).Take(uiSpbCreateEntity.Take).ToList();
            // .OrderBy(o => o.Phone).ThenBy(x => x.CustomerName).ThenBy(y => y.CustomerPlace).ThenBy(z => z.CustomerStore).ThenBy(zz => zz.CustomerAddress)
            //.ToList();
            if (vCustomerV2Entities != null)
            {
                rtn.Data = vCustomerV2Entities;
                rtn.Status = StatusCodes.Status200OK;
            }

            // count total
            if (uiSpbCreateEntity.RequireTotalCount == true)
            {
                //totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(filterItems, subCityCode).Count();
                totalRow = _repoPs.CustomerRepo.Read_VCustomerNameV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.ReceiverName, vCompanyCityEntity.CityId)
                            .Count();
            }

            // return
            return rtn;

        }

        public ReturnFormat ReadSenderIdPel(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow)
        {



            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VCustomerV2Entity> vCustomerV2Entities;
            List<string[]> filterItems = new List<string[]>();
            VCompanyCityEntity vCompanyCityEntity = new VCompanyCityEntity();
            Int32 subCityCode = 0;
            List<VCustomerIdPelangganEntity> VCustomerIdPelangganEntities;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;
            totalRow = 0;
            if (uiSpbCreateEntity.Filter != null
                && uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems = JsonConvert.DeserializeObject<List<string[]>>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ","));
            }

            if (uiSpbCreateEntity.Filter != null
                && !uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems.Add(JsonConvert.DeserializeObject<string[]>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ",")));
            }

            // logic

            // find city code
            vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(uiSpbCreateEntity.WorkCityId)).FirstOrDefault();

            // set var
            Int32.TryParse(vCompanyCityEntity.CityId.Substring(0, 2), out subCityCode);

            if (uiSpbCreateEntity.CustomerCode != null)
            {
                VCustomerIdPelangganEntities = _repoPs.CustomerRepo.Read_VCustomerIdPelangganEntity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.CustomerCode, vCompanyCityEntity.CityId).ToList();
                if (VCustomerIdPelangganEntities != null)
                {
                    rtn.Data = VCustomerIdPelangganEntities;
                    rtn.Status = StatusCodes.Status200OK;
                }

                // count total
                if (uiSpbCreateEntity.RequireTotalCount == true)
                {
                    totalRow = _repoPs.CustomerRepo.Read_VCustomerIdPelangganEntity(uiSpbCreateEntity, filterItems, Convert.ToString(uiSpbCreateEntity.CustomerCode), vCompanyCityEntity.CityId)
                                .Count();
                }
            }
            else
            {
                vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId).ToList();
                if (vCustomerV2Entities != null)
                {
                    rtn.Data = vCustomerV2Entities;
                    rtn.Status = StatusCodes.Status200OK;
                }

                // count total
                if (uiSpbCreateEntity.RequireTotalCount == true)
                {
                    totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId)
                                .Count();
                }

            }

            //vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId).ToList();

            //if (vCustomerV2Entities != null)
            //{
            //    rtn.Data = vCustomerV2Entities;
            //    rtn.Status = StatusCodes.Status200OK;
            //}

            //// count total
            //if (uiSpbCreateEntity.RequireTotalCount == true)
            //{
            //    totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId)
            //                .Count();
            //}

            // return
            return rtn;
        }

        //
        // PRIVATE 

        public ReturnFormat ReadReceiverIdPel(UiUserProfileEntity uiUserProfileEntity, UiSpbCreateEntity uiSpbCreateEntity, out int totalRow)
        {



            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            List<VCustomerV2Entity> vCustomerV2Entities;
            List<string[]> filterItems = new List<string[]>();
            VCompanyCityEntity vCompanyCityEntity = new VCompanyCityEntity();
            Int32 subCityCode = 0;
            List<VCustomerIdPelangganEntity> VCustomerIdPelangganEntities;

            //
            // set variable
            rtn.Status = StatusCodes.Status204NoContent;
            totalRow = 0;
            if (uiSpbCreateEntity.Filter != null
                && uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems = JsonConvert.DeserializeObject<List<string[]>>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ","));
            }

            if (uiSpbCreateEntity.Filter != null
                && !uiSpbCreateEntity.Filter.Contains("[["))
            {
                filterItems.Add(JsonConvert.DeserializeObject<string[]>(uiSpbCreateEntity.Filter.Replace(",\"and\",", ",").Replace(",\"or\",", ",")));
            }

            // logic

            // find city code
            vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(uiSpbCreateEntity.WorkCityId)).FirstOrDefault();

            // set var
            Int32.TryParse(vCompanyCityEntity.CityId.Substring(0, 2), out subCityCode);

            if (uiSpbCreateEntity.CustomerCode != null)
            {
                VCustomerIdPelangganEntities = _repoPs.CustomerRepo.Read_VCustomerIdPelangganEntity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.CustomerCode, vCompanyCityEntity.CityId).ToList();
                if (VCustomerIdPelangganEntities != null)
                {
                    rtn.Data = VCustomerIdPelangganEntities;
                    rtn.Status = StatusCodes.Status200OK;
                }

                // count total
                if (uiSpbCreateEntity.RequireTotalCount == true)
                {
                    totalRow = _repoPs.CustomerRepo.Read_VCustomerIdPelangganEntity(uiSpbCreateEntity, filterItems, Convert.ToString(uiSpbCreateEntity.CustomerCode), vCompanyCityEntity.CityId)
                                .Count();
                }
            }
            else
            {
                vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId).ToList();
                if (vCustomerV2Entities != null)
                {
                    rtn.Data = vCustomerV2Entities;
                    rtn.Status = StatusCodes.Status200OK;
                }

                // count total
                if (uiSpbCreateEntity.RequireTotalCount == true)
                {
                    totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId)
                                .Count();
                }

            }

            //vCustomerV2Entities = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId).ToList();

            //if (vCustomerV2Entities != null)
            //{
            //    rtn.Data = vCustomerV2Entities;
            //    rtn.Status = StatusCodes.Status200OK;
            //}

            //// count total
            //if (uiSpbCreateEntity.RequireTotalCount == true)
            //{
            //    totalRow = _repoPs.CustomerRepo.Read_VCustomerV2Entity(uiSpbCreateEntity, filterItems, uiSpbCreateEntity.Sender, vCompanyCityEntity.CityId)
            //                .Count();
            //}

            // return
            return rtn;
        }


        //
        // PRIVATE 

        private int? SaveCustomerReceiver(UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            CustomerEntity entity;
            VCompanyCityEntity vCompanyCityEntity;


            //
            // logic

            // get city code
            vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(Int32.Parse(uiSpbCreateEntity.DestCity))).FirstOrDefault();

            // check if no exsist on DB
            entity = _repoPs.CustomerRepo
                // Pati 10mei22 (notelp yg sudah ada seharusnya data lainnya bertambah bukan direplace, companyId sementara di bypass krn ada yg isinya null
                .Read(w => w.IsActive != true && w.Phone.Equals(uiSpbCreateEntity.Receiver) && w.CityCode.Equals(vCompanyCityEntity.CityId)).FirstOrDefault();
            //.Read(w => w.CompanyId.Equals(this.uiUserProfileEntity.companyId) && w.Phone.Equals(uiSpbCreateEntity.Receiver) && w.CityCode.Equals(vCompanyCityEntity.CityId)).FirstOrDefault();

            //if (entity != null) { return entity.CustomerId; }
            if (entity != null)
            {
                if (entity.Phone != null)     // Pati 19Jul22
                {
                    return entity.CustomerId;
                }
            }


            // data not exsist then save
            entity = new CustomerEntity();
            entity.CompanyId = this.uiUserProfileEntity.companyId;
            entity.Phone = uiSpbCreateEntity.Receiver;
            entity.Telp = uiSpbCreateEntity.ReceiverTelp;
            entity.CityCode = vCompanyCityEntity.CityId;
            entity.Type = uiSpbCreateEntity.ReceiverType;
            entity.Area = (uiSpbCreateEntity.ReceiverArea == "null") ? null : uiSpbCreateEntity.ReceiverArea;

            _repoPs.CustomerRepo.Create(entity);
            _repoPs.CustomerRepo.Save();

            //
            // return
            return entity.CustomerId;

        }

        private int? SaveCustomer(UiSpbCreateEntity uiSpbCreateEntity)
        {

            //
            // variable
            CustomerEntity entity;
            VCompanyCityEntity vCompanyCityEntity;


            //
            // logic

            // check if no exsist on DB
            //entity = _repoPs.CustomerRepo.Read(w => w.CompanyId.Equals(this.uiUserProfileEntity.companyId) && w.Phone.Equals(uiSpbCreateEntity.Sender)).FirstOrDefault();

            //if (entity != null) { return entity.CustomerId; }

            // get city code
            vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(this.uiUserProfileEntity.workCityId)).FirstOrDefault();

            //Begin Pati 21 Des 21, Di Test 5 Jan 22
            // check if no exsist on DB
            entity = _repoPs.CustomerRepo.Read(w => w.CompanyId.Equals(this.uiUserProfileEntity.companyId) && w.CityCode.Equals(vCompanyCityEntity.CityId) && w.Phone.Equals(uiSpbCreateEntity.Sender) && w.IsActive != true).FirstOrDefault();

            //if (entity != null) { return entity.CustomerId; }
            if (entity != null)
            {
                if (entity.Phone != null)     // Pati 19Jul22
                {
                    return entity.CustomerId;
                }
            }

                //End Pati 21 Des 21 Di Test 5 Jan 22 Ok

                // data not exsist then save
                entity = new CustomerEntity();
                entity.CompanyId = this.uiUserProfileEntity.companyId;
                entity.Phone = uiSpbCreateEntity.Sender;
                entity.Telp = uiSpbCreateEntity.SenderTelp;
                entity.CityCode = vCompanyCityEntity.CityId;
                entity.Type = uiSpbCreateEntity.SenderType;
                entity.Area = (uiSpbCreateEntity.senderArea == "null") ? null : uiSpbCreateEntity.senderArea;

                entity.CreatedAt = DateTime.UtcNow;
                entity.CreatedBy = uiUserProfileEntity.userId.ToString();

                _repoPs.CustomerRepo.Create(entity);
                _repoPs.CustomerRepo.Save();

                //
                // return
                return entity.CustomerId;


        }

        private int? SaveCustomerName(int? customerId, String name)
        {

            //
            // variable
            CustomerNameEntity entity;


            //
            // logic

            if (name == "null" || name == null) { name = null; }

            // check if no exsist on DB
            entity = _repoPs.CustomerNameRepo.Read(w => w.CustomerId.Equals(customerId) && w.CustomerName.Equals(name)).FirstOrDefault();

            if (entity != null) { return entity.CustomerNameId; }

            // data not exsist then save
            entity = new CustomerNameEntity();
            entity.CustomerId = customerId;
            entity.CustomerName = name;

            entity.CreatedAt = DateTime.UtcNow;
            entity.CreatedBy = uiUserProfileEntity.userId.ToString();

            _repoPs.CustomerNameRepo.Create(entity);
            _repoPs.CustomerNameRepo.Save();

            //
            // return
            return entity.CustomerNameId;

        }

        private int? SaveCustomerStore(int? customerId, String store)
        {

            //
            // variable
            CustomerStoreEntity entity;


            //
            // logic
            if (store == "null" || store == null) { store = null; }

            // check if no exsist on DB
            entity = _repoPs.CustomerStoreRepo.Read(w => w.CustomerId.Equals(customerId) && w.CustomerStore.Equals(store)).FirstOrDefault();

            if (entity != null) { return entity.CustomerStoreId; }

            // data not exsist then save
            entity = new CustomerStoreEntity();
            entity.CustomerId = customerId;
            entity.CustomerStore = store;

            entity.CreatedAt = DateTime.UtcNow;
            entity.CreatedBy = uiUserProfileEntity.userId.ToString();

            _repoPs.CustomerStoreRepo.Create(entity);
            _repoPs.CustomerStoreRepo.Save();

            //
            // return
            return entity.CustomerStoreId;

        }

        private int? SaveCustomerPlace(int? customerId, String place)
        {

            //
            // variable
            CustomerPlaceEntity entity;


            //
            // logic
            if (place == "null" || place == null) { place = null; }

            // check if no exsist on DB
            entity = _repoPs.CustomerPlaceRepo.Read(w => w.CustomerId.Equals(customerId) && w.CustomerPlace.Equals(place)).FirstOrDefault();

            if (entity != null) { return entity.CustomerPlaceId; }

            // data not exsist then save
            entity = new CustomerPlaceEntity();
            entity.CustomerId = customerId;
            entity.CustomerPlace = place;

            entity.CreatedAt = DateTime.UtcNow;
            entity.CreatedBy = uiUserProfileEntity.userId.ToString();

            _repoPs.CustomerPlaceRepo.Create(entity);
            _repoPs.CustomerPlaceRepo.Save();

            //
            // return
            return entity.CustomerPlaceId;

        }

        private int? SaveCustomerAddress(int? customerId, String address, int? rt, int? rw,
             String? urban, String? subDistrict, String? city
                , String? postalCode)
        {

            //
            // variable
            CustomerAddressEntity entity;


            //
            // logic
            //if (address == "null") { address = null; }
            if (address == "null" || address == null) { address = null; }

            // check if no exsist on DB
            entity = _repoPs.CustomerAddressRepo.Read(w => w.CustomerId.Equals(customerId) && w.CustomerAddress.Equals(address)).FirstOrDefault();

            if (entity != null) { return entity.CustomerAddressId; }

            // data not exsist then save
            entity = new CustomerAddressEntity
            {
                CustomerId = customerId,
                CustomerAddress = address,
                Rt = rt,
                Rw = rw,
                Urban = (urban == "null") ? null : urban,
                SubDistrict = (subDistrict == "null") ? null : subDistrict,
                City = (city == "null") ? null : city,
                PostalCode = (postalCode == "null") ? null : postalCode
            };

            entity.CreatedAt = DateTime.UtcNow;
            entity.CreatedBy = uiUserProfileEntity.userId.ToString();

            _repoPs.CustomerAddressRepo.Create(entity);
            _repoPs.CustomerAddressRepo.Save();

            //
            // return
            return entity.CustomerAddressId;

        }

        private int? SaveReceiverVia(UiUserProfileEntity uiUserProfile, UiSpbCreateEntity uiSpbCreate, String viaName)
        {

            //
            // variable
            ViaEntity entity;
            VCompanyCityEntity vCompanyCity;


            //
            // logic

            if (uiSpbCreate.receiverVia == null || uiSpbCreate.receiverVia == "null") { return null; }

            // get city ID
            vCompanyCity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(Int32.Parse(uiSpbCreate.DestCity))).FirstOrDefault();

            // check if no exsist on DB
            entity = _repoPs.ViaRepo.Read(w => w.CompanyId.Equals(uiUserProfile.companyId) && w.CityId.Equals(vCompanyCity.CityId) && w.ViaName.ToLower().Equals(uiSpbCreate.receiverVia.Trim().ToLower())).FirstOrDefault();

            if (entity != null) { return entity.ViaId; }


            // data not exsist then save
            entity = new ViaEntity();
            entity.CompanyId = uiUserProfile.companyId;
            entity.CityId = vCompanyCity.CityId;
            entity.ViaName = uiSpbCreate.receiverVia;

            entity.CreatedAt = DateTime.UtcNow;
            entity.CreatedBy = uiUserProfileEntity.userId.ToString();

            _repoPs.ViaRepo.Create(entity);
            _repoPs.ViaRepo.Save();

            //
            // return
            return entity.ViaId;

        }

        private String SetSheetFor(int index, VSpbEntity vSpb)
        {

            //(s.PaymentMethod.ToLower() == "ta") ? "1 - Asli Pelanggan" : "2A - Salinan Pengirim"

            if (index == 0 && vSpb.PaymentMethod.ToLower() == "ta") { return "1 - Asli Pelanggan"; }
            if (index == 0 && vSpb.PaymentMethod.ToLower() == "tt") { return "2A - Salinan Pengirim"; }

            if (index == 1 && vSpb.PaymentMethod.ToLower() == "ta") { return "3 - Gerai"; }
            if (index == 1 && vSpb.PaymentMethod.ToLower() == "tt") { return "3 - Gerai"; }

            return "";
        }

    }

    [Serializable]
    internal class ex : Exception
    {
        public ex()
        {
        }

        public ex(string message) : base(message)
        {
        }

        public ex(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ex(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    public class RatesDetail
    {
        public int? Min { get; }
        public int? Max { get; }
        public decimal? Rates { get; }
        public string Type { get; }

        public RatesDetail(int? min, int? max, decimal? rates, string type)
        {
            Min = min;
            Max = max;
            Rates = rates;
            Type = type;
        }

        public override bool Equals(object obj)
        {
            return obj is RatesDetail other &&
                   Min == other.Min &&
                   Max == other.Max &&
                   Rates == other.Rates &&
                   Type == other.Type;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Min, Max, Rates, Type);
        }
    }

    public class RatesObj
    {
        public decimal? Rates { get; }
        public string RatesType { get; }
        public IEnumerable<RatesDetail> Detail { get; }

        public RatesObj(decimal? rates, string ratesType, IEnumerable<RatesDetail> detail)
        {
            Rates = rates;
            RatesType = ratesType;
            Detail = detail;
        }

        public override bool Equals(object obj)
        {
            return obj is RatesObj other &&
                   Rates == other.Rates &&
                   RatesType == other.RatesType &&
                   EqualityComparer<IEnumerable<RatesDetail>>.Default.Equals(Detail, other.Detail);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Rates, RatesType, Detail);
        }
    }
}

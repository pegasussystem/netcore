﻿using Core.Entity.Base;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Validator.Base
{
    public class MenuValidator : AbstractValidator<MenuEntity>
    {
        public MenuValidator()
        //public UserValidation()
        {
            RuleFor(x => x.MenuName).NotNull().NotEmpty();
            RuleFor(x => x.Route).NotNull().NotEmpty();
            RuleFor(x => x.MenuCode).NotNull().NotEmpty();
        }
    }
}

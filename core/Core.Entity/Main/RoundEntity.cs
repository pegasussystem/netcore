﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class RoundEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? RoundId { get; set; }
        public String? CarrierId { get; set; }
        public Decimal? first_value { get; set; }
        public Decimal? last_value { get; set; }
        public Decimal? final_value { get; set; }
    }
}

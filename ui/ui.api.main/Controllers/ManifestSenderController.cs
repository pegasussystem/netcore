﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Base;
using Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ui.api.main.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ManifestSenderController : ControllerBase
    {

        private readonly IAppWrapperPs _appPs;
        public ManifestSenderController(IAppWrapperPs app)
        {
            _appPs = app;

        }

        ////---------------------------- Sudah tidak dipake di pindahkan ke Controllers -> UseCase -> Manifest -> Sender -------------------------------//
        //[HttpGet("list")]
        //public async Task<IActionResult> List([FromForm]VManifestEntity entity = null)
        //{

        //    //
        //    // variable
        //    ReturnFormat _returnFormat = new ReturnFormat();


        //    //
        //    // logic

        //    // get data Vspb
        //    _returnFormat = _appPs.ManifestSenderApp.List(entity, GetUserClaim());


        //    //
        //    // return
        //    return StatusCode(_returnFormat.Status, _returnFormat);
        //}



        [HttpGet("list-detail")]
        public async Task<IActionResult> ListDetail([FromQuery]VManifestDetailEntity entity = null)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.ManifestSenderApp.ListDetail(entity, GetUserClaim());


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpGet("list-spb")]
        public async Task<IActionResult> ListSpb([FromQuery]VManifestEntity entity = null)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.ManifestSenderApp.ListSpb(entity, GetUserClaim());


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("spb-customer-print")]
        public async Task<IActionResult> SpbCustomer([FromForm]VSpbEntity entity)
        {

            //
            // variable
            ReturnFormat _returnFormat = new ReturnFormat();


            //
            // logic

            // get data Vspb
            _returnFormat = _appPs.ManifestSenderApp.SpbCustomer(GetUserClaim(), entity);


            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        UiUserProfileEntity GetUserClaim() {
            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            // get claim user
            var claims = User.Claims.Where(w => w.Type.Equals("userProfile")).First().Value;
            uiUserProfileEntity = JsonConvert.DeserializeObject<UiUserProfileEntity>(claims);

            return uiUserProfileEntity;
        }
    }
}
﻿using core.app.Common.Interfaces;
using core.helper;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main;
using Core.Entity.Ui;

namespace core.app.UseCases.Base.Commands
{
    public partial class StartTimePageCommand : IRequest<ReturnFormat>
    {
        public Int32? historyLoginId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class StartTimePageCommandHandler : IRequestHandler<StartTimePageCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public StartTimePageCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(StartTimePageCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                //cek key exists di entity
                var entity = await _context.HistoryLoginEntitys.FindAsync(req.historyLoginId);

                if (entity == null)
                {
                    return rtn;
                }

                entity.StartTime = DateTime.UtcNow;
                entity.EndTime   = DateTime.UtcNow;

                _context.HistoryLoginEntitys.Update(entity);
                _context.SaveChanges();

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;

                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class HistoryPrintEntity
    {
        public Int32? HistoryPrintId { get; set; }
        public Int32? UserId { get; set; }
        public Int32? DocId { get; set; }
        public String? TypePrint { get; set; }
        public String? Menu { get; set; }
        public String? MenuAction { get; set; }
        public Boolean? Printed { get; set; }
        public DateTime? PrintDate { get; set; }
        public String? Desc { get; set; }
    }
}

﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_RoleListConfig : IEntityTypeConfiguration<V_RoleListEntity>
    {
        public void Configure(EntityTypeBuilder<V_RoleListEntity> builder)
        {
            builder.HasKey(e => e.RolesId);
            builder.ToTable("V_RoleList");


        }
    }
}

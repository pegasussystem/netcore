﻿using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Configuration.Privilege.Query
{
    public partial class PrivilegeListItemQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class PrivilegeListItemQueryHandler : IRequestHandler<PrivilegeListItemQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public PrivilegeListItemQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(PrivilegeListItemQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.PrivilegeEntitys
                        select new
                        {
                            id      = a.PrivilegeId
                            , name  = a.PrivilegeName
                            , order = a.OrderCode
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.id
                        , s.name
                        , s.order
                    }).OrderBy(o => o.order);
            }

            // return
            return rtn;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main

{
    public class MasterStatistikEntity
    {
        public String? Kab_Kota { get; set; }
        public Int32? Tahun { get; set; }
        public Int32? SPB { get; set; }
        public Decimal? Total { get; set; }
    }
}
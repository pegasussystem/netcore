﻿using System;
using System.Collections.Generic;
using System.Text;

namespace core.app.UseCases.Manifest.Change_KoliNo
{
    public class Change_KoliNo_Input
    {
        public Int64 NextManifestKoliId { get; set; }
        public Int64 ManifestKoliSpbId { get; set; }
    }
}

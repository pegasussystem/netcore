﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_CawFinalSpbEntity
    {
        public Decimal? CawFinalSpb { get; set; }
        public Decimal? TotalPrice { get; set; }
        public Decimal? PpnValue { get; set; }
        public Decimal? DiscountValue { get; set; }

    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.SpbUpdate.Query
{
    public partial class SpbGoodsDataFormQuery : IRequest<ReturnFormat>
    {
        public String? spbNo { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class SpbGoodsDataFormQueryHandler : IRequestHandler<SpbGoodsDataFormQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public SpbGoodsDataFormQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(SpbGoodsDataFormQuery req, CancellationToken cancellationToken)
        {
            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query =
                from mcc in _context.V_SpbGoodsDataFormEntitys
                where mcc.SpbNo.Equals(req.spbNo)

                select new
                {
                    mcc.SpbId
	                , mcc.SpbNo
	                , mcc.SpbGoodsId
	                , mcc.TotalKoli
                    , mcc.TotalAw
                    , mcc.KoliNo
	                , mcc.Aw
	                , mcc.Caw
	                , mcc.Vw
	                , mcc.Length
	                , mcc.Width
	                , mcc.Height
	                , mcc.Price
	                , mcc.Min
	                , mcc.MinRates
	                , mcc.CawFinal
                };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.SpbId
	                    , s.SpbNo
	                    , s.SpbGoodsId
	                    , s.TotalKoli
                        , s.TotalAw
                        , s.KoliNo
	                    , s.Aw
	                    , s.Caw
	                    , s.Vw
	                    , s.Length
	                    , s.Width
	                    , s.Height
	                    , s.Price
	                    , s.Min
	                    , s.MinRates
	                    , s.CawFinal

                    }).OrderBy(o => o.SpbGoodsId);
            }

            //
            // return
            return rtn;
        }
    }
}

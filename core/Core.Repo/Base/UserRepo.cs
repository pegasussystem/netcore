﻿using Core.Entity.Base;
using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Core.Repo.Base
{
    public class UserRepo : Repo<UserEntity>, IUserRepo
    {
        public UserRepo(DbContextPs ctx) : base(ctx)
        {
        }

        IQueryable<VUserWithProfileEntity> IUserRepo.ReadVUserWithProfile(Expression<Func<VUserWithProfileEntity, bool>> expression)
        {
            var read = this._contextPs.Set<VUserWithProfileEntity>().Where(expression);
            return read;
        }
    }
}

﻿using core.app.UseCases.SpbUpdate.Commands;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Spb.SpbUpdate
{
    [Authorize]
    [Route("spb-update")]
    [ApiController]

    public class ActionController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ActionController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("update-spb")]
        public async Task<IActionResult> UpdateSpb([FromBody] UpdateSpbCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();


            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

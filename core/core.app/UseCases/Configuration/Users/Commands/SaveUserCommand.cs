﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Users.Commands
{
    public partial class SaveUserCommand : IRequest<ReturnFormat>
    {
        public String? username { get; set; }
        public String? fullname { get; set; }
        public String? email { get; set; }
        public Int32? unitId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class SaveUserCommandHandler : IRequestHandler<SaveUserCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public SaveUserCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(SaveUserCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.SaveUser(req.username, req.fullname, req.email, req.unitId, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_SaveDataUserEntity> SaveUser(String? username, String? fullname, String? email, Int32? unitId, Int32? loginId)
        {
            String query = $@"EXEC SP_SaveDataUser @username='{username}', @fullname='{fullname}', @email='{email}', @unitId={unitId}, @loginId={loginId} ";
            var res = this._context.SP_SaveDataUserEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

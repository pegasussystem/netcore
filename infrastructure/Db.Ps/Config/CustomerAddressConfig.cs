﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class CustomerAddressConfig : IEntityTypeConfiguration<CustomerAddressEntity>
    {
        public void Configure(EntityTypeBuilder<CustomerAddressEntity> builder)
        {
            builder.HasKey(e => e.CustomerAddressId);
            builder.ToTable("CustomerAddress");


            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

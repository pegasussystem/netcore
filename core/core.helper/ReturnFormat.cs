﻿using System;
using System.Collections.Generic;
using System.Text;

namespace core.helper
{
    public class ReturnFormat
    {
        public Object Data { get; set; }
        public Int32 Status { get; set; }


        public void Status200OK() {
            this.Status = 200;
        }

        public void Status422UnprocessableEntity()
        {
            this.Status = 422;
        }

        public void Status500InternalServerError() {
            this.Status = 500;
        }

    }
}

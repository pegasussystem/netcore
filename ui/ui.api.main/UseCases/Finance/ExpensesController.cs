using System.Threading.Tasks;
using core.app.UseCases.BukuKas.Commands;
using core.app.UseCases.BukuKas.Queries;
using core.app.UseCases.Expenses.Commands;
using core.app.UseCases.Expenses.Queries;
using core.app.UseCases.GeneralLedger.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Finance
{
    [Authorize]
    [Route("expenses")]
    [ApiController]

    public class ExpensesController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ExpensesController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list")]
        public async Task<IActionResult> List([FromQuery] ExpensesListQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);
            

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("expenses-create")]
        public async Task<IActionResult> AddBukuKas([FromBody] ExpensesCreateCommand q)
        {   

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

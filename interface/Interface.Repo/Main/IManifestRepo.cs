﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.Repo.Main
{ 

    public interface IManifestRepo : IRepo<ManifestEntity>
    {
        IEnumerable<SPManifestNumberGeneratorEntity> GetKoliNo(UiUserProfileEntity uiUserProfileEntity, SpbGoodsEntity entity, String carrier);
        IEnumerable<SPManifest_MonitoringEntity> ListManfestMonitoring(int year = 0, int month = 0, int day = 0, int companyId = 0);
        IEnumerable<VManifestWithKoliEntity> VManifest_Ddb_WithTotalKoli(UiUserProfileEntity uiUserProfileEntity, ManifestEntity manifest);
        //IEnumerable<ListGoodsMonitoringReceiverEntity> List_MonitoringGoods_Receiver(int year = 0, int month = 0, int day = 0, string? prevId = "", int? companyId = 0);

        public IEnumerable<V_Manifest_Dbb_WithWeight_Entity> V_Manifest_Dbb_WithWeight(
           UiUserProfileEntity uiUserProfileEntity
           );

    }
 
}

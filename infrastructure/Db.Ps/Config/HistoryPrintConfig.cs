﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
     class HistoryPrintConfig : IEntityTypeConfiguration<HistoryPrintEntity>
    {
        public void Configure(EntityTypeBuilder<HistoryPrintEntity> builder)
        {
            builder.HasKey(e => e.HistoryPrintId);
            builder.ToTable("HistoryPrint");
        }
    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class PrivilegeConfig : IEntityTypeConfiguration<PrivilegeEntity>
    {
        public void Configure(EntityTypeBuilder<PrivilegeEntity> builder)
        {
            builder.HasKey(e => e.PrivilegeId);
            builder.ToTable("Privilege");
        }
    }
}
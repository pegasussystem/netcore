﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Privillege.Queries
{
    public partial class ListPrivilegeMenuQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public Int32? PrivilegeId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListPrivilegeMenuQueryHandler : IRequestHandler<ListPrivilegeMenuQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListPrivilegeMenuQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListPrivilegeMenuQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.PrivilegeActionEntitys
                        join b in _context.MenuActionEntitys on a.MenuActionId equals b.MenuActionId
                        join c in _context.MenuDataEntitys on b.MenuId equals c.MenuId
                        where a.PrivilegeId.Equals(req.PrivilegeId)
                        select new
                        {
                            c.MenuId
                            , c.MenuKey
                            , c.MenuName
                        };

            var query2 = query.Distinct();
            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(
                    s => new
                    {
                        s.MenuId
                        , s.MenuKey
                        , s.MenuName
                    }).OrderBy(o => o.MenuId);
            }

            // return
            return rtn;
        }
    }
}
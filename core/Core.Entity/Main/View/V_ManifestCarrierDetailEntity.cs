﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_ManifestCarrierDetailEntity
    {
        // CostCarrier
        public Int32? ManifestCarrierId { get; set; }
        public String? ManifestCarrierNo { get; set; }
        public Int32? OriginStationId { get; set; }
        public String? OriginStation { get; set; }
        public Int32? VendorOriginId { get; set; }
        public String? OriginVendor { get; set; }
        public String? OriginCityId { get; set; }
        public String? OriginCity { get; set; }
        public Int32? DestinationStationId { get; set; }
        public String? DestinationStation { get; set; }
        public Int32? DestinationVendorId { get; set; }
        public String? DestinationVendor { get; set; }
        public String? DestinationCityId { get; set; }
        public String? DestinationCity { get; set; }
        public String? NoSmu { get; set; }
        public String? FlightDate { get; set; }
        public Int32? ModaId { get; set; }
        public String? ModaName { get; set; }
        public Int32? CarrierId { get; set; }
        public Int32? VendorCarrierId { get; set; }
        public String? vendorCarrier { get; set; }
        public Int32? FirstFlightCodeId { get; set; }
        public String? FirstFlightCode { get; set; }
        public Int32? FirstFlightNoId { get; set; }
        public String? FirstFlightNo { get; set; }
        public Int32? SecondFlightCodeId { get; set; }
        public String? SecondFlightCode { get; set; }
        public Int32? SecondFlightNoId { get; set; }
        public String? SecondFlightNo { get; set; }
        public Int32? ThirdFlightCodeId { get; set; }
        public String? ThirdFlightCode { get; set; }
        public Int32? ThirdFlightNoId { get; set; }
        public String? ThirdFlightNo { get; set; }
        public Int32? FourthFlightCodeId { get; set; }
        public String? FourthFlightCode { get; set; }
        public Int32? FourthFlightNoId { get; set; }
        public String? FourthFlightNo { get; set; }
        public Int32? FirstStationTransitId { get; set; }
        public String? FirstStationTransit { get; set; }
        public Int32? FirstVendorTransitId { get; set; }
        public String? FirstVendorTransit { get; set; } 
        public Int32? SecondStationTransitId { get; set; }
        public String? SecondStationTransit { get; set; }
        public Int32? SecondVendorTransitId { get; set; }
        public String? SecondVendorTransit { get; set; }
        public Int32? ThirdStationTransitId { get; set; }
        public String? ThirdStationTransit { get; set; }
        public Int32? ThirdVendorTransitId { get; set; }
        public String? ThirdVendorTransit { get; set; }
        public String? ReasonKoli { get; set; }
        public String? ReasonManifest { get; set; }
        public String? ReasonSMU { get; set; }
        public Int32? ApprovedById { get; set; }
        public String? ApprovedByPassword { get; set; }
        public String? ApprovedBy { get; set; }
        public String? ApprovedDate { get; set; }
        public Int32? PpnId { get; set; }
        public String? Ppn { get; set; }
        public String? CostSmu { get; set; }
        public String? TotalFinalWeightSMU { get; set; }
        public String? TotalKoliFinal { get; set; }
        public Int32? Revise { get; set; }
    }
}

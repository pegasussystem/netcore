using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using core.app.Common.Interfaces;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.Data.SqlClient;


namespace core.app.UseCases.GeneralLedger.Queries
{
    public partial class ListGeneralLedgerQuery : IRequest<ReturnFormat>
    {
        public String? date { get; set; }
        public int? coaParentId { get; set; }
        public int? month { get; set; }
        public int? year { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }
    public class ListGeneralLedgerQueryHandler : IRequestHandler<ListGeneralLedgerQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListGeneralLedgerQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListGeneralLedgerQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_GeneralLedgerListEntity> entities;

            // DateTime utcDate = req.CreatedAt.Value.AddHours((double)(-1 * req.uiUserProfile.WorkTimeZoneHour)).AddMinutes((double)(req.uiUserProfile.WorkTimeZoneMinute * -1));

            entities = this.GetGeneralLedgerListByMonth(req.month, req.year);

            if (!string.IsNullOrEmpty(req.date) && req.coaParentId != null)
            {
                DateTime targetDate = DateTime.ParseExact(req.date, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                if (entities != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = entities.Where(e => e.createdAt.Day == targetDate.Day
                    && e.createdAt.Month == targetDate.Month
                    && e.createdAt.Year == targetDate.Year
                    && e.coaParentId == req.coaParentId).Select(
                         s => new
                         {
                             s.coaParentId
                         ,
                             s.keterangan
                         ,
                             s.total

                         });
                }

            }
            else
            {
                if (entities != null)
                {
                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = entities.Select(
                         s => new
                         {
                             s.tanggal
                             ,
                             s.coaCode
                             ,
                             s.coaName
                             ,
                             s.coaParentId
                             ,
                             s.keterangan
                             ,
                             s.total
                             ,
                             s.lastDigit
                             ,
                             s.karakterPertama
                             ,
                             s.karakterKedua
                             ,
                             s.karakterKetiga
                             ,
                             s.karakterKeempat
                             ,
                             s.karakterKelima
                             ,
                             s.karakterKeenam
                             ,
                             s.karakterKetujuh
                             ,
                             s.karakterKedelapan
                         });
                }

            }
            // return 
            return rtn;
        }

        public IEnumerable<SP_GeneralLedgerListEntity> GetGeneralLedgerListByMonth(int? month, int? year)
        {
            // Jika bulan atau tahun null, gunakan bulan dan tahun saat ini
            int currentMonth = month ?? DateTime.Now.Month;
            int currentYear = year ?? DateTime.Now.Year;

            // Gunakan interpolasi untuk query
            var res = this._context.SP_GeneralLedgerListEntities
                .FromSqlInterpolated($"EXEC SP_GeneralLedgerList @month = {currentMonth}, @year = {currentYear}")
                .ToList();
            return res;
        }


    }
}
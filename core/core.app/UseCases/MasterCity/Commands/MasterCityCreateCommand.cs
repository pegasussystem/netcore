﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.MasterCity.Commands
    {
        public partial class MasterCityCreateCommand : IRequest<ReturnFormat>
        {
        public String? companyId { get; set; }
        public String? masterCityId { get; set; }
        public Int32?  masterProvinceId { get; set; }
        public String? masterCityType { get; set; }
        public String? masterCityCode { get; set; }
        public String? masterCityName { get; set; }
        public String? masterCityCapital { get; set; }
        
        public UiUserProfileEntity uiUserProfile { get; set; }
        }

        public class MasterCityCreateCommandHandler : IRequestHandler<MasterCityCreateCommand, ReturnFormat>
        {
            private readonly IApplicationDbContext _context;

            public MasterCityCreateCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<ReturnFormat> Handle(MasterCityCreateCommand req, CancellationToken cancellationToken)
            {
                //
                // variable
                ReturnFormat rtn = new ReturnFormat();
                //
                // set variable

                // default status 
                rtn.Status = StatusCodes.Status204NoContent;

                try
                {
                    // Mark as Changed
                    var res = this.MasterCityCreate(req.uiUserProfile.companyId, req.masterCityId, req.masterProvinceId, req.masterCityType, req.masterCityName, req.masterCityCapital, req.masterCityCode, req.uiUserProfile.userId);

                    await _context.SaveChangesAsync(cancellationToken);

                    rtn.Status = StatusCodes.Status200OK;
                    rtn.Data = res;

                    //
                    // return
                    return rtn;

                }
                catch (Exception ex)
                {
                    var err = ex.Message;
                    rtn.Status = StatusCodes.Status204NoContent;
                    rtn.Data = null;
                }
                return rtn;
            }

            public IEnumerable<SP_MasterCityCreateEntity> MasterCityCreate(Int32? companyId, String? masterCityId, Int32? masterProvinceId, String? masterCityType, String? masterCityName, String? masterCityCapital, String? masterCityCode, Int32? userId)
            {
                String query = $@"EXEC SP_MasterCityCreate  @companyId='{companyId}', @cityType = 'city',@masterProvinceId={masterProvinceId}, @masterCityType='{masterCityType}', @masterCityCode='{masterCityCode}', @masterCityName='{masterCityName}', @masterCityCapital='{masterCityCapital}', @userId={userId}";
            var res = this._context.SP_MasterCityCreateEntitys.FromSqlRaw(query).ToList();
                return res;
            }
        }
    }

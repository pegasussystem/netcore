﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit.Queries
{
    public partial class ListTransitReadQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListTransitReadQueryHandler : IRequestHandler<ListTransitReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListTransitReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListTransitReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // --- --- --- --- ---
            var query = from a in _context.FlightTransitRouteEntitys
                        join b in _context.StationEntitys on a.OriginStationId equals b.StationId
                        join c in _context.CarrierEntitys on a.FirstFlightCodeId equals c.CarrierId
                        join d in _context.FlightEntitys on a.FirstFlightId equals d.FlightId
                        join e in _context.StationEntitys on a.StationTransitId equals e.StationId
                        join f in _context.CarrierEntitys on a.LastFlightCodeId equals f.CarrierId
                        join g in _context.FlightEntitys on a.LastFlightId equals g.FlightId 
                        join h in _context.StationEntitys on a.FinalDestinationStationId equals h.StationId
                        join j in _context.VendorTransitEntitys on a.VendorTransitId equals j.VendorTransitId
                        join k in _context.VendorEntitys on j.VendorId equals k.VendorId
                        where c.CarrierId.Equals(d.CarrierId) && g.CarrierId.Equals(f.CarrierId)
                        select new
                        {
                            a.TransitRouteId
                            ,a.SequenceTransit
                            ,k.VendorName
	                        , a.OriginStationId
	                        , OriginStationName = b.StationName
	                        , a.FirstFlightCodeId
	                        , FirstFlightCode = c.CarrierCode + " - " + d.FlightNo
	                        , a.StationTransitId
	                        , TransitStationName = e.StationName
	                        , a.LastFlightCodeId
	                        , LastFlightCode = f.CarrierCode + " - " + g.FlightNo
	                        , a.FinalDestinationStationId
	                        , FinalDestinationStationName = h.StationName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                          s.TransitRouteId
                          ,s.SequenceTransit
                          ,s.VendorName
	                        , s.OriginStationId
	                        , s.OriginStationName
	                        , s.FirstFlightCodeId
	                        , s.FirstFlightCode
	                        , s.StationTransitId
	                        , s.TransitStationName
	                        , s.LastFlightCodeId
	                        , s.LastFlightCode
	                        , s.FinalDestinationStationId
	                        , s.FinalDestinationStationName
                    }).OrderByDescending(o => o.TransitRouteId);
            }

            //
            // return
            return rtn;
        }
    }
}

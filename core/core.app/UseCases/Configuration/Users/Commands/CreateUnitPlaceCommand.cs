﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Configuration.Users.Commands
{
    public partial class CreateUnitPlaceCommand : IRequest<ReturnFormat>
    {
        public String? cityId { get; set; }
        public String? subDistrictId { get; set; }
        public Int32? unitId { get; set; }
        public String? unitPlaceCode { get; set; }
        public String? unitPlaceName { get; set; }
        public String? unitPlaceAddress { get; set; }
        public String? timeZone { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreateUnitPlaceCommandHandler : IRequestHandler<CreateUnitPlaceCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreateUnitPlaceCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreateUnitPlaceCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.SaveUnitPlace(req.cityId, req.subDistrictId, req.unitId, req.unitPlaceCode, req.unitPlaceName, req.unitPlaceAddress, req.timeZone, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_SaveDataUnitEntity> SaveUnitPlace(String? cityId, String? subDistrictId, Int32? unitId, String? unitPlaceCode, String? unitPlaceName, String? unitPlaceAddress, String? timeZone, Int32? loginId)
        {
            String query = $@"EXEC SP_SaveDataUnit @cityId='{cityId}', @subDistrictId='{subDistrictId}', @unitId={unitId}, @unitPlaceCode='{unitPlaceCode}', @unitPlaceName='{unitPlaceName}', @unitPlaceAddress='{unitPlaceAddress}', @timeZone='{timeZone}', @loginId={loginId} ";
            var res = this._context.SP_SaveDataUnitEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Text;

namespace Core.Entity.Main
{
    public class CoaCodeEntity
    {
        
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public Int64 CoaId { get; set; }
        public String CoaCode { get; set; }
        public Int32? CompanyId { get; set; }
        public Int32? BranchId { get; set; }
        public String CoaName { get; set; }
        public String? CoaDescription { get; set; }
        public Int32 CoaParentId { get; set; }
        

    }
}

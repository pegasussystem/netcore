﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Configuration.Privilege.Commands
{
    public partial class UpdateDataCommand : IRequest<ReturnFormat>
    {
        public Int32? privilegeId { get; set; }
        public Int32? userId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class UpdateDataCommandHandler : IRequestHandler<UpdateDataCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdateDataCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdateDataCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var dataItem = _context.PrivilegeUserEntitys.Where(w => w.UserId.Equals(req.userId)).FirstOrDefault();

                dataItem.PrivilegeId = req.privilegeId;
                dataItem.UpdatedBy   = req.uiUserProfile.userId.ToString();
                dataItem.UpdatedAt   = DateTime.Now;

                _context.PrivilegeUserEntitys.Update(dataItem);
                _context.SaveChanges();

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = dataItem;
                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }
    }
}

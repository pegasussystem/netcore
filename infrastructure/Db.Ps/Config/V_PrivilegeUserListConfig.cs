﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_PrivilegeUserListConfig : IEntityTypeConfiguration<V_PrivilegeUserListEntity>
    {
        public void Configure(EntityTypeBuilder<V_PrivilegeUserListEntity> builder)
        {
            builder.HasKey(e => e.UserId);
            builder.ToTable("V_PrivilegeUserList");
        }
    }
}

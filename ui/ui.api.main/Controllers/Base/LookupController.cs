﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Core.Entity.Base;
using Interface.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ui.api.main.Controllers.Base
{
    [Route("[controller]")]
    [ApiController]
    public class LookupController : ControllerBase
    {
        private readonly IAppWrapperPs _appPs;
        public LookupController(IAppWrapperPs app)
        {
            _appPs = app;

        }


        [HttpGet("get")]
        public async Task<IActionResult> Get(String parentKey= "")
        {

            //
            // variable
            LookupEntity _menuEntity = new LookupEntity();
            ReturnFormat _returnFormat = new ReturnFormat();

            //
            // ser variable
            _menuEntity.LookupKeyParent = parentKey;

            //
            // get data
            _returnFormat = _appPs.LookupApp.Read(_menuEntity);

            //
            // return
            return StatusCode(_returnFormat.Status, _returnFormat);
        }



    }
}
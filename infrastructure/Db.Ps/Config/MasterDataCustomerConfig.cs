﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class MasterDataCustomerConfig : IEntityTypeConfiguration<MasterDataCustomerEntity>
    {
        public void Configure(EntityTypeBuilder<MasterDataCustomerEntity> builder)
        {
            builder.HasKey(e => e.CustomerId);
            builder.ToTable("VMasterDataCustomer");
        }
    }
}

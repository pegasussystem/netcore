﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Core.Repo.Main
{
    public class CustomerRepo : Repo<CustomerEntity>, ICustomerRepo
    {
        public CustomerRepo(DbContextPs ctx) : base(ctx)
        {
        }

        public IQueryable<VCustomerV2Entity> Read_VCustomerV2Entity(UiSpbCreateEntity uiSpbCreateEntity, List<string[]> filters, String uiPhone, string uiCity)
        {
            //var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.CityCode.Contains(subCityCode.ToString()));

            //var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.Phone.Equals(uiPhone.ToString()));  // && w.IsActive == null);
            var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.Phone.Contains(uiPhone.ToString()) && w.CityCode == uiCity.ToString() && w.TypeCustomer == uiSpbCreateEntity.TipeCust);

            if (uiSpbCreateEntity.Filter != null   && !uiSpbCreateEntity.Filter.Contains("\"or\""))
            {
                foreach (var filter in filters)
                {
                    switch (filter[0])
                    {
                        case "customerName":
                            if (filter[1] == "=")
                            {
                                filtertable = filtertable.Where(w => w.CustomerName.Equals(filter[2]));
                            }
                            else
                            {
                                filtertable = filtertable.Where(w => w.CustomerName.Contains(filter[2]));
                            }

                            //filtertable = filtertable.Where(w => w.CustomerName.Contains("nabe"));
                            break;

                        case "phone":
                            filtertable = filtertable.Where(w => w.Phone.Contains(filter[2]));
                            break;
                        case "telp":
                            filtertable = filtertable.Where(w => w.Telp.Contains(filter[2]));
                            break;

                        case "customerPlace":
                            filtertable = filtertable.Where(w => w.CustomerPlace.Contains(filter[2]));
                            break;

                        case "customerStore":
                            if (filter[1] == "=")
                            {
                                filtertable = filtertable.Where(w => w.CustomerStore.Equals(filter[2]));
                            }
                            else
                            {
                                filtertable = filtertable.Where(w => w.CustomerStore.Contains(filter[2]));
                            }

                            break;

                        case "masterUrbanPostalCode":
                            filtertable = filtertable.Where(w => w.MasterUrbanPostalCode.Contains(filter[2]));
                            break;

                        case "customerAddress":
                            filtertable = filtertable.Where(w => w.CustomerAddress.Contains(filter[2]));
                            break;

                        case "rt":
                            filtertable = filtertable.Where(w => filter[2].Contains(w.Rt.ToString()));
                            break;

                        case "rw":
                            filtertable = filtertable.Where(w => filter[2].Contains(w.Rw.ToString()));
                            break;

                        case "masterUrbanName":
                            filtertable = filtertable.Where(w => w.MasterUrbanName.Contains(filter[2]));
                            break;

                        case "masterSubDistrictName":
                            filtertable = filtertable.Where(w => w.MasterSubDistrictName.Contains(filter[2]));
                            break;

                        case "masterCityName":
                            filtertable = filtertable.Where(w => w.MasterCityName.Contains(filter[2]));
                            break;

                    }
                }
            }
            else if (filters.Count > 0) { //  penggunaan OR

                filtertable = filtertable.Where(w => w.DataConcat.Contains(filters[0][2])
                    //w.CustomerName.Contains(filters[0][2])
                    //|| w.Phone.Contains(filters[0][2])
                    //|| w.CustomerPlace.Contains(filters[0][2])
                    //|| w.CustomerStore.Contains(filters[0][2])
                    //|| w.MasterUrbanPostalCode.Contains(filters[0][2])
                    //|| w.CustomerAddress.Contains(filters[0][2])
                    //|| filters[0][2].Contains(w.Rt.ToString())
                    //|| filters[0][2].Contains(w.Rw.ToString())
                    //|| w.MasterUrbanName.Contains(filters[0][2])
                    //|| w.MasterSubDistrictName.Contains(filters[0][2])
                    //|| w.MasterCityName.Contains(filters[0][2])
                    );
            }

            

            return filtertable;
        }

        public IQueryable<VCustomerV2Entity> Read_VCustomerV2Telp(UiSpbCreateEntity uiSpbCreateEntity, List<string[]> filters, String uiTelp, string uiCity)
        {
            //var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.CityCode.Contains(subCityCode.ToString()));

            //var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.Phone.Equals(uiPhone.ToString()));  // && w.IsActive == null);
            var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.Telp.Contains(uiTelp.ToString()) && w.CityCode == uiCity.ToString() && w.TypeCustomer == uiSpbCreateEntity.TipeCust);

            if (uiSpbCreateEntity.Filter != null && !uiSpbCreateEntity.Filter.Contains("\"or\""))
            {
                foreach (var filter in filters)
                {
                    switch (filter[0])
                    {
                        case "customerName":
                            if (filter[1] == "=")
                            {
                                filtertable = filtertable.Where(w => w.CustomerName.Equals(filter[2]));
                            }
                            else
                            {
                                filtertable = filtertable.Where(w => w.CustomerName.Contains(filter[2]));
                            }

                            //filtertable = filtertable.Where(w => w.CustomerName.Contains("nabe"));
                            break;

                        case "phone":
                            filtertable = filtertable.Where(w => w.Phone.Contains(filter[2]));
                            break;
                        case "telp":
                            filtertable = filtertable.Where(w => w.Telp.Contains(filter[2]));
                            break;

                        case "customerPlace":
                            filtertable = filtertable.Where(w => w.CustomerPlace.Contains(filter[2]));
                            break;

                        case "customerStore":
                            if (filter[1] == "=")
                            {
                                filtertable = filtertable.Where(w => w.CustomerStore.Equals(filter[2]));
                            }
                            else
                            {
                                filtertable = filtertable.Where(w => w.CustomerStore.Contains(filter[2]));
                            }

                            break;

                        case "masterUrbanPostalCode":
                            filtertable = filtertable.Where(w => w.MasterUrbanPostalCode.Contains(filter[2]));
                            break;

                        case "customerAddress":
                            filtertable = filtertable.Where(w => w.CustomerAddress.Contains(filter[2]));
                            break;

                        case "rt":
                            filtertable = filtertable.Where(w => filter[2].Contains(w.Rt.ToString()));
                            break;

                        case "rw":
                            filtertable = filtertable.Where(w => filter[2].Contains(w.Rw.ToString()));
                            break;

                        case "masterUrbanName":
                            filtertable = filtertable.Where(w => w.MasterUrbanName.Contains(filter[2]));
                            break;

                        case "masterSubDistrictName":
                            filtertable = filtertable.Where(w => w.MasterSubDistrictName.Contains(filter[2]));
                            break;

                        case "masterCityName":
                            filtertable = filtertable.Where(w => w.MasterCityName.Contains(filter[2]));
                            break;

                    }
                }
            }
            else if (filters.Count > 0)
            { //  penggunaan OR

                filtertable = filtertable.Where(w => w.DataConcat.Contains(filters[0][2]));
            }



            return filtertable;
        }

        public IQueryable<VCustomerV2Entity> Read_VCustomerNameV2Entity(UiSpbCreateEntity uiSpbCreateEntity, List<string[]> filters, String uiName, string uiCity)
        {
            //var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.CityCode.Contains(subCityCode.ToString()));

            //var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.Phone.Equals(uiPhone.ToString()));  // && w.IsActive == null);
            var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.CustomerName.Contains(uiName.ToString()) && w.CityCode == uiCity.ToString() && w.TypeCustomer == uiSpbCreateEntity.TipeCust);

            if (uiSpbCreateEntity.Filter != null && !uiSpbCreateEntity.Filter.Contains("\"or\""))
            {
                foreach (var filter in filters)
                {
                    switch (filter[0])
                    {
                        case "customerName":
                            if (filter[1] == "=")
                            {
                                filtertable = filtertable.Where(w => w.CustomerName.Equals(filter[2]));
                            }
                            else
                            {
                                filtertable = filtertable.Where(w => w.CustomerName.Contains(filter[2]));
                            }

                            //filtertable = filtertable.Where(w => w.CustomerName.Contains("nabe"));
                            break;

                        case "phone":
                            filtertable = filtertable.Where(w => w.Phone.Contains(filter[2]));
                            break;

                        case "customerPlace":
                            filtertable = filtertable.Where(w => w.CustomerPlace.Contains(filter[2]));
                            break;

                        case "customerStore":
                            if (filter[1] == "=")
                            {
                                filtertable = filtertable.Where(w => w.CustomerStore.Equals(filter[2]));
                            }
                            else
                            {
                                filtertable = filtertable.Where(w => w.CustomerStore.Contains(filter[2]));
                            }

                            break;

                        case "masterUrbanPostalCode":
                            filtertable = filtertable.Where(w => w.MasterUrbanPostalCode.Contains(filter[2]));
                            break;

                        case "customerAddress":
                            filtertable = filtertable.Where(w => w.CustomerAddress.Contains(filter[2]));
                            break;

                        case "rt":
                            filtertable = filtertable.Where(w => filter[2].Contains(w.Rt.ToString()));
                            break;

                        case "rw":
                            filtertable = filtertable.Where(w => filter[2].Contains(w.Rw.ToString()));
                            break;

                        case "masterUrbanName":
                            filtertable = filtertable.Where(w => w.MasterUrbanName.Contains(filter[2]));
                            break;

                        case "masterSubDistrictName":
                            filtertable = filtertable.Where(w => w.MasterSubDistrictName.Contains(filter[2]));
                            break;

                        case "masterCityName":
                            filtertable = filtertable.Where(w => w.MasterCityName.Contains(filter[2]));
                            break;

                    }
                }
            }
            else if (filters.Count > 0)
            { //  penggunaan OR

                filtertable = filtertable.Where(w => w.DataConcat.Contains(filters[0][2])
                    //w.CustomerName.Contains(filters[0][2])
                    //|| w.Phone.Contains(filters[0][2])
                    //|| w.CustomerPlace.Contains(filters[0][2])
                    //|| w.CustomerStore.Contains(filters[0][2])
                    //|| w.MasterUrbanPostalCode.Contains(filters[0][2])
                    //|| w.CustomerAddress.Contains(filters[0][2])
                    //|| filters[0][2].Contains(w.Rt.ToString())
                    //|| filters[0][2].Contains(w.Rw.ToString())
                    //|| w.MasterUrbanName.Contains(filters[0][2])
                    //|| w.MasterSubDistrictName.Contains(filters[0][2])
                    //|| w.MasterCityName.Contains(filters[0][2])
                    );
            }



            return filtertable;
        }

        public IEnumerable<SP_SaveDataCustomerHeaderEntity> SaveDataCustomer(int? customerId, int? customerNameId, int? customerStoreId, int? customerPlaceId, int? customerAddressId, string customerType, int userId)
        {
            var res = this._contextPs.Set<SP_SaveDataCustomerHeaderEntity>()
                .FromSqlRaw($@"EXEC SP_SaveDataCustomerHeader @customerId={customerId}, @customerNameId={customerNameId}, @customerStoreId={customerStoreId}, @customerPlaceId={customerPlaceId}, @customerAddressId={customerAddressId}, @customerType={customerType}, @userId={userId} ");
            return res;
        }

        public IQueryable<VCustomerIdPelangganEntity> Read_VCustomerIdPelangganEntity(UiSpbCreateEntity uiSpbCreateEntity, List<string[]> filters, String uiCustomerCode, string uiCity)
        {
            //var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.CityCode.Contains(subCityCode.ToString()));

            //var filtertable = this._contextPs.Set<VCustomerV2Entity>().Where(w => w.Phone.Equals("02130036923"));  // && w.IsActive == null);
            var filtertable = this._contextPs.Set<VCustomerIdPelangganEntity>().Where(w => w.CustomerCode.Equals(uiSpbCreateEntity.CustomerCode.ToString())); // && w.CityCode == uiCity.ToString(); // && w.TypeCustomer == uiSpbCreateEntity.TipeCust);

            if (uiSpbCreateEntity.Filter != null && !uiSpbCreateEntity.Filter.Contains("\"or\""))
            {
                foreach (var filter in filters)
                {
                    switch (filter[0])
                    {
                        case "customerName":
                            if (filter[1] == "=")
                            {
                                filtertable = filtertable.Where(w => w.CustomerName.Equals(filter[2]));
                            }
                            else
                            {
                                filtertable = filtertable.Where(w => w.CustomerName.Contains(filter[2]));
                            }

                            //filtertable = filtertable.Where(w => w.CustomerName.Contains("nabe"));
                            break;

                        case "phone":
                            filtertable = filtertable.Where(w => w.Phone.Contains(filter[2]));
                            break;

                        case "customerPlace":
                            filtertable = filtertable.Where(w => w.CustomerPlace.Contains(filter[2]));
                            break;

                        case "customerStore":
                            if (filter[1] == "=")
                            {
                                filtertable = filtertable.Where(w => w.CustomerStore.Equals(filter[2]));
                            }
                            else
                            {
                                filtertable = filtertable.Where(w => w.CustomerStore.Contains(filter[2]));
                            }

                            break;

                        case "masterUrbanPostalCode":
                            filtertable = filtertable.Where(w => w.MasterUrbanPostalCode.Contains(filter[2]));
                            break;

                        case "customerAddress":
                            filtertable = filtertable.Where(w => w.CustomerAddress.Contains(filter[2]));
                            break;

                        case "rt":
                            filtertable = filtertable.Where(w => filter[2].Contains(w.Rt.ToString()));
                            break;

                        case "rw":
                            filtertable = filtertable.Where(w => filter[2].Contains(w.Rw.ToString()));
                            break;

                        case "masterUrbanName":
                            filtertable = filtertable.Where(w => w.MasterUrbanName.Contains(filter[2]));
                            break;

                        case "masterSubDistrictName":
                            filtertable = filtertable.Where(w => w.MasterSubDistrictName.Contains(filter[2]));
                            break;

                        case "masterCityName":
                            filtertable = filtertable.Where(w => w.MasterCityName.Contains(filter[2]));
                            break;

                    }
                }
            }
            else if (filters.Count > 0)
            { //  penggunaan OR

                filtertable = filtertable.Where(w => w.DataConcat.Contains(filters[0][2])
                    //w.CustomerName.Contains(filters[0][2])
                    //|| w.Phone.Contains(filters[0][2])
                    //|| w.CustomerPlace.Contains(filters[0][2])
                    //|| w.CustomerStore.Contains(filters[0][2])
                    //|| w.MasterUrbanPostalCode.Contains(filters[0][2])
                    //|| w.CustomerAddress.Contains(filters[0][2])
                    //|| filters[0][2].Contains(w.Rt.ToString())
                    //|| filters[0][2].Contains(w.Rw.ToString())
                    //|| w.MasterUrbanName.Contains(filters[0][2])
                    //|| w.MasterSubDistrictName.Contains(filters[0][2])
                    //|| w.MasterCityName.Contains(filters[0][2])
                    );
            }



            return filtertable;
        }
    }
}

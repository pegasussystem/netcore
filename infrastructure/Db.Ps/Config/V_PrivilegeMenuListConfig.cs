﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_PrivilegeMenuListConfig : IEntityTypeConfiguration<V_PrivilegeMenuListEntity>
    {
        public void Configure(EntityTypeBuilder<V_PrivilegeMenuListEntity> builder)
        {
            builder.HasKey(e => e.MenuId);
            builder.ToTable("V_PrivilegeMenuList");
        }
    }
}

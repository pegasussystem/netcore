﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main
{
    public class ManifestCarrierItemEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }

        public Int32? ManifestCarrierItemId { get; set; }
        public Int32? ManifestCarrierId { get; set; }
        public Int64? ManifestId { get; set; }

        public Decimal? Aw { get; set; }
        public Decimal? Caw { get; set; }
        public Int32? Koli { get; set; }

        public Decimal? CawFinal { get; set; }
        public Int32? KoliFinal { get; set; }
        public Decimal? FinalWeightSMU { get; set; }

        public String? DestinationCityId { get; set; }
        public Int32? VendorDestinationId { get; set; }
    }
}

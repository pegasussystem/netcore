﻿using core.app.UseCases.Master.MasterModa.Command;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Master.MasterModa
{
    [Authorize]
    [Route("master-moda-city")]
    [ApiController]

    public class ActionController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ActionController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("create-moda-city")]
        public async Task<IActionResult> CreatePaymentMethod([FromBody] CreateModaCityCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

        [HttpPost("delete-moda-city")]
        public async Task<IActionResult> DeletePaymentMethod([FromBody] DeleteModaCityCommand q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

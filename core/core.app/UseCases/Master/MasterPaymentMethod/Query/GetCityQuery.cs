﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using Core.Entity.Ui;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MasterPaymentMethod.Query
{
    public partial class GetCityQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class GetCityQueryHandler : IRequestHandler<GetCityQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetCityQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetCityQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.MasterCityEntitys
                        select new
                        {
                           a.MasterCityId
                           , a.MasterCityCode
                           , a.MasterCityType
                           , a.MasterCityName
                           , a.MasterCityCapital
                           , MasterCityCustom = a.MasterCityCode + " - " + a.MasterCityType + " " + a.MasterCityName + " " + (a.MasterCityCapital != null ? " [" + a.MasterCityCapital + "] " : "")
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.MasterCityId
                        , s.MasterCityCode
                        , s.MasterCityType
                        , s.MasterCityName
                        , s.MasterCityCapital
                        , s.MasterCityCustom
                    }).OrderBy(o => o.MasterCityId);
            }

            // return
            return rtn;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_MonitoringShipmentListEntity
    {
        public Int32? MonitoringShipmentId { get; set; }
        public String? Via { get; set; }
        public String? OriginCityName { get; set; }
        public String? CorporateName { get; set; }
        public String? DestinationCityName { get; set; }
        public String? SttPegasus { get; set; }
        public String? SttLP { get; set; }
        public String? SmuNo { get; set; }
        public Int32? Koli { get; set; }
        public Decimal? Caw { get; set; }
        public String? CarrierCode { get; set; }
        public String? FlightNo { get; set; }
        public DateTime? FlightDate { get; set; }
        public DateTime? ETD { get; set; }
        public DateTime? ETA { get; set; }
        public String? FirstDayAfternoon { get; set; }
        public String? FirstDayMorning { get; set; }
        public String? SecondDayAfternoon { get; set; }
        public String? SecondDayMorning { get; set; }
        public String? ThirdDayAfternoon { get; set; }
        public String? ThirdDayMorning { get; set; }
        public String? FourthDayAfternoon { get; set; }
        public String? FourthDayMorning { get; set; }
        public String? FifthDayAfternoon { get; set; }
        public String? FifthDayMorning { get; set; }
        public String? POD { get; set; }
        public DateTime? PODDate { get; set; }
        public String? Desc { get; set; }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.MasterTransit.Queries
{
    public partial class MasterTransitReadCityTransitQuery : IRequest<ReturnFormat>
    {
        public DateTime? CreatedAt { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MasterTransitReadCityTransitQueryHandler : IRequestHandler<MasterTransitReadCityTransitQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MasterTransitReadCityTransitQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MasterTransitReadCityTransitQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat
            {
                Status = StatusCodes.Status204NoContent
            };

            // --- --- --- --- ---
            var query = from sc in _context.StationCityEntitys
                        join st in _context.StationEntitys on sc.StationId equals st.StationId
                        where sc.Priority.Equals("1")
                        select new
                        {
                           sc.StationId
                            ,st.StationName
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                         s.StationId
                        , s.StationName
                    }).OrderBy(o => o.StationId);
            }

            //
            // return
            return rtn;
        }
    }
}

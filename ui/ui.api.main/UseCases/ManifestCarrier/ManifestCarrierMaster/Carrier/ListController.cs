﻿using core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.Carrier.Query;
using core.helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.ManifestCarrierMaster.Carrier
{
    [Authorize]
    [Route("manifest-carrier-master")]
    [ApiController]
    public class ListController : ApiController
    {
        [HttpGet("manifest-carrier-master-carrier-list")]
        public async Task<IActionResult> ListCarrier([FromQuery] ManifestCarrierMasterCarrierListQuery q)
        {
            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

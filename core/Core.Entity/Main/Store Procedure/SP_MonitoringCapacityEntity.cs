﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_MonitoringCapacityEntity
    {
        public String? CreatedAtLocal { get; set; }
        public String? Moda { get; set; }
        public String? OriginCityCode { get; set; }
        public String? OriginCityName { get; set; }
        public String? Tujuan { get; set; }
        public Decimal? Aw { get; set; }
        public Decimal? Caw { get; set; }
    }
}

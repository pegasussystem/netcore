﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.MonitoringShipment.Query
{
    public partial class MonitoringShipmentReadCarrierQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class MonitoringShipmentReadCarrierQueryHandler : IRequestHandler<MonitoringShipmentReadCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public MonitoringShipmentReadCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(MonitoringShipmentReadCarrierQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.CarrierEntitys

                        select new
                        {

                            a.CarrierId
                            ,a.CarrierCode
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.CarrierId,
                        s.CarrierCode

                    }).OrderBy(o => o.CarrierId);
            }

            // return
            return rtn;
        }
    }
}



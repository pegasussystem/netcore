﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Core.Entity.Base;

namespace Core.Repo.Main
{
    public class ManifestCarrierRepo : Repo<ManifestCarrierEntity>, IManifestCarrierRepo
    {
        public ManifestCarrierRepo(DbContextPs ctx) : base(ctx)
        {

        }

        public IEnumerable<VManifestCarrierEntity> VManifestCarrier(UiUserProfileEntity userProfile)
        {

            var query =
                from mc in _contextPs.Set<ManifestCarrierEntity>()
              //  join c1 in _contextPs.Set<CarrierEntity>() on mc.CarrierId1 equals c1.CarrierId
                //join c2 in _contextPs.Set<CarrierEntity>() on mc.CarrierId2 equals c2.CarrierId into gc2
                //from subc2 in gc2.DefaultIfEmpty()
                //join c3 in _contextPs.Set<CarrierEntity>() on mc.CarrierId3 equals c3.CarrierId into gc3
                //from subc3 in gc3.DefaultIfEmpty()
                //join c4 in _contextPs.Set<CarrierEntity>() on mc.CarrierId4 equals c4.CarrierId into gc4
                //from subc4 in gc4.DefaultIfEmpty()

                //where mc.CompanyId.Equals(userProfile.companyId)

                select new VManifestCarrierEntity
                {
                    //SendDateUtc = mc.SendDate,
                    //SendDateLocal = mc.SendDate.Value.AddHours((double)userProfile.WorkTimeZoneHour).AddMinutes((double)userProfile.WorkTimeZoneMinute),
                    CreatedAt = mc.CreatedAt,
                    UpdatedAt = mc.UpdatedAt,
                    CreatedBy = mc.CreatedBy.ToString(),
                    UpdatedBy = mc.UpdatedBy,
                    ManifestCarrierId = mc.ManifestCarrierId,
                    //CompanyId = mc.CompanyId,
                    //BranchId = mc.BranchId,
                    //SubBranchId = mc.SubBranchId,
                    //CarrierId1 = mc.CarrierId1,
                    //CarrierId2 = mc.CarrierId2,
                    //CarrierId3 = mc.CarrierId3,
                    //CarrierId4 = mc.CarrierId4,
                    //NoSmu = mc.NoSmu,
                    //FinalWeight = mc.FinalWeight,
                    //CarrierName1 = c1.CarrierName,
                    //CarrierName2 = subc2.CarrierName,
                    //CarrierName3 = subc3.CarrierName,
                    //CarrierName4 = subc4.CarrierName,
                    FlightDateUtc = mc.FlightDate,
                    FlightDateLocal = mc.FlightDate.Value.AddHours((double)userProfile.WorkTimeZoneHour).AddMinutes((double)userProfile.WorkTimeZoneMinute),
                };

            if (userProfile.BranchId != null)
            {
                query = query.Where(w => w.BranchId.Equals(userProfile.BranchId));
            }

            return query.ToList();

           
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public IEnumerable<VManifestCarrier_Report_List> ManifestCarrier_Report_List(UiUserProfileEntity userProfile, String flightDate)
        {

            // --- --- ---
            // variable
            // --- --- ---
            DateTime _flightDate = DateTime.Parse(flightDate);
            DateTime _flightDateStart = new DateTime(_flightDate.Year,
                _flightDate.Month,
                _flightDate.Day);

            DateTime _flightDateEnd = new DateTime(_flightDate.Year,
                _flightDate.Month,
                _flightDate.Day, 23, 59, 59);

            // get data from ManifestCarrier
            // --- --- ---
            var query =
                from mci in _contextPs.Set<ManifestCarrierItemEntity>()
                join mc in _contextPs.Set<ManifestCarrierEntity>() on mci.ManifestCarrierId equals mc.ManifestCarrierId
                join mcc in _contextPs.Set<ManifestCarrierCostEntity>() on mci.ManifestCarrierItemId equals mcc.ManifestCarrierItemId

                join m in _contextPs.Set<ManifestEntity>() on mci.ManifestId equals m.ManifestId
                join a in _contextPs.Set<AlphabetEntity>() on m.ManifestAlphabet equals a.AlphabetId

                // origin
                join cco in _contextPs.Set<CompanyCityEntity>() on m.OriginCityCode equals cco.CompanyCityId
                join mco in _contextPs.Set<MasterCityEntity>() on cco.CityId equals mco.MasterCityId

                // destination
                join ccd in _contextPs.Set<CompanyCityEntity>() on m.DestinationCityCode equals ccd.CompanyCityId
                join mcd in _contextPs.Set<MasterCityEntity>() on ccd.CityId equals mcd.MasterCityId

                join lct in _contextPs.Set<LookupEntity>() on mcc.CostType equals lct.LookupKey
                join lcg in _contextPs.Set<LookupEntity>() on mcc.CostGroup equals lcg.LookupKey

                //where mci.BranchId.Equals(userProfile.BranchId)
                where (
                    mc.FlightDate.Value.AddHours((double)userProfile.WorkTimeZoneHour).AddMinutes((double)userProfile.WorkTimeZoneMinute) >= _flightDateStart
                    && mc.FlightDate.Value.AddHours((double)userProfile.WorkTimeZoneHour).AddMinutes((double)userProfile.WorkTimeZoneMinute) <= _flightDateEnd)

                select new VManifestCarrier_Report_List
                {
                    ManifestCarrierCostId = mcc.ManifestCarrierCostId,
                    NoSmu = mc.NoSmu,
                    NoManifest = mcd.MasterCityCode + " " + a.Alphabet + " #" + m.ManifestId.ToString(),

                    CostGroup = lcg.LookupValue,
                    CostType = lct.LookupValue,
                    
                    Origin = mco.MasterCityCode,
                    Destination = mcd.MasterCityCode,

                    FlightDateLocal = mc.FlightDate.Value.AddHours((double)userProfile.WorkTimeZoneHour).AddMinutes((double)userProfile.WorkTimeZoneMinute)
                                    .ToString("yyyy-MMM-dd"),
                    FlightDateUtc = mc.FlightDate,

                    //SendDateLocal = mc.SendDate.Value.AddHours((double)userProfile.WorkTimeZoneHour).AddMinutes((double)userProfile.WorkTimeZoneMinute)
                    //                .ToString("yyyy-MMM-dd"),
                    //SendDateUtc = mc.SendDate
                };


            return query.ToList();


        }

    }
}

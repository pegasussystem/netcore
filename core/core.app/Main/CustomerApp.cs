﻿using core.helper;
using Core.Entity.Base; // Pati 26Agt21
using Newtonsoft.Json;
using Core.Entity.Main;
using Interface.App.Main;
using Interface.Other;
using Interface.Repo;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Core.Entity.Ui;  //pati 08feb22
using MediatR;
using System.IO;
using System.Threading.Tasks;


namespace core.app.Main
{
    public class CustomerApp : ICustomerApp
    {

        private IRepoWarpperPs _repoPs;
        private ILog _log;
        private UiUserProfileEntity uiUserProfileEntity; //pati 08feb22

        public CustomerApp(IRepoWarpperPs repo, ILog log)
        {
            _repoPs = repo;
            _log = log;
        }

        public ReturnFormat Create(CustomerEntity entity, String name, String place, String Store, string address)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            try
            {
                _repoPs.CustomerRepo.Create(entity);
                _repoPs.CustomerRepo.Save();
                CustomerEntity customerEntity = _repoPs.CustomerRepo.Read(w => w.Phone.Contains(entity.Phone)).First();

                if (name != null && name != "")
                {
                    CustomerNameEntity customerNameEntity = new CustomerNameEntity();
                    customerNameEntity.CustomerName = name;
                    customerNameEntity.CustomerId = customerEntity.CustomerId;
                    _repoPs.CustomerNameRepo.Create(customerNameEntity);
                    _repoPs.CustomerNameRepo.Save();
                }
                if (place != null && place != "")
                {
                    CustomerPlaceEntity customerPlaceEntity = new CustomerPlaceEntity();
                    customerPlaceEntity.CustomerPlace = place;
                    customerPlaceEntity.CustomerId = customerEntity.CustomerId;
                    _repoPs.CustomerPlaceRepo.Create(customerPlaceEntity);
                    _repoPs.CustomerPlaceRepo.Save();
                }
                if (Store != null && Store != "")
                {
                    CustomerStoreEntity customerStoreEntity = new CustomerStoreEntity();
                    customerStoreEntity.CustomerStore = Store;
                    customerStoreEntity.CustomerId = customerEntity.CustomerId;
                    _repoPs.CustomerStoreRepo.Create(customerStoreEntity);
                    _repoPs.CustomerStoreRepo.Save();
                }
                if (address != null && address != "")
                {
                    CustomerAddressEntity customerAddressEntity = new CustomerAddressEntity();
                    customerAddressEntity.CustomerAddress = address;
                    customerAddressEntity.CustomerId = customerEntity.CustomerId;
                    _repoPs.CustomerAddressRepo.Create(customerAddressEntity);
                    _repoPs.CustomerAddressRepo.Save();
                }
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = "sukses jeh";
            } catch (Exception e)
            {
                rtn.Status = StatusCodes.Status500InternalServerError;
                rtn.Data = e.Message;
            }
            //
            // return
            return rtn;
        }

        public ReturnFormat Create(CustomerEntity entity)
        {
            throw new NotImplementedException();
        }

        public CustomerEntity Delete(CustomerEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat Read()
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();


            //
            // get data
            rtn.Data = _repoPs.CustomerRepo.Read().Take(100).ToList();
            rtn.Status = StatusCodes.Status200OK;

            //
            // return
            return rtn;
        }

        public ReturnFormat Read(CustomerEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            VCompanyCityEntity vCompanyCityEntity = new VCompanyCityEntity();
            Int32 subCityCode = 0;

            try {

                //
                // find city code
                vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(Int32.Parse(entity.CityCode))).FirstOrDefault();

                //
                // set var
                Int32.TryParse(vCompanyCityEntity.CityId.Substring(0, 2), out subCityCode);


                //
                // get data
                if (entity.Phone != null && entity.Phone != "1")
                {
                    rtn.Data = _repoPs.VCustomerRepo.Read(w => w.CityCode.Contains(subCityCode.ToString()) && w.PhoneCustom.Contains(entity.Phone)).ToList();
                }
                else
                {
                    rtn.Data = _repoPs.VCustomerRepo.Read(w => w.CustomerId.Equals(entity.CustomerId)).ToList().Take(1);
                }
               

                //rtn.Data = _repoPs.VCustomerRepo
                //    .Read(w =>w.PhoneCustom.Contains(entity.Phone)).ToList();

                rtn.Status = StatusCodes.Status200OK;

                //
                // return
                return rtn;
            }

            catch (Exception ex) {

                return rtn;
            }
            
        }

        public CustomerEntity Read(Guid id)
        {
            throw new NotImplementedException();
        }

        public CustomerEntity Read(int id)
        {
            throw new NotImplementedException();
        }

       

        public ReturnFormat ReadPhone(CustomerEntity customerEntity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat ReadName(CustomerNameEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();


            //
            // get data
            if (entity.CustomerId != 0)
            {
                rtn.Data = _repoPs.CustomerNameRepo.Read(w => w.CustomerId.Equals(entity.CustomerId)).ToList();

            }
            else { 
            rtn.Data = _repoPs.CustomerNameRepo.Read(w => w.CustomerNameId.Equals(entity.CustomerNameId)).ToList();

            }
            rtn.Status = StatusCodes.Status200OK;

            //
            // return
            return rtn;
        }

        public ReturnFormat ReadAddress(CustomerAddressEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();


            //
            // get data
            if (entity.CustomerId != 0)
            {
                rtn.Data = _repoPs.VCustomerAddressRepo.Read(w => w.CustomerId.Equals(entity.CustomerId)).ToList();
            }
            else
            {
                rtn.Data = _repoPs.VCustomerAddressRepo.Read(w => w.CustomerAddressId.Equals(entity.CustomerAddressId)).ToList();
            }
            
            rtn.Status = StatusCodes.Status200OK;

            //
            // return
            return rtn;
        }

        public ReturnFormat ReadPlace(CustomerPlaceEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();


            //
            // get data
            if (entity.CustomerId != 0)
            {
                rtn.Data = _repoPs.CustomerPlaceRepo.Read(w => w.CustomerId.Equals(entity.CustomerId)).ToList();

            }
            else
            {
                rtn.Data = _repoPs.CustomerPlaceRepo.Read(w => w.CustomerPlaceId.Equals(entity.CustomerPlaceId)).ToList();

            }
           
            rtn.Status = StatusCodes.Status200OK;

            //
            // return
            return rtn;
        }

        public ReturnFormat ReadStore(CustomerStoreEntity entity)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();


            //
            // get data
            if (entity.CustomerId != 0)
            {
                rtn.Data = _repoPs.CustomerStoreRepo.Read(w => w.CustomerId.Equals(entity.CustomerId)).ToList();

            }
            else
            {
                rtn.Data = _repoPs.CustomerStoreRepo.Read(w => w.CustomerStoreId.Equals(entity.CustomerStoreId)).ToList();

            }
           
            rtn.Status = StatusCodes.Status200OK;

            //
            // return
            return rtn;
        }

        public ReturnFormat Update(CustomerEntity entity)
        {
            throw new NotImplementedException();
        }

        public ReturnFormat CekPhone(CustomerEntity entity)  //Pati 19Agt21
        {
            ReturnFormat rtn = new ReturnFormat();

            //rtn.Data = CheckDataExistsSender(entity.Phone, entity.KotaId);
            //rtn.Status = StatusCodes.Status200OK;

            return rtn;
        }

        public Int32 CheckDataExistsSender(String? phone, String? kotaid)    //Pati 19Agt21
        {
            CustomerEntity entityCheck;
            VCompanyCityEntity vCompanyCityEntity;
            //UiUserProfileEntity uiUserProfileEntity;
            //this.uiUserProfileEntity = uiUserProfileEntity;

            UiUserProfileEntity uiUserProfileEntity = new UiUserProfileEntity();

            //// get claim user
            //var claims = User.Claim.Where(w => w.Type.Equals("userProfile")).First().Value;
            //uiUserProfileEntity = JsonConvert.DeserializeObject<UiUserProfileEntity>(claims);

            //vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(Int32.Parse(entityCheck.CityCode))).FirstOrDefault();

            // get city code
            vCompanyCityEntity = _repoPs.VCompanyCityRepo.Read(w => w.CompanyCityId.Equals(this.uiUserProfileEntity.workCityId)).FirstOrDefault();

            //Begin Pati 21 Des 21, Di Test 5 Jan 22
            // check if no exsist on DB
            entityCheck = _repoPs.CustomerRepo.Read(w => w.CompanyId.Equals(this.uiUserProfileEntity.companyId) && w.CityCode.Equals(vCompanyCityEntity.CityId) && w.Phone.Equals(phone)).FirstOrDefault();


            //entityCheck = _repoPs.CustomerRepo.Read(w => w.Phone.Equals(phone) && w.Phone != null).FirstOrDefault();
            int ada = 0;
            if (entityCheck != null) { ada = (int)entityCheck.CustomerId; }
            else { ada = 0; }
            return ada;
        }

        public ReturnFormat ApprovalCust(UserEntity entity)  //Pati 26Agt21
        {
            ReturnFormat rtn = new ReturnFormat();

            rtn.Data = ApproveCustSPB(entity.Username);
            rtn.Status = StatusCodes.Status200OK;

            return rtn;
        }

        public string ApproveCustSPB(String? username)    //Pati 26Agt21
        {
            VCustomerApprovalEntity custApprovalCheck;
            custApprovalCheck = _repoPs.VCustomerApprovalRepo.Read(w => w.UserName.Equals(username)).FirstOrDefault();

            string adaPassword = string.Empty;
            if (custApprovalCheck == null)
            { adaPassword = string.Empty; } else { adaPassword = custApprovalCheck.Password; }

            return adaPassword;
        }
    }
}

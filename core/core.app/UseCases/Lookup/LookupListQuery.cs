﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Lookup
{
    public partial class LookupListQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
        public String? LookupKeyParent { get; set; }
    }

    public class LookupListQueryHandler : IRequestHandler<LookupListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public LookupListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(LookupListQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                //cek key exists di entity
                //var entity = _context.RatesDetailEntitys.Where(w => w.RatesId.Equals(req.RatesId)).FirstOrDefault();
                //var entity = from a in _context.LookupEntitys.Where(w => w.LookupKeyParent.Equals(req.LookupKeyParent))
                //             select a.LookupKey;

                var entity = _context.LookupEntitys.Where(w => w.LookupKeyParent.Equals(req.LookupKeyParent)).ToList();

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.UpdatePassword.Commands
{
    public partial class UpdateUserPasswordCommand : IRequest<ReturnFormat>
    {
        public String? email { get; set; }
        public String? currentPassword { get; set; }
        public String? newPassword { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class UpdateUserPasswordCommandHandler : IRequestHandler<UpdateUserPasswordCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public UpdateUserPasswordCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(UpdateUserPasswordCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            //
            // logic
            //cek key exists di entity
            var entity = await _context.UserEntitys.FindAsync(req.uiUserProfile.userId);

            if (entity == null)
            {
                return rtn;
            }

            //
            // logic
            entity.Email = req.email;
            if (req.newPassword != null) {
                entity.Password = req.newPassword;
            }

            await _context.SaveChangesAsync(cancellationToken);

            rtn.Status = StatusCodes.Status200OK;
            rtn.Data = entity;

            //
            // return
            return rtn;
        }

    }
}


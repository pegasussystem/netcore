﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_GetDataTotalCustomerDetailEntity
    {
        public Decimal? TotalCAW { get; set; }
        public Int32? TotalKoli { get; set; }
    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class SuperVisionConfig : IEntityTypeConfiguration<SuperVisionEntity>
    {
        public void Configure(EntityTypeBuilder<SuperVisionEntity> builder)
        {
            builder.HasKey(e => e.SuperVisionId);
            builder.ToTable("SuperVision");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

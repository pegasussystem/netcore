﻿using core.app.UseCases.PrintStatus.Query;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.PrintStatus
{
    [Authorize]
    [Route("print-status")]
    [ApiController]

    public class RatesController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public RatesController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpPost("check-status")]
        public async Task<IActionResult> CheckStatusPrint([FromBody] CheckStatusPrintQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfile = GetUserClaim();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

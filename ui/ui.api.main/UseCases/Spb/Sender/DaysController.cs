﻿using core.app.UseCases.Spb.Queries.ListSender;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Spb.Sender
{
    [Authorize]
    [Route("spblistsender")]
    [ApiController]
    public class DaysController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public DaysController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("days")]
        public async Task<IActionResult> List([FromQuery] DaysQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();


            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

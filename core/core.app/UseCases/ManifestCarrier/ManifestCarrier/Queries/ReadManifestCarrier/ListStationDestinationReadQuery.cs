﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries
{
    public partial class ListStationDestinationReadQuery : IRequest<ReturnFormat>
    {
        public Int32? originStationId { get; set; }
    }

    public class ListStationDestinationReadQueryHandler : IRequestHandler<ListStationDestinationReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListStationDestinationReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListStationDestinationReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;
            var query = from fr in _context.FlightRouteEntitys
                        join st in _context.StationEntitys on fr.DestinationStationId equals st.StationId
                        join sc in _context.StationCityEntitys on st.StationId equals sc.StationId
                        join mc in _context.MasterCityEntitys on sc.CityId equals mc.MasterCityId
                        where fr.OriginStationId.Equals(req.originStationId) && sc.Priority.Equals("1")
                        select new
                        {
                            st.StationId
                            , st.StationType
                            , StationName = " [" +mc.MasterCityCode+"] - "+st.StationName
                        };

            //var query = from st in _context.StationEntitys
            //            join vd in _context.VendorDestinationEntitys on st.StationId equals vd.StationId
            //            where vd.VendorId.Equals(req.destinationVendorId)
            //            select new
            //            {
            //                st.StationId
            //                , st.StationType
            //                , st.StationName
            //            };

            var query2 = query.Distinct();
            if (query2 != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query2.Select(
                    s => new
                    {
                        s.StationId
                        , s.StationType
                        , s.StationName
                    }).OrderByDescending(o => o.StationId);
            }

            //
            // return
            return rtn;
        }
    }
}

using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace core.app.UseCases.Rates
{
    public partial class RatesUpdateCommand : IRequest<ReturnFormat>
    {
        public Int32? RatesId { get; set; }
        public Decimal? NewRates { get; set; }
        public Decimal? MinRates { get; set; }
        public Int32? Max { get; set; }
        public Int32? Min { get; set; }
        public String? subDistrict { get; set; }

        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class RatesUpdateCommandHandler : IRequestHandler<RatesUpdateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public RatesUpdateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(RatesUpdateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try{
                //cek key exists di entity
                //var entity = await _context.SpbEntitys.FindAsync(req.SpbId);
                var entity = await _context.RatesEntitys.FindAsync(req.RatesId);
                var rdEntity = _context.RatesDetailEntitys.Where(w=>w.RatesId.Equals(req.RatesId)).FirstOrDefault();

                var ratesSubDistrictEntity = await _context.RatesSubDistrictEntitys.FindAsync(req.RatesId);
                var ratesSubDistrictDetailEntity = _context.RatesSubDistrictDetailEntitys.Where(w => w.RatesSubDistrictId.Equals(req.RatesId)).FirstOrDefault();

                //
                // logic
                if (entity == null && ratesSubDistrictEntity == null)
                {
                    return rtn;
                }

                if (ratesSubDistrictEntity != null)
                {
                    //kecamatan
                    ratesSubDistrictEntity.Rates = req.NewRates;
                    ratesSubDistrictEntity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                    ratesSubDistrictEntity.UpdatedAt = DateTime.UtcNow;

                    if (ratesSubDistrictDetailEntity != null)
                    {
                        ratesSubDistrictDetailEntity.UpdatedBy      = req.uiUserProfileEntity.userId.ToString();
                        ratesSubDistrictDetailEntity.UpdatedAt      = DateTime.UtcNow;
                        ratesSubDistrictDetailEntity.Min            = req.Min;
                        ratesSubDistrictDetailEntity.Max            = req.Max;
                        ratesSubDistrictDetailEntity.Rates          = req.MinRates;

                    }
                    else if (ratesSubDistrictDetailEntity == null)
                    {
                        if (req.MinRates != null && req.Min != null && req.Max != null)
                        {
                            ratesSubDistrictDetailEntity.CreatedBy  = req.uiUserProfileEntity.userId.ToString();
                            ratesSubDistrictDetailEntity.CreatedAt  = DateTime.UtcNow;
                            ratesSubDistrictDetailEntity.Min        = req.Min;
                            ratesSubDistrictDetailEntity.Max        = req.Max;
                            ratesSubDistrictDetailEntity.Rates      = req.MinRates;

                            _context.RatesSubDistrictDetailEntitys.Add(ratesSubDistrictDetailEntity);
                        }
                    }

                    await _context.SaveChangesAsync(cancellationToken);

                    var trf_lama_kec = ratesSubDistrictEntity.Rates;

                    var desc = ratesSubDistrictEntity.OriginCity + " ke " + ratesSubDistrictEntity.DestinationCity + ", tarif lama : " + Convert.ToInt32(trf_lama_kec) + ", tarif baru : " + ratesSubDistrictEntity.Rates;

                    this.SetHistoryAction(req.uiUserProfileEntity.userId, ratesSubDistrictEntity.RatesSubDistrictId, "Update Rates", "Rates", desc);

                }
                else
                {
                    //kota / kab
                    entity.Rates = req.NewRates;
                    entity.UpdatedBy = req.uiUserProfileEntity.userId.ToString();
                    entity.UpdatedAt = DateTime.UtcNow;

                    if (rdEntity != null)
                    {
                        rdEntity.UpdatedBy      = req.uiUserProfileEntity.userId.ToString();
                        rdEntity.UpdatedAt      = DateTime.UtcNow;
                        rdEntity.Min            = req.Min;
                        rdEntity.Max            = req.Max;
                        rdEntity.Rates          = req.MinRates;

                    }
                    else if (rdEntity == null)
                    {
                        if (req.MinRates != null && req.Min != null && req.Max != null)
                        {
                            rdEntity.CreatedBy  = req.uiUserProfileEntity.userId.ToString();
                            rdEntity.CreatedAt  = DateTime.UtcNow;
                            rdEntity.Min        = req.Min;
                            rdEntity.Max        = req.Max;
                            rdEntity.Rates      = req.MinRates;

                            _context.RatesDetailEntitys.Add(rdEntity);
                        }
                    }

                    await _context.SaveChangesAsync(cancellationToken);

                    var trf_lama = entity.Rates;

                    var desc = entity.OriginCity + " ke " + entity.DestinationCity + ", tarif lama : " + Convert.ToInt32(trf_lama) + ", tarif baru : " + entity.Rates;

                    this.SetHistoryAction(req.uiUserProfileEntity.userId, entity.RatesId, "Update Rates", "Rates", desc);

                }

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entity;

                //
                // return
                return rtn;
            }
            catch (Exception ex) {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public void SetHistoryAction(Int32? UserId, long? ActionId, String? ActionDesc, String? Menu, String? Desc)
        {
            // Mark as Changed
            HistoryActionEntity dataItem = new HistoryActionEntity();

            dataItem.UserId = UserId;
            dataItem.ActionDate = DateTime.UtcNow;
            dataItem.ActionId = Convert.ToInt32(ActionId);
            dataItem.ActionDesc = ActionDesc;
            dataItem.Menu = Menu;
            dataItem.Desc = Desc;

            _context.HistoryActionEntitys.Add(dataItem);
            _context.SaveChanges();
        }

    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Core.Entity.Main.Store_Procedure;

namespace core.app.UseCases.Master.MasterPaymentMethod.Command
{
    public partial class CreatePaymentMethodCommand : IRequest<ReturnFormat>
    {
        public String? masterCityId { get; set; }
        public Int32? lookupId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CreatePaymentMethodCommandHandler : IRequestHandler<CreatePaymentMethodCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CreatePaymentMethodCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CreatePaymentMethodCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.ManageData(req.masterCityId, req.lookupId, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_PaymentMethod_CreateEntity> ManageData(String? masterCityId, Int32? lookupId, Int32? userid)
        {
            String query = $@"EXEC SP_PaymentMethod_Create @MasterCityId={masterCityId}, @LookupId='{lookupId}', @UserId={userid} ";
            var res = this._context.SP_PaymentMethod_CreateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
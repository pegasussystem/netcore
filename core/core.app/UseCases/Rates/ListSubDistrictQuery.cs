﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Rates
{
    public partial class ListSubDistrictQuery : IRequest<ReturnFormat>
    {
        public String? cityId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListSubDistrictQueryHandler : IRequestHandler<ListSubDistrictQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListSubDistrictQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListSubDistrictQuery req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            if (req.cityId == "null" || req.cityId == null) { return rtn; }

            var query = from a in _context.MasterSubDistrictEntitys
                        join b in _context.CompanyCityEntitys on a.MasterCityId equals b.CityId
                        join c in _context.CompanyAreaEntitys on a.MasterSubDistrictId equals c.MasterSubDistrictId
                        where b.CompanyCityId.Equals(Int32.Parse(req.cityId))
                        select new
                        {
                            a.MasterCityId
                            , c.CompanyAreaId
                            , a.MasterSubDistrictId
                            , a.MasterSubDistrictCode
                            , a.MasterSubDistrictName
                            , a.MasterSubDistrictUrl
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Distinct()
                    .Select(s => new
                    {
                        s.MasterSubDistrictId,
                        s.MasterCityId,
                        s.MasterSubDistrictCode,
                        s.MasterSubDistrictName,
                        s.MasterSubDistrictUrl,
                        s.CompanyAreaId
                    }).OrderByDescending(o => o.MasterSubDistrictId);
            }

            // return
            return rtn;
        }
       
    }
}

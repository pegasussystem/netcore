﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_ManifestCarrierReviseListConfig : IEntityTypeConfiguration<V_ManifestCarrierReviseListEntity>
    {
        public void Configure(EntityTypeBuilder<V_ManifestCarrierReviseListEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierId);
            builder.ToTable("V_ManifestCarrierReviseList");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MediatR;
using core.helper;
using core.app.Common.Interfaces;
using Interface.Other;
using Interface.Repo;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Configuration.Users.Query
{
    public partial class CheckDetailUserQuery : IRequest<ReturnFormat>
    {
        public Int32? userId { get; set; }
    }

    public class CheckDetailUserQueryHandler : IRequestHandler<CheckDetailUserQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public CheckDetailUserQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(CheckDetailUserQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.V_DataUserEntitys
                        where a.UserId.Equals(req.userId)
                        select new
                        {
                            a.UserId
                            , a.Username
                            , a.Email
                            , a.FullName
                            , a.TableName
                            , a.TableNameId
                            , a.WorkUnit
                            , a.WorkPlace
                            , a.PrivilegeId
                            , a.PrivilegeName
                            , a.RolesId
                            , a.RolesName
                            , a.Description
                            , a.IsActive
                            , a.Status
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.UserId
                        , s.Username
                        , s.Email
                        , s.FullName
                        , s.TableName
                        , s.TableNameId
                        , s.WorkUnit
                        , s.WorkPlace
                        , s.PrivilegeId
                        , s.PrivilegeName
                        , s.RolesId
                        , s.RolesName
                        , s.Description
                        , s.IsActive
                        , s.Status
                    }).OrderByDescending(o => o.UserId);
            }

            // return
            return rtn;
        }

    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class MDataCustomerSenderConfig : IEntityTypeConfiguration<MDataCustomerSenderEntity>
    {
        public void Configure(EntityTypeBuilder<MDataCustomerSenderEntity> builder)
        {
            builder.HasKey(e => e.Id);
            builder.ToTable("VMasterDataCustomerSender");
        }
    }
}

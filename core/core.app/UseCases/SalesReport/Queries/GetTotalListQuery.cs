using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;


namespace core.app.UseCases.SalesReport.Queries
{
    public partial class GetTotalListQuery : IRequest<ReturnFormat>
    {
        public String? startDate { get; set; }
        public String? endDate { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }
    public class GetTotalListQueryHandler : IRequestHandler<GetTotalListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public GetTotalListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(GetTotalListQuery req, CancellationToken cancellationToken)
        {

            // --- variable  
            // --- --- --- --- --- 
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable 
            // --- --- --- --- --- 
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_GetSalesReportOnlyTotalEntity> entities;


            // --- --- ---
            // logic
            // --- --- ---
            entities = this.GetOnlyTotalList(req.startDate, req.endDate);

            if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                     s => new
                     {
                        s.DestinationCityCode,
                        s.OriginCityCode,
                        s.TotalPrice,
                        s.LastCreatedAtManifest,
                        s.Carrier
                     });
            }

            // return 
            return rtn;
        }

        public IEnumerable<SP_GetSalesReportOnlyTotalEntity> GetOnlyTotalList(String? startDate, String? endDate)
        {
            String query = $@"EXEC SP_GetSalesReportOnlyTotal @startDate = '{startDate}', @endDate = '{endDate}'";
            var res = this._context.SP_GetSalesReportOnlyTotalEntities.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
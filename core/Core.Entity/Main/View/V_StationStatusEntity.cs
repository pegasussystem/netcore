﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_StationStatusEntity
    {
        public Int32? StatusId { get; set; }
        public String? StatusName { get; set; }
    }
}

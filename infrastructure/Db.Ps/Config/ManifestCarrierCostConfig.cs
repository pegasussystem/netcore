﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class ManifestCarrierCostConfig : IEntityTypeConfiguration<ManifestCarrierCostEntity>
    {
        public void Configure(EntityTypeBuilder<ManifestCarrierCostEntity> builder)
        {
            builder.HasKey(e => e.ManifestCarrierCostId);
            builder.ToTable("ManifestCarrierCost");

            builder.Property(p => p.CreatedAt)
              .HasDefaultValueSql("getutcdate()");
        }
    }
}

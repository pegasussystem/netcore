﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main;
//using Core.Entity.Main.Core.Entity.Main;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.Spb.Queries.ListSender 
{
    public partial class ApproveCustSenderQuery : IRequest<ReturnFormat>
    {
        public String? UserName { get; set; }
        public String? Password { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ApproveCustSenderQueryHandler : IRequestHandler<ApproveCustSenderQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ApproveCustSenderQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ApproveCustSenderQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            List<VSpbEntity> vSpbEntities = null;
            List<LoginApprovalEntity> LoginApprovalEntities = null;
            DateTime? utcDate = null;

            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;
            //if (req.CreatedAt != null)
            //{
            //    // konversi ke UTC
            //    utcDate = req.CreatedAt.Value.AddHours((double)(-1 * req.uiUserProfileEntity.WorkTimeZoneHour)).AddMinutes((double)(req.uiUserProfileEntity.WorkTimeZoneMinute * -1));
            //}




            // --- logic
            // --- --- --- --- ---


            // from sub branch
            if (req.uiUserProfileEntity.companyId != null && req.uiUserProfileEntity.BranchId != null && req.uiUserProfileEntity.SubBranchId != null)
            {
                vSpbEntities = _context.VSpbEntitys.Where(w => w.CompanyId.Equals(req.uiUserProfileEntity.companyId)
                && w.BranchId.Equals(req.uiUserProfileEntity.BranchId)
                && w.SubBranchId.Equals(req.uiUserProfileEntity.SubBranchId)
                && w.CreatedAt >= utcDate).ToList();
            }

            // from branch
            if (req.uiUserProfileEntity.companyId != null && req.uiUserProfileEntity.BranchId != null && req.uiUserProfileEntity.SubBranchId == null)
            {
                vSpbEntities = _context.VSpbEntitys.Where(w => w.CompanyId.Equals(req.uiUserProfileEntity.companyId)
                && w.BranchId.Equals(req.uiUserProfileEntity.BranchId)
                && w.CreatedAt >= utcDate).ToList();
            }

            // from company
            if (req.uiUserProfileEntity.companyId != null && req.uiUserProfileEntity.BranchId == null && req.uiUserProfileEntity.SubBranchId == null)
            {
                vSpbEntities = _context.VSpbEntitys.Where(w => w.CompanyId.Equals(req.uiUserProfileEntity.companyId)
                && w.CreatedAt >= utcDate).ToList();
            }


            if (vSpbEntities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = vSpbEntities
                    .Select(s => new {
                        s.SpbId,
                        s.SpbNo,
                        s.Username,
                        s.OriginCityNameCustom,
                        s.DestinationCityNameCustom,
                        s.Rates,
                        s.Packing,
                        s.Quarantine,
                        s.Etc,
                        s.Ppn,
                        s.Discount,
                        s.TotalPrice,
                        s.SenderName,
                        s.SenderPhone,
                        s.SenderPlace,
                        s.SenderStore,
                        s.SenderAddress,
                        s.ReceiverName,
                        s.ReceiverPhone,
                        s.ReceiverPlace,
                        s.ReceiverStore,
                        s.ReceiverAddress,
                        s.CarrierName,
                        s.PaymentMethod,
                        s.QualityOfServiceName,
                        s.TypeOfServiceName,
                        s.IsVoid,
                        s.VoidDate,
                        s.VoidBy,
                        s.VoidReason,
                        s.Description,
                        CreatedAtLocal = s.CreatedAt.Value.AddHours((Double)req.uiUserProfileEntity.WorkTimeZoneHour).AddMinutes((Double)req.uiUserProfileEntity.WorkTimeZoneMinute)
                    }).OrderByDescending(o => o.SpbId);
            }

            //
            // return
            return rtn;
        }
    }
}

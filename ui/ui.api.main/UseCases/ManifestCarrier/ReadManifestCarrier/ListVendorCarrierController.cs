﻿using core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrier;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier.ReadManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ListVendorCarrierController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public ListVendorCarrierController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("read-vendor-carrier")]
        public async Task<IActionResult> ListVendorCarrier([FromQuery] ListVendorCarrierReadQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_CawVolumeEntity
    {
        public Decimal? Volume { get; set; }
        public Decimal? CawFinal { get; set; }

    }
}

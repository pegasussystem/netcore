﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_ManifestCarrierItemDetailEntity
    {
        // CostCarrier
        public Int32? ManifestCarrierItemId { get; set; }
        public Int32? ManifestCarrierId { get; set; }
        public Int64? ManifestId { get; set; }
        public String? ManifestNo { get; set; }
        public String? Koli { get; set; }
        public String? Aw { get; set; }
        public String? Caw { get; set; }
        public String? KoliFinal { get; set; }
        public String? CawFinal { get; set; }
        public String? FinalWeightSMU { get; set; }
        public String? DestinationCityId { get; set; }
        public String? MasterCityName { get; set; }
        public Int32? VendorDestinationId { get; set; }
        public String? VendorName { get; set; }
    }
}

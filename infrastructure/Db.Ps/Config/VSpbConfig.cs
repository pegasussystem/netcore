﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VSpbConfig : IEntityTypeConfiguration<VSpbEntity>
    {
        public void Configure(EntityTypeBuilder<VSpbEntity> builder)
        {
            builder.HasKey(e => e.SpbId);
            builder.ToTable("VSpb");
        }
    }
}

﻿using core.app.UseCases.GoodsMonitoring.Queries.ListReceiver;
using core.app.UseCases.Privillege.Queries;
using core.helper;
using Interface.App;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.GoodsMonitoring.Receiver
{
    [Authorize]
    [Route("goods-monitoring-receiver")]
    [ApiController]

    public class ListController : ApiController
    {

        private readonly IAppWrapperPs _appPs;
        public ListController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("list")]
        public async Task<IActionResult> List([FromQuery] ListReceiverQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();

            ListDataDestBlockCityQuery dcity = new ListDataDestBlockCityQuery();

            dcity.UserId = q.uiUserProfileEntity.userId;
            dcity.CityValue = "kota-tujuan";

            var dresponse = await Mediator.Send(dcity).ConfigureAwait(false);
            var dres = dresponse.Data;

            ListDataDestBlockCityQuery bcity = new ListDataDestBlockCityQuery();

            bcity.UserId = q.uiUserProfileEntity.userId;
            bcity.CityValue = "kota-blokir";

            var bresponse = await Mediator.Send(bcity).ConfigureAwait(false);
            var bres = bresponse.Data;

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();
            q.DestinationCityDataValue = dres.ToString();
            q.BlockCityDataValue = bres.ToString();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);

            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

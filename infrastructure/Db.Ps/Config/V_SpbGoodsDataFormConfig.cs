﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class V_SpbGoodsDataFormConfig : IEntityTypeConfiguration<V_SpbGoodsDataFormEntity>
    {
        public void Configure(EntityTypeBuilder<V_SpbGoodsDataFormEntity> builder)
        {
            builder.HasKey(e => e.SpbGoodsId);
            builder.ToView("V_SpbGoodsDataForm");
        }
    }
}

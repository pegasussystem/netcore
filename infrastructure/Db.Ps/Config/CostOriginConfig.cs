﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
   class CostOriginConfig : IEntityTypeConfiguration<CostOriginEntity>
    {
        public void Configure(EntityTypeBuilder<CostOriginEntity> builder)
        {
            builder.HasKey(e => e.CostOriginId);
            builder.ToTable("CostOrigin");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

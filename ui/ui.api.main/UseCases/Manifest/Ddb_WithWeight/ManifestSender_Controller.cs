﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ui.api.main.UseCases.Manifest.Ddb_WithWeight
{
    [Authorize]
    [Route("manifest")]
    [ApiController]
    public class ManifestSender_Controller : DefaultController
    {
        private readonly IAppWrapperPs _appPs;
        public ManifestSender_Controller(IAppWrapperPs app)
        {
            _appPs = app;

        }

        [HttpGet("ddb_WithWeight")]
        public async Task<IActionResult> Ddb_WithWeight()
        {

            // variable
            // ///// ///// /////
            ReturnFormat _returnFormat = new ReturnFormat();

            // logic
            // ///// ///// /////
            _returnFormat = _appPs.Manifest_Ddb_WithWeight_UseCase.Read(GetUserClaim());

            // return
            // ///// ///// /////
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}

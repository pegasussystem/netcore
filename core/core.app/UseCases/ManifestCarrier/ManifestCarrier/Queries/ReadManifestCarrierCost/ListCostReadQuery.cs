﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.View;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace core.app.UseCases.ManifestCarrier.Queries.ReadManifestCarrierCost
{
    public partial class ListCostReadQuery : IRequest<ReturnFormat>
    {
        public Int32? manifestCarrierId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListCostReadQueryHandler : IRequestHandler<ListCostReadQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCostReadQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCostReadQuery req, CancellationToken cancellationToken)
        {

            // --- --- ---
            // variable
            // --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            rtn.Status = StatusCodes.Status204NoContent;

            var query =
                from mcc in _context.V_ManifestCarrierCostEntitys
                where mcc.ManifestCarrierId.Equals(req.manifestCarrierId)

                select new 
                {
                    ManifestCarrierId       = mcc.ManifestCarrierId
                    , ManifestNo            = mcc.ManifestNo
                    , CostType              = mcc.CostType
                    , BaseCost              = mcc.BaseCost
                    , Unit                  = mcc.Unit
                    , CostName              = (mcc.CityCode != null ? mcc.CostName + " " + mcc.CityCode : mcc.CostName) + "   ( " + mcc.UnitValue + " )"
                    //, UnitValue             = mcc.UnitValue
                    , Order                 = mcc.Order
                    , CityCodeDestination   = mcc.CityCodeDestination
                    , Weight                = mcc.FinalWeight
                    , Cost                  = mcc.TotalCost
                };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.ManifestCarrierId
                        , s.ManifestNo
                        , s.CostType
                        , s.BaseCost
                        , s.Unit
                        , s.CostName
                        //, s.UnitValue
                        , s.Order
                        , s.CityCodeDestination
                        , s.Weight
                        , s.Cost
                    }).OrderByDescending(o => o.Order).ThenBy(o => o.CityCodeDestination);
            }

            //
            // return
            return rtn;
        }
    }
}

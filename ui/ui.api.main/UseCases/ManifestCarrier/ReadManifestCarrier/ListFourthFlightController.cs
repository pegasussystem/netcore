﻿using core.app.UseCases.ManifestCarrier.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.ManifestCarrier
{
    [Authorize]
    [Route("manifest-carrier")]
    [ApiController]
    public class ListFourthFlightController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public ListFourthFlightController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("read-fourth-flight")]
        public async Task<IActionResult> List([FromQuery] ListFourthFlightReadQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }
    }
}
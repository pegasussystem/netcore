﻿using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Db.Ps;
using Interface.Repo.Main;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class RatesSubDistrictDetailRepo : Repo<RatesSubDistrictDetailEntity>, IRatesSubDistrictDetailRepo
    {
        public RatesSubDistrictDetailRepo(DbContextPs ctx) : base(ctx)
        {
        }

    }
}

﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class WorkUnitConfig : IEntityTypeConfiguration<WorkUnitEntity>
    {
        public void Configure(EntityTypeBuilder<WorkUnitEntity> builder)
        {
            builder.HasKey(e => e.WorkUnitId);
            builder.ToTable("WorkUnit");
        }
    }
}

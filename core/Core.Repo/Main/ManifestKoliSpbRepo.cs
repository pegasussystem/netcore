﻿using Core.Entity.Main;
using Db.Ps;
using Interface.Repo.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Repo.Main
{
    public class ManifestKoliSpbRepo : Repo<ManifestKoliSpbEntity>, I_ManifestKoliSpb_Repo
    {
        public ManifestKoliSpbRepo(DbContextPs ctx) : base(ctx)
        {
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class V_UserPrivilegeEntity
    {
        public Int32? UserId { get; set; }
        public String? Username { get; set; }
        public String? Email { get; set; }
        public String? TableName { get; set; }
        public Int32? TableNameId { get; set; }
        public Int32? PrivilegeId { get; set; }
        public String? PlaceName { get; set; }
        public String? PlaceAddress { get; set; }
    }
}

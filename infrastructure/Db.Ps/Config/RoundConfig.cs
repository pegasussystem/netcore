﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class RoundConfig : IEntityTypeConfiguration<RoundEntity>
    {
        public void Configure(EntityTypeBuilder<RoundEntity> builder)
        {
            builder.HasKey(e => e.RoundId);
            builder.ToTable("Round");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

﻿using core.app.UseCases.Master.MasterTransit.Queries;
using core.helper;
using Interface.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ui.api.main.Controllers;

namespace ui.api.main.UseCases.Master.MasterTransit.ReadDataOptions
{
    [Authorize]
    [Route("master-transit")]
    [ApiController]
    public class ListDataOptionsController : ApiController
    {
        private readonly IAppWrapperPs _appPs;
        public ListDataOptionsController(IAppWrapperPs app)
        {
            _appPs = app;
        }

        [HttpGet("read-city-transit")]
        public async Task<IActionResult> ListDataCityTransit([FromQuery] ListDataCityTransitReadQuery q)
        {

            // --- variable
            // --- --- --- --- ---
            ReturnFormat _returnFormat = new ReturnFormat();

            // --- set variable
            // --- --- --- --- ---
            q.uiUserProfileEntity = GetUserClaim();


            // --- get data
            // --- --- --- --- ---
            _returnFormat = await Mediator.Send(q).ConfigureAwait(false);


            // --- return
            // --- --- --- --- ---
            return StatusCode(_returnFormat.Status, _returnFormat);
        }

    }
}

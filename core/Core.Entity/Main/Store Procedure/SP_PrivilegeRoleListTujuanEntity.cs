﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.Store_Procedure
{
    public class SP_PrivilegeRoleListTujuanEntity
    {
        public String? RolesCode { get; set; }
        public String? RolesName { get; set; }
        public String? MasterCityId { get; set; }
        public String? MasterCityCode { get; set; }
        public String? MasterCityType { get; set; }
        public String? MasterCityName { get; set; }
        public String? MasterCityCapital { get; set; }
        public Int64? ItemNumber { get; set; }
        public Int64? OrderCode { get; set; }

    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;




    public partial class BukuKasDeleteCommand : IRequest<ReturnFormat>
    {
        public Int64? bukuKasId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class BukuKasDeleteCommandHandler : IRequestHandler<BukuKasDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public BukuKasDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(BukuKasDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;

            try
            {
                // Mark as Changed
                var res = this.BukuKasDelete(req.bukuKasId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;

            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_DeleteBukuKasEntity> BukuKasDelete(Int64? bukuKasId)
        {
            String query = $@"EXEC SP_DeleteBukuKas  @bukuKasId={bukuKasId}";
            var res = this._context.SP_DeleteBukuKasEntities.FromSqlRaw(query).ToList();
            return res;
        }
    }
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Ui
{
    public class UiSpbCreateEntity
    {
        public String? SpbNo { get; set; }
        public String? Tog { get; set; }
        public String? Carrier { get; set; }
        public String? Qos { get; set; }
        public String? Tos { get; set; }
        public String? DestCity { get; set; }
        public String? DestArea { get; set; }
        public Decimal? Rates { get; set; }
        public Decimal? TotalPrice { get; set; }


        public String? SenderType { get; set; }
        public String? senderArea { get; set; }
        public String? Sender { get; set; }
        public String? SenderTelp { get; set; }
        public String? SenderName { get; set; }
        public String? SenderStore { get; set; }
        public String? SenderPlace { get; set; }
        public String? SenderAddress { get; set; }


        public String? ReceiverType { get; set; }
        public String? ReceiverArea { get; set; }
        public String? Receiver { get; set; }
        public String? ReceiverTelp { get; set; }
        public String? ReceiverName { get; set; }
        public String? ReceiverStore { get; set; }
        public String? ReceiverPlace { get; set; }
        public String? ReceiverAddress { get; set; }
        public int? ReceiverRt { get; set; }
        public int? ReceiverRw { get; set; }
        public String? ReceiverUrban { get; set; }
        public String? ReceiverPostalCode { get; set; }
        public String? ReceiverSubDistrict { get; set; }
        public String? ReceiverCity { get; set; }
        public int? receiverViaId { get; set; }
        public String? receiverVia { get; set; }
        public Boolean? isVia { get; set; }



        // datagrid
        public String Filter { get; set; }
        public bool RequireTotalCount { get; set; }
        public Int32 Take { get; set; }
        public Int32 Skip { get; set; }
        public Int32? WorkCityId { get; set; }

        public String TipeCust { get; set; }  //Pati 23Nov21

        public String CustomerCode { get; set; }  //Pati 18Mei22
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.FlightRoute.Command
{
    public partial class FlightRouteDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? flightRouteId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class FlightRouteDeleteCommandHandler : IRequestHandler<FlightRouteDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public FlightRouteDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(FlightRouteDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.FlightDelete(req.flightRouteId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierMaster_FlightRouteDeleteEntity> FlightDelete(Int32? flightRouteId)
        {
            String query = $@"EXEC SP_ManifestCarrierMaster_FlightRouteDelete @flightRouteId='{flightRouteId}'";
            var res = this._context.SP_ManifestCarrierMaster_FlightRouteDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
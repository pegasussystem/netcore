﻿using Core.Entity.Main;
using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Db.Ps.Config
{
    class VSpbGoodsConfig : IEntityTypeConfiguration<VSpbGoodsEntity>
    {
        public void Configure(EntityTypeBuilder<VSpbGoodsEntity> builder)
        {
            builder.HasKey(e => e.SpbGoodsId);
            builder.ToTable("VSpbGoods");
        }
    }
}

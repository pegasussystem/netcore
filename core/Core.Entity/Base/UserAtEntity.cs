﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Entity.Base
{
    public class UserAtEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public String? CreatedBy { get; set; }
        public String? UpdatedBy { get; set; }
        public Int32? UserAtId { get; set; }
        public Int32? UserId { get; set; }
        public Int32? TableNameId { get; set; }
        public String? TableName { get; set; }
        public String? RefreshToken { get; set; }
        public Int32? WorkUnitId { get; set; }
    }
}
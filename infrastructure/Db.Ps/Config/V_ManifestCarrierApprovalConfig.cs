﻿using Core.Entity.Main.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class V_ManifestCarrierApprovalConfig : IEntityTypeConfiguration<V_ManifestCarrierApprovalEntity>
    {
        public void Configure(EntityTypeBuilder<V_ManifestCarrierApprovalEntity> builder)
        {
            builder.HasKey(e => e.PrivilegeId);
            builder.ToTable("V_ManifestCarrierApproval");
        }
    }
}

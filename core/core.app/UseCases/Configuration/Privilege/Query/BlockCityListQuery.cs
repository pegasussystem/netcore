﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Main.Store_Procedure;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.Configuration.Privilege
{
    public partial class BlockCityListQuery : IRequest<ReturnFormat>
    {
        public Int32? rolesId { get; set; }
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class BlockCityListQueryHandler : IRequestHandler<BlockCityListQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public BlockCityListQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(BlockCityListQuery req, CancellationToken cancellationToken)
        {
            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            IEnumerable<SP_PrivilegeRoleListBlockEntity> entities;

            // --- --- ---
            // logic
            // --- --- ---
            entities = this.PrivilegeRoleListBlockListData(req.rolesId);

           if (entities != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = entities.Select(
                    s => new
                    {
                        id = s.MasterCityId
                        , name = s.MasterCityType + " " + s.MasterCityName + ( s.MasterCityCapital == null ? "" : " [ "+ s.MasterCityCapital+" ]")
                        , s.RolesCode
                        , s.RolesName
                        , s.MasterCityCode
                        , s.MasterCityType
                        , s.MasterCityName
                        , s.MasterCityCapital
                        , s.ItemNumber
                        , s.OrderCode
                    });
            }

            // return
            return rtn;
        }

        public IEnumerable<SP_PrivilegeRoleListBlockEntity> PrivilegeRoleListBlockListData(Int32? rolesId)
        {
            String query = $@"EXEC SP_PrivilegeRoleListBlock @rolesId={rolesId}";
            var res = this._context.SP_PrivilegeRoleListBlockEntitys.FromSqlRaw(query).ToList();
            return res;
        }

    }
}

<<<<<<<
USE [Ps_20201201]
GO
/****** Object:  StoredProcedure [dbo].[List_GoodsMonitoring_Receiver]    Script Date: 14/12/2020 09.17.30 ******/
=======
/****** Object:  StoredProcedure [dbo].[SPManifest_Monitoring]    Script Date: 1/7/2021 4:43:49 PM ******/
>>>>>>>
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- [SPManifest_Monitoring] @year=2020, @month=7, @day= 17, @branchConfigKey='via_all', @companyId=1
-- =============================================
ALTER PROCEDURE [dbo].[SPManifest_Monitoring] 
	@companyId INT =0,
	@year INT =0,
	@month INT = 0,
	@day INT = 0
	-- @branchConfigKey Varchar(100)=''

	
AS
BEGIN
	SET NOCOUNT ON;

	/** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  VARIABLE & SET VARIABLE
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */
	 

	 

	 /** 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 *  LOGIC
	 * 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	 */

	 SELECT DISTINCT
		m.ManifestId, m.CompanyId, m.BranchId,
		bo.BranchName 'OriginBranch' ,m.SubBranchId, m.Carrier,
		m.ManifestAlphabet,a.Alphabet, 
		mco.MasterCityId 'OriginCityId' ,mco.MasterCityCode 'OriginCityCode' , mco.MasterCityName 'OriginCityName',
		mcd.MasterCityId 'DestinationCityId', mcd.MasterCityCode 'DestinationCityCode', mcd.MasterCityName 'DestinationCityName',
		COUNT( DISTINCT mk.KoliNo) totalKoli, SUM(sg.Aw) TotalAw, SUM(sg.Caw) TotalCaw, 
		SUM( CASE WHEN s.PaymentMethod='ta' THEN sg.Price ELSE 0 END ) Ta,
		SUM( CASE WHEN s.PaymentMethod='tt' THEN sg.Price ELSE 0 END ) Tt, SUM(sg.Price) TotalTaTt
		--, '#####', , '#####', cco.*, '#####', mco.*
	FROM Manifest m
	LEFT JOIN Alphabet a ON a.AlphabetId = m.ManifestAlphabet
	INNER JOIN CompanyCity cco ON cco.CompanyCityId = m.OriginCityCode
	INNER JOIN MasterCity mco ON mco.MasterCityId = cco.CityId
	INNER JOIN CompanyCity ccd ON ccd.CompanyCityId = m.DestinationCityCode
	INNER JOIN MasterCity mcd ON mcd.MasterCityId = ccd.CityId
	INNER JOIN ManifestKoli mk ON mk.ManifestId = m.ManifestId
	INNER JOIN ManifestKoliSpb mks ON mks.ManifestKoliId = mk.ManifestKoliId
	INNER JOIN SpbGoods sg ON sg.SpbGoodsId = mks.SpbGoodsId
	INNER JOIN Spb s ON s.SpbId = sg.SpbId

	-- company
	LEFT JOIN Branch b ON b.CityHandled like '%<'+mcd.MasterCityId+'>%'
	LEFT JOIN Branch bo ON bo.CityHandled like '%<'+mco.MasterCityId+'>%'

	WHERE -- YEAR(m.CreatedAt) = @year AND MONTH(m.CreatedAt)=@month AND DAY(m.CreatedAt)=@day
		DAY( DATEADD(minute,0,DATEADD(HOUR,7,s.CreatedAt)) )=@day  
		AND MONTH( DATEADD(minute,0,DATEADD(HOUR,7,s.CreatedAt)) )=@month  
		AND YEAR( DATEADD(minute,0,DATEADD(HOUR,7,s.CreatedAt)) )=@year 

		AND m.CompanyId = @companyId
		--AND mcd.MasterCityId IN (
		--	SELECT REPLACE(t.CityId, '<', '') 
		--	FROM (
		--		SELECT value CityId
		--		FROM BranchConfig CROSS APPLY STRING_SPLIT(val, '>')
		--		WHERE [key]=@branchConfigKey
		--	) as t
		--)
		AND s.TotalPrice IS NOT NULL
		AND s.IsVoid IS NULL
	GROUP BY m.ManifestId, m.CompanyId, m.BranchId,b.BranchName, m.SubBranchId, m.Carrier,
		m.ManifestAlphabet,a.Alphabet, 
		mco.MasterCityId, mco.MasterCityCode  , mco.MasterCityName ,
		mcd.MasterCityId, mcd.MasterCityCode , mcd.MasterCityName , bo.BranchName
	ORDER BY m.Carrier,mco.MasterCityCode, mcd.MasterCityCode, m.ManifestAlphabet


END
<<<<<<<

=======

>>>>>>>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Numerics;
using System.Text;

namespace Core.Entity.Main
{
    public class AccountReceivableEntity
    {
        public Int64 arId { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public Int64? coaId { get; set; }
        public Int64? manifestId { get; set; }
        public String? keterangan { get; set; }
        public Int32? paymentStatusId { get; set; }
        public DateTime? paidAt { get; set; }
        public Decimal? debit { get; set; }
        public Decimal? credit { get; set; }
        public Decimal? saldo { get; set; }
         public Int64? spbId { get; set; }
         public Int64? bukuKasId { get; set; }

    }
}

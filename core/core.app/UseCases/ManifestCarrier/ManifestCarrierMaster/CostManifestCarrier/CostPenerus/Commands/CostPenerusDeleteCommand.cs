﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostPenerus.Commands
{
    public partial class CostPenerusDeleteCommand : IRequest<ReturnFormat>
    {
        public Int32? costPenerusDetailId { get; set; }
        public Int32? costPenerusId { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostPenerusDeleteCommandHandler : IRequestHandler<CostPenerusDeleteCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostPenerusDeleteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostPenerusDeleteCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostPenerusDelete(req.costPenerusDetailId, req.costPenerusId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostPenerusDeleteEntity> CostPenerusDelete(Int32? costPenerusId, Int32? costPenerusDetailId)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostPenerusDelete @costPenerusId='{costPenerusId}', @costPenerusDetailId='{costPenerusDetailId}' ";
            var res = this._context.SP_ManifestCarrierCost_CostPenerusDeleteEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}

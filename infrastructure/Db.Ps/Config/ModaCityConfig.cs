﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class ModaCityConfig : IEntityTypeConfiguration<ModaCityEntity>
    {
        public void Configure(EntityTypeBuilder<ModaCityEntity> builder)
        {
            builder.HasKey(e => e.ModaCityId);
            builder.ToTable("ModaCity");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}

/****** Object:  Table [dbo].[CityConfig]    Script Date: 21/10/2020 10:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CityConfig](
	[CreatedAt] [datetime2](4) NULL,
	[UpdatedAt] [datetime2](4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[CityConfigId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NULL,
	[Cityid] [int] NULL,
	[ConfigName] [varchar](50) NULL,
	[ConfigValue] [varchar](50) NULL,
 CONSTRAINT [PK_CityConfig] PRIMARY KEY CLUSTERED 
(
	[CityConfigId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CityConfig] ADD  CONSTRAINT [DF_CityConfig_CreatedAt]  DEFAULT (getutcdate()) FOR [CreatedAt]
GO

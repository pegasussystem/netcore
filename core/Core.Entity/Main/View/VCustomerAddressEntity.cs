﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entity.Main.View
{
    public class VCustomerAddressEntity
    {
        public Int32? CustomerAddressId { get; set; }
        public Int32? CustomerId { get; set; }
        public String? CustomerAddress { get; set; }

    }


}

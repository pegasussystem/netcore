﻿using core.helper;
using Core.Entity.Ui;
using Interface.App.Base;
using Interface.App.Main;
using Interface.App.Main.CarrierManagement;
using Interface.App.Main.GoodsMonitoring_Receiver;
using Interface.App.Main.Manifest;
using Interface.App.Main.ManifestCarrier;
using Interface.App.Main.ManifestCarrier_Report;
using Interface.App.Main.SpbHourly;
using Interface.App.Main.SpbReceiver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface.App
{
    public interface IAppWrapperPs
    {
        //
        // BASE
        //IMenuApp MenuApp { get; }
        ILookupApp LookupApp { get; }
        IUserApp UserApp { get; }

        //
        // MAIN
        ICustomerApp CustomerApp { get;  }
        ICompanyApp CompanyApp { get;  }

        IMasterSubDistrictApp MasterSubDistrictApp { get; }
        IMasterUrbanApp MasterUrbanApp { get; }

        ISpbApp SpbApp { get; }
        IMasterCityApp MasterCityApp { get; }
        ISpbCreateApp SpbCreateApp { get; }
        IManifestSenderApp ManifestSenderApp{ get; }
        ISpbListSenderApp SpbListSenderApp{ get; }
        ISpbListReceiverApp SpbListReceiverApp{ get; }


        IManifestReceiverApp ManifestReceiverApp{ get; }
        IGoodsMonitoringApp GoodsMonitoringApp { get; }
        IManifestCarrierApp ManifestCarrierApp{ get; }
        ISpbStatisticApp SpbStatisticApp{ get; }
        //ICreateManifestCarrierUseCase CreateManifestCarrierUseCase { get; }
        //IReadManifestCarrierUseCase ReadManifestCarrierUseCase { get; }
        //IDeleteManifestCarrierUseCase DeleteManifestCarrierUseCase { get; }
        //IReadManifestCarrierItemByManifestCarrierUseCase ReadManifestCarrierItemByManifestCarrierUseCase { get; }
        //IManifestCarrierItemListUseCase ManifestCarrierItemListUseCase { get; }
        //IManifestCarrierCostListUseCase ManifestCarrierCostListUseCase { get; }
        IDdb_Manifest_WithTotalKoli_UseCase Ddb_Manifest_WithTotalKoli_UseCase { get; }
        I_Change_KoliNo_UseCase Change_KoliNo_UseCase { get; }
        I_OpenClose_Manifest_UseCase OpenClose_Manifest_UseCase { get; }
        I_OpenClose_Koli_UseCase OpenClose_Koli_UseCase { get; }
        I_Manifest_Ddb_WithWeight_UseCase Manifest_Ddb_WithWeight_UseCase { get; }
        //I_Sb_Carrier_UseCase Sb_Carrier_UseCase { get; }
        //IManifestCarrier_Report_List_UseCase ManifestCarrier_Report_List_UseCase { get; }
        IListSpbHourlyUseCase ListSpbHourlyUseCase { get; }
        IListSpbReceiverUsecase ListSpbReceiverUsecase { get; }
        IListReceiverSpbHourlyUseCase ListReceiverSpbHourlyUseCase { get; }
        IListGoodsMonitoringReceiverUseCase ListGoodsMonitoringReceiverUseCase { get; }

        ICoaParentApp CoaParentApp { get; }
        ICoaCodeApp CoaCodeApp { get; }
    }
}

﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using Interface.Other;
using Interface.Repo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace core.app.UseCases.Master.Round.Query
{
    public partial class ListCarrierQuery : IRequest<ReturnFormat>
    {
        public UiUserProfileEntity uiUserProfileEntity { get; set; }
    }

    public class ListCarrierQueryHandler : IRequestHandler<ListCarrierQuery, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;
        private ILog _log;
        private IRepoWarpperPs _repoPs;

        public ListCarrierQueryHandler(IApplicationDbContext context, ILog log, IRepoWarpperPs repo)
        {
            _context = context;
            _log = log;
            _repoPs = repo;
        }

        public async Task<ReturnFormat> Handle(ListCarrierQuery req, CancellationToken cancellationToken)
        {

            // --- variable 
            // --- --- --- --- ---
            ReturnFormat rtn = new ReturnFormat();
            // --- set variable
            // --- --- --- --- ---
            rtn.Status = StatusCodes.Status204NoContent;

            var query = from a in _context.LookupEntitys
                        where a.LookupKeyParent.Equals("carrier")
                        select new
                        {
                            carrierId       = a.LookupKey
                            , carrierName   = a.LookupValue
                        };

            if (query != null)
            {
                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = query.Select(
                    s => new
                    {
                        s.carrierId
                        , s.carrierName
                    }).OrderBy(o => o.carrierName);
            }

            // return
            return rtn;
        }
    }
}

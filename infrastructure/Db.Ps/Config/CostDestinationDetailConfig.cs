﻿using Core.Entity.Main;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Ps.Config
{
    class CostDestinationDetailConfig : IEntityTypeConfiguration<CostDestinationDetailEntity>
    {
        public void Configure(EntityTypeBuilder<CostDestinationDetailEntity> builder)
        {
            builder.HasKey(e => e.CostDestinationDetailId);
            builder.ToTable("CostDestinationDetail");

            builder.Property(p => p.CreatedAt)
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
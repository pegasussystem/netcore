﻿using core.app.Common.Interfaces;
using core.helper;
using Core.Entity.Ui;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entity.Main.Store_Procedure;
using Microsoft.EntityFrameworkCore;

namespace core.app.UseCases.ManifestCarrier.ManifestCarrierMaster.CostManifestCarrier.CostOrigin.Commands
{
    public partial class CostOriginUpdateCommand : IRequest<ReturnFormat>
    {
        public Int32? costOriginId { get; set; }
        public Int32? costOriginDetailId { get; set; }
        public String? baseCost { get; set; }
        public UiUserProfileEntity uiUserProfile { get; set; }
    }

    public class CostOriginUpdateCommandHandler : IRequestHandler<CostOriginUpdateCommand, ReturnFormat>
    {
        private readonly IApplicationDbContext _context;

        public CostOriginUpdateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReturnFormat> Handle(CostOriginUpdateCommand req, CancellationToken cancellationToken)
        {
            //
            // variable
            ReturnFormat rtn = new ReturnFormat();
            //
            // set variable

            // default status 
            rtn.Status = StatusCodes.Status204NoContent;


            try
            {
                // Mark as Changed
                var res = this.CostOriginUpdate(req.costOriginId, req.costOriginDetailId, req.baseCost, req.uiUserProfile.userId);

                await _context.SaveChangesAsync(cancellationToken);

                rtn.Status = StatusCodes.Status200OK;
                rtn.Data = res;

                //
                // return
                return rtn;
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                rtn.Status = StatusCodes.Status204NoContent;
                rtn.Data = null;
            }
            return rtn;
        }

        public IEnumerable<SP_ManifestCarrierCost_CostOriginUpdateEntity> CostOriginUpdate(Int32? costOriginId, Int32? costOriginDetailId, String? baseCost, Int32? userid)
        {
            String query = $@"EXEC SP_ManifestCarrierCost_CostOriginUpdate @costOriginid='{costOriginId}', @costOriginDetailid='{costOriginDetailId}', @baseCost='{baseCost}', @userId={userid} ";
            var res = this._context.SP_ManifestCarrierCost_CostOriginUpdateEntitys.FromSqlRaw(query).ToList();
            return res;
        }
    }
}
